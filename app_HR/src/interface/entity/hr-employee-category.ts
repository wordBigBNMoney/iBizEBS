/**
 * 员工类别
 *
 * @export
 * @interface Hr_employee_category
 */
export interface Hr_employee_category {

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    color?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    employee_ids?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    id?: any;

    /**
     * 员工标签
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    name?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    display_name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    __last_update?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    write_date?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    create_date?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    create_uid_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_employee_category
     */
    create_uid?: any;
}