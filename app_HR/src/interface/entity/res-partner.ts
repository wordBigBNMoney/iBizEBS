/**
 * 联系人
 *
 * @export
 * @interface Res_partner
 */
export interface Res_partner {

    /**
     * 图像
     *
     * @returns {*}
     * @memberof Res_partner
     */
    image?: any;

    /**
     * 地址类型
     *
     * @returns {*}
     * @memberof Res_partner
     */
    type?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Res_partner
     */
    color?: any;

    /**
     * 付款令牌
     *
     * @returns {*}
     * @memberof Res_partner
     */
    payment_token_ids?: any;

    /**
     * 发票
     *
     * @returns {*}
     * @memberof Res_partner
     */
    invoice_ids?: any;

    /**
     * #会议
     *
     * @returns {*}
     * @memberof Res_partner
     */
    meeting_count?: any;

    /**
     * ＃供应商账单
     *
     * @returns {*}
     * @memberof Res_partner
     */
    supplier_invoice_count?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof Res_partner
     */
    company_name?: any;

    /**
     * 在当前网站显示
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_published?: any;

    /**
     * 最近的发票和付款匹配时间
     *
     * @returns {*}
     * @memberof Res_partner
     */
    last_time_entries_checked?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_unread?: any;

    /**
     * 对此债务人的信任度
     *
     * @returns {*}
     * @memberof Res_partner
     */
    trust?: any;

    /**
     * 已开票总计
     *
     * @returns {*}
     * @memberof Res_partner
     */
    total_invoiced?: any;

    /**
     * 销售点订单计数
     *
     * @returns {*}
     * @memberof Res_partner
     */
    pos_order_count?: any;

    /**
     * 完整地址
     *
     * @returns {*}
     * @memberof Res_partner
     */
    contact_address?: any;

    /**
     * 发票
     *
     * @returns {*}
     * @memberof Res_partner
     */
    invoice_warn?: any;

    /**
     * 银行
     *
     * @returns {*}
     * @memberof Res_partner
     */
    bank_ids?: any;

    /**
     * 注册到期
     *
     * @returns {*}
     * @memberof Res_partner
     */
    signup_expiration?: any;

    /**
     * 采购订单数
     *
     * @returns {*}
     * @memberof Res_partner
     */
    purchase_order_count?: any;

    /**
     * 有未核销的分录
     *
     * @returns {*}
     * @memberof Res_partner
     */
    has_unreconciled_entries?: any;

    /**
     * 标签
     *
     * @returns {*}
     * @memberof Res_partner
     */
    category_id?: any;

    /**
     * customer rank
     *
     * @returns {*}
     * @memberof Res_partner
     */
    customer_rank?: any;

    /**
     * 网站业务伙伴的详细说明
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_description?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_main_attachment_id?: any;

    /**
     * 会议
     *
     * @returns {*}
     * @memberof Res_partner
     */
    meeting_ids?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Res_partner
     */
    employee?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_partner
     */
    display_name?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Res_partner
     */
    child_ids?: any;

    /**
     * 网站元说明
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_meta_description?: any;

    /**
     * 黑名单
     *
     * @returns {*}
     * @memberof Res_partner
     */
    is_blacklisted?: any;

    /**
     * 价格表
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_product_pricelist?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Res_partner
     */
    activity_date_deadline?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Res_partner
     */
    activity_type_id?: any;

    /**
     * 注册令牌 Token
     *
     * @returns {*}
     * @memberof Res_partner
     */
    signup_token?: any;

    /**
     * 公司是指业务伙伴
     *
     * @returns {*}
     * @memberof Res_partner
     */
    ref_company_ids?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_partner
     */
    is_company?: any;

    /**
     * 电话
     *
     * @returns {*}
     * @memberof Res_partner
     */
    phone?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_partner
     */
    create_date?: any;

    /**
     * 时区
     *
     * @returns {*}
     * @memberof Res_partner
     */
    tz?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Res_partner
     */
    event_count?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_has_error?: any;

    /**
     * 最后的提醒已经标志为已读
     *
     * @returns {*}
     * @memberof Res_partner
     */
    calendar_last_notif_ack?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_channel_ids?: any;

    /**
     * 注册令牌（Token）类型
     *
     * @returns {*}
     * @memberof Res_partner
     */
    signup_type?: any;

    /**
     * 格式化的邮件
     *
     * @returns {*}
     * @memberof Res_partner
     */
    email_formatted?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_message_ids?: any;

    /**
     * 共享合作伙伴
     *
     * @returns {*}
     * @memberof Res_partner
     */
    partner_share?: any;

    /**
     * 街道 2
     *
     * @returns {*}
     * @memberof Res_partner
     */
    street2?: any;

    /**
     * 工作岗位
     *
     * @returns {*}
     * @memberof Res_partner
     */
    function?: any;

    /**
     * 应付总计
     *
     * @returns {*}
     * @memberof Res_partner
     */
    debit?: any;

    /**
     * 付款令牌计数
     *
     * @returns {*}
     * @memberof Res_partner
     */
    payment_token_count?: any;

    /**
     * 内部参考
     *
     * @returns {*}
     * @memberof Res_partner
     */
    ref?: any;

    /**
     * 公司数据库ID
     *
     * @returns {*}
     * @memberof Res_partner
     */
    partner_gid?: any;

    /**
     * 注册令牌（ Token  ）是有效的
     *
     * @returns {*}
     * @memberof Res_partner
     */
    signup_valid?: any;

    /**
     * 网站opengraph图像
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_meta_og_img?: any;

    /**
     * 小尺寸图像
     *
     * @returns {*}
     * @memberof Res_partner
     */
    image_small?: any;

    /**
     * 银行
     *
     * @returns {*}
     * @memberof Res_partner
     */
    bank_account_count?: any;

    /**
     * 街道
     *
     * @returns {*}
     * @memberof Res_partner
     */
    street?: any;

    /**
     * 销售警告
     *
     * @returns {*}
     * @memberof Res_partner
     */
    sale_warn?: any;

    /**
     * 退回
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_bounce?: any;

    /**
     * 操作次数
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_needaction_counter?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_follower_ids?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Res_partner
     */
    opportunity_count?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof Res_partner
     */
    date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_partner
     */
    __last_update?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_partner_ids?: any;

    /**
     * 自己
     *
     * @returns {*}
     * @memberof Res_partner
     */
    self?: any;

    /**
     * IM的状态
     *
     * @returns {*}
     * @memberof Res_partner
     */
    im_status?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_partner
     */
    write_date?: any;

    /**
     * 错误个数
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_has_error_counter?: any;

    /**
     * 发票消息
     *
     * @returns {*}
     * @memberof Res_partner
     */
    invoice_warn_msg?: any;

    /**
     * 前置操作
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_needaction?: any;

    /**
     * 库存拣货
     *
     * @returns {*}
     * @memberof Res_partner
     */
    picking_warn?: any;

    /**
     * 客户合同
     *
     * @returns {*}
     * @memberof Res_partner
     */
    contract_ids?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_partner
     */
    currency_id?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website?: any;

    /**
     * 手机
     *
     * @returns {*}
     * @memberof Res_partner
     */
    mobile?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_attachment_count?: any;

    /**
     * 城市
     *
     * @returns {*}
     * @memberof Res_partner
     */
    city?: any;

    /**
     * 客户付款条款
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_payment_term_id?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Res_partner
     */
    user_ids?: any;

    /**
     * 网站meta关键词
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_meta_keywords?: any;

    /**
     * 渠道
     *
     * @returns {*}
     * @memberof Res_partner
     */
    channel_ids?: any;

    /**
     * 采购订单
     *
     * @returns {*}
     * @memberof Res_partner
     */
    purchase_warn?: any;

    /**
     * 日记账项目
     *
     * @returns {*}
     * @memberof Res_partner
     */
    journal_item_count?: any;

    /**
     * 供应商位置
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_stock_supplier?: any;

    /**
     * 应付账款
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_account_payable_id?: any;

    /**
     * 网站业务伙伴简介
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_short_description?: any;

    /**
     * 销售订单消息
     *
     * @returns {*}
     * @memberof Res_partner
     */
    sale_warn_msg?: any;

    /**
     * 应收总计
     *
     * @returns {*}
     * @memberof Res_partner
     */
    credit?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Res_partner
     */
    activity_state?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Res_partner
     */
    activity_ids?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_is_follower?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Res_partner
     */
    name?: any;

    /**
     * 税号
     *
     * @returns {*}
     * @memberof Res_partner
     */
    vat?: any;

    /**
     * 供应商付款条款
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_supplier_payment_term_id?: any;

    /**
     * supplier rank
     *
     * @returns {*}
     * @memberof Res_partner
     */
    supplier_rank?: any;

    /**
     * 客户位置
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_stock_customer?: any;

    /**
     * 便签
     *
     * @returns {*}
     * @memberof Res_partner
     */
    comment?: any;

    /**
     * 任务
     *
     * @returns {*}
     * @memberof Res_partner
     */
    task_ids?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_unread_counter?: any;

    /**
     * EMail
     *
     * @returns {*}
     * @memberof Res_partner
     */
    email?: any;

    /**
     * 采购订单消息
     *
     * @returns {*}
     * @memberof Res_partner
     */
    purchase_warn_msg?: any;

    /**
     * 网站meta标题
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_meta_title?: any;

    /**
     * 邮政编码
     *
     * @returns {*}
     * @memberof Res_partner
     */
    zip?: any;

    /**
     * 时区偏移
     *
     * @returns {*}
     * @memberof Res_partner
     */
    tz_offset?: any;

    /**
     * 公司类别
     *
     * @returns {*}
     * @memberof Res_partner
     */
    company_type?: any;

    /**
     * 下一个活动摘要
     *
     * @returns {*}
     * @memberof Res_partner
     */
    activity_summary?: any;

    /**
     * # 任务
     *
     * @returns {*}
     * @memberof Res_partner
     */
    task_count?: any;

    /**
     * 信用额度
     *
     * @returns {*}
     * @memberof Res_partner
     */
    credit_limit?: any;

    /**
     * 应收账款
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_account_receivable_id?: any;

    /**
     * 供应商货币
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_purchase_currency_id?: any;

    /**
     * 库存拣货单消息
     *
     * @returns {*}
     * @memberof Res_partner
     */
    picking_warn_msg?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_partner
     */
    id?: any;

    /**
     * 注册网址
     *
     * @returns {*}
     * @memberof Res_partner
     */
    signup_url?: any;

    /**
     * 语言
     *
     * @returns {*}
     * @memberof Res_partner
     */
    lang?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Res_partner
     */
    message_ids?: any;

    /**
     * 税科目调整
     *
     * @returns {*}
     * @memberof Res_partner
     */
    property_account_position_id?: any;

    /**
     * 登记网站
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_id?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Res_partner
     */
    active?: any;

    /**
     * 条码
     *
     * @returns {*}
     * @memberof Res_partner
     */
    barcode?: any;

    /**
     * 已发布
     *
     * @returns {*}
     * @memberof Res_partner
     */
    is_published?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Res_partner
     */
    activity_user_id?: any;

    /**
     * 销售订单个数
     *
     * @returns {*}
     * @memberof Res_partner
     */
    sale_order_count?: any;

    /**
     * 中等尺寸图像
     *
     * @returns {*}
     * @memberof Res_partner
     */
    image_medium?: any;

    /**
     * 附加信息
     *
     * @returns {*}
     * @memberof Res_partner
     */
    additional_info?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Res_partner
     */
    opportunity_ids?: any;

    /**
     * 合同统计
     *
     * @returns {*}
     * @memberof Res_partner
     */
    contracts_count?: any;

    /**
     * 应付限额
     *
     * @returns {*}
     * @memberof Res_partner
     */
    debit_limit?: any;

    /**
     * 网站网址
     *
     * @returns {*}
     * @memberof Res_partner
     */
    website_url?: any;

    /**
     * 销售订单
     *
     * @returns {*}
     * @memberof Res_partner
     */
    sale_order_ids?: any;

    /**
     * 最近的在线销售订单
     *
     * @returns {*}
     * @memberof Res_partner
     */
    last_website_so_id?: any;

    /**
     * SEO优化
     *
     * @returns {*}
     * @memberof Res_partner
     */
    is_seo_optimized?: any;

    /**
     * 公司名称实体
     *
     * @returns {*}
     * @memberof Res_partner
     */
    commercial_company_name?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_partner
     */
    write_uid_text?: any;

    /**
     * 称谓
     *
     * @returns {*}
     * @memberof Res_partner
     */
    title_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_partner
     */
    company_id_text?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_partner
     */
    country_id_text?: any;

    /**
     * 省/ 州
     *
     * @returns {*}
     * @memberof Res_partner
     */
    state_id_text?: any;

    /**
     * 商业实体
     *
     * @returns {*}
     * @memberof Res_partner
     */
    commercial_partner_id_text?: any;

    /**
     * 上级名称
     *
     * @returns {*}
     * @memberof Res_partner
     */
    parent_name?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof Res_partner
     */
    user_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_partner
     */
    create_uid_text?: any;

    /**
     * 工业
     *
     * @returns {*}
     * @memberof Res_partner
     */
    industry_id_text?: any;

    /**
     * 销售团队
     *
     * @returns {*}
     * @memberof Res_partner
     */
    team_id_text?: any;

    /**
     * 销售团队
     *
     * @returns {*}
     * @memberof Res_partner
     */
    team_id?: any;

    /**
     * 省/ 州
     *
     * @returns {*}
     * @memberof Res_partner
     */
    state_id?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof Res_partner
     */
    user_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_partner
     */
    create_uid?: any;

    /**
     * 关联公司
     *
     * @returns {*}
     * @memberof Res_partner
     */
    parent_id?: any;

    /**
     * 称谓
     *
     * @returns {*}
     * @memberof Res_partner
     */
    title?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_partner
     */
    write_uid?: any;

    /**
     * 商业实体
     *
     * @returns {*}
     * @memberof Res_partner
     */
    commercial_partner_id?: any;

    /**
     * 工业
     *
     * @returns {*}
     * @memberof Res_partner
     */
    industry_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_partner
     */
    company_id?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_partner
     */
    country_id?: any;
}