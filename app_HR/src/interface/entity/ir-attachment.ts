/**
 * 附件
 *
 * @export
 * @interface Ir_attachment
 */
export interface Ir_attachment {

    /**
     * res_field
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    res_field?: any;

    /**
     * file_size
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    file_size?: any;

    /**
     * store_fname
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    store_fname?: any;

    /**
     * public
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    ibizpublic?: any;

    /**
     * type
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    type?: any;

    /**
     * res_id
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    res_id?: any;

    /**
     * checksum
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    checksum?: any;

    /**
     * res_model
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    res_model?: any;

    /**
     * description
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    description?: any;

    /**
     * access_token
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    access_token?: any;

    /**
     * url
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    url?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    id?: any;

    /**
     * key
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    key?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    name?: any;

    /**
     * mimetype
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    mimetype?: any;

    /**
     * index_content
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    index_content?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Ir_attachment
     */
    company_id?: any;
}