/**
 * 简历类型
 *
 * @export
 * @interface Hr_resume_line_type
 */
export interface Hr_resume_line_type {

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Hr_resume_line_type
     */
    name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_resume_line_type
     */
    create_date?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Hr_resume_line_type
     */
    sequence?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_resume_line_type
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_resume_line_type
     */
    id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_resume_line_type
     */
    write_uid?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_resume_line_type
     */
    create_uid?: any;
}