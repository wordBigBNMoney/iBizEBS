/**
 * 分析行
 *
 * @export
 * @interface Account_analytic_line
 */
export interface Account_analytic_line {

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    write_date?: any;

    /**
     * 代码
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    code?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    unit_amount?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    date?: any;

    /**
     * 标签
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    tag_ids?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    __last_update?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    display_name?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    name?: any;

    /**
     * 参考.
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    ref?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    create_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    id?: any;

    /**
     * 金额
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    amount?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    account_id_text?: any;

    /**
     * 日记账项目
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    move_id_text?: any;

    /**
     * 销售订单项目
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    so_line_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    create_uid_text?: any;

    /**
     * 财务会计
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    general_account_id_text?: any;

    /**
     * 组
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    group_id_text?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    user_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    company_id_text?: any;

    /**
     * 业务伙伴
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    partner_id_text?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    currency_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    write_uid_text?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    product_id_text?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    product_uom_id_text?: any;

    /**
     * 销售订单项目
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    so_line?: any;

    /**
     * 财务会计
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    general_account_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    create_uid?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    product_uom_id?: any;

    /**
     * 业务伙伴
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    partner_id?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    product_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    company_id?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    account_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    write_uid?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    currency_id?: any;

    /**
     * 组
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    group_id?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    user_id?: any;

    /**
     * 日记账项目
     *
     * @returns {*}
     * @memberof Account_analytic_line
     */
    move_id?: any;
}