/**
 * 日记账项目
 *
 * @export
 * @interface Account_move_line
 */
export interface Account_move_line {

    /**
     * 分析标签
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    analytic_tag_ids?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    write_date?: any;

    /**
     * 标签
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    name?: any;

    /**
     * 余额
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    balance?: any;

    /**
     * 采用的税
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    tax_ids?: any;

    /**
     * 货币金额
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    amount_currency?: any;

    /**
     * 旧税额
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    tax_line_grouping_key?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    id?: any;

    /**
     * 已核销
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    reconciled?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    display_name?: any;

    /**
     * 现金余额
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    balance_cash_basis?: any;

    /**
     * 贷方现金基础
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    credit_cash_basis?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    quantity?: any;

    /**
     * 残值金额
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    amount_residual?: any;

    /**
     * 重新计算税额
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    recompute_tax_line?: any;

    /**
     * 出现在VAT报告
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    tax_exigible?: any;

    /**
     * 外币残余金额
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    amount_residual_currency?: any;

    /**
     * 基本金额
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    tax_base_amount?: any;

    /**
     * 上级状态
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    parent_state?: any;

    /**
     * 无催款
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    blocked?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    create_date?: any;

    /**
     * 匹配的贷方
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    matched_credit_ids?: any;

    /**
     * 分析明细行
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    analytic_line_ids?: any;

    /**
     * 到期日期
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    date_maturity?: any;

    /**
     * 借方
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    debit?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    __last_update?: any;

    /**
     * 对方
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    counterpart?: any;

    /**
     * 匹配的借记卡
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    matched_debit_ids?: any;

    /**
     * 借记现金基础
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    debit_cash_basis?: any;

    /**
     * 贷方
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    credit?: any;

    /**
     * 记叙
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    narration?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    product_id_text?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    user_type_id_text?: any;

    /**
     * 发起人付款
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    payment_id_text?: any;

    /**
     * 发起人税
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    tax_line_id_text?: any;

    /**
     * 匹配号码
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    full_reconcile_id_text?: any;

    /**
     * 费用
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    expense_id_text?: any;

    /**
     * 日记账分录
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    move_id_text?: any;

    /**
     * 日记账
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    journal_id_text?: any;

    /**
     * 公司货币
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    company_currency_id_text?: any;

    /**
     * 发票
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    invoice_id_text?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    analytic_account_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    create_uid_text?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    currency_id_text?: any;

    /**
     * 参考
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    ref?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    company_id_text?: any;

    /**
     * 报告
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    statement_id_text?: any;

    /**
     * 用该分录核销的银行核销单明细
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    statement_line_id_text?: any;

    /**
     * 业务伙伴
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    partner_id_text?: any;

    /**
     * 科目
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    account_id_text?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    date?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    product_uom_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    write_uid_text?: any;

    /**
     * 日记账分录
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    move_id?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    currency_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    company_id?: any;

    /**
     * 发起人付款
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    payment_id?: any;

    /**
     * 公司货币
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    company_currency_id?: any;

    /**
     * 日记账
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    journal_id?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    analytic_account_id?: any;

    /**
     * 发票
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    invoice_id?: any;

    /**
     * 发起人税
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    tax_line_id?: any;

    /**
     * 匹配号码
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    full_reconcile_id?: any;

    /**
     * 用该分录核销的银行核销单明细
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    statement_line_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    create_uid?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    product_id?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    product_uom_id?: any;

    /**
     * 业务伙伴
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    partner_id?: any;

    /**
     * 科目
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    account_id?: any;

    /**
     * 报告
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    statement_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    write_uid?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    user_type_id?: any;

    /**
     * 费用
     *
     * @returns {*}
     * @memberof Account_move_line
     */
    expense_id?: any;
}