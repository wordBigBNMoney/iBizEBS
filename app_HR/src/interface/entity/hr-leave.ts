/**
 * 休假
 *
 * @export
 * @interface Hr_leave
 */
export interface Hr_leave {

    /**
     * 分配模式
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    holiday_type?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    activity_state?: any;

    /**
     * 请假开始日期
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    request_date_from?: any;

    /**
     * 理由
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    notes?: any;

    /**
     * 持续时间（天）
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    number_of_days?: any;

    /**
     * 请求结束日期
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    request_date_to?: any;

    /**
     * 半天
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    request_unit_half?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_channel_ids?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    __last_update?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    state?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_needaction_counter?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_has_error?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    activity_user_id?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    activity_ids?: any;

    /**
     * 持续时间（天）
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    number_of_days_display?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    website_message_ids?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    activity_date_deadline?: any;

    /**
     * 时间从
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    request_hour_from?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_partner_ids?: any;

    /**
     * 下一活动摘要
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    activity_summary?: any;

    /**
     * 反映在最近的工资单中
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    payslip_status?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_unread_counter?: any;

    /**
     * 自定义时间
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    request_unit_hours?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    date_to?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_unread?: any;

    /**
     * 信息
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_ids?: any;

    /**
     * 能重置
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    can_reset?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_is_follower?: any;

    /**
     * 持续时间（小时）
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    number_of_hours_display?: any;

    /**
     * 长达数天的定制时间
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    request_unit_custom?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    id?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    activity_type_id?: any;

    /**
     * HR 备注
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    report_note?: any;

    /**
     * 时间到
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    request_hour_to?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    date_from?: any;

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_needaction?: any;

    /**
     * 链接申请
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    linked_request_ids?: any;

    /**
     * 能批准
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    can_approve?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    write_date?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_follower_ids?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    display_name?: any;

    /**
     * 日期开始
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    request_date_from_period?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    create_date?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_attachment_count?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_has_error_counter?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    message_main_attachment_id?: any;

    /**
     * 要求的（天/小时）
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    duration_display?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    mode_company_id_text?: any;

    /**
     * 休假
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    leave_type_request_unit?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    employee_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    write_uid_text?: any;

    /**
     * 会议
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    meeting_id_text?: any;

    /**
     * 第二次审批
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    second_approver_id_text?: any;

    /**
     * 员工标签
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    category_id_text?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    user_id_text?: any;

    /**
     * 首次审批
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    first_approver_id_text?: any;

    /**
     * 休假类型
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    holiday_status_id_text?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    manager_id_text?: any;

    /**
     * 上级
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    parent_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    create_uid_text?: any;

    /**
     * 验证人
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    validation_type?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    department_id_text?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    manager_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    write_uid?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    user_id?: any;

    /**
     * 上级
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    parent_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    mode_company_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    create_uid?: any;

    /**
     * 第二次审批
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    second_approver_id?: any;

    /**
     * 会议
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    meeting_id?: any;

    /**
     * 休假类型
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    holiday_status_id?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    department_id?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    employee_id?: any;

    /**
     * 员工标签
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    category_id?: any;

    /**
     * 首次审批
     *
     * @returns {*}
     * @memberof Hr_leave
     */
    first_approver_id?: any;
}