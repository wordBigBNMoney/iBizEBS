export default {
  fields: {
    display_name: "显示名称",
    __last_update: "最后修改日",
    id: "ID",
    res_id: "相关文档编号",
    subtype_ids: "子类型",
    res_model: "相关的文档模型名称",
    partner_id_text: "相关的业务伙伴",
    channel_id_text: "监听器",
    channel_id: "监听器",
    partner_id: "相关的业务伙伴",
  },
	views: {
		byreslistview: {
			caption: "文档关注者",
      		title: "文档关注者列表视图",
		},
	},
	list_list: {
		nodata: "",
		uiactions: {
		},
	},
};