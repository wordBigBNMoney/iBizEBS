export default {
  fields: {
    message_needaction: "需要采取行动",
    message_follower_ids: "关注者",
    absence_of_today: "今日缺勤",
    message_unread: "未读消息",
    leave_to_approve_count: "待批准休假",
    color: "颜色索引",
    message_partner_ids: "关注者(业务伙伴)",
    message_main_attachment_id: "附件",
    message_unread_counter: "未读消息计数器",
    name: "部门名称",
    message_is_follower: "是关注者",
    active: "有效",
    write_date: "最后更新时间",
    expense_sheets_to_approve_count: "待批准的费用报告",
    allocation_to_approve_count: "待批准的分配",
    new_hired_employee: "新雇用的员工",
    note: "笔记",
    child_ids: "子部门",
    message_channel_ids: "关注者(渠道)",
    message_has_error_counter: "错误数",
    expected_employee: "预期的员工",
    website_message_ids: "网站消息",
    message_has_error: "消息递送错误",
    total_employee: "员工总数",
    __last_update: "最后修改日",
    id: "ID",
    jobs_ids: "工作",
    message_ids: "信息",
    display_name: "显示名称",
    message_attachment_count: "附件数量",
    new_applicant_count: "新申请",
    create_date: "创建时间",
    complete_name: "完整名称",
    message_needaction_counter: "行动数量",
    member_ids: "会员",
    write_uid_text: "最后更新人",
    manager_id_text: "经理",
    create_uid_text: "创建人",
    parent_id_text: "上级部门",
    company_id_text: "公司",
    manager_id: "经理",
    create_uid: "创建人",
    parent_id: "上级部门",
    write_uid: "最后更新人",
    company_id: "公司",
  },
	views: {
		mastergridview: {
			caption: "HR 部门",
      		title: "首选表格视图",
		},
		pickupview: {
			caption: "HR 部门",
      		title: "HR 部门数据选择视图",
		},
		pickupgridview: {
			caption: "HR 部门",
      		title: "HR 部门选择表格视图",
		},
		mastertabinfoview: {
			caption: "HR 部门",
      		title: "主信息总览视图",
		},
		masterquickview: {
			caption: "HR 部门",
      		title: "快速新建",
		},
	},
	ef_masterquick_form: {
		details: {
			group1: "基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "部门名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "部门名称", 
			company_id_text: "公司", 
			manager_id_text: "经理", 
			parent_id_text: "上级部门", 
			parent_id: "上级部门", 
			manager_id: "经理", 
			company_id: "公司", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	if_datapanel_form: {
		details: {
			group1: "基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "部门名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "部门名称",
		},
		uiactions: {
		},
	},
	master_grid: {
		nodata: "",
		columns: {
			id: "ID",
			name: "部门名称",
			display_name: "显示名称",
			complete_name: "完整名称",
			manager_id: "经理",
			manager_id_text: "经理",
			company_id: "公司",
			company_id_text: "公司",
			parent_id: "上级部门",
			parent_id_text: "上级部门",
			active: "有效",
			note: "笔记",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	sf_master_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	mastertabinfoviewtoolbar_toolbar: {
		tbitem12: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	mastergridviewtoolbar_toolbar: {
		tbitem1_masterquick: {
			caption: "新建",
			tip: "新建",
		},
		tbitem2: {
			caption: "-",
			tip: "",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};