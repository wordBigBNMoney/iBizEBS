export default {
  fields: {
    sequence: "序号",
    display_name: "显示名称",
    __last_update: "最后修改日",
    create_date: "创建时间",
    name: "合同类型",
    write_date: "最后更新时间",
    id: "ID",
    write_uid_text: "最后更新人",
    create_uid_text: "创建人",
    write_uid: "最后更新人",
    create_uid: "创建人",
  },
};