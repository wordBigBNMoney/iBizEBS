export default {
  fields: {
    id: "ID",
    name: "名称",
    __last_update: "最后修改日",
    parent_path: "父级路径",
    create_date: "创建时间",
    display_name: "显示名称",
    complete_name: "完整名称",
    description: "说明",
    children_ids: "下级",
    write_date: "最后更新时间",
    parent_id_text: "上级",
    write_uid_text: "最后更新人",
    create_uid_text: "创建人",
    company_id_text: "公司",
    write_uid: "最后更新人",
    create_uid: "创建人",
    company_id: "公司",
    parent_id: "上级",
  },
	views: {
		pickupgridview: {
			caption: "分析类别",
      		title: "分析类别选择表格视图",
		},
		pickupview: {
			caption: "分析类别",
      		title: "分析类别数据选择视图",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};