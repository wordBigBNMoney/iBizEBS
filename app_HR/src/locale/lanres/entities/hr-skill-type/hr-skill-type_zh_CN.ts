export default {
  fields: {
    create_date: "创建时间",
    write_date: "最后更新时间",
    id: "ID",
    name: "名称",
    write_uid: "ID",
    create_uid: "ID",
  },
	views: {
		basiceditview: {
			caption: "供应商价格表",
      		title: "配置信息编辑视图",
		},
		basicquickview: {
			caption: "快速新建",
      		title: "快速新建视图",
		},
		basiclistexpview: {
			caption: "技能类型",
      		title: "配置列表导航视图",
		},
	},
	ef_basic_form: {
		details: {
			grouppanel3: "基本信息", 
			druipart1: "", 
			grouppanel1: "技能", 
			druipart2: "", 
			grouppanel2: "等级", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "名称", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	ef_basicquick_form: {
		details: {
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "名称", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	basiclistexpviewlistexpbar_toolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
	},
	basic_list: {
		nodata: "",
		uiactions: {
		},
	},
};