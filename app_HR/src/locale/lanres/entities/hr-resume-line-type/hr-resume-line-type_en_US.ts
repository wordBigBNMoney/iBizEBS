export default {
  fields: {
    name: "名称",
    create_date: "创建时间",
    sequence: "序号",
    write_date: "最后更新时间",
    id: "ID",
    write_uid: "ID",
    create_uid: "ID",
  },
	views: {
		pickupgridview: {
			caption: "简历类型",
      		title: "简历类型选择表格视图",
		},
		editview: {
			caption: "简历类型",
      		title: "简历类型编辑视图",
		},
		pickupview: {
			caption: "简历类型",
      		title: "简历类型数据选择视图",
		},
		lineedit: {
			caption: "简历类型",
      		title: "行编辑表格视图",
		},
	},
	main_form: {
		details: {
			group1: "简历类型基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	main_grid: {
		nodata: "",
		columns: {
		},
		uiactions: {
		},
	},
	lineedit_grid: {
		nodata: "",
		columns: {
			id: "ID",
			sequence: "序号",
			name: "名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	lineedittoolbar_toolbar: {
		tbitem24: {
			caption: "行编辑",
			tip: "行编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem25: {
			caption: "新建行",
			tip: "新建行",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "保存行",
			tip: "保存行",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem11: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
	},
};