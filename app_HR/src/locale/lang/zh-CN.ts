import { Util } from '@/utils/util/util';
import zhCNUser from '../user/zh-CN.user';
import account_analytic_line_zh_CN from '@locale/lanres/entities/account-analytic-line/account-analytic-line_zh_CN';
import hr_employee_zh_CN from '@locale/lanres/entities/hr-employee/hr-employee_zh_CN';
import res_currency_zh_CN from '@locale/lanres/entities/res-currency/res-currency_zh_CN';
import hr_job_zh_CN from '@locale/lanres/entities/hr-job/hr-job_zh_CN';
import mail_followers_zh_CN from '@locale/lanres/entities/mail-followers/mail-followers_zh_CN';
import account_move_line_zh_CN from '@locale/lanres/entities/account-move-line/account-move-line_zh_CN';
import mail_activity_zh_CN from '@locale/lanres/entities/mail-activity/mail-activity_zh_CN';
import hr_skill_type_zh_CN from '@locale/lanres/entities/hr-skill-type/hr-skill-type_zh_CN';
import account_analytic_group_zh_CN from '@locale/lanres/entities/account-analytic-group/account-analytic-group_zh_CN';
import hr_resume_line_zh_CN from '@locale/lanres/entities/hr-resume-line/hr-resume-line_zh_CN';
import hr_skill_zh_CN from '@locale/lanres/entities/hr-skill/hr-skill_zh_CN';
import res_company_zh_CN from '@locale/lanres/entities/res-company/res-company_zh_CN';
import hr_recruitment_source_zh_CN from '@locale/lanres/entities/hr-recruitment-source/hr-recruitment-source_zh_CN';
import hr_leave_zh_CN from '@locale/lanres/entities/hr-leave/hr-leave_zh_CN';
import res_users_zh_CN from '@locale/lanres/entities/res-users/res-users_zh_CN';
import survey_survey_zh_CN from '@locale/lanres/entities/survey-survey/survey-survey_zh_CN';
import account_analytic_account_zh_CN from '@locale/lanres/entities/account-analytic-account/account-analytic-account_zh_CN';
import ir_attachment_zh_CN from '@locale/lanres/entities/ir-attachment/ir-attachment_zh_CN';
import hr_skill_level_zh_CN from '@locale/lanres/entities/hr-skill-level/hr-skill-level_zh_CN';
import hr_department_zh_CN from '@locale/lanres/entities/hr-department/hr-department_zh_CN';
import gamification_goal_zh_CN from '@locale/lanres/entities/gamification-goal/gamification-goal_zh_CN';
import hr_contract_zh_CN from '@locale/lanres/entities/hr-contract/hr-contract_zh_CN';
import hr_employee_category_zh_CN from '@locale/lanres/entities/hr-employee-category/hr-employee-category_zh_CN';
import resource_calendar_zh_CN from '@locale/lanres/entities/resource-calendar/resource-calendar_zh_CN';
import mail_message_zh_CN from '@locale/lanres/entities/mail-message/mail-message_zh_CN';
import hr_contract_type_zh_CN from '@locale/lanres/entities/hr-contract-type/hr-contract-type_zh_CN';
import res_config_settings_zh_CN from '@locale/lanres/entities/res-config-settings/res-config-settings_zh_CN';
import product_product_zh_CN from '@locale/lanres/entities/product-product/product-product_zh_CN';
import hr_resume_line_type_zh_CN from '@locale/lanres/entities/hr-resume-line-type/hr-resume-line-type_zh_CN';
import gamification_challenge_zh_CN from '@locale/lanres/entities/gamification-challenge/gamification-challenge_zh_CN';
import mail_tracking_value_zh_CN from '@locale/lanres/entities/mail-tracking-value/mail-tracking-value_zh_CN';
import product_category_zh_CN from '@locale/lanres/entities/product-category/product-category_zh_CN';
import res_partner_zh_CN from '@locale/lanres/entities/res-partner/res-partner_zh_CN';
import sale_order_line_zh_CN from '@locale/lanres/entities/sale-order-line/sale-order-line_zh_CN';
import resource_resource_zh_CN from '@locale/lanres/entities/resource-resource/resource-resource_zh_CN';
import gamification_badge_zh_CN from '@locale/lanres/entities/gamification-badge/gamification-badge_zh_CN';
import hr_leave_type_zh_CN from '@locale/lanres/entities/hr-leave-type/hr-leave-type_zh_CN';
import maintenance_equipment_zh_CN from '@locale/lanres/entities/maintenance-equipment/maintenance-equipment_zh_CN';
import account_account_zh_CN from '@locale/lanres/entities/account-account/account-account_zh_CN';
import components_zh_CN from '@locale/lanres/components/components_zh_CN';
import codelist_zh_CN from '@locale/lanres/codelist/codelist_zh_CN';
import userCustom_zh_CN from '@locale/lanres/userCustom/userCustom_zh_CN';

const data: any = {
    app: {
        commonWords:{
            error: "失败",
            success: "成功",
            ok: "确认",
            cancel: "取消",
            save: "保存",
            codeNotExist: "代码表不存在",
            reqException: "请求异常",
            sysException: "系统异常",
            warning: "警告",
            wrong: "错误",
            rulesException: "值规则校验异常",
            saveSuccess: "保存成功",
            saveFailed: "保存失败",
            deleteSuccess: "删除成功！",
            deleteError: "删除失败！",
            delDataFail: "删除数据失败",
            noData: "暂无数据",
            startsuccess:"启动成功",
            createFailed: '无法创建',
            isExist: '已存在'
        },
        local:{
            new: "新建",
            add: "增加",
        },
        gridpage: {
            choicecolumns: "选择列",
            refresh: "刷新",
            show: "显示",
            records: "条",
            totle: "共",
            noData: "无数据",
            valueVail: "值不能为空",
            notConfig: {
                fetchAction: "视图表格fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图表格createAction参数未配置",
                updateAction: "视图表格updateAction参数未配置",
                loaddraftAction: "视图表格loaddraftAction参数未配置",
            },
            data: "数据",
            delDataFail: "删除数据失败",
            delSuccess: "删除成功!",
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
            notBatch: "批量添加未实现",
            grid: "表",
            exportFail: "数据导出失败",
            sum: "合计",
            formitemFailed: "表单项更新失败",
        },
        list: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图列表createAction参数未配置",
                updateAction: "视图列表updateAction参数未配置",
            },
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
        },
        listExpBar: {
            title: "列表导航栏",
        },
        wfExpBar: {
            title: "流程导航栏",
        },
        calendarExpBar:{
            title: "日历导航栏",
        },
        treeExpBar: {
            title: "树视图导航栏",
        },
        portlet: {
            noExtensions: "无扩展插件",
        },
        tabpage: {
            sureclosetip: {
                title: "关闭提醒",
                content: "表单数据已经修改，确定要关闭？",
            },
            closeall: "关闭所有",
            closeother: "关闭其他",
        },
        fileUpload: {
            caption: "上传",
        },
        searchButton: {
            search: "搜索",
            reset: "重置",
        },
        calendar:{
          today: "今天",
          month: "月",
          week: "周",
          day: "天",
          list: "列",
          dateSelectModalTitle: "选择要跳转的时间",
          gotoDate: "跳转",
          from: "从",
          to: "至",
        },
        // 非实体视图
        views: {
            hrindexview: {
                caption: "员工",
                title: "员工首页视图",
            },
        },
        utilview:{
            importview:"导入数据",
            warning:"警告",
            info:"请配置数据导入项" 
        },
        menus: {
            hrindexview: {
                user_menus: "用户菜单",
                top_menus: "顶部菜单",
                left_exp: "左侧菜单",
                menuitem1: "员工",
                menuitem4: "员工",
                menuitem5: "合同",
                menuitem6: "工作岗位",
                menuitem3: "配置",
                menuitem2: "设置",
                menuitem14: "员工",
                menuitem15: "标签",
                menuitem16: "技能",
                menuitem7: "部门",
                menuitem17: "简历",
                menuitem18: "类型",
                menuitem8: "活动规划",
                menuitem19: "规划类型",
                menuitem10: "计划",
                menuitem9: "挑战",
                menuitem11: "徽章",
                menuitem12: "挑战",
                menuitem13: "历史目标",
                bottom_exp: "底部内容",
                footer_left: "底部左侧",
                footer_center: "底部中间",
                footer_right: "底部右侧",
            },
        },
        formpage:{
            desc1: "操作失败,未能找到当前表单项",
            desc2: "无法继续操作",
            notconfig: {
                loadaction: "视图表单loadAction参数未配置",
                loaddraftaction: "视图表单loaddraftAction参数未配置",
                actionname: "视图表单'+actionName+'参数未配置",
                removeaction: "视图表单removeAction参数未配置",
            },
            saveerror: "保存数据发生错误",
            savecontent: "数据不一致，可能后台数据已经被修改,是否要重新加载数据？",
            valuecheckex: "值规则校验异常",
            savesuccess: "保存成功！",
            deletesuccess: "删除成功！",  
            workflow: {
                starterror: "工作流启动失败",
                startsuccess: "工作流启动成功",
                submiterror: "工作流提交失败",
                submitsuccess: "工作流提交成功",
            },
            updateerror: "表单项更新失败",     
        },
        gridBar: {
            title: "表格导航栏",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "视图多编辑视图面板fetchAction参数未配置",
                loaddraftAction: "视图多编辑视图面板loaddraftAction参数未配置",
            },
        },
        dataViewExpBar: {
            title: "卡片视图导航栏",
        },
        kanban: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
            },
            delete1: "确认要删除 ",
            delete2: "删除操作将不可恢复？",
        },
        dashBoard: {
            handleClick: {
                title: "面板设计",
            },
        },
        dataView: {
            sum: "共",
            data: "条数据",
        },
        chart: {
            undefined: "未定义",
            quarter: "季度",   
            year: "年",
        },
        searchForm: {
            notConfig: {
                loadAction: "视图搜索表单loadAction参数未配置",
                loaddraftAction: "视图搜索表单loaddraftAction参数未配置",
            },
            custom: "存储自定义查询",
            title: "名称",
        },
        wizardPanel: {
            back: "上一步",
            next: "下一步",
            complete: "完成",
            preactionmessage:"未配置计算上一步行为"
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "尊敬的客户您好，您已成功退出系统，将在",
                prompt2: "秒后跳转至",
                logingPage: "登录页",
            },
            appWfstepTraceView: {
                title: "应用流程处理记录视图",
            },
            appWfstepDataView: {
                title: "应用流程跟踪视图",
            },
            appLoginView: {
                username: "用户名",
                password: "密码",
                login: "登录",
            },
        },
    },
    form: {
        group: {
            show_more: "显示更多",
            hidden_more: "隐藏更多"
        }
    },
    entities: {
        account_analytic_line: account_analytic_line_zh_CN,
        hr_employee: hr_employee_zh_CN,
        res_currency: res_currency_zh_CN,
        hr_job: hr_job_zh_CN,
        mail_followers: mail_followers_zh_CN,
        account_move_line: account_move_line_zh_CN,
        mail_activity: mail_activity_zh_CN,
        hr_skill_type: hr_skill_type_zh_CN,
        account_analytic_group: account_analytic_group_zh_CN,
        hr_resume_line: hr_resume_line_zh_CN,
        hr_skill: hr_skill_zh_CN,
        res_company: res_company_zh_CN,
        hr_recruitment_source: hr_recruitment_source_zh_CN,
        hr_leave: hr_leave_zh_CN,
        res_users: res_users_zh_CN,
        survey_survey: survey_survey_zh_CN,
        account_analytic_account: account_analytic_account_zh_CN,
        ir_attachment: ir_attachment_zh_CN,
        hr_skill_level: hr_skill_level_zh_CN,
        hr_department: hr_department_zh_CN,
        gamification_goal: gamification_goal_zh_CN,
        hr_contract: hr_contract_zh_CN,
        hr_employee_category: hr_employee_category_zh_CN,
        resource_calendar: resource_calendar_zh_CN,
        mail_message: mail_message_zh_CN,
        hr_contract_type: hr_contract_type_zh_CN,
        res_config_settings: res_config_settings_zh_CN,
        product_product: product_product_zh_CN,
        hr_resume_line_type: hr_resume_line_type_zh_CN,
        gamification_challenge: gamification_challenge_zh_CN,
        mail_tracking_value: mail_tracking_value_zh_CN,
        product_category: product_category_zh_CN,
        res_partner: res_partner_zh_CN,
        sale_order_line: sale_order_line_zh_CN,
        resource_resource: resource_resource_zh_CN,
        gamification_badge: gamification_badge_zh_CN,
        hr_leave_type: hr_leave_type_zh_CN,
        maintenance_equipment: maintenance_equipment_zh_CN,
        account_account: account_account_zh_CN,
    },
    components: components_zh_CN,
    codelist: codelist_zh_CN,
    userCustom: userCustom_zh_CN,
};
// 合并用户自定义多语言
Util.mergeDeepObject(data, zhCNUser);
// 默认导出
export default data;