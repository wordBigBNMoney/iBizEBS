import Hr_recruitment_sourceAuthServiceBase from './hr-recruitment-source-auth-service-base';


/**
 * 应聘者来源权限服务对象
 *
 * @export
 * @class Hr_recruitment_sourceAuthService
 * @extends {Hr_recruitment_sourceAuthServiceBase}
 */
export default class Hr_recruitment_sourceAuthService extends Hr_recruitment_sourceAuthServiceBase {

    /**
     * Creates an instance of  Hr_recruitment_sourceAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_recruitment_sourceAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}