import Hr_leaveAuthServiceBase from './hr-leave-auth-service-base';


/**
 * 休假权限服务对象
 *
 * @export
 * @class Hr_leaveAuthService
 * @extends {Hr_leaveAuthServiceBase}
 */
export default class Hr_leaveAuthService extends Hr_leaveAuthServiceBase {

    /**
     * Creates an instance of  Hr_leaveAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_leaveAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}