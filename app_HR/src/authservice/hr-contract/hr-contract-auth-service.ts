import Hr_contractAuthServiceBase from './hr-contract-auth-service-base';


/**
 * 合同权限服务对象
 *
 * @export
 * @class Hr_contractAuthService
 * @extends {Hr_contractAuthServiceBase}
 */
export default class Hr_contractAuthService extends Hr_contractAuthServiceBase {

    /**
     * Creates an instance of  Hr_contractAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_contractAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}