import AuthService from '../auth-service';

/**
 * 关注消息类型权限服务对象基类
 *
 * @export
 * @class Mail_followers_mail_message_subtype_relAuthServiceBase
 * @extends {AuthService}
 */
export default class Mail_followers_mail_message_subtype_relAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  Mail_followers_mail_message_subtype_relAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_followers_mail_message_subtype_relAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof Mail_followers_mail_message_subtype_relAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = this.getSysOPPrivs();
        let copyDefaultOPPrivs:any = JSON.parse(JSON.stringify(curDefaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        // 统一资源优先
        Object.keys(curDefaultOPPrivs).forEach((name:string) => {
            if(this.sysOPPrivsMap.get(name) && copyDefaultOPPrivs[name] === 0){
                curDefaultOPPrivs[name] = copyDefaultOPPrivs[name];
            }
        });
        return curDefaultOPPrivs;
    }

}