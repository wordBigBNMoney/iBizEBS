import Hr_contract_typeAuthServiceBase from './hr-contract-type-auth-service-base';


/**
 * 合同类型权限服务对象
 *
 * @export
 * @class Hr_contract_typeAuthService
 * @extends {Hr_contract_typeAuthServiceBase}
 */
export default class Hr_contract_typeAuthService extends Hr_contract_typeAuthServiceBase {

    /**
     * Creates an instance of  Hr_contract_typeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_contract_typeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}