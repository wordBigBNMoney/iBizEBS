import Mail_followersAuthServiceBase from './mail-followers-auth-service-base';


/**
 * 文档关注者权限服务对象
 *
 * @export
 * @class Mail_followersAuthService
 * @extends {Mail_followersAuthServiceBase}
 */
export default class Mail_followersAuthService extends Mail_followersAuthServiceBase {

    /**
     * Creates an instance of  Mail_followersAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_followersAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}