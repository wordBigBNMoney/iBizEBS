import Res_currencyAuthServiceBase from './res-currency-auth-service-base';


/**
 * 币种权限服务对象
 *
 * @export
 * @class Res_currencyAuthService
 * @extends {Res_currencyAuthServiceBase}
 */
export default class Res_currencyAuthService extends Res_currencyAuthServiceBase {

    /**
     * Creates an instance of  Res_currencyAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_currencyAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}