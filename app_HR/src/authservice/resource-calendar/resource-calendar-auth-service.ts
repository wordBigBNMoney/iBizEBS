import Resource_calendarAuthServiceBase from './resource-calendar-auth-service-base';


/**
 * 资源工作时间权限服务对象
 *
 * @export
 * @class Resource_calendarAuthService
 * @extends {Resource_calendarAuthServiceBase}
 */
export default class Resource_calendarAuthService extends Resource_calendarAuthServiceBase {

    /**
     * Creates an instance of  Resource_calendarAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Resource_calendarAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}