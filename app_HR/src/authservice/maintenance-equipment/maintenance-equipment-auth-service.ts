import Maintenance_equipmentAuthServiceBase from './maintenance-equipment-auth-service-base';


/**
 * 保养设备权限服务对象
 *
 * @export
 * @class Maintenance_equipmentAuthService
 * @extends {Maintenance_equipmentAuthServiceBase}
 */
export default class Maintenance_equipmentAuthService extends Maintenance_equipmentAuthServiceBase {

    /**
     * Creates an instance of  Maintenance_equipmentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Maintenance_equipmentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}