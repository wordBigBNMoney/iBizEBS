import Gamification_badgeAuthServiceBase from './gamification-badge-auth-service-base';


/**
 * 游戏化徽章权限服务对象
 *
 * @export
 * @class Gamification_badgeAuthService
 * @extends {Gamification_badgeAuthServiceBase}
 */
export default class Gamification_badgeAuthService extends Gamification_badgeAuthServiceBase {

    /**
     * Creates an instance of  Gamification_badgeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_badgeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}