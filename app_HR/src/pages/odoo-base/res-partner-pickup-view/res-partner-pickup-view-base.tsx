import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Res_partnerService from '@/service/res-partner/res-partner-service';
import Res_partnerAuthService from '@/authservice/res-partner/res-partner-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Res_partnerUIService from '@/uiservice/res-partner/res-partner-ui-service';

/**
 * 联系人数据选择视图视图基类
 *
 * @export
 * @class Res_partnerPickupViewBase
 * @extends {PickupViewBase}
 */
export class Res_partnerPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupViewBase
     */
    protected appDeName: string = 'res_partner';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Res_partnerService}
     * @memberof Res_partnerPickupViewBase
     */
    protected appEntityService: Res_partnerService = new Res_partnerService;

    /**
     * 实体权限服务对象
     *
     * @type Res_partnerUIService
     * @memberof Res_partnerPickupViewBase
     */
    public appUIService: Res_partnerUIService = new Res_partnerUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_partnerPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_partner.views.pickupview.caption',
        srfTitle: 'entities.res_partner.views.pickupview.title',
        srfSubTitle: 'entities.res_partner.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_partnerPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupViewBase
     */
	protected viewtag: string = '3d90688394c225f742436305b4f54a92';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupViewBase
     */ 
    protected viewName:string = "res_partnerPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_partnerPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_partnerPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_partnerPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'res_partner',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}