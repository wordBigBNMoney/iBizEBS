import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Survey_surveyService from '@/service/survey-survey/survey-survey-service';
import Survey_surveyAuthService from '@/authservice/survey-survey/survey-survey-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Survey_surveyUIService from '@/uiservice/survey-survey/survey-survey-ui-service';

/**
 * 问卷数据选择视图视图基类
 *
 * @export
 * @class Survey_surveyPickupViewBase
 * @extends {PickupViewBase}
 */
export class Survey_surveyPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupViewBase
     */
    protected appDeName: string = 'survey_survey';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 实体服务对象
     *
     * @type {Survey_surveyService}
     * @memberof Survey_surveyPickupViewBase
     */
    protected appEntityService: Survey_surveyService = new Survey_surveyService;

    /**
     * 实体权限服务对象
     *
     * @type Survey_surveyUIService
     * @memberof Survey_surveyPickupViewBase
     */
    public appUIService: Survey_surveyUIService = new Survey_surveyUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Survey_surveyPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.survey_survey.views.pickupview.caption',
        srfTitle: 'entities.survey_survey.views.pickupview.title',
        srfSubTitle: 'entities.survey_survey.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Survey_surveyPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupViewBase
     */
	protected viewtag: string = '69e4c3d30d14aa7afbc44c1f9cc3b887';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupViewBase
     */ 
    protected viewName:string = "survey_surveyPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Survey_surveyPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Survey_surveyPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Survey_surveyPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'survey_survey',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}