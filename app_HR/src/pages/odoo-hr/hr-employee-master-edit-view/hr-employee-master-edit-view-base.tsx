import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Hr_employeeService from '@/service/hr-employee/hr-employee-service';
import Hr_employeeAuthService from '@/authservice/hr-employee/hr-employee-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Hr_employeeUIService from '@/uiservice/hr-employee/hr-employee-ui-service';

/**
 * 主编辑视图视图基类
 *
 * @export
 * @class Hr_employeeMasterEditViewBase
 * @extends {EditViewBase}
 */
export class Hr_employeeMasterEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterEditViewBase
     */
    protected appDeName: string = 'hr_employee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterEditViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterEditViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Hr_employeeService}
     * @memberof Hr_employeeMasterEditViewBase
     */
    protected appEntityService: Hr_employeeService = new Hr_employeeService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_employeeUIService
     * @memberof Hr_employeeMasterEditViewBase
     */
    public appUIService: Hr_employeeUIService = new Hr_employeeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_employeeMasterEditViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeeMasterEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_employee.views.mastereditview.caption',
        srfTitle: 'entities.hr_employee.views.mastereditview.title',
        srfSubTitle: 'entities.hr_employee.views.mastereditview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeeMasterEditViewBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_form: { name: 'form', type: 'FORM' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Hr_employeeMasterEditView
     */
    public toolBarModels: any = {
    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterEditViewBase
     */
	protected viewtag: string = 'dd123e093294210ed5319ccd063709c0';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterEditViewBase
     */ 
    protected viewName:string = "hr_employeeMasterEditView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_employeeMasterEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_employeeMasterEditViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_employeeMasterEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'hr_employee',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeMasterEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeMasterEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeMasterEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}