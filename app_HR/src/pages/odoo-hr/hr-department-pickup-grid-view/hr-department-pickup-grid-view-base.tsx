import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import Hr_departmentAuthService from '@/authservice/hr-department/hr-department-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Hr_departmentUIService from '@/uiservice/hr-department/hr-department-ui-service';

/**
 * HR 部门选择表格视图视图基类
 *
 * @export
 * @class Hr_departmentPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Hr_departmentPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupGridViewBase
     */
    protected appDeName: string = 'hr_department';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupGridViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Hr_departmentService}
     * @memberof Hr_departmentPickupGridViewBase
     */
    protected appEntityService: Hr_departmentService = new Hr_departmentService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_departmentUIService
     * @memberof Hr_departmentPickupGridViewBase
     */
    public appUIService: Hr_departmentUIService = new Hr_departmentUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_departmentPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_department.views.pickupgridview.caption',
        srfTitle: 'entities.hr_department.views.pickupgridview.title',
        srfSubTitle: 'entities.hr_department.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_departmentPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupGridViewBase
     */
	protected viewtag: string = '9e9b93bac1ef049bb6e24b2d217aae7a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupGridViewBase
     */ 
    protected viewName:string = "hr_departmentPickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_departmentPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_departmentPickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_departmentPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'hr_department',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Hr_departmentPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}