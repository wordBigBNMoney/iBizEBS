import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Hr_skill_typeService from '@/service/hr-skill-type/hr-skill-type-service';
import Hr_skill_typeAuthService from '@/authservice/hr-skill-type/hr-skill-type-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Hr_skill_typeUIService from '@/uiservice/hr-skill-type/hr-skill-type-ui-service';

/**
 * 快速新建视图视图基类
 *
 * @export
 * @class Hr_skill_typeBasicQuickViewBase
 * @extends {OptionViewBase}
 */
export class Hr_skill_typeBasicQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    protected appDeName: string = 'hr_skill_type';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Hr_skill_typeService}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    protected appEntityService: Hr_skill_typeService = new Hr_skill_typeService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_skill_typeUIService
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    public appUIService: Hr_skill_typeUIService = new Hr_skill_typeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_skill_type.views.basicquickview.caption',
        srfTitle: 'entities.hr_skill_type.views.basicquickview.title',
        srfSubTitle: 'entities.hr_skill_type.views.basicquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
	protected viewtag: string = '4dd3028fce390013a76d56212a66d426';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */ 
    protected viewName:string = "hr_skill_typeBasicQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_skill_typeBasicQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'hr_skill_type',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}