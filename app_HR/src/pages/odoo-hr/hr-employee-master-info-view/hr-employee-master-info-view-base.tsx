import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Hr_employeeService from '@/service/hr-employee/hr-employee-service';
import Hr_employeeAuthService from '@/authservice/hr-employee/hr-employee-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Hr_employeeUIService from '@/uiservice/hr-employee/hr-employee-ui-service';

/**
 * 主信息概览视图基类
 *
 * @export
 * @class Hr_employeeMasterInfoViewBase
 * @extends {EditViewBase}
 */
export class Hr_employeeMasterInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterInfoViewBase
     */
    protected appDeName: string = 'hr_employee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterInfoViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Hr_employeeService}
     * @memberof Hr_employeeMasterInfoViewBase
     */
    protected appEntityService: Hr_employeeService = new Hr_employeeService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_employeeUIService
     * @memberof Hr_employeeMasterInfoViewBase
     */
    public appUIService: Hr_employeeUIService = new Hr_employeeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_employeeMasterInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeeMasterInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_employee.views.masterinfoview.caption',
        srfTitle: 'entities.hr_employee.views.masterinfoview.title',
        srfSubTitle: 'entities.hr_employee.views.masterinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeeMasterInfoViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterInfoViewBase
     */
	protected viewtag: string = 'f3651c83b6944418c948f91616a23eb9';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterInfoViewBase
     */ 
    protected viewName:string = "hr_employeeMasterInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_employeeMasterInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_employeeMasterInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_employeeMasterInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'hr_employee',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeMasterInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeMasterInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeMasterInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}