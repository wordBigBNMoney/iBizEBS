import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import Hr_employeeService from '@/service/hr-employee/hr-employee-service';
import Hr_employeeAuthService from '@/authservice/hr-employee/hr-employee-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import Hr_employeeUIService from '@/uiservice/hr-employee/hr-employee-ui-service';

/**
 * 员工树导航视图视图基类
 *
 * @export
 * @class Hr_employeeTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class Hr_employeeTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeTreeExpViewBase
     */
    protected appDeName: string = 'hr_employee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeTreeExpViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeTreeExpViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Hr_employeeService}
     * @memberof Hr_employeeTreeExpViewBase
     */
    protected appEntityService: Hr_employeeService = new Hr_employeeService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_employeeUIService
     * @memberof Hr_employeeTreeExpViewBase
     */
    public appUIService: Hr_employeeUIService = new Hr_employeeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_employeeTreeExpViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeeTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_employee.views.treeexpview.caption',
        srfTitle: 'entities.hr_employee.views.treeexpview.title',
        srfSubTitle: 'entities.hr_employee.views.treeexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeeTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: { name: 'treeexpbar', type: 'TREEEXPBAR' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeTreeExpViewBase
     */
	protected viewtag: string = '86a904137b85ce5f7fadfcc6b711d11f';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeTreeExpViewBase
     */ 
    protected viewName:string = "hr_employeeTreeExpView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_employeeTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_employeeTreeExpViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_employeeTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'hr_employee',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Hr_employeeTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Hr_employeeTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof Hr_employeeTreeExpView
     */
    public viewUID: string = 'odoo-hr-hr-employee-tree-exp-view';


}