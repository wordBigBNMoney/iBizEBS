import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Mail_followers_mail_message_subtype_relService from '@/service/mail-followers-mail-message-subtype-rel/mail-followers-mail-message-subtype-rel-service';
import Mail_followers_mail_message_subtype_relAuthService from '@/authservice/mail-followers-mail-message-subtype-rel/mail-followers-mail-message-subtype-rel-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Mail_followers_mail_message_subtype_relUIService from '@/uiservice/mail-followers-mail-message-subtype-rel/mail-followers-mail-message-subtype-rel-ui-service';

/**
 * 关注消息类型选择表格视图视图基类
 *
 * @export
 * @class Mail_followers_mail_message_subtype_relPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Mail_followers_mail_message_subtype_relPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    protected appDeName: string = 'mail_followers_mail_message_subtype_rel';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    protected appDeMajor: string = 'id';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Mail_followers_mail_message_subtype_relService}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    protected appEntityService: Mail_followers_mail_message_subtype_relService = new Mail_followers_mail_message_subtype_relService;

    /**
     * 实体权限服务对象
     *
     * @type Mail_followers_mail_message_subtype_relUIService
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public appUIService: Mail_followers_mail_message_subtype_relUIService = new Mail_followers_mail_message_subtype_relUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.mail_followers_mail_message_subtype_rel.views.pickupgridview.caption',
        srfTitle: 'entities.mail_followers_mail_message_subtype_rel.views.pickupgridview.title',
        srfSubTitle: 'entities.mail_followers_mail_message_subtype_rel.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
	protected viewtag: string = '7b8521e7692b779d3eab70073008d561';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */ 
    protected viewName:string = "mail_followers_mail_message_subtype_relPickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'mail_followers_mail_message_subtype_rel',
            majorPSDEField: 'id',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Mail_followers_mail_message_subtype_relPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}