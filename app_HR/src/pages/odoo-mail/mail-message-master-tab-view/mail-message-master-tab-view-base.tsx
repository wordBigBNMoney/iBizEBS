import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import Mail_messageAuthService from '@/authservice/mail-message/mail-message-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';

/**
 * 消息分页导航视图视图基类
 *
 * @export
 * @class Mail_messageMasterTabViewBase
 * @extends {TabExpViewBase}
 */
export class Mail_messageMasterTabViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMasterTabViewBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMasterTabViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMasterTabViewBase
     */
    protected appDeMajor: string = 'id';

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof Mail_messageMasterTabViewBase
     */
    protected appEntityService: Mail_messageService = new Mail_messageService;

    /**
     * 实体权限服务对象
     *
     * @type Mail_messageUIService
     * @memberof Mail_messageMasterTabViewBase
     */
    public appUIService: Mail_messageUIService = new Mail_messageUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Mail_messageMasterTabViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Mail_messageMasterTabViewBase
     */
    protected model: any = {
        srfCaption: 'entities.mail_message.views.mastertabview.caption',
        srfTitle: 'entities.mail_message.views.mastertabview.title',
        srfSubTitle: 'entities.mail_message.views.mastertabview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Mail_messageMasterTabViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMasterTabViewBase
     */
	protected viewtag: string = '50fbd127c0ef3a1fd2284548ec60fdfe';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMasterTabViewBase
     */ 
    protected viewName:string = "mail_messageMasterTabView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Mail_messageMasterTabViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Mail_messageMasterTabViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Mail_messageMasterTabViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'mail_message',
            majorPSDEField: 'id',
            isLoadDefault: true,
        });
    }


}