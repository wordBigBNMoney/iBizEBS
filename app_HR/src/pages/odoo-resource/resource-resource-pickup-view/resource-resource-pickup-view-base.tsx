import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Resource_resourceService from '@/service/resource-resource/resource-resource-service';
import Resource_resourceAuthService from '@/authservice/resource-resource/resource-resource-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Resource_resourceUIService from '@/uiservice/resource-resource/resource-resource-ui-service';

/**
 * 资源数据选择视图视图基类
 *
 * @export
 * @class Resource_resourcePickupViewBase
 * @extends {PickupViewBase}
 */
export class Resource_resourcePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupViewBase
     */
    protected appDeName: string = 'resource_resource';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Resource_resourceService}
     * @memberof Resource_resourcePickupViewBase
     */
    protected appEntityService: Resource_resourceService = new Resource_resourceService;

    /**
     * 实体权限服务对象
     *
     * @type Resource_resourceUIService
     * @memberof Resource_resourcePickupViewBase
     */
    public appUIService: Resource_resourceUIService = new Resource_resourceUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Resource_resourcePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.resource_resource.views.pickupview.caption',
        srfTitle: 'entities.resource_resource.views.pickupview.title',
        srfSubTitle: 'entities.resource_resource.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Resource_resourcePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupViewBase
     */
	protected viewtag: string = '8f480c3fa80b2efd38fd263d831a26dd';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupViewBase
     */ 
    protected viewName:string = "resource_resourcePickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Resource_resourcePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Resource_resourcePickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Resource_resourcePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'resource_resource',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}