import { Http } from '@/utils';
import { Util } from '@/utils';
import Survey_surveyServiceBase from './survey-survey-service-base';


/**
 * 问卷服务对象
 *
 * @export
 * @class Survey_surveyService
 * @extends {Survey_surveyServiceBase}
 */
export default class Survey_surveyService extends Survey_surveyServiceBase {

    /**
     * Creates an instance of  Survey_surveyService.
     * 
     * @param {*} [opts={}]
     * @memberof  Survey_surveyService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}