import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_analytic_groupServiceBase from './account-analytic-group-service-base';


/**
 * 分析类别服务对象
 *
 * @export
 * @class Account_analytic_groupService
 * @extends {Account_analytic_groupServiceBase}
 */
export default class Account_analytic_groupService extends Account_analytic_groupServiceBase {

    /**
     * Creates an instance of  Account_analytic_groupService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_groupService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}