import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_jobServiceBase from './hr-job-service-base';


/**
 * 工作岗位服务对象
 *
 * @export
 * @class Hr_jobService
 * @extends {Hr_jobServiceBase}
 */
export default class Hr_jobService extends Hr_jobServiceBase {

    /**
     * Creates an instance of  Hr_jobService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_jobService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}