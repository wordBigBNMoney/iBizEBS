import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_leave_typeServiceBase from './hr-leave-type-service-base';


/**
 * 休假类型服务对象
 *
 * @export
 * @class Hr_leave_typeService
 * @extends {Hr_leave_typeServiceBase}
 */
export default class Hr_leave_typeService extends Hr_leave_typeServiceBase {

    /**
     * Creates an instance of  Hr_leave_typeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_leave_typeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}