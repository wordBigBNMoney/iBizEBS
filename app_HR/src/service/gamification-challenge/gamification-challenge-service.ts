import { Http } from '@/utils';
import { Util } from '@/utils';
import Gamification_challengeServiceBase from './gamification-challenge-service-base';


/**
 * 游戏化挑战服务对象
 *
 * @export
 * @class Gamification_challengeService
 * @extends {Gamification_challengeServiceBase}
 */
export default class Gamification_challengeService extends Gamification_challengeServiceBase {

    /**
     * Creates an instance of  Gamification_challengeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_challengeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}