import { Http } from '@/utils';
import { Util } from '@/utils';
import Product_productServiceBase from './product-product-service-base';


/**
 * 产品服务对象
 *
 * @export
 * @class Product_productService
 * @extends {Product_productServiceBase}
 */
export default class Product_productService extends Product_productServiceBase {

    /**
     * Creates an instance of  Product_productService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_productService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}