import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_departmentServiceBase from './hr-department-service-base';


/**
 * HR 部门服务对象
 *
 * @export
 * @class Hr_departmentService
 * @extends {Hr_departmentServiceBase}
 */
export default class Hr_departmentService extends Hr_departmentServiceBase {

    /**
     * Creates an instance of  Hr_departmentService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_departmentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}