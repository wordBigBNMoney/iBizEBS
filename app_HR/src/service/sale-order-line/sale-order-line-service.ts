import { Http } from '@/utils';
import { Util } from '@/utils';
import Sale_order_lineServiceBase from './sale-order-line-service-base';


/**
 * 销售订单行服务对象
 *
 * @export
 * @class Sale_order_lineService
 * @extends {Sale_order_lineServiceBase}
 */
export default class Sale_order_lineService extends Sale_order_lineServiceBase {

    /**
     * Creates an instance of  Sale_order_lineService.
     * 
     * @param {*} [opts={}]
     * @memberof  Sale_order_lineService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}