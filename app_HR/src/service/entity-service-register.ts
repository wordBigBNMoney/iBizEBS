/**
 * 实体数据服务注册中心
 *
 * @export
 * @class EntityServiceRegister
 */
export class EntityServiceRegister {

    /**
     * 所有实体数据服务Map
     *
     * @protected
     * @type {*}
     * @memberof EntityServiceRegister
     */
    protected allEntityService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体数据服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof EntityServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of EntityServiceRegister.
     * @memberof EntityServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof EntityServiceRegister
     */
    protected init(): void {
                this.allEntityService.set('account_analytic_line', () => import('@/service/account-analytic-line/account-analytic-line-service'));
        this.allEntityService.set('hr_employee', () => import('@/service/hr-employee/hr-employee-service'));
        this.allEntityService.set('res_currency', () => import('@/service/res-currency/res-currency-service'));
        this.allEntityService.set('hr_job', () => import('@/service/hr-job/hr-job-service'));
        this.allEntityService.set('mail_followers', () => import('@/service/mail-followers/mail-followers-service'));
        this.allEntityService.set('account_move_line', () => import('@/service/account-move-line/account-move-line-service'));
        this.allEntityService.set('mail_activity', () => import('@/service/mail-activity/mail-activity-service'));
        this.allEntityService.set('hr_skill_type', () => import('@/service/hr-skill-type/hr-skill-type-service'));
        this.allEntityService.set('account_analytic_group', () => import('@/service/account-analytic-group/account-analytic-group-service'));
        this.allEntityService.set('hr_resume_line', () => import('@/service/hr-resume-line/hr-resume-line-service'));
        this.allEntityService.set('hr_skill', () => import('@/service/hr-skill/hr-skill-service'));
        this.allEntityService.set('res_company', () => import('@/service/res-company/res-company-service'));
        this.allEntityService.set('hr_recruitment_source', () => import('@/service/hr-recruitment-source/hr-recruitment-source-service'));
        this.allEntityService.set('hr_leave', () => import('@/service/hr-leave/hr-leave-service'));
        this.allEntityService.set('res_users', () => import('@/service/res-users/res-users-service'));
        this.allEntityService.set('survey_survey', () => import('@/service/survey-survey/survey-survey-service'));
        this.allEntityService.set('account_analytic_account', () => import('@/service/account-analytic-account/account-analytic-account-service'));
        this.allEntityService.set('ir_attachment', () => import('@/service/ir-attachment/ir-attachment-service'));
        this.allEntityService.set('hr_skill_level', () => import('@/service/hr-skill-level/hr-skill-level-service'));
        this.allEntityService.set('hr_department', () => import('@/service/hr-department/hr-department-service'));
        this.allEntityService.set('gamification_goal', () => import('@/service/gamification-goal/gamification-goal-service'));
        this.allEntityService.set('hr_contract', () => import('@/service/hr-contract/hr-contract-service'));
        this.allEntityService.set('hr_employee_category', () => import('@/service/hr-employee-category/hr-employee-category-service'));
        this.allEntityService.set('resource_calendar', () => import('@/service/resource-calendar/resource-calendar-service'));
        this.allEntityService.set('mail_message', () => import('@/service/mail-message/mail-message-service'));
        this.allEntityService.set('hr_contract_type', () => import('@/service/hr-contract-type/hr-contract-type-service'));
        this.allEntityService.set('res_config_settings', () => import('@/service/res-config-settings/res-config-settings-service'));
        this.allEntityService.set('product_product', () => import('@/service/product-product/product-product-service'));
        this.allEntityService.set('hr_resume_line_type', () => import('@/service/hr-resume-line-type/hr-resume-line-type-service'));
        this.allEntityService.set('gamification_challenge', () => import('@/service/gamification-challenge/gamification-challenge-service'));
        this.allEntityService.set('mail_tracking_value', () => import('@/service/mail-tracking-value/mail-tracking-value-service'));
        this.allEntityService.set('product_category', () => import('@/service/product-category/product-category-service'));
        this.allEntityService.set('res_partner', () => import('@/service/res-partner/res-partner-service'));
        this.allEntityService.set('sale_order_line', () => import('@/service/sale-order-line/sale-order-line-service'));
        this.allEntityService.set('resource_resource', () => import('@/service/resource-resource/resource-resource-service'));
        this.allEntityService.set('gamification_badge', () => import('@/service/gamification-badge/gamification-badge-service'));
        this.allEntityService.set('hr_leave_type', () => import('@/service/hr-leave-type/hr-leave-type-service'));
        this.allEntityService.set('maintenance_equipment', () => import('@/service/maintenance-equipment/maintenance-equipment-service'));
        this.allEntityService.set('account_account', () => import('@/service/account-account/account-account-service'));
    }

    /**
     * 加载实体数据服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allEntityService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体数据服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const entityServiceRegister: EntityServiceRegister = new EntityServiceRegister();