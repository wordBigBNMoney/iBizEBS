import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 保养设备服务对象基类
 *
 * @export
 * @class Maintenance_equipmentServiceBase
 * @extends {EntityServie}
 */
export default class Maintenance_equipmentServiceBase extends EntityService {

    /**
     * Creates an instance of  Maintenance_equipmentServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Maintenance_equipmentServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Maintenance_equipmentServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='maintenance_equipment';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'maintenance_equipments';
        this.APPDETEXT = 'name';
        this.APPNAME = 'hr';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/maintenance_equipments`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/maintenance_equipments`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.maintenance_equipment){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_employees/${context.hr_employee}/maintenance_equipments/${context.maintenance_equipment}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/maintenance_equipments/${context.maintenance_equipment}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.maintenance_equipment){
            let res:any = Http.getInstance().delete(`/hr_employees/${context.hr_employee}/maintenance_equipments/${context.maintenance_equipment}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/maintenance_equipments/${context.maintenance_equipment}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.maintenance_equipment){
            let res:any = await Http.getInstance().get(`/hr_employees/${context.hr_employee}/maintenance_equipments/${context.maintenance_equipment}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/maintenance_equipments/${context.maintenance_equipment}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let res:any = await Http.getInstance().get(`/hr_employees/${context.hr_employee}/maintenance_equipments/getdraft`,isloading);
            res.data.maintenance_equipment = data.maintenance_equipment;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/maintenance_equipments/getdraft`,isloading);
        res.data.maintenance_equipment = data.maintenance_equipment;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.maintenance_equipment){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/maintenance_equipments/${context.maintenance_equipment}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/maintenance_equipments/${context.maintenance_equipment}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.maintenance_equipment){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/maintenance_equipments/${context.maintenance_equipment}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/maintenance_equipments/${context.maintenance_equipment}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.maintenance_equipment){
            let res:any = Http.getInstance().delete(`/hr_employees/${context.hr_employee}/maintenance_equipments/${context.maintenance_equipment}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/maintenance_equipments/${context.maintenance_equipment}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.maintenance_equipment){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/maintenance_equipments/${context.maintenance_equipment}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/maintenance_equipments/${context.maintenance_equipment}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.maintenance_equipment){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_employees/${context.hr_employee}/maintenance_equipments/${context.maintenance_equipment}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/maintenance_equipments/${context.maintenance_equipment}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Maintenance_equipmentServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/hr_employees/${context.hr_employee}/maintenance_equipments/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/maintenance_equipments/fetchdefault`,tempData,isloading);
        return res;
    }
}