import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 技能等级服务对象基类
 *
 * @export
 * @class Hr_skill_levelServiceBase
 * @extends {EntityServie}
 */
export default class Hr_skill_levelServiceBase extends EntityService {

    /**
     * Creates an instance of  Hr_skill_levelServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skill_levelServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Hr_skill_levelServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='hr_skill_level';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'hr_skill_levels';
        this.APPDETEXT = 'name';
        this.APPNAME = 'hr';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill_level){
            let res:any = Http.getInstance().get(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels/${context.hr_skill_level}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/hr_skill_levels/${context.hr_skill_level}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/hr_skill_levels`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill_level){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels/${context.hr_skill_level}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/hr_skill_levels/${context.hr_skill_level}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill_level){
            let res:any = Http.getInstance().delete(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels/${context.hr_skill_level}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/hr_skill_levels/${context.hr_skill_level}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill_level){
            let res:any = await Http.getInstance().get(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels/${context.hr_skill_level}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/hr_skill_levels/${context.hr_skill_level}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && true){
            let res:any = await Http.getInstance().get(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels/getdraft`,isloading);
            res.data.hr_skill_level = data.hr_skill_level;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/hr_skill_levels/getdraft`,isloading);
        res.data.hr_skill_level = data.hr_skill_level;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill_level){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels/${context.hr_skill_level}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/hr_skill_levels/${context.hr_skill_level}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill_level){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels/${context.hr_skill_level}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/hr_skill_levels/${context.hr_skill_level}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skill_levelServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/hr_skill_types/${context.hr_skill_type}/hr_skill_levels/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/hr_skill_levels/fetchdefault`,tempData,isloading);
        return res;
    }
}