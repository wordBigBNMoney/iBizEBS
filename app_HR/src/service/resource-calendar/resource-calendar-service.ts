import { Http } from '@/utils';
import { Util } from '@/utils';
import Resource_calendarServiceBase from './resource-calendar-service-base';


/**
 * 资源工作时间服务对象
 *
 * @export
 * @class Resource_calendarService
 * @extends {Resource_calendarServiceBase}
 */
export default class Resource_calendarService extends Resource_calendarServiceBase {

    /**
     * Creates an instance of  Resource_calendarService.
     * 
     * @param {*} [opts={}]
     * @memberof  Resource_calendarService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}