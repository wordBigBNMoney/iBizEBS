import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 技能服务对象基类
 *
 * @export
 * @class Hr_skillServiceBase
 * @extends {EntityServie}
 */
export default class Hr_skillServiceBase extends EntityService {

    /**
     * Creates an instance of  Hr_skillServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skillServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Hr_skillServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='hr_skill';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'hr_skills';
        this.APPDETEXT = 'name';
        this.APPNAME = 'hr';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill){
            let res:any = Http.getInstance().get(`/hr_skill_types/${context.hr_skill_type}/hr_skills/${context.hr_skill}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/hr_skills/${context.hr_skill}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/hr_skill_types/${context.hr_skill_type}/hr_skills`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/hr_skills`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_skill_types/${context.hr_skill_type}/hr_skills/${context.hr_skill}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/hr_skills/${context.hr_skill}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill){
            let res:any = Http.getInstance().delete(`/hr_skill_types/${context.hr_skill_type}/hr_skills/${context.hr_skill}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/hr_skills/${context.hr_skill}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill){
            let res:any = await Http.getInstance().get(`/hr_skill_types/${context.hr_skill_type}/hr_skills/${context.hr_skill}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/hr_skills/${context.hr_skill}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && true){
            let res:any = await Http.getInstance().get(`/hr_skill_types/${context.hr_skill_type}/hr_skills/getdraft`,isloading);
            res.data.hr_skill = data.hr_skill;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/hr_skills/getdraft`,isloading);
        res.data.hr_skill = data.hr_skill;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_skill_types/${context.hr_skill_type}/hr_skills/${context.hr_skill}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/hr_skills/${context.hr_skill}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && context.hr_skill){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_skill_types/${context.hr_skill_type}/hr_skills/${context.hr_skill}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/hr_skills/${context.hr_skill}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_skillServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_skill_type && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/hr_skill_types/${context.hr_skill_type}/hr_skills/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/hr_skills/fetchdefault`,tempData,isloading);
        return res;
    }
}