import Hr_resume_line_typeUIServiceBase from './hr-resume-line-type-ui-service-base';

/**
 * 简历类型UI服务对象
 *
 * @export
 * @class Hr_resume_line_typeUIService
 */
export default class Hr_resume_line_typeUIService extends Hr_resume_line_typeUIServiceBase {

    /**
     * Creates an instance of  Hr_resume_line_typeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_resume_line_typeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}