import Res_usersUIServiceBase from './res-users-ui-service-base';

/**
 * 用户UI服务对象
 *
 * @export
 * @class Res_usersUIService
 */
export default class Res_usersUIService extends Res_usersUIServiceBase {

    /**
     * Creates an instance of  Res_usersUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_usersUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}