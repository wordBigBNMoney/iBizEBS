import Account_accountUIServiceBase from './account-account-ui-service-base';

/**
 * 科目UI服务对象
 *
 * @export
 * @class Account_accountUIService
 */
export default class Account_accountUIService extends Account_accountUIServiceBase {

    /**
     * Creates an instance of  Account_accountUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_accountUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}