import Hr_contract_typeUIServiceBase from './hr-contract-type-ui-service-base';

/**
 * 合同类型UI服务对象
 *
 * @export
 * @class Hr_contract_typeUIService
 */
export default class Hr_contract_typeUIService extends Hr_contract_typeUIServiceBase {

    /**
     * Creates an instance of  Hr_contract_typeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_contract_typeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}