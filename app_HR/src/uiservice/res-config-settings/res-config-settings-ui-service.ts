import Res_config_settingsUIServiceBase from './res-config-settings-ui-service-base';

/**
 * 配置设定UI服务对象
 *
 * @export
 * @class Res_config_settingsUIService
 */
export default class Res_config_settingsUIService extends Res_config_settingsUIServiceBase {

    /**
     * Creates an instance of  Res_config_settingsUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_config_settingsUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}