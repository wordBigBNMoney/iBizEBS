import Res_partnerUIServiceBase from './res-partner-ui-service-base';

/**
 * 联系人UI服务对象
 *
 * @export
 * @class Res_partnerUIService
 */
export default class Res_partnerUIService extends Res_partnerUIServiceBase {

    /**
     * Creates an instance of  Res_partnerUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partnerUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}