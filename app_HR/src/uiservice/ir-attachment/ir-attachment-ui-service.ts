import Ir_attachmentUIServiceBase from './ir-attachment-ui-service-base';

/**
 * 附件UI服务对象
 *
 * @export
 * @class Ir_attachmentUIService
 */
export default class Ir_attachmentUIService extends Ir_attachmentUIServiceBase {

    /**
     * Creates an instance of  Ir_attachmentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Ir_attachmentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}