import Account_analytic_accountUIServiceBase from './account-analytic-account-ui-service-base';

/**
 * 分析账户UI服务对象
 *
 * @export
 * @class Account_analytic_accountUIService
 */
export default class Account_analytic_accountUIService extends Account_analytic_accountUIServiceBase {

    /**
     * Creates an instance of  Account_analytic_accountUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_accountUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}