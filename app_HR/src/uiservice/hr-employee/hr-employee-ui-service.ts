import Hr_employeeUIServiceBase from './hr-employee-ui-service-base';

/**
 * 员工UI服务对象
 *
 * @export
 * @class Hr_employeeUIService
 */
export default class Hr_employeeUIService extends Hr_employeeUIServiceBase {

    /**
     * Creates an instance of  Hr_employeeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_employeeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}