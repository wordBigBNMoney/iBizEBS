import Hr_leaveUIServiceBase from './hr-leave-ui-service-base';

/**
 * 休假UI服务对象
 *
 * @export
 * @class Hr_leaveUIService
 */
export default class Hr_leaveUIService extends Hr_leaveUIServiceBase {

    /**
     * Creates an instance of  Hr_leaveUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_leaveUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}