import Account_analytic_lineUIServiceBase from './account-analytic-line-ui-service-base';

/**
 * 分析行UI服务对象
 *
 * @export
 * @class Account_analytic_lineUIService
 */
export default class Account_analytic_lineUIService extends Account_analytic_lineUIServiceBase {

    /**
     * Creates an instance of  Account_analytic_lineUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_lineUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}