import Hr_leave_typeUIServiceBase from './hr-leave-type-ui-service-base';

/**
 * 休假类型UI服务对象
 *
 * @export
 * @class Hr_leave_typeUIService
 */
export default class Hr_leave_typeUIService extends Hr_leave_typeUIServiceBase {

    /**
     * Creates an instance of  Hr_leave_typeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_leave_typeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}