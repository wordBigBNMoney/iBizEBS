import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Hr_employeeService from '@/service/hr-employee/hr-employee-service';
import IF_MasterService from './if-master-form-service';
import Hr_employeeUIService from '@/uiservice/hr-employee/hr-employee-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {IF_MasterEditFormBase}
 */
export class IF_MasterEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof IF_MasterEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {IF_MasterService}
     * @memberof IF_MasterEditFormBase
     */
    public service: IF_MasterService = new IF_MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_employeeService}
     * @memberof IF_MasterEditFormBase
     */
    public appEntityService: Hr_employeeService = new Hr_employeeService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof IF_MasterEditFormBase
     */
    protected appDeName: string = 'hr_employee';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof IF_MasterEditFormBase
     */
    protected appDeLogicName: string = '员工';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_employeeUIService}
     * @memberof IF_MasterBase
     */  
    public appUIService:Hr_employeeUIService = new Hr_employeeUIService(this.$store);


    /**
     * 关系界面数量
     *
     * @protected
     * @type {number}
     * @memberof IF_MasterEditFormBase
     */
    protected drCount: number = 1;
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof IF_MasterEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        job_title: null,
        mobile_phone: null,
        work_phone: null,
        work_email: null,
        work_location: null,
        company_id_text: null,
        department_id_text: null,
        job_id_text: null,
        parent_id_text: null,
        job_id: null,
        company_id: null,
        department_id: null,
        parent_id: null,
        address_id: null,
        address_id_text: null,
        coach_id: null,
        expense_manager_id: null,
        expense_manager_id_text: null,
        coach_id_text: null,
        address_home_id: null,
        address_home_id_text: null,
        emergency_contact: null,
        emergency_phone: null,
        marital: null,
        spouse_complete_name: null,
        spouse_birthdate: null,
        children: null,
        country_id: null,
        country_id_text: null,
        identification_id: null,
        passport_id: null,
        gender: null,
        birthday: null,
        place_of_birth: null,
        country_of_birth: null,
        country_of_birth_text: null,
        certificate: null,
        study_field: null,
        study_school: null,
        permit_no: null,
        visa_no: null,
        visa_expire: null,
        pin: null,
        barcode: null,
        id: null,
        hr_employee:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof IF_MasterEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof IF_MasterEditFormBase
     */
    public rules():any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof IF_MasterBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof IF_MasterEditFormBase
     */
    public detailsModel: any = {
        grouppanel10: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel10', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        druipart1: new FormDRUIPartModel({ caption: '', detailType: 'DRUIPART', name: 'druipart1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel12: new FormGroupPanelModel({ caption: '简历', detailType: 'GROUPPANEL', name: 'grouppanel12', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel16: new FormGroupPanelModel({ caption: '技能', detailType: 'GROUPPANEL', name: 'grouppanel16', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel8: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel8', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '位置', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '负责人', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel13: new FormGroupPanelModel({ caption: '工作信息', detailType: 'GROUPPANEL', name: 'grouppanel13', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel3: new FormGroupPanelModel({ caption: '私人联系', detailType: 'GROUPPANEL', name: 'grouppanel3', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel5: new FormGroupPanelModel({ caption: '紧急', detailType: 'GROUPPANEL', name: 'grouppanel5', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel4: new FormGroupPanelModel({ caption: '婚姻状况', detailType: 'GROUPPANEL', name: 'grouppanel4', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel7: new FormGroupPanelModel({ caption: '国籍', detailType: 'GROUPPANEL', name: 'grouppanel7', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel6: new FormGroupPanelModel({ caption: '教育', detailType: 'GROUPPANEL', name: 'grouppanel6', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel9: new FormGroupPanelModel({ caption: '工作签证', detailType: 'GROUPPANEL', name: 'grouppanel9', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel14: new FormGroupPanelModel({ caption: '个人隐私信息', detailType: 'GROUPPANEL', name: 'grouppanel14', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel11: new FormGroupPanelModel({ caption: '出勤', detailType: 'GROUPPANEL', name: 'grouppanel11', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel15: new FormGroupPanelModel({ caption: 'HR设置', detailType: 'GROUPPANEL', name: 'grouppanel15', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_employee.if_master_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        job_title: new FormItemModel({ caption: '工作头衔', detailType: 'FORMITEM', name: 'job_title', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        mobile_phone: new FormItemModel({ caption: '办公手机', detailType: 'FORMITEM', name: 'mobile_phone', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        work_phone: new FormItemModel({ caption: '办公电话', detailType: 'FORMITEM', name: 'work_phone', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        work_email: new FormItemModel({ caption: '工作EMail', detailType: 'FORMITEM', name: 'work_email', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        work_location: new FormItemModel({ caption: '工作地点', detailType: 'FORMITEM', name: 'work_location', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id_text: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        department_id_text: new FormItemModel({ caption: '部门', detailType: 'FORMITEM', name: 'department_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        job_id_text: new FormItemModel({ caption: '工作岗位', detailType: 'FORMITEM', name: 'job_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        parent_id_text: new FormItemModel({ caption: '经理', detailType: 'FORMITEM', name: 'parent_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        job_id: new FormItemModel({ caption: '工作岗位', detailType: 'FORMITEM', name: 'job_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        department_id: new FormItemModel({ caption: '部门', detailType: 'FORMITEM', name: 'department_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        parent_id: new FormItemModel({ caption: '经理', detailType: 'FORMITEM', name: 'parent_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        address_id: new FormItemModel({ caption: '工作地址', detailType: 'FORMITEM', name: 'address_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        address_id_text: new FormItemModel({ caption: '工作地址', detailType: 'FORMITEM', name: 'address_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        coach_id: new FormItemModel({ caption: '教练', detailType: 'FORMITEM', name: 'coach_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        expense_manager_id: new FormItemModel({ caption: '费用', detailType: 'FORMITEM', name: 'expense_manager_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        expense_manager_id_text: new FormItemModel({ caption: '费用', detailType: 'FORMITEM', name: 'expense_manager_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        coach_id_text: new FormItemModel({ caption: '教练', detailType: 'FORMITEM', name: 'coach_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        address_home_id: new FormItemModel({ caption: '家庭住址', detailType: 'FORMITEM', name: 'address_home_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        address_home_id_text: new FormItemModel({ caption: '家庭住址', detailType: 'FORMITEM', name: 'address_home_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        emergency_contact: new FormItemModel({ caption: '紧急联系人', detailType: 'FORMITEM', name: 'emergency_contact', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        emergency_phone: new FormItemModel({ caption: '紧急电话', detailType: 'FORMITEM', name: 'emergency_phone', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        marital: new FormItemModel({ caption: '婚姻状况', detailType: 'FORMITEM', name: 'marital', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        spouse_complete_name: new FormItemModel({ caption: '配偶全名', detailType: 'FORMITEM', name: 'spouse_complete_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        spouse_birthdate: new FormItemModel({ caption: '配偶生日', detailType: 'FORMITEM', name: 'spouse_birthdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        children: new FormItemModel({ caption: '子女数目', detailType: 'FORMITEM', name: 'children', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        country_id: new FormItemModel({ caption: '国籍(国家)', detailType: 'FORMITEM', name: 'country_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        country_id_text: new FormItemModel({ caption: '国籍(国家)', detailType: 'FORMITEM', name: 'country_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        identification_id: new FormItemModel({ caption: '身份证号', detailType: 'FORMITEM', name: 'identification_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        passport_id: new FormItemModel({ caption: '护照号', detailType: 'FORMITEM', name: 'passport_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        gender: new FormItemModel({ caption: '性别', detailType: 'FORMITEM', name: 'gender', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        birthday: new FormItemModel({ caption: '出生日期', detailType: 'FORMITEM', name: 'birthday', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        place_of_birth: new FormItemModel({ caption: '出生地', detailType: 'FORMITEM', name: 'place_of_birth', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        country_of_birth: new FormItemModel({ caption: '国籍', detailType: 'FORMITEM', name: 'country_of_birth', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        country_of_birth_text: new FormItemModel({ caption: '国籍', detailType: 'FORMITEM', name: 'country_of_birth_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        certificate: new FormItemModel({ caption: '证书等级', detailType: 'FORMITEM', name: 'certificate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        study_field: new FormItemModel({ caption: '研究领域', detailType: 'FORMITEM', name: 'study_field', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        study_school: new FormItemModel({ caption: '毕业院校', detailType: 'FORMITEM', name: 'study_school', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        permit_no: new FormItemModel({ caption: '工作许可编号', detailType: 'FORMITEM', name: 'permit_no', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        visa_no: new FormItemModel({ caption: '签证号', detailType: 'FORMITEM', name: 'visa_no', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        visa_expire: new FormItemModel({ caption: '签证到期日期', detailType: 'FORMITEM', name: 'visa_expire', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        pin: new FormItemModel({ caption: 'PIN', detailType: 'FORMITEM', name: 'pin', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        barcode: new FormItemModel({ caption: '工牌 ID', detailType: 'FORMITEM', name: 'barcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

    };
}