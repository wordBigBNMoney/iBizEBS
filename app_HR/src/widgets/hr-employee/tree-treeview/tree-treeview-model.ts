/**
 * Tree 部件模型
 *
 * @export
 * @class TreeModel
 */
export default class TreeModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TreeModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'mobile_phone',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'leave_date_from',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'children',
      },
      {
        name: 'image_small',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'pin',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'study_school',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'marital',
      },
      {
        name: 'direct_badge_ids',
      },
      {
        name: 'image_medium',
      },
      {
        name: '__last_update',
      },
      {
        name: 'emergency_phone',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'km_home_work',
      },
      {
        name: 'visa_no',
      },
      {
        name: 'place_of_birth',
      },
      {
        name: 'spouse_complete_name',
      },
      {
        name: 'sinid',
      },
      {
        name: 'badge_ids',
      },
      {
        name: 'work_email',
      },
      {
        name: 'has_badges',
      },
      {
        name: 'current_leave_id',
      },
      {
        name: 'contract_ids',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'barcode',
      },
      {
        name: 'birthday',
      },
      {
        name: 'certificate',
      },
      {
        name: 'activity_user_id',
      },
      {
        name: 'study_field',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'attendance_state',
      },
      {
        name: 'manually_set_present',
      },
      {
        name: 'contracts_count',
      },
      {
        name: 'gender',
      },
      {
        name: 'image',
      },
      {
        name: 'spouse_birthdate',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'leaves_count',
      },
      {
        name: 'notes',
      },
      {
        name: 'additional_note',
      },
      {
        name: 'write_date',
      },
      {
        name: 'medic_exam',
      },
      {
        name: 'visa_expire',
      },
      {
        name: 'work_phone',
      },
      {
        name: 'emergency_contact',
      },
      {
        name: 'newly_hired_employee',
      },
      {
        name: 'display_name',
      },
      {
        name: 'is_address_home_a_company',
      },
      {
        name: 'attendance_ids',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'current_leave_state',
      },
      {
        name: 'child_ids',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'color',
      },
      {
        name: 'manual_attendance',
      },
      {
        name: 'remaining_leaves',
      },
      {
        name: 'ssnid',
      },
      {
        name: 'job_title',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'is_absent_totay',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'passport_id',
      },
      {
        name: 'goal_ids',
      },
      {
        name: 'leave_date_to',
      },
      {
        name: 'work_location',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'contract_id',
      },
      {
        name: 'permit_no',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'hr_employee',
        prop: 'id',
      },
      {
        name: 'identification_id',
      },
      {
        name: 'vehicle',
      },
      {
        name: 'category_ids',
      },
      {
        name: 'show_leaves',
      },
      {
        name: 'create_date',
      },
      {
        name: 'parent_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'name',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'address_home_id_text',
      },
      {
        name: 'tz',
      },
      {
        name: 'resource_calendar_id_text',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'department_id_text',
      },
      {
        name: 'country_of_birth_text',
      },
      {
        name: 'expense_manager_id_text',
      },
      {
        name: 'job_id_text',
      },
      {
        name: 'address_id_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'active',
      },
      {
        name: 'coach_id_text',
      },
      {
        name: 'country_id_text',
      },
      {
        name: 'address_home_id',
      },
      {
        name: 'expense_manager_id',
      },
      {
        name: 'bank_account_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'country_id',
      },
      {
        name: 'company_id',
      },
      {
        name: 'job_id',
      },
      {
        name: 'resource_id',
      },
      {
        name: 'user_id',
      },
      {
        name: 'department_id',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'last_attendance_id',
      },
      {
        name: 'coach_id',
      },
      {
        name: 'address_id',
      },
      {
        name: 'country_of_birth',
      },
      {
        name: 'resource_calendar_id',
      },
      {
        name: 'leave_manager_id',
      },
    ]
  }


}