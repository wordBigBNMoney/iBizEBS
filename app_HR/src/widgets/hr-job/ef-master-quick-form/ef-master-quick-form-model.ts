/**
 * EF_MasterQuick 部件模型
 *
 * @export
 * @class EF_MasterQuickModel
 */
export default class EF_MasterQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'department_id_text',
        prop: 'department_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'address_id_text',
        prop: 'address_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'no_of_recruitment',
        prop: 'no_of_recruitment',
        dataType: 'INT',
      },
      {
        name: 'survey_title',
        prop: 'survey_title',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'user_id_text',
        prop: 'user_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'user_id',
        prop: 'user_id',
        dataType: 'PICKUP',
      },
      {
        name: 'department_id',
        prop: 'department_id',
        dataType: 'PICKUP',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'address_id',
        prop: 'address_id',
        dataType: 'PICKUP',
      },
      {
        name: 'survey_id',
        prop: 'survey_id',
        dataType: 'PICKUP',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'LONGTEXT',
      },
      {
        name: 'state',
        prop: 'state',
        dataType: 'SSCODELIST',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'hr_job',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}