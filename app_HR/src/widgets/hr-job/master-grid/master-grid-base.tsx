import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import Hr_jobService from '@/service/hr-job/hr-job-service';
import MasterService from './master-grid-service';
import Hr_jobUIService from '@/uiservice/hr-job/hr-job-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MasterGridBase}
 */
export class MasterGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MasterService}
     * @memberof MasterGridBase
     */
    public service: MasterService = new MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_jobService}
     * @memberof MasterGridBase
     */
    public appEntityService: Hr_jobService = new Hr_jobService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterGridBase
     */
    protected appDeName: string = 'hr_job';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterGridBase
     */
    protected appDeLogicName: string = '工作岗位';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_jobUIService}
     * @memberof MasterBase
     */  
    public appUIService:Hr_jobUIService = new Hr_jobUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MasterBase
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof MasterBase
     */  
    public majorInfoColName:string = "name";


    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MasterBase
     */
    protected localStorageTag: string = 'hr_job_master_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MasterGridBase
     */
    public allColumns: any[] = [
        {
            name: 'name',
            label: '工作岗位',
            langtag: 'entities.hr_job.master_grid.columns.name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'company_id_text',
            label: '公司',
            langtag: 'entities.hr_job.master_grid.columns.company_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'department_id',
            label: '部门',
            langtag: 'entities.hr_job.master_grid.columns.department_id',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'department_id_text',
            label: '部门',
            langtag: 'entities.hr_job.master_grid.columns.department_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'no_of_employee',
            label: '当前员工数量',
            langtag: 'entities.hr_job.master_grid.columns.no_of_employee',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'no_of_recruitment',
            label: '期望的新员工',
            langtag: 'entities.hr_job.master_grid.columns.no_of_recruitment',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'expected_employees',
            label: '预计员工数合计',
            langtag: 'entities.hr_job.master_grid.columns.expected_employees',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'no_of_hired_employee',
            label: '已雇用员工',
            langtag: 'entities.hr_job.master_grid.columns.no_of_hired_employee',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'user_id_text',
            label: '招聘负责人',
            langtag: 'entities.hr_job.master_grid.columns.user_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'state',
            label: '状态',
            langtag: 'entities.hr_job.master_grid.columns.state',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'company_id',
            label: '公司',
            langtag: 'entities.hr_job.master_grid.columns.company_id',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'user_id',
            label: '招聘负责人',
            langtag: 'entities.hr_job.master_grid.columns.user_id',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MasterGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MasterGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MasterBase
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MasterBase
     */
    public hasRowEdit: any = {
        'name':false,
        'company_id_text':false,
        'department_id':false,
        'department_id_text':false,
        'no_of_employee':false,
        'no_of_recruitment':false,
        'expected_employees':false,
        'no_of_hired_employee':false,
        'user_id_text':false,
        'state':false,
        'company_id':false,
        'user_id':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MasterBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MasterGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'state',
                srfkey: 'HR_JOB__STATE',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof MasterBase
     */
    public updateDefault(row: any){                    
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof MasterBase
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.id === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}