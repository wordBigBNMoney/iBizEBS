/**
 * Master 部件模型
 *
 * @export
 * @class MasterModel
 */
export default class MasterModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MasterGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MasterGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'company_id_text',
          prop: 'company_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'department_id',
          prop: 'department_id',
          dataType: 'PICKUP',
        },
        {
          name: 'department_id_text',
          prop: 'department_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'no_of_employee',
          prop: 'no_of_employee',
          dataType: 'INT',
        },
        {
          name: 'no_of_recruitment',
          prop: 'no_of_recruitment',
          dataType: 'INT',
        },
        {
          name: 'expected_employees',
          prop: 'expected_employees',
          dataType: 'INT',
        },
        {
          name: 'no_of_hired_employee',
          prop: 'no_of_hired_employee',
          dataType: 'INT',
        },
        {
          name: 'user_id_text',
          prop: 'user_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'state',
          prop: 'state',
          dataType: 'SSCODELIST',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'manager_id',
          prop: 'manager_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'alias_id',
          prop: 'alias_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'address_id',
          prop: 'address_id',
          dataType: 'PICKUP',
        },
        {
          name: 'survey_id',
          prop: 'survey_id',
          dataType: 'PICKUP',
        },
        {
          name: 'hr_responsible_id',
          prop: 'hr_responsible_id',
          dataType: 'PICKUP',
        },
        {
          name: 'hr_job',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}