/**
 * MasterTabInfoViewtabviewpanel21 部件模型
 *
 * @export
 * @class MasterTabInfoViewtabviewpanel21Model
 */
export default class MasterTabInfoViewtabviewpanel21Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterTabInfoViewtabviewpanel21Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'message_is_follower',
      },
      {
        name: 'no_of_employee',
      },
      {
        name: 'state',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: '__last_update',
      },
      {
        name: 'no_of_hired_employee',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'website_meta_description',
      },
      {
        name: 'no_of_recruitment',
      },
      {
        name: 'write_date',
      },
      {
        name: 'is_seo_optimized',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'name',
      },
      {
        name: 'display_name',
      },
      {
        name: 'description',
      },
      {
        name: 'website_url',
      },
      {
        name: 'requirements',
      },
      {
        name: 'website_meta_title',
      },
      {
        name: 'application_count',
      },
      {
        name: 'application_ids',
      },
      {
        name: 'is_published',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'website_meta_og_img',
      },
      {
        name: 'expected_employees',
      },
      {
        name: 'create_date',
      },
      {
        name: 'website_published',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'website_description',
      },
      {
        name: 'hr_job',
        prop: 'id',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'color',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'document_ids',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'employee_ids',
      },
      {
        name: 'documents_count',
      },
      {
        name: 'website_id',
      },
      {
        name: 'website_meta_keywords',
      },
      {
        name: 'alias_defaults',
      },
      {
        name: 'alias_name',
      },
      {
        name: 'alias_force_thread_id',
      },
      {
        name: 'alias_domain',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'alias_parent_model_id',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'survey_title',
      },
      {
        name: 'alias_parent_thread_id',
      },
      {
        name: 'alias_model_id',
      },
      {
        name: 'address_id_text',
      },
      {
        name: 'alias_user_id',
      },
      {
        name: 'department_id_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'manager_id_text',
      },
      {
        name: 'alias_contact',
      },
      {
        name: 'hr_responsible_id_text',
      },
      {
        name: 'company_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'manager_id',
      },
      {
        name: 'address_id',
      },
      {
        name: 'survey_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'hr_responsible_id',
      },
      {
        name: 'department_id',
      },
      {
        name: 'alias_id',
      },
      {
        name: 'user_id',
      },
    ]
  }


}