import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import Hr_skill_typeService from '@/service/hr-skill-type/hr-skill-type-service';
import BasicService from './basic-list-service';
import Hr_skill_typeUIService from '@/uiservice/hr-skill-type/hr-skill-type-ui-service';


/**
 * listexpbar_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {BasicListBase}
 */
export class BasicListBase extends ListControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {BasicService}
     * @memberof BasicListBase
     */
    public service: BasicService = new BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_skill_typeService}
     * @memberof BasicListBase
     */
    public appEntityService: Hr_skill_typeService = new Hr_skill_typeService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeName: string = 'hr_skill_type';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeLogicName: string = '技能类型';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_skill_typeUIService}
     * @memberof BasicBase
     */  
    public appUIService:Hr_skill_typeUIService = new Hr_skill_typeUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof BasicListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof BasicListBase
     */
    public minorSortDir: string = '';


}