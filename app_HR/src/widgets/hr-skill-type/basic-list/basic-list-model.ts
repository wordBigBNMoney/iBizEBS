/**
 * Basic 部件模型
 *
 * @export
 * @class BasicModel
 */
export default class BasicModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof BasicListexpbar_listMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'id',
			},
			{
				name: 'name',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'TEXT',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'hr_skill_type',
				prop: 'id',
				dataType: 'FONTKEY',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}