import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Hr_skill_typeService from '@/service/hr-skill-type/hr-skill-type-service';
import BasicListExpViewlistexpbarModel from './basic-list-exp-viewlistexpbar-listexpbar-model';


/**
 * BasicListExpViewlistexpbar 部件服务对象
 *
 * @export
 * @class BasicListExpViewlistexpbarService
 */
export default class BasicListExpViewlistexpbarService extends ControlService {

    /**
     * 技能类型服务对象
     *
     * @type {Hr_skill_typeService}
     * @memberof BasicListExpViewlistexpbarService
     */
    public appEntityService: Hr_skill_typeService = new Hr_skill_typeService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof BasicListExpViewlistexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of BasicListExpViewlistexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof BasicListExpViewlistexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new BasicListExpViewlistexpbarModel();
    }

}