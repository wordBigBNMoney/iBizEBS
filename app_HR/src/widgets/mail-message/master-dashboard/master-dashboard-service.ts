import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Master 部件服务对象
 *
 * @export
 * @class MasterService
 */
export default class MasterService extends ControlService {
}