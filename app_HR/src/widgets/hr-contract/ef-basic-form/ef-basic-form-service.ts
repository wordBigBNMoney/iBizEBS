import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Hr_contractService from '@/service/hr-contract/hr-contract-service';
import EF_BasicModel from './ef-basic-form-model';
import Res_companyService from '@/service/res-company/res-company-service';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import Hr_employeeService from '@/service/hr-employee/hr-employee-service';
import Hr_jobService from '@/service/hr-job/hr-job-service';
import Resource_calendarService from '@/service/resource-calendar/resource-calendar-service';
import Res_usersService from '@/service/res-users/res-users-service';


/**
 * EF_Basic 部件服务对象
 *
 * @export
 * @class EF_BasicService
 */
export default class EF_BasicService extends ControlService {

    /**
     * 合同服务对象
     *
     * @type {Hr_contractService}
     * @memberof EF_BasicService
     */
    public appEntityService: Hr_contractService = new Hr_contractService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof EF_BasicService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of EF_BasicService.
     * 
     * @param {*} [opts={}]
     * @memberof EF_BasicService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new EF_BasicModel();
    }

    /**
     * 公司服务对象
     *
     * @type {Res_companyService}
     * @memberof EF_BasicService
     */
    public res_companyService: Res_companyService = new Res_companyService();

    /**
     * HR 部门服务对象
     *
     * @type {Hr_departmentService}
     * @memberof EF_BasicService
     */
    public hr_departmentService: Hr_departmentService = new Hr_departmentService();

    /**
     * 员工服务对象
     *
     * @type {Hr_employeeService}
     * @memberof EF_BasicService
     */
    public hr_employeeService: Hr_employeeService = new Hr_employeeService();

    /**
     * 工作岗位服务对象
     *
     * @type {Hr_jobService}
     * @memberof EF_BasicService
     */
    public hr_jobService: Hr_jobService = new Hr_jobService();

    /**
     * 资源工作时间服务对象
     *
     * @type {Resource_calendarService}
     * @memberof EF_BasicService
     */
    public resource_calendarService: Resource_calendarService = new Resource_calendarService();

    /**
     * 用户服务对象
     *
     * @type {Res_usersService}
     * @memberof EF_BasicService
     */
    public res_usersService: Res_usersService = new Res_usersService();

    /**
     * 远端数据
     *
     * @type {*}
     * @memberof EF_BasicService
     */
    private remoteCopyData:any = {};

    /**
     * 处理数据
     *
     * @private
     * @param {Promise<any>} promise
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    private doItems(promise: Promise<any>, deKeyField: string, deName: string): Promise<any> {
        return new Promise((resolve, reject) => {
            promise.then((response: any) => {
                if (response && response.status === 200) {
                    const data = response.data;
                    data.forEach((item:any,index:number) =>{
                        item[deName] = item[deKeyField];
                        data[index] = item;
                    });
                    resolve(data);
                } else {
                    reject([])
                }
            }).catch((response: any) => {
                reject([])
            });
        });
    }

    /**
     * 获取跨实体数据集合
     *
     * @param {string} serviceName 服务名称
     * @param {string} interfaceName 接口名称
     * @param {*} data
     * @param {boolean} [isloading]
     * @returns {Promise<any[]>}
     * @memberof  EF_BasicService
     */
    @Errorlog
    public getItems(serviceName: string, interfaceName: string, context: any = {}, data: any, isloading?: boolean): Promise<any[]> {
        data.page = data.page ? data.page : 0;
        data.size = data.size ? data.size : 1000;
        if (Object.is(serviceName, 'Res_companyService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.res_companyService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'res_company');
        }
        if (Object.is(serviceName, 'Hr_departmentService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.hr_departmentService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'hr_department');
        }
        if (Object.is(serviceName, 'Hr_employeeService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.hr_employeeService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'hr_employee');
        }
        if (Object.is(serviceName, 'Hr_jobService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.hr_jobService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'hr_job');
        }
        if (Object.is(serviceName, 'Resource_calendarService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.resource_calendarService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'resource_calendar');
        }
        if (Object.is(serviceName, 'Res_usersService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.res_usersService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'res_users');
        }

        return Promise.reject([])
    }

    /**
     * 启动工作流
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    @Errorlog
    public wfstart(action: string,context: any = {},data: any = {}, isloading?: boolean,localdata?:any): Promise<any> {
        data = this.handleWFData(data);
        context = this.handleRequestData(action,context,data).context;
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](context,data, isloading,localdata);
            } else {
                result = this.appEntityService.WFStart(context,data, isloading,localdata);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 提交工作流
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    @Errorlog
    public wfsubmit(action: string,context: any = {}, data: any = {}, isloading?: boolean,localdata?:any): Promise<any> {
        data = this.handleWFData(data,true);
        context = this.handleRequestData(action,context,data,true).context;
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](context,data, isloading,localdata);
            } else {
                result = this.appEntityService.WFSubmit(context,data, isloading,localdata);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 添加数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    @Errorlog
    public add(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Create(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    @Errorlog
    public delete(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Remove(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    @Errorlog
    public update(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Update(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    @Errorlog
    public get(action: string,context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Get(Context,Data, isloading);
            }
            result.then((response) => {
                this.setRemoteCopyData(response);
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 加载草稿
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    @Errorlog
    public loadDraft(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        //仿真主键数据
        const PrimaryKey = Util.createUUID();
        Data.id = PrimaryKey;
        Data.hr_contract = PrimaryKey;
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.GetDraft(Context,Data, isloading);
            }
            result.then((response) => {
                this.setRemoteCopyData(response);
                response.data.id = PrimaryKey;
                this.handleResponse(action, response, true);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

     /**
     * 前台逻辑
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EF_BasicService
     */
    @Errorlog
    public frontLogic(action:string,context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data);
        return new Promise((resolve: any, reject: any)=>{
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                return Promise.reject({ status: 500, data: { title: '失败', message: '系统异常' } });
            }
            result.then((response) => {
                this.handleResponse(action, response,true);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        })
    }

    /**
     * 处理请求数据
     * 
     * @param action 行为 
     * @param data 数据
     * @memberof EF_BasicService
     */
    public handleRequestData(action: string,context:any, data: any = {},isMerge:boolean = false){
        let mode: any = this.getMode();
        if (!mode && mode.getDataItems instanceof Function) {
            return data;
        }
        let formItemItems: any[] = mode.getDataItems();
        let requestData:any = {};
        if(isMerge && (data && data.viewparams)){
            Object.assign(requestData,data.viewparams);
        }
        formItemItems.forEach((item:any) =>{
            if(item && item.dataType && Object.is(item.dataType,'FONTKEY')){
                if(item && item.prop){
                    requestData[item.prop] = context[item.name];
                }
            }else{
                if(item && item.prop){
                    requestData[item.prop] = data[item.name];
                }
            }
        });
        let tempContext:any = JSON.parse(JSON.stringify(context));
        if(tempContext && tempContext.srfsessionid){
            tempContext.srfsessionkey = tempContext.srfsessionid;
            delete tempContext.srfsessionid;
        }
        return {context:tempContext,data:requestData};
    }

    /**
     * 通过属性名称获取表单项名称
     * 
     * @param name 实体属性名称 
     * @memberof EF_BasicService
     */
    public getItemNameByDeName(name:string) :string{
        let itemName = name;
        let mode: any = this.getMode();
        if (!mode && mode.getDataItems instanceof Function) {
            return name;
        }
        let formItemItems: any[] = mode.getDataItems();
        formItemItems.forEach((item:any)=>{
            if(item.prop === name){
                itemName = item.name;
            }
        });
        return itemName.trim();
    }

    /**
     * 设置远端数据
     * 
     * @param result 远端请求结果 
     * @memberof EF_BasicService
     */
    public setRemoteCopyData(result:any){
        if (result && result.status === 200) {
            this.remoteCopyData = Util.deepCopy(result.data);
        }
    }

    /**
     * 获取远端数据
     * 
     * @memberof EF_BasicService
     */
    public getRemoteCopyData(){
        return this.remoteCopyData;
    }

}