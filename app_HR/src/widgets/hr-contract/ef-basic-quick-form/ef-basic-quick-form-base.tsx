import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Hr_contractService from '@/service/hr-contract/hr-contract-service';
import EF_BasicQuickService from './ef-basic-quick-form-service';
import Hr_contractUIService from '@/uiservice/hr-contract/hr-contract-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_BasicQuickEditFormBase}
 */
export class EF_BasicQuickEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicQuickEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_BasicQuickService}
     * @memberof EF_BasicQuickEditFormBase
     */
    public service: EF_BasicQuickService = new EF_BasicQuickService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_contractService}
     * @memberof EF_BasicQuickEditFormBase
     */
    public appEntityService: Hr_contractService = new Hr_contractService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicQuickEditFormBase
     */
    protected appDeName: string = 'hr_contract';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicQuickEditFormBase
     */
    protected appDeLogicName: string = '合同';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_contractUIService}
     * @memberof EF_BasicQuickBase
     */  
    public appUIService:Hr_contractUIService = new Hr_contractUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_BasicQuickEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        company_id_text: null,
        department_id_text: null,
        employee_id_text: null,
        job_id_text: null,
        company_id: null,
        employee_id: null,
        department_id: null,
        job_id: null,
        date_start: null,
        wage: null,
        date_end: null,
        resource_calendar_id_text: null,
        trial_date_end: null,
        state: null,
        resource_calendar_id: null,
        id: null,
        hr_contract:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_BasicQuickEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicQuickEditFormBase
     */
    public rules():any{
        return {
        name: [
            { required: this.detailsModel.name.required, type: 'string', message: '合同名 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name.required, type: 'string', message: '合同名 值不能为空', trigger: 'blur' },
        ],
        company_id_text: [
            { required: this.detailsModel.company_id_text.required, type: 'string', message: '公司 值不能为空', trigger: 'change' },
            { required: this.detailsModel.company_id_text.required, type: 'string', message: '公司 值不能为空', trigger: 'blur' },
        ],
        date_start: [
            { required: this.detailsModel.date_start.required, type: 'string', message: '开始日期 值不能为空', trigger: 'change' },
            { required: this.detailsModel.date_start.required, type: 'string', message: '开始日期 值不能为空', trigger: 'blur' },
        ],
        wage: [
            { required: this.detailsModel.wage.required, type: 'number', message: '工资 值不能为空', trigger: 'change' },
            { required: this.detailsModel.wage.required, type: 'number', message: '工资 值不能为空', trigger: 'blur' },
        ],
        date_end: [
            {validator:(rule:any, value:any)=>{return this.verifyDeRules("date_end").isPast},message: this.verifyDeRules("date_end").infoMessage, trigger: 'change' },
            {validator:(rule:any, value:any)=>{return this.verifyDeRules("date_end").isPast},message: this.verifyDeRules("date_end").infoMessage, trigger: 'blur' },
        ],
        trial_date_end: [
            {validator:(rule:any, value:any)=>{return this.verifyDeRules("trial_date_end").isPast},message: this.verifyDeRules("trial_date_end").infoMessage, trigger: 'change' },
            {validator:(rule:any, value:any)=>{return this.verifyDeRules("trial_date_end").isPast},message: this.verifyDeRules("trial_date_end").infoMessage, trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicQuickBase
     */
    public deRules:any = {
                date_end:[
],
                trial_date_end:[
                  {
                      type:"SIMPLE",
                      condOP:"GT",
                      ruleInfo:"", 
                      isKeyCond:false,
                      paramValue:"DATE_START",
                      paramType:"ENTITYFIELD",
                      isNotMode:false,
                      deName:"trial_date_end",
                  },
                  {
                      type:"SIMPLE",
                      condOP:"LTANDEQ",
                      ruleInfo:"", 
                      isKeyCond:false,
                      paramValue:"trial_date_end",
                      paramType:"ENTITYFIELD",
                      isNotMode:false,
                      deName:"trial_date_end",
                  },
                ],
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_BasicQuickEditFormBase
     */
    public detailsModel: any = {
        grouppanel1: new FormGroupPanelModel({ caption: '合同信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_contract.ef_basicquick_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '合同参考', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '合同名', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        company_id_text: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        department_id_text: new FormItemModel({ caption: '部门', detailType: 'FORMITEM', name: 'department_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        employee_id_text: new FormItemModel({ caption: '员工', detailType: 'FORMITEM', name: 'employee_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        job_id_text: new FormItemModel({ caption: '工作岗位', detailType: 'FORMITEM', name: 'job_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        employee_id: new FormItemModel({ caption: '员工', detailType: 'FORMITEM', name: 'employee_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        department_id: new FormItemModel({ caption: '部门', detailType: 'FORMITEM', name: 'department_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        job_id: new FormItemModel({ caption: '工作岗位', detailType: 'FORMITEM', name: 'job_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        date_start: new FormItemModel({ caption: '开始日期', detailType: 'FORMITEM', name: 'date_start', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        wage: new FormItemModel({ caption: '工资', detailType: 'FORMITEM', name: 'wage', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        date_end: new FormItemModel({ caption: '结束日期', detailType: 'FORMITEM', name: 'date_end', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        resource_calendar_id_text: new FormItemModel({ caption: '工作安排', detailType: 'FORMITEM', name: 'resource_calendar_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        trial_date_end: new FormItemModel({ caption: '试用期结束', detailType: 'FORMITEM', name: 'trial_date_end', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        state: new FormItemModel({ caption: '状态', detailType: 'FORMITEM', name: 'state', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        resource_calendar_id: new FormItemModel({ caption: '工作安排', detailType: 'FORMITEM', name: 'resource_calendar_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

    };

    /**
     * 重置表单项值
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof EF_BasicQuickEditFormBase
     */
    public resetFormData({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
        if (Object.is(name, 'company_id_text')) {
            this.onFormItemValueChange({ name: 'department_id_text', value: null });
            this.onFormItemValueChange({ name: 'department_id', value: null });
        }
        if (Object.is(name, 'department_id_text')) {
            this.onFormItemValueChange({ name: 'employee_id_text', value: null });
            this.onFormItemValueChange({ name: 'employee_id', value: null });
        }
        if (Object.is(name, 'department_id_text')) {
            this.onFormItemValueChange({ name: 'job_id_text', value: null });
            this.onFormItemValueChange({ name: 'job_id', value: null });
        }
    }

    /**
     * 新建默认值
     * @memberof EF_BasicQuickEditFormBase
     */
    public createDefault(){                    
        if (this.data.hasOwnProperty('date_start')) {
            this.data['date_start'] = this.$util.dateFormat(new Date());
        }
        if (this.data.hasOwnProperty('wage')) {
            this.data['wage'] = 0;
        }
        if (this.data.hasOwnProperty('state')) {
            this.data['state'] = 'draft';
        }
    }
}