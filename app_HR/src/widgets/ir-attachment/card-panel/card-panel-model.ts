/**
 * Card 部件模型
 *
 * @export
 * @class CardModel
 */
export default class CardModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof CardModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'name',
        prop: 'name'
      }
    ]
  }
}