/**
 * List 部件模型
 *
 * @export
 * @class ListModel
 */
export default class ListModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ListModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'partner_id_text',
        prop: 'partner_id_text'
      }
    ]
  }
}