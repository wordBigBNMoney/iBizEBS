/**
 * MasterMessage 部件模型
 *
 * @export
 * @class MasterMessageModel
 */
export default class MasterMessageModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterMessageModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}
