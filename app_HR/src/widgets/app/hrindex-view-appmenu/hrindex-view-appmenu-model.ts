import { ViewTool } from '@/utils';

/**
 * HRIndexView 部件模型
 *
 * @export
 * @class HRIndexViewModel
 */
export default class HRIndexViewModel {

    /**
     * 菜单项集合
     *
     * @private
     * @type {any[]}
     * @memberof HRIndexViewModel
     */
    private items: any[] = [
                {
        	id: 'ac39619c0559bee9c4ce171b789cef96',
        	name: 'user_menus',
        	text: '用户菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '用户菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'HR-HRIndexView-user_menus',
        }
        ,
                {
        	id: 'f40e40431eabbc4f535144ea170ab1ce',
        	name: 'top_menus',
        	text: '顶部菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '顶部菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'HR-HRIndexView-top_menus',
        }
        ,
                {
        	id: '0291365a2dbe809e130463dd35882f0e',
        	name: 'left_exp',
        	text: '左侧菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '左侧菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'HR-HRIndexView-left_exp',
        	items: [
                		        {
                	id: 'f0fba4509ad1fdc797a654e9cb7ba9f7',
                	name: 'menuitem1',
                	text: '员工',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '员工',
                	expanded: true,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-street-view',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	authtag:'HR-HRIndexView-menuitem1',
                	items: [
                        		        {
                        	id: 'e09f8b90f471419153c2f2d41b79827a',
                        	name: 'menuitem4',
                        	text: '员工',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '员工',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-street-view',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc4',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'hr-employee-tree-exp-view',
                        	authtag:'HR-HRIndexView-menuitem4',
                        }
                        ,
                        		        {
                        	id: 'b9f89174b6dab44eea2b9fa8505a3574',
                        	name: 'menuitem5',
                        	text: '合同',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '合同',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-leanpub',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc2',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'hr-contract-grid-view',
                        	authtag:'HR-HRIndexView-menuitem5',
                        }
                        ,
                	],
                }
                ,
                		        {
                	id: '0cecf582a5a9852d2b7323abe09c2803',
                	name: 'menuitem6',
                	text: '工作岗位',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '工作岗位',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-flickr',
                	icon: '',
                	textcls: '',
                	appfunctag: 'AppFunc3',
                	appfuncyype: 'APPVIEW',
                	viewname: 'hr-job-tree-exp-view',
                	authtag:'HR-HRIndexView-menuitem6',
                }
                ,
                		        {
                	id: '345226bf40b92be4eb2b2d8254dc43db',
                	name: 'menuitem3',
                	text: '配置',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '配置',
                	expanded: false,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-cogs',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	authtag:'HR-HRIndexView-menuitem3',
                	items: [
                        		        {
                        	id: '21c12ebc78c00fa105f4c8b81ab74ec5',
                        	name: 'menuitem2',
                        	text: '设置',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '设置',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '',
                        	authtag:'HR-HRIndexView-menuitem2',
                        }
                        ,
                        		        {
                        	id: '17c327783f17cc70bbcd6d1c26769b42',
                        	name: 'menuitem14',
                        	text: '员工',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '员工',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-puzzle-piece',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '',
                        	authtag:'HR-HRIndexView-menuitem14',
                        	items: [
                                		        {
                                	id: 'ed276d10a3900eb821d1b0088f6f11cc',
                                	name: 'menuitem15',
                                	text: '标签',
                                	type: 'MENUITEM',
                                	counterid: '',
                                	tooltip: '标签',
                                	expanded: false,
                                	separator: false,
                                	hidden: false,
                                	hidesidebar: false,
                                	opendefault: false,
                                	iconcls: 'fa fa-stumbleupon',
                                	icon: '',
                                	textcls: '',
                                	appfunctag: 'AppFunc6',
                                	appfuncyype: 'APPVIEW',
                                	viewname: 'hr-employee-category-line-edit',
                                	authtag:'HR-HRIndexView-menuitem15',
                                }
                                ,
                                		        {
                                	id: '3948ab7500bfdab65c5721f3d6ac1ad4',
                                	name: 'menuitem16',
                                	text: '技能',
                                	type: 'MENUITEM',
                                	counterid: '',
                                	tooltip: '技能',
                                	expanded: false,
                                	separator: false,
                                	hidden: false,
                                	hidesidebar: false,
                                	opendefault: false,
                                	iconcls: 'fa fa-fire',
                                	icon: '',
                                	textcls: '',
                                	appfunctag: 'AppFunc5',
                                	appfuncyype: 'APPVIEW',
                                	viewname: 'hr-skill-type-basic-list-exp-view',
                                	authtag:'HR-HRIndexView-menuitem16',
                                }
                                ,
                        	],
                        }
                        ,
                        		        {
                        	id: '1858373e12a907de9284d56f6ec7ef41',
                        	name: 'menuitem7',
                        	text: '部门',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '部门',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-group',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc7',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'hr-department-master-grid-view',
                        	authtag:'HR-HRIndexView-menuitem7',
                        }
                        ,
                        		        {
                        	id: '63c1873c95771c072bf395d1bbaab4bd',
                        	name: 'menuitem17',
                        	text: '简历',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '简历',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-file-o',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '',
                        	authtag:'HR-HRIndexView-menuitem17',
                        	items: [
                                		        {
                                	id: '56776097adde37933be63bbda6d6a839',
                                	name: 'menuitem18',
                                	text: '类型',
                                	type: 'MENUITEM',
                                	counterid: '',
                                	tooltip: '类型',
                                	expanded: false,
                                	separator: false,
                                	hidden: false,
                                	hidesidebar: false,
                                	opendefault: false,
                                	iconcls: 'fa fa-tumblr-square',
                                	icon: '',
                                	textcls: '',
                                	appfunctag: 'AppFunc8',
                                	appfuncyype: 'APPVIEW',
                                	viewname: 'hr-resume-line-type-line-edit',
                                	authtag:'HR-HRIndexView-menuitem18',
                                }
                                ,
                        	],
                        }
                        ,
                        		        {
                        	id: '2384b1b32150cc54bdb8d06a59c170e6',
                        	name: 'menuitem8',
                        	text: '活动规划',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '活动规划',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-th-large',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '',
                        	authtag:'HR-HRIndexView-menuitem8',
                        	items: [
                                		        {
                                	id: 'e34ebe9a31d36880f16add07edc1c3a2',
                                	name: 'menuitem19',
                                	text: '规划类型',
                                	type: 'MENUITEM',
                                	counterid: '',
                                	tooltip: '规划类型',
                                	expanded: false,
                                	separator: false,
                                	hidden: false,
                                	hidesidebar: false,
                                	opendefault: false,
                                	iconcls: 'fa fa-try',
                                	icon: '',
                                	textcls: '',
                                	appfunctag: '',
                                	authtag:'HR-HRIndexView-menuitem19',
                                }
                                ,
                                		        {
                                	id: 'fe53fa6d201232065eb18d3c3b99ce11',
                                	name: 'menuitem10',
                                	text: '计划',
                                	type: 'MENUITEM',
                                	counterid: '',
                                	tooltip: '计划',
                                	expanded: false,
                                	separator: false,
                                	hidden: false,
                                	hidesidebar: false,
                                	opendefault: false,
                                	iconcls: 'fa fa-send-o',
                                	icon: '',
                                	textcls: '',
                                	appfunctag: '',
                                	authtag:'HR-HRIndexView-menuitem10',
                                }
                                ,
                        	],
                        }
                        ,
                        		        {
                        	id: 'aa945d698d5d8e07a910167a63cd064b',
                        	name: 'menuitem9',
                        	text: '挑战',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '挑战',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-line-chart',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: '',
                        	authtag:'HR-HRIndexView-menuitem9',
                        	items: [
                                		        {
                                	id: '8c41def30d8ecdc928e29edbd99e5663',
                                	name: 'menuitem11',
                                	text: '徽章',
                                	type: 'MENUITEM',
                                	counterid: '',
                                	tooltip: '徽章',
                                	expanded: false,
                                	separator: false,
                                	hidden: false,
                                	hidesidebar: false,
                                	opendefault: false,
                                	iconcls: 'fa fa-lightbulb-o',
                                	icon: '',
                                	textcls: '',
                                	appfunctag: '',
                                	authtag:'HR-HRIndexView-menuitem11',
                                }
                                ,
                                		        {
                                	id: '2dffc4dd53a2935dc85f438911ca99b5',
                                	name: 'menuitem12',
                                	text: '挑战',
                                	type: 'MENUITEM',
                                	counterid: '',
                                	tooltip: '挑战',
                                	expanded: false,
                                	separator: false,
                                	hidden: false,
                                	hidesidebar: false,
                                	opendefault: false,
                                	iconcls: 'fa fa-pencil',
                                	icon: '',
                                	textcls: '',
                                	appfunctag: '',
                                	authtag:'HR-HRIndexView-menuitem12',
                                }
                                ,
                                		        {
                                	id: '34dfc1e4ac249dbf6f8cd2ea1155c75a',
                                	name: 'menuitem13',
                                	text: '历史目标',
                                	type: 'MENUITEM',
                                	counterid: '',
                                	tooltip: '历史目标',
                                	expanded: false,
                                	separator: false,
                                	hidden: false,
                                	hidesidebar: false,
                                	opendefault: false,
                                	iconcls: 'fa fa-graduation-cap',
                                	icon: '',
                                	textcls: '',
                                	appfunctag: '',
                                	authtag:'HR-HRIndexView-menuitem13',
                                }
                                ,
                        	],
                        }
                        ,
                	],
                }
                ,
        	],
        }
        ,
                {
        	id: '388ec8e716a3ea97f6dfef1bd09fdae1',
        	name: 'bottom_exp',
        	text: '底部内容',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部内容',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'HR-HRIndexView-bottom_exp',
        }
        ,
                {
        	id: '5f673d4979ce966f580a4a30db1d9ebe',
        	name: 'footer_left',
        	text: '底部左侧',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部左侧',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'HR-HRIndexView-footer_left',
        }
        ,
                {
        	id: '50a49e7375c2560198403683d389170a',
        	name: 'footer_center',
        	text: '底部中间',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部中间',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'HR-HRIndexView-footer_center',
        }
        ,
                {
        	id: 'b462bb7280fbb4281a45fc2a005bc22e',
        	name: 'footer_right',
        	text: '底部右侧',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部右侧',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'HR-HRIndexView-footer_right',
        }
        ,
    ];

	/**
	 * 应用功能集合
	 *
	 * @private
	 * @type {any[]}
	 * @memberof HRIndexViewModel
	 */
	private funcs: any[] = [
        {
            appfunctag: 'AppFunc2',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'hr_contractgridview',
            deResParameters: [],
            routepath: '/hrindexview/:hrindexview?/hr_contracts/:hr_contract?/gridview/:gridview?',
            parameters: [
                { pathName: 'hr_contracts', parameterName: 'hr_contract' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
        },
        {
            appfunctag: 'AppFunc8',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'hr_resume_line_typelineedit',
            deResParameters: [],
            routepath: '/hrindexview/:hrindexview?/hr_resume_line_types/:hr_resume_line_type?/lineedit/:lineedit?',
            parameters: [
                { pathName: 'hr_resume_line_types', parameterName: 'hr_resume_line_type' },
                { pathName: 'lineedit', parameterName: 'lineedit' },
            ],
        },
        {
            appfunctag: 'AppFunc3',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'hr_jobtreeexpview',
            deResParameters: [],
            routepath: '/hrindexview/:hrindexview?/hr_jobs/:hr_job?/treeexpview/:treeexpview?',
            parameters: [
                { pathName: 'hr_jobs', parameterName: 'hr_job' },
                { pathName: 'treeexpview', parameterName: 'treeexpview' },
            ],
        },
        {
            appfunctag: 'AppFunc5',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'hr_skill_typebasiclistexpview',
            deResParameters: [],
            routepath: '/hrindexview/:hrindexview?/hr_skill_types/:hr_skill_type?/basiclistexpview/:basiclistexpview?',
            parameters: [
                { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
                { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
            ],
        },
        {
            appfunctag: 'AppFunc7',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'hr_departmentmastergridview',
            deResParameters: [],
            routepath: '/hrindexview/:hrindexview?/hr_departments/:hr_department?/mastergridview/:mastergridview?',
            parameters: [
                { pathName: 'hr_departments', parameterName: 'hr_department' },
                { pathName: 'mastergridview', parameterName: 'mastergridview' },
            ],
        },
        {
            appfunctag: 'AppFunc4',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'hr_employeetreeexpview',
            deResParameters: [],
            routepath: '/hrindexview/:hrindexview?/hr_employees/:hr_employee?/treeexpview/:treeexpview?',
            parameters: [
                { pathName: 'hr_employees', parameterName: 'hr_employee' },
                { pathName: 'treeexpview', parameterName: 'treeexpview' },
            ],
        },
        {
            appfunctag: 'AppFunc6',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'hr_employee_categorylineedit',
            deResParameters: [],
            routepath: '/hrindexview/:hrindexview?/hr_employee_categories/:hr_employee_category?/lineedit/:lineedit?',
            parameters: [
                { pathName: 'hr_employee_categories', parameterName: 'hr_employee_category' },
                { pathName: 'lineedit', parameterName: 'lineedit' },
            ],
        },
	];

	/**
	 * 根据当前路由查找激活菜单
	 *
	 * @param {*} route
	 * @returns {*}
	 * @memberof HRIndexViewModel
	 */
	public findActiveMenuByRoute(route: any): any {
		if (route) {
			const func = this.funcs.find((item: any) => {
				if (item.openmode === '') {
					const url: string = ViewTool.buildUpRoutePath(route, route.params, [], item.parameters, [], {});
					return url === route.fullPath;
				}
			});
            if (func) {
			    return this.findMenuByFuncTag(func.appfunctag);
            }
		}
	}

	/**
	 * 根据应用功能id查找菜单项
	 *
	 * @param {string} tag
	 * @param {any[]} [menus=this.items]
	 * @returns {*}
	 * @memberof HRIndexViewModel
	 */
	public findMenuByFuncTag(tag: string, menus: any[] = this.items): any {
		let menu: any;
		menus.every((item: any) => {
			if (item.appfunctag === tag) {
				menu = item;
				return false;
			}
			if (item.items) {
				menu = this.findMenuByFuncTag(tag, item.items);
				if (menu) {
					return false;
				}
			}
			return true;
		});
		return menu;
	}

	/**
	 * 查找默认打开菜单
	 *
	 * @param {any[]} [menus=this.items]
	 * @returns {*}
	 * @memberof HRIndexViewModel
	 */
	public findDefaultOpenMenu(menus: any[] = this.items): any {
		let menu: any;
		menus.every((item: any) => {
			if (item.opendefault === true) {
				menu = item;
				return false;
			}
			if (item.items) {
				menu = this.findMenuByFuncTag(item.items);
				if (menu) {
					return false;
				}
			}
			return true;
		});
		return menu;
	}

    /**
     * 获取所有菜单项集合
     *
     * @returns {any[]}
     * @memberof HRIndexViewModel
     */
    public getAppMenuItems(): any[] {
        return this.items;
    }

	/**
	 * 根据名称获取菜单组
	 *
	 * @param {string} name
	 * @returns {*}
	 * @memberof HRIndexViewModel
	 */
	public getMenuGroup(name: string): any {
		return this.items.find((item: any) => Object.is(item.name, name));
	}

    /**
     * 获取所有应用功能集合
     *
     * @returns {any[]}
     * @memberof HRIndexViewModel
     */
    public getAppFuncs(): any[] {
        return this.funcs;
    }
}