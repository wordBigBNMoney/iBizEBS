import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import EF_MasterQuickService from './ef-master-quick-form-service';
import Hr_departmentUIService from '@/uiservice/hr-department/hr-department-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_MasterQuickEditFormBase}
 */
export class EF_MasterQuickEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_MasterQuickService}
     * @memberof EF_MasterQuickEditFormBase
     */
    public service: EF_MasterQuickService = new EF_MasterQuickService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_departmentService}
     * @memberof EF_MasterQuickEditFormBase
     */
    public appEntityService: Hr_departmentService = new Hr_departmentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected appDeName: string = 'hr_department';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected appDeLogicName: string = 'HR 部门';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_departmentUIService}
     * @memberof EF_MasterQuickBase
     */  
    public appUIService:Hr_departmentUIService = new Hr_departmentUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        company_id_text: null,
        manager_id_text: null,
        parent_id_text: null,
        parent_id: null,
        manager_id: null,
        company_id: null,
        id: null,
        hr_department:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public rules():any{
        return {
        name: [
            { required: this.detailsModel.name.required, type: 'string', message: '部门名称 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name.required, type: 'string', message: '部门名称 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterQuickBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.hr_department.ef_masterquick_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '部门名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '部门名称', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        company_id_text: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        manager_id_text: new FormItemModel({ caption: '经理', detailType: 'FORMITEM', name: 'manager_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        parent_id_text: new FormItemModel({ caption: '上级部门', detailType: 'FORMITEM', name: 'parent_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        parent_id: new FormItemModel({ caption: '上级部门', detailType: 'FORMITEM', name: 'parent_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        manager_id: new FormItemModel({ caption: '经理', detailType: 'FORMITEM', name: 'manager_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

    };

    /**
     * 重置表单项值
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof EF_MasterQuickEditFormBase
     */
    public resetFormData({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
        if (Object.is(name, 'company_id_text')) {
            this.onFormItemValueChange({ name: 'parent_id_text', value: null });
            this.onFormItemValueChange({ name: 'parent_id', value: null });
        }
    }
}