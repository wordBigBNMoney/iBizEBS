/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'message_needaction',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'absence_of_today',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'leave_to_approve_count',
      },
      {
        name: 'color',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'name',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'active',
      },
      {
        name: 'write_date',
      },
      {
        name: 'expense_sheets_to_approve_count',
      },
      {
        name: 'allocation_to_approve_count',
      },
      {
        name: 'new_hired_employee',
      },
      {
        name: 'note',
      },
      {
        name: 'child_ids',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'expected_employee',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'total_employee',
      },
      {
        name: '__last_update',
      },
      {
        name: 'hr_department',
        prop: 'id',
      },
      {
        name: 'jobs_ids',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'display_name',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'new_applicant_count',
      },
      {
        name: 'create_date',
      },
      {
        name: 'complete_name',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'member_ids',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'manager_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'parent_id_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'manager_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'company_id',
      },
    ]
  }


}