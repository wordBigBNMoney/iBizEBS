package cn.ibizlab.businesscentral.core.odoo_stock.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_level;
/**
 * 关系型数据实体[Stock_package_level] 查询条件对象
 */
@Slf4j
@Data
public class Stock_package_levelSearchContext extends QueryWrapperContext<Stock_package_level> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_picking_id_text_eq;//[分拣]
	public void setN_picking_id_text_eq(String n_picking_id_text_eq) {
        this.n_picking_id_text_eq = n_picking_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_id_text_eq)){
            this.getSearchCond().eq("picking_id_text", n_picking_id_text_eq);
        }
    }
	private String n_picking_id_text_like;//[分拣]
	public void setN_picking_id_text_like(String n_picking_id_text_like) {
        this.n_picking_id_text_like = n_picking_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_picking_id_text_like)){
            this.getSearchCond().like("picking_id_text", n_picking_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_package_id_text_eq;//[包裹]
	public void setN_package_id_text_eq(String n_package_id_text_eq) {
        this.n_package_id_text_eq = n_package_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_package_id_text_eq)){
            this.getSearchCond().eq("package_id_text", n_package_id_text_eq);
        }
    }
	private String n_package_id_text_like;//[包裹]
	public void setN_package_id_text_like(String n_package_id_text_like) {
        this.n_package_id_text_like = n_package_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_package_id_text_like)){
            this.getSearchCond().like("package_id_text", n_package_id_text_like);
        }
    }
	private String n_location_dest_id_text_eq;//[至]
	public void setN_location_dest_id_text_eq(String n_location_dest_id_text_eq) {
        this.n_location_dest_id_text_eq = n_location_dest_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_text_eq)){
            this.getSearchCond().eq("location_dest_id_text", n_location_dest_id_text_eq);
        }
    }
	private String n_location_dest_id_text_like;//[至]
	public void setN_location_dest_id_text_like(String n_location_dest_id_text_like) {
        this.n_location_dest_id_text_like = n_location_dest_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_text_like)){
            this.getSearchCond().like("location_dest_id_text", n_location_dest_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_picking_id_eq;//[分拣]
	public void setN_picking_id_eq(Long n_picking_id_eq) {
        this.n_picking_id_eq = n_picking_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_id_eq)){
            this.getSearchCond().eq("picking_id", n_picking_id_eq);
        }
    }
	private Long n_package_id_eq;//[包裹]
	public void setN_package_id_eq(Long n_package_id_eq) {
        this.n_package_id_eq = n_package_id_eq;
        if(!ObjectUtils.isEmpty(this.n_package_id_eq)){
            this.getSearchCond().eq("package_id", n_package_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_location_dest_id_eq;//[至]
	public void setN_location_dest_id_eq(Long n_location_dest_id_eq) {
        this.n_location_dest_id_eq = n_location_dest_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_eq)){
            this.getSearchCond().eq("location_dest_id", n_location_dest_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



