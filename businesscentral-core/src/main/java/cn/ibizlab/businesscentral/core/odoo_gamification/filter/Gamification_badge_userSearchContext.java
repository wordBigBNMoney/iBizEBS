package cn.ibizlab.businesscentral.core.odoo_gamification.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user;
/**
 * 关系型数据实体[Gamification_badge_user] 查询条件对象
 */
@Slf4j
@Data
public class Gamification_badge_userSearchContext extends QueryWrapperContext<Gamification_badge_user> {

	private String n_user_id_text_eq;//[用户]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[用户]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_sender_id_text_eq;//[发送者]
	public void setN_sender_id_text_eq(String n_sender_id_text_eq) {
        this.n_sender_id_text_eq = n_sender_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sender_id_text_eq)){
            this.getSearchCond().eq("sender_id_text", n_sender_id_text_eq);
        }
    }
	private String n_sender_id_text_like;//[发送者]
	public void setN_sender_id_text_like(String n_sender_id_text_like) {
        this.n_sender_id_text_like = n_sender_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sender_id_text_like)){
            this.getSearchCond().like("sender_id_text", n_sender_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_badge_name_eq;//[徽章名称]
	public void setN_badge_name_eq(String n_badge_name_eq) {
        this.n_badge_name_eq = n_badge_name_eq;
        if(!ObjectUtils.isEmpty(this.n_badge_name_eq)){
            this.getSearchCond().eq("badge_name", n_badge_name_eq);
        }
    }
	private String n_badge_name_like;//[徽章名称]
	public void setN_badge_name_like(String n_badge_name_like) {
        this.n_badge_name_like = n_badge_name_like;
        if(!ObjectUtils.isEmpty(this.n_badge_name_like)){
            this.getSearchCond().like("badge_name", n_badge_name_like);
        }
    }
	private String n_challenge_id_text_eq;//[挑战源于]
	public void setN_challenge_id_text_eq(String n_challenge_id_text_eq) {
        this.n_challenge_id_text_eq = n_challenge_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_challenge_id_text_eq)){
            this.getSearchCond().eq("challenge_id_text", n_challenge_id_text_eq);
        }
    }
	private String n_challenge_id_text_like;//[挑战源于]
	public void setN_challenge_id_text_like(String n_challenge_id_text_like) {
        this.n_challenge_id_text_like = n_challenge_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_challenge_id_text_like)){
            this.getSearchCond().like("challenge_id_text", n_challenge_id_text_like);
        }
    }
	private String n_employee_id_text_eq;//[员工]
	public void setN_employee_id_text_eq(String n_employee_id_text_eq) {
        this.n_employee_id_text_eq = n_employee_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_id_text_eq)){
            this.getSearchCond().eq("employee_id_text", n_employee_id_text_eq);
        }
    }
	private String n_employee_id_text_like;//[员工]
	public void setN_employee_id_text_like(String n_employee_id_text_like) {
        this.n_employee_id_text_like = n_employee_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_employee_id_text_like)){
            this.getSearchCond().like("employee_id_text", n_employee_id_text_like);
        }
    }
	private Long n_sender_id_eq;//[发送者]
	public void setN_sender_id_eq(Long n_sender_id_eq) {
        this.n_sender_id_eq = n_sender_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sender_id_eq)){
            this.getSearchCond().eq("sender_id", n_sender_id_eq);
        }
    }
	private Long n_badge_id_eq;//[徽章]
	public void setN_badge_id_eq(Long n_badge_id_eq) {
        this.n_badge_id_eq = n_badge_id_eq;
        if(!ObjectUtils.isEmpty(this.n_badge_id_eq)){
            this.getSearchCond().eq("badge_id", n_badge_id_eq);
        }
    }
	private Long n_challenge_id_eq;//[挑战源于]
	public void setN_challenge_id_eq(Long n_challenge_id_eq) {
        this.n_challenge_id_eq = n_challenge_id_eq;
        if(!ObjectUtils.isEmpty(this.n_challenge_id_eq)){
            this.getSearchCond().eq("challenge_id", n_challenge_id_eq);
        }
    }
	private Long n_employee_id_eq;//[员工]
	public void setN_employee_id_eq(Long n_employee_id_eq) {
        this.n_employee_id_eq = n_employee_id_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_id_eq)){
            this.getSearchCond().eq("employee_id", n_employee_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_user_id_eq;//[用户]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



