package cn.ibizlab.businesscentral.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Resource_calendar_attendance] 服务对象接口
 */
public interface IResource_calendar_attendanceService extends IService<Resource_calendar_attendance>{

    boolean create(Resource_calendar_attendance et) ;
    void createBatch(List<Resource_calendar_attendance> list) ;
    boolean update(Resource_calendar_attendance et) ;
    void updateBatch(List<Resource_calendar_attendance> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Resource_calendar_attendance get(Long key) ;
    Resource_calendar_attendance getDraft(Resource_calendar_attendance et) ;
    boolean checkKey(Resource_calendar_attendance et) ;
    boolean save(Resource_calendar_attendance et) ;
    void saveBatch(List<Resource_calendar_attendance> list) ;
    Page<Resource_calendar_attendance> searchDefault(Resource_calendar_attendanceSearchContext context) ;
    List<Resource_calendar_attendance> selectByCalendarId(Long id);
    void removeByCalendarId(Collection<Long> ids);
    void removeByCalendarId(Long id);
    List<Resource_calendar_attendance> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Resource_calendar_attendance> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Resource_calendar_attendance> getResourceCalendarAttendanceByIds(List<Long> ids) ;
    List<Resource_calendar_attendance> getResourceCalendarAttendanceByEntities(List<Resource_calendar_attendance> entities) ;
}


