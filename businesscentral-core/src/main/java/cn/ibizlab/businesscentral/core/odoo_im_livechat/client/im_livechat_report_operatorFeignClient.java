package cn.ibizlab.businesscentral.core.odoo_im_livechat.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[im_livechat_report_operator] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-im-livechat:odoo-im-livechat}", contextId = "im-livechat-report-operator", fallback = im_livechat_report_operatorFallback.class)
public interface im_livechat_report_operatorFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/search")
    Page<Im_livechat_report_operator> search(@RequestBody Im_livechat_report_operatorSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_operators/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_operators/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators")
    Im_livechat_report_operator create(@RequestBody Im_livechat_report_operator im_livechat_report_operator);

    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/batch")
    Boolean createBatch(@RequestBody List<Im_livechat_report_operator> im_livechat_report_operators);


    @RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_operators/{id}")
    Im_livechat_report_operator update(@PathVariable("id") Long id,@RequestBody Im_livechat_report_operator im_livechat_report_operator);

    @RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_operators/batch")
    Boolean updateBatch(@RequestBody List<Im_livechat_report_operator> im_livechat_report_operators);


    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_operators/{id}")
    Im_livechat_report_operator get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_operators/select")
    Page<Im_livechat_report_operator> select();


    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_operators/getdraft")
    Im_livechat_report_operator getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/checkkey")
    Boolean checkKey(@RequestBody Im_livechat_report_operator im_livechat_report_operator);


    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/save")
    Boolean save(@RequestBody Im_livechat_report_operator im_livechat_report_operator);

    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/savebatch")
    Boolean saveBatch(@RequestBody List<Im_livechat_report_operator> im_livechat_report_operators);



    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/searchdefault")
    Page<Im_livechat_report_operator> searchDefault(@RequestBody Im_livechat_report_operatorSearchContext context);


}
