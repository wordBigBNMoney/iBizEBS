package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_putawaySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_putaway] 服务对象接口
 */
public interface IProduct_putawayService extends IService<Product_putaway>{

    boolean create(Product_putaway et) ;
    void createBatch(List<Product_putaway> list) ;
    boolean update(Product_putaway et) ;
    void updateBatch(List<Product_putaway> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_putaway get(Long key) ;
    Product_putaway getDraft(Product_putaway et) ;
    boolean checkKey(Product_putaway et) ;
    boolean save(Product_putaway et) ;
    void saveBatch(List<Product_putaway> list) ;
    Page<Product_putaway> searchDefault(Product_putawaySearchContext context) ;
    List<Product_putaway> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_putaway> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_putaway> getProductPutawayByIds(List<Long> ids) ;
    List<Product_putaway> getProductPutawayByEntities(List<Product_putaway> entities) ;
}


