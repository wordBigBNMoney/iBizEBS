package cn.ibizlab.businesscentral.core.odoo_event.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_type_mailSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Event_type_mailMapper extends BaseMapper<Event_type_mail>{

    Page<Event_type_mail> searchDefault(IPage page, @Param("srf") Event_type_mailSearchContext context, @Param("ew") Wrapper<Event_type_mail> wrapper) ;
    @Override
    Event_type_mail selectById(Serializable id);
    @Override
    int insert(Event_type_mail entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Event_type_mail entity);
    @Override
    int update(@Param(Constants.ENTITY) Event_type_mail entity, @Param("ew") Wrapper<Event_type_mail> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Event_type_mail> selectByEventTypeId(@Param("id") Serializable id) ;

    List<Event_type_mail> selectByTemplateId(@Param("id") Serializable id) ;

    List<Event_type_mail> selectByCreateUid(@Param("id") Serializable id) ;

    List<Event_type_mail> selectByWriteUid(@Param("id") Serializable id) ;


}
