package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_followersSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_followers] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-followers", fallback = mail_followersFallback.class)
public interface mail_followersFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_followers/{id}")
    Mail_followers update(@PathVariable("id") Long id,@RequestBody Mail_followers mail_followers);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_followers/batch")
    Boolean updateBatch(@RequestBody List<Mail_followers> mail_followers);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_followers/{id}")
    Mail_followers get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_followers/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_followers/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_followers/search")
    Page<Mail_followers> search(@RequestBody Mail_followersSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_followers")
    Mail_followers create(@RequestBody Mail_followers mail_followers);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_followers/batch")
    Boolean createBatch(@RequestBody List<Mail_followers> mail_followers);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_followers/select")
    Page<Mail_followers> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_followers/getdraft")
    Mail_followers getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_followers/checkkey")
    Boolean checkKey(@RequestBody Mail_followers mail_followers);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_followers/save")
    Boolean save(@RequestBody Mail_followers mail_followers);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_followers/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_followers> mail_followers);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_followers/searchdefault")
    Page<Mail_followers> searchDefault(@RequestBody Mail_followersSearchContext context);


}
