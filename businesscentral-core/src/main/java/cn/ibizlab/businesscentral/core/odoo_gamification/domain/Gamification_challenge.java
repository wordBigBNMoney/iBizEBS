package cn.ibizlab.businesscentral.core.odoo_gamification.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[游戏化挑战]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "GAMIFICATION_CHALLENGE",resultMap = "Gamification_challengeResultMap")
public class Gamification_challenge extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 挑战名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 需要激活
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 开始日期
     */
    @DEField(name = "start_date")
    @TableField(value = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 最新报告日期
     */
    @DEField(name = "last_report_date")
    @TableField(value = "last_report_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_report_date" , format="yyyy-MM-dd")
    @JsonProperty("last_report_date")
    private Timestamp lastReportDate;
    /**
     * 结束日期
     */
    @DEField(name = "end_date")
    @TableField(value = "end_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "end_date" , format="yyyy-MM-dd")
    @JsonProperty("end_date")
    private Timestamp endDate;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 用户领域
     */
    @DEField(name = "user_domain")
    @TableField(value = "user_domain")
    @JSONField(name = "user_domain")
    @JsonProperty("user_domain")
    private String userDomain;
    /**
     * 出现在
     */
    @TableField(value = "category")
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 用户
     */
    @TableField(exist = false)
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 明细行
     */
    @TableField(exist = false)
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    private String lineIds;
    /**
     * 报告的频率
     */
    @DEField(name = "report_message_frequency")
    @TableField(value = "report_message_frequency")
    @JSONField(name = "report_message_frequency")
    @JsonProperty("report_message_frequency")
    private String reportMessageFrequency;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 每完成一个目标就马上奖励
     */
    @DEField(name = "reward_realtime")
    @TableField(value = "reward_realtime")
    @JSONField(name = "reward_realtime")
    @JsonProperty("reward_realtime")
    private Boolean rewardRealtime;
    /**
     * 奖励未达成目标的最优者?
     */
    @DEField(name = "reward_failure")
    @TableField(value = "reward_failure")
    @JSONField(name = "reward_failure")
    @JsonProperty("reward_failure")
    private Boolean rewardFailure;
    /**
     * 周期
     */
    @TableField(value = "period")
    @JSONField(name = "period")
    @JsonProperty("period")
    private String period;
    /**
     * 显示模式
     */
    @DEField(name = "visibility_mode")
    @TableField(value = "visibility_mode")
    @JSONField(name = "visibility_mode")
    @JsonProperty("visibility_mode")
    private String visibilityMode;
    /**
     * 未更新的手动目标稍后将被提醒
     */
    @DEField(name = "remind_update_delay")
    @TableField(value = "remind_update_delay")
    @JSONField(name = "remind_update_delay")
    @JsonProperty("remind_update_delay")
    private Integer remindUpdateDelay;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 建议用户
     */
    @TableField(exist = false)
    @JSONField(name = "invited_user_ids")
    @JsonProperty("invited_user_ids")
    private String invitedUserIds;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 下次报告日期
     */
    @DEField(name = "next_report_date")
    @TableField(value = "next_report_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_report_date" , format="yyyy-MM-dd")
    @JsonProperty("next_report_date")
    private Timestamp nextReportDate;
    /**
     * 负责人
     */
    @TableField(exist = false)
    @JSONField(name = "manager_id_text")
    @JsonProperty("manager_id_text")
    private String managerIdText;
    /**
     * 抄送
     */
    @TableField(exist = false)
    @JSONField(name = "report_message_group_id_text")
    @JsonProperty("report_message_group_id_text")
    private String reportMessageGroupIdText;
    /**
     * 报告模板
     */
    @TableField(exist = false)
    @JSONField(name = "report_template_id_text")
    @JsonProperty("report_template_id_text")
    private String reportTemplateIdText;
    /**
     * 每位获得成功的用户
     */
    @TableField(exist = false)
    @JSONField(name = "reward_id_text")
    @JsonProperty("reward_id_text")
    private String rewardIdText;
    /**
     * 第一位用户
     */
    @TableField(exist = false)
    @JSONField(name = "reward_first_id_text")
    @JsonProperty("reward_first_id_text")
    private String rewardFirstIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 第二位用户
     */
    @TableField(exist = false)
    @JSONField(name = "reward_second_id_text")
    @JsonProperty("reward_second_id_text")
    private String rewardSecondIdText;
    /**
     * 第三位用户
     */
    @TableField(exist = false)
    @JSONField(name = "reward_third_id_text")
    @JsonProperty("reward_third_id_text")
    private String rewardThirdIdText;
    /**
     * 第一位用户
     */
    @DEField(name = "reward_first_id")
    @TableField(value = "reward_first_id")
    @JSONField(name = "reward_first_id")
    @JsonProperty("reward_first_id")
    private Long rewardFirstId;
    /**
     * 报告模板
     */
    @DEField(name = "report_template_id")
    @TableField(value = "report_template_id")
    @JSONField(name = "report_template_id")
    @JsonProperty("report_template_id")
    private Long reportTemplateId;
    /**
     * 第二位用户
     */
    @DEField(name = "reward_second_id")
    @TableField(value = "reward_second_id")
    @JSONField(name = "reward_second_id")
    @JsonProperty("reward_second_id")
    private Long rewardSecondId;
    /**
     * 第三位用户
     */
    @DEField(name = "reward_third_id")
    @TableField(value = "reward_third_id")
    @JSONField(name = "reward_third_id")
    @JsonProperty("reward_third_id")
    private Long rewardThirdId;
    /**
     * 抄送
     */
    @DEField(name = "report_message_group_id")
    @TableField(value = "report_message_group_id")
    @JSONField(name = "report_message_group_id")
    @JsonProperty("report_message_group_id")
    private Long reportMessageGroupId;
    /**
     * 每位获得成功的用户
     */
    @DEField(name = "reward_id")
    @TableField(value = "reward_id")
    @JSONField(name = "reward_id")
    @JsonProperty("reward_id")
    private Long rewardId;
    /**
     * 负责人
     */
    @DEField(name = "manager_id")
    @TableField(value = "manager_id")
    @JSONField(name = "manager_id")
    @JsonProperty("manager_id")
    private Long managerId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooRewardFirst;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooReward;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooRewardSecond;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooRewardThird;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel odooReportMessageGroup;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooReportTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooManager;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [挑战名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatStartDate(){
        if (this.startDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(startDate);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [最新报告日期]
     */
    public void setLastReportDate(Timestamp lastReportDate){
        this.lastReportDate = lastReportDate ;
        this.modify("last_report_date",lastReportDate);
    }

    /**
     * 格式化日期 [最新报告日期]
     */
    public String formatLastReportDate(){
        if (this.lastReportDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(lastReportDate);
    }
    /**
     * 设置 [结束日期]
     */
    public void setEndDate(Timestamp endDate){
        this.endDate = endDate ;
        this.modify("end_date",endDate);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatEndDate(){
        if (this.endDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(endDate);
    }
    /**
     * 设置 [用户领域]
     */
    public void setUserDomain(String userDomain){
        this.userDomain = userDomain ;
        this.modify("user_domain",userDomain);
    }

    /**
     * 设置 [出现在]
     */
    public void setCategory(String category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [报告的频率]
     */
    public void setReportMessageFrequency(String reportMessageFrequency){
        this.reportMessageFrequency = reportMessageFrequency ;
        this.modify("report_message_frequency",reportMessageFrequency);
    }

    /**
     * 设置 [每完成一个目标就马上奖励]
     */
    public void setRewardRealtime(Boolean rewardRealtime){
        this.rewardRealtime = rewardRealtime ;
        this.modify("reward_realtime",rewardRealtime);
    }

    /**
     * 设置 [奖励未达成目标的最优者?]
     */
    public void setRewardFailure(Boolean rewardFailure){
        this.rewardFailure = rewardFailure ;
        this.modify("reward_failure",rewardFailure);
    }

    /**
     * 设置 [周期]
     */
    public void setPeriod(String period){
        this.period = period ;
        this.modify("period",period);
    }

    /**
     * 设置 [显示模式]
     */
    public void setVisibilityMode(String visibilityMode){
        this.visibilityMode = visibilityMode ;
        this.modify("visibility_mode",visibilityMode);
    }

    /**
     * 设置 [未更新的手动目标稍后将被提醒]
     */
    public void setRemindUpdateDelay(Integer remindUpdateDelay){
        this.remindUpdateDelay = remindUpdateDelay ;
        this.modify("remind_update_delay",remindUpdateDelay);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [下次报告日期]
     */
    public void setNextReportDate(Timestamp nextReportDate){
        this.nextReportDate = nextReportDate ;
        this.modify("next_report_date",nextReportDate);
    }

    /**
     * 格式化日期 [下次报告日期]
     */
    public String formatNextReportDate(){
        if (this.nextReportDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(nextReportDate);
    }
    /**
     * 设置 [第一位用户]
     */
    public void setRewardFirstId(Long rewardFirstId){
        this.rewardFirstId = rewardFirstId ;
        this.modify("reward_first_id",rewardFirstId);
    }

    /**
     * 设置 [报告模板]
     */
    public void setReportTemplateId(Long reportTemplateId){
        this.reportTemplateId = reportTemplateId ;
        this.modify("report_template_id",reportTemplateId);
    }

    /**
     * 设置 [第二位用户]
     */
    public void setRewardSecondId(Long rewardSecondId){
        this.rewardSecondId = rewardSecondId ;
        this.modify("reward_second_id",rewardSecondId);
    }

    /**
     * 设置 [第三位用户]
     */
    public void setRewardThirdId(Long rewardThirdId){
        this.rewardThirdId = rewardThirdId ;
        this.modify("reward_third_id",rewardThirdId);
    }

    /**
     * 设置 [抄送]
     */
    public void setReportMessageGroupId(Long reportMessageGroupId){
        this.reportMessageGroupId = reportMessageGroupId ;
        this.modify("report_message_group_id",reportMessageGroupId);
    }

    /**
     * 设置 [每位获得成功的用户]
     */
    public void setRewardId(Long rewardId){
        this.rewardId = rewardId ;
        this.modify("reward_id",rewardId);
    }

    /**
     * 设置 [负责人]
     */
    public void setManagerId(Long managerId){
        this.managerId = managerId ;
        this.modify("manager_id",managerId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


