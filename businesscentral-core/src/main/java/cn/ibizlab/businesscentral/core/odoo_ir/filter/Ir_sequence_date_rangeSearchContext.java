package cn.ibizlab.businesscentral.core.odoo_ir.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence_date_range;
/**
 * 关系型数据实体[Ir_sequence_date_range] 查询条件对象
 */
@Slf4j
@Data
public class Ir_sequence_date_rangeSearchContext extends QueryWrapperContext<Ir_sequence_date_range> {

    @JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
    @JSONField(format="yyyy-MM-dd")
	private Timestamp n_date_from_ltandeq;//[起始日期]
	public void setN_date_from_ltandeq(Timestamp n_date_from_ltandeq) {
        this.n_date_from_ltandeq = n_date_from_ltandeq;
        if(!ObjectUtils.isEmpty(this.n_date_from_ltandeq)){
            this.getSearchCond().le("date_from", n_date_from_ltandeq);
        }
    }
    @JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
    @JSONField(format="yyyy-MM-dd")
	private Timestamp n_date_to_gtandeq;//[结束日期]
	public void setN_date_to_gtandeq(Timestamp n_date_to_gtandeq) {
        this.n_date_to_gtandeq = n_date_to_gtandeq;
        if(!ObjectUtils.isEmpty(this.n_date_to_gtandeq)){
            this.getSearchCond().ge("date_to", n_date_to_gtandeq);
        }
    }
	private String n_create_uname_eq;//[创建人]
	public void setN_create_uname_eq(String n_create_uname_eq) {
        this.n_create_uname_eq = n_create_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uname_eq)){
            this.getSearchCond().eq("create_uname", n_create_uname_eq);
        }
    }
	private String n_create_uname_like;//[创建人]
	public void setN_create_uname_like(String n_create_uname_like) {
        this.n_create_uname_like = n_create_uname_like;
        if(!ObjectUtils.isEmpty(this.n_create_uname_like)){
            this.getSearchCond().like("create_uname", n_create_uname_like);
        }
    }
	private String n_write_uname_eq;//[最后更新人]
	public void setN_write_uname_eq(String n_write_uname_eq) {
        this.n_write_uname_eq = n_write_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uname_eq)){
            this.getSearchCond().eq("write_uname", n_write_uname_eq);
        }
    }
	private String n_write_uname_like;//[最后更新人]
	public void setN_write_uname_like(String n_write_uname_like) {
        this.n_write_uname_like = n_write_uname_like;
        if(!ObjectUtils.isEmpty(this.n_write_uname_like)){
            this.getSearchCond().like("write_uname", n_write_uname_like);
        }
    }
	private Long n_sequence_id_eq;//[ID]
	public void setN_sequence_id_eq(Long n_sequence_id_eq) {
        this.n_sequence_id_eq = n_sequence_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sequence_id_eq)){
            this.getSearchCond().eq("sequence_id", n_sequence_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



