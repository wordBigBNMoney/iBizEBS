package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_chart_templateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_chart_template] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-chart-template", fallback = account_chart_templateFallback.class)
public interface account_chart_templateFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates")
    Account_chart_template create(@RequestBody Account_chart_template account_chart_template);

    @RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates/batch")
    Boolean createBatch(@RequestBody List<Account_chart_template> account_chart_templates);



    @RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates/search")
    Page<Account_chart_template> search(@RequestBody Account_chart_templateSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_chart_templates/{id}")
    Account_chart_template get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_chart_templates/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_chart_templates/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_chart_templates/{id}")
    Account_chart_template update(@PathVariable("id") Long id,@RequestBody Account_chart_template account_chart_template);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_chart_templates/batch")
    Boolean updateBatch(@RequestBody List<Account_chart_template> account_chart_templates);



    @RequestMapping(method = RequestMethod.GET, value = "/account_chart_templates/select")
    Page<Account_chart_template> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_chart_templates/getdraft")
    Account_chart_template getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates/checkkey")
    Boolean checkKey(@RequestBody Account_chart_template account_chart_template);


    @RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates/save")
    Boolean save(@RequestBody Account_chart_template account_chart_template);

    @RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates/savebatch")
    Boolean saveBatch(@RequestBody List<Account_chart_template> account_chart_templates);



    @RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates/searchdefault")
    Page<Account_chart_template> searchDefault(@RequestBody Account_chart_templateSearchContext context);


}
