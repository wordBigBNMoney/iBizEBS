package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_jobSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_job] 服务对象接口
 */
public interface IHr_jobService extends IService<Hr_job>{

    boolean create(Hr_job et) ;
    void createBatch(List<Hr_job> list) ;
    boolean update(Hr_job et) ;
    void updateBatch(List<Hr_job> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_job get(Long key) ;
    Hr_job getDraft(Hr_job et) ;
    boolean checkKey(Hr_job et) ;
    boolean save(Hr_job et) ;
    void saveBatch(List<Hr_job> list) ;
    Page<Hr_job> searchDefault(Hr_jobSearchContext context) ;
    Page<Hr_job> searchMaster(Hr_jobSearchContext context) ;
    List<Hr_job> selectBySurveyId(Long id);
    void removeBySurveyId(Long id);
    List<Hr_job> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Hr_job> selectByManagerId(Long id);
    void resetByManagerId(Long id);
    void resetByManagerId(Collection<Long> ids);
    void removeByManagerId(Long id);
    List<Hr_job> selectByAliasId(Long id);
    List<Hr_job> selectByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    List<Hr_job> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Hr_job> selectByAddressId(Long id);
    void resetByAddressId(Long id);
    void resetByAddressId(Collection<Long> ids);
    void removeByAddressId(Long id);
    List<Hr_job> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_job> selectByHrResponsibleId(Long id);
    void resetByHrResponsibleId(Long id);
    void resetByHrResponsibleId(Collection<Long> ids);
    void removeByHrResponsibleId(Long id);
    List<Hr_job> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Hr_job> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_job> getHrJobByIds(List<Long> ids) ;
    List<Hr_job> getHrJobByEntities(List<Hr_job> entities) ;
}


