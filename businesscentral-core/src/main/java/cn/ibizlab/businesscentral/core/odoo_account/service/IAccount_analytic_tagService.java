package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_tag;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_tagSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_analytic_tag] 服务对象接口
 */
public interface IAccount_analytic_tagService extends IService<Account_analytic_tag>{

    boolean create(Account_analytic_tag et) ;
    void createBatch(List<Account_analytic_tag> list) ;
    boolean update(Account_analytic_tag et) ;
    void updateBatch(List<Account_analytic_tag> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_analytic_tag get(Long key) ;
    Account_analytic_tag getDraft(Account_analytic_tag et) ;
    boolean checkKey(Account_analytic_tag et) ;
    boolean save(Account_analytic_tag et) ;
    void saveBatch(List<Account_analytic_tag> list) ;
    Page<Account_analytic_tag> searchDefault(Account_analytic_tagSearchContext context) ;
    List<Account_analytic_tag> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_analytic_tag> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_analytic_tag> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_analytic_tag> getAccountAnalyticTagByIds(List<Long> ids) ;
    List<Account_analytic_tag> getAccountAnalyticTagByEntities(List<Account_analytic_tag> entities) ;
}


