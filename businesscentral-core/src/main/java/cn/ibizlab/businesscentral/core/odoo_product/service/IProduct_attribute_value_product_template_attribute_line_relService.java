package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute_value_product_template_attribute_line_rel;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_attribute_value_product_template_attribute_line_relSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_attribute_value_product_template_attribute_line_rel] 服务对象接口
 */
public interface IProduct_attribute_value_product_template_attribute_line_relService extends IService<Product_attribute_value_product_template_attribute_line_rel>{

    boolean create(Product_attribute_value_product_template_attribute_line_rel et) ;
    void createBatch(List<Product_attribute_value_product_template_attribute_line_rel> list) ;
    boolean update(Product_attribute_value_product_template_attribute_line_rel et) ;
    void updateBatch(List<Product_attribute_value_product_template_attribute_line_rel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Product_attribute_value_product_template_attribute_line_rel get(String key) ;
    Product_attribute_value_product_template_attribute_line_rel getDraft(Product_attribute_value_product_template_attribute_line_rel et) ;
    boolean checkKey(Product_attribute_value_product_template_attribute_line_rel et) ;
    boolean save(Product_attribute_value_product_template_attribute_line_rel et) ;
    void saveBatch(List<Product_attribute_value_product_template_attribute_line_rel> list) ;
    Page<Product_attribute_value_product_template_attribute_line_rel> searchDefault(Product_attribute_value_product_template_attribute_line_relSearchContext context) ;
    List<Product_attribute_value_product_template_attribute_line_rel> selectByProductAttributeValueId(Long id);
    void removeByProductAttributeValueId(Long id);
    List<Product_attribute_value_product_template_attribute_line_rel> selectByProductTemplateAttributeLineId(Long id);
    void removeByProductTemplateAttributeLineId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


