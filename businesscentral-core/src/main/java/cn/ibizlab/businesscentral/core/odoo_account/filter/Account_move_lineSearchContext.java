package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line;
/**
 * 关系型数据实体[Account_move_line] 查询条件对象
 */
@Slf4j
@Data
public class Account_move_lineSearchContext extends QueryWrapperContext<Account_move_line> {

	private String n_name_like;//[标签]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_user_type_id_text_eq;//[类型]
	public void setN_user_type_id_text_eq(String n_user_type_id_text_eq) {
        this.n_user_type_id_text_eq = n_user_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_type_id_text_eq)){
            this.getSearchCond().eq("user_type_id_text", n_user_type_id_text_eq);
        }
    }
	private String n_user_type_id_text_like;//[类型]
	public void setN_user_type_id_text_like(String n_user_type_id_text_like) {
        this.n_user_type_id_text_like = n_user_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_type_id_text_like)){
            this.getSearchCond().like("user_type_id_text", n_user_type_id_text_like);
        }
    }
	private String n_payment_id_text_eq;//[发起人付款]
	public void setN_payment_id_text_eq(String n_payment_id_text_eq) {
        this.n_payment_id_text_eq = n_payment_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_id_text_eq)){
            this.getSearchCond().eq("payment_id_text", n_payment_id_text_eq);
        }
    }
	private String n_payment_id_text_like;//[发起人付款]
	public void setN_payment_id_text_like(String n_payment_id_text_like) {
        this.n_payment_id_text_like = n_payment_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_id_text_like)){
            this.getSearchCond().like("payment_id_text", n_payment_id_text_like);
        }
    }
	private String n_tax_line_id_text_eq;//[发起人税]
	public void setN_tax_line_id_text_eq(String n_tax_line_id_text_eq) {
        this.n_tax_line_id_text_eq = n_tax_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_line_id_text_eq)){
            this.getSearchCond().eq("tax_line_id_text", n_tax_line_id_text_eq);
        }
    }
	private String n_tax_line_id_text_like;//[发起人税]
	public void setN_tax_line_id_text_like(String n_tax_line_id_text_like) {
        this.n_tax_line_id_text_like = n_tax_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_tax_line_id_text_like)){
            this.getSearchCond().like("tax_line_id_text", n_tax_line_id_text_like);
        }
    }
	private String n_full_reconcile_id_text_eq;//[匹配号码]
	public void setN_full_reconcile_id_text_eq(String n_full_reconcile_id_text_eq) {
        this.n_full_reconcile_id_text_eq = n_full_reconcile_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_full_reconcile_id_text_eq)){
            this.getSearchCond().eq("full_reconcile_id_text", n_full_reconcile_id_text_eq);
        }
    }
	private String n_full_reconcile_id_text_like;//[匹配号码]
	public void setN_full_reconcile_id_text_like(String n_full_reconcile_id_text_like) {
        this.n_full_reconcile_id_text_like = n_full_reconcile_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_full_reconcile_id_text_like)){
            this.getSearchCond().like("full_reconcile_id_text", n_full_reconcile_id_text_like);
        }
    }
	private String n_expense_id_text_eq;//[费用]
	public void setN_expense_id_text_eq(String n_expense_id_text_eq) {
        this.n_expense_id_text_eq = n_expense_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_expense_id_text_eq)){
            this.getSearchCond().eq("expense_id_text", n_expense_id_text_eq);
        }
    }
	private String n_expense_id_text_like;//[费用]
	public void setN_expense_id_text_like(String n_expense_id_text_like) {
        this.n_expense_id_text_like = n_expense_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_expense_id_text_like)){
            this.getSearchCond().like("expense_id_text", n_expense_id_text_like);
        }
    }
	private String n_move_id_text_eq;//[日记账分录]
	public void setN_move_id_text_eq(String n_move_id_text_eq) {
        this.n_move_id_text_eq = n_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_text_eq)){
            this.getSearchCond().eq("move_id_text", n_move_id_text_eq);
        }
    }
	private String n_move_id_text_like;//[日记账分录]
	public void setN_move_id_text_like(String n_move_id_text_like) {
        this.n_move_id_text_like = n_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_move_id_text_like)){
            this.getSearchCond().like("move_id_text", n_move_id_text_like);
        }
    }
	private String n_journal_id_text_eq;//[日记账]
	public void setN_journal_id_text_eq(String n_journal_id_text_eq) {
        this.n_journal_id_text_eq = n_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_eq)){
            this.getSearchCond().eq("journal_id_text", n_journal_id_text_eq);
        }
    }
	private String n_journal_id_text_like;//[日记账]
	public void setN_journal_id_text_like(String n_journal_id_text_like) {
        this.n_journal_id_text_like = n_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_like)){
            this.getSearchCond().like("journal_id_text", n_journal_id_text_like);
        }
    }
	private String n_company_currency_id_text_eq;//[公司货币]
	public void setN_company_currency_id_text_eq(String n_company_currency_id_text_eq) {
        this.n_company_currency_id_text_eq = n_company_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_currency_id_text_eq)){
            this.getSearchCond().eq("company_currency_id_text", n_company_currency_id_text_eq);
        }
    }
	private String n_company_currency_id_text_like;//[公司货币]
	public void setN_company_currency_id_text_like(String n_company_currency_id_text_like) {
        this.n_company_currency_id_text_like = n_company_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_currency_id_text_like)){
            this.getSearchCond().like("company_currency_id_text", n_company_currency_id_text_like);
        }
    }
	private String n_invoice_id_text_eq;//[发票]
	public void setN_invoice_id_text_eq(String n_invoice_id_text_eq) {
        this.n_invoice_id_text_eq = n_invoice_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_text_eq)){
            this.getSearchCond().eq("invoice_id_text", n_invoice_id_text_eq);
        }
    }
	private String n_invoice_id_text_like;//[发票]
	public void setN_invoice_id_text_like(String n_invoice_id_text_like) {
        this.n_invoice_id_text_like = n_invoice_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_text_like)){
            this.getSearchCond().like("invoice_id_text", n_invoice_id_text_like);
        }
    }
	private String n_analytic_account_id_text_eq;//[分析账户]
	public void setN_analytic_account_id_text_eq(String n_analytic_account_id_text_eq) {
        this.n_analytic_account_id_text_eq = n_analytic_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_eq)){
            this.getSearchCond().eq("analytic_account_id_text", n_analytic_account_id_text_eq);
        }
    }
	private String n_analytic_account_id_text_like;//[分析账户]
	public void setN_analytic_account_id_text_like(String n_analytic_account_id_text_like) {
        this.n_analytic_account_id_text_like = n_analytic_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_like)){
            this.getSearchCond().like("analytic_account_id_text", n_analytic_account_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_statement_id_text_eq;//[报告]
	public void setN_statement_id_text_eq(String n_statement_id_text_eq) {
        this.n_statement_id_text_eq = n_statement_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_statement_id_text_eq)){
            this.getSearchCond().eq("statement_id_text", n_statement_id_text_eq);
        }
    }
	private String n_statement_id_text_like;//[报告]
	public void setN_statement_id_text_like(String n_statement_id_text_like) {
        this.n_statement_id_text_like = n_statement_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_statement_id_text_like)){
            this.getSearchCond().like("statement_id_text", n_statement_id_text_like);
        }
    }
	private String n_statement_line_id_text_eq;//[用该分录核销的银行核销单明细]
	public void setN_statement_line_id_text_eq(String n_statement_line_id_text_eq) {
        this.n_statement_line_id_text_eq = n_statement_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_statement_line_id_text_eq)){
            this.getSearchCond().eq("statement_line_id_text", n_statement_line_id_text_eq);
        }
    }
	private String n_statement_line_id_text_like;//[用该分录核销的银行核销单明细]
	public void setN_statement_line_id_text_like(String n_statement_line_id_text_like) {
        this.n_statement_line_id_text_like = n_statement_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_statement_line_id_text_like)){
            this.getSearchCond().like("statement_line_id_text", n_statement_line_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[业务伙伴]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[业务伙伴]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_account_id_text_eq;//[科目]
	public void setN_account_id_text_eq(String n_account_id_text_eq) {
        this.n_account_id_text_eq = n_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_eq)){
            this.getSearchCond().eq("account_id_text", n_account_id_text_eq);
        }
    }
	private String n_account_id_text_like;//[科目]
	public void setN_account_id_text_like(String n_account_id_text_like) {
        this.n_account_id_text_like = n_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_like)){
            this.getSearchCond().like("account_id_text", n_account_id_text_like);
        }
    }
	private String n_product_uom_id_text_eq;//[计量单位]
	public void setN_product_uom_id_text_eq(String n_product_uom_id_text_eq) {
        this.n_product_uom_id_text_eq = n_product_uom_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_eq)){
            this.getSearchCond().eq("product_uom_id_text", n_product_uom_id_text_eq);
        }
    }
	private String n_product_uom_id_text_like;//[计量单位]
	public void setN_product_uom_id_text_like(String n_product_uom_id_text_like) {
        this.n_product_uom_id_text_like = n_product_uom_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_like)){
            this.getSearchCond().like("product_uom_id_text", n_product_uom_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_move_id_eq;//[日记账分录]
	public void setN_move_id_eq(Long n_move_id_eq) {
        this.n_move_id_eq = n_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_eq)){
            this.getSearchCond().eq("move_id", n_move_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_payment_id_eq;//[发起人付款]
	public void setN_payment_id_eq(Long n_payment_id_eq) {
        this.n_payment_id_eq = n_payment_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_id_eq)){
            this.getSearchCond().eq("payment_id", n_payment_id_eq);
        }
    }
	private Long n_company_currency_id_eq;//[公司货币]
	public void setN_company_currency_id_eq(Long n_company_currency_id_eq) {
        this.n_company_currency_id_eq = n_company_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_currency_id_eq)){
            this.getSearchCond().eq("company_currency_id", n_company_currency_id_eq);
        }
    }
	private Long n_journal_id_eq;//[日记账]
	public void setN_journal_id_eq(Long n_journal_id_eq) {
        this.n_journal_id_eq = n_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_eq)){
            this.getSearchCond().eq("journal_id", n_journal_id_eq);
        }
    }
	private Long n_analytic_account_id_eq;//[分析账户]
	public void setN_analytic_account_id_eq(Long n_analytic_account_id_eq) {
        this.n_analytic_account_id_eq = n_analytic_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_eq)){
            this.getSearchCond().eq("analytic_account_id", n_analytic_account_id_eq);
        }
    }
	private Long n_invoice_id_eq;//[发票]
	public void setN_invoice_id_eq(Long n_invoice_id_eq) {
        this.n_invoice_id_eq = n_invoice_id_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_eq)){
            this.getSearchCond().eq("invoice_id", n_invoice_id_eq);
        }
    }
	private Long n_tax_line_id_eq;//[发起人税]
	public void setN_tax_line_id_eq(Long n_tax_line_id_eq) {
        this.n_tax_line_id_eq = n_tax_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_line_id_eq)){
            this.getSearchCond().eq("tax_line_id", n_tax_line_id_eq);
        }
    }
	private Long n_full_reconcile_id_eq;//[匹配号码]
	public void setN_full_reconcile_id_eq(Long n_full_reconcile_id_eq) {
        this.n_full_reconcile_id_eq = n_full_reconcile_id_eq;
        if(!ObjectUtils.isEmpty(this.n_full_reconcile_id_eq)){
            this.getSearchCond().eq("full_reconcile_id", n_full_reconcile_id_eq);
        }
    }
	private Long n_statement_line_id_eq;//[用该分录核销的银行核销单明细]
	public void setN_statement_line_id_eq(Long n_statement_line_id_eq) {
        this.n_statement_line_id_eq = n_statement_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_statement_line_id_eq)){
            this.getSearchCond().eq("statement_line_id", n_statement_line_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_product_uom_id_eq;//[计量单位]
	public void setN_product_uom_id_eq(Long n_product_uom_id_eq) {
        this.n_product_uom_id_eq = n_product_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_eq)){
            this.getSearchCond().eq("product_uom_id", n_product_uom_id_eq);
        }
    }
	private Long n_partner_id_eq;//[业务伙伴]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_account_id_eq;//[科目]
	public void setN_account_id_eq(Long n_account_id_eq) {
        this.n_account_id_eq = n_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_eq)){
            this.getSearchCond().eq("account_id", n_account_id_eq);
        }
    }
	private Long n_statement_id_eq;//[报告]
	public void setN_statement_id_eq(Long n_statement_id_eq) {
        this.n_statement_id_eq = n_statement_id_eq;
        if(!ObjectUtils.isEmpty(this.n_statement_id_eq)){
            this.getSearchCond().eq("statement_id", n_statement_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_user_type_id_eq;//[类型]
	public void setN_user_type_id_eq(Long n_user_type_id_eq) {
        this.n_user_type_id_eq = n_user_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_type_id_eq)){
            this.getSearchCond().eq("user_type_id", n_user_type_id_eq);
        }
    }
	private Long n_expense_id_eq;//[费用]
	public void setN_expense_id_eq(Long n_expense_id_eq) {
        this.n_expense_id_eq = n_expense_id_eq;
        if(!ObjectUtils.isEmpty(this.n_expense_id_eq)){
            this.getSearchCond().eq("expense_id", n_expense_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



