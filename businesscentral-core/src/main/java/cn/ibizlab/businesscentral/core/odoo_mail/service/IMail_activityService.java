package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_activitySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_activity] 服务对象接口
 */
public interface IMail_activityService extends IService<Mail_activity>{

    boolean create(Mail_activity et) ;
    void createBatch(List<Mail_activity> list) ;
    boolean update(Mail_activity et) ;
    void updateBatch(List<Mail_activity> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_activity get(Long key) ;
    Mail_activity getDraft(Mail_activity et) ;
    Mail_activity action_done(Mail_activity et) ;
    boolean checkKey(Mail_activity et) ;
    boolean save(Mail_activity et) ;
    void saveBatch(List<Mail_activity> list) ;
    Page<Mail_activity> searchDefault(Mail_activitySearchContext context) ;
    List<Mail_activity> selectByCalendarEventId(Long id);
    void removeByCalendarEventId(Collection<Long> ids);
    void removeByCalendarEventId(Long id);
    List<Mail_activity> selectByActivityTypeId(Long id);
    List<Mail_activity> selectByActivityTypeId(Collection<Long> ids);
    void removeByActivityTypeId(Long id);
    List<Mail_activity> selectByPreviousActivityTypeId(Long id);
    void resetByPreviousActivityTypeId(Long id);
    void resetByPreviousActivityTypeId(Collection<Long> ids);
    void removeByPreviousActivityTypeId(Long id);
    List<Mail_activity> selectByRecommendedActivityTypeId(Long id);
    void resetByRecommendedActivityTypeId(Long id);
    void resetByRecommendedActivityTypeId(Collection<Long> ids);
    void removeByRecommendedActivityTypeId(Long id);
    List<Mail_activity> selectByNoteId(Long id);
    void removeByNoteId(Collection<Long> ids);
    void removeByNoteId(Long id);
    List<Mail_activity> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_activity> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Mail_activity> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_activity> getMailActivityByIds(List<Long> ids) ;
    List<Mail_activity> getMailActivityByEntities(List<Mail_activity> entities) ;
}


