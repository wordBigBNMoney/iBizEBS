package cn.ibizlab.businesscentral.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_mail;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_mailSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_mail] 服务对象接口
 */
@Component
public class event_mailFallback implements event_mailFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Event_mail create(Event_mail event_mail){
            return null;
     }
    public Boolean createBatch(List<Event_mail> event_mails){
            return false;
     }



    public Page<Event_mail> search(Event_mailSearchContext context){
            return null;
     }


    public Event_mail get(Long id){
            return null;
     }


    public Event_mail update(Long id, Event_mail event_mail){
            return null;
     }
    public Boolean updateBatch(List<Event_mail> event_mails){
            return false;
     }


    public Page<Event_mail> select(){
            return null;
     }

    public Event_mail getDraft(){
            return null;
    }



    public Boolean checkKey(Event_mail event_mail){
            return false;
     }


    public Boolean save(Event_mail event_mail){
            return false;
     }
    public Boolean saveBatch(List<Event_mail> event_mails){
            return false;
     }

    public Page<Event_mail> searchDefault(Event_mailSearchContext context){
            return null;
     }


}
