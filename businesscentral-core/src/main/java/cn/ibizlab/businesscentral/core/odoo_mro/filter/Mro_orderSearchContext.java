package cn.ibizlab.businesscentral.core.odoo_mro.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_order;
/**
 * 关系型数据实体[Mro_order] 查询条件对象
 */
@Slf4j
@Data
public class Mro_orderSearchContext extends QueryWrapperContext<Mro_order> {

	private String n_maintenance_type_eq;//[保养类型]
	public void setN_maintenance_type_eq(String n_maintenance_type_eq) {
        this.n_maintenance_type_eq = n_maintenance_type_eq;
        if(!ObjectUtils.isEmpty(this.n_maintenance_type_eq)){
            this.getSearchCond().eq("maintenance_type", n_maintenance_type_eq);
        }
    }
	private String n_name_like;//[编号]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_wo_id_text_eq;//[工单]
	public void setN_wo_id_text_eq(String n_wo_id_text_eq) {
        this.n_wo_id_text_eq = n_wo_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_wo_id_text_eq)){
            this.getSearchCond().eq("wo_id_text", n_wo_id_text_eq);
        }
    }
	private String n_wo_id_text_like;//[工单]
	public void setN_wo_id_text_like(String n_wo_id_text_like) {
        this.n_wo_id_text_like = n_wo_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_wo_id_text_like)){
            this.getSearchCond().like("wo_id_text", n_wo_id_text_like);
        }
    }
	private String n_asset_id_text_eq;//[Asset]
	public void setN_asset_id_text_eq(String n_asset_id_text_eq) {
        this.n_asset_id_text_eq = n_asset_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_asset_id_text_eq)){
            this.getSearchCond().eq("asset_id_text", n_asset_id_text_eq);
        }
    }
	private String n_asset_id_text_like;//[Asset]
	public void setN_asset_id_text_like(String n_asset_id_text_like) {
        this.n_asset_id_text_like = n_asset_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_asset_id_text_like)){
            this.getSearchCond().like("asset_id_text", n_asset_id_text_like);
        }
    }
	private String n_task_id_text_eq;//[Task]
	public void setN_task_id_text_eq(String n_task_id_text_eq) {
        this.n_task_id_text_eq = n_task_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_task_id_text_eq)){
            this.getSearchCond().eq("task_id_text", n_task_id_text_eq);
        }
    }
	private String n_task_id_text_like;//[Task]
	public void setN_task_id_text_like(String n_task_id_text_like) {
        this.n_task_id_text_like = n_task_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_task_id_text_like)){
            this.getSearchCond().like("task_id_text", n_task_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_request_id_text_eq;//[请求]
	public void setN_request_id_text_eq(String n_request_id_text_eq) {
        this.n_request_id_text_eq = n_request_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_request_id_text_eq)){
            this.getSearchCond().eq("request_id_text", n_request_id_text_eq);
        }
    }
	private String n_request_id_text_like;//[请求]
	public void setN_request_id_text_like(String n_request_id_text_like) {
        this.n_request_id_text_like = n_request_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_request_id_text_like)){
            this.getSearchCond().like("request_id_text", n_request_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[负责人]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[负责人]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private Long n_request_id_eq;//[请求]
	public void setN_request_id_eq(Long n_request_id_eq) {
        this.n_request_id_eq = n_request_id_eq;
        if(!ObjectUtils.isEmpty(this.n_request_id_eq)){
            this.getSearchCond().eq("request_id", n_request_id_eq);
        }
    }
	private Long n_task_id_eq;//[Task]
	public void setN_task_id_eq(Long n_task_id_eq) {
        this.n_task_id_eq = n_task_id_eq;
        if(!ObjectUtils.isEmpty(this.n_task_id_eq)){
            this.getSearchCond().eq("task_id", n_task_id_eq);
        }
    }
	private Long n_wo_id_eq;//[工单]
	public void setN_wo_id_eq(Long n_wo_id_eq) {
        this.n_wo_id_eq = n_wo_id_eq;
        if(!ObjectUtils.isEmpty(this.n_wo_id_eq)){
            this.getSearchCond().eq("wo_id", n_wo_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_user_id_eq;//[负责人]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_asset_id_eq;//[Asset]
	public void setN_asset_id_eq(Long n_asset_id_eq) {
        this.n_asset_id_eq = n_asset_id_eq;
        if(!ObjectUtils.isEmpty(this.n_asset_id_eq)){
            this.getSearchCond().eq("asset_id", n_asset_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



