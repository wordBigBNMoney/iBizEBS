package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statementSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_bank_statement] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-bank-statement", fallback = account_bank_statementFallback.class)
public interface account_bank_statementFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statements/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statements/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/search")
    Page<Account_bank_statement> search(@RequestBody Account_bank_statementSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statements/{id}")
    Account_bank_statement get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements")
    Account_bank_statement create(@RequestBody Account_bank_statement account_bank_statement);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/batch")
    Boolean createBatch(@RequestBody List<Account_bank_statement> account_bank_statements);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statements/{id}")
    Account_bank_statement update(@PathVariable("id") Long id,@RequestBody Account_bank_statement account_bank_statement);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statements/batch")
    Boolean updateBatch(@RequestBody List<Account_bank_statement> account_bank_statements);



    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statements/select")
    Page<Account_bank_statement> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statements/getdraft")
    Account_bank_statement getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/checkkey")
    Boolean checkKey(@RequestBody Account_bank_statement account_bank_statement);


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/save")
    Boolean save(@RequestBody Account_bank_statement account_bank_statement);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/savebatch")
    Boolean saveBatch(@RequestBody List<Account_bank_statement> account_bank_statements);



    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/searchdefault")
    Page<Account_bank_statement> searchDefault(@RequestBody Account_bank_statementSearchContext context);


}
