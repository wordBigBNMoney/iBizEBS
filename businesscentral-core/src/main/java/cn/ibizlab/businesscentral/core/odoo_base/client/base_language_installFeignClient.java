package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_install;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_installSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_language_install] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "base-language-install", fallback = base_language_installFallback.class)
public interface base_language_installFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/base_language_installs/{id}")
    Base_language_install update(@PathVariable("id") Long id,@RequestBody Base_language_install base_language_install);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_language_installs/batch")
    Boolean updateBatch(@RequestBody List<Base_language_install> base_language_installs);




    @RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/search")
    Page<Base_language_install> search(@RequestBody Base_language_installSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/base_language_installs")
    Base_language_install create(@RequestBody Base_language_install base_language_install);

    @RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/batch")
    Boolean createBatch(@RequestBody List<Base_language_install> base_language_installs);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_language_installs/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_language_installs/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_language_installs/{id}")
    Base_language_install get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/base_language_installs/select")
    Page<Base_language_install> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_language_installs/getdraft")
    Base_language_install getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/checkkey")
    Boolean checkKey(@RequestBody Base_language_install base_language_install);


    @RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/save")
    Boolean save(@RequestBody Base_language_install base_language_install);

    @RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/savebatch")
    Boolean saveBatch(@RequestBody List<Base_language_install> base_language_installs);



    @RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/searchdefault")
    Page<Base_language_install> searchDefault(@RequestBody Base_language_installSearchContext context);


}
