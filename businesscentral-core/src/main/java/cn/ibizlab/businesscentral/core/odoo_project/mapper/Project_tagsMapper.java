package cn.ibizlab.businesscentral.core.odoo_project.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_tags;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_tagsSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Project_tagsMapper extends BaseMapper<Project_tags>{

    Page<Project_tags> searchDefault(IPage page, @Param("srf") Project_tagsSearchContext context, @Param("ew") Wrapper<Project_tags> wrapper) ;
    @Override
    Project_tags selectById(Serializable id);
    @Override
    int insert(Project_tags entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Project_tags entity);
    @Override
    int update(@Param(Constants.ENTITY) Project_tags entity, @Param("ew") Wrapper<Project_tags> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Project_tags> selectByCreateUid(@Param("id") Serializable id) ;

    List<Project_tags> selectByWriteUid(@Param("id") Serializable id) ;


}
