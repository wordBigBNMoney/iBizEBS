package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_change_standard_price] 服务对象接口
 */
public interface IStock_change_standard_priceService extends IService<Stock_change_standard_price>{

    boolean create(Stock_change_standard_price et) ;
    void createBatch(List<Stock_change_standard_price> list) ;
    boolean update(Stock_change_standard_price et) ;
    void updateBatch(List<Stock_change_standard_price> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_change_standard_price get(Long key) ;
    Stock_change_standard_price getDraft(Stock_change_standard_price et) ;
    boolean checkKey(Stock_change_standard_price et) ;
    boolean save(Stock_change_standard_price et) ;
    void saveBatch(List<Stock_change_standard_price> list) ;
    Page<Stock_change_standard_price> searchDefault(Stock_change_standard_priceSearchContext context) ;
    List<Stock_change_standard_price> selectByCounterpartAccountId(Long id);
    void resetByCounterpartAccountId(Long id);
    void resetByCounterpartAccountId(Collection<Long> ids);
    void removeByCounterpartAccountId(Long id);
    List<Stock_change_standard_price> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_change_standard_price> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_change_standard_price> getStockChangeStandardPriceByIds(List<Long> ids) ;
    List<Stock_change_standard_price> getStockChangeStandardPriceByEntities(List<Stock_change_standard_price> entities) ;
}


