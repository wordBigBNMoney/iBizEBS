package cn.ibizlab.businesscentral.core.odoo_mail.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_compose_messageSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mail_compose_messageMapper extends BaseMapper<Mail_compose_message>{

    Page<Mail_compose_message> searchDefault(IPage page, @Param("srf") Mail_compose_messageSearchContext context, @Param("ew") Wrapper<Mail_compose_message> wrapper) ;
    @Override
    Mail_compose_message selectById(Serializable id);
    @Override
    int insert(Mail_compose_message entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mail_compose_message entity);
    @Override
    int update(@Param(Constants.ENTITY) Mail_compose_message entity, @Param("ew") Wrapper<Mail_compose_message> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mail_compose_message> selectByMailActivityTypeId(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectByMassMailingCampaignId(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectByMassMailingId(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectBySubtypeId(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectByParentId(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectByTemplateId(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectByAuthorId(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectByModeratorId(@Param("id") Serializable id) ;

    List<Mail_compose_message> selectByWriteUid(@Param("id") Serializable id) ;


}
