package cn.ibizlab.businesscentral.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_badge_user_wizard] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-gamification:odoo-gamification}", contextId = "gamification-badge-user-wizard", fallback = gamification_badge_user_wizardFallback.class)
public interface gamification_badge_user_wizardFeignClient {



    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_user_wizards/{id}")
    Gamification_badge_user_wizard get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_user_wizards/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_user_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards")
    Gamification_badge_user_wizard create(@RequestBody Gamification_badge_user_wizard gamification_badge_user_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/batch")
    Boolean createBatch(@RequestBody List<Gamification_badge_user_wizard> gamification_badge_user_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/search")
    Page<Gamification_badge_user_wizard> search(@RequestBody Gamification_badge_user_wizardSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_user_wizards/{id}")
    Gamification_badge_user_wizard update(@PathVariable("id") Long id,@RequestBody Gamification_badge_user_wizard gamification_badge_user_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_user_wizards/batch")
    Boolean updateBatch(@RequestBody List<Gamification_badge_user_wizard> gamification_badge_user_wizards);



    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_user_wizards/select")
    Page<Gamification_badge_user_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_user_wizards/getdraft")
    Gamification_badge_user_wizard getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/checkkey")
    Boolean checkKey(@RequestBody Gamification_badge_user_wizard gamification_badge_user_wizard);


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/save")
    Boolean save(@RequestBody Gamification_badge_user_wizard gamification_badge_user_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/savebatch")
    Boolean saveBatch(@RequestBody List<Gamification_badge_user_wizard> gamification_badge_user_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/searchdefault")
    Page<Gamification_badge_user_wizard> searchDefault(@RequestBody Gamification_badge_user_wizardSearchContext context);


}
