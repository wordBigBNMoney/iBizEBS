package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_package_destinationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_package_destination] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-package-destination", fallback = stock_package_destinationFallback.class)
public interface stock_package_destinationFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_package_destinations/{id}")
    Stock_package_destination update(@PathVariable("id") Long id,@RequestBody Stock_package_destination stock_package_destination);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_package_destinations/batch")
    Boolean updateBatch(@RequestBody List<Stock_package_destination> stock_package_destinations);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/search")
    Page<Stock_package_destination> search(@RequestBody Stock_package_destinationSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_package_destinations/{id}")
    Stock_package_destination get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations")
    Stock_package_destination create(@RequestBody Stock_package_destination stock_package_destination);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/batch")
    Boolean createBatch(@RequestBody List<Stock_package_destination> stock_package_destinations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_destinations/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_destinations/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/stock_package_destinations/select")
    Page<Stock_package_destination> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_package_destinations/getdraft")
    Stock_package_destination getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/checkkey")
    Boolean checkKey(@RequestBody Stock_package_destination stock_package_destination);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/save")
    Boolean save(@RequestBody Stock_package_destination stock_package_destination);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_package_destination> stock_package_destinations);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/searchdefault")
    Page<Stock_package_destination> searchDefault(@RequestBody Stock_package_destinationSearchContext context);


}
