package cn.ibizlab.businesscentral.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_user_inputSearchContext;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_inputService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_survey.mapper.Survey_user_inputMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[调查用户输入] 服务对象接口实现
 */
@Slf4j
@Service("Survey_user_inputServiceImpl")
public class Survey_user_inputServiceImpl extends EBSServiceImpl<Survey_user_inputMapper, Survey_user_input> implements ISurvey_user_inputService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_input_lineService surveyUserInputLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_pageService surveyPageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_surveyService surveySurveyService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "survey.user.input" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Survey_user_input et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_user_inputService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Survey_user_input> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Survey_user_input et) {
        Survey_user_input old = new Survey_user_input() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_user_inputService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_user_inputService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Survey_user_input> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        surveyUserInputLineService.removeByUserInputId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        surveyUserInputLineService.removeByUserInputId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Survey_user_input get(Long key) {
        Survey_user_input et = getById(key);
        if(et==null){
            et=new Survey_user_input();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Survey_user_input getDraft(Survey_user_input et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Survey_user_input et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Survey_user_input et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Survey_user_input et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Survey_user_input> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Survey_user_input> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Survey_user_input> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Survey_user_input>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Survey_user_input>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Survey_user_input>().eq("partner_id",id));
    }

	@Override
    public List<Survey_user_input> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Survey_user_input>().eq("create_uid",id));
    }

	@Override
    public List<Survey_user_input> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Survey_user_input>().eq("write_uid",id));
    }

	@Override
    public List<Survey_user_input> selectByLastDisplayedPageId(Long id) {
        return baseMapper.selectByLastDisplayedPageId(id);
    }
    @Override
    public void resetByLastDisplayedPageId(Long id) {
        this.update(new UpdateWrapper<Survey_user_input>().set("last_displayed_page_id",null).eq("last_displayed_page_id",id));
    }

    @Override
    public void resetByLastDisplayedPageId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Survey_user_input>().set("last_displayed_page_id",null).in("last_displayed_page_id",ids));
    }

    @Override
    public void removeByLastDisplayedPageId(Long id) {
        this.remove(new QueryWrapper<Survey_user_input>().eq("last_displayed_page_id",id));
    }

	@Override
    public List<Survey_user_input> selectBySurveyId(Long id) {
        return baseMapper.selectBySurveyId(id);
    }
    @Override
    public List<Survey_user_input> selectBySurveyId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Survey_user_input>().in("id",ids));
    }

    @Override
    public void removeBySurveyId(Long id) {
        this.remove(new QueryWrapper<Survey_user_input>().eq("survey_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Survey_user_input> searchDefault(Survey_user_inputSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Survey_user_input> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Survey_user_input>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Survey_user_input et){
        //实体关系[DER1N_SURVEY_USER_INPUT__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_SURVEY_USER_INPUT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_SURVEY_USER_INPUT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_SURVEY_USER_INPUT__SURVEY_SURVEY__SURVEY_ID]
        if(!ObjectUtils.isEmpty(et.getSurveyId())){
            cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey odooSurvey=et.getOdooSurvey();
            if(ObjectUtils.isEmpty(odooSurvey)){
                cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey majorEntity=surveySurveyService.get(et.getSurveyId());
                et.setOdooSurvey(majorEntity);
                odooSurvey=majorEntity;
            }
            et.setPrintUrl(odooSurvey.getPrintUrl());
            et.setResultUrl(odooSurvey.getResultUrl());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Survey_user_input> getSurveyUserInputByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Survey_user_input> getSurveyUserInputByEntities(List<Survey_user_input> entities) {
        List ids =new ArrayList();
        for(Survey_user_input entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



