package cn.ibizlab.businesscentral.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_costService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_fleet.mapper.Fleet_vehicle_costMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[车辆相关的费用] 服务对象接口实现
 */
@Slf4j
@Service("Fleet_vehicle_costServiceImpl")
public class Fleet_vehicle_costServiceImpl extends EBSServiceImpl<Fleet_vehicle_costMapper, Fleet_vehicle_cost> implements IFleet_vehicle_costService {


    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_costService fleetVehicleCostService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_contractService fleetVehicleLogContractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_fuelService fleetVehicleLogFuelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_servicesService fleetVehicleLogServicesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_service_typeService fleetServiceTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_odometerService fleetVehicleOdometerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicleService fleetVehicleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "fleet.vehicle.cost" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Fleet_vehicle_cost et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_costService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Fleet_vehicle_cost> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Fleet_vehicle_cost et) {
        Fleet_vehicle_cost old = new Fleet_vehicle_cost() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_costService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_costService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Fleet_vehicle_cost> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        fleetVehicleCostService.resetByParentId(key);
        fleetVehicleLogContractService.removeByCostId(key);
        fleetVehicleLogFuelService.removeByCostId(key);
        fleetVehicleLogServicesService.removeByCostId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        fleetVehicleCostService.resetByParentId(idList);
        fleetVehicleLogContractService.removeByCostId(idList);
        fleetVehicleLogFuelService.removeByCostId(idList);
        fleetVehicleLogServicesService.removeByCostId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Fleet_vehicle_cost get(Long key) {
        Fleet_vehicle_cost et = getById(key);
        if(et==null){
            et=new Fleet_vehicle_cost();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Fleet_vehicle_cost getDraft(Fleet_vehicle_cost et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Fleet_vehicle_cost et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Fleet_vehicle_cost et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Fleet_vehicle_cost et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Fleet_vehicle_cost> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Fleet_vehicle_cost> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Fleet_vehicle_cost> selectByCostSubtypeId(Long id) {
        return baseMapper.selectByCostSubtypeId(id);
    }
    @Override
    public void resetByCostSubtypeId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("cost_subtype_id",null).eq("cost_subtype_id",id));
    }

    @Override
    public void resetByCostSubtypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("cost_subtype_id",null).in("cost_subtype_id",ids));
    }

    @Override
    public void removeByCostSubtypeId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_cost>().eq("cost_subtype_id",id));
    }

	@Override
    public List<Fleet_vehicle_cost> selectByParentId(Long id) {
        return baseMapper.selectByParentId(id);
    }
    @Override
    public void resetByParentId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("parent_id",null).eq("parent_id",id));
    }

    @Override
    public void resetByParentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("parent_id",null).in("parent_id",ids));
    }

    @Override
    public void removeByParentId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_cost>().eq("parent_id",id));
    }

	@Override
    public List<Fleet_vehicle_cost> selectByContractId(Long id) {
        return baseMapper.selectByContractId(id);
    }
    @Override
    public void resetByContractId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("contract_id",null).eq("contract_id",id));
    }

    @Override
    public void resetByContractId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("contract_id",null).in("contract_id",ids));
    }

    @Override
    public void removeByContractId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_cost>().eq("contract_id",id));
    }

	@Override
    public List<Fleet_vehicle_cost> selectByOdometerId(Long id) {
        return baseMapper.selectByOdometerId(id);
    }
    @Override
    public void resetByOdometerId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("odometer_id",null).eq("odometer_id",id));
    }

    @Override
    public void resetByOdometerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("odometer_id",null).in("odometer_id",ids));
    }

    @Override
    public void removeByOdometerId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_cost>().eq("odometer_id",id));
    }

	@Override
    public List<Fleet_vehicle_cost> selectByVehicleId(Long id) {
        return baseMapper.selectByVehicleId(id);
    }
    @Override
    public void resetByVehicleId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("vehicle_id",null).eq("vehicle_id",id));
    }

    @Override
    public void resetByVehicleId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_cost>().set("vehicle_id",null).in("vehicle_id",ids));
    }

    @Override
    public void removeByVehicleId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_cost>().eq("vehicle_id",id));
    }

	@Override
    public List<Fleet_vehicle_cost> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_cost>().eq("create_uid",id));
    }

	@Override
    public List<Fleet_vehicle_cost> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_cost>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Fleet_vehicle_cost> searchDefault(Fleet_vehicle_costSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Fleet_vehicle_cost> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Fleet_vehicle_cost>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Fleet_vehicle_cost et){
        //实体关系[DER1N_FLEET_VEHICLE_COST__FLEET_SERVICE_TYPE__COST_SUBTYPE_ID]
        if(!ObjectUtils.isEmpty(et.getCostSubtypeId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_service_type odooCostSubtype=et.getOdooCostSubtype();
            if(ObjectUtils.isEmpty(odooCostSubtype)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_service_type majorEntity=fleetServiceTypeService.get(et.getCostSubtypeId());
                et.setOdooCostSubtype(majorEntity);
                odooCostSubtype=majorEntity;
            }
            et.setCostSubtypeIdText(odooCostSubtype.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_COST__FLEET_VEHICLE_COST__PARENT_ID]
        if(!ObjectUtils.isEmpty(et.getParentId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost odooParent=et.getOdooParent();
            if(ObjectUtils.isEmpty(odooParent)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost majorEntity=fleetVehicleCostService.get(et.getParentId());
                et.setOdooParent(majorEntity);
                odooParent=majorEntity;
            }
            et.setParentIdText(odooParent.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_COST__FLEET_VEHICLE_LOG_CONTRACT__CONTRACT_ID]
        if(!ObjectUtils.isEmpty(et.getContractId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract odooContract=et.getOdooContract();
            if(ObjectUtils.isEmpty(odooContract)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract majorEntity=fleetVehicleLogContractService.get(et.getContractId());
                et.setOdooContract(majorEntity);
                odooContract=majorEntity;
            }
            et.setContractIdText(odooContract.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_COST__FLEET_VEHICLE_ODOMETER__ODOMETER_ID]
        if(!ObjectUtils.isEmpty(et.getOdometerId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_odometer odooOdometer=et.getOdooOdometer();
            if(ObjectUtils.isEmpty(odooOdometer)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_odometer majorEntity=fleetVehicleOdometerService.get(et.getOdometerId());
                et.setOdooOdometer(majorEntity);
                odooOdometer=majorEntity;
            }
            et.setOdometerIdText(odooOdometer.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_COST__FLEET_VEHICLE__VEHICLE_ID]
        if(!ObjectUtils.isEmpty(et.getVehicleId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle odooVehicle=et.getOdooVehicle();
            if(ObjectUtils.isEmpty(odooVehicle)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle majorEntity=fleetVehicleService.get(et.getVehicleId());
                et.setOdooVehicle(majorEntity);
                odooVehicle=majorEntity;
            }
            et.setName(odooVehicle.getName());
            et.setOdometerUnit(odooVehicle.getOdometerUnit());
        }
        //实体关系[DER1N_FLEET_VEHICLE_COST__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_COST__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Fleet_vehicle_cost> getFleetVehicleCostByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Fleet_vehicle_cost> getFleetVehicleCostByEntities(List<Fleet_vehicle_cost> entities) {
        List ids =new ArrayList();
        for(Fleet_vehicle_cost entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



