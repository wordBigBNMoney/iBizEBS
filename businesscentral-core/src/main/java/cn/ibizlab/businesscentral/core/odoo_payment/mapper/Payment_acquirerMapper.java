package cn.ibizlab.businesscentral.core.odoo_payment.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirerSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Payment_acquirerMapper extends BaseMapper<Payment_acquirer>{

    Page<Payment_acquirer> searchDefault(IPage page, @Param("srf") Payment_acquirerSearchContext context, @Param("ew") Wrapper<Payment_acquirer> wrapper) ;
    @Override
    Payment_acquirer selectById(Serializable id);
    @Override
    int insert(Payment_acquirer entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Payment_acquirer entity);
    @Override
    int update(@Param(Constants.ENTITY) Payment_acquirer entity, @Param("ew") Wrapper<Payment_acquirer> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Payment_acquirer> selectByJournalId(@Param("id") Serializable id) ;

    List<Payment_acquirer> selectByCompanyId(@Param("id") Serializable id) ;

    List<Payment_acquirer> selectByCreateUid(@Param("id") Serializable id) ;

    List<Payment_acquirer> selectByWriteUid(@Param("id") Serializable id) ;


}
