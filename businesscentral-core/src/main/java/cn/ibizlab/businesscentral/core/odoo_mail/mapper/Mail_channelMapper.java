package cn.ibizlab.businesscentral.core.odoo_mail.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_channelSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mail_channelMapper extends BaseMapper<Mail_channel>{

    Page<Mail_channel> searchDefault(IPage page, @Param("srf") Mail_channelSearchContext context, @Param("ew") Wrapper<Mail_channel> wrapper) ;
    @Override
    Mail_channel selectById(Serializable id);
    @Override
    int insert(Mail_channel entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mail_channel entity);
    @Override
    int update(@Param(Constants.ENTITY) Mail_channel entity, @Param("ew") Wrapper<Mail_channel> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mail_channel> selectByLivechatChannelId(@Param("id") Serializable id) ;

    List<Mail_channel> selectByAliasId(@Param("id") Serializable id) ;

    List<Mail_channel> selectByGroupPublicId(@Param("id") Serializable id) ;

    List<Mail_channel> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mail_channel> selectByWriteUid(@Param("id") Serializable id) ;


}
