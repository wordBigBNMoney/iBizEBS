package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_package_destinationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_package_destination] 服务对象接口
 */
public interface IStock_package_destinationService extends IService<Stock_package_destination>{

    boolean create(Stock_package_destination et) ;
    void createBatch(List<Stock_package_destination> list) ;
    boolean update(Stock_package_destination et) ;
    void updateBatch(List<Stock_package_destination> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_package_destination get(Long key) ;
    Stock_package_destination getDraft(Stock_package_destination et) ;
    boolean checkKey(Stock_package_destination et) ;
    boolean save(Stock_package_destination et) ;
    void saveBatch(List<Stock_package_destination> list) ;
    Page<Stock_package_destination> searchDefault(Stock_package_destinationSearchContext context) ;
    List<Stock_package_destination> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_package_destination> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_package_destination> selectByLocationDestId(Long id);
    void resetByLocationDestId(Long id);
    void resetByLocationDestId(Collection<Long> ids);
    void removeByLocationDestId(Long id);
    List<Stock_package_destination> selectByPickingId(Long id);
    void resetByPickingId(Long id);
    void resetByPickingId(Collection<Long> ids);
    void removeByPickingId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_package_destination> getStockPackageDestinationByIds(List<Long> ids) ;
    List<Stock_package_destination> getStockPackageDestinationByEntities(List<Stock_package_destination> entities) ;
}


