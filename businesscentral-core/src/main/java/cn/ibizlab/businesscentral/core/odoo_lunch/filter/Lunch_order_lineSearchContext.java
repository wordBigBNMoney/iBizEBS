package cn.ibizlab.businesscentral.core.odoo_lunch.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order_line;
/**
 * 关系型数据实体[Lunch_order_line] 查询条件对象
 */
@Slf4j
@Data
public class Lunch_order_lineSearchContext extends QueryWrapperContext<Lunch_order_line> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_supplier_text_eq;//[供应商]
	public void setN_supplier_text_eq(String n_supplier_text_eq) {
        this.n_supplier_text_eq = n_supplier_text_eq;
        if(!ObjectUtils.isEmpty(this.n_supplier_text_eq)){
            this.getSearchCond().eq("supplier_text", n_supplier_text_eq);
        }
    }
	private String n_supplier_text_like;//[供应商]
	public void setN_supplier_text_like(String n_supplier_text_like) {
        this.n_supplier_text_like = n_supplier_text_like;
        if(!ObjectUtils.isEmpty(this.n_supplier_text_like)){
            this.getSearchCond().like("supplier_text", n_supplier_text_like);
        }
    }
	private String n_user_id_text_eq;//[用户]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[用户]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_category_id_text_eq;//[产品种类]
	public void setN_category_id_text_eq(String n_category_id_text_eq) {
        this.n_category_id_text_eq = n_category_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_text_eq)){
            this.getSearchCond().eq("category_id_text", n_category_id_text_eq);
        }
    }
	private String n_category_id_text_like;//[产品种类]
	public void setN_category_id_text_like(String n_category_id_text_like) {
        this.n_category_id_text_like = n_category_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_category_id_text_like)){
            this.getSearchCond().like("category_id_text", n_category_id_text_like);
        }
    }
	private String n_name_eq;//[产品名称]
	public void setN_name_eq(String n_name_eq) {
        this.n_name_eq = n_name_eq;
        if(!ObjectUtils.isEmpty(this.n_name_eq)){
            this.getSearchCond().eq("name", n_name_eq);
        }
    }
	private String n_name_like;//[产品名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private Long n_user_id_eq;//[用户]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_supplier_eq;//[供应商]
	public void setN_supplier_eq(Long n_supplier_eq) {
        this.n_supplier_eq = n_supplier_eq;
        if(!ObjectUtils.isEmpty(this.n_supplier_eq)){
            this.getSearchCond().eq("supplier", n_supplier_eq);
        }
    }
	private Long n_category_id_eq;//[产品种类]
	public void setN_category_id_eq(Long n_category_id_eq) {
        this.n_category_id_eq = n_category_id_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_eq)){
            this.getSearchCond().eq("category_id", n_category_id_eq);
        }
    }
	private Long n_order_id_eq;//[订单]
	public void setN_order_id_eq(Long n_order_id_eq) {
        this.n_order_id_eq = n_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_eq)){
            this.getSearchCond().eq("order_id", n_order_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



