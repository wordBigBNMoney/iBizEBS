package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_followersSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_followers] 服务对象接口
 */
public interface IMail_followersService extends IService<Mail_followers>{

    boolean create(Mail_followers et) ;
    void createBatch(List<Mail_followers> list) ;
    boolean update(Mail_followers et) ;
    void updateBatch(List<Mail_followers> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_followers get(Long key) ;
    Mail_followers getDraft(Mail_followers et) ;
    boolean checkKey(Mail_followers et) ;
    boolean save(Mail_followers et) ;
    void saveBatch(List<Mail_followers> list) ;
    Page<Mail_followers> searchDefault(Mail_followersSearchContext context) ;
    List<Mail_followers> selectByChannelId(Long id);
    void removeByChannelId(Collection<Long> ids);
    void removeByChannelId(Long id);
    List<Mail_followers> selectByPartnerId(Long id);
    void removeByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


