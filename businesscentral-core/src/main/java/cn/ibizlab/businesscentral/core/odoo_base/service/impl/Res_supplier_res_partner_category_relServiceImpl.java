package cn.ibizlab.businesscentral.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier_res_partner_category_rel;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_supplier_res_partner_category_relSearchContext;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplier_res_partner_category_relService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_base.mapper.Res_supplier_res_partner_category_relMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[供应商标签] 服务对象接口实现
 */
@Slf4j
@Service("Res_supplier_res_partner_category_relServiceImpl")
public class Res_supplier_res_partner_category_relServiceImpl extends EBSServiceImpl<Res_supplier_res_partner_category_relMapper, Res_supplier_res_partner_category_rel> implements IRes_supplier_res_partner_category_relService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_categoryService resPartnerCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService resSupplierService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "res.supplier.res.partner.category.rel" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Res_supplier_res_partner_category_rel et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;

        if (messageinfo && !mail_create_nosubscribe && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack && false) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Res_supplier_res_partner_category_rel> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Res_supplier_res_partner_category_rel et) {
        Res_supplier_res_partner_category_rel old = new Res_supplier_res_partner_category_rel() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_supplier_res_partner_category_relService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_supplier_res_partner_category_relService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Res_supplier_res_partner_category_rel> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Res_supplier_res_partner_category_rel get(String key) {
        Res_supplier_res_partner_category_rel et = getById(key);
        if(et==null){
            et=new Res_supplier_res_partner_category_rel();
        }
        else{
        }
        return et;
    }

    @Override
    public Res_supplier_res_partner_category_rel getDraft(Res_supplier_res_partner_category_rel et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Res_supplier_res_partner_category_rel et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Res_supplier_res_partner_category_rel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Res_supplier_res_partner_category_rel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Res_supplier_res_partner_category_rel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Res_supplier_res_partner_category_rel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Res_supplier_res_partner_category_rel> selectByCategoryId(Long id) {
        return baseMapper.selectByCategoryId(id);
    }
    @Override
    public void removeByCategoryId(Long id) {
        this.remove(new QueryWrapper<Res_supplier_res_partner_category_rel>().eq("category_id",id));
    }

	@Override
    public List<Res_supplier_res_partner_category_rel> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void removeByPartnerId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Res_supplier_res_partner_category_rel>().in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Res_supplier_res_partner_category_rel>().eq("partner_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Res_supplier_res_partner_category_rel> searchDefault(Res_supplier_res_partner_category_relSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_supplier_res_partner_category_rel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_supplier_res_partner_category_rel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Res_supplier_res_partner_category_rel et){
        //实体关系[DER1N_RES_SUPPLIER_RES_PARTNER_CATEGORY_REL_RES_PARTNER_CATEGORY_CATEGORY_ID]
        if(!ObjectUtils.isEmpty(et.getCategoryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_category odooCategory=et.getOdooCategory();
            if(ObjectUtils.isEmpty(odooCategory)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_category majorEntity=resPartnerCategoryService.get(et.getCategoryId());
                et.setOdooCategory(majorEntity);
                odooCategory=majorEntity;
            }
            et.setCategoryName(odooCategory.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_RES_PARTNER_CATEGORY_REL_RES_SUPPLIER_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier odooSupplier=et.getOdooSupplier();
            if(ObjectUtils.isEmpty(odooSupplier)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier majorEntity=resSupplierService.get(et.getPartnerId());
                et.setOdooSupplier(majorEntity);
                odooSupplier=majorEntity;
            }
            et.setPartnerName(odooSupplier.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



