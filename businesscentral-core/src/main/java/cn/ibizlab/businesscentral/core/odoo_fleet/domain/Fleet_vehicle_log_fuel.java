package cn.ibizlab.businesscentral.core.odoo_fleet.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[车辆燃油记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "FLEET_VEHICLE_LOG_FUEL",resultMap = "Fleet_vehicle_log_fuelResultMap")
public class Fleet_vehicle_log_fuel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 每公升价格
     */
    @DEField(name = "price_per_liter")
    @TableField(value = "price_per_liter")
    @JSONField(name = "price_per_liter")
    @JsonProperty("price_per_liter")
    private Double pricePerLiter;
    /**
     * 包括服务
     */
    @TableField(exist = false)
    @JSONField(name = "cost_ids")
    @JsonProperty("cost_ids")
    private String costIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 升
     */
    @TableField(value = "liter")
    @JSONField(name = "liter")
    @JsonProperty("liter")
    private Double liter;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 便签
     */
    @TableField(value = "notes")
    @JSONField(name = "notes")
    @JsonProperty("notes")
    private String notes;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 发票参考
     */
    @DEField(name = "inv_ref")
    @TableField(value = "inv_ref")
    @JSONField(name = "inv_ref")
    @JsonProperty("inv_ref")
    private String invRef;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "cost_subtype_id")
    @JsonProperty("cost_subtype_id")
    private Long costSubtypeId;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 里程表
     */
    @TableField(exist = false)
    @JSONField(name = "odometer_id")
    @JsonProperty("odometer_id")
    private Long odometerId;
    /**
     * 费用所属类别
     */
    @TableField(exist = false)
    @JSONField(name = "cost_type")
    @JsonProperty("cost_type")
    private String costType;
    /**
     * 总价
     */
    @TableField(exist = false)
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 采购
     */
    @TableField(exist = false)
    @JSONField(name = "purchaser_id_text")
    @JsonProperty("purchaser_id_text")
    private String purchaserIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 总额
     */
    @TableField(exist = false)
    @JSONField(name = "cost_amount")
    @JsonProperty("cost_amount")
    private Double costAmount;
    /**
     * 上级
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 车辆
     */
    @TableField(exist = false)
    @JSONField(name = "vehicle_id")
    @JsonProperty("vehicle_id")
    private Long vehicleId;
    /**
     * 自动生成
     */
    @TableField(exist = false)
    @JSONField(name = "auto_generated")
    @JsonProperty("auto_generated")
    private Boolean autoGenerated;
    /**
     * 合同
     */
    @TableField(exist = false)
    @JSONField(name = "contract_id")
    @JsonProperty("contract_id")
    private Long contractId;
    /**
     * 成本说明
     */
    @TableField(exist = false)
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 名称
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "vendor_id_text")
    @JsonProperty("vendor_id_text")
    private String vendorIdText;
    /**
     * 里程表数值
     */
    @TableField(exist = false)
    @JSONField(name = "odometer")
    @JsonProperty("odometer")
    private Double odometer;
    /**
     * 日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "odometer_unit")
    @JsonProperty("odometer_unit")
    private String odometerUnit;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 成本
     */
    @DEField(name = "cost_id")
    @TableField(value = "cost_id")
    @JSONField(name = "cost_id")
    @JsonProperty("cost_id")
    private Long costId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 供应商
     */
    @DEField(name = "vendor_id")
    @TableField(value = "vendor_id")
    @JSONField(name = "vendor_id")
    @JsonProperty("vendor_id")
    private Long vendorId;
    /**
     * 采购
     */
    @DEField(name = "purchaser_id")
    @TableField(value = "purchaser_id")
    @JSONField(name = "purchaser_id")
    @JsonProperty("purchaser_id")
    private Long purchaserId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost odooCost;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPurchaser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooVendor;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [每公升价格]
     */
    public void setPricePerLiter(Double pricePerLiter){
        this.pricePerLiter = pricePerLiter ;
        this.modify("price_per_liter",pricePerLiter);
    }

    /**
     * 设置 [升]
     */
    public void setLiter(Double liter){
        this.liter = liter ;
        this.modify("liter",liter);
    }

    /**
     * 设置 [便签]
     */
    public void setNotes(String notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [发票参考]
     */
    public void setInvRef(String invRef){
        this.invRef = invRef ;
        this.modify("inv_ref",invRef);
    }

    /**
     * 设置 [成本]
     */
    public void setCostId(Long costId){
        this.costId = costId ;
        this.modify("cost_id",costId);
    }

    /**
     * 设置 [供应商]
     */
    public void setVendorId(Long vendorId){
        this.vendorId = vendorId ;
        this.modify("vendor_id",vendorId);
    }

    /**
     * 设置 [采购]
     */
    public void setPurchaserId(Long purchaserId){
        this.purchaserId = purchaserId ;
        this.modify("purchaser_id",purchaserId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


