package cn.ibizlab.businesscentral.core.odoo_asset.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_asset;
/**
 * 关系型数据实体[Asset_asset] 查询条件对象
 */
@Slf4j
@Data
public class Asset_assetSearchContext extends QueryWrapperContext<Asset_asset> {

	private String n_name_like;//[Asset Name]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_criticality_eq;//[Criticality]
	public void setN_criticality_eq(String n_criticality_eq) {
        this.n_criticality_eq = n_criticality_eq;
        if(!ObjectUtils.isEmpty(this.n_criticality_eq)){
            this.getSearchCond().eq("criticality", n_criticality_eq);
        }
    }
	private String n_warehouse_state_id_text_eq;//[省/ 州]
	public void setN_warehouse_state_id_text_eq(String n_warehouse_state_id_text_eq) {
        this.n_warehouse_state_id_text_eq = n_warehouse_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_state_id_text_eq)){
            this.getSearchCond().eq("warehouse_state_id_text", n_warehouse_state_id_text_eq);
        }
    }
	private String n_warehouse_state_id_text_like;//[省/ 州]
	public void setN_warehouse_state_id_text_like(String n_warehouse_state_id_text_like) {
        this.n_warehouse_state_id_text_like = n_warehouse_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_warehouse_state_id_text_like)){
            this.getSearchCond().like("warehouse_state_id_text", n_warehouse_state_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[分派给]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[分派给]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_accounting_state_id_text_eq;//[省/ 州]
	public void setN_accounting_state_id_text_eq(String n_accounting_state_id_text_eq) {
        this.n_accounting_state_id_text_eq = n_accounting_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_accounting_state_id_text_eq)){
            this.getSearchCond().eq("accounting_state_id_text", n_accounting_state_id_text_eq);
        }
    }
	private String n_accounting_state_id_text_like;//[省/ 州]
	public void setN_accounting_state_id_text_like(String n_accounting_state_id_text_like) {
        this.n_accounting_state_id_text_like = n_accounting_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_accounting_state_id_text_like)){
            this.getSearchCond().like("accounting_state_id_text", n_accounting_state_id_text_like);
        }
    }
	private String n_manufacture_state_id_text_eq;//[省/ 州]
	public void setN_manufacture_state_id_text_eq(String n_manufacture_state_id_text_eq) {
        this.n_manufacture_state_id_text_eq = n_manufacture_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_manufacture_state_id_text_eq)){
            this.getSearchCond().eq("manufacture_state_id_text", n_manufacture_state_id_text_eq);
        }
    }
	private String n_manufacture_state_id_text_like;//[省/ 州]
	public void setN_manufacture_state_id_text_like(String n_manufacture_state_id_text_like) {
        this.n_manufacture_state_id_text_like = n_manufacture_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_manufacture_state_id_text_like)){
            this.getSearchCond().like("manufacture_state_id_text", n_manufacture_state_id_text_like);
        }
    }
	private String n_finance_state_id_text_eq;//[省/ 州]
	public void setN_finance_state_id_text_eq(String n_finance_state_id_text_eq) {
        this.n_finance_state_id_text_eq = n_finance_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_finance_state_id_text_eq)){
            this.getSearchCond().eq("finance_state_id_text", n_finance_state_id_text_eq);
        }
    }
	private String n_finance_state_id_text_like;//[省/ 州]
	public void setN_finance_state_id_text_like(String n_finance_state_id_text_like) {
        this.n_finance_state_id_text_like = n_finance_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_finance_state_id_text_like)){
            this.getSearchCond().like("finance_state_id_text", n_finance_state_id_text_like);
        }
    }
	private String n_maintenance_state_id_text_eq;//[省/ 州]
	public void setN_maintenance_state_id_text_eq(String n_maintenance_state_id_text_eq) {
        this.n_maintenance_state_id_text_eq = n_maintenance_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_maintenance_state_id_text_eq)){
            this.getSearchCond().eq("maintenance_state_id_text", n_maintenance_state_id_text_eq);
        }
    }
	private String n_maintenance_state_id_text_like;//[省/ 州]
	public void setN_maintenance_state_id_text_like(String n_maintenance_state_id_text_like) {
        this.n_maintenance_state_id_text_like = n_maintenance_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_maintenance_state_id_text_like)){
            this.getSearchCond().like("maintenance_state_id_text", n_maintenance_state_id_text_like);
        }
    }
	private String n_manufacturer_id_text_eq;//[Manufacturer]
	public void setN_manufacturer_id_text_eq(String n_manufacturer_id_text_eq) {
        this.n_manufacturer_id_text_eq = n_manufacturer_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_manufacturer_id_text_eq)){
            this.getSearchCond().eq("manufacturer_id_text", n_manufacturer_id_text_eq);
        }
    }
	private String n_manufacturer_id_text_like;//[Manufacturer]
	public void setN_manufacturer_id_text_like(String n_manufacturer_id_text_like) {
        this.n_manufacturer_id_text_like = n_manufacturer_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_manufacturer_id_text_like)){
            this.getSearchCond().like("manufacturer_id_text", n_manufacturer_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_vendor_id_text_eq;//[供应商]
	public void setN_vendor_id_text_eq(String n_vendor_id_text_eq) {
        this.n_vendor_id_text_eq = n_vendor_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_id_text_eq)){
            this.getSearchCond().eq("vendor_id_text", n_vendor_id_text_eq);
        }
    }
	private String n_vendor_id_text_like;//[供应商]
	public void setN_vendor_id_text_like(String n_vendor_id_text_like) {
        this.n_vendor_id_text_like = n_vendor_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_vendor_id_text_like)){
            this.getSearchCond().like("vendor_id_text", n_vendor_id_text_like);
        }
    }
	private Long n_manufacture_state_id_eq;//[省/ 州]
	public void setN_manufacture_state_id_eq(Long n_manufacture_state_id_eq) {
        this.n_manufacture_state_id_eq = n_manufacture_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_manufacture_state_id_eq)){
            this.getSearchCond().eq("manufacture_state_id", n_manufacture_state_id_eq);
        }
    }
	private Long n_accounting_state_id_eq;//[省/ 州]
	public void setN_accounting_state_id_eq(Long n_accounting_state_id_eq) {
        this.n_accounting_state_id_eq = n_accounting_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_accounting_state_id_eq)){
            this.getSearchCond().eq("accounting_state_id", n_accounting_state_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_vendor_id_eq;//[供应商]
	public void setN_vendor_id_eq(Long n_vendor_id_eq) {
        this.n_vendor_id_eq = n_vendor_id_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_id_eq)){
            this.getSearchCond().eq("vendor_id", n_vendor_id_eq);
        }
    }
	private Long n_warehouse_state_id_eq;//[省/ 州]
	public void setN_warehouse_state_id_eq(Long n_warehouse_state_id_eq) {
        this.n_warehouse_state_id_eq = n_warehouse_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_state_id_eq)){
            this.getSearchCond().eq("warehouse_state_id", n_warehouse_state_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_user_id_eq;//[分派给]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_finance_state_id_eq;//[省/ 州]
	public void setN_finance_state_id_eq(Long n_finance_state_id_eq) {
        this.n_finance_state_id_eq = n_finance_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_finance_state_id_eq)){
            this.getSearchCond().eq("finance_state_id", n_finance_state_id_eq);
        }
    }
	private Long n_maintenance_state_id_eq;//[省/ 州]
	public void setN_maintenance_state_id_eq(Long n_maintenance_state_id_eq) {
        this.n_maintenance_state_id_eq = n_maintenance_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_maintenance_state_id_eq)){
            this.getSearchCond().eq("maintenance_state_id", n_maintenance_state_id_eq);
        }
    }
	private Long n_manufacturer_id_eq;//[Manufacturer]
	public void setN_manufacturer_id_eq(Long n_manufacturer_id_eq) {
        this.n_manufacturer_id_eq = n_manufacturer_id_eq;
        if(!ObjectUtils.isEmpty(this.n_manufacturer_id_eq)){
            this.getSearchCond().eq("manufacturer_id", n_manufacturer_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



