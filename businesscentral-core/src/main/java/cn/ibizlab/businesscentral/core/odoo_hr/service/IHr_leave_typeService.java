package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leave_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_leave_type] 服务对象接口
 */
public interface IHr_leave_typeService extends IService<Hr_leave_type>{

    boolean create(Hr_leave_type et) ;
    void createBatch(List<Hr_leave_type> list) ;
    boolean update(Hr_leave_type et) ;
    void updateBatch(List<Hr_leave_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_leave_type get(Long key) ;
    Hr_leave_type getDraft(Hr_leave_type et) ;
    boolean checkKey(Hr_leave_type et) ;
    boolean save(Hr_leave_type et) ;
    void saveBatch(List<Hr_leave_type> list) ;
    Page<Hr_leave_type> searchDefault(Hr_leave_typeSearchContext context) ;
    List<Hr_leave_type> selectByCategId(Long id);
    void resetByCategId(Long id);
    void resetByCategId(Collection<Long> ids);
    void removeByCategId(Long id);
    List<Hr_leave_type> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Hr_leave_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_leave_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_leave_type> getHrLeaveTypeByIds(List<Long> ids) ;
    List<Hr_leave_type> getHrLeaveTypeByEntities(List<Hr_leave_type> entities) ;
}


