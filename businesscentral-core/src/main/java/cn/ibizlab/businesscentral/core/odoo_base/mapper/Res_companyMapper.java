package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_companySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Res_companyMapper extends BaseMapper<Res_company>{

    Page<Res_company> searchDefault(IPage page, @Param("srf") Res_companySearchContext context, @Param("ew") Wrapper<Res_company> wrapper) ;
    Page<Res_company> searchROOT(IPage page, @Param("srf") Res_companySearchContext context, @Param("ew") Wrapper<Res_company> wrapper) ;
    @Override
    Res_company selectById(Serializable id);
    @Override
    int insert(Res_company entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Res_company entity);
    @Override
    int update(@Param(Constants.ENTITY) Res_company entity, @Param("ew") Wrapper<Res_company> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Res_company> selectByPropertyStockAccountInputCategId(@Param("id") Serializable id) ;

    List<Res_company> selectByPropertyStockAccountOutputCategId(@Param("id") Serializable id) ;

    List<Res_company> selectByPropertyStockValuationAccountId(@Param("id") Serializable id) ;

    List<Res_company> selectByTransferAccountId(@Param("id") Serializable id) ;

    List<Res_company> selectByChartTemplateId(@Param("id") Serializable id) ;

    List<Res_company> selectByIncotermId(@Param("id") Serializable id) ;

    List<Res_company> selectByCurrencyExchangeJournalId(@Param("id") Serializable id) ;

    List<Res_company> selectByTaxCashBasisJournalId(@Param("id") Serializable id) ;

    List<Res_company> selectByAccountOpeningMoveId(@Param("id") Serializable id) ;

    List<Res_company> selectByAccountPurchaseTaxId(@Param("id") Serializable id) ;

    List<Res_company> selectByAccountSaleTaxId(@Param("id") Serializable id) ;

    List<Res_company> selectByResourceCalendarId(@Param("id") Serializable id) ;

    List<Res_company> selectByParentId(@Param("id") Serializable id) ;

    List<Res_company> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Res_company> selectByPartnerId(@Param("id") Serializable id) ;

    List<Res_company> selectByCreateUid(@Param("id") Serializable id) ;

    List<Res_company> selectByWriteUid(@Param("id") Serializable id) ;

    List<Res_company> selectByInternalTransitLocationId(@Param("id") Serializable id) ;


}
