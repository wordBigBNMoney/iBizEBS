package cn.ibizlab.businesscentral.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_tags;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_tagsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[project_tags] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-project:odoo-project}", contextId = "project-tags", fallback = project_tagsFallback.class)
public interface project_tagsFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/project_tags/search")
    Page<Project_tags> search(@RequestBody Project_tagsSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/project_tags/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/project_tags/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/project_tags/{id}")
    Project_tags update(@PathVariable("id") Long id,@RequestBody Project_tags project_tags);

    @RequestMapping(method = RequestMethod.PUT, value = "/project_tags/batch")
    Boolean updateBatch(@RequestBody List<Project_tags> project_tags);



    @RequestMapping(method = RequestMethod.POST, value = "/project_tags")
    Project_tags create(@RequestBody Project_tags project_tags);

    @RequestMapping(method = RequestMethod.POST, value = "/project_tags/batch")
    Boolean createBatch(@RequestBody List<Project_tags> project_tags);



    @RequestMapping(method = RequestMethod.GET, value = "/project_tags/{id}")
    Project_tags get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.GET, value = "/project_tags/select")
    Page<Project_tags> select();


    @RequestMapping(method = RequestMethod.GET, value = "/project_tags/getdraft")
    Project_tags getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/project_tags/checkkey")
    Boolean checkKey(@RequestBody Project_tags project_tags);


    @RequestMapping(method = RequestMethod.POST, value = "/project_tags/save")
    Boolean save(@RequestBody Project_tags project_tags);

    @RequestMapping(method = RequestMethod.POST, value = "/project_tags/savebatch")
    Boolean saveBatch(@RequestBody List<Project_tags> project_tags);



    @RequestMapping(method = RequestMethod.POST, value = "/project_tags/searchdefault")
    Page<Project_tags> searchDefault(@RequestBody Project_tagsSearchContext context);


}
