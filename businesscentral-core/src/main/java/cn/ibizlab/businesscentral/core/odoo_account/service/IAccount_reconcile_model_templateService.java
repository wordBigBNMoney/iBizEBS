package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_reconcile_model_template] 服务对象接口
 */
public interface IAccount_reconcile_model_templateService extends IService<Account_reconcile_model_template>{

    boolean create(Account_reconcile_model_template et) ;
    void createBatch(List<Account_reconcile_model_template> list) ;
    boolean update(Account_reconcile_model_template et) ;
    void updateBatch(List<Account_reconcile_model_template> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_reconcile_model_template get(Long key) ;
    Account_reconcile_model_template getDraft(Account_reconcile_model_template et) ;
    boolean checkKey(Account_reconcile_model_template et) ;
    boolean save(Account_reconcile_model_template et) ;
    void saveBatch(List<Account_reconcile_model_template> list) ;
    Page<Account_reconcile_model_template> searchDefault(Account_reconcile_model_templateSearchContext context) ;
    List<Account_reconcile_model_template> selectByAccountId(Long id);
    void removeByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_reconcile_model_template> selectBySecondAccountId(Long id);
    void removeBySecondAccountId(Collection<Long> ids);
    void removeBySecondAccountId(Long id);
    List<Account_reconcile_model_template> selectByChartTemplateId(Long id);
    void resetByChartTemplateId(Long id);
    void resetByChartTemplateId(Collection<Long> ids);
    void removeByChartTemplateId(Long id);
    List<Account_reconcile_model_template> selectBySecondTaxId(Long id);
    List<Account_reconcile_model_template> selectBySecondTaxId(Collection<Long> ids);
    void removeBySecondTaxId(Long id);
    List<Account_reconcile_model_template> selectByTaxId(Long id);
    List<Account_reconcile_model_template> selectByTaxId(Collection<Long> ids);
    void removeByTaxId(Long id);
    List<Account_reconcile_model_template> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_reconcile_model_template> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_reconcile_model_template> getAccountReconcileModelTemplateByIds(List<Long> ids) ;
    List<Account_reconcile_model_template> getAccountReconcileModelTemplateByEntities(List<Account_reconcile_model_template> entities) ;
}


