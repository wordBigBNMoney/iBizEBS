package cn.ibizlab.businesscentral.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Fleet_vehicle_cost] 服务对象接口
 */
public interface IFleet_vehicle_costService extends IService<Fleet_vehicle_cost>{

    boolean create(Fleet_vehicle_cost et) ;
    void createBatch(List<Fleet_vehicle_cost> list) ;
    boolean update(Fleet_vehicle_cost et) ;
    void updateBatch(List<Fleet_vehicle_cost> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Fleet_vehicle_cost get(Long key) ;
    Fleet_vehicle_cost getDraft(Fleet_vehicle_cost et) ;
    boolean checkKey(Fleet_vehicle_cost et) ;
    boolean save(Fleet_vehicle_cost et) ;
    void saveBatch(List<Fleet_vehicle_cost> list) ;
    Page<Fleet_vehicle_cost> searchDefault(Fleet_vehicle_costSearchContext context) ;
    List<Fleet_vehicle_cost> selectByCostSubtypeId(Long id);
    void resetByCostSubtypeId(Long id);
    void resetByCostSubtypeId(Collection<Long> ids);
    void removeByCostSubtypeId(Long id);
    List<Fleet_vehicle_cost> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Fleet_vehicle_cost> selectByContractId(Long id);
    void resetByContractId(Long id);
    void resetByContractId(Collection<Long> ids);
    void removeByContractId(Long id);
    List<Fleet_vehicle_cost> selectByOdometerId(Long id);
    void resetByOdometerId(Long id);
    void resetByOdometerId(Collection<Long> ids);
    void removeByOdometerId(Long id);
    List<Fleet_vehicle_cost> selectByVehicleId(Long id);
    void resetByVehicleId(Long id);
    void resetByVehicleId(Collection<Long> ids);
    void removeByVehicleId(Long id);
    List<Fleet_vehicle_cost> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Fleet_vehicle_cost> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Fleet_vehicle_cost> getFleetVehicleCostByIds(List<Long> ids) ;
    List<Fleet_vehicle_cost> getFleetVehicleCostByEntities(List<Fleet_vehicle_cost> entities) ;
}


