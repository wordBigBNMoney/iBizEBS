package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_templateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_template] 服务对象接口
 */
public interface IMail_templateService extends IService<Mail_template>{

    boolean create(Mail_template et) ;
    void createBatch(List<Mail_template> list) ;
    boolean update(Mail_template et) ;
    void updateBatch(List<Mail_template> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_template get(Long key) ;
    Mail_template getDraft(Mail_template et) ;
    boolean checkKey(Mail_template et) ;
    boolean save(Mail_template et) ;
    void saveBatch(List<Mail_template> list) ;
    Page<Mail_template> searchDefault(Mail_templateSearchContext context) ;
    List<Mail_template> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_template> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_template> getMailTemplateByIds(List<Long> ids) ;
    List<Mail_template> getMailTemplateByEntities(List<Mail_template> entities) ;
}


