package cn.ibizlab.businesscentral.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_usersSearchContext;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_base.mapper.Res_usersMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[用户] 服务对象接口实现
 */
@Slf4j
@Service("Res_usersServiceImpl")
public class Res_usersServiceImpl extends EBSServiceImpl<Res_usersMapper, Res_users> implements IRes_usersService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_tagService accountAccountTagService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_templateService accountAccountTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_typeService accountAccountTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_distributionService accountAnalyticDistributionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_groupService accountAnalyticGroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_lineService accountAnalyticLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_tagService accountAnalyticTagService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_cashboxService accountBankStatementCashboxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_closebalanceService accountBankStatementClosebalanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_import_journal_creationService accountBankStatementImportJournalCreationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_importService accountBankStatementImportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_lineService accountBankStatementLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statementService accountBankStatementService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_cashbox_lineService accountCashboxLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_cash_roundingService accountCashRoundingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_chart_templateService accountChartTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_common_journal_reportService accountCommonJournalReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_common_reportService accountCommonReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_financial_year_opService accountFinancialYearOpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_account_templateService accountFiscalPositionAccountTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_accountService accountFiscalPositionAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_tax_templateService accountFiscalPositionTaxTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_taxService accountFiscalPositionTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_templateService accountFiscalPositionTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_yearService accountFiscalYearService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_full_reconcileService accountFullReconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_groupService accountGroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_incotermsService accountIncotermsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_confirmService accountInvoiceConfirmService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_import_wizardService accountInvoiceImportWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService accountInvoiceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_refundService accountInvoiceRefundService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_reportService accountInvoiceReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_sendService accountInvoiceSendService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_taxService accountInvoiceTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_reversalService accountMoveReversalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_partial_reconcileService accountPartialReconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_methodService accountPaymentMethodService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_term_lineService accountPaymentTermLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService accountPaymentTermService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_paymentService accountPaymentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_print_journalService accountPrintJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_model_templateService accountReconcileModelTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_modelService accountReconcileModelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_register_paymentsService accountRegisterPaymentsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_setup_bank_manual_configService accountSetupBankManualConfigService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_tax_groupService accountTaxGroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_tax_templateService accountTaxTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_taxService accountTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_unreconcileService accountUnreconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_asset.service.IAsset_assetService assetAssetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_asset.service.IAsset_categoryService assetCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_asset.service.IAsset_stateService assetStateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_automation_lead_testService baseAutomationLeadTestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_automation_line_testService baseAutomationLineTestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_automationService baseAutomationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_importService baseImportImportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_mappingService baseImportMappingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_char_noreadonlyService baseImportTestsModelsCharNoreadonlyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_char_readonlyService baseImportTestsModelsCharReadonlyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_char_requiredService baseImportTestsModelsCharRequiredService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_char_statesService baseImportTestsModelsCharStatesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_char_stillreadonlyService baseImportTestsModelsCharStillreadonlyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_charService baseImportTestsModelsCharService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_complexService baseImportTestsModelsComplexService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_floatService baseImportTestsModelsFloatService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_m2o_relatedService baseImportTestsModelsM2oRelatedService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_m2o_required_relatedService baseImportTestsModelsM2oRequiredRelatedService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_m2o_requiredService baseImportTestsModelsM2oRequiredService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_m2oService baseImportTestsModelsM2oService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_o2m_childService baseImportTestsModelsO2mChildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_o2mService baseImportTestsModelsO2mService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_previewService baseImportTestsModelsPreviewService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_language_exportService baseLanguageExportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_language_importService baseLanguageImportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_language_installService baseLanguageInstallService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_module_uninstallService baseModuleUninstallService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_module_updateService baseModuleUpdateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_module_upgradeService baseModuleUpgradeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_partner_merge_automatic_wizardService basePartnerMergeAutomaticWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_partner_merge_lineService basePartnerMergeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_update_translationsService baseUpdateTranslationsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_bus.service.IBus_busService busBusService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_bus.service.IBus_presenceService busPresenceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_alarmService calendarAlarmService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_attendeeService calendarAttendeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_contactsService calendarContactsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_event_typeService calendarEventTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_eventService calendarEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_activity_reportService crmActivityReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partner_massService crmLead2opportunityPartnerMassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partnerService crmLead2opportunityPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead_lostService crmLeadLostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead_tagService crmLeadTagService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_leadService crmLeadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lost_reasonService crmLostReasonService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_merge_opportunityService crmMergeOpportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_partner_bindingService crmPartnerBindingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_stageService crmStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IDelivery_carrierService deliveryCarrierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_digest.service.IDigest_digestService digestDigestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_digest.service.IDigest_tipService digestTipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_confirmService eventConfirmService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_event_ticketService eventEventTicketService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_eventService eventEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_mail_registrationService eventMailRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_mailService eventMailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService eventRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_type_mailService eventTypeMailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_typeService eventTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fetchmail.service.IFetchmail_serverService fetchmailServerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_service_typeService fleetServiceTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_assignation_logService fleetVehicleAssignationLogService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_costService fleetVehicleCostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_contractService fleetVehicleLogContractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_fuelService fleetVehicleLogFuelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_servicesService fleetVehicleLogServicesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_model_brandService fleetVehicleModelBrandService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_modelService fleetVehicleModelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_odometerService fleetVehicleOdometerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_stateService fleetVehicleStateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_tagService fleetVehicleTagService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicleService fleetVehicleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badge_user_wizardService gamificationBadgeUserWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badge_userService gamificationBadgeUserService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badgeService gamificationBadgeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challenge_lineService gamificationChallengeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challengeService gamificationChallengeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goal_definitionService gamificationGoalDefinitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goal_wizardService gamificationGoalWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goalService gamificationGoalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicant_categoryService hrApplicantCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService hrApplicantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_attendanceService hrAttendanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contractService hrContractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contract_typeService hrContractTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employee_categoryService hrEmployeeCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employee_skillService hrEmployeeSkillService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_refuse_wizardService hrExpenseRefuseWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheet_register_payment_wizardService hrExpenseSheetRegisterPaymentWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expenseService hrExpenseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_holidays_summary_deptService hrHolidaysSummaryDeptService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_holidays_summary_employeeService hrHolidaysSummaryEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_allocationService hrLeaveAllocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_typeService hrLeaveTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leaveService hrLeaveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_degreeService hrRecruitmentDegreeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_sourceService hrRecruitmentSourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_stageService hrRecruitmentStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_resume_lineService hrResumeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_resume_line_typeService hrResumeLineTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_skill_levelService hrSkillLevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_skillService hrSkillService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_skill_typeService hrSkillTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_iap.service.IIap_accountService iapAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_channel_ruleService imLivechatChannelRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_channelService imLivechatChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequence_date_rangeService irSequenceDateRangeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequenceService irSequenceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_alertService lunchAlertService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_cashmoveService lunchCashmoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_order_line_luckyService lunchOrderLineLuckyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_order_lineService lunchOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_orderService lunchOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_product_categoryService lunchProductCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_productService lunchProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activity_typeService mailActivityTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activityService mailActivityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_aliasService mailAliasService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_blacklistService mailBlacklistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channel_partnerService mailChannelPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channelService mailChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_compose_messageService mailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mail_statisticsService mailMailStatisticsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mailService mailMailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_campaignService mailMassMailingCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_contactService mailMassMailingContactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_list_contact_relService mailMassMailingListContactRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_listService mailMassMailingListService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_stageService mailMassMailingStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_tagService mailMassMailingTagService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_testService mailMassMailingTestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailingService mailMassMailingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_message_subtypeService mailMessageSubtypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService mailMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_moderationService mailModerationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_resend_cancelService mailResendCancelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_resend_messageService mailResendMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_resend_partnerService mailResendPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_shortcodeService mailShortcodeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService mailTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_tracking_valueService mailTrackingValueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_wizard_inviteService mailWizardInviteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipment_categoryService maintenanceEquipmentCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipmentService maintenanceEquipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_requestService maintenanceRequestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_stageService maintenanceStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_teamService maintenanceTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_convert_orderService mroConvertOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_order_parts_lineService mroOrderPartsLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_orderService mroOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meter_intervalService mroPmMeterIntervalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meter_lineService mroPmMeterLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meter_ratioService mroPmMeterRatioService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meterService mroPmMeterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_parameterService mroPmParameterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_replanService mroPmReplanService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_rule_lineService mroPmRuleLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_ruleService mroPmRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_request_rejectService mroRequestRejectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_requestService mroRequestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_task_parts_lineService mroTaskPartsLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_taskService mroTaskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_workorderService mroWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bom_lineService mrpBomLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService mrpBomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_documentService mrpDocumentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produce_lineService mrpProductProduceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produceService mrpProductProduceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routing_workcenterService mrpRoutingWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routingService mrpRoutingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService mrpUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivity_loss_typeService mrpWorkcenterProductivityLossTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivity_lossService mrpWorkcenterProductivityLossService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivityService mrpWorkcenterProductivityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenterService mrpWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workorderService mrpWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_note.service.INote_noteService noteNoteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_note.service.INote_stageService noteStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_note.service.INote_tagService noteTagService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_acquirer_onboarding_wizardService paymentAcquirerOnboardingWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_acquirerService paymentAcquirerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_iconService paymentIconService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_tokenService paymentTokenService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_transactionService paymentTransactionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_portal.service.IPortal_shareService portalShareService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_portal.service.IPortal_wizard_userService portalWizardUserService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_portal.service.IPortal_wizardService portalWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_attribute_custom_valueService productAttributeCustomValueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_attribute_valueService productAttributeValueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_attributeService productAttributeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_categoryService productCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_imageService productImageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_packagingService productPackagingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelist_itemService productPricelistItemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService productPricelistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_price_historyService productPriceHistoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_price_listService productPriceListService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_public_categoryService productPublicCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_putawayService productPutawayService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_removalService productRemovalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_replenishService productReplenishService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_styleService productStyleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_supplierinfoService productSupplierinfoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_exclusionService productTemplateAttributeExclusionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_lineService productTemplateAttributeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_valueService productTemplateAttributeValueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_projectService projectProjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_tagsService projectTagsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_task_typeService projectTaskTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_taskService projectTaskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_reportService purchaseReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService purchaseRequisitionLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService purchaseRequisitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_typeService purchaseRequisitionTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_rating.service.IRating_ratingService ratingRatingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_cancelService repairCancelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_feeService repairFeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService repairLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_order_make_invoiceService repairOrderMakeInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_attendanceService resourceCalendarAttendanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_leavesService resourceCalendarLeavesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendarService resourceCalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_resourceService resourceResourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_testService resourceTestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_bankService resBankService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_config_installerService resConfigInstallerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_config_settingsService resConfigSettingsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_configService resConfigService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_country_groupService resCountryGroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_country_stateService resCountryStateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currency_rateService resCurrencyRateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_groupsService resGroupsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_langService resLangService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_autocomplete_syncService resPartnerAutocompleteSyncService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_bankService resPartnerBankService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_categoryService resPartnerCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_industryService resPartnerIndustryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_titleService resPartnerTitleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService resSupplierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_users_logService resUsersLogService;

    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_advance_payment_invService saleAdvancePaymentInvService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_optionService saleOrderOptionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_template_lineService saleOrderTemplateLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_template_optionService saleOrderTemplateOptionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_templateService saleOrderTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_payment_acquirer_onboarding_wizardService salePaymentAcquirerOnboardingWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_product_configuratorService saleProductConfiguratorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService saleReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sms.service.ISms_send_smsService smsSendSmsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_snailmail.service.ISnailmail_letterService snailmailLetterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_backorder_confirmationService stockBackorderConfirmationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_change_product_qtyService stockChangeProductQtyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_change_standard_priceService stockChangeStandardPriceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_fixed_putaway_stratService stockFixedPutawayStratService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_immediate_transferService stockImmediateTransferService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventory_lineService stockInventoryLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventoryService stockInventoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_location_routeService stockLocationRouteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_overprocessed_transferService stockOverprocessedTransferService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_destinationService stockPackageDestinationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_levelService stockPackageLevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService stockProductionLotService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantity_historyService stockQuantityHistoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quant_packageService stockQuantPackageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantService stockQuantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_picking_lineService stockReturnPickingLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_pickingService stockReturnPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_rules_reportService stockRulesReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService stockRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scheduler_computeService stockSchedulerComputeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_traceability_reportService stockTraceabilityReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_track_confirmationService stockTrackConfirmationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_track_lineService stockTrackLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouse_orderpointService stockWarehouseOrderpointService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_repairService stockWarnInsufficientQtyRepairService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_scrapService stockWarnInsufficientQtyScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_unbuildService stockWarnInsufficientQtyUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_labelService surveyLabelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_mail_compose_messageService surveyMailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_pageService surveyPageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_questionService surveyQuestionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_stageService surveyStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_surveyService surveySurveyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_input_lineService surveyUserInputLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_inputService surveyUserInputService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_categoryService uomCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_campaignService utmCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mediumService utmMediumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService utmSourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_menuService websiteMenuService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_pageService websitePageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_redirectService websiteRedirectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_sale_payment_acquirer_onboarding_wizardService websiteSalePaymentAcquirerOnboardingWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_web_editor.service.IWeb_editor_converter_test_subService webEditorConverterTestSubService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_web_editor.service.IWeb_editor_converter_testService webEditorConverterTestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_web_tour.service.IWeb_tour_tourService webTourTourService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "res.users" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Res_users et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_usersService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Res_users> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Res_users et) {
        Res_users old = new Res_users() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_usersService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_usersService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Res_users> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountAnalyticLineService.resetByUserId(key);
        accountBankStatementService.resetByUserId(key);
        accountInvoiceService.resetByUserId(key);
        assetAssetService.resetByUserId(key);
        baseAutomationLeadTestService.resetByUserId(key);
        baseAutomationLineTestService.resetByUserId(key);
        busPresenceService.removeByUserId(key);
        calendarContactsService.resetByUserId(key);
        calendarEventService.resetByUserId(key);
        crmLead2opportunityPartnerMassService.resetByUserId(key);
        crmLead2opportunityPartnerService.resetByUserId(key);
        crmLeadService.resetByUserId(key);
        crmMergeOpportunityService.resetByUserId(key);
        crmTeamService.resetByUserId(key);
        eventEventService.resetByUserId(key);
        fleetVehicleLogContractService.resetByUserId(key);
        gamificationBadgeUserService.resetBySenderId(key);
        gamificationBadgeUserService.removeByUserId(key);
        gamificationChallengeService.resetByManagerId(key);
        gamificationGoalService.removeByUserId(key);
        hrApplicantService.resetByUserId(key);
        hrEmployeeService.resetByExpenseManagerId(key);
        hrEmployeeService.resetByUserId(key);
        hrExpenseSheetService.resetByUserId(key);
        hrJobService.resetByHrResponsibleId(key);
        hrJobService.resetByUserId(key);
        hrLeaveService.resetByUserId(key);
        lunchCashmoveService.resetByUserId(key);
        lunchOrderLineService.resetByUserId(key);
        lunchOrderService.resetByUserId(key);
        mailActivityService.resetByUserId(key);
        mailAliasService.resetByAliasUserId(key);
        mailComposeMessageService.resetByModeratorId(key);
        mailMassMailingCampaignService.resetByUserId(key);
        mailMassMailingService.resetByUserId(key);
        mailMessageService.resetByModeratorId(key);
        maintenanceEquipmentCategoryService.resetByTechnicianUserId(key);
        maintenanceEquipmentService.resetByOwnerUserId(key);
        maintenanceEquipmentService.resetByTechnicianUserId(key);
        maintenanceRequestService.resetByOwnerUserId(key);
        maintenanceRequestService.resetByUserId(key);
        mroOrderService.resetByUserId(key);
        mroWorkorderService.resetByUserId(key);
        mrpProductionService.resetByUserId(key);
        mrpWorkcenterProductivityService.resetByUserId(key);
        noteNoteService.resetByUserId(key);
        noteStageService.removeByUserId(key);
        portalWizardUserService.resetByUserId(key);
        projectProjectService.resetByUserId(key);
        projectTaskService.resetByUserId(key);
        purchaseOrderService.resetByUserId(key);
        purchaseReportService.resetByUserId(key);
        resourceResourceService.resetByUserId(key);
        resConfigSettingsService.resetByAuthSignupTemplateUserId(key);
        resPartnerService.resetByUserId(key);
        saleOrderLineService.resetBySalesmanId(key);
        saleOrderService.resetByUserId(key);
        saleReportService.resetByUserId(key);
        snailmailLetterService.resetByUserId(key);
        surveyMailComposeMessageService.resetByModeratorId(key);
        webTourTourService.resetByUserId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountAnalyticLineService.resetByUserId(idList);
        accountBankStatementService.resetByUserId(idList);
        accountInvoiceService.resetByUserId(idList);
        assetAssetService.resetByUserId(idList);
        baseAutomationLeadTestService.resetByUserId(idList);
        baseAutomationLineTestService.resetByUserId(idList);
        busPresenceService.removeByUserId(idList);
        calendarContactsService.resetByUserId(idList);
        calendarEventService.resetByUserId(idList);
        crmLead2opportunityPartnerMassService.resetByUserId(idList);
        crmLead2opportunityPartnerService.resetByUserId(idList);
        crmLeadService.resetByUserId(idList);
        crmMergeOpportunityService.resetByUserId(idList);
        crmTeamService.resetByUserId(idList);
        eventEventService.resetByUserId(idList);
        fleetVehicleLogContractService.resetByUserId(idList);
        gamificationBadgeUserService.resetBySenderId(idList);
        gamificationBadgeUserService.removeByUserId(idList);
        gamificationChallengeService.resetByManagerId(idList);
        gamificationGoalService.removeByUserId(idList);
        hrApplicantService.resetByUserId(idList);
        hrEmployeeService.resetByExpenseManagerId(idList);
        hrEmployeeService.resetByUserId(idList);
        hrExpenseSheetService.resetByUserId(idList);
        hrJobService.resetByHrResponsibleId(idList);
        hrJobService.resetByUserId(idList);
        hrLeaveService.resetByUserId(idList);
        lunchCashmoveService.resetByUserId(idList);
        lunchOrderLineService.resetByUserId(idList);
        lunchOrderService.resetByUserId(idList);
        mailActivityService.resetByUserId(idList);
        mailAliasService.resetByAliasUserId(idList);
        mailComposeMessageService.resetByModeratorId(idList);
        mailMassMailingCampaignService.resetByUserId(idList);
        mailMassMailingService.resetByUserId(idList);
        mailMessageService.resetByModeratorId(idList);
        maintenanceEquipmentCategoryService.resetByTechnicianUserId(idList);
        maintenanceEquipmentService.resetByOwnerUserId(idList);
        maintenanceEquipmentService.resetByTechnicianUserId(idList);
        maintenanceRequestService.resetByOwnerUserId(idList);
        maintenanceRequestService.resetByUserId(idList);
        mroOrderService.resetByUserId(idList);
        mroWorkorderService.resetByUserId(idList);
        mrpProductionService.resetByUserId(idList);
        mrpWorkcenterProductivityService.resetByUserId(idList);
        noteNoteService.resetByUserId(idList);
        noteStageService.removeByUserId(idList);
        portalWizardUserService.resetByUserId(idList);
        projectProjectService.resetByUserId(idList);
        projectTaskService.resetByUserId(idList);
        purchaseOrderService.resetByUserId(idList);
        purchaseReportService.resetByUserId(idList);
        resourceResourceService.resetByUserId(idList);
        resConfigSettingsService.resetByAuthSignupTemplateUserId(idList);
        resPartnerService.resetByUserId(idList);
        saleOrderLineService.resetBySalesmanId(idList);
        saleOrderService.resetByUserId(idList);
        saleReportService.resetByUserId(idList);
        snailmailLetterService.resetByUserId(idList);
        surveyMailComposeMessageService.resetByModeratorId(idList);
        webTourTourService.resetByUserId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Res_users get(Long key) {
        Res_users et = getById(key);
        if(et==null){
            et=new Res_users();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Res_users getDraft(Res_users et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Res_users a(Res_users et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Res_users et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Res_users et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Res_users et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Res_users> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Res_users> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Res_users> selectBySaleTeamId(Long id) {
        return baseMapper.selectBySaleTeamId(id);
    }
    @Override
    public void resetBySaleTeamId(Long id) {
        this.update(new UpdateWrapper<Res_users>().set("sale_team_id",null).eq("sale_team_id",id));
    }

    @Override
    public void resetBySaleTeamId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_users>().set("sale_team_id",null).in("sale_team_id",ids));
    }

    @Override
    public void removeBySaleTeamId(Long id) {
        this.remove(new QueryWrapper<Res_users>().eq("sale_team_id",id));
    }

	@Override
    public List<Res_users> selectByAliasId(Long id) {
        return baseMapper.selectByAliasId(id);
    }
    @Override
    public void resetByAliasId(Long id) {
        this.update(new UpdateWrapper<Res_users>().set("alias_id",null).eq("alias_id",id));
    }

    @Override
    public void resetByAliasId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_users>().set("alias_id",null).in("alias_id",ids));
    }

    @Override
    public void removeByAliasId(Long id) {
        this.remove(new QueryWrapper<Res_users>().eq("alias_id",id));
    }

	@Override
    public List<Res_users> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Res_users>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_users>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Res_users>().eq("company_id",id));
    }

	@Override
    public List<Res_users> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public List<Res_users> selectByPartnerId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Res_users>().in("id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Res_users>().eq("partner_id",id));
    }

	@Override
    public List<Res_users> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Res_users>().eq("create_uid",id));
    }

	@Override
    public List<Res_users> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Res_users>().eq("write_uid",id));
    }


    /**
     * 查询集合 有效用户
     */
    @Override
    public Page<Res_users> searchActive(Res_usersSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_users> pages=baseMapper.searchActive(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_users>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Res_users> searchDefault(Res_usersSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_users> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_users>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Res_users et){
        //实体关系[DER1N_RES_USERS__CRM_TEAM__SALE_TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getSaleTeamId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooSaleTeam=et.getOdooSaleTeam();
            if(ObjectUtils.isEmpty(odooSaleTeam)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team majorEntity=crmTeamService.get(et.getSaleTeamId());
                et.setOdooSaleTeam(majorEntity);
                odooSaleTeam=majorEntity;
            }
            et.setSaleTeamIdText(odooSaleTeam.getName());
        }
        //实体关系[DER1N_RES_USERS__MAIL_ALIAS__ALIAS_ID]
        if(!ObjectUtils.isEmpty(et.getAliasId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias=et.getOdooAlias();
            if(ObjectUtils.isEmpty(odooAlias)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias majorEntity=mailAliasService.get(et.getAliasId());
                et.setOdooAlias(majorEntity);
                odooAlias=majorEntity;
            }
            et.setAliasContact(odooAlias.getAliasContact());
        }
        //实体关系[DER1N_RES_USERS__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_RES_USERS__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setRef(odooPartner.getRef());
            et.setMessageHasErrorCounter(odooPartner.getMessageHasErrorCounter());
            et.setLastWebsiteSoId(odooPartner.getLastWebsiteSoId());
            et.setDate(odooPartner.getDate());
            et.setMessageIsFollower(odooPartner.getMessageIsFollower());
            et.setPropertyStockCustomer(odooPartner.getPropertyStockCustomer());
            et.setLastTimeEntriesChecked(odooPartner.getLastTimeEntriesChecked());
            et.setLang(odooPartner.getLang());
            et.setSaleWarn(odooPartner.getSaleWarn());
            et.setMeetingCount(odooPartner.getMeetingCount());
            et.setStreet(odooPartner.getStreet());
            et.setInvoiceWarn(odooPartner.getInvoiceWarn());
            et.setSignupToken(odooPartner.getSignupToken());
            et.setTaskCount(odooPartner.getTaskCount());
            et.setSignupValid(odooPartner.getSignupValid());
            et.setMessageUnreadCounter(odooPartner.getMessageUnreadCounter());
            et.setSignupType(odooPartner.getSignupType());
            et.setPropertyAccountReceivableId(odooPartner.getPropertyAccountReceivableId());
            et.setWebsiteMetaOgImg(odooPartner.getWebsiteMetaOgImg());
            et.setEventCount(odooPartner.getEventCount());
            et.setJournalItemCount(odooPartner.getJournalItemCount());
            et.setParentName(odooPartner.getParentName());
            et.setCredit(odooPartner.getCredit());
            et.setOpportunityCount(odooPartner.getOpportunityCount());
            et.setSignupUrl(odooPartner.getSignupUrl());
            et.setPropertyAccountPayableId(odooPartner.getPropertyAccountPayableId());
            et.setStateId(odooPartner.getStateId());
            et.setMessageHasError(odooPartner.getMessageHasError());
            et.setSaleWarnMsg(odooPartner.getSaleWarnMsg());
            et.setWebsitePublished(odooPartner.getWebsitePublished());
            et.setTotalInvoiced(odooPartner.getTotalInvoiced());
            et.setDebit(odooPartner.getDebit());
            et.setPosOrderCount(odooPartner.getPosOrderCount());
            et.setTitle(odooPartner.getTitle());
            et.setMessageUnread(odooPartner.getMessageUnread());
            et.setSupplierInvoiceCount(odooPartner.getSupplierInvoiceCount());
            et.setCity(odooPartner.getCity());
            et.setPickingWarn(odooPartner.getPickingWarn());
            et.setMessageMainAttachmentId(odooPartner.getMessageMainAttachmentId());
            et.setActivityUserId(odooPartner.getActivityUserId());
            et.setAdditionalInfo(odooPartner.getAdditionalInfo());
            et.setFunction(odooPartner.getFunction());
            et.setWebsiteUrl(odooPartner.getWebsiteUrl());
            et.setCalendarLastNotifAck(odooPartner.getCalendarLastNotifAck());
            et.setMessageNeedactionCounter(odooPartner.getMessageNeedactionCounter());
            et.setActivityDateDeadline(odooPartner.getActivityDateDeadline());
            et.setEmployee(odooPartner.getEmployee());
            et.setActivityState(odooPartner.getActivityState());
            et.setBarcode(odooPartner.getBarcode());
            et.setPartnerGid(odooPartner.getPartnerGid());
            et.setType(odooPartner.getType());
            et.setVat(odooPartner.getVat());
            et.setPurchaseWarnMsg(odooPartner.getPurchaseWarnMsg());
            et.setComment(odooPartner.getComment());
            et.setWebsiteMetaKeywords(odooPartner.getWebsiteMetaKeywords());
            et.setParentId(odooPartner.getParentId());
            et.setPurchaseWarn(odooPartner.getPurchaseWarn());
            et.setActivePartner(odooPartner.getActive());
            et.setCurrencyId(odooPartner.getCurrencyId());
            et.setIndustryId(odooPartner.getIndustryId());
            et.setPropertyStockSupplier(odooPartner.getPropertyStockSupplier());
            et.setPaymentTokenCount(odooPartner.getPaymentTokenCount());
            et.setMessageNeedaction(odooPartner.getMessageNeedaction());
            et.setUserId(odooPartner.getUserId());
            et.setPropertyPaymentTermId(odooPartner.getPropertyPaymentTermId());
            et.setActivityTypeId(odooPartner.getActivityTypeId());
            et.setContractsCount(odooPartner.getContractsCount());
            et.setSelf(odooPartner.getSelf());
            et.setWebsiteMetaDescription(odooPartner.getWebsiteMetaDescription());
            et.setImage(odooPartner.getImage());
            et.setEmail(odooPartner.getEmail());
            et.setImageMedium(odooPartner.getImageMedium());
            et.setActivitySummary(odooPartner.getActivitySummary());
            et.setDebitLimit(odooPartner.getDebitLimit());
            et.setCountryId(odooPartner.getCountryId());
            et.setCreditLimit(odooPartner.getCreditLimit());
            et.setCommercialCompanyName(odooPartner.getCommercialCompanyName());
            et.setInvoiceWarnMsg(odooPartner.getInvoiceWarnMsg());
            et.setIsPublished(odooPartner.getIsPublished());
            et.setTrust(odooPartner.getTrust());
            et.setMobile(odooPartner.getMobile());
            et.setEmailFormatted(odooPartner.getEmailFormatted());
            et.setIsCompany(odooPartner.getIsCompany());
            et.setTeamId(odooPartner.getTeamId());
            et.setIsBlacklisted(odooPartner.getIsBlacklisted());
            et.setBankAccountCount(odooPartner.getBankAccountCount());
            et.setPropertyProductPricelist(odooPartner.getPropertyProductPricelist());
            et.setName(odooPartner.getName());
            et.setPickingWarnMsg(odooPartner.getPickingWarnMsg());
            et.setMessageAttachmentCount(odooPartner.getMessageAttachmentCount());
            et.setWebsite(odooPartner.getWebsite());
            et.setPhone(odooPartner.getPhone());
            et.setStreet2(odooPartner.getStreet2());
            et.setHasUnreconciledEntries(odooPartner.getHasUnreconciledEntries());
            et.setSignupExpiration(odooPartner.getSignupExpiration());
            et.setTz(odooPartner.getTz());
            et.setContactAddress(odooPartner.getContactAddress());
            et.setWebsiteShortDescription(odooPartner.getWebsiteShortDescription());
            et.setPartnerShare(odooPartner.getPartnerShare());
            et.setColor(odooPartner.getColor());
            et.setZip(odooPartner.getZip());
            et.setSaleOrderCount(odooPartner.getSaleOrderCount());
            et.setCompanyType(odooPartner.getCompanyType());
            et.setPropertyAccountPositionId(odooPartner.getPropertyAccountPositionId());
            et.setIsSeoOptimized(odooPartner.getIsSeoOptimized());
            et.setMessageBounce(odooPartner.getMessageBounce());
            et.setWebsiteMetaTitle(odooPartner.getWebsiteMetaTitle());
            et.setImageSmall(odooPartner.getImageSmall());
            et.setPropertyPurchaseCurrencyId(odooPartner.getPropertyPurchaseCurrencyId());
            et.setPurchaseOrderCount(odooPartner.getPurchaseOrderCount());
            et.setWebsiteDescription(odooPartner.getWebsiteDescription());
            et.setPropertySupplierPaymentTermId(odooPartner.getPropertySupplierPaymentTermId());
            et.setCommercialPartnerId(odooPartner.getCommercialPartnerId());
            et.setCompanyName(odooPartner.getCompanyName());
        }
        //实体关系[DER1N_RES_USERS__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_RES_USERS__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Res_users> getResUsersByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Res_users> getResUsersByEntities(List<Res_users> entities) {
        List ids =new ArrayList();
        for(Res_users entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



