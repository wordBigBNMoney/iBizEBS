package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_holidays_summary_employee] 服务对象接口
 */
public interface IHr_holidays_summary_employeeService extends IService<Hr_holidays_summary_employee>{

    boolean create(Hr_holidays_summary_employee et) ;
    void createBatch(List<Hr_holidays_summary_employee> list) ;
    boolean update(Hr_holidays_summary_employee et) ;
    void updateBatch(List<Hr_holidays_summary_employee> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_holidays_summary_employee get(Long key) ;
    Hr_holidays_summary_employee getDraft(Hr_holidays_summary_employee et) ;
    boolean checkKey(Hr_holidays_summary_employee et) ;
    boolean save(Hr_holidays_summary_employee et) ;
    void saveBatch(List<Hr_holidays_summary_employee> list) ;
    Page<Hr_holidays_summary_employee> searchDefault(Hr_holidays_summary_employeeSearchContext context) ;
    List<Hr_holidays_summary_employee> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_holidays_summary_employee> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_holidays_summary_employee> getHrHolidaysSummaryEmployeeByIds(List<Long> ids) ;
    List<Hr_holidays_summary_employee> getHrHolidaysSummaryEmployeeByEntities(List<Hr_holidays_summary_employee> entities) ;
}


