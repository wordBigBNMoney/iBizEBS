package cn.ibizlab.businesscentral.core.odoo_hr.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_skill;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_skillSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Hr_skillMapper extends BaseMapper<Hr_skill>{

    Page<Hr_skill> searchDefault(IPage page, @Param("srf") Hr_skillSearchContext context, @Param("ew") Wrapper<Hr_skill> wrapper) ;
    @Override
    Hr_skill selectById(Serializable id);
    @Override
    int insert(Hr_skill entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Hr_skill entity);
    @Override
    int update(@Param(Constants.ENTITY) Hr_skill entity, @Param("ew") Wrapper<Hr_skill> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Hr_skill> selectBySkillTypeId(@Param("id") Serializable id) ;

    List<Hr_skill> selectByCreateUid(@Param("id") Serializable id) ;

    List<Hr_skill> selectByWriteUid(@Param("id") Serializable id) ;


}
