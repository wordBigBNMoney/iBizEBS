package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[群发邮件订阅信息]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_MASS_MAILING_LIST_CONTACT_REL",resultMap = "Mail_mass_mailing_list_contact_relResultMap")
public class Mail_mass_mailing_list_contact_rel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 取消订阅日期
     */
    @DEField(name = "unsubscription_date")
    @TableField(value = "unsubscription_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "unsubscription_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("unsubscription_date")
    private Timestamp unsubscriptionDate;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 退出
     */
    @DEField(name = "opt_out")
    @TableField(value = "opt_out")
    @JSONField(name = "opt_out")
    @JsonProperty("opt_out")
    private Boolean optOut;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 被退回
     */
    @TableField(exist = false)
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;
    /**
     * 黑名单
     */
    @TableField(exist = false)
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private Boolean isBlacklisted;
    /**
     * 邮件列表
     */
    @TableField(exist = false)
    @JSONField(name = "list_id_text")
    @JsonProperty("list_id_text")
    private String listIdText;
    /**
     * 联系
     */
    @TableField(exist = false)
    @JSONField(name = "contact_id_text")
    @JsonProperty("contact_id_text")
    private String contactIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 联系人人数
     */
    @TableField(exist = false)
    @JSONField(name = "contact_count")
    @JsonProperty("contact_count")
    private Integer contactCount;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 联系
     */
    @DEField(name = "contact_id")
    @TableField(value = "contact_id")
    @JSONField(name = "contact_id")
    @JsonProperty("contact_id")
    private Long contactId;
    /**
     * 邮件列表
     */
    @DEField(name = "list_id")
    @TableField(value = "list_id")
    @JSONField(name = "list_id")
    @JsonProperty("list_id")
    private Long listId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_contact odooContact;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list odooList;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [取消订阅日期]
     */
    public void setUnsubscriptionDate(Timestamp unsubscriptionDate){
        this.unsubscriptionDate = unsubscriptionDate ;
        this.modify("unsubscription_date",unsubscriptionDate);
    }

    /**
     * 格式化日期 [取消订阅日期]
     */
    public String formatUnsubscriptionDate(){
        if (this.unsubscriptionDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(unsubscriptionDate);
    }
    /**
     * 设置 [退出]
     */
    public void setOptOut(Boolean optOut){
        this.optOut = optOut ;
        this.modify("opt_out",optOut);
    }

    /**
     * 设置 [联系]
     */
    public void setContactId(Long contactId){
        this.contactId = contactId ;
        this.modify("contact_id",contactId);
    }

    /**
     * 设置 [邮件列表]
     */
    public void setListId(Long listId){
        this.listId = listId ;
        this.modify("list_id",listId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


