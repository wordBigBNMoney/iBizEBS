package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel;
/**
 * 关系型数据实体[Mail_channel] 查询条件对象
 */
@Slf4j
@Data
public class Mail_channelSearchContext extends QueryWrapperContext<Mail_channel> {

	private String n_ibizpublic_eq;//[隐私]
	public void setN_ibizpublic_eq(String n_ibizpublic_eq) {
        this.n_ibizpublic_eq = n_ibizpublic_eq;
        if(!ObjectUtils.isEmpty(this.n_ibizpublic_eq)){
            this.getSearchCond().eq("ibizpublic", n_ibizpublic_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_channel_type_eq;//[渠道类型]
	public void setN_channel_type_eq(String n_channel_type_eq) {
        this.n_channel_type_eq = n_channel_type_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_type_eq)){
            this.getSearchCond().eq("channel_type", n_channel_type_eq);
        }
    }
	private String n_group_public_id_text_eq;//[经授权的群组]
	public void setN_group_public_id_text_eq(String n_group_public_id_text_eq) {
        this.n_group_public_id_text_eq = n_group_public_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_group_public_id_text_eq)){
            this.getSearchCond().eq("group_public_id_text", n_group_public_id_text_eq);
        }
    }
	private String n_group_public_id_text_like;//[经授权的群组]
	public void setN_group_public_id_text_like(String n_group_public_id_text_like) {
        this.n_group_public_id_text_like = n_group_public_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_group_public_id_text_like)){
            this.getSearchCond().like("group_public_id_text", n_group_public_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_livechat_channel_id_text_eq;//[渠道]
	public void setN_livechat_channel_id_text_eq(String n_livechat_channel_id_text_eq) {
        this.n_livechat_channel_id_text_eq = n_livechat_channel_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_livechat_channel_id_text_eq)){
            this.getSearchCond().eq("livechat_channel_id_text", n_livechat_channel_id_text_eq);
        }
    }
	private String n_livechat_channel_id_text_like;//[渠道]
	public void setN_livechat_channel_id_text_like(String n_livechat_channel_id_text_like) {
        this.n_livechat_channel_id_text_like = n_livechat_channel_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_livechat_channel_id_text_like)){
            this.getSearchCond().like("livechat_channel_id_text", n_livechat_channel_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private Long n_group_public_id_eq;//[经授权的群组]
	public void setN_group_public_id_eq(Long n_group_public_id_eq) {
        this.n_group_public_id_eq = n_group_public_id_eq;
        if(!ObjectUtils.isEmpty(this.n_group_public_id_eq)){
            this.getSearchCond().eq("group_public_id", n_group_public_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_alias_id_eq;//[别名]
	public void setN_alias_id_eq(Long n_alias_id_eq) {
        this.n_alias_id_eq = n_alias_id_eq;
        if(!ObjectUtils.isEmpty(this.n_alias_id_eq)){
            this.getSearchCond().eq("alias_id", n_alias_id_eq);
        }
    }
	private Long n_livechat_channel_id_eq;//[渠道]
	public void setN_livechat_channel_id_eq(Long n_livechat_channel_id_eq) {
        this.n_livechat_channel_id_eq = n_livechat_channel_id_eq;
        if(!ObjectUtils.isEmpty(this.n_livechat_channel_id_eq)){
            this.getSearchCond().eq("livechat_channel_id", n_livechat_channel_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



