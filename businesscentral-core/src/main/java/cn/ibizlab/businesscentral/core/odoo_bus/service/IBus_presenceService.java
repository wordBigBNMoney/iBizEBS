package cn.ibizlab.businesscentral.core.odoo_bus.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.businesscentral.core.odoo_bus.filter.Bus_presenceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Bus_presence] 服务对象接口
 */
public interface IBus_presenceService extends IService<Bus_presence>{

    boolean create(Bus_presence et) ;
    void createBatch(List<Bus_presence> list) ;
    boolean update(Bus_presence et) ;
    void updateBatch(List<Bus_presence> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Bus_presence get(Long key) ;
    Bus_presence getDraft(Bus_presence et) ;
    boolean checkKey(Bus_presence et) ;
    boolean save(Bus_presence et) ;
    void saveBatch(List<Bus_presence> list) ;
    Page<Bus_presence> searchDefault(Bus_presenceSearchContext context) ;
    List<Bus_presence> selectByUserId(Long id);
    void removeByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


