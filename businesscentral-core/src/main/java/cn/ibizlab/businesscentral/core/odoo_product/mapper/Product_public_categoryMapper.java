package cn.ibizlab.businesscentral.core.odoo_product.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_public_categorySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Product_public_categoryMapper extends BaseMapper<Product_public_category>{

    Page<Product_public_category> searchDefault(IPage page, @Param("srf") Product_public_categorySearchContext context, @Param("ew") Wrapper<Product_public_category> wrapper) ;
    @Override
    Product_public_category selectById(Serializable id);
    @Override
    int insert(Product_public_category entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Product_public_category entity);
    @Override
    int update(@Param(Constants.ENTITY) Product_public_category entity, @Param("ew") Wrapper<Product_public_category> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Product_public_category> selectByParentId(@Param("id") Serializable id) ;

    List<Product_public_category> selectByCreateUid(@Param("id") Serializable id) ;

    List<Product_public_category> selectByWriteUid(@Param("id") Serializable id) ;


}
