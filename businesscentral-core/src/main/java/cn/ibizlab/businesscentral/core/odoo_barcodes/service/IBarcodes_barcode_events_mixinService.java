package cn.ibizlab.businesscentral.core.odoo_barcodes.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.businesscentral.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Barcodes_barcode_events_mixin] 服务对象接口
 */
public interface IBarcodes_barcode_events_mixinService extends IService<Barcodes_barcode_events_mixin>{

    boolean create(Barcodes_barcode_events_mixin et) ;
    void createBatch(List<Barcodes_barcode_events_mixin> list) ;
    boolean update(Barcodes_barcode_events_mixin et) ;
    void updateBatch(List<Barcodes_barcode_events_mixin> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Barcodes_barcode_events_mixin get(Long key) ;
    Barcodes_barcode_events_mixin getDraft(Barcodes_barcode_events_mixin et) ;
    boolean checkKey(Barcodes_barcode_events_mixin et) ;
    boolean save(Barcodes_barcode_events_mixin et) ;
    void saveBatch(List<Barcodes_barcode_events_mixin> list) ;
    Page<Barcodes_barcode_events_mixin> searchDefault(Barcodes_barcode_events_mixinSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


