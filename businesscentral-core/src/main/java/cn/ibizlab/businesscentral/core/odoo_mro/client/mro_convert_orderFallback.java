package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_convert_orderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_convert_order] 服务对象接口
 */
@Component
public class mro_convert_orderFallback implements mro_convert_orderFeignClient{

    public Mro_convert_order create(Mro_convert_order mro_convert_order){
            return null;
     }
    public Boolean createBatch(List<Mro_convert_order> mro_convert_orders){
            return false;
     }


    public Mro_convert_order get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Mro_convert_order update(Long id, Mro_convert_order mro_convert_order){
            return null;
     }
    public Boolean updateBatch(List<Mro_convert_order> mro_convert_orders){
            return false;
     }


    public Page<Mro_convert_order> search(Mro_convert_orderSearchContext context){
            return null;
     }




    public Page<Mro_convert_order> select(){
            return null;
     }

    public Mro_convert_order getDraft(){
            return null;
    }



    public Boolean checkKey(Mro_convert_order mro_convert_order){
            return false;
     }


    public Boolean save(Mro_convert_order mro_convert_order){
            return false;
     }
    public Boolean saveBatch(List<Mro_convert_order> mro_convert_orders){
            return false;
     }

    public Page<Mro_convert_order> searchDefault(Mro_convert_orderSearchContext context){
            return null;
     }


}
