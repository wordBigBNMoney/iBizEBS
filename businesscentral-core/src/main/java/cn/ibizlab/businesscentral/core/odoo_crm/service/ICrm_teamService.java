package cn.ibizlab.businesscentral.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_teamSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Crm_team] 服务对象接口
 */
public interface ICrm_teamService extends IService<Crm_team>{

    boolean create(Crm_team et) ;
    void createBatch(List<Crm_team> list) ;
    boolean update(Crm_team et) ;
    void updateBatch(List<Crm_team> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Crm_team get(Long key) ;
    Crm_team getDraft(Crm_team et) ;
    boolean checkKey(Crm_team et) ;
    boolean save(Crm_team et) ;
    void saveBatch(List<Crm_team> list) ;
    Page<Crm_team> searchDefault(Crm_teamSearchContext context) ;
    List<Crm_team> selectByAliasId(Long id);
    List<Crm_team> selectByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    List<Crm_team> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Crm_team> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Crm_team> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Crm_team> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Crm_team> getCrmTeamByIds(List<Long> ids) ;
    List<Crm_team> getCrmTeamByEntities(List<Crm_team> entities) ;
}


