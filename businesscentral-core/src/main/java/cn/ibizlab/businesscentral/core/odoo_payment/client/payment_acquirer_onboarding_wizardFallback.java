package cn.ibizlab.businesscentral.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Component
public class payment_acquirer_onboarding_wizardFallback implements payment_acquirer_onboarding_wizardFeignClient{


    public Payment_acquirer_onboarding_wizard create(Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
            return null;
     }
    public Boolean createBatch(List<Payment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
            return false;
     }

    public Page<Payment_acquirer_onboarding_wizard> search(Payment_acquirer_onboarding_wizardSearchContext context){
            return null;
     }


    public Payment_acquirer_onboarding_wizard update(Long id, Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
            return null;
     }
    public Boolean updateBatch(List<Payment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
            return false;
     }



    public Payment_acquirer_onboarding_wizard get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Payment_acquirer_onboarding_wizard> select(){
            return null;
     }

    public Payment_acquirer_onboarding_wizard getDraft(){
            return null;
    }



    public Boolean checkKey(Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
            return false;
     }


    public Boolean save(Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
            return false;
     }
    public Boolean saveBatch(List<Payment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
            return false;
     }

    public Page<Payment_acquirer_onboarding_wizard> searchDefault(Payment_acquirer_onboarding_wizardSearchContext context){
            return null;
     }


}
