package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_pricelist_itemSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_pricelist_item] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-pricelist-item", fallback = product_pricelist_itemFallback.class)
public interface product_pricelist_itemFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelist_items/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelist_items/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_pricelist_items/{id}")
    Product_pricelist_item update(@PathVariable("id") Long id,@RequestBody Product_pricelist_item product_pricelist_item);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_pricelist_items/batch")
    Boolean updateBatch(@RequestBody List<Product_pricelist_item> product_pricelist_items);


    @RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/{id}")
    Product_pricelist_item get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items")
    Product_pricelist_item create(@RequestBody Product_pricelist_item product_pricelist_item);

    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/batch")
    Boolean createBatch(@RequestBody List<Product_pricelist_item> product_pricelist_items);




    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/search")
    Page<Product_pricelist_item> search(@RequestBody Product_pricelist_itemSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/select")
    Page<Product_pricelist_item> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/getdraft")
    Product_pricelist_item getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/checkkey")
    Boolean checkKey(@RequestBody Product_pricelist_item product_pricelist_item);


    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/save")
    Boolean save(@RequestBody Product_pricelist_item product_pricelist_item);

    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/savebatch")
    Boolean saveBatch(@RequestBody List<Product_pricelist_item> product_pricelist_items);



    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/searchdefault")
    Page<Product_pricelist_item> searchDefault(@RequestBody Product_pricelist_itemSearchContext context);


}
