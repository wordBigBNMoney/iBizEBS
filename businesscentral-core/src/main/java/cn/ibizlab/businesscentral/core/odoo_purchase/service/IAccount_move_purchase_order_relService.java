package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Account_move_purchase_order_rel;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Account_move_purchase_order_relSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_move_purchase_order_rel] 服务对象接口
 */
public interface IAccount_move_purchase_order_relService extends IService<Account_move_purchase_order_rel>{

    boolean create(Account_move_purchase_order_rel et) ;
    void createBatch(List<Account_move_purchase_order_rel> list) ;
    boolean update(Account_move_purchase_order_rel et) ;
    void updateBatch(List<Account_move_purchase_order_rel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Account_move_purchase_order_rel get(String key) ;
    Account_move_purchase_order_rel getDraft(Account_move_purchase_order_rel et) ;
    boolean checkKey(Account_move_purchase_order_rel et) ;
    boolean save(Account_move_purchase_order_rel et) ;
    void saveBatch(List<Account_move_purchase_order_rel> list) ;
    Page<Account_move_purchase_order_rel> searchDefault(Account_move_purchase_order_relSearchContext context) ;
    List<Account_move_purchase_order_rel> selectByAccountMoveId(Long id);
    void removeByAccountMoveId(Long id);
    List<Account_move_purchase_order_rel> selectByPurchaseOrderId(Long id);
    void removeByPurchaseOrderId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


