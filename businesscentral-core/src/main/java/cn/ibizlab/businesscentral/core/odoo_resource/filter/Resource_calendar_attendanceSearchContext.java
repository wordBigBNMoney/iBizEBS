package cn.ibizlab.businesscentral.core.odoo_resource.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_attendance;
/**
 * 关系型数据实体[Resource_calendar_attendance] 查询条件对象
 */
@Slf4j
@Data
public class Resource_calendar_attendanceSearchContext extends QueryWrapperContext<Resource_calendar_attendance> {

	private String n_dayofweek_eq;//[星期]
	public void setN_dayofweek_eq(String n_dayofweek_eq) {
        this.n_dayofweek_eq = n_dayofweek_eq;
        if(!ObjectUtils.isEmpty(this.n_dayofweek_eq)){
            this.getSearchCond().eq("dayofweek", n_dayofweek_eq);
        }
    }
	private String n_day_period_eq;//[日期]
	public void setN_day_period_eq(String n_day_period_eq) {
        this.n_day_period_eq = n_day_period_eq;
        if(!ObjectUtils.isEmpty(this.n_day_period_eq)){
            this.getSearchCond().eq("day_period", n_day_period_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_calendar_id_text_eq;//[资源的日历]
	public void setN_calendar_id_text_eq(String n_calendar_id_text_eq) {
        this.n_calendar_id_text_eq = n_calendar_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_calendar_id_text_eq)){
            this.getSearchCond().eq("calendar_id_text", n_calendar_id_text_eq);
        }
    }
	private String n_calendar_id_text_like;//[资源的日历]
	public void setN_calendar_id_text_like(String n_calendar_id_text_like) {
        this.n_calendar_id_text_like = n_calendar_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_calendar_id_text_like)){
            this.getSearchCond().like("calendar_id_text", n_calendar_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_calendar_id_eq;//[资源的日历]
	public void setN_calendar_id_eq(Long n_calendar_id_eq) {
        this.n_calendar_id_eq = n_calendar_id_eq;
        if(!ObjectUtils.isEmpty(this.n_calendar_id_eq)){
            this.getSearchCond().eq("calendar_id", n_calendar_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



