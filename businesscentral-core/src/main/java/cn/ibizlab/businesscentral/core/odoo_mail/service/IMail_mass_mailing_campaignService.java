package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_mass_mailing_campaign] 服务对象接口
 */
public interface IMail_mass_mailing_campaignService extends IService<Mail_mass_mailing_campaign>{

    boolean create(Mail_mass_mailing_campaign et) ;
    void createBatch(List<Mail_mass_mailing_campaign> list) ;
    boolean update(Mail_mass_mailing_campaign et) ;
    void updateBatch(List<Mail_mass_mailing_campaign> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_mass_mailing_campaign get(Long key) ;
    Mail_mass_mailing_campaign getDraft(Mail_mass_mailing_campaign et) ;
    boolean checkKey(Mail_mass_mailing_campaign et) ;
    boolean save(Mail_mass_mailing_campaign et) ;
    void saveBatch(List<Mail_mass_mailing_campaign> list) ;
    Page<Mail_mass_mailing_campaign> searchDefault(Mail_mass_mailing_campaignSearchContext context) ;
    List<Mail_mass_mailing_campaign> selectByStageId(Long id);
    List<Mail_mass_mailing_campaign> selectByStageId(Collection<Long> ids);
    void removeByStageId(Long id);
    List<Mail_mass_mailing_campaign> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_mass_mailing_campaign> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Mail_mass_mailing_campaign> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mail_mass_mailing_campaign> selectByCampaignId(Long id);
    void removeByCampaignId(Collection<Long> ids);
    void removeByCampaignId(Long id);
    List<Mail_mass_mailing_campaign> selectByMediumId(Long id);
    void resetByMediumId(Long id);
    void resetByMediumId(Collection<Long> ids);
    void removeByMediumId(Long id);
    List<Mail_mass_mailing_campaign> selectBySourceId(Long id);
    void resetBySourceId(Long id);
    void resetBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_mass_mailing_campaign> getMailMassMailingCampaignByIds(List<Long> ids) ;
    List<Mail_mass_mailing_campaign> getMailMassMailingCampaignByEntities(List<Mail_mass_mailing_campaign> entities) ;
}


