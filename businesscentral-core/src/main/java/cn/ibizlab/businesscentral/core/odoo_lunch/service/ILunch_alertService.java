package cn.ibizlab.businesscentral.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_alertSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Lunch_alert] 服务对象接口
 */
public interface ILunch_alertService extends IService<Lunch_alert>{

    boolean create(Lunch_alert et) ;
    void createBatch(List<Lunch_alert> list) ;
    boolean update(Lunch_alert et) ;
    void updateBatch(List<Lunch_alert> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Lunch_alert get(Long key) ;
    Lunch_alert getDraft(Lunch_alert et) ;
    boolean checkKey(Lunch_alert et) ;
    boolean save(Lunch_alert et) ;
    void saveBatch(List<Lunch_alert> list) ;
    Page<Lunch_alert> searchDefault(Lunch_alertSearchContext context) ;
    List<Lunch_alert> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Lunch_alert> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Lunch_alert> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Lunch_alert> getLunchAlertByIds(List<Long> ids) ;
    List<Lunch_alert> getLunchAlertByEntities(List<Lunch_alert> entities) ;
}


