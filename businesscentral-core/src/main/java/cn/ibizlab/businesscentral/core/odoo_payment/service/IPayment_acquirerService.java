package cn.ibizlab.businesscentral.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirerSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Payment_acquirer] 服务对象接口
 */
public interface IPayment_acquirerService extends IService<Payment_acquirer>{

    boolean create(Payment_acquirer et) ;
    void createBatch(List<Payment_acquirer> list) ;
    boolean update(Payment_acquirer et) ;
    void updateBatch(List<Payment_acquirer> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Payment_acquirer get(Long key) ;
    Payment_acquirer getDraft(Payment_acquirer et) ;
    boolean checkKey(Payment_acquirer et) ;
    boolean save(Payment_acquirer et) ;
    void saveBatch(List<Payment_acquirer> list) ;
    Page<Payment_acquirer> searchDefault(Payment_acquirerSearchContext context) ;
    List<Payment_acquirer> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Payment_acquirer> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Payment_acquirer> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Payment_acquirer> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Payment_acquirer> getPaymentAcquirerByIds(List<Long> ids) ;
    List<Payment_acquirer> getPaymentAcquirerByEntities(List<Payment_acquirer> entities) ;
}


