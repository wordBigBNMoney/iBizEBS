package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_sheetSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_expense_sheet] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-expense-sheet", fallback = hr_expense_sheetFallback.class)
public interface hr_expense_sheetFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets")
    Hr_expense_sheet create(@RequestBody Hr_expense_sheet hr_expense_sheet);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/batch")
    Boolean createBatch(@RequestBody List<Hr_expense_sheet> hr_expense_sheets);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/search")
    Page<Hr_expense_sheet> search(@RequestBody Hr_expense_sheetSearchContext context);




    @RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheets/{id}")
    Hr_expense_sheet update(@PathVariable("id") Long id,@RequestBody Hr_expense_sheet hr_expense_sheet);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheets/batch")
    Boolean updateBatch(@RequestBody List<Hr_expense_sheet> hr_expense_sheets);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheets/{id}")
    Hr_expense_sheet get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheets/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheets/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheets/select")
    Page<Hr_expense_sheet> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheets/getdraft")
    Hr_expense_sheet getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/checkkey")
    Boolean checkKey(@RequestBody Hr_expense_sheet hr_expense_sheet);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/save")
    Boolean save(@RequestBody Hr_expense_sheet hr_expense_sheet);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_expense_sheet> hr_expense_sheets);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/searchdefault")
    Page<Hr_expense_sheet> searchDefault(@RequestBody Hr_expense_sheetSearchContext context);


}
