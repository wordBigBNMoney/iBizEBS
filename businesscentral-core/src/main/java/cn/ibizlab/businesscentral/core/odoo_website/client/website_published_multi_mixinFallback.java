package cn.ibizlab.businesscentral.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_published_multi_mixin;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_published_multi_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_published_multi_mixin] 服务对象接口
 */
@Component
public class website_published_multi_mixinFallback implements website_published_multi_mixinFeignClient{


    public Website_published_multi_mixin get(Long id){
            return null;
     }


    public Page<Website_published_multi_mixin> search(Website_published_multi_mixinSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Website_published_multi_mixin create(Website_published_multi_mixin website_published_multi_mixin){
            return null;
     }
    public Boolean createBatch(List<Website_published_multi_mixin> website_published_multi_mixins){
            return false;
     }

    public Website_published_multi_mixin update(Long id, Website_published_multi_mixin website_published_multi_mixin){
            return null;
     }
    public Boolean updateBatch(List<Website_published_multi_mixin> website_published_multi_mixins){
            return false;
     }



    public Page<Website_published_multi_mixin> select(){
            return null;
     }

    public Website_published_multi_mixin getDraft(){
            return null;
    }



    public Boolean checkKey(Website_published_multi_mixin website_published_multi_mixin){
            return false;
     }


    public Boolean save(Website_published_multi_mixin website_published_multi_mixin){
            return false;
     }
    public Boolean saveBatch(List<Website_published_multi_mixin> website_published_multi_mixins){
            return false;
     }

    public Page<Website_published_multi_mixin> searchDefault(Website_published_multi_mixinSearchContext context){
            return null;
     }


}
