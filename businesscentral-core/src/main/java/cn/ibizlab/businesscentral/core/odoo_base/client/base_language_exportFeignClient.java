package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_exportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_language_export] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "base-language-export", fallback = base_language_exportFallback.class)
public interface base_language_exportFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/base_language_exports")
    Base_language_export create(@RequestBody Base_language_export base_language_export);

    @RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/batch")
    Boolean createBatch(@RequestBody List<Base_language_export> base_language_exports);




    @RequestMapping(method = RequestMethod.GET, value = "/base_language_exports/{id}")
    Base_language_export get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_language_exports/{id}")
    Base_language_export update(@PathVariable("id") Long id,@RequestBody Base_language_export base_language_export);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_language_exports/batch")
    Boolean updateBatch(@RequestBody List<Base_language_export> base_language_exports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_language_exports/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_language_exports/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/search")
    Page<Base_language_export> search(@RequestBody Base_language_exportSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/base_language_exports/select")
    Page<Base_language_export> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_language_exports/getdraft")
    Base_language_export getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/checkkey")
    Boolean checkKey(@RequestBody Base_language_export base_language_export);


    @RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/save")
    Boolean save(@RequestBody Base_language_export base_language_export);

    @RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/savebatch")
    Boolean saveBatch(@RequestBody List<Base_language_export> base_language_exports);



    @RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/searchdefault")
    Page<Base_language_export> searchDefault(@RequestBody Base_language_exportSearchContext context);


}
