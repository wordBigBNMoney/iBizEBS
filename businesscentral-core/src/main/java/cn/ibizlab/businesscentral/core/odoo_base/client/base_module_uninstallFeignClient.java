package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_module_uninstallSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_module_uninstall] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "base-module-uninstall", fallback = base_module_uninstallFallback.class)
public interface base_module_uninstallFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/base_module_uninstalls/{id}")
    Base_module_uninstall update(@PathVariable("id") Long id,@RequestBody Base_module_uninstall base_module_uninstall);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_module_uninstalls/batch")
    Boolean updateBatch(@RequestBody List<Base_module_uninstall> base_module_uninstalls);




    @RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/{id}")
    Base_module_uninstall get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/search")
    Page<Base_module_uninstall> search(@RequestBody Base_module_uninstallSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_module_uninstalls/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_module_uninstalls/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls")
    Base_module_uninstall create(@RequestBody Base_module_uninstall base_module_uninstall);

    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/batch")
    Boolean createBatch(@RequestBody List<Base_module_uninstall> base_module_uninstalls);


    @RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/select")
    Page<Base_module_uninstall> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/getdraft")
    Base_module_uninstall getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/checkkey")
    Boolean checkKey(@RequestBody Base_module_uninstall base_module_uninstall);


    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/save")
    Boolean save(@RequestBody Base_module_uninstall base_module_uninstall);

    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/savebatch")
    Boolean saveBatch(@RequestBody List<Base_module_uninstall> base_module_uninstalls);



    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/searchdefault")
    Page<Base_module_uninstall> searchDefault(@RequestBody Base_module_uninstallSearchContext context);


}
