package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation_line_test;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_automation_line_testSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_automation_line_test] 服务对象接口
 */
@Component
public class base_automation_line_testFallback implements base_automation_line_testFeignClient{



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Base_automation_line_test get(Long id){
            return null;
     }


    public Base_automation_line_test create(Base_automation_line_test base_automation_line_test){
            return null;
     }
    public Boolean createBatch(List<Base_automation_line_test> base_automation_line_tests){
            return false;
     }

    public Base_automation_line_test update(Long id, Base_automation_line_test base_automation_line_test){
            return null;
     }
    public Boolean updateBatch(List<Base_automation_line_test> base_automation_line_tests){
            return false;
     }


    public Page<Base_automation_line_test> search(Base_automation_line_testSearchContext context){
            return null;
     }


    public Page<Base_automation_line_test> select(){
            return null;
     }

    public Base_automation_line_test getDraft(){
            return null;
    }



    public Boolean checkKey(Base_automation_line_test base_automation_line_test){
            return false;
     }


    public Boolean save(Base_automation_line_test base_automation_line_test){
            return false;
     }
    public Boolean saveBatch(List<Base_automation_line_test> base_automation_line_tests){
            return false;
     }

    public Page<Base_automation_line_test> searchDefault(Base_automation_line_testSearchContext context){
            return null;
     }


}
