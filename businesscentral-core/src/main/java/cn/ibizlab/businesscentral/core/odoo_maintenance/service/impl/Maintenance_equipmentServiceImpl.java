package cn.ibizlab.businesscentral.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;
import cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipmentService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_maintenance.mapper.Maintenance_equipmentMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[保养设备] 服务对象接口实现
 */
@Slf4j
@Service("Maintenance_equipmentServiceImpl")
public class Maintenance_equipmentServiceImpl extends EBSServiceImpl<Maintenance_equipmentMapper, Maintenance_equipment> implements IMaintenance_equipmentService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_requestService maintenanceRequestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipment_categoryService maintenanceEquipmentCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_teamService maintenanceTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "maintenance.equipment" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Maintenance_equipment et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMaintenance_equipmentService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Maintenance_equipment> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Maintenance_equipment et) {
        Maintenance_equipment old = new Maintenance_equipment() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMaintenance_equipmentService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMaintenance_equipmentService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Maintenance_equipment> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        if(!ObjectUtils.isEmpty(maintenanceRequestService.selectByEquipmentId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Maintenance_request]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        if(!ObjectUtils.isEmpty(maintenanceRequestService.selectByEquipmentId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Maintenance_request]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Maintenance_equipment get(Long key) {
        Maintenance_equipment et = getById(key);
        if(et==null){
            et=new Maintenance_equipment();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Maintenance_equipment getDraft(Maintenance_equipment et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Maintenance_equipment et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Maintenance_equipment et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Maintenance_equipment et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Maintenance_equipment> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Maintenance_equipment> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Maintenance_equipment> selectByDepartmentId(Long id) {
        return baseMapper.selectByDepartmentId(id);
    }
    @Override
    public void resetByDepartmentId(Long id) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("department_id",null).eq("department_id",id));
    }

    @Override
    public void resetByDepartmentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("department_id",null).in("department_id",ids));
    }

    @Override
    public void removeByDepartmentId(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("department_id",id));
    }

	@Override
    public List<Maintenance_equipment> selectByEmployeeId(Long id) {
        return baseMapper.selectByEmployeeId(id);
    }
    @Override
    public void resetByEmployeeId(Long id) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("employee_id",null).eq("employee_id",id));
    }

    @Override
    public void resetByEmployeeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("employee_id",null).in("employee_id",ids));
    }

    @Override
    public void removeByEmployeeId(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("employee_id",id));
    }

	@Override
    public List<Maintenance_equipment> selectByCategoryId(Long id) {
        return baseMapper.selectByCategoryId(id);
    }
    @Override
    public void resetByCategoryId(Long id) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("category_id",null).eq("category_id",id));
    }

    @Override
    public void resetByCategoryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("category_id",null).in("category_id",ids));
    }

    @Override
    public void removeByCategoryId(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("category_id",id));
    }

	@Override
    public List<Maintenance_equipment> selectByMaintenanceTeamId(Long id) {
        return baseMapper.selectByMaintenanceTeamId(id);
    }
    @Override
    public void resetByMaintenanceTeamId(Long id) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("maintenance_team_id",null).eq("maintenance_team_id",id));
    }

    @Override
    public void resetByMaintenanceTeamId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("maintenance_team_id",null).in("maintenance_team_id",ids));
    }

    @Override
    public void removeByMaintenanceTeamId(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("maintenance_team_id",id));
    }

	@Override
    public List<Maintenance_equipment> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("company_id",id));
    }

	@Override
    public List<Maintenance_equipment> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("partner_id",id));
    }

	@Override
    public List<Maintenance_equipment> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("create_uid",id));
    }

	@Override
    public List<Maintenance_equipment> selectByOwnerUserId(Long id) {
        return baseMapper.selectByOwnerUserId(id);
    }
    @Override
    public void resetByOwnerUserId(Long id) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("owner_user_id",null).eq("owner_user_id",id));
    }

    @Override
    public void resetByOwnerUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("owner_user_id",null).in("owner_user_id",ids));
    }

    @Override
    public void removeByOwnerUserId(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("owner_user_id",id));
    }

	@Override
    public List<Maintenance_equipment> selectByTechnicianUserId(Long id) {
        return baseMapper.selectByTechnicianUserId(id);
    }
    @Override
    public void resetByTechnicianUserId(Long id) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("technician_user_id",null).eq("technician_user_id",id));
    }

    @Override
    public void resetByTechnicianUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_equipment>().set("technician_user_id",null).in("technician_user_id",ids));
    }

    @Override
    public void removeByTechnicianUserId(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("technician_user_id",id));
    }

	@Override
    public List<Maintenance_equipment> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Maintenance_equipment>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Maintenance_equipment> searchDefault(Maintenance_equipmentSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Maintenance_equipment> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Maintenance_equipment>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Maintenance_equipment et){
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__HR_DEPARTMENT__DEPARTMENT_ID]
        if(!ObjectUtils.isEmpty(et.getDepartmentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment=et.getOdooDepartment();
            if(ObjectUtils.isEmpty(odooDepartment)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department majorEntity=hrDepartmentService.get(et.getDepartmentId());
                et.setOdooDepartment(majorEntity);
                odooDepartment=majorEntity;
            }
            et.setDepartmentIdText(odooDepartment.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__HR_EMPLOYEE__EMPLOYEE_ID]
        if(!ObjectUtils.isEmpty(et.getEmployeeId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee=et.getOdooEmployee();
            if(ObjectUtils.isEmpty(odooEmployee)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getEmployeeId());
                et.setOdooEmployee(majorEntity);
                odooEmployee=majorEntity;
            }
            et.setEmployeeIdText(odooEmployee.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__MAINTENANCE_EQUIPMENT_CATEGORY__CATEGORY_ID]
        if(!ObjectUtils.isEmpty(et.getCategoryId())){
            cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category odooCategory=et.getOdooCategory();
            if(ObjectUtils.isEmpty(odooCategory)){
                cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category majorEntity=maintenanceEquipmentCategoryService.get(et.getCategoryId());
                et.setOdooCategory(majorEntity);
                odooCategory=majorEntity;
            }
            et.setCategoryIdText(odooCategory.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__MAINTENANCE_TEAM__MAINTENANCE_TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getMaintenanceTeamId())){
            cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team odooMaintenanceTeam=et.getOdooMaintenanceTeam();
            if(ObjectUtils.isEmpty(odooMaintenanceTeam)){
                cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team majorEntity=maintenanceTeamService.get(et.getMaintenanceTeamId());
                et.setOdooMaintenanceTeam(majorEntity);
                odooMaintenanceTeam=majorEntity;
            }
            et.setMaintenanceTeamIdText(odooMaintenanceTeam.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__RES_USERS__OWNER_USER_ID]
        if(!ObjectUtils.isEmpty(et.getOwnerUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooOwnerUser=et.getOdooOwnerUser();
            if(ObjectUtils.isEmpty(odooOwnerUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getOwnerUserId());
                et.setOdooOwnerUser(majorEntity);
                odooOwnerUser=majorEntity;
            }
            et.setOwnerUserIdText(odooOwnerUser.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__RES_USERS__TECHNICIAN_USER_ID]
        if(!ObjectUtils.isEmpty(et.getTechnicianUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooTechnicianUser=et.getOdooTechnicianUser();
            if(ObjectUtils.isEmpty(odooTechnicianUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getTechnicianUserId());
                et.setOdooTechnicianUser(majorEntity);
                odooTechnicianUser=majorEntity;
            }
            et.setTechnicianUserIdText(odooTechnicianUser.getName());
        }
        //实体关系[DER1N_MAINTENANCE_EQUIPMENT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Maintenance_equipment> getMaintenanceEquipmentByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Maintenance_equipment> getMaintenanceEquipmentByEntities(List<Maintenance_equipment> entities) {
        List ids =new ArrayList();
        for(Maintenance_equipment entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



