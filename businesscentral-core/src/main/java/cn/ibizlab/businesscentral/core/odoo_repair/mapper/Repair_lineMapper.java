package cn.ibizlab.businesscentral.core.odoo_repair.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_lineSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Repair_lineMapper extends BaseMapper<Repair_line>{

    Page<Repair_line> searchDefault(IPage page, @Param("srf") Repair_lineSearchContext context, @Param("ew") Wrapper<Repair_line> wrapper) ;
    @Override
    Repair_line selectById(Serializable id);
    @Override
    int insert(Repair_line entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Repair_line entity);
    @Override
    int update(@Param(Constants.ENTITY) Repair_line entity, @Param("ew") Wrapper<Repair_line> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Repair_line> selectByInvoiceLineId(@Param("id") Serializable id) ;

    List<Repair_line> selectByProductId(@Param("id") Serializable id) ;

    List<Repair_line> selectByRepairId(@Param("id") Serializable id) ;

    List<Repair_line> selectByCreateUid(@Param("id") Serializable id) ;

    List<Repair_line> selectByWriteUid(@Param("id") Serializable id) ;

    List<Repair_line> selectByLocationDestId(@Param("id") Serializable id) ;

    List<Repair_line> selectByLocationId(@Param("id") Serializable id) ;

    List<Repair_line> selectByMoveId(@Param("id") Serializable id) ;

    List<Repair_line> selectByLotId(@Param("id") Serializable id) ;

    List<Repair_line> selectByProductUom(@Param("id") Serializable id) ;


}
