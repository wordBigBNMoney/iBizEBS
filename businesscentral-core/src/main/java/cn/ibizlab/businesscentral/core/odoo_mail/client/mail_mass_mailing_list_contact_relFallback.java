package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mass_mailing_list_contact_rel] 服务对象接口
 */
@Component
public class mail_mass_mailing_list_contact_relFallback implements mail_mass_mailing_list_contact_relFeignClient{

    public Page<Mail_mass_mailing_list_contact_rel> search(Mail_mass_mailing_list_contact_relSearchContext context){
            return null;
     }


    public Mail_mass_mailing_list_contact_rel create(Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
            return null;
     }
    public Boolean createBatch(List<Mail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Mail_mass_mailing_list_contact_rel update(Long id, Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
            return null;
     }
    public Boolean updateBatch(List<Mail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
            return false;
     }



    public Mail_mass_mailing_list_contact_rel get(Long id){
            return null;
     }


    public Page<Mail_mass_mailing_list_contact_rel> select(){
            return null;
     }

    public Mail_mass_mailing_list_contact_rel getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
            return false;
     }


    public Boolean save(Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
            return false;
     }
    public Boolean saveBatch(List<Mail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
            return false;
     }

    public Page<Mail_mass_mailing_list_contact_rel> searchDefault(Mail_mass_mailing_list_contact_relSearchContext context){
            return null;
     }


}
