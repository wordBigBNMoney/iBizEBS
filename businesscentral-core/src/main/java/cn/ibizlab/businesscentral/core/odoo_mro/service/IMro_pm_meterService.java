package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meterSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_pm_meter] 服务对象接口
 */
public interface IMro_pm_meterService extends IService<Mro_pm_meter>{

    boolean create(Mro_pm_meter et) ;
    void createBatch(List<Mro_pm_meter> list) ;
    boolean update(Mro_pm_meter et) ;
    void updateBatch(List<Mro_pm_meter> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_pm_meter get(Long key) ;
    Mro_pm_meter getDraft(Mro_pm_meter et) ;
    boolean checkKey(Mro_pm_meter et) ;
    boolean save(Mro_pm_meter et) ;
    void saveBatch(List<Mro_pm_meter> list) ;
    Page<Mro_pm_meter> searchDefault(Mro_pm_meterSearchContext context) ;
    List<Mro_pm_meter> selectByAssetId(Long id);
    List<Mro_pm_meter> selectByAssetId(Collection<Long> ids);
    void removeByAssetId(Long id);
    List<Mro_pm_meter> selectByParentRatioId(Long id);
    List<Mro_pm_meter> selectByParentRatioId(Collection<Long> ids);
    void removeByParentRatioId(Long id);
    List<Mro_pm_meter> selectByParentMeterId(Long id);
    List<Mro_pm_meter> selectByParentMeterId(Collection<Long> ids);
    void removeByParentMeterId(Long id);
    List<Mro_pm_meter> selectByName(Long id);
    List<Mro_pm_meter> selectByName(Collection<Long> ids);
    void removeByName(Long id);
    List<Mro_pm_meter> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_pm_meter> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_pm_meter> getMroPmMeterByIds(List<Long> ids) ;
    List<Mro_pm_meter> getMroPmMeterByEntities(List<Mro_pm_meter> entities) ;
}


