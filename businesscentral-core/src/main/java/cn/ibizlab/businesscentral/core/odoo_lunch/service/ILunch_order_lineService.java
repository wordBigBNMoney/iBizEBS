package cn.ibizlab.businesscentral.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_order_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Lunch_order_line] 服务对象接口
 */
public interface ILunch_order_lineService extends IService<Lunch_order_line>{

    boolean create(Lunch_order_line et) ;
    void createBatch(List<Lunch_order_line> list) ;
    boolean update(Lunch_order_line et) ;
    void updateBatch(List<Lunch_order_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Lunch_order_line get(Long key) ;
    Lunch_order_line getDraft(Lunch_order_line et) ;
    boolean checkKey(Lunch_order_line et) ;
    boolean save(Lunch_order_line et) ;
    void saveBatch(List<Lunch_order_line> list) ;
    Page<Lunch_order_line> searchDefault(Lunch_order_lineSearchContext context) ;
    List<Lunch_order_line> selectByOrderId(Long id);
    void removeByOrderId(Collection<Long> ids);
    void removeByOrderId(Long id);
    List<Lunch_order_line> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Lunch_order_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Lunch_order_line> selectBySupplier(Long id);
    void resetBySupplier(Long id);
    void resetBySupplier(Collection<Long> ids);
    void removeBySupplier(Long id);
    List<Lunch_order_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Lunch_order_line> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Lunch_order_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Lunch_order_line> getLunchOrderLineByIds(List<Long> ids) ;
    List<Lunch_order_line> getLunchOrderLineByEntities(List<Lunch_order_line> entities) ;
}


