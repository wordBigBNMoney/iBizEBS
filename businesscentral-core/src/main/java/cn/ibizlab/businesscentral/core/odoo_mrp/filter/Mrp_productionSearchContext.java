package cn.ibizlab.businesscentral.core.odoo_mrp.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production;
/**
 * 关系型数据实体[Mrp_production] 查询条件对象
 */
@Slf4j
@Data
public class Mrp_productionSearchContext extends QueryWrapperContext<Mrp_production> {

	private String n_priority_eq;//[优先级]
	public void setN_priority_eq(String n_priority_eq) {
        this.n_priority_eq = n_priority_eq;
        if(!ObjectUtils.isEmpty(this.n_priority_eq)){
            this.getSearchCond().eq("priority", n_priority_eq);
        }
    }
	private String n_name_like;//[参考]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_availability_eq;//[材料可用性]
	public void setN_availability_eq(String n_availability_eq) {
        this.n_availability_eq = n_availability_eq;
        if(!ObjectUtils.isEmpty(this.n_availability_eq)){
            this.getSearchCond().eq("availability", n_availability_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_location_dest_id_text_eq;//[成品位置]
	public void setN_location_dest_id_text_eq(String n_location_dest_id_text_eq) {
        this.n_location_dest_id_text_eq = n_location_dest_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_text_eq)){
            this.getSearchCond().eq("location_dest_id_text", n_location_dest_id_text_eq);
        }
    }
	private String n_location_dest_id_text_like;//[成品位置]
	public void setN_location_dest_id_text_like(String n_location_dest_id_text_like) {
        this.n_location_dest_id_text_like = n_location_dest_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_text_like)){
            this.getSearchCond().like("location_dest_id_text", n_location_dest_id_text_like);
        }
    }
	private String n_location_src_id_text_eq;//[原料位置]
	public void setN_location_src_id_text_eq(String n_location_src_id_text_eq) {
        this.n_location_src_id_text_eq = n_location_src_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_src_id_text_eq)){
            this.getSearchCond().eq("location_src_id_text", n_location_src_id_text_eq);
        }
    }
	private String n_location_src_id_text_like;//[原料位置]
	public void setN_location_src_id_text_like(String n_location_src_id_text_like) {
        this.n_location_src_id_text_like = n_location_src_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_src_id_text_like)){
            this.getSearchCond().like("location_src_id_text", n_location_src_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_picking_type_id_text_eq;//[作业类型]
	public void setN_picking_type_id_text_eq(String n_picking_type_id_text_eq) {
        this.n_picking_type_id_text_eq = n_picking_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_eq)){
            this.getSearchCond().eq("picking_type_id_text", n_picking_type_id_text_eq);
        }
    }
	private String n_picking_type_id_text_like;//[作业类型]
	public void setN_picking_type_id_text_like(String n_picking_type_id_text_like) {
        this.n_picking_type_id_text_like = n_picking_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_like)){
            this.getSearchCond().like("picking_type_id_text", n_picking_type_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[负责人]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[负责人]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_product_uom_id_text_eq;//[计量单位]
	public void setN_product_uom_id_text_eq(String n_product_uom_id_text_eq) {
        this.n_product_uom_id_text_eq = n_product_uom_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_eq)){
            this.getSearchCond().eq("product_uom_id_text", n_product_uom_id_text_eq);
        }
    }
	private String n_product_uom_id_text_like;//[计量单位]
	public void setN_product_uom_id_text_like(String n_product_uom_id_text_like) {
        this.n_product_uom_id_text_like = n_product_uom_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_like)){
            this.getSearchCond().like("product_uom_id_text", n_product_uom_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_routing_id_text_eq;//[工艺]
	public void setN_routing_id_text_eq(String n_routing_id_text_eq) {
        this.n_routing_id_text_eq = n_routing_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_routing_id_text_eq)){
            this.getSearchCond().eq("routing_id_text", n_routing_id_text_eq);
        }
    }
	private String n_routing_id_text_like;//[工艺]
	public void setN_routing_id_text_like(String n_routing_id_text_like) {
        this.n_routing_id_text_like = n_routing_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_routing_id_text_like)){
            this.getSearchCond().like("routing_id_text", n_routing_id_text_like);
        }
    }
	private Long n_product_uom_id_eq;//[计量单位]
	public void setN_product_uom_id_eq(Long n_product_uom_id_eq) {
        this.n_product_uom_id_eq = n_product_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_eq)){
            this.getSearchCond().eq("product_uom_id", n_product_uom_id_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_routing_id_eq;//[工艺]
	public void setN_routing_id_eq(Long n_routing_id_eq) {
        this.n_routing_id_eq = n_routing_id_eq;
        if(!ObjectUtils.isEmpty(this.n_routing_id_eq)){
            this.getSearchCond().eq("routing_id", n_routing_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_user_id_eq;//[负责人]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_bom_id_eq;//[物料清单]
	public void setN_bom_id_eq(Long n_bom_id_eq) {
        this.n_bom_id_eq = n_bom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_bom_id_eq)){
            this.getSearchCond().eq("bom_id", n_bom_id_eq);
        }
    }
	private Long n_location_src_id_eq;//[原料位置]
	public void setN_location_src_id_eq(Long n_location_src_id_eq) {
        this.n_location_src_id_eq = n_location_src_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_src_id_eq)){
            this.getSearchCond().eq("location_src_id", n_location_src_id_eq);
        }
    }
	private Long n_picking_type_id_eq;//[作业类型]
	public void setN_picking_type_id_eq(Long n_picking_type_id_eq) {
        this.n_picking_type_id_eq = n_picking_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_eq)){
            this.getSearchCond().eq("picking_type_id", n_picking_type_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_location_dest_id_eq;//[成品位置]
	public void setN_location_dest_id_eq(Long n_location_dest_id_eq) {
        this.n_location_dest_id_eq = n_location_dest_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_eq)){
            this.getSearchCond().eq("location_dest_id", n_location_dest_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



