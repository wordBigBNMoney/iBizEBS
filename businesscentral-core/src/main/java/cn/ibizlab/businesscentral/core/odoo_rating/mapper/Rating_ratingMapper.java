package cn.ibizlab.businesscentral.core.odoo_rating.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.businesscentral.core.odoo_rating.filter.Rating_ratingSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Rating_ratingMapper extends BaseMapper<Rating_rating>{

    Page<Rating_rating> searchDefault(IPage page, @Param("srf") Rating_ratingSearchContext context, @Param("ew") Wrapper<Rating_rating> wrapper) ;
    @Override
    Rating_rating selectById(Serializable id);
    @Override
    int insert(Rating_rating entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Rating_rating entity);
    @Override
    int update(@Param(Constants.ENTITY) Rating_rating entity, @Param("ew") Wrapper<Rating_rating> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Rating_rating> selectByMessageId(@Param("id") Serializable id) ;

    List<Rating_rating> selectByPartnerId(@Param("id") Serializable id) ;

    List<Rating_rating> selectByRatedPartnerId(@Param("id") Serializable id) ;

    List<Rating_rating> selectByCreateUid(@Param("id") Serializable id) ;

    List<Rating_rating> selectByWriteUid(@Param("id") Serializable id) ;


}
