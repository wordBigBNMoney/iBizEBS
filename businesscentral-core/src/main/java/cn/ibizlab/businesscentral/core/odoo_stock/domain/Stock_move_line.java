package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[产品移动(移库明细)]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "STOCK_MOVE_LINE",resultMap = "Stock_move_lineResultMap")
public class Stock_move_line extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 完成
     */
    @DEField(name = "qty_done")
    @TableField(value = "qty_done")
    @JSONField(name = "qty_done")
    @JsonProperty("qty_done")
    private Double qtyDone;
    /**
     * 使用已有批次/序列号码
     */
    @TableField(exist = false)
    @JSONField(name = "picking_type_use_existing_lots")
    @JsonProperty("picking_type_use_existing_lots")
    private Boolean pickingTypeUseExistingLots;
    /**
     * 消耗行
     */
    @TableField(exist = false)
    @JSONField(name = "consume_line_ids")
    @JsonProperty("consume_line_ids")
    private String consumeLineIds;
    /**
     * 批次可见
     */
    @TableField(exist = false)
    @JSONField(name = "lots_visible")
    @JsonProperty("lots_visible")
    private Boolean lotsVisible;
    /**
     * 已保留
     */
    @DEField(name = "product_uom_qty")
    @TableField(value = "product_uom_qty")
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 完成工单
     */
    @DEField(name = "done_wo")
    @TableField(value = "done_wo")
    @JSONField(name = "done_wo")
    @JsonProperty("done_wo")
    private Boolean doneWo;
    /**
     * 创建新批次/序列号码
     */
    @TableField(exist = false)
    @JSONField(name = "picking_type_use_create_lots")
    @JsonProperty("picking_type_use_create_lots")
    private Boolean pickingTypeUseCreateLots;
    /**
     * 生产行
     */
    @TableField(exist = false)
    @JSONField(name = "produce_line_ids")
    @JsonProperty("produce_line_ids")
    private String produceLineIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 移动整个包裹
     */
    @TableField(exist = false)
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private Boolean pickingTypeEntirePacks;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 批次/序列号 名称
     */
    @DEField(name = "lot_name")
    @TableField(value = "lot_name")
    @JSONField(name = "lot_name")
    @JsonProperty("lot_name")
    private String lotName;
    /**
     * 实际预留数量
     */
    @DEField(name = "product_qty")
    @TableField(value = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;
    /**
     * 产成品数量
     */
    @DEField(name = "lot_produced_qty")
    @TableField(value = "lot_produced_qty")
    @JSONField(name = "lot_produced_qty")
    @JsonProperty("lot_produced_qty")
    private Double lotProducedQty;
    /**
     * 源包裹
     */
    @TableField(exist = false)
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    private String packageIdText;
    /**
     * 完工批次/序列号
     */
    @TableField(exist = false)
    @JSONField(name = "lot_produced_id_text")
    @JsonProperty("lot_produced_id_text")
    private String lotProducedIdText;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;
    /**
     * 工单
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;
    /**
     * 追踪
     */
    @TableField(exist = false)
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 库存移动
     */
    @TableField(exist = false)
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;
    /**
     * 至
     */
    @TableField(exist = false)
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;
    /**
     * 是锁定
     */
    @TableField(exist = false)
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private Boolean isLocked;
    /**
     * 初始需求是否可以编辑
     */
    @TableField(exist = false)
    @JSONField(name = "is_initial_demand_editable")
    @JsonProperty("is_initial_demand_editable")
    private Boolean isInitialDemandEditable;
    /**
     * 生产单
     */
    @TableField(exist = false)
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;
    /**
     * 状态
     */
    @TableField(exist = false)
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 批次/序列号码
     */
    @TableField(exist = false)
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    private String lotIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 编号
     */
    @TableField(exist = false)
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;
    /**
     * 目的地包裹
     */
    @TableField(exist = false)
    @JSONField(name = "result_package_id_text")
    @JsonProperty("result_package_id_text")
    private String resultPackageIdText;
    /**
     * 移动完成
     */
    @TableField(exist = false)
    @JSONField(name = "done_move")
    @JsonProperty("done_move")
    private Boolean doneMove;
    /**
     * 库存拣货
     */
    @TableField(exist = false)
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "owner_id_text")
    @JsonProperty("owner_id_text")
    private String ownerIdText;
    /**
     * 从
     */
    @TableField(exist = false)
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;
    /**
     * 单位
     */
    @DEField(name = "product_uom_id")
    @TableField(value = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Long productUomId;
    /**
     * 库存移动
     */
    @DEField(name = "move_id")
    @TableField(value = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Long moveId;
    /**
     * 目的地包裹
     */
    @DEField(name = "result_package_id")
    @TableField(value = "result_package_id")
    @JSONField(name = "result_package_id")
    @JsonProperty("result_package_id")
    private Long resultPackageId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 完工批次/序列号
     */
    @DEField(name = "lot_produced_id")
    @TableField(value = "lot_produced_id")
    @JSONField(name = "lot_produced_id")
    @JsonProperty("lot_produced_id")
    private Long lotProducedId;
    /**
     * 产品
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 批次/序列号码
     */
    @DEField(name = "lot_id")
    @TableField(value = "lot_id")
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    private Long lotId;
    /**
     * 库存拣货
     */
    @DEField(name = "picking_id")
    @TableField(value = "picking_id")
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Long pickingId;
    /**
     * 工单
     */
    @DEField(name = "workorder_id")
    @TableField(value = "workorder_id")
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Long workorderId;
    /**
     * 从
     */
    @DEField(name = "location_id")
    @TableField(value = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Long locationId;
    /**
     * 源包裹
     */
    @DEField(name = "package_id")
    @TableField(value = "package_id")
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    private Long packageId;
    /**
     * 所有者
     */
    @DEField(name = "owner_id")
    @TableField(value = "owner_id")
    @JSONField(name = "owner_id")
    @JsonProperty("owner_id")
    private Long ownerId;
    /**
     * 包裹层级
     */
    @DEField(name = "package_level_id")
    @TableField(value = "package_level_id")
    @JSONField(name = "package_level_id")
    @JsonProperty("package_level_id")
    private Long packageLevelId;
    /**
     * 至
     */
    @DEField(name = "location_dest_id")
    @TableField(value = "location_dest_id")
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Long locationDestId;
    /**
     * 生产单
     */
    @DEField(name = "production_id")
    @TableField(value = "production_id")
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Long productionId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooProduction;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder odooWorkorder;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOwner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooMove;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_level odooPackageLevel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooPicking;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLot;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLotProduced;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package odooPackage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package odooResultPackage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom;



    /**
     * 设置 [完成]
     */
    public void setQtyDone(Double qtyDone){
        this.qtyDone = qtyDone ;
        this.modify("qty_done",qtyDone);
    }

    /**
     * 设置 [已保留]
     */
    public void setProductUomQty(Double productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [完成工单]
     */
    public void setDoneWo(Boolean doneWo){
        this.doneWo = doneWo ;
        this.modify("done_wo",doneWo);
    }

    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
    /**
     * 设置 [批次/序列号 名称]
     */
    public void setLotName(String lotName){
        this.lotName = lotName ;
        this.modify("lot_name",lotName);
    }

    /**
     * 设置 [实际预留数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [产成品数量]
     */
    public void setLotProducedQty(Double lotProducedQty){
        this.lotProducedQty = lotProducedQty ;
        this.modify("lot_produced_qty",lotProducedQty);
    }

    /**
     * 设置 [单位]
     */
    public void setProductUomId(Long productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [库存移动]
     */
    public void setMoveId(Long moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [目的地包裹]
     */
    public void setResultPackageId(Long resultPackageId){
        this.resultPackageId = resultPackageId ;
        this.modify("result_package_id",resultPackageId);
    }

    /**
     * 设置 [完工批次/序列号]
     */
    public void setLotProducedId(Long lotProducedId){
        this.lotProducedId = lotProducedId ;
        this.modify("lot_produced_id",lotProducedId);
    }

    /**
     * 设置 [产品]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [批次/序列号码]
     */
    public void setLotId(Long lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }

    /**
     * 设置 [库存拣货]
     */
    public void setPickingId(Long pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }

    /**
     * 设置 [工单]
     */
    public void setWorkorderId(Long workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }

    /**
     * 设置 [从]
     */
    public void setLocationId(Long locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [源包裹]
     */
    public void setPackageId(Long packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }

    /**
     * 设置 [所有者]
     */
    public void setOwnerId(Long ownerId){
        this.ownerId = ownerId ;
        this.modify("owner_id",ownerId);
    }

    /**
     * 设置 [包裹层级]
     */
    public void setPackageLevelId(Long packageLevelId){
        this.packageLevelId = packageLevelId ;
        this.modify("package_level_id",packageLevelId);
    }

    /**
     * 设置 [至]
     */
    public void setLocationDestId(Long locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }

    /**
     * 设置 [生产单]
     */
    public void setProductionId(Long productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


