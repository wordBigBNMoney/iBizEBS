package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statementSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_bank_statement] 服务对象接口
 */
public interface IAccount_bank_statementService extends IService<Account_bank_statement>{

    boolean create(Account_bank_statement et) ;
    void createBatch(List<Account_bank_statement> list) ;
    boolean update(Account_bank_statement et) ;
    void updateBatch(List<Account_bank_statement> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_bank_statement get(Long key) ;
    Account_bank_statement getDraft(Account_bank_statement et) ;
    boolean checkKey(Account_bank_statement et) ;
    boolean save(Account_bank_statement et) ;
    void saveBatch(List<Account_bank_statement> list) ;
    Page<Account_bank_statement> searchDefault(Account_bank_statementSearchContext context) ;
    List<Account_bank_statement> selectByCashboxEndId(Long id);
    void resetByCashboxEndId(Long id);
    void resetByCashboxEndId(Collection<Long> ids);
    void removeByCashboxEndId(Long id);
    List<Account_bank_statement> selectByCashboxStartId(Long id);
    void resetByCashboxStartId(Long id);
    void resetByCashboxStartId(Collection<Long> ids);
    void removeByCashboxStartId(Long id);
    List<Account_bank_statement> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_bank_statement> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_bank_statement> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_bank_statement> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Account_bank_statement> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_bank_statement> getAccountBankStatementByIds(List<Long> ids) ;
    List<Account_bank_statement> getAccountBankStatementByEntities(List<Account_bank_statement> entities) ;
}


