package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_accountSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_accountService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_fiscal_position_accountMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[会计税科目映射] 服务对象接口实现
 */
@Slf4j
@Service("Account_fiscal_position_accountServiceImpl")
public class Account_fiscal_position_accountServiceImpl extends EBSServiceImpl<Account_fiscal_position_accountMapper, Account_fiscal_position_account> implements IAccount_fiscal_position_accountService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.fiscal.position.account" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_fiscal_position_account et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_fiscal_position_accountService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_fiscal_position_account> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_fiscal_position_account et) {
        Account_fiscal_position_account old = new Account_fiscal_position_account() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_fiscal_position_accountService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_fiscal_position_accountService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_fiscal_position_account> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_fiscal_position_account get(Long key) {
        Account_fiscal_position_account et = getById(key);
        if(et==null){
            et=new Account_fiscal_position_account();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_fiscal_position_account getDraft(Account_fiscal_position_account et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_fiscal_position_account et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_fiscal_position_account et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_fiscal_position_account et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_fiscal_position_account> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_fiscal_position_account> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_fiscal_position_account> selectByAccountDestId(Long id) {
        return baseMapper.selectByAccountDestId(id);
    }
    @Override
    public void resetByAccountDestId(Long id) {
        this.update(new UpdateWrapper<Account_fiscal_position_account>().set("account_dest_id",null).eq("account_dest_id",id));
    }

    @Override
    public void resetByAccountDestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_fiscal_position_account>().set("account_dest_id",null).in("account_dest_id",ids));
    }

    @Override
    public void removeByAccountDestId(Long id) {
        this.remove(new QueryWrapper<Account_fiscal_position_account>().eq("account_dest_id",id));
    }

	@Override
    public List<Account_fiscal_position_account> selectByAccountSrcId(Long id) {
        return baseMapper.selectByAccountSrcId(id);
    }
    @Override
    public void resetByAccountSrcId(Long id) {
        this.update(new UpdateWrapper<Account_fiscal_position_account>().set("account_src_id",null).eq("account_src_id",id));
    }

    @Override
    public void resetByAccountSrcId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_fiscal_position_account>().set("account_src_id",null).in("account_src_id",ids));
    }

    @Override
    public void removeByAccountSrcId(Long id) {
        this.remove(new QueryWrapper<Account_fiscal_position_account>().eq("account_src_id",id));
    }

	@Override
    public List<Account_fiscal_position_account> selectByPositionId(Long id) {
        return baseMapper.selectByPositionId(id);
    }
    @Override
    public void removeByPositionId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_fiscal_position_account>().in("position_id",ids));
    }

    @Override
    public void removeByPositionId(Long id) {
        this.remove(new QueryWrapper<Account_fiscal_position_account>().eq("position_id",id));
    }

	@Override
    public List<Account_fiscal_position_account> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_fiscal_position_account>().eq("create_uid",id));
    }

	@Override
    public List<Account_fiscal_position_account> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_fiscal_position_account>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_fiscal_position_account> searchDefault(Account_fiscal_position_accountSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_fiscal_position_account> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_fiscal_position_account>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_fiscal_position_account et){
        //实体关系[DER1N_ACCOUNT_FISCAL_POSITION_ACCOUNT__ACCOUNT_ACCOUNT__ACCOUNT_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getAccountDestId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccountDest=et.getOdooAccountDest();
            if(ObjectUtils.isEmpty(odooAccountDest)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountDestId());
                et.setOdooAccountDest(majorEntity);
                odooAccountDest=majorEntity;
            }
            et.setAccountDestIdText(odooAccountDest.getName());
        }
        //实体关系[DER1N_ACCOUNT_FISCAL_POSITION_ACCOUNT__ACCOUNT_ACCOUNT__ACCOUNT_SRC_ID]
        if(!ObjectUtils.isEmpty(et.getAccountSrcId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccountSrc=et.getOdooAccountSrc();
            if(ObjectUtils.isEmpty(odooAccountSrc)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountSrcId());
                et.setOdooAccountSrc(majorEntity);
                odooAccountSrc=majorEntity;
            }
            et.setAccountSrcIdText(odooAccountSrc.getName());
        }
        //实体关系[DER1N_ACCOUNT_FISCAL_POSITION_ACCOUNT__ACCOUNT_FISCAL_POSITION__POSITION_ID]
        if(!ObjectUtils.isEmpty(et.getPositionId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooPosition=et.getOdooPosition();
            if(ObjectUtils.isEmpty(odooPosition)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position majorEntity=accountFiscalPositionService.get(et.getPositionId());
                et.setOdooPosition(majorEntity);
                odooPosition=majorEntity;
            }
            et.setPositionIdText(odooPosition.getName());
        }
        //实体关系[DER1N_ACCOUNT_FISCAL_POSITION_ACCOUNT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_FISCAL_POSITION_ACCOUNT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_fiscal_position_account> getAccountFiscalPositionAccountByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_fiscal_position_account> getAccountFiscalPositionAccountByEntities(List<Account_fiscal_position_account> entities) {
        List ids =new ArrayList();
        for(Account_fiscal_position_account entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



