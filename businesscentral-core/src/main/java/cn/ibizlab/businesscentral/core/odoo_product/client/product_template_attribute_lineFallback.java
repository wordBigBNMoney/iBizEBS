package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_line;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_template_attribute_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_template_attribute_line] 服务对象接口
 */
@Component
public class product_template_attribute_lineFallback implements product_template_attribute_lineFeignClient{



    public Page<Product_template_attribute_line> search(Product_template_attribute_lineSearchContext context){
            return null;
     }


    public Product_template_attribute_line update(Long id, Product_template_attribute_line product_template_attribute_line){
            return null;
     }
    public Boolean updateBatch(List<Product_template_attribute_line> product_template_attribute_lines){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Product_template_attribute_line create(Product_template_attribute_line product_template_attribute_line){
            return null;
     }
    public Boolean createBatch(List<Product_template_attribute_line> product_template_attribute_lines){
            return false;
     }


    public Product_template_attribute_line get(Long id){
            return null;
     }


    public Page<Product_template_attribute_line> select(){
            return null;
     }

    public Product_template_attribute_line getDraft(){
            return null;
    }



    public Boolean checkKey(Product_template_attribute_line product_template_attribute_line){
            return false;
     }


    public Boolean save(Product_template_attribute_line product_template_attribute_line){
            return false;
     }
    public Boolean saveBatch(List<Product_template_attribute_line> product_template_attribute_lines){
            return false;
     }

    public Page<Product_template_attribute_line> searchDefault(Product_template_attribute_lineSearchContext context){
            return null;
     }


}
