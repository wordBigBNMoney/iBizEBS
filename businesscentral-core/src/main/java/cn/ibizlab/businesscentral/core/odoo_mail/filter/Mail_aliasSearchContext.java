package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias;
/**
 * 关系型数据实体[Mail_alias] 查询条件对象
 */
@Slf4j
@Data
public class Mail_aliasSearchContext extends QueryWrapperContext<Mail_alias> {

	private String n_alias_contact_eq;//[安全联系人别名]
	public void setN_alias_contact_eq(String n_alias_contact_eq) {
        this.n_alias_contact_eq = n_alias_contact_eq;
        if(!ObjectUtils.isEmpty(this.n_alias_contact_eq)){
            this.getSearchCond().eq("alias_contact", n_alias_contact_eq);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_alias_user_id_text_eq;//[所有者]
	public void setN_alias_user_id_text_eq(String n_alias_user_id_text_eq) {
        this.n_alias_user_id_text_eq = n_alias_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_alias_user_id_text_eq)){
            this.getSearchCond().eq("alias_user_id_text", n_alias_user_id_text_eq);
        }
    }
	private String n_alias_user_id_text_like;//[所有者]
	public void setN_alias_user_id_text_like(String n_alias_user_id_text_like) {
        this.n_alias_user_id_text_like = n_alias_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_alias_user_id_text_like)){
            this.getSearchCond().like("alias_user_id_text", n_alias_user_id_text_like);
        }
    }
	private Long n_alias_user_id_eq;//[所有者]
	public void setN_alias_user_id_eq(Long n_alias_user_id_eq) {
        this.n_alias_user_id_eq = n_alias_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_alias_user_id_eq)){
            this.getSearchCond().eq("alias_user_id", n_alias_user_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



