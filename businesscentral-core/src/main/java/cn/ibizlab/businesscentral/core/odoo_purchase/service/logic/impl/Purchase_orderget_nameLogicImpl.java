package cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_orderget_nameLogic;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;

/**
 * 关系型数据实体[get_name] 对象
 */
@Slf4j
@Service
public class Purchase_orderget_nameLogicImpl implements IPurchase_orderget_nameLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchase_orderservice;

    public cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService getPurchase_orderService() {
        return this.purchase_orderservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Purchase_order et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("purchase_orderget_namedefault",et);
           kieSession.setGlobal("purchase_orderservice",purchase_orderservice);
           kieSession.setGlobal("iBzSysPurchase_orderDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.purchase_orderget_name");

        }catch(Exception e){
            throw new RuntimeException("执行[获取订单编号]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
