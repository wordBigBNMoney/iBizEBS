package cn.ibizlab.businesscentral.core.odoo_event.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_registration;
/**
 * 关系型数据实体[Event_registration] 查询条件对象
 */
@Slf4j
@Data
public class Event_registrationSearchContext extends QueryWrapperContext<Event_registration> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_like;//[与会者姓名]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_sale_order_id_text_eq;//[源销售订单]
	public void setN_sale_order_id_text_eq(String n_sale_order_id_text_eq) {
        this.n_sale_order_id_text_eq = n_sale_order_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_text_eq)){
            this.getSearchCond().eq("sale_order_id_text", n_sale_order_id_text_eq);
        }
    }
	private String n_sale_order_id_text_like;//[源销售订单]
	public void setN_sale_order_id_text_like(String n_sale_order_id_text_like) {
        this.n_sale_order_id_text_like = n_sale_order_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_text_like)){
            this.getSearchCond().like("sale_order_id_text", n_sale_order_id_text_like);
        }
    }
	private String n_sale_order_line_id_text_eq;//[销售订单行]
	public void setN_sale_order_line_id_text_eq(String n_sale_order_line_id_text_eq) {
        this.n_sale_order_line_id_text_eq = n_sale_order_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_line_id_text_eq)){
            this.getSearchCond().eq("sale_order_line_id_text", n_sale_order_line_id_text_eq);
        }
    }
	private String n_sale_order_line_id_text_like;//[销售订单行]
	public void setN_sale_order_line_id_text_like(String n_sale_order_line_id_text_like) {
        this.n_sale_order_line_id_text_like = n_sale_order_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sale_order_line_id_text_like)){
            this.getSearchCond().like("sale_order_line_id_text", n_sale_order_line_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_event_ticket_id_text_eq;//[活动入场券]
	public void setN_event_ticket_id_text_eq(String n_event_ticket_id_text_eq) {
        this.n_event_ticket_id_text_eq = n_event_ticket_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_event_ticket_id_text_eq)){
            this.getSearchCond().eq("event_ticket_id_text", n_event_ticket_id_text_eq);
        }
    }
	private String n_event_ticket_id_text_like;//[活动入场券]
	public void setN_event_ticket_id_text_like(String n_event_ticket_id_text_like) {
        this.n_event_ticket_id_text_like = n_event_ticket_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_event_ticket_id_text_like)){
            this.getSearchCond().like("event_ticket_id_text", n_event_ticket_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_event_id_text_eq;//[活动]
	public void setN_event_id_text_eq(String n_event_id_text_eq) {
        this.n_event_id_text_eq = n_event_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_event_id_text_eq)){
            this.getSearchCond().eq("event_id_text", n_event_id_text_eq);
        }
    }
	private String n_event_id_text_like;//[活动]
	public void setN_event_id_text_like(String n_event_id_text_like) {
        this.n_event_id_text_like = n_event_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_event_id_text_like)){
            this.getSearchCond().like("event_id_text", n_event_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[联系]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[联系]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private Long n_event_id_eq;//[活动]
	public void setN_event_id_eq(Long n_event_id_eq) {
        this.n_event_id_eq = n_event_id_eq;
        if(!ObjectUtils.isEmpty(this.n_event_id_eq)){
            this.getSearchCond().eq("event_id", n_event_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_sale_order_line_id_eq;//[销售订单行]
	public void setN_sale_order_line_id_eq(Long n_sale_order_line_id_eq) {
        this.n_sale_order_line_id_eq = n_sale_order_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_line_id_eq)){
            this.getSearchCond().eq("sale_order_line_id", n_sale_order_line_id_eq);
        }
    }
	private Long n_partner_id_eq;//[联系]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_event_ticket_id_eq;//[活动入场券]
	public void setN_event_ticket_id_eq(Long n_event_ticket_id_eq) {
        this.n_event_ticket_id_eq = n_event_ticket_id_eq;
        if(!ObjectUtils.isEmpty(this.n_event_ticket_id_eq)){
            this.getSearchCond().eq("event_ticket_id", n_event_ticket_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_sale_order_id_eq;//[源销售订单]
	public void setN_sale_order_id_eq(Long n_sale_order_id_eq) {
        this.n_sale_order_id_eq = n_sale_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_eq)){
            this.getSearchCond().eq("sale_order_id", n_sale_order_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



