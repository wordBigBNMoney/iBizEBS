package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_workcenter_productivity] 服务对象接口
 */
public interface IMrp_workcenter_productivityService extends IService<Mrp_workcenter_productivity>{

    boolean create(Mrp_workcenter_productivity et) ;
    void createBatch(List<Mrp_workcenter_productivity> list) ;
    boolean update(Mrp_workcenter_productivity et) ;
    void updateBatch(List<Mrp_workcenter_productivity> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_workcenter_productivity get(Long key) ;
    Mrp_workcenter_productivity getDraft(Mrp_workcenter_productivity et) ;
    boolean checkKey(Mrp_workcenter_productivity et) ;
    boolean save(Mrp_workcenter_productivity et) ;
    void saveBatch(List<Mrp_workcenter_productivity> list) ;
    Page<Mrp_workcenter_productivity> searchDefault(Mrp_workcenter_productivitySearchContext context) ;
    List<Mrp_workcenter_productivity> selectByLossId(Long id);
    List<Mrp_workcenter_productivity> selectByLossId(Collection<Long> ids);
    void removeByLossId(Long id);
    List<Mrp_workcenter_productivity> selectByWorkcenterId(Long id);
    void resetByWorkcenterId(Long id);
    void resetByWorkcenterId(Collection<Long> ids);
    void removeByWorkcenterId(Long id);
    List<Mrp_workcenter_productivity> selectByWorkorderId(Long id);
    void resetByWorkorderId(Long id);
    void resetByWorkorderId(Collection<Long> ids);
    void removeByWorkorderId(Long id);
    List<Mrp_workcenter_productivity> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_workcenter_productivity> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Mrp_workcenter_productivity> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_workcenter_productivity> getMrpWorkcenterProductivityByIds(List<Long> ids) ;
    List<Mrp_workcenter_productivity> getMrpWorkcenterProductivityByEntities(List<Mrp_workcenter_productivity> entities) ;
}


