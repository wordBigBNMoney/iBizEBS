package cn.ibizlab.businesscentral.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_menu;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_menuSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Website_menu] 服务对象接口
 */
public interface IWebsite_menuService extends IService<Website_menu>{

    boolean create(Website_menu et) ;
    void createBatch(List<Website_menu> list) ;
    boolean update(Website_menu et) ;
    void updateBatch(List<Website_menu> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Website_menu get(Long key) ;
    Website_menu getDraft(Website_menu et) ;
    boolean checkKey(Website_menu et) ;
    boolean save(Website_menu et) ;
    void saveBatch(List<Website_menu> list) ;
    Page<Website_menu> searchDefault(Website_menuSearchContext context) ;
    List<Website_menu> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Website_menu> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Website_menu> selectByParentId(Long id);
    void removeByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Website_menu> selectByPageId(Long id);
    void removeByPageId(Collection<Long> ids);
    void removeByPageId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Website_menu> getWebsiteMenuByIds(List<Long> ids) ;
    List<Website_menu> getWebsiteMenuByEntities(List<Website_menu> entities) ;
}


