package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[邮件列表]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_MASS_MAILING_LIST",resultMap = "Mail_mass_mailing_listResultMap")
public class Mail_mass_mailing_list extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 联系人人数
     */
    @TableField(exist = false)
    @JSONField(name = "contact_nbr")
    @JsonProperty("contact_nbr")
    private Integer contactNbr;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 订阅信息
     */
    @TableField(exist = false)
    @JSONField(name = "subscription_contact_ids")
    @JsonProperty("subscription_contact_ids")
    private String subscriptionContactIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 邮件列表
     */
    @TableField(exist = false)
    @JSONField(name = "contact_ids")
    @JsonProperty("contact_ids")
    private String contactIds;
    /**
     * 网站弹出内容
     */
    @DEField(name = "popup_content")
    @TableField(value = "popup_content")
    @JSONField(name = "popup_content")
    @JsonProperty("popup_content")
    private String popupContent;
    /**
     * 公开
     */
    @DEField(name = "is_public")
    @TableField(value = "is_public")
    @JSONField(name = "is_public")
    @JsonProperty("is_public")
    private Boolean isPublic;
    /**
     * 邮件列表
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 网站弹出重新定向网址
     */
    @DEField(name = "popup_redirect_url")
    @TableField(value = "popup_redirect_url")
    @JSONField(name = "popup_redirect_url")
    @JsonProperty("popup_redirect_url")
    private String popupRedirectUrl;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [网站弹出内容]
     */
    public void setPopupContent(String popupContent){
        this.popupContent = popupContent ;
        this.modify("popup_content",popupContent);
    }

    /**
     * 设置 [公开]
     */
    public void setIsPublic(Boolean isPublic){
        this.isPublic = isPublic ;
        this.modify("is_public",isPublic);
    }

    /**
     * 设置 [邮件列表]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [网站弹出重新定向网址]
     */
    public void setPopupRedirectUrl(String popupRedirectUrl){
        this.popupRedirectUrl = popupRedirectUrl ;
        this.modify("popup_redirect_url",popupRedirectUrl);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


