package cn.ibizlab.businesscentral.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_pricelist_itemSearchContext;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelist_itemService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_product.mapper.Product_pricelist_itemMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[价格表明细] 服务对象接口实现
 */
@Slf4j
@Service("Product_pricelist_itemServiceImpl")
public class Product_pricelist_itemServiceImpl extends EBSServiceImpl<Product_pricelist_itemMapper, Product_pricelist_item> implements IProduct_pricelist_itemService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_categoryService productCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService productPricelistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "product.pricelist.item" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Product_pricelist_item et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_pricelist_itemService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Product_pricelist_item> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Product_pricelist_item et) {
        Product_pricelist_item old = new Product_pricelist_item() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_pricelist_itemService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_pricelist_itemService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Product_pricelist_item> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Product_pricelist_item get(Long key) {
        Product_pricelist_item et = getById(key);
        if(et==null){
            et=new Product_pricelist_item();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Product_pricelist_item getDraft(Product_pricelist_item et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Product_pricelist_item et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Product_pricelist_item et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Product_pricelist_item et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Product_pricelist_item> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Product_pricelist_item> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Product_pricelist_item> selectByCategId(Long id) {
        return baseMapper.selectByCategId(id);
    }
    @Override
    public void removeByCategId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Product_pricelist_item>().in("categ_id",ids));
    }

    @Override
    public void removeByCategId(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("categ_id",id));
    }

	@Override
    public List<Product_pricelist_item> selectByBasePricelistId(Long id) {
        return baseMapper.selectByBasePricelistId(id);
    }
    @Override
    public void resetByBasePricelistId(Long id) {
        this.update(new UpdateWrapper<Product_pricelist_item>().set("base_pricelist_id",null).eq("base_pricelist_id",id));
    }

    @Override
    public void resetByBasePricelistId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Product_pricelist_item>().set("base_pricelist_id",null).in("base_pricelist_id",ids));
    }

    @Override
    public void removeByBasePricelistId(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("base_pricelist_id",id));
    }

	@Override
    public List<Product_pricelist_item> selectByPricelistId(Long id) {
        return baseMapper.selectByPricelistId(id);
    }
    @Override
    public void removeByPricelistId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Product_pricelist_item>().in("pricelist_id",ids));
    }

    @Override
    public void removeByPricelistId(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("pricelist_id",id));
    }

	@Override
    public List<Product_pricelist_item> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void removeByProductId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Product_pricelist_item>().in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("product_id",id));
    }

	@Override
    public List<Product_pricelist_item> selectByProductTmplId(Long id) {
        return baseMapper.selectByProductTmplId(id);
    }
    @Override
    public void removeByProductTmplId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Product_pricelist_item>().in("product_tmpl_id",ids));
    }

    @Override
    public void removeByProductTmplId(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("product_tmpl_id",id));
    }

	@Override
    public List<Product_pricelist_item> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Product_pricelist_item>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Product_pricelist_item>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("company_id",id));
    }

	@Override
    public List<Product_pricelist_item> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Product_pricelist_item>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Product_pricelist_item>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("currency_id",id));
    }

	@Override
    public List<Product_pricelist_item> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("create_uid",id));
    }

	@Override
    public List<Product_pricelist_item> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Product_pricelist_item>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Product_pricelist_item> searchDefault(Product_pricelist_itemSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Product_pricelist_item> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Product_pricelist_item>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Product_pricelist_item et){
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__PRODUCT_CATEGORY__CATEG_ID]
        if(!ObjectUtils.isEmpty(et.getCategId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category odooCateg=et.getOdooCateg();
            if(ObjectUtils.isEmpty(odooCateg)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category majorEntity=productCategoryService.get(et.getCategId());
                et.setOdooCateg(majorEntity);
                odooCateg=majorEntity;
            }
            et.setCategIdText(odooCateg.getName());
        }
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__PRODUCT_PRICELIST__BASE_PRICELIST_ID]
        if(!ObjectUtils.isEmpty(et.getBasePricelistId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooBasePricelist=et.getOdooBasePricelist();
            if(ObjectUtils.isEmpty(odooBasePricelist)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist majorEntity=productPricelistService.get(et.getBasePricelistId());
                et.setOdooBasePricelist(majorEntity);
                odooBasePricelist=majorEntity;
            }
            et.setBasePricelistIdText(odooBasePricelist.getName());
        }
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__PRODUCT_PRICELIST__PRICELIST_ID]
        if(!ObjectUtils.isEmpty(et.getPricelistId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist=et.getOdooPricelist();
            if(ObjectUtils.isEmpty(odooPricelist)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist majorEntity=productPricelistService.get(et.getPricelistId());
                et.setOdooPricelist(majorEntity);
                odooPricelist=majorEntity;
            }
            et.setPricelistIdText(odooPricelist.getName());
        }
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__PRODUCT_TEMPLATE__PRODUCT_TMPL_ID]
        if(!ObjectUtils.isEmpty(et.getProductTmplId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template odooProductTmpl=et.getOdooProductTmpl();
            if(ObjectUtils.isEmpty(odooProductTmpl)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template majorEntity=productTemplateService.get(et.getProductTmplId());
                et.setOdooProductTmpl(majorEntity);
                odooProductTmpl=majorEntity;
            }
            et.setProductTmplIdText(odooProductTmpl.getName());
        }
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_PRODUCT_PRICELIST_ITEM__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Product_pricelist_item> getProductPricelistItemByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Product_pricelist_item> getProductPricelistItemByEntities(List<Product_pricelist_item> entities) {
        List ids =new ArrayList();
        for(Product_pricelist_item entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



