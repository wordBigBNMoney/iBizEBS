package cn.ibizlab.businesscentral.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_transactionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[payment_transaction] 服务对象接口
 */
@Component
public class payment_transactionFallback implements payment_transactionFeignClient{


    public Page<Payment_transaction> search(Payment_transactionSearchContext context){
            return null;
     }


    public Payment_transaction create(Payment_transaction payment_transaction){
            return null;
     }
    public Boolean createBatch(List<Payment_transaction> payment_transactions){
            return false;
     }



    public Payment_transaction get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Payment_transaction update(Long id, Payment_transaction payment_transaction){
            return null;
     }
    public Boolean updateBatch(List<Payment_transaction> payment_transactions){
            return false;
     }


    public Page<Payment_transaction> select(){
            return null;
     }

    public Payment_transaction getDraft(){
            return null;
    }



    public Boolean checkKey(Payment_transaction payment_transaction){
            return false;
     }


    public Boolean save(Payment_transaction payment_transaction){
            return false;
     }
    public Boolean saveBatch(List<Payment_transaction> payment_transactions){
            return false;
     }

    public Page<Payment_transaction> searchDefault(Payment_transactionSearchContext context){
            return null;
     }


}
