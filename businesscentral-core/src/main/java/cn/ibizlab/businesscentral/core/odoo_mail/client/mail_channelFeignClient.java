package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_channelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_channel] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-channel", fallback = mail_channelFallback.class)
public interface mail_channelFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mail_channels")
    Mail_channel create(@RequestBody Mail_channel mail_channel);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_channels/batch")
    Boolean createBatch(@RequestBody List<Mail_channel> mail_channels);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_channels/search")
    Page<Mail_channel> search(@RequestBody Mail_channelSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_channels/{id}")
    Mail_channel get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.PUT, value = "/mail_channels/{id}")
    Mail_channel update(@PathVariable("id") Long id,@RequestBody Mail_channel mail_channel);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_channels/batch")
    Boolean updateBatch(@RequestBody List<Mail_channel> mail_channels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_channels/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_channels/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_channels/select")
    Page<Mail_channel> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_channels/getdraft")
    Mail_channel getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_channels/checkkey")
    Boolean checkKey(@RequestBody Mail_channel mail_channel);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_channels/save")
    Boolean save(@RequestBody Mail_channel mail_channel);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_channels/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_channel> mail_channels);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_channels/searchdefault")
    Page<Mail_channel> searchDefault(@RequestBody Mail_channelSearchContext context);


}
