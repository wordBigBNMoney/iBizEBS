package cn.ibizlab.businesscentral.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meterSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meterService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mro.mapper.Mro_pm_meterMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[Asset Meters] 服务对象接口实现
 */
@Slf4j
@Service("Mro_pm_meterServiceImpl")
public class Mro_pm_meterServiceImpl extends EBSServiceImpl<Mro_pm_meterMapper, Mro_pm_meter> implements IMro_pm_meterService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meter_lineService mroPmMeterLineService;

    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meterService mroPmMeterService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_asset.service.IAsset_assetService assetAssetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meter_ratioService mroPmMeterRatioService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_parameterService mroPmParameterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mro.pm.meter" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mro_pm_meter et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_pm_meterService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mro_pm_meter> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mro_pm_meter et) {
        Mro_pm_meter old = new Mro_pm_meter() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_pm_meterService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_pm_meterService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mro_pm_meter> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        if(!ObjectUtils.isEmpty(mroPmMeterLineService.selectByMeterId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mro_pm_meter_line]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(mroPmMeterService.selectByParentMeterId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mro_pm_meter]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        if(!ObjectUtils.isEmpty(mroPmMeterLineService.selectByMeterId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mro_pm_meter_line]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(mroPmMeterService.selectByParentMeterId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mro_pm_meter]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mro_pm_meter get(Long key) {
        Mro_pm_meter et = getById(key);
        if(et==null){
            et=new Mro_pm_meter();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mro_pm_meter getDraft(Mro_pm_meter et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mro_pm_meter et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mro_pm_meter et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mro_pm_meter et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mro_pm_meter> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mro_pm_meter> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mro_pm_meter> selectByAssetId(Long id) {
        return baseMapper.selectByAssetId(id);
    }
    @Override
    public List<Mro_pm_meter> selectByAssetId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mro_pm_meter>().in("id",ids));
    }

    @Override
    public void removeByAssetId(Long id) {
        this.remove(new QueryWrapper<Mro_pm_meter>().eq("asset_id",id));
    }

	@Override
    public List<Mro_pm_meter> selectByParentRatioId(Long id) {
        return baseMapper.selectByParentRatioId(id);
    }
    @Override
    public List<Mro_pm_meter> selectByParentRatioId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mro_pm_meter>().in("id",ids));
    }

    @Override
    public void removeByParentRatioId(Long id) {
        this.remove(new QueryWrapper<Mro_pm_meter>().eq("parent_ratio_id",id));
    }

	@Override
    public List<Mro_pm_meter> selectByParentMeterId(Long id) {
        return baseMapper.selectByParentMeterId(id);
    }
    @Override
    public List<Mro_pm_meter> selectByParentMeterId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mro_pm_meter>().in("id",ids));
    }

    @Override
    public void removeByParentMeterId(Long id) {
        this.remove(new QueryWrapper<Mro_pm_meter>().eq("parent_meter_id",id));
    }

	@Override
    public List<Mro_pm_meter> selectByName(Long id) {
        return baseMapper.selectByName(id);
    }
    @Override
    public List<Mro_pm_meter> selectByName(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mro_pm_meter>().in("id",ids));
    }

    @Override
    public void removeByName(Long id) {
        this.remove(new QueryWrapper<Mro_pm_meter>().eq("name",id));
    }

	@Override
    public List<Mro_pm_meter> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mro_pm_meter>().eq("create_uid",id));
    }

	@Override
    public List<Mro_pm_meter> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mro_pm_meter>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mro_pm_meter> searchDefault(Mro_pm_meterSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mro_pm_meter> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mro_pm_meter>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mro_pm_meter et){
        //实体关系[DER1N_MRO_PM_METER__ASSET_ASSET__ASSET_ID]
        if(!ObjectUtils.isEmpty(et.getAssetId())){
            cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_asset odooAsset=et.getOdooAsset();
            if(ObjectUtils.isEmpty(odooAsset)){
                cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_asset majorEntity=assetAssetService.get(et.getAssetId());
                et.setOdooAsset(majorEntity);
                odooAsset=majorEntity;
            }
            et.setAssetIdText(odooAsset.getName());
        }
        //实体关系[DER1N_MRO_PM_METER__MRO_PM_METER_RATIO__PARENT_RATIO_ID]
        if(!ObjectUtils.isEmpty(et.getParentRatioId())){
            cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_ratio odooParentRatio=et.getOdooParentRatio();
            if(ObjectUtils.isEmpty(odooParentRatio)){
                cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_ratio majorEntity=mroPmMeterRatioService.get(et.getParentRatioId());
                et.setOdooParentRatio(majorEntity);
                odooParentRatio=majorEntity;
            }
            et.setParentRatioIdText(odooParentRatio.getName());
        }
        //实体关系[DER1N_MRO_PM_METER__MRO_PM_PARAMETER__NAME]
        if(!ObjectUtils.isEmpty(et.getName())){
            cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_parameter odooName=et.getOdooName();
            if(ObjectUtils.isEmpty(odooName)){
                cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_parameter majorEntity=mroPmParameterService.get(et.getName());
                et.setOdooName(majorEntity);
                odooName=majorEntity;
            }
            et.setNameText(odooName.getName());
            et.setMeterUom(odooName.getParameterUom());
        }
        //实体关系[DER1N_MRO_PM_METER__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MRO_PM_METER__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mro_pm_meter> getMroPmMeterByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mro_pm_meter> getMroPmMeterByEntities(List<Mro_pm_meter> entities) {
        List ids =new ArrayList();
        for(Mro_pm_meter entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



