package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_config_settingsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_config_settings] 服务对象接口
 */
public interface IRes_config_settingsService extends IService<Res_config_settings>{

    boolean create(Res_config_settings et) ;
    void createBatch(List<Res_config_settings> list) ;
    boolean update(Res_config_settings et) ;
    void updateBatch(List<Res_config_settings> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_config_settings get(Long key) ;
    Res_config_settings getDraft(Res_config_settings et) ;
    boolean checkKey(Res_config_settings et) ;
    Res_config_settings getLatestSettings(Res_config_settings et) ;
    Res_config_settings get_default(Res_config_settings et) ;
    boolean save(Res_config_settings et) ;
    void saveBatch(List<Res_config_settings> list) ;
    Page<Res_config_settings> searchDefault(Res_config_settingsSearchContext context) ;
    List<Res_config_settings> selectByChartTemplateId(Long id);
    void resetByChartTemplateId(Long id);
    void resetByChartTemplateId(Collection<Long> ids);
    void removeByChartTemplateId(Long id);
    List<Res_config_settings> selectByDigestId(Long id);
    void resetByDigestId(Long id);
    void resetByDigestId(Collection<Long> ids);
    void removeByDigestId(Long id);
    List<Res_config_settings> selectByTemplateId(Long id);
    void resetByTemplateId(Long id);
    void resetByTemplateId(Collection<Long> ids);
    void removeByTemplateId(Long id);
    List<Res_config_settings> selectByDepositDefaultProductId(Long id);
    void resetByDepositDefaultProductId(Long id);
    void resetByDepositDefaultProductId(Collection<Long> ids);
    void removeByDepositDefaultProductId(Long id);
    List<Res_config_settings> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Res_config_settings> selectByAuthSignupTemplateUserId(Long id);
    void resetByAuthSignupTemplateUserId(Long id);
    void resetByAuthSignupTemplateUserId(Collection<Long> ids);
    void removeByAuthSignupTemplateUserId(Long id);
    List<Res_config_settings> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_config_settings> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Res_config_settings> selectByDefaultSaleOrderTemplateId(Long id);
    void resetByDefaultSaleOrderTemplateId(Long id);
    void resetByDefaultSaleOrderTemplateId(Collection<Long> ids);
    void removeByDefaultSaleOrderTemplateId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_config_settings> getResConfigSettingsByIds(List<Long> ids) ;
    List<Res_config_settings> getResConfigSettingsByEntities(List<Res_config_settings> entities) ;
}


