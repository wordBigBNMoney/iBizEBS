package cn.ibizlab.businesscentral.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_registration;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_registrationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Event_registration] 服务对象接口
 */
public interface IEvent_registrationService extends IService<Event_registration>{

    boolean create(Event_registration et) ;
    void createBatch(List<Event_registration> list) ;
    boolean update(Event_registration et) ;
    void updateBatch(List<Event_registration> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Event_registration get(Long key) ;
    Event_registration getDraft(Event_registration et) ;
    boolean checkKey(Event_registration et) ;
    boolean save(Event_registration et) ;
    void saveBatch(List<Event_registration> list) ;
    Page<Event_registration> searchDefault(Event_registrationSearchContext context) ;
    List<Event_registration> selectByEventTicketId(Long id);
    void resetByEventTicketId(Long id);
    void resetByEventTicketId(Collection<Long> ids);
    void removeByEventTicketId(Long id);
    List<Event_registration> selectByEventId(Long id);
    void resetByEventId(Long id);
    void resetByEventId(Collection<Long> ids);
    void removeByEventId(Long id);
    List<Event_registration> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Event_registration> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Event_registration> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Event_registration> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Event_registration> selectBySaleOrderLineId(Long id);
    void removeBySaleOrderLineId(Collection<Long> ids);
    void removeBySaleOrderLineId(Long id);
    List<Event_registration> selectBySaleOrderId(Long id);
    void removeBySaleOrderId(Collection<Long> ids);
    void removeBySaleOrderId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Event_registration> getEventRegistrationByIds(List<Long> ids) ;
    List<Event_registration> getEventRegistrationByEntities(List<Event_registration> entities) ;
}


