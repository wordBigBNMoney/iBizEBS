package cn.ibizlab.businesscentral.core.odoo_gamification.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[游戏化目标]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "GAMIFICATION_GOAL",resultMap = "Gamification_goalResultMap")
public class Gamification_goal extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 开始日期
     */
    @DEField(name = "start_date")
    @TableField(value = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;
    /**
     * 更新
     */
    @DEField(name = "to_update")
    @TableField(value = "to_update")
    @JSONField(name = "to_update")
    @JsonProperty("to_update")
    private Boolean toUpdate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 提醒延迟
     */
    @DEField(name = "remind_update_delay")
    @TableField(value = "remind_update_delay")
    @JSONField(name = "remind_update_delay")
    @JsonProperty("remind_update_delay")
    private Integer remindUpdateDelay;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 完整性
     */
    @TableField(exist = false)
    @JSONField(name = "completeness")
    @JsonProperty("completeness")
    private Double completeness;
    /**
     * 达到
     */
    @DEField(name = "target_goal")
    @TableField(value = "target_goal")
    @JSONField(name = "target_goal")
    @JsonProperty("target_goal")
    private Double targetGoal;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 关闭的目标
     */
    @TableField(value = "closed")
    @JSONField(name = "closed")
    @JsonProperty("closed")
    private Boolean closed;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最近更新
     */
    @DEField(name = "last_update")
    @TableField(value = "last_update")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_update" , format="yyyy-MM-dd")
    @JsonProperty("last_update")
    private Timestamp lastUpdate;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 结束日期
     */
    @DEField(name = "end_date")
    @TableField(value = "end_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "end_date" , format="yyyy-MM-dd")
    @JsonProperty("end_date")
    private Timestamp endDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 当前值
     */
    @TableField(value = "current")
    @JSONField(name = "current")
    @JsonProperty("current")
    private Double current;
    /**
     * 定义说明
     */
    @TableField(exist = false)
    @JSONField(name = "definition_description")
    @JsonProperty("definition_description")
    private String definitionDescription;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 后缀
     */
    @TableField(exist = false)
    @JSONField(name = "definition_suffix")
    @JsonProperty("definition_suffix")
    private String definitionSuffix;
    /**
     * 挑战
     */
    @TableField(exist = false)
    @JSONField(name = "challenge_id_text")
    @JsonProperty("challenge_id_text")
    private String challengeIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 目标定义
     */
    @TableField(exist = false)
    @JSONField(name = "definition_id_text")
    @JsonProperty("definition_id_text")
    private String definitionIdText;
    /**
     * 显示为
     */
    @TableField(exist = false)
    @JSONField(name = "definition_display")
    @JsonProperty("definition_display")
    private String definitionDisplay;
    /**
     * 挑战行
     */
    @TableField(exist = false)
    @JSONField(name = "line_id_text")
    @JsonProperty("line_id_text")
    private String lineIdText;
    /**
     * 用户
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 目标绩效
     */
    @TableField(exist = false)
    @JSONField(name = "definition_condition")
    @JsonProperty("definition_condition")
    private String definitionCondition;
    /**
     * 计算模式
     */
    @TableField(exist = false)
    @JSONField(name = "computation_mode")
    @JsonProperty("computation_mode")
    private String computationMode;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 挑战行
     */
    @DEField(name = "line_id")
    @TableField(value = "line_id")
    @JSONField(name = "line_id")
    @JsonProperty("line_id")
    private Long lineId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 目标定义
     */
    @DEField(name = "definition_id")
    @TableField(value = "definition_id")
    @JSONField(name = "definition_id")
    @JsonProperty("definition_id")
    private Long definitionId;
    /**
     * 挑战
     */
    @DEField(name = "challenge_id")
    @TableField(value = "challenge_id")
    @JSONField(name = "challenge_id")
    @JsonProperty("challenge_id")
    private Long challengeId;
    /**
     * 用户
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge_line odooLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge odooChallenge;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_definition odooDefinition;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatStartDate(){
        if (this.startDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(startDate);
    }
    /**
     * 设置 [更新]
     */
    public void setToUpdate(Boolean toUpdate){
        this.toUpdate = toUpdate ;
        this.modify("to_update",toUpdate);
    }

    /**
     * 设置 [提醒延迟]
     */
    public void setRemindUpdateDelay(Integer remindUpdateDelay){
        this.remindUpdateDelay = remindUpdateDelay ;
        this.modify("remind_update_delay",remindUpdateDelay);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [达到]
     */
    public void setTargetGoal(Double targetGoal){
        this.targetGoal = targetGoal ;
        this.modify("target_goal",targetGoal);
    }

    /**
     * 设置 [关闭的目标]
     */
    public void setClosed(Boolean closed){
        this.closed = closed ;
        this.modify("closed",closed);
    }

    /**
     * 设置 [最近更新]
     */
    public void setLastUpdate(Timestamp lastUpdate){
        this.lastUpdate = lastUpdate ;
        this.modify("last_update",lastUpdate);
    }

    /**
     * 格式化日期 [最近更新]
     */
    public String formatLastUpdate(){
        if (this.lastUpdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(lastUpdate);
    }
    /**
     * 设置 [结束日期]
     */
    public void setEndDate(Timestamp endDate){
        this.endDate = endDate ;
        this.modify("end_date",endDate);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatEndDate(){
        if (this.endDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(endDate);
    }
    /**
     * 设置 [当前值]
     */
    public void setCurrent(Double current){
        this.current = current ;
        this.modify("current",current);
    }

    /**
     * 设置 [挑战行]
     */
    public void setLineId(Long lineId){
        this.lineId = lineId ;
        this.modify("line_id",lineId);
    }

    /**
     * 设置 [目标定义]
     */
    public void setDefinitionId(Long definitionId){
        this.definitionId = definitionId ;
        this.modify("definition_id",definitionId);
    }

    /**
     * 设置 [挑战]
     */
    public void setChallengeId(Long challengeId){
        this.challengeId = challengeId ;
        this.modify("challenge_id",challengeId);
    }

    /**
     * 设置 [用户]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


