package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_productionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_production] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-production", fallback = mrp_productionFallback.class)
public interface mrp_productionFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_productions")
    Mrp_production create(@RequestBody Mrp_production mrp_production);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/batch")
    Boolean createBatch(@RequestBody List<Mrp_production> mrp_productions);



    @RequestMapping(method = RequestMethod.GET, value = "/mrp_productions/{id}")
    Mrp_production get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/search")
    Page<Mrp_production> search(@RequestBody Mrp_productionSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_productions/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_productions/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_productions/{id}")
    Mrp_production update(@PathVariable("id") Long id,@RequestBody Mrp_production mrp_production);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_productions/batch")
    Boolean updateBatch(@RequestBody List<Mrp_production> mrp_productions);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_productions/select")
    Page<Mrp_production> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_productions/getdraft")
    Mrp_production getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/checkkey")
    Boolean checkKey(@RequestBody Mrp_production mrp_production);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/save")
    Boolean save(@RequestBody Mrp_production mrp_production);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_production> mrp_productions);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/searchdefault")
    Page<Mrp_production> searchDefault(@RequestBody Mrp_productionSearchContext context);


}
