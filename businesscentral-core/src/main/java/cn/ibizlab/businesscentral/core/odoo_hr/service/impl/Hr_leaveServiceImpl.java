package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leaveSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leaveService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_leaveMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[休假] 服务对象接口实现
 */
@Slf4j
@Service("Hr_leaveServiceImpl")
public class Hr_leaveServiceImpl extends EBSServiceImpl<Hr_leaveMapper, Hr_leave> implements IHr_leaveService {


    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leaveService hrLeaveService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_leavesService resourceCalendarLeavesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_eventService calendarEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employee_categoryService hrEmployeeCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_typeService hrLeaveTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.leave" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_leave et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_leaveService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_leave> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_leave et) {
        Hr_leave old = new Hr_leave() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_leaveService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_leaveService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_leave> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        hrLeaveService.resetByParentId(key);
        resourceCalendarLeavesService.resetByHolidayId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        hrLeaveService.resetByParentId(idList);
        resourceCalendarLeavesService.resetByHolidayId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_leave get(Long key) {
        Hr_leave et = getById(key);
        if(et==null){
            et=new Hr_leave();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_leave getDraft(Hr_leave et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_leave et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_leave et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_leave et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_leave> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_leave> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_leave> selectByMeetingId(Long id) {
        return baseMapper.selectByMeetingId(id);
    }
    @Override
    public void resetByMeetingId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("meeting_id",null).eq("meeting_id",id));
    }

    @Override
    public void resetByMeetingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("meeting_id",null).in("meeting_id",ids));
    }

    @Override
    public void removeByMeetingId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("meeting_id",id));
    }

	@Override
    public List<Hr_leave> selectByDepartmentId(Long id) {
        return baseMapper.selectByDepartmentId(id);
    }
    @Override
    public void resetByDepartmentId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("department_id",null).eq("department_id",id));
    }

    @Override
    public void resetByDepartmentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("department_id",null).in("department_id",ids));
    }

    @Override
    public void removeByDepartmentId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("department_id",id));
    }

	@Override
    public List<Hr_leave> selectByCategoryId(Long id) {
        return baseMapper.selectByCategoryId(id);
    }
    @Override
    public void resetByCategoryId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("category_id",null).eq("category_id",id));
    }

    @Override
    public void resetByCategoryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("category_id",null).in("category_id",ids));
    }

    @Override
    public void removeByCategoryId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("category_id",id));
    }

	@Override
    public List<Hr_leave> selectByEmployeeId(Long id) {
        return baseMapper.selectByEmployeeId(id);
    }
    @Override
    public void resetByEmployeeId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("employee_id",null).eq("employee_id",id));
    }

    @Override
    public void resetByEmployeeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("employee_id",null).in("employee_id",ids));
    }

    @Override
    public void removeByEmployeeId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("employee_id",id));
    }

	@Override
    public List<Hr_leave> selectByFirstApproverId(Long id) {
        return baseMapper.selectByFirstApproverId(id);
    }
    @Override
    public void resetByFirstApproverId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("first_approver_id",null).eq("first_approver_id",id));
    }

    @Override
    public void resetByFirstApproverId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("first_approver_id",null).in("first_approver_id",ids));
    }

    @Override
    public void removeByFirstApproverId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("first_approver_id",id));
    }

	@Override
    public List<Hr_leave> selectByManagerId(Long id) {
        return baseMapper.selectByManagerId(id);
    }
    @Override
    public void resetByManagerId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("manager_id",null).eq("manager_id",id));
    }

    @Override
    public void resetByManagerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("manager_id",null).in("manager_id",ids));
    }

    @Override
    public void removeByManagerId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("manager_id",id));
    }

	@Override
    public List<Hr_leave> selectBySecondApproverId(Long id) {
        return baseMapper.selectBySecondApproverId(id);
    }
    @Override
    public void resetBySecondApproverId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("second_approver_id",null).eq("second_approver_id",id));
    }

    @Override
    public void resetBySecondApproverId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("second_approver_id",null).in("second_approver_id",ids));
    }

    @Override
    public void removeBySecondApproverId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("second_approver_id",id));
    }

	@Override
    public List<Hr_leave> selectByHolidayStatusId(Long id) {
        return baseMapper.selectByHolidayStatusId(id);
    }
    @Override
    public void resetByHolidayStatusId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("holiday_status_id",null).eq("holiday_status_id",id));
    }

    @Override
    public void resetByHolidayStatusId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("holiday_status_id",null).in("holiday_status_id",ids));
    }

    @Override
    public void removeByHolidayStatusId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("holiday_status_id",id));
    }

	@Override
    public List<Hr_leave> selectByParentId(Long id) {
        return baseMapper.selectByParentId(id);
    }
    @Override
    public void resetByParentId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("parent_id",null).eq("parent_id",id));
    }

    @Override
    public void resetByParentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("parent_id",null).in("parent_id",ids));
    }

    @Override
    public void removeByParentId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("parent_id",id));
    }

	@Override
    public List<Hr_leave> selectByModeCompanyId(Long id) {
        return baseMapper.selectByModeCompanyId(id);
    }
    @Override
    public void resetByModeCompanyId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("mode_company_id",null).eq("mode_company_id",id));
    }

    @Override
    public void resetByModeCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("mode_company_id",null).in("mode_company_id",ids));
    }

    @Override
    public void removeByModeCompanyId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("mode_company_id",id));
    }

	@Override
    public List<Hr_leave> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("create_uid",id));
    }

	@Override
    public List<Hr_leave> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Hr_leave>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_leave>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("user_id",id));
    }

	@Override
    public List<Hr_leave> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_leave>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_leave> searchDefault(Hr_leaveSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_leave> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_leave>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_leave et){
        //实体关系[DER1N_HR_LEAVE__CALENDAR_EVENT__MEETING_ID]
        if(!ObjectUtils.isEmpty(et.getMeetingId())){
            cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event odooMeeting=et.getOdooMeeting();
            if(ObjectUtils.isEmpty(odooMeeting)){
                cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event majorEntity=calendarEventService.get(et.getMeetingId());
                et.setOdooMeeting(majorEntity);
                odooMeeting=majorEntity;
            }
            et.setMeetingIdText(odooMeeting.getName());
        }
        //实体关系[DER1N_HR_LEAVE__HR_DEPARTMENT__DEPARTMENT_ID]
        if(!ObjectUtils.isEmpty(et.getDepartmentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment=et.getOdooDepartment();
            if(ObjectUtils.isEmpty(odooDepartment)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department majorEntity=hrDepartmentService.get(et.getDepartmentId());
                et.setOdooDepartment(majorEntity);
                odooDepartment=majorEntity;
            }
            et.setDepartmentIdText(odooDepartment.getName());
        }
        //实体关系[DER1N_HR_LEAVE__HR_EMPLOYEE_CATEGORY__CATEGORY_ID]
        if(!ObjectUtils.isEmpty(et.getCategoryId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee_category odooCategory=et.getOdooCategory();
            if(ObjectUtils.isEmpty(odooCategory)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee_category majorEntity=hrEmployeeCategoryService.get(et.getCategoryId());
                et.setOdooCategory(majorEntity);
                odooCategory=majorEntity;
            }
            et.setCategoryIdText(odooCategory.getName());
        }
        //实体关系[DER1N_HR_LEAVE__HR_EMPLOYEE__EMPLOYEE_ID]
        if(!ObjectUtils.isEmpty(et.getEmployeeId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee=et.getOdooEmployee();
            if(ObjectUtils.isEmpty(odooEmployee)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getEmployeeId());
                et.setOdooEmployee(majorEntity);
                odooEmployee=majorEntity;
            }
            et.setEmployeeIdText(odooEmployee.getName());
        }
        //实体关系[DER1N_HR_LEAVE__HR_EMPLOYEE__FIRST_APPROVER_ID]
        if(!ObjectUtils.isEmpty(et.getFirstApproverId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooFirstApprover=et.getOdooFirstApprover();
            if(ObjectUtils.isEmpty(odooFirstApprover)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getFirstApproverId());
                et.setOdooFirstApprover(majorEntity);
                odooFirstApprover=majorEntity;
            }
            et.setFirstApproverIdText(odooFirstApprover.getName());
        }
        //实体关系[DER1N_HR_LEAVE__HR_EMPLOYEE__MANAGER_ID]
        if(!ObjectUtils.isEmpty(et.getManagerId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooManager=et.getOdooManager();
            if(ObjectUtils.isEmpty(odooManager)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getManagerId());
                et.setOdooManager(majorEntity);
                odooManager=majorEntity;
            }
            et.setManagerIdText(odooManager.getName());
        }
        //实体关系[DER1N_HR_LEAVE__HR_EMPLOYEE__SECOND_APPROVER_ID]
        if(!ObjectUtils.isEmpty(et.getSecondApproverId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooSecondApprover=et.getOdooSecondApprover();
            if(ObjectUtils.isEmpty(odooSecondApprover)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getSecondApproverId());
                et.setOdooSecondApprover(majorEntity);
                odooSecondApprover=majorEntity;
            }
            et.setSecondApproverIdText(odooSecondApprover.getName());
        }
        //实体关系[DER1N_HR_LEAVE__HR_LEAVE_TYPE__HOLIDAY_STATUS_ID]
        if(!ObjectUtils.isEmpty(et.getHolidayStatusId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_type odooHolidayStatus=et.getOdooHolidayStatus();
            if(ObjectUtils.isEmpty(odooHolidayStatus)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_type majorEntity=hrLeaveTypeService.get(et.getHolidayStatusId());
                et.setOdooHolidayStatus(majorEntity);
                odooHolidayStatus=majorEntity;
            }
            et.setLeaveTypeRequestUnit(odooHolidayStatus.getRequestUnit());
            et.setHolidayStatusIdText(odooHolidayStatus.getName());
            et.setValidationType(odooHolidayStatus.getValidationType());
        }
        //实体关系[DER1N_HR_LEAVE__HR_LEAVE__PARENT_ID]
        if(!ObjectUtils.isEmpty(et.getParentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave odooParent=et.getOdooParent();
            if(ObjectUtils.isEmpty(odooParent)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave majorEntity=hrLeaveService.get(et.getParentId());
                et.setOdooParent(majorEntity);
                odooParent=majorEntity;
            }
            et.setParentIdText(odooParent.getName());
        }
        //实体关系[DER1N_HR_LEAVE__RES_COMPANY__MODE_COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getModeCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooModeCompany=et.getOdooModeCompany();
            if(ObjectUtils.isEmpty(odooModeCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getModeCompanyId());
                et.setOdooModeCompany(majorEntity);
                odooModeCompany=majorEntity;
            }
            et.setModeCompanyIdText(odooModeCompany.getName());
        }
        //实体关系[DER1N_HR_LEAVE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_LEAVE__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_HR_LEAVE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_leave> getHrLeaveByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_leave> getHrLeaveByEntities(List<Hr_leave> entities) {
        List ids =new ArrayList();
        for(Hr_leave entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



