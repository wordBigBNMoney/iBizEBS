package cn.ibizlab.businesscentral.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_wizard_userSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[portal_wizard_user] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-portal:odoo-portal}", contextId = "portal-wizard-user", fallback = portal_wizard_userFallback.class)
public interface portal_wizard_userFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/portal_wizard_users/{id}")
    Portal_wizard_user get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users")
    Portal_wizard_user create(@RequestBody Portal_wizard_user portal_wizard_user);

    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/batch")
    Boolean createBatch(@RequestBody List<Portal_wizard_user> portal_wizard_users);





    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/search")
    Page<Portal_wizard_user> search(@RequestBody Portal_wizard_userSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizard_users/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizard_users/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/portal_wizard_users/{id}")
    Portal_wizard_user update(@PathVariable("id") Long id,@RequestBody Portal_wizard_user portal_wizard_user);

    @RequestMapping(method = RequestMethod.PUT, value = "/portal_wizard_users/batch")
    Boolean updateBatch(@RequestBody List<Portal_wizard_user> portal_wizard_users);


    @RequestMapping(method = RequestMethod.GET, value = "/portal_wizard_users/select")
    Page<Portal_wizard_user> select();


    @RequestMapping(method = RequestMethod.GET, value = "/portal_wizard_users/getdraft")
    Portal_wizard_user getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/checkkey")
    Boolean checkKey(@RequestBody Portal_wizard_user portal_wizard_user);


    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/save")
    Boolean save(@RequestBody Portal_wizard_user portal_wizard_user);

    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/savebatch")
    Boolean saveBatch(@RequestBody List<Portal_wizard_user> portal_wizard_users);



    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/searchdefault")
    Page<Portal_wizard_user> searchDefault(@RequestBody Portal_wizard_userSearchContext context);


}
