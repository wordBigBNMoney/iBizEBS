package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_bank_statement_line] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-bank-statement-line", fallback = account_bank_statement_lineFallback.class)
public interface account_bank_statement_lineFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_lines/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_lines/{id}")
    Account_bank_statement_line get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/search")
    Page<Account_bank_statement_line> search(@RequestBody Account_bank_statement_lineSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_lines/{id}")
    Account_bank_statement_line update(@PathVariable("id") Long id,@RequestBody Account_bank_statement_line account_bank_statement_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_lines/batch")
    Boolean updateBatch(@RequestBody List<Account_bank_statement_line> account_bank_statement_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines")
    Account_bank_statement_line create(@RequestBody Account_bank_statement_line account_bank_statement_line);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/batch")
    Boolean createBatch(@RequestBody List<Account_bank_statement_line> account_bank_statement_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_lines/select")
    Page<Account_bank_statement_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_lines/getdraft")
    Account_bank_statement_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/checkkey")
    Boolean checkKey(@RequestBody Account_bank_statement_line account_bank_statement_line);


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/save")
    Boolean save(@RequestBody Account_bank_statement_line account_bank_statement_line);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/savebatch")
    Boolean saveBatch(@RequestBody List<Account_bank_statement_line> account_bank_statement_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/searchdefault")
    Page<Account_bank_statement_line> searchDefault(@RequestBody Account_bank_statement_lineSearchContext context);


}
