package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_cashbox_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_cashbox_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_cashbox_line] 服务对象接口
 */
public interface IAccount_cashbox_lineService extends IService<Account_cashbox_line>{

    boolean create(Account_cashbox_line et) ;
    void createBatch(List<Account_cashbox_line> list) ;
    boolean update(Account_cashbox_line et) ;
    void updateBatch(List<Account_cashbox_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_cashbox_line get(Long key) ;
    Account_cashbox_line getDraft(Account_cashbox_line et) ;
    boolean checkKey(Account_cashbox_line et) ;
    boolean save(Account_cashbox_line et) ;
    void saveBatch(List<Account_cashbox_line> list) ;
    Page<Account_cashbox_line> searchDefault(Account_cashbox_lineSearchContext context) ;
    List<Account_cashbox_line> selectByCashboxId(Long id);
    void resetByCashboxId(Long id);
    void resetByCashboxId(Collection<Long> ids);
    void removeByCashboxId(Long id);
    List<Account_cashbox_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_cashbox_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_cashbox_line> getAccountCashboxLineByIds(List<Long> ids) ;
    List<Account_cashbox_line> getAccountCashboxLineByEntities(List<Account_cashbox_line> entities) ;
}


