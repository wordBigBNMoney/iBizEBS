package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_bom_line] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-bom-line", fallback = mrp_bom_lineFallback.class)
public interface mrp_bom_lineFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/search")
    Page<Mrp_bom_line> search(@RequestBody Mrp_bom_lineSearchContext context);




    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_bom_lines/{id}")
    Mrp_bom_line update(@PathVariable("id") Long id,@RequestBody Mrp_bom_line mrp_bom_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_bom_lines/batch")
    Boolean updateBatch(@RequestBody List<Mrp_bom_line> mrp_bom_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_bom_lines/{id}")
    Mrp_bom_line get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_bom_lines/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_bom_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines")
    Mrp_bom_line create(@RequestBody Mrp_bom_line mrp_bom_line);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/batch")
    Boolean createBatch(@RequestBody List<Mrp_bom_line> mrp_bom_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_bom_lines/select")
    Page<Mrp_bom_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_bom_lines/getdraft")
    Mrp_bom_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/checkkey")
    Boolean checkKey(@RequestBody Mrp_bom_line mrp_bom_line);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/save")
    Boolean save(@RequestBody Mrp_bom_line mrp_bom_line);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_bom_line> mrp_bom_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/searchdefault")
    Page<Mrp_bom_line> searchDefault(@RequestBody Mrp_bom_lineSearchContext context);


}
