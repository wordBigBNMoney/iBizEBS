package cn.ibizlab.businesscentral.core.util.helper;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.javers.core.diff.Diff;

public class EBSServiceImpl<M extends BaseMapper<T>, T extends EntityMP> extends ServiceImpl<M, T> {

    /**
     * 获取消息子类型
     * @param et
     * @param diff
     * @return
     */
    public Mail_message_subtype getMessageSubType(T et, Diff diff){
        Mail_message_subtype message_subtype = new Mail_message_subtype();
        message_subtype.setId(2l);
        return message_subtype ;
    }

    public String getIrModel(){
        return "";
    }

    /**
     * 获取实体实例
     * @param clazz
     * @return
     * @throws Exception
     */
    public T getInstance(Class<T> clazz) throws Exception {
        return clazz.newInstance();
    }

}
