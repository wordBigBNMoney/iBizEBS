package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_distributionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_analytic_distribution] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-analytic-distribution", fallback = account_analytic_distributionFallback.class)
public interface account_analytic_distributionFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions")
    Account_analytic_distribution create(@RequestBody Account_analytic_distribution account_analytic_distribution);

    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/batch")
    Boolean createBatch(@RequestBody List<Account_analytic_distribution> account_analytic_distributions);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_distributions/{id}")
    Account_analytic_distribution update(@PathVariable("id") Long id,@RequestBody Account_analytic_distribution account_analytic_distribution);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_distributions/batch")
    Boolean updateBatch(@RequestBody List<Account_analytic_distribution> account_analytic_distributions);




    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/search")
    Page<Account_analytic_distribution> search(@RequestBody Account_analytic_distributionSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_distributions/{id}")
    Account_analytic_distribution get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_distributions/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_distributions/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_distributions/select")
    Page<Account_analytic_distribution> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_distributions/getdraft")
    Account_analytic_distribution getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/checkkey")
    Boolean checkKey(@RequestBody Account_analytic_distribution account_analytic_distribution);


    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/save")
    Boolean save(@RequestBody Account_analytic_distribution account_analytic_distribution);

    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/savebatch")
    Boolean saveBatch(@RequestBody List<Account_analytic_distribution> account_analytic_distributions);



    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/searchdefault")
    Page<Account_analytic_distribution> searchDefault(@RequestBody Account_analytic_distributionSearchContext context);


}
