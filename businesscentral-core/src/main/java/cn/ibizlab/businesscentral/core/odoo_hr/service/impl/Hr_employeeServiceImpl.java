package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employeeSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_employeeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[员工] 服务对象接口实现
 */
@Slf4j
@Service("Hr_employeeServiceImpl")
public class Hr_employeeServiceImpl extends EBSServiceImpl<Hr_employeeMapper, Hr_employee> implements IHr_employeeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badge_user_wizardService gamificationBadgeUserWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badge_userService gamificationBadgeUserService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService hrApplicantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_attendanceService hrAttendanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contractService hrContractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employee_skillService hrEmployeeSkillService;

    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expenseService hrExpenseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_allocationService hrLeaveAllocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_reportService hrLeaveReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leaveService hrLeaveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_resume_lineService hrResumeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipmentService maintenanceEquipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_requestService maintenanceRequestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendarService resourceCalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_resourceService resourceResourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_bankService resPartnerBankService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.employee" ;
    }

    private boolean messageinfo = true ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_employee et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_employeeService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_employee> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_employee et) {
        Hr_employee old = new Hr_employee() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_employeeService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_employeeService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_employee> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        gamificationBadgeUserWizardService.resetByEmployeeId(key);
        gamificationBadgeUserService.resetByEmployeeId(key);
        hrApplicantService.resetByEmpId(key);
        hrAttendanceService.removeByEmployeeId(key);
        hrContractService.resetByEmployeeId(key);
        hrDepartmentService.resetByManagerId(key);
        hrEmployeeService.resetByCoachId(key);
        hrEmployeeService.resetByParentId(key);
        hrExpenseSheetService.resetByEmployeeId(key);
        hrExpenseService.resetByEmployeeId(key);
        hrJobService.resetByManagerId(key);
        hrLeaveAllocationService.resetByEmployeeId(key);
        hrLeaveAllocationService.resetByFirstApproverId(key);
        hrLeaveAllocationService.resetBySecondApproverId(key);
        hrLeaveService.resetByEmployeeId(key);
        hrLeaveService.resetByFirstApproverId(key);
        hrLeaveService.resetByManagerId(key);
        hrLeaveService.resetBySecondApproverId(key);
        maintenanceEquipmentService.resetByEmployeeId(key);
        maintenanceRequestService.resetByEmployeeId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        gamificationBadgeUserWizardService.resetByEmployeeId(idList);
        gamificationBadgeUserService.resetByEmployeeId(idList);
        hrApplicantService.resetByEmpId(idList);
        hrAttendanceService.removeByEmployeeId(idList);
        hrContractService.resetByEmployeeId(idList);
        hrDepartmentService.resetByManagerId(idList);
        hrEmployeeService.resetByCoachId(idList);
        hrEmployeeService.resetByParentId(idList);
        hrExpenseSheetService.resetByEmployeeId(idList);
        hrExpenseService.resetByEmployeeId(idList);
        hrJobService.resetByManagerId(idList);
        hrLeaveAllocationService.resetByEmployeeId(idList);
        hrLeaveAllocationService.resetByFirstApproverId(idList);
        hrLeaveAllocationService.resetBySecondApproverId(idList);
        hrLeaveService.resetByEmployeeId(idList);
        hrLeaveService.resetByFirstApproverId(idList);
        hrLeaveService.resetByManagerId(idList);
        hrLeaveService.resetBySecondApproverId(idList);
        maintenanceEquipmentService.resetByEmployeeId(idList);
        maintenanceRequestService.resetByEmployeeId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_employee get(Long key) {
        Hr_employee et = getById(key);
        if(et==null){
            et=new Hr_employee();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_employee getDraft(Hr_employee et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_employee et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_employee et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_employee et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_employee> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_employee> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_employee> selectByLeaveManagerId(Long id) {
        return baseMapper.selectByLeaveManagerId(id);
    }
    @Override
    public void removeByLeaveManagerId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("leave_manager_id",id));
    }

	@Override
    public List<Hr_employee> selectByLastAttendanceId(Long id) {
        return baseMapper.selectByLastAttendanceId(id);
    }
    @Override
    public void resetByLastAttendanceId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("last_attendance_id",null).eq("last_attendance_id",id));
    }

    @Override
    public void resetByLastAttendanceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("last_attendance_id",null).in("last_attendance_id",ids));
    }

    @Override
    public void removeByLastAttendanceId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("last_attendance_id",id));
    }

	@Override
    public List<Hr_employee> selectByDepartmentId(Long id) {
        return baseMapper.selectByDepartmentId(id);
    }
    @Override
    public void resetByDepartmentId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("department_id",null).eq("department_id",id));
    }

    @Override
    public void resetByDepartmentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("department_id",null).in("department_id",ids));
    }

    @Override
    public void removeByDepartmentId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("department_id",id));
    }

	@Override
    public List<Hr_employee> selectByCoachId(Long id) {
        return baseMapper.selectByCoachId(id);
    }
    @Override
    public void resetByCoachId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("coach_id",null).eq("coach_id",id));
    }

    @Override
    public void resetByCoachId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("coach_id",null).in("coach_id",ids));
    }

    @Override
    public void removeByCoachId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("coach_id",id));
    }

	@Override
    public List<Hr_employee> selectByParentId(Long id) {
        return baseMapper.selectByParentId(id);
    }
    @Override
    public void resetByParentId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("parent_id",null).eq("parent_id",id));
    }

    @Override
    public void resetByParentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("parent_id",null).in("parent_id",ids));
    }

    @Override
    public void removeByParentId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("parent_id",id));
    }

	@Override
    public List<Hr_employee> selectByJobId(Long id) {
        return baseMapper.selectByJobId(id);
    }
    @Override
    public void resetByJobId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("job_id",null).eq("job_id",id));
    }

    @Override
    public void resetByJobId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("job_id",null).in("job_id",ids));
    }

    @Override
    public void removeByJobId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("job_id",id));
    }

	@Override
    public List<Hr_employee> selectByResourceCalendarId(Long id) {
        return baseMapper.selectByResourceCalendarId(id);
    }
    @Override
    public void resetByResourceCalendarId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("resource_calendar_id",null).eq("resource_calendar_id",id));
    }

    @Override
    public void resetByResourceCalendarId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("resource_calendar_id",null).in("resource_calendar_id",ids));
    }

    @Override
    public void removeByResourceCalendarId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("resource_calendar_id",id));
    }

	@Override
    public List<Hr_employee> selectByResourceId(Long id) {
        return baseMapper.selectByResourceId(id);
    }
    @Override
    public List<Hr_employee> selectByResourceId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Hr_employee>().in("id",ids));
    }

    @Override
    public void removeByResourceId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("resource_id",id));
    }

	@Override
    public List<Hr_employee> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("company_id",id));
    }

	@Override
    public List<Hr_employee> selectByCountryId(Long id) {
        return baseMapper.selectByCountryId(id);
    }
    @Override
    public void resetByCountryId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("country_id",null).eq("country_id",id));
    }

    @Override
    public void resetByCountryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("country_id",null).in("country_id",ids));
    }

    @Override
    public void removeByCountryId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("country_id",id));
    }

	@Override
    public List<Hr_employee> selectByCountryOfBirth(Long id) {
        return baseMapper.selectByCountryOfBirth(id);
    }
    @Override
    public void resetByCountryOfBirth(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("country_of_birth",null).eq("country_of_birth",id));
    }

    @Override
    public void resetByCountryOfBirth(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("country_of_birth",null).in("country_of_birth",ids));
    }

    @Override
    public void removeByCountryOfBirth(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("country_of_birth",id));
    }

	@Override
    public List<Hr_employee> selectByBankAccountId(Long id) {
        return baseMapper.selectByBankAccountId(id);
    }
    @Override
    public void resetByBankAccountId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("bank_account_id",null).eq("bank_account_id",id));
    }

    @Override
    public void resetByBankAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("bank_account_id",null).in("bank_account_id",ids));
    }

    @Override
    public void removeByBankAccountId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("bank_account_id",id));
    }

	@Override
    public List<Hr_employee> selectByAddressHomeId(Long id) {
        return baseMapper.selectByAddressHomeId(id);
    }
    @Override
    public void resetByAddressHomeId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("address_home_id",null).eq("address_home_id",id));
    }

    @Override
    public void resetByAddressHomeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("address_home_id",null).in("address_home_id",ids));
    }

    @Override
    public void removeByAddressHomeId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("address_home_id",id));
    }

	@Override
    public List<Hr_employee> selectByAddressId(Long id) {
        return baseMapper.selectByAddressId(id);
    }
    @Override
    public void resetByAddressId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("address_id",null).eq("address_id",id));
    }

    @Override
    public void resetByAddressId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("address_id",null).in("address_id",ids));
    }

    @Override
    public void removeByAddressId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("address_id",id));
    }

	@Override
    public List<Hr_employee> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("create_uid",id));
    }

	@Override
    public List<Hr_employee> selectByExpenseManagerId(Long id) {
        return baseMapper.selectByExpenseManagerId(id);
    }
    @Override
    public void resetByExpenseManagerId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("expense_manager_id",null).eq("expense_manager_id",id));
    }

    @Override
    public void resetByExpenseManagerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("expense_manager_id",null).in("expense_manager_id",ids));
    }

    @Override
    public void removeByExpenseManagerId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("expense_manager_id",id));
    }

	@Override
    public List<Hr_employee> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Hr_employee>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_employee>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("user_id",id));
    }

	@Override
    public List<Hr_employee> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_employee>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_employee> searchDefault(Hr_employeeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_employee> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_employee>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 首选表格
     */
    @Override
    public Page<Hr_employee> searchMaster(Hr_employeeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_employee> pages=baseMapper.searchMaster(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_employee>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_employee et){
        //实体关系[DER1N_HR_EMPLOYEE__HR_DEPARTMENT__DEPARTMENT_ID]
        if(!ObjectUtils.isEmpty(et.getDepartmentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment=et.getOdooDepartment();
            if(ObjectUtils.isEmpty(odooDepartment)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department majorEntity=hrDepartmentService.get(et.getDepartmentId());
                et.setOdooDepartment(majorEntity);
                odooDepartment=majorEntity;
            }
            et.setDepartmentIdText(odooDepartment.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__HR_EMPLOYEE__COACH_ID]
        if(!ObjectUtils.isEmpty(et.getCoachId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooCoach=et.getOdooCoach();
            if(ObjectUtils.isEmpty(odooCoach)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getCoachId());
                et.setOdooCoach(majorEntity);
                odooCoach=majorEntity;
            }
            et.setCoachIdText(odooCoach.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__HR_EMPLOYEE__PARENT_ID]
        if(!ObjectUtils.isEmpty(et.getParentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooParent=et.getOdooParent();
            if(ObjectUtils.isEmpty(odooParent)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getParentId());
                et.setOdooParent(majorEntity);
                odooParent=majorEntity;
            }
            et.setParentIdText(odooParent.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__HR_JOB__JOB_ID]
        if(!ObjectUtils.isEmpty(et.getJobId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job odooJob=et.getOdooJob();
            if(ObjectUtils.isEmpty(odooJob)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job majorEntity=hrJobService.get(et.getJobId());
                et.setOdooJob(majorEntity);
                odooJob=majorEntity;
            }
            et.setJobIdText(odooJob.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RESOURCE_CALENDAR__RESOURCE_CALENDAR_ID]
        if(!ObjectUtils.isEmpty(et.getResourceCalendarId())){
            cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooResourceCalendar=et.getOdooResourceCalendar();
            if(ObjectUtils.isEmpty(odooResourceCalendar)){
                cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar majorEntity=resourceCalendarService.get(et.getResourceCalendarId());
                et.setOdooResourceCalendar(majorEntity);
                odooResourceCalendar=majorEntity;
            }
            et.setResourceCalendarIdText(odooResourceCalendar.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RESOURCE_RESOURCE__RESOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getResourceId())){
            cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_resource odooResource=et.getOdooResource();
            if(ObjectUtils.isEmpty(odooResource)){
                cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_resource majorEntity=resourceResourceService.get(et.getResourceId());
                et.setOdooResource(majorEntity);
                odooResource=majorEntity;
            }
            et.setName(odooResource.getName());
            et.setTz(odooResource.getTz());
            et.setActive(odooResource.getActive());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_COUNTRY__COUNTRY_ID]
        if(!ObjectUtils.isEmpty(et.getCountryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry=et.getOdooCountry();
            if(ObjectUtils.isEmpty(odooCountry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getCountryId());
                et.setOdooCountry(majorEntity);
                odooCountry=majorEntity;
            }
            et.setCountryIdText(odooCountry.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_COUNTRY__COUNTRY_OF_BIRTH]
        if(!ObjectUtils.isEmpty(et.getCountryOfBirth())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountryOf=et.getOdooCountryOf();
            if(ObjectUtils.isEmpty(odooCountryOf)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getCountryOfBirth());
                et.setOdooCountryOf(majorEntity);
                odooCountryOf=majorEntity;
            }
            et.setCountryOfBirthText(odooCountryOf.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_PARTNER__ADDRESS_HOME_ID]
        if(!ObjectUtils.isEmpty(et.getAddressHomeId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAddressHome=et.getOdooAddressHome();
            if(ObjectUtils.isEmpty(odooAddressHome)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getAddressHomeId());
                et.setOdooAddressHome(majorEntity);
                odooAddressHome=majorEntity;
            }
            et.setAddressHomeIdText(odooAddressHome.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_PARTNER__ADDRESS_ID]
        if(!ObjectUtils.isEmpty(et.getAddressId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAddress=et.getOdooAddress();
            if(ObjectUtils.isEmpty(odooAddress)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getAddressId());
                et.setOdooAddress(majorEntity);
                odooAddress=majorEntity;
            }
            et.setAddressIdText(odooAddress.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_USERS__EXPENSE_MANAGER_ID]
        if(!ObjectUtils.isEmpty(et.getExpenseManagerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooExpenseManager=et.getOdooExpenseManager();
            if(ObjectUtils.isEmpty(odooExpenseManager)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getExpenseManagerId());
                et.setOdooExpenseManager(majorEntity);
                odooExpenseManager=majorEntity;
            }
            et.setExpenseManagerIdText(odooExpenseManager.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_HR_EMPLOYEE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_employee> getHrEmployeeByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_employee> getHrEmployeeByEntities(List<Hr_employee> entities) {
        List ids =new ArrayList();
        for(Hr_employee entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



