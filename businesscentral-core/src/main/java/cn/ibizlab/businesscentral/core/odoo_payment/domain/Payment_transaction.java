package cn.ibizlab.businesscentral.core.odoo_payment.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[付款交易]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PAYMENT_TRANSACTION",resultMap = "Payment_transactionResultMap")
public class Payment_transaction extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private BigDecimal amount;
    /**
     * 电话
     */
    @DEField(name = "partner_phone")
    @TableField(value = "partner_phone")
    @JSONField(name = "partner_phone")
    @JsonProperty("partner_phone")
    private String partnerPhone;
    /**
     * EMail
     */
    @DEField(name = "partner_email")
    @TableField(value = "partner_email")
    @JSONField(name = "partner_email")
    @JsonProperty("partner_email")
    private String partnerEmail;
    /**
     * 回调方法
     */
    @DEField(name = "callback_method")
    @TableField(value = "callback_method")
    @JSONField(name = "callback_method")
    @JsonProperty("callback_method")
    private String callbackMethod;
    /**
     * 付款后返回网址
     */
    @DEField(name = "return_url")
    @TableField(value = "return_url")
    @JSONField(name = "return_url")
    @JsonProperty("return_url")
    private String returnUrl;
    /**
     * # 销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_ids_nbr")
    @JsonProperty("sale_order_ids_nbr")
    private Integer saleOrderIdsNbr;
    /**
     * 收单方参考
     */
    @DEField(name = "acquirer_reference")
    @TableField(value = "acquirer_reference")
    @JSONField(name = "acquirer_reference")
    @JsonProperty("acquirer_reference")
    private String acquirerReference;
    /**
     * 语言
     */
    @DEField(name = "partner_lang")
    @TableField(value = "partner_lang")
    @JSONField(name = "partner_lang")
    @JsonProperty("partner_lang")
    private String partnerLang;
    /**
     * 邮政编码
     */
    @DEField(name = "partner_zip")
    @TableField(value = "partner_zip")
    @JSONField(name = "partner_zip")
    @JsonProperty("partner_zip")
    private String partnerZip;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 验证日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 回调文档模型
     */
    @DEField(name = "callback_model_id")
    @TableField(value = "callback_model_id")
    @JSONField(name = "callback_model_id")
    @JsonProperty("callback_model_id")
    private Integer callbackModelId;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;
    /**
     * 销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    private String saleOrderIds;
    /**
     * 费用
     */
    @TableField(value = "fees")
    @JSONField(name = "fees")
    @JsonProperty("fees")
    private BigDecimal fees;
    /**
     * 消息
     */
    @DEField(name = "state_message")
    @TableField(value = "state_message")
    @JSONField(name = "state_message")
    @JsonProperty("state_message")
    private String stateMessage;
    /**
     * 回调文档 ID
     */
    @DEField(name = "callback_res_id")
    @TableField(value = "callback_res_id")
    @JSONField(name = "callback_res_id")
    @JsonProperty("callback_res_id")
    private Integer callbackResId;
    /**
     * 城市
     */
    @DEField(name = "partner_city")
    @TableField(value = "partner_city")
    @JSONField(name = "partner_city")
    @JsonProperty("partner_city")
    private String partnerCity;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * # 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_ids_nbr")
    @JsonProperty("invoice_ids_nbr")
    private Integer invoiceIdsNbr;
    /**
     * 类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 3D Secure HTML
     */
    @DEField(name = "html_3ds")
    @TableField(value = "html_3ds")
    @JSONField(name = "html_3ds")
    @JsonProperty("html_3ds")
    private String html3ds;
    /**
     * 合作伙伴名称
     */
    @DEField(name = "partner_name")
    @TableField(value = "partner_name")
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;
    /**
     * 参考
     */
    @TableField(value = "reference")
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;
    /**
     * 地址
     */
    @DEField(name = "partner_address")
    @TableField(value = "partner_address")
    @JSONField(name = "partner_address")
    @JsonProperty("partner_address")
    private String partnerAddress;
    /**
     * 付款是否已过账处理
     */
    @DEField(name = "is_processed")
    @TableField(value = "is_processed")
    @JSONField(name = "is_processed")
    @JsonProperty("is_processed")
    private Boolean isProcessed;
    /**
     * 回调哈希函数
     */
    @DEField(name = "callback_hash")
    @TableField(value = "callback_hash")
    @JSONField(name = "callback_hash")
    @JsonProperty("callback_hash")
    private String callbackHash;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 付款
     */
    @TableField(exist = false)
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    private String paymentIdText;
    /**
     * 客户
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "provider")
    @JsonProperty("provider")
    private String provider;
    /**
     * 收单方
     */
    @TableField(exist = false)
    @JSONField(name = "acquirer_id_text")
    @JsonProperty("acquirer_id_text")
    private String acquirerIdText;
    /**
     * 国家
     */
    @TableField(exist = false)
    @JSONField(name = "partner_country_id_text")
    @JsonProperty("partner_country_id_text")
    private String partnerCountryIdText;
    /**
     * 付款令牌
     */
    @TableField(exist = false)
    @JSONField(name = "payment_token_id_text")
    @JsonProperty("payment_token_id_text")
    private String paymentTokenIdText;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 国家
     */
    @DEField(name = "partner_country_id")
    @TableField(value = "partner_country_id")
    @JSONField(name = "partner_country_id")
    @JsonProperty("partner_country_id")
    private Long partnerCountryId;
    /**
     * 付款令牌
     */
    @DEField(name = "payment_token_id")
    @TableField(value = "payment_token_id")
    @JSONField(name = "payment_token_id")
    @JsonProperty("payment_token_id")
    private Long paymentTokenId;
    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 付款
     */
    @DEField(name = "payment_id")
    @TableField(value = "payment_id")
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    private Long paymentId;
    /**
     * 收单方
     */
    @DEField(name = "acquirer_id")
    @TableField(value = "acquirer_id")
    @JSONField(name = "acquirer_id")
    @JsonProperty("acquirer_id")
    private Long acquirerId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment odooPayment;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer odooAcquirer;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_token odooPaymentToken;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooPartnerCountry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [金额]
     */
    public void setAmount(BigDecimal amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [电话]
     */
    public void setPartnerPhone(String partnerPhone){
        this.partnerPhone = partnerPhone ;
        this.modify("partner_phone",partnerPhone);
    }

    /**
     * 设置 [EMail]
     */
    public void setPartnerEmail(String partnerEmail){
        this.partnerEmail = partnerEmail ;
        this.modify("partner_email",partnerEmail);
    }

    /**
     * 设置 [回调方法]
     */
    public void setCallbackMethod(String callbackMethod){
        this.callbackMethod = callbackMethod ;
        this.modify("callback_method",callbackMethod);
    }

    /**
     * 设置 [付款后返回网址]
     */
    public void setReturnUrl(String returnUrl){
        this.returnUrl = returnUrl ;
        this.modify("return_url",returnUrl);
    }

    /**
     * 设置 [收单方参考]
     */
    public void setAcquirerReference(String acquirerReference){
        this.acquirerReference = acquirerReference ;
        this.modify("acquirer_reference",acquirerReference);
    }

    /**
     * 设置 [语言]
     */
    public void setPartnerLang(String partnerLang){
        this.partnerLang = partnerLang ;
        this.modify("partner_lang",partnerLang);
    }

    /**
     * 设置 [邮政编码]
     */
    public void setPartnerZip(String partnerZip){
        this.partnerZip = partnerZip ;
        this.modify("partner_zip",partnerZip);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [验证日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [验证日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
    /**
     * 设置 [回调文档模型]
     */
    public void setCallbackModelId(Integer callbackModelId){
        this.callbackModelId = callbackModelId ;
        this.modify("callback_model_id",callbackModelId);
    }

    /**
     * 设置 [费用]
     */
    public void setFees(BigDecimal fees){
        this.fees = fees ;
        this.modify("fees",fees);
    }

    /**
     * 设置 [消息]
     */
    public void setStateMessage(String stateMessage){
        this.stateMessage = stateMessage ;
        this.modify("state_message",stateMessage);
    }

    /**
     * 设置 [回调文档 ID]
     */
    public void setCallbackResId(Integer callbackResId){
        this.callbackResId = callbackResId ;
        this.modify("callback_res_id",callbackResId);
    }

    /**
     * 设置 [城市]
     */
    public void setPartnerCity(String partnerCity){
        this.partnerCity = partnerCity ;
        this.modify("partner_city",partnerCity);
    }

    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [3D Secure HTML]
     */
    public void setHtml3ds(String html3ds){
        this.html3ds = html3ds ;
        this.modify("html_3ds",html3ds);
    }

    /**
     * 设置 [合作伙伴名称]
     */
    public void setPartnerName(String partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }

    /**
     * 设置 [参考]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [地址]
     */
    public void setPartnerAddress(String partnerAddress){
        this.partnerAddress = partnerAddress ;
        this.modify("partner_address",partnerAddress);
    }

    /**
     * 设置 [付款是否已过账处理]
     */
    public void setIsProcessed(Boolean isProcessed){
        this.isProcessed = isProcessed ;
        this.modify("is_processed",isProcessed);
    }

    /**
     * 设置 [回调哈希函数]
     */
    public void setCallbackHash(String callbackHash){
        this.callbackHash = callbackHash ;
        this.modify("callback_hash",callbackHash);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [国家]
     */
    public void setPartnerCountryId(Long partnerCountryId){
        this.partnerCountryId = partnerCountryId ;
        this.modify("partner_country_id",partnerCountryId);
    }

    /**
     * 设置 [付款令牌]
     */
    public void setPaymentTokenId(Long paymentTokenId){
        this.paymentTokenId = paymentTokenId ;
        this.modify("payment_token_id",paymentTokenId);
    }

    /**
     * 设置 [客户]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [付款]
     */
    public void setPaymentId(Long paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }

    /**
     * 设置 [收单方]
     */
    public void setAcquirerId(Long acquirerId){
        this.acquirerId = acquirerId ;
        this.modify("acquirer_id",acquirerId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


