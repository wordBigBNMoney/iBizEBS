package cn.ibizlab.businesscentral.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_services;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_servicesService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_fleet.mapper.Fleet_vehicle_log_servicesMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[车辆服务] 服务对象接口实现
 */
@Slf4j
@Service("Fleet_vehicle_log_servicesServiceImpl")
public class Fleet_vehicle_log_servicesServiceImpl extends EBSServiceImpl<Fleet_vehicle_log_servicesMapper, Fleet_vehicle_log_services> implements IFleet_vehicle_log_servicesService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_costService fleetVehicleCostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "fleet.vehicle.log.services" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Fleet_vehicle_log_services et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_log_servicesService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Fleet_vehicle_log_services> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Fleet_vehicle_log_services et) {
        Fleet_vehicle_log_services old = new Fleet_vehicle_log_services() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_log_servicesService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_log_servicesService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Fleet_vehicle_log_services> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Fleet_vehicle_log_services get(Long key) {
        Fleet_vehicle_log_services et = getById(key);
        if(et==null){
            et=new Fleet_vehicle_log_services();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Fleet_vehicle_log_services getDraft(Fleet_vehicle_log_services et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Fleet_vehicle_log_services et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Fleet_vehicle_log_services et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Fleet_vehicle_log_services et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Fleet_vehicle_log_services> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Fleet_vehicle_log_services> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Fleet_vehicle_log_services> selectByCostId(Long id) {
        return baseMapper.selectByCostId(id);
    }
    @Override
    public void removeByCostId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_services>().in("cost_id",ids));
    }

    @Override
    public void removeByCostId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_services>().eq("cost_id",id));
    }

	@Override
    public List<Fleet_vehicle_log_services> selectByPurchaserId(Long id) {
        return baseMapper.selectByPurchaserId(id);
    }
    @Override
    public void resetByPurchaserId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_services>().set("purchaser_id",null).eq("purchaser_id",id));
    }

    @Override
    public void resetByPurchaserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_services>().set("purchaser_id",null).in("purchaser_id",ids));
    }

    @Override
    public void removeByPurchaserId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_services>().eq("purchaser_id",id));
    }

	@Override
    public List<Fleet_vehicle_log_services> selectByVendorId(Long id) {
        return baseMapper.selectByVendorId(id);
    }
    @Override
    public void resetByVendorId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_services>().set("vendor_id",null).eq("vendor_id",id));
    }

    @Override
    public void resetByVendorId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_services>().set("vendor_id",null).in("vendor_id",ids));
    }

    @Override
    public void removeByVendorId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_services>().eq("vendor_id",id));
    }

	@Override
    public List<Fleet_vehicle_log_services> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_services>().eq("create_uid",id));
    }

	@Override
    public List<Fleet_vehicle_log_services> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_services>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Fleet_vehicle_log_services> searchDefault(Fleet_vehicle_log_servicesSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Fleet_vehicle_log_services> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Fleet_vehicle_log_services>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Fleet_vehicle_log_services et){
        //实体关系[DER1N_FLEET_VEHICLE_LOG_SERVICES__FLEET_VEHICLE_COST__COST_ID]
        if(!ObjectUtils.isEmpty(et.getCostId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost odooCost=et.getOdooCost();
            if(ObjectUtils.isEmpty(odooCost)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost majorEntity=fleetVehicleCostService.get(et.getCostId());
                et.setOdooCost(majorEntity);
                odooCost=majorEntity;
            }
            et.setVehicleId(odooCost.getVehicleId());
            et.setContractId(odooCost.getContractId());
            et.setOdometerUnit(odooCost.getOdometerUnit());
            et.setOdometer(odooCost.getOdometer());
            et.setAutoGenerated(odooCost.getAutoGenerated());
            et.setOdometerId(odooCost.getOdometerId());
            et.setParentId(odooCost.getParentId());
            et.setAmount(odooCost.getAmount());
            et.setCostSubtypeId(odooCost.getCostSubtypeId());
            et.setDescription(odooCost.getDescription());
            et.setCostType(odooCost.getCostType());
            et.setCostAmount(odooCost.getAmount());
            et.setName(odooCost.getName());
            et.setDate(odooCost.getDate());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_SERVICES__RES_PARTNER__PURCHASER_ID]
        if(!ObjectUtils.isEmpty(et.getPurchaserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPurchaser=et.getOdooPurchaser();
            if(ObjectUtils.isEmpty(odooPurchaser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPurchaserId());
                et.setOdooPurchaser(majorEntity);
                odooPurchaser=majorEntity;
            }
            et.setPurchaserIdText(odooPurchaser.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_SERVICES__RES_PARTNER__VENDOR_ID]
        if(!ObjectUtils.isEmpty(et.getVendorId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooVendor=et.getOdooVendor();
            if(ObjectUtils.isEmpty(odooVendor)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getVendorId());
                et.setOdooVendor(majorEntity);
                odooVendor=majorEntity;
            }
            et.setVendorIdText(odooVendor.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_SERVICES__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_SERVICES__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Fleet_vehicle_log_services> getFleetVehicleLogServicesByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Fleet_vehicle_log_services> getFleetVehicleLogServicesByEntities(List<Fleet_vehicle_log_services> entities) {
        List ids =new ArrayList();
        for(Fleet_vehicle_log_services entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



