package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_return_picking_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_return_picking_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_return_picking_line] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-return-picking-line", fallback = stock_return_picking_lineFallback.class)
public interface stock_return_picking_lineFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_picking_lines/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_picking_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_return_picking_lines/{id}")
    Stock_return_picking_line get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/search")
    Page<Stock_return_picking_line> search(@RequestBody Stock_return_picking_lineSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_return_picking_lines/{id}")
    Stock_return_picking_line update(@PathVariable("id") Long id,@RequestBody Stock_return_picking_line stock_return_picking_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_return_picking_lines/batch")
    Boolean updateBatch(@RequestBody List<Stock_return_picking_line> stock_return_picking_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines")
    Stock_return_picking_line create(@RequestBody Stock_return_picking_line stock_return_picking_line);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/batch")
    Boolean createBatch(@RequestBody List<Stock_return_picking_line> stock_return_picking_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_return_picking_lines/select")
    Page<Stock_return_picking_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_return_picking_lines/getdraft")
    Stock_return_picking_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/checkkey")
    Boolean checkKey(@RequestBody Stock_return_picking_line stock_return_picking_line);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/save")
    Boolean save(@RequestBody Stock_return_picking_line stock_return_picking_line);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_return_picking_line> stock_return_picking_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/searchdefault")
    Page<Stock_return_picking_line> searchDefault(@RequestBody Stock_return_picking_lineSearchContext context);


}
