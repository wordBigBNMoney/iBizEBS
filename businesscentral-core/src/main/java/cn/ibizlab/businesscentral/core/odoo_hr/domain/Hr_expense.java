package cn.ibizlab.businesscentral.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[费用]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "HR_EXPENSE",resultMap = "Hr_expenseResultMap")
public class Hr_expense extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "attachment_number")
    @JsonProperty("attachment_number")
    private Integer attachmentNumber;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 说明
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 总计
     */
    @DEField(name = "total_amount")
    @TableField(value = "total_amount")
    @JSONField(name = "total_amount")
    @JsonProperty("total_amount")
    private BigDecimal totalAmount;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 税率设置
     */
    @TableField(exist = false)
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    private String taxIds;
    /**
     * 单价
     */
    @DEField(name = "unit_amount")
    @TableField(value = "unit_amount")
    @JSONField(name = "unit_amount")
    @JsonProperty("unit_amount")
    private Double unitAmount;
    /**
     * 支付
     */
    @DEField(name = "payment_mode")
    @TableField(value = "payment_mode")
    @JSONField(name = "payment_mode")
    @JsonProperty("payment_mode")
    private String paymentMode;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 合计 (公司货币)
     */
    @DEField(name = "total_amount_company")
    @TableField(value = "total_amount_company")
    @JSONField(name = "total_amount_company")
    @JsonProperty("total_amount_company")
    private BigDecimal totalAmountCompany;
    /**
     * 由经理或会计人员明确地拒绝
     */
    @DEField(name = "is_refused")
    @TableField(value = "is_refused")
    @JSONField(name = "is_refused")
    @JsonProperty("is_refused")
    private Boolean isRefused;
    /**
     * 日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 信息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 数量
     */
    @TableField(value = "quantity")
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private Double quantity;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 小计
     */
    @DEField(name = "untaxed_amount")
    @TableField(value = "untaxed_amount")
    @JSONField(name = "untaxed_amount")
    @JsonProperty("untaxed_amount")
    private Double untaxedAmount;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 分析标签
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;
    /**
     * 备注...
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 账单参照
     */
    @TableField(value = "reference")
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 费用报表
     */
    @TableField(exist = false)
    @JSONField(name = "sheet_id_text")
    @JsonProperty("sheet_id_text")
    private String sheetIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 公司货币报告
     */
    @TableField(exist = false)
    @JSONField(name = "company_currency_id_text")
    @JsonProperty("company_currency_id_text")
    private String companyCurrencyIdText;
    /**
     * 员工
     */
    @TableField(exist = false)
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;
    /**
     * 销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_id_text")
    @JsonProperty("sale_order_id_text")
    private String saleOrderIdText;
    /**
     * 分析账户
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    private String analyticAccountIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 科目
     */
    @TableField(exist = false)
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 员工
     */
    @DEField(name = "employee_id")
    @TableField(value = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Long employeeId;
    /**
     * 销售订单
     */
    @DEField(name = "sale_order_id")
    @TableField(value = "sale_order_id")
    @JSONField(name = "sale_order_id")
    @JsonProperty("sale_order_id")
    private Long saleOrderId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 产品
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 单位
     */
    @DEField(name = "product_uom_id")
    @TableField(value = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Long productUomId;
    /**
     * 费用报表
     */
    @DEField(name = "sheet_id")
    @TableField(value = "sheet_id")
    @JSONField(name = "sheet_id")
    @JsonProperty("sheet_id")
    private Long sheetId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 科目
     */
    @DEField(name = "account_id")
    @TableField(value = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Long accountId;
    /**
     * 公司货币报告
     */
    @DEField(name = "company_currency_id")
    @TableField(value = "company_currency_id")
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Long companyCurrencyId;
    /**
     * 分析账户
     */
    @DEField(name = "analytic_account_id")
    @TableField(value = "analytic_account_id")
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    private Long analyticAccountId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet odooSheet;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCompanyCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooSaleOrder;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom;



    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [总计]
     */
    public void setTotalAmount(BigDecimal totalAmount){
        this.totalAmount = totalAmount ;
        this.modify("total_amount",totalAmount);
    }

    /**
     * 设置 [单价]
     */
    public void setUnitAmount(Double unitAmount){
        this.unitAmount = unitAmount ;
        this.modify("unit_amount",unitAmount);
    }

    /**
     * 设置 [支付]
     */
    public void setPaymentMode(String paymentMode){
        this.paymentMode = paymentMode ;
        this.modify("payment_mode",paymentMode);
    }

    /**
     * 设置 [合计 (公司货币)]
     */
    public void setTotalAmountCompany(BigDecimal totalAmountCompany){
        this.totalAmountCompany = totalAmountCompany ;
        this.modify("total_amount_company",totalAmountCompany);
    }

    /**
     * 设置 [由经理或会计人员明确地拒绝]
     */
    public void setIsRefused(Boolean isRefused){
        this.isRefused = isRefused ;
        this.modify("is_refused",isRefused);
    }

    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    /**
     * 设置 [数量]
     */
    public void setQuantity(Double quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [小计]
     */
    public void setUntaxedAmount(Double untaxedAmount){
        this.untaxedAmount = untaxedAmount ;
        this.modify("untaxed_amount",untaxedAmount);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [备注...]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [账单参照]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [员工]
     */
    public void setEmployeeId(Long employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [销售订单]
     */
    public void setSaleOrderId(Long saleOrderId){
        this.saleOrderId = saleOrderId ;
        this.modify("sale_order_id",saleOrderId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [产品]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [单位]
     */
    public void setProductUomId(Long productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [费用报表]
     */
    public void setSheetId(Long sheetId){
        this.sheetId = sheetId ;
        this.modify("sheet_id",sheetId);
    }

    /**
     * 设置 [科目]
     */
    public void setAccountId(Long accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [公司货币报告]
     */
    public void setCompanyCurrencyId(Long companyCurrencyId){
        this.companyCurrencyId = companyCurrencyId ;
        this.modify("company_currency_id",companyCurrencyId);
    }

    /**
     * 设置 [分析账户]
     */
    public void setAnalyticAccountId(Long analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


