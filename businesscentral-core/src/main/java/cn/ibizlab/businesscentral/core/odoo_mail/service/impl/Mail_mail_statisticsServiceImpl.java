package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mail_statisticsService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_mail_statisticsMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[邮件统计] 服务对象接口实现
 */
@Slf4j
@Service("Mail_mail_statisticsServiceImpl")
public class Mail_mail_statisticsServiceImpl extends EBSServiceImpl<Mail_mail_statisticsMapper, Mail_mail_statistics> implements IMail_mail_statisticsService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mailService mailMailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_campaignService mailMassMailingCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailingService mailMassMailingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.mail.statistics" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_mail_statistics et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mail_statisticsService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_mail_statistics> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_mail_statistics et) {
        Mail_mail_statistics old = new Mail_mail_statistics() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mail_statisticsService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mail_statisticsService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_mail_statistics> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_mail_statistics get(Long key) {
        Mail_mail_statistics et = getById(key);
        if(et==null){
            et=new Mail_mail_statistics();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_mail_statistics getDraft(Mail_mail_statistics et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_mail_statistics et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_mail_statistics et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_mail_statistics et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_mail_statistics> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_mail_statistics> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_mail_statistics> selectByMailMailId(Long id) {
        return baseMapper.selectByMailMailId(id);
    }
    @Override
    public void resetByMailMailId(Long id) {
        this.update(new UpdateWrapper<Mail_mail_statistics>().set("mail_mail_id",null).eq("mail_mail_id",id));
    }

    @Override
    public void resetByMailMailId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_mail_statistics>().set("mail_mail_id",null).in("mail_mail_id",ids));
    }

    @Override
    public void removeByMailMailId(Long id) {
        this.remove(new QueryWrapper<Mail_mail_statistics>().eq("mail_mail_id",id));
    }

	@Override
    public List<Mail_mail_statistics> selectByMassMailingCampaignId(Long id) {
        return baseMapper.selectByMassMailingCampaignId(id);
    }
    @Override
    public void resetByMassMailingCampaignId(Long id) {
        this.update(new UpdateWrapper<Mail_mail_statistics>().set("mass_mailing_campaign_id",null).eq("mass_mailing_campaign_id",id));
    }

    @Override
    public void resetByMassMailingCampaignId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_mail_statistics>().set("mass_mailing_campaign_id",null).in("mass_mailing_campaign_id",ids));
    }

    @Override
    public void removeByMassMailingCampaignId(Long id) {
        this.remove(new QueryWrapper<Mail_mail_statistics>().eq("mass_mailing_campaign_id",id));
    }

	@Override
    public List<Mail_mail_statistics> selectByMassMailingId(Long id) {
        return baseMapper.selectByMassMailingId(id);
    }
    @Override
    public void resetByMassMailingId(Long id) {
        this.update(new UpdateWrapper<Mail_mail_statistics>().set("mass_mailing_id",null).eq("mass_mailing_id",id));
    }

    @Override
    public void resetByMassMailingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_mail_statistics>().set("mass_mailing_id",null).in("mass_mailing_id",ids));
    }

    @Override
    public void removeByMassMailingId(Long id) {
        this.remove(new QueryWrapper<Mail_mail_statistics>().eq("mass_mailing_id",id));
    }

	@Override
    public List<Mail_mail_statistics> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mail_mail_statistics>().eq("create_uid",id));
    }

	@Override
    public List<Mail_mail_statistics> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mail_mail_statistics>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_mail_statistics> searchDefault(Mail_mail_statisticsSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_mail_statistics> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_mail_statistics>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_mail_statistics et){
        //实体关系[DER1N_MAIL_MAIL_STATISTICS__MAIL_MASS_MAILING_CAMPAIGN__MASS_MAILING_CAMPAIGN_ID]
        if(!ObjectUtils.isEmpty(et.getMassMailingCampaignId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign odooMassMailingCampaign=et.getOdooMassMailingCampaign();
            if(ObjectUtils.isEmpty(odooMassMailingCampaign)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign majorEntity=mailMassMailingCampaignService.get(et.getMassMailingCampaignId());
                et.setOdooMassMailingCampaign(majorEntity);
                odooMassMailingCampaign=majorEntity;
            }
            et.setMassMailingCampaignIdText(odooMassMailingCampaign.getName());
        }
        //实体关系[DER1N_MAIL_MAIL_STATISTICS__MAIL_MASS_MAILING__MASS_MAILING_ID]
        if(!ObjectUtils.isEmpty(et.getMassMailingId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing odooMassMailing=et.getOdooMassMailing();
            if(ObjectUtils.isEmpty(odooMassMailing)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing majorEntity=mailMassMailingService.get(et.getMassMailingId());
                et.setOdooMassMailing(majorEntity);
                odooMassMailing=majorEntity;
            }
            et.setMassMailingIdText(odooMassMailing.getName());
        }
        //实体关系[DER1N_MAIL_MAIL_STATISTICS__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAIL_MAIL_STATISTICS__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mail_mail_statistics> getMailMailStatisticsByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mail_mail_statistics> getMailMailStatisticsByEntities(List<Mail_mail_statistics> entities) {
        List ids =new ArrayList();
        for(Mail_mail_statistics entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



