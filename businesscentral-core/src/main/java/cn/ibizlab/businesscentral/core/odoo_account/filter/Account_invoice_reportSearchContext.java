package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_report;
/**
 * 关系型数据实体[Account_invoice_report] 查询条件对象
 */
@Slf4j
@Data
public class Account_invoice_reportSearchContext extends QueryWrapperContext<Account_invoice_report> {

	private String n_state_eq;//[发票状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_type_eq;//[类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_team_id_text_eq;//[销售团队]
	public void setN_team_id_text_eq(String n_team_id_text_eq) {
        this.n_team_id_text_eq = n_team_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_eq)){
            this.getSearchCond().eq("team_id_text", n_team_id_text_eq);
        }
    }
	private String n_team_id_text_like;//[销售团队]
	public void setN_team_id_text_like(String n_team_id_text_like) {
        this.n_team_id_text_like = n_team_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_like)){
            this.getSearchCond().like("team_id_text", n_team_id_text_like);
        }
    }
	private String n_account_analytic_id_text_eq;//[分析账户]
	public void setN_account_analytic_id_text_eq(String n_account_analytic_id_text_eq) {
        this.n_account_analytic_id_text_eq = n_account_analytic_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_text_eq)){
            this.getSearchCond().eq("account_analytic_id_text", n_account_analytic_id_text_eq);
        }
    }
	private String n_account_analytic_id_text_like;//[分析账户]
	public void setN_account_analytic_id_text_like(String n_account_analytic_id_text_like) {
        this.n_account_analytic_id_text_like = n_account_analytic_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_text_like)){
            this.getSearchCond().like("account_analytic_id_text", n_account_analytic_id_text_like);
        }
    }
	private String n_categ_id_text_eq;//[产品类别]
	public void setN_categ_id_text_eq(String n_categ_id_text_eq) {
        this.n_categ_id_text_eq = n_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_categ_id_text_eq)){
            this.getSearchCond().eq("categ_id_text", n_categ_id_text_eq);
        }
    }
	private String n_categ_id_text_like;//[产品类别]
	public void setN_categ_id_text_like(String n_categ_id_text_like) {
        this.n_categ_id_text_like = n_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_categ_id_text_like)){
            this.getSearchCond().like("categ_id_text", n_categ_id_text_like);
        }
    }
	private String n_journal_id_text_eq;//[日记账]
	public void setN_journal_id_text_eq(String n_journal_id_text_eq) {
        this.n_journal_id_text_eq = n_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_eq)){
            this.getSearchCond().eq("journal_id_text", n_journal_id_text_eq);
        }
    }
	private String n_journal_id_text_like;//[日记账]
	public void setN_journal_id_text_like(String n_journal_id_text_like) {
        this.n_journal_id_text_like = n_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_like)){
            this.getSearchCond().like("journal_id_text", n_journal_id_text_like);
        }
    }
	private String n_fiscal_position_id_text_eq;//[税科目调整]
	public void setN_fiscal_position_id_text_eq(String n_fiscal_position_id_text_eq) {
        this.n_fiscal_position_id_text_eq = n_fiscal_position_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_eq)){
            this.getSearchCond().eq("fiscal_position_id_text", n_fiscal_position_id_text_eq);
        }
    }
	private String n_fiscal_position_id_text_like;//[税科目调整]
	public void setN_fiscal_position_id_text_like(String n_fiscal_position_id_text_like) {
        this.n_fiscal_position_id_text_like = n_fiscal_position_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_like)){
            this.getSearchCond().like("fiscal_position_id_text", n_fiscal_position_id_text_like);
        }
    }
	private String n_invoice_id_text_eq;//[发票]
	public void setN_invoice_id_text_eq(String n_invoice_id_text_eq) {
        this.n_invoice_id_text_eq = n_invoice_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_text_eq)){
            this.getSearchCond().eq("invoice_id_text", n_invoice_id_text_eq);
        }
    }
	private String n_invoice_id_text_like;//[发票]
	public void setN_invoice_id_text_like(String n_invoice_id_text_like) {
        this.n_invoice_id_text_like = n_invoice_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_text_like)){
            this.getSearchCond().like("invoice_id_text", n_invoice_id_text_like);
        }
    }
	private String n_country_id_text_eq;//[业务伙伴的国家]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[业务伙伴的国家]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_payment_term_id_text_eq;//[付款条款]
	public void setN_payment_term_id_text_eq(String n_payment_term_id_text_eq) {
        this.n_payment_term_id_text_eq = n_payment_term_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_text_eq)){
            this.getSearchCond().eq("payment_term_id_text", n_payment_term_id_text_eq);
        }
    }
	private String n_payment_term_id_text_like;//[付款条款]
	public void setN_payment_term_id_text_like(String n_payment_term_id_text_like) {
        this.n_payment_term_id_text_like = n_payment_term_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_text_like)){
            this.getSearchCond().like("payment_term_id_text", n_payment_term_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[业务伙伴]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[业务伙伴]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_account_line_id_text_eq;//[收入/费用科目]
	public void setN_account_line_id_text_eq(String n_account_line_id_text_eq) {
        this.n_account_line_id_text_eq = n_account_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_line_id_text_eq)){
            this.getSearchCond().eq("account_line_id_text", n_account_line_id_text_eq);
        }
    }
	private String n_account_line_id_text_like;//[收入/费用科目]
	public void setN_account_line_id_text_like(String n_account_line_id_text_like) {
        this.n_account_line_id_text_like = n_account_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_line_id_text_like)){
            this.getSearchCond().like("account_line_id_text", n_account_line_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_commercial_partner_id_text_eq;//[业务伙伴公司]
	public void setN_commercial_partner_id_text_eq(String n_commercial_partner_id_text_eq) {
        this.n_commercial_partner_id_text_eq = n_commercial_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_eq)){
            this.getSearchCond().eq("commercial_partner_id_text", n_commercial_partner_id_text_eq);
        }
    }
	private String n_commercial_partner_id_text_like;//[业务伙伴公司]
	public void setN_commercial_partner_id_text_like(String n_commercial_partner_id_text_like) {
        this.n_commercial_partner_id_text_like = n_commercial_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_like)){
            this.getSearchCond().like("commercial_partner_id_text", n_commercial_partner_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[销售员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[销售员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_account_id_text_eq;//[收／付款账户]
	public void setN_account_id_text_eq(String n_account_id_text_eq) {
        this.n_account_id_text_eq = n_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_eq)){
            this.getSearchCond().eq("account_id_text", n_account_id_text_eq);
        }
    }
	private String n_account_id_text_like;//[收／付款账户]
	public void setN_account_id_text_like(String n_account_id_text_like) {
        this.n_account_id_text_like = n_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_like)){
            this.getSearchCond().like("account_id_text", n_account_id_text_like);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_user_id_eq;//[销售员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_country_id_eq;//[业务伙伴的国家]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }
	private Long n_partner_bank_id_eq;//[银行账户]
	public void setN_partner_bank_id_eq(Long n_partner_bank_id_eq) {
        this.n_partner_bank_id_eq = n_partner_bank_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_bank_id_eq)){
            this.getSearchCond().eq("partner_bank_id", n_partner_bank_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_categ_id_eq;//[产品类别]
	public void setN_categ_id_eq(Long n_categ_id_eq) {
        this.n_categ_id_eq = n_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_categ_id_eq)){
            this.getSearchCond().eq("categ_id", n_categ_id_eq);
        }
    }
	private Long n_commercial_partner_id_eq;//[业务伙伴公司]
	public void setN_commercial_partner_id_eq(Long n_commercial_partner_id_eq) {
        this.n_commercial_partner_id_eq = n_commercial_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_eq)){
            this.getSearchCond().eq("commercial_partner_id", n_commercial_partner_id_eq);
        }
    }
	private Long n_account_id_eq;//[收／付款账户]
	public void setN_account_id_eq(Long n_account_id_eq) {
        this.n_account_id_eq = n_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_eq)){
            this.getSearchCond().eq("account_id", n_account_id_eq);
        }
    }
	private Long n_partner_id_eq;//[业务伙伴]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_payment_term_id_eq;//[付款条款]
	public void setN_payment_term_id_eq(Long n_payment_term_id_eq) {
        this.n_payment_term_id_eq = n_payment_term_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_eq)){
            this.getSearchCond().eq("payment_term_id", n_payment_term_id_eq);
        }
    }
	private Long n_account_analytic_id_eq;//[分析账户]
	public void setN_account_analytic_id_eq(Long n_account_analytic_id_eq) {
        this.n_account_analytic_id_eq = n_account_analytic_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_eq)){
            this.getSearchCond().eq("account_analytic_id", n_account_analytic_id_eq);
        }
    }
	private Long n_fiscal_position_id_eq;//[税科目调整]
	public void setN_fiscal_position_id_eq(Long n_fiscal_position_id_eq) {
        this.n_fiscal_position_id_eq = n_fiscal_position_id_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_eq)){
            this.getSearchCond().eq("fiscal_position_id", n_fiscal_position_id_eq);
        }
    }
	private Long n_journal_id_eq;//[日记账]
	public void setN_journal_id_eq(Long n_journal_id_eq) {
        this.n_journal_id_eq = n_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_eq)){
            this.getSearchCond().eq("journal_id", n_journal_id_eq);
        }
    }
	private Long n_account_line_id_eq;//[收入/费用科目]
	public void setN_account_line_id_eq(Long n_account_line_id_eq) {
        this.n_account_line_id_eq = n_account_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_line_id_eq)){
            this.getSearchCond().eq("account_line_id", n_account_line_id_eq);
        }
    }
	private Long n_team_id_eq;//[销售团队]
	public void setN_team_id_eq(Long n_team_id_eq) {
        this.n_team_id_eq = n_team_id_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_eq)){
            this.getSearchCond().eq("team_id", n_team_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_invoice_id_eq;//[发票]
	public void setN_invoice_id_eq(Long n_invoice_id_eq) {
        this.n_invoice_id_eq = n_invoice_id_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_eq)){
            this.getSearchCond().eq("invoice_id", n_invoice_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



