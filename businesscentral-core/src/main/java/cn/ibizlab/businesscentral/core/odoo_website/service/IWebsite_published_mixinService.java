package cn.ibizlab.businesscentral.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_published_mixin;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_published_mixinSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Website_published_mixin] 服务对象接口
 */
public interface IWebsite_published_mixinService extends IService<Website_published_mixin>{

    boolean create(Website_published_mixin et) ;
    void createBatch(List<Website_published_mixin> list) ;
    boolean update(Website_published_mixin et) ;
    void updateBatch(List<Website_published_mixin> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Website_published_mixin get(Long key) ;
    Website_published_mixin getDraft(Website_published_mixin et) ;
    boolean checkKey(Website_published_mixin et) ;
    boolean save(Website_published_mixin et) ;
    void saveBatch(List<Website_published_mixin> list) ;
    Page<Website_published_mixin> searchDefault(Website_published_mixinSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


