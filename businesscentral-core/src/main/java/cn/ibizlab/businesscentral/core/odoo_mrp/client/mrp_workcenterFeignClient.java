package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenterSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_workcenter] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-workcenter", fallback = mrp_workcenterFallback.class)
public interface mrp_workcenterFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/search")
    Page<Mrp_workcenter> search(@RequestBody Mrp_workcenterSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters")
    Mrp_workcenter create(@RequestBody Mrp_workcenter mrp_workcenter);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/batch")
    Boolean createBatch(@RequestBody List<Mrp_workcenter> mrp_workcenters);



    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/{id}")
    Mrp_workcenter get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenters/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenters/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenters/{id}")
    Mrp_workcenter update(@PathVariable("id") Long id,@RequestBody Mrp_workcenter mrp_workcenter);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenters/batch")
    Boolean updateBatch(@RequestBody List<Mrp_workcenter> mrp_workcenters);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/select")
    Page<Mrp_workcenter> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/getdraft")
    Mrp_workcenter getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/checkkey")
    Boolean checkKey(@RequestBody Mrp_workcenter mrp_workcenter);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/save")
    Boolean save(@RequestBody Mrp_workcenter mrp_workcenter);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_workcenter> mrp_workcenters);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/searchdefault")
    Page<Mrp_workcenter> searchDefault(@RequestBody Mrp_workcenterSearchContext context);


}
