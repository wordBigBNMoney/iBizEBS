package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_track_confirmationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_track_confirmation] 服务对象接口
 */
@Component
public class stock_track_confirmationFallback implements stock_track_confirmationFeignClient{




    public Stock_track_confirmation create(Stock_track_confirmation stock_track_confirmation){
            return null;
     }
    public Boolean createBatch(List<Stock_track_confirmation> stock_track_confirmations){
            return false;
     }

    public Stock_track_confirmation update(Long id, Stock_track_confirmation stock_track_confirmation){
            return null;
     }
    public Boolean updateBatch(List<Stock_track_confirmation> stock_track_confirmations){
            return false;
     }


    public Stock_track_confirmation get(Long id){
            return null;
     }


    public Page<Stock_track_confirmation> search(Stock_track_confirmationSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Stock_track_confirmation> select(){
            return null;
     }

    public Stock_track_confirmation getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_track_confirmation stock_track_confirmation){
            return false;
     }


    public Boolean save(Stock_track_confirmation stock_track_confirmation){
            return false;
     }
    public Boolean saveBatch(List<Stock_track_confirmation> stock_track_confirmations){
            return false;
     }

    public Page<Stock_track_confirmation> searchDefault(Stock_track_confirmationSearchContext context){
            return null;
     }


}
