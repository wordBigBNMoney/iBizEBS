package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_lineSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_invoice_lineMapper extends BaseMapper<Account_invoice_line>{

    Page<Account_invoice_line> searchDefault(IPage page, @Param("srf") Account_invoice_lineSearchContext context, @Param("ew") Wrapper<Account_invoice_line> wrapper) ;
    @Override
    Account_invoice_line selectById(Serializable id);
    @Override
    int insert(Account_invoice_line entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_invoice_line entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_invoice_line entity, @Param("ew") Wrapper<Account_invoice_line> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_invoice_line> selectByAccountId(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByAccountAnalyticId(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByInvoiceId(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByProductId(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByPurchaseLineId(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByCompanyId(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByPartnerId(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByWriteUid(@Param("id") Serializable id) ;

    List<Account_invoice_line> selectByUomId(@Param("id") Serializable id) ;


}
