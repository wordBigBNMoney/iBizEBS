package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_exportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_language_export] 服务对象接口
 */
public interface IBase_language_exportService extends IService<Base_language_export>{

    boolean create(Base_language_export et) ;
    void createBatch(List<Base_language_export> list) ;
    boolean update(Base_language_export et) ;
    void updateBatch(List<Base_language_export> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_language_export get(Long key) ;
    Base_language_export getDraft(Base_language_export et) ;
    boolean checkKey(Base_language_export et) ;
    boolean save(Base_language_export et) ;
    void saveBatch(List<Base_language_export> list) ;
    Page<Base_language_export> searchDefault(Base_language_exportSearchContext context) ;
    List<Base_language_export> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_language_export> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_language_export> getBaseLanguageExportByIds(List<Long> ids) ;
    List<Base_language_export> getBaseLanguageExportByEntities(List<Base_language_export> entities) ;
}


