package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[邮件统计]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_MAIL_STATISTICS",resultMap = "Mail_mail_statisticsResultMap")
public class Mail_mail_statistics extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 安排
     */
    @TableField(value = "scheduled")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled")
    private Timestamp scheduled;
    /**
     * 点击率
     */
    @TableField(value = "clicked")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "clicked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("clicked")
    private Timestamp clicked;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 收件人email地址
     */
    @TableField(value = "email")
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 状态更新
     */
    @DEField(name = "state_update")
    @TableField(value = "state_update")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "state_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("state_update")
    private Timestamp stateUpdate;
    /**
     * 已回复
     */
    @TableField(value = "replied")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "replied" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("replied")
    private Timestamp replied;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 文档模型
     */
    @TableField(value = "model")
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;
    /**
     * 消息ID
     */
    @DEField(name = "message_id")
    @TableField(value = "message_id")
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;
    /**
     * 异常
     */
    @TableField(value = "exception")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "exception" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("exception")
    private Timestamp exception;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 被退回
     */
    @TableField(value = "bounced")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "bounced" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bounced")
    private Timestamp bounced;
    /**
     * 忽略
     */
    @TableField(value = "ignored")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "ignored" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("ignored")
    private Timestamp ignored;
    /**
     * 文档ID
     */
    @DEField(name = "res_id")
    @TableField(value = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * 已汇
     */
    @TableField(value = "sent")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sent" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sent")
    private Timestamp sent;
    /**
     * 邮件ID（技术）
     */
    @DEField(name = "mail_mail_id_int")
    @TableField(value = "mail_mail_id_int")
    @JSONField(name = "mail_mail_id_int")
    @JsonProperty("mail_mail_id_int")
    private Integer mailMailIdInt;
    /**
     * 点击链接
     */
    @TableField(exist = false)
    @JSONField(name = "links_click_ids")
    @JsonProperty("links_click_ids")
    private String linksClickIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 已开启
     */
    @TableField(value = "opened")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "opened" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("opened")
    private Timestamp opened;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 群发邮件营销
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_campaign_id_text")
    @JsonProperty("mass_mailing_campaign_id_text")
    private String massMailingCampaignIdText;
    /**
     * 群发邮件
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_id_text")
    @JsonProperty("mass_mailing_id_text")
    private String massMailingIdText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 群发邮件营销
     */
    @DEField(name = "mass_mailing_campaign_id")
    @TableField(value = "mass_mailing_campaign_id")
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    private Long massMailingCampaignId;
    /**
     * 邮件
     */
    @DEField(name = "mail_mail_id")
    @TableField(value = "mail_mail_id")
    @JSONField(name = "mail_mail_id")
    @JsonProperty("mail_mail_id")
    private Long mailMailId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 群发邮件
     */
    @DEField(name = "mass_mailing_id")
    @TableField(value = "mass_mailing_id")
    @JSONField(name = "mass_mailing_id")
    @JsonProperty("mass_mailing_id")
    private Long massMailingId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail odooMailMail;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign odooMassMailingCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing odooMassMailing;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [安排]
     */
    public void setScheduled(Timestamp scheduled){
        this.scheduled = scheduled ;
        this.modify("scheduled",scheduled);
    }

    /**
     * 格式化日期 [安排]
     */
    public String formatScheduled(){
        if (this.scheduled == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduled);
    }
    /**
     * 设置 [点击率]
     */
    public void setClicked(Timestamp clicked){
        this.clicked = clicked ;
        this.modify("clicked",clicked);
    }

    /**
     * 格式化日期 [点击率]
     */
    public String formatClicked(){
        if (this.clicked == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(clicked);
    }
    /**
     * 设置 [收件人email地址]
     */
    public void setEmail(String email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [状态更新]
     */
    public void setStateUpdate(Timestamp stateUpdate){
        this.stateUpdate = stateUpdate ;
        this.modify("state_update",stateUpdate);
    }

    /**
     * 格式化日期 [状态更新]
     */
    public String formatStateUpdate(){
        if (this.stateUpdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(stateUpdate);
    }
    /**
     * 设置 [已回复]
     */
    public void setReplied(Timestamp replied){
        this.replied = replied ;
        this.modify("replied",replied);
    }

    /**
     * 格式化日期 [已回复]
     */
    public String formatReplied(){
        if (this.replied == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(replied);
    }
    /**
     * 设置 [文档模型]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [消息ID]
     */
    public void setMessageId(String messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }

    /**
     * 设置 [异常]
     */
    public void setException(Timestamp exception){
        this.exception = exception ;
        this.modify("exception",exception);
    }

    /**
     * 格式化日期 [异常]
     */
    public String formatException(){
        if (this.exception == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(exception);
    }
    /**
     * 设置 [被退回]
     */
    public void setBounced(Timestamp bounced){
        this.bounced = bounced ;
        this.modify("bounced",bounced);
    }

    /**
     * 格式化日期 [被退回]
     */
    public String formatBounced(){
        if (this.bounced == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(bounced);
    }
    /**
     * 设置 [忽略]
     */
    public void setIgnored(Timestamp ignored){
        this.ignored = ignored ;
        this.modify("ignored",ignored);
    }

    /**
     * 格式化日期 [忽略]
     */
    public String formatIgnored(){
        if (this.ignored == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(ignored);
    }
    /**
     * 设置 [文档ID]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [已汇]
     */
    public void setSent(Timestamp sent){
        this.sent = sent ;
        this.modify("sent",sent);
    }

    /**
     * 格式化日期 [已汇]
     */
    public String formatSent(){
        if (this.sent == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sent);
    }
    /**
     * 设置 [邮件ID（技术）]
     */
    public void setMailMailIdInt(Integer mailMailIdInt){
        this.mailMailIdInt = mailMailIdInt ;
        this.modify("mail_mail_id_int",mailMailIdInt);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [已开启]
     */
    public void setOpened(Timestamp opened){
        this.opened = opened ;
        this.modify("opened",opened);
    }

    /**
     * 格式化日期 [已开启]
     */
    public String formatOpened(){
        if (this.opened == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(opened);
    }
    /**
     * 设置 [群发邮件营销]
     */
    public void setMassMailingCampaignId(Long massMailingCampaignId){
        this.massMailingCampaignId = massMailingCampaignId ;
        this.modify("mass_mailing_campaign_id",massMailingCampaignId);
    }

    /**
     * 设置 [邮件]
     */
    public void setMailMailId(Long mailMailId){
        this.mailMailId = mailMailId ;
        this.modify("mail_mail_id",mailMailId);
    }

    /**
     * 设置 [群发邮件]
     */
    public void setMassMailingId(Long massMailingId){
        this.massMailingId = massMailingId ;
        this.modify("mass_mailing_id",massMailingId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


