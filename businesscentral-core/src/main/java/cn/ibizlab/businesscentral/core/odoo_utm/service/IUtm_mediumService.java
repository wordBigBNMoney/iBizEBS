package cn.ibizlab.businesscentral.core.odoo_utm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_mediumSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Utm_medium] 服务对象接口
 */
public interface IUtm_mediumService extends IService<Utm_medium>{

    boolean create(Utm_medium et) ;
    void createBatch(List<Utm_medium> list) ;
    boolean update(Utm_medium et) ;
    void updateBatch(List<Utm_medium> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Utm_medium get(Long key) ;
    Utm_medium getDraft(Utm_medium et) ;
    boolean checkKey(Utm_medium et) ;
    boolean save(Utm_medium et) ;
    void saveBatch(List<Utm_medium> list) ;
    Page<Utm_medium> searchDefault(Utm_mediumSearchContext context) ;
    List<Utm_medium> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Utm_medium> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Utm_medium> getUtmMediumByIds(List<Long> ids) ;
    List<Utm_medium> getUtmMediumByEntities(List<Utm_medium> entities) ;
}


