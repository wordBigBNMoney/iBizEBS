package cn.ibizlab.businesscentral.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_mediumSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[utm_medium] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-utm:odoo-utm}", contextId = "utm-medium", fallback = utm_mediumFallback.class)
public interface utm_mediumFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/utm_media")
    Utm_medium create(@RequestBody Utm_medium utm_medium);

    @RequestMapping(method = RequestMethod.POST, value = "/utm_media/batch")
    Boolean createBatch(@RequestBody List<Utm_medium> utm_media);


    @RequestMapping(method = RequestMethod.DELETE, value = "/utm_media/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/utm_media/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/utm_media/search")
    Page<Utm_medium> search(@RequestBody Utm_mediumSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/utm_media/{id}")
    Utm_medium update(@PathVariable("id") Long id,@RequestBody Utm_medium utm_medium);

    @RequestMapping(method = RequestMethod.PUT, value = "/utm_media/batch")
    Boolean updateBatch(@RequestBody List<Utm_medium> utm_media);


    @RequestMapping(method = RequestMethod.GET, value = "/utm_media/{id}")
    Utm_medium get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/utm_media/select")
    Page<Utm_medium> select();


    @RequestMapping(method = RequestMethod.GET, value = "/utm_media/getdraft")
    Utm_medium getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/utm_media/checkkey")
    Boolean checkKey(@RequestBody Utm_medium utm_medium);


    @RequestMapping(method = RequestMethod.POST, value = "/utm_media/save")
    Boolean save(@RequestBody Utm_medium utm_medium);

    @RequestMapping(method = RequestMethod.POST, value = "/utm_media/savebatch")
    Boolean saveBatch(@RequestBody List<Utm_medium> utm_media);



    @RequestMapping(method = RequestMethod.POST, value = "/utm_media/searchdefault")
    Page<Utm_medium> searchDefault(@RequestBody Utm_mediumSearchContext context);


}
