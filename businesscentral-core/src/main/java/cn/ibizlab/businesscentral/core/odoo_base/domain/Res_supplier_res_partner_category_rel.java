package cn.ibizlab.businesscentral.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[供应商标签]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "res_partner_res_partner_category_rel",resultMap = "Res_supplier_res_partner_category_relResultMap")
public class Res_supplier_res_partner_category_rel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableField(exist = false)
    @JSONField(name = "id")
    @JsonProperty("id")
    private String id;
    /**
     * 标签名
     */
    @TableField(exist = false)
    @JSONField(name = "category_name")
    @JsonProperty("category_name")
    private String categoryName;
    /**
     * 名称
     */
    @TableField(exist = false)
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;
    /**
     * ID
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * ID
     */
    @DEField(name = "category_id")
    @TableField(value = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Long categoryId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_category odooCategory;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier odooSupplier;



    /**
     * 设置 [ID]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [ID]
     */
    public void setCategoryId(Long categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


