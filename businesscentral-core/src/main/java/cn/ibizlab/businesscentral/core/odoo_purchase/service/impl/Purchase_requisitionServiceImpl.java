package cn.ibizlab.businesscentral.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisitionSearchContext;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_purchase.mapper.Purchase_requisitionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[采购申请] 服务对象接口实现
 */
@Slf4j
@Service("Purchase_requisitionServiceImpl")
public class Purchase_requisitionServiceImpl extends EBSServiceImpl<Purchase_requisitionMapper, Purchase_requisition> implements IPurchase_requisitionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService purchaseRequisitionLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_typeService purchaseRequisitionTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService resSupplierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_requisitionUpdate__MSDenyLogic update__msdenyLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_requisitionRemove__MSDenyLogic remove__msdenyLogic;

    protected int batchSize = 500;

    public String getIrModel(){
        return "purchase.requisition" ;
    }

    private boolean messageinfo = true ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Purchase_requisition et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_requisitionService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Purchase_requisition> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Purchase_requisition et) {
        Purchase_requisition old = new Purchase_requisition() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_requisitionService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_requisitionService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Purchase_requisition> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        Purchase_requisition et=new Purchase_requisition();
        et.set("id",key);
        purchaseRequisitionLineService.removeByRequisitionId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        purchaseRequisitionLineService.removeByRequisitionId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Purchase_requisition get(Long key) {
        Purchase_requisition et = getById(key);
        if(et==null){
            et=new Purchase_requisition();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Purchase_requisition getDraft(Purchase_requisition et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Purchase_requisition action_cancel(Purchase_requisition et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_requisition action_done(Purchase_requisition et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_requisition action_draft(Purchase_requisition et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_requisition action_in_progress(Purchase_requisition et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_requisition action_open(Purchase_requisition et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Purchase_requisition et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public Purchase_requisition masterTabCount(Purchase_requisition et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(Purchase_requisition et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Purchase_requisition et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Purchase_requisition> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Purchase_requisition> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Purchase_requisition> selectByTypeId(Long id) {
        return baseMapper.selectByTypeId(id);
    }
    @Override
    public void removeByTypeId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("type_id",id));
    }

	@Override
    public List<Purchase_requisition> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("company_id",id));
    }

	@Override
    public List<Purchase_requisition> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("currency_id",id));
    }

	@Override
    public List<Purchase_requisition> selectByVendorId(Long id) {
        return baseMapper.selectByVendorId(id);
    }
    @Override
    public void removeByVendorId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("vendor_id",id));
    }

	@Override
    public List<Purchase_requisition> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("create_uid",id));
    }

	@Override
    public List<Purchase_requisition> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("user_id",id));
    }

	@Override
    public List<Purchase_requisition> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("write_uid",id));
    }

	@Override
    public List<Purchase_requisition> selectByPickingTypeId(Long id) {
        return baseMapper.selectByPickingTypeId(id);
    }
    @Override
    public void removeByPickingTypeId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("picking_type_id",id));
    }

	@Override
    public List<Purchase_requisition> selectByWarehouseId(Long id) {
        return baseMapper.selectByWarehouseId(id);
    }
    @Override
    public void removeByWarehouseId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition>().eq("warehouse_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Purchase_requisition> searchDefault(Purchase_requisitionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_requisition> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_requisition>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 首选表格
     */
    @Override
    public Page<Purchase_requisition> searchMaster(Purchase_requisitionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_requisition> pages=baseMapper.searchMaster(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_requisition>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Purchase_requisition et){
        //实体关系[DER1N_PURCHASE_REQUISITION_PURCHASE_REQUISITION_TYPE_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getTypeId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_type odooType=et.getOdooType();
            if(ObjectUtils.isEmpty(odooType)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_type majorEntity=purchaseRequisitionTypeService.get(et.getTypeId());
                et.setOdooType(majorEntity);
                odooType=majorEntity;
            }
            et.setQuantityCopy(odooType.getQuantityCopy());
            et.setTypeIdText(odooType.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_RES_COMPANY_COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyName(odooCompany.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_RES_CURRENCY_CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyName(odooCurrency.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_RES_SUPPLIER_VENDOR_ID]
        if(!ObjectUtils.isEmpty(et.getVendorId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier odooVendor=et.getOdooVendor();
            if(ObjectUtils.isEmpty(odooVendor)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier majorEntity=resSupplierService.get(et.getVendorId());
                et.setOdooVendor(majorEntity);
                odooVendor=majorEntity;
            }
            et.setVendorIdText(odooVendor.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_RES_USERS_CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUname(odooCreate.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_RES_USERS_USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserName(odooUser.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_RES_USERS_WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUname(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Purchase_requisition> getPurchaseRequisitionByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Purchase_requisition> getPurchaseRequisitionByEntities(List<Purchase_requisition> entities) {
        List ids =new ArrayList();
        for(Purchase_requisition entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



