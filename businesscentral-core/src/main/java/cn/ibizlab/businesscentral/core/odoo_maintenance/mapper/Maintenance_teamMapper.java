package cn.ibizlab.businesscentral.core.odoo_maintenance.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_teamSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Maintenance_teamMapper extends BaseMapper<Maintenance_team>{

    Page<Maintenance_team> searchDefault(IPage page, @Param("srf") Maintenance_teamSearchContext context, @Param("ew") Wrapper<Maintenance_team> wrapper) ;
    @Override
    Maintenance_team selectById(Serializable id);
    @Override
    int insert(Maintenance_team entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Maintenance_team entity);
    @Override
    int update(@Param(Constants.ENTITY) Maintenance_team entity, @Param("ew") Wrapper<Maintenance_team> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Maintenance_team> selectByCompanyId(@Param("id") Serializable id) ;

    List<Maintenance_team> selectByCreateUid(@Param("id") Serializable id) ;

    List<Maintenance_team> selectByWriteUid(@Param("id") Serializable id) ;


}
