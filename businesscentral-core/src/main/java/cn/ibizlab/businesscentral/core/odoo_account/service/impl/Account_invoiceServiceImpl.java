package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoiceSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_invoiceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[发票] 服务对象接口实现
 */
@Slf4j
@Service("Account_invoiceServiceImpl")
public class Account_invoiceServiceImpl extends EBSServiceImpl<Account_invoiceMapper, Account_invoice> implements IAccount_invoiceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService accountInvoiceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_reportService accountInvoiceReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_taxService accountInvoiceTaxService;

    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_bill_unionService purchaseBillUnionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_cash_roundingService accountCashRoundingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_incotermsService accountIncotermsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService accountPaymentTermService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_bankService resPartnerBankService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_campaignService utmCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mediumService utmMediumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService utmSourceService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.invoice" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_invoice et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoiceService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_invoice> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_invoice et) {
        Account_invoice old = new Account_invoice() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoiceService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoiceService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_invoice> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountInvoiceLineService.removeByInvoiceId(key);
        accountInvoiceTaxService.removeByInvoiceId(key);
        accountInvoiceService.resetByRefundInvoiceId(key);
        accountInvoiceService.resetByVendorBillId(key);
        accountMoveLineService.resetByInvoiceId(key);
        purchaseBillUnionService.resetByVendorBillId(key);
        repairOrderService.resetByInvoiceId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountInvoiceLineService.removeByInvoiceId(idList);
        accountInvoiceTaxService.removeByInvoiceId(idList);
        accountInvoiceService.resetByRefundInvoiceId(idList);
        accountInvoiceService.resetByVendorBillId(idList);
        accountMoveLineService.resetByInvoiceId(idList);
        purchaseBillUnionService.resetByVendorBillId(idList);
        repairOrderService.resetByInvoiceId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_invoice get(Long key) {
        Account_invoice et = getById(key);
        if(et==null){
            et=new Account_invoice();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_invoice getDraft(Account_invoice et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_invoice et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_invoice et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_invoice et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_invoice> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_invoice> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_invoice> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public void resetByAccountId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("account_id",null).eq("account_id",id));
    }

    @Override
    public void resetByAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("account_id",null).in("account_id",ids));
    }

    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("account_id",id));
    }

	@Override
    public List<Account_invoice> selectByCashRoundingId(Long id) {
        return baseMapper.selectByCashRoundingId(id);
    }
    @Override
    public void resetByCashRoundingId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("cash_rounding_id",null).eq("cash_rounding_id",id));
    }

    @Override
    public void resetByCashRoundingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("cash_rounding_id",null).in("cash_rounding_id",ids));
    }

    @Override
    public void removeByCashRoundingId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("cash_rounding_id",id));
    }

	@Override
    public List<Account_invoice> selectByFiscalPositionId(Long id) {
        return baseMapper.selectByFiscalPositionId(id);
    }
    @Override
    public void resetByFiscalPositionId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("fiscal_position_id",null).eq("fiscal_position_id",id));
    }

    @Override
    public void resetByFiscalPositionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("fiscal_position_id",null).in("fiscal_position_id",ids));
    }

    @Override
    public void removeByFiscalPositionId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("fiscal_position_id",id));
    }

	@Override
    public List<Account_invoice> selectByIncotermsId(Long id) {
        return baseMapper.selectByIncotermsId(id);
    }
    @Override
    public void resetByIncotermsId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("incoterms_id",null).eq("incoterms_id",id));
    }

    @Override
    public void resetByIncotermsId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("incoterms_id",null).in("incoterms_id",ids));
    }

    @Override
    public void removeByIncotermsId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("incoterms_id",id));
    }

	@Override
    public List<Account_invoice> selectByIncotermId(Long id) {
        return baseMapper.selectByIncotermId(id);
    }
    @Override
    public void resetByIncotermId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("incoterm_id",null).eq("incoterm_id",id));
    }

    @Override
    public void resetByIncotermId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("incoterm_id",null).in("incoterm_id",ids));
    }

    @Override
    public void removeByIncotermId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("incoterm_id",id));
    }

	@Override
    public List<Account_invoice> selectByRefundInvoiceId(Long id) {
        return baseMapper.selectByRefundInvoiceId(id);
    }
    @Override
    public void resetByRefundInvoiceId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("refund_invoice_id",null).eq("refund_invoice_id",id));
    }

    @Override
    public void resetByRefundInvoiceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("refund_invoice_id",null).in("refund_invoice_id",ids));
    }

    @Override
    public void removeByRefundInvoiceId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("refund_invoice_id",id));
    }

	@Override
    public List<Account_invoice> selectByVendorBillId(Long id) {
        return baseMapper.selectByVendorBillId(id);
    }
    @Override
    public void resetByVendorBillId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("vendor_bill_id",null).eq("vendor_bill_id",id));
    }

    @Override
    public void resetByVendorBillId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("vendor_bill_id",null).in("vendor_bill_id",ids));
    }

    @Override
    public void removeByVendorBillId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("vendor_bill_id",id));
    }

	@Override
    public List<Account_invoice> selectByJournalId(Long id) {
        return baseMapper.selectByJournalId(id);
    }
    @Override
    public void resetByJournalId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("journal_id",null).eq("journal_id",id));
    }

    @Override
    public void resetByJournalId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("journal_id",null).in("journal_id",ids));
    }

    @Override
    public void removeByJournalId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("journal_id",id));
    }

	@Override
    public List<Account_invoice> selectByMoveId(Long id) {
        return baseMapper.selectByMoveId(id);
    }
    @Override
    public List<Account_invoice> selectByMoveId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_invoice>().in("id",ids));
    }

    @Override
    public void removeByMoveId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("move_id",id));
    }

	@Override
    public List<Account_invoice> selectByPaymentTermId(Long id) {
        return baseMapper.selectByPaymentTermId(id);
    }
    @Override
    public void resetByPaymentTermId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("payment_term_id",null).eq("payment_term_id",id));
    }

    @Override
    public void resetByPaymentTermId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("payment_term_id",null).in("payment_term_id",ids));
    }

    @Override
    public void removeByPaymentTermId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("payment_term_id",id));
    }

	@Override
    public List<Account_invoice> selectByTeamId(Long id) {
        return baseMapper.selectByTeamId(id);
    }
    @Override
    public void resetByTeamId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("team_id",null).eq("team_id",id));
    }

    @Override
    public void resetByTeamId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("team_id",null).in("team_id",ids));
    }

    @Override
    public void removeByTeamId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("team_id",id));
    }

	@Override
    public List<Account_invoice> selectByVendorBillPurchaseId(Long id) {
        return baseMapper.selectByVendorBillPurchaseId(id);
    }
    @Override
    public void resetByVendorBillPurchaseId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("vendor_bill_purchase_id",null).eq("vendor_bill_purchase_id",id));
    }

    @Override
    public void resetByVendorBillPurchaseId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("vendor_bill_purchase_id",null).in("vendor_bill_purchase_id",ids));
    }

    @Override
    public void removeByVendorBillPurchaseId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("vendor_bill_purchase_id",id));
    }

	@Override
    public List<Account_invoice> selectByPurchaseId(Long id) {
        return baseMapper.selectByPurchaseId(id);
    }
    @Override
    public void resetByPurchaseId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("purchase_id",null).eq("purchase_id",id));
    }

    @Override
    public void resetByPurchaseId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("purchase_id",null).in("purchase_id",ids));
    }

    @Override
    public void removeByPurchaseId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("purchase_id",id));
    }

	@Override
    public List<Account_invoice> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("company_id",id));
    }

	@Override
    public List<Account_invoice> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("currency_id",id));
    }

	@Override
    public List<Account_invoice> selectByPartnerBankId(Long id) {
        return baseMapper.selectByPartnerBankId(id);
    }
    @Override
    public void resetByPartnerBankId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("partner_bank_id",null).eq("partner_bank_id",id));
    }

    @Override
    public void resetByPartnerBankId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("partner_bank_id",null).in("partner_bank_id",ids));
    }

    @Override
    public void removeByPartnerBankId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("partner_bank_id",id));
    }

	@Override
    public List<Account_invoice> selectByCommercialPartnerId(Long id) {
        return baseMapper.selectByCommercialPartnerId(id);
    }
    @Override
    public void resetByCommercialPartnerId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("commercial_partner_id",null).eq("commercial_partner_id",id));
    }

    @Override
    public void resetByCommercialPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("commercial_partner_id",null).in("commercial_partner_id",ids));
    }

    @Override
    public void removeByCommercialPartnerId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("commercial_partner_id",id));
    }

	@Override
    public List<Account_invoice> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public List<Account_invoice> selectByPartnerId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_invoice>().in("id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("partner_id",id));
    }

	@Override
    public List<Account_invoice> selectByPartnerShippingId(Long id) {
        return baseMapper.selectByPartnerShippingId(id);
    }
    @Override
    public void resetByPartnerShippingId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("partner_shipping_id",null).eq("partner_shipping_id",id));
    }

    @Override
    public void resetByPartnerShippingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("partner_shipping_id",null).in("partner_shipping_id",ids));
    }

    @Override
    public void removeByPartnerShippingId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("partner_shipping_id",id));
    }

	@Override
    public List<Account_invoice> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("create_uid",id));
    }

	@Override
    public List<Account_invoice> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("user_id",id));
    }

	@Override
    public List<Account_invoice> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("write_uid",id));
    }

	@Override
    public List<Account_invoice> selectByCampaignId(Long id) {
        return baseMapper.selectByCampaignId(id);
    }
    @Override
    public void resetByCampaignId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("campaign_id",null).eq("campaign_id",id));
    }

    @Override
    public void resetByCampaignId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("campaign_id",null).in("campaign_id",ids));
    }

    @Override
    public void removeByCampaignId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("campaign_id",id));
    }

	@Override
    public List<Account_invoice> selectByMediumId(Long id) {
        return baseMapper.selectByMediumId(id);
    }
    @Override
    public void resetByMediumId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("medium_id",null).eq("medium_id",id));
    }

    @Override
    public void resetByMediumId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("medium_id",null).in("medium_id",ids));
    }

    @Override
    public void removeByMediumId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("medium_id",id));
    }

	@Override
    public List<Account_invoice> selectBySourceId(Long id) {
        return baseMapper.selectBySourceId(id);
    }
    @Override
    public void resetBySourceId(Long id) {
        this.update(new UpdateWrapper<Account_invoice>().set("source_id",null).eq("source_id",id));
    }

    @Override
    public void resetBySourceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice>().set("source_id",null).in("source_id",ids));
    }

    @Override
    public void removeBySourceId(Long id) {
        this.remove(new QueryWrapper<Account_invoice>().eq("source_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_invoice> searchDefault(Account_invoiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_invoice> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_invoice>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_invoice et){
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_ACCOUNT__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_CASH_ROUNDING__CASH_ROUNDING_ID]
        if(!ObjectUtils.isEmpty(et.getCashRoundingId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_cash_rounding odooCashRounding=et.getOdooCashRounding();
            if(ObjectUtils.isEmpty(odooCashRounding)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_cash_rounding majorEntity=accountCashRoundingService.get(et.getCashRoundingId());
                et.setOdooCashRounding(majorEntity);
                odooCashRounding=majorEntity;
            }
            et.setCashRoundingIdText(odooCashRounding.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_FISCAL_POSITION__FISCAL_POSITION_ID]
        if(!ObjectUtils.isEmpty(et.getFiscalPositionId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition=et.getOdooFiscalPosition();
            if(ObjectUtils.isEmpty(odooFiscalPosition)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position majorEntity=accountFiscalPositionService.get(et.getFiscalPositionId());
                et.setOdooFiscalPosition(majorEntity);
                odooFiscalPosition=majorEntity;
            }
            et.setFiscalPositionIdText(odooFiscalPosition.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_INCOTERMS__INCOTERMS_ID]
        if(!ObjectUtils.isEmpty(et.getIncotermsId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterms=et.getOdooIncoterms();
            if(ObjectUtils.isEmpty(odooIncoterms)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms majorEntity=accountIncotermsService.get(et.getIncotermsId());
                et.setOdooIncoterms(majorEntity);
                odooIncoterms=majorEntity;
            }
            et.setIncotermsIdText(odooIncoterms.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_INCOTERMS__INCOTERM_ID]
        if(!ObjectUtils.isEmpty(et.getIncotermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterm=et.getOdooIncoterm();
            if(ObjectUtils.isEmpty(odooIncoterm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms majorEntity=accountIncotermsService.get(et.getIncotermId());
                et.setOdooIncoterm(majorEntity);
                odooIncoterm=majorEntity;
            }
            et.setIncotermIdText(odooIncoterm.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_INVOICE__REFUND_INVOICE_ID]
        if(!ObjectUtils.isEmpty(et.getRefundInvoiceId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooRefundInvoice=et.getOdooRefundInvoice();
            if(ObjectUtils.isEmpty(odooRefundInvoice)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice majorEntity=accountInvoiceService.get(et.getRefundInvoiceId());
                et.setOdooRefundInvoice(majorEntity);
                odooRefundInvoice=majorEntity;
            }
            et.setRefundInvoiceIdText(odooRefundInvoice.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_INVOICE__VENDOR_BILL_ID]
        if(!ObjectUtils.isEmpty(et.getVendorBillId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooVendorBill=et.getOdooVendorBill();
            if(ObjectUtils.isEmpty(odooVendorBill)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice majorEntity=accountInvoiceService.get(et.getVendorBillId());
                et.setOdooVendorBill(majorEntity);
                odooVendorBill=majorEntity;
            }
            et.setVendorBillIdText(odooVendorBill.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_JOURNAL__JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal=et.getOdooJournal();
            if(ObjectUtils.isEmpty(odooJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getJournalId());
                et.setOdooJournal(majorEntity);
                odooJournal=majorEntity;
            }
            et.setJournalIdText(odooJournal.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_MOVE__MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getMoveId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move odooMove=et.getOdooMove();
            if(ObjectUtils.isEmpty(odooMove)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move majorEntity=accountMoveService.get(et.getMoveId());
                et.setOdooMove(majorEntity);
                odooMove=majorEntity;
            }
            et.setNumber(odooMove.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__ACCOUNT_PAYMENT_TERM__PAYMENT_TERM_ID]
        if(!ObjectUtils.isEmpty(et.getPaymentTermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooPaymentTerm=et.getOdooPaymentTerm();
            if(ObjectUtils.isEmpty(odooPaymentTerm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term majorEntity=accountPaymentTermService.get(et.getPaymentTermId());
                et.setOdooPaymentTerm(majorEntity);
                odooPaymentTerm=majorEntity;
            }
            et.setPaymentTermIdText(odooPaymentTerm.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__CRM_TEAM__TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getTeamId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam=et.getOdooTeam();
            if(ObjectUtils.isEmpty(odooTeam)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team majorEntity=crmTeamService.get(et.getTeamId());
                et.setOdooTeam(majorEntity);
                odooTeam=majorEntity;
            }
            et.setTeamIdText(odooTeam.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__PURCHASE_BILL_UNION__VENDOR_BILL_PURCHASE_ID]
        if(!ObjectUtils.isEmpty(et.getVendorBillPurchaseId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_bill_union odooVendorBillPurchase=et.getOdooVendorBillPurchase();
            if(ObjectUtils.isEmpty(odooVendorBillPurchase)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_bill_union majorEntity=purchaseBillUnionService.get(et.getVendorBillPurchaseId());
                et.setOdooVendorBillPurchase(majorEntity);
                odooVendorBillPurchase=majorEntity;
            }
            et.setVendorBillPurchaseIdText(odooVendorBillPurchase.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__PURCHASE_ORDER__PURCHASE_ID]
        if(!ObjectUtils.isEmpty(et.getPurchaseId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order odooPurchase=et.getOdooPurchase();
            if(ObjectUtils.isEmpty(odooPurchase)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order majorEntity=purchaseOrderService.get(et.getPurchaseId());
                et.setOdooPurchase(majorEntity);
                odooPurchase=majorEntity;
            }
            et.setPurchaseIdText(odooPurchase.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyCurrencyId(odooCompany.getCurrencyId());
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__RES_PARTNER__COMMERCIAL_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getCommercialPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooCommercialPartner=et.getOdooCommercialPartner();
            if(ObjectUtils.isEmpty(odooCommercialPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getCommercialPartnerId());
                et.setOdooCommercialPartner(majorEntity);
                odooCommercialPartner=majorEntity;
            }
            et.setCommercialPartnerIdText(odooCommercialPartner.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__RES_PARTNER__PARTNER_SHIPPING_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerShippingId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerShipping=et.getOdooPartnerShipping();
            if(ObjectUtils.isEmpty(odooPartnerShipping)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerShippingId());
                et.setOdooPartnerShipping(majorEntity);
                odooPartnerShipping=majorEntity;
            }
            et.setPartnerShippingIdText(odooPartnerShipping.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__UTM_CAMPAIGN__CAMPAIGN_ID]
        if(!ObjectUtils.isEmpty(et.getCampaignId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign=et.getOdooCampaign();
            if(ObjectUtils.isEmpty(odooCampaign)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign majorEntity=utmCampaignService.get(et.getCampaignId());
                et.setOdooCampaign(majorEntity);
                odooCampaign=majorEntity;
            }
            et.setCampaignIdText(odooCampaign.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__UTM_MEDIUM__MEDIUM_ID]
        if(!ObjectUtils.isEmpty(et.getMediumId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium=et.getOdooMedium();
            if(ObjectUtils.isEmpty(odooMedium)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium majorEntity=utmMediumService.get(et.getMediumId());
                et.setOdooMedium(majorEntity);
                odooMedium=majorEntity;
            }
            et.setMediumIdText(odooMedium.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE__UTM_SOURCE__SOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getSourceId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource=et.getOdooSource();
            if(ObjectUtils.isEmpty(odooSource)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source majorEntity=utmSourceService.get(et.getSourceId());
                et.setOdooSource(majorEntity);
                odooSource=majorEntity;
            }
            et.setSourceIdText(odooSource.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_invoice> getAccountInvoiceByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_invoice> getAccountInvoiceByEntities(List<Account_invoice> entities) {
        List ids =new ArrayList();
        for(Account_invoice entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



