package cn.ibizlab.businesscentral.core.odoo_mrp.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mrp_bom_lineMapper extends BaseMapper<Mrp_bom_line>{

    Page<Mrp_bom_line> searchDefault(IPage page, @Param("srf") Mrp_bom_lineSearchContext context, @Param("ew") Wrapper<Mrp_bom_line> wrapper) ;
    @Override
    Mrp_bom_line selectById(Serializable id);
    @Override
    int insert(Mrp_bom_line entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mrp_bom_line entity);
    @Override
    int update(@Param(Constants.ENTITY) Mrp_bom_line entity, @Param("ew") Wrapper<Mrp_bom_line> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mrp_bom_line> selectByBomId(@Param("id") Serializable id) ;

    List<Mrp_bom_line> selectByOperationId(@Param("id") Serializable id) ;

    List<Mrp_bom_line> selectByRoutingId(@Param("id") Serializable id) ;

    List<Mrp_bom_line> selectByProductId(@Param("id") Serializable id) ;

    List<Mrp_bom_line> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mrp_bom_line> selectByWriteUid(@Param("id") Serializable id) ;

    List<Mrp_bom_line> selectByProductUomId(@Param("id") Serializable id) ;


}
