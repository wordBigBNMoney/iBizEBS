package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_reconcile_model_templateMapper extends BaseMapper<Account_reconcile_model_template>{

    Page<Account_reconcile_model_template> searchDefault(IPage page, @Param("srf") Account_reconcile_model_templateSearchContext context, @Param("ew") Wrapper<Account_reconcile_model_template> wrapper) ;
    @Override
    Account_reconcile_model_template selectById(Serializable id);
    @Override
    int insert(Account_reconcile_model_template entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_reconcile_model_template entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_reconcile_model_template entity, @Param("ew") Wrapper<Account_reconcile_model_template> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_reconcile_model_template> selectByAccountId(@Param("id") Serializable id) ;

    List<Account_reconcile_model_template> selectBySecondAccountId(@Param("id") Serializable id) ;

    List<Account_reconcile_model_template> selectByChartTemplateId(@Param("id") Serializable id) ;

    List<Account_reconcile_model_template> selectBySecondTaxId(@Param("id") Serializable id) ;

    List<Account_reconcile_model_template> selectByTaxId(@Param("id") Serializable id) ;

    List<Account_reconcile_model_template> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_reconcile_model_template> selectByWriteUid(@Param("id") Serializable id) ;


}
