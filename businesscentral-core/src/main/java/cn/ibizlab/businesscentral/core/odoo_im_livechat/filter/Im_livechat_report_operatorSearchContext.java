package cn.ibizlab.businesscentral.core.odoo_im_livechat.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_operator;
/**
 * 关系型数据实体[Im_livechat_report_operator] 查询条件对象
 */
@Slf4j
@Data
public class Im_livechat_report_operatorSearchContext extends QueryWrapperContext<Im_livechat_report_operator> {

	private String n_livechat_channel_id_text_eq;//[渠道]
	public void setN_livechat_channel_id_text_eq(String n_livechat_channel_id_text_eq) {
        this.n_livechat_channel_id_text_eq = n_livechat_channel_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_livechat_channel_id_text_eq)){
            this.getSearchCond().eq("livechat_channel_id_text", n_livechat_channel_id_text_eq);
        }
    }
	private String n_livechat_channel_id_text_like;//[渠道]
	public void setN_livechat_channel_id_text_like(String n_livechat_channel_id_text_like) {
        this.n_livechat_channel_id_text_like = n_livechat_channel_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_livechat_channel_id_text_like)){
            this.getSearchCond().like("livechat_channel_id_text", n_livechat_channel_id_text_like);
        }
    }
	private String n_channel_id_text_eq;//[对话]
	public void setN_channel_id_text_eq(String n_channel_id_text_eq) {
        this.n_channel_id_text_eq = n_channel_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_id_text_eq)){
            this.getSearchCond().eq("channel_id_text", n_channel_id_text_eq);
        }
    }
	private String n_channel_id_text_like;//[对话]
	public void setN_channel_id_text_like(String n_channel_id_text_like) {
        this.n_channel_id_text_like = n_channel_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_channel_id_text_like)){
            this.getSearchCond().like("channel_id_text", n_channel_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[运算符]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[运算符]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private Long n_channel_id_eq;//[对话]
	public void setN_channel_id_eq(Long n_channel_id_eq) {
        this.n_channel_id_eq = n_channel_id_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_id_eq)){
            this.getSearchCond().eq("channel_id", n_channel_id_eq);
        }
    }
	private Long n_partner_id_eq;//[运算符]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_livechat_channel_id_eq;//[渠道]
	public void setN_livechat_channel_id_eq(Long n_livechat_channel_id_eq) {
        this.n_livechat_channel_id_eq = n_livechat_channel_id_eq;
        if(!ObjectUtils.isEmpty(this.n_livechat_channel_id_eq)){
            this.getSearchCond().eq("livechat_channel_id", n_livechat_channel_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



