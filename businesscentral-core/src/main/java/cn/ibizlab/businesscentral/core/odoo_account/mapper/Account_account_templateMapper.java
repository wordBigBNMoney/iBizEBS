package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_account_templateSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_account_templateMapper extends BaseMapper<Account_account_template>{

    Page<Account_account_template> searchDefault(IPage page, @Param("srf") Account_account_templateSearchContext context, @Param("ew") Wrapper<Account_account_template> wrapper) ;
    @Override
    Account_account_template selectById(Serializable id);
    @Override
    int insert(Account_account_template entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_account_template entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_account_template entity, @Param("ew") Wrapper<Account_account_template> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_account_template> selectByUserTypeId(@Param("id") Serializable id) ;

    List<Account_account_template> selectByChartTemplateId(@Param("id") Serializable id) ;

    List<Account_account_template> selectByGroupId(@Param("id") Serializable id) ;

    List<Account_account_template> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Account_account_template> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_account_template> selectByWriteUid(@Param("id") Serializable id) ;


}
