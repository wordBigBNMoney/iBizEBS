package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_tracking_valueSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_tracking_value] 服务对象接口
 */
@Component
public class mail_tracking_valueFallback implements mail_tracking_valueFeignClient{

    public Page<Mail_tracking_value> search(Mail_tracking_valueSearchContext context){
            return null;
     }


    public Mail_tracking_value get(Long id){
            return null;
     }


    public Mail_tracking_value update(Long id, Mail_tracking_value mail_tracking_value){
            return null;
     }
    public Boolean updateBatch(List<Mail_tracking_value> mail_tracking_values){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Mail_tracking_value create(Mail_tracking_value mail_tracking_value){
            return null;
     }
    public Boolean createBatch(List<Mail_tracking_value> mail_tracking_values){
            return false;
     }


    public Page<Mail_tracking_value> select(){
            return null;
     }

    public Mail_tracking_value getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_tracking_value mail_tracking_value){
            return false;
     }


    public Boolean save(Mail_tracking_value mail_tracking_value){
            return false;
     }
    public Boolean saveBatch(List<Mail_tracking_value> mail_tracking_values){
            return false;
     }

    public Page<Mail_tracking_value> searchDefault(Mail_tracking_valueSearchContext context){
            return null;
     }


}
