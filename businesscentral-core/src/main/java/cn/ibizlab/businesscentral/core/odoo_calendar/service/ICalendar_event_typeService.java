package cn.ibizlab.businesscentral.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_event_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Calendar_event_type] 服务对象接口
 */
public interface ICalendar_event_typeService extends IService<Calendar_event_type>{

    boolean create(Calendar_event_type et) ;
    void createBatch(List<Calendar_event_type> list) ;
    boolean update(Calendar_event_type et) ;
    void updateBatch(List<Calendar_event_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Calendar_event_type get(Long key) ;
    Calendar_event_type getDraft(Calendar_event_type et) ;
    boolean checkKey(Calendar_event_type et) ;
    boolean save(Calendar_event_type et) ;
    void saveBatch(List<Calendar_event_type> list) ;
    Page<Calendar_event_type> searchDefault(Calendar_event_typeSearchContext context) ;
    List<Calendar_event_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Calendar_event_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Calendar_event_type> getCalendarEventTypeByIds(List<Long> ids) ;
    List<Calendar_event_type> getCalendarEventTypeByEntities(List<Calendar_event_type> entities) ;
}


