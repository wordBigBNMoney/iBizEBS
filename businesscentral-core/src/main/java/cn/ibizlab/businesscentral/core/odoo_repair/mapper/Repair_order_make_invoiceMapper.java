package cn.ibizlab.businesscentral.core.odoo_repair.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Repair_order_make_invoiceMapper extends BaseMapper<Repair_order_make_invoice>{

    Page<Repair_order_make_invoice> searchDefault(IPage page, @Param("srf") Repair_order_make_invoiceSearchContext context, @Param("ew") Wrapper<Repair_order_make_invoice> wrapper) ;
    @Override
    Repair_order_make_invoice selectById(Serializable id);
    @Override
    int insert(Repair_order_make_invoice entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Repair_order_make_invoice entity);
    @Override
    int update(@Param(Constants.ENTITY) Repair_order_make_invoice entity, @Param("ew") Wrapper<Repair_order_make_invoice> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Repair_order_make_invoice> selectByCreateUid(@Param("id") Serializable id) ;

    List<Repair_order_make_invoice> selectByWriteUid(@Param("id") Serializable id) ;


}
