package cn.ibizlab.businesscentral.core.odoo_asset.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.businesscentral.core.odoo_asset.filter.Asset_assetSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Asset_asset] 服务对象接口
 */
public interface IAsset_assetService extends IService<Asset_asset>{

    boolean create(Asset_asset et) ;
    void createBatch(List<Asset_asset> list) ;
    boolean update(Asset_asset et) ;
    void updateBatch(List<Asset_asset> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Asset_asset get(Long key) ;
    Asset_asset getDraft(Asset_asset et) ;
    boolean checkKey(Asset_asset et) ;
    boolean save(Asset_asset et) ;
    void saveBatch(List<Asset_asset> list) ;
    Page<Asset_asset> searchDefault(Asset_assetSearchContext context) ;
    List<Asset_asset> selectByAccountingStateId(Long id);
    void resetByAccountingStateId(Long id);
    void resetByAccountingStateId(Collection<Long> ids);
    void removeByAccountingStateId(Long id);
    List<Asset_asset> selectByFinanceStateId(Long id);
    void resetByFinanceStateId(Long id);
    void resetByFinanceStateId(Collection<Long> ids);
    void removeByFinanceStateId(Long id);
    List<Asset_asset> selectByMaintenanceStateId(Long id);
    void resetByMaintenanceStateId(Long id);
    void resetByMaintenanceStateId(Collection<Long> ids);
    void removeByMaintenanceStateId(Long id);
    List<Asset_asset> selectByManufactureStateId(Long id);
    void resetByManufactureStateId(Long id);
    void resetByManufactureStateId(Collection<Long> ids);
    void removeByManufactureStateId(Long id);
    List<Asset_asset> selectByWarehouseStateId(Long id);
    void resetByWarehouseStateId(Long id);
    void resetByWarehouseStateId(Collection<Long> ids);
    void removeByWarehouseStateId(Long id);
    List<Asset_asset> selectByManufacturerId(Long id);
    void resetByManufacturerId(Long id);
    void resetByManufacturerId(Collection<Long> ids);
    void removeByManufacturerId(Long id);
    List<Asset_asset> selectByVendorId(Long id);
    void resetByVendorId(Long id);
    void resetByVendorId(Collection<Long> ids);
    void removeByVendorId(Long id);
    List<Asset_asset> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Asset_asset> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Asset_asset> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Asset_asset> getAssetAssetByIds(List<Long> ids) ;
    List<Asset_asset> getAssetAssetByEntities(List<Asset_asset> entities) ;
}


