package cn.ibizlab.businesscentral.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_surveySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Survey_survey] 服务对象接口
 */
public interface ISurvey_surveyService extends IService<Survey_survey>{

    boolean create(Survey_survey et) ;
    void createBatch(List<Survey_survey> list) ;
    boolean update(Survey_survey et) ;
    void updateBatch(List<Survey_survey> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Survey_survey get(Long key) ;
    Survey_survey getDraft(Survey_survey et) ;
    boolean checkKey(Survey_survey et) ;
    boolean save(Survey_survey et) ;
    void saveBatch(List<Survey_survey> list) ;
    Page<Survey_survey> searchDefault(Survey_surveySearchContext context) ;
    List<Survey_survey> selectByEmailTemplateId(Long id);
    void resetByEmailTemplateId(Long id);
    void resetByEmailTemplateId(Collection<Long> ids);
    void removeByEmailTemplateId(Long id);
    List<Survey_survey> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Survey_survey> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Survey_survey> selectByStageId(Long id);
    List<Survey_survey> selectByStageId(Collection<Long> ids);
    void removeByStageId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Survey_survey> getSurveySurveyByIds(List<Long> ids) ;
    List<Survey_survey> getSurveySurveyByEntities(List<Survey_survey> entities) ;
}


