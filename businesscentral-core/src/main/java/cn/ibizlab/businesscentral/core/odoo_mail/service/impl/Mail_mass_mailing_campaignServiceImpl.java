package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_campaignService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_mass_mailing_campaignMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[群发邮件营销] 服务对象接口实现
 */
@Slf4j
@Service("Mail_mass_mailing_campaignServiceImpl")
public class Mail_mass_mailing_campaignServiceImpl extends EBSServiceImpl<Mail_mass_mailing_campaignMapper, Mail_mass_mailing_campaign> implements IMail_mass_mailing_campaignService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_compose_messageService mailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mail_statisticsService mailMailStatisticsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailingService mailMassMailingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_mail_compose_messageService surveyMailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_stageService mailMassMailingStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_campaignService utmCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mediumService utmMediumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService utmSourceService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.mass.mailing.campaign" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_mass_mailing_campaign et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mass_mailing_campaignService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_mass_mailing_campaign> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_mass_mailing_campaign et) {
        Mail_mass_mailing_campaign old = new Mail_mass_mailing_campaign() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mass_mailing_campaignService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mass_mailing_campaignService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_mass_mailing_campaign> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        mailComposeMessageService.resetByMassMailingCampaignId(key);
        mailMailStatisticsService.resetByMassMailingCampaignId(key);
        mailMassMailingService.resetByMassMailingCampaignId(key);
        surveyMailComposeMessageService.resetByMassMailingCampaignId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        mailComposeMessageService.resetByMassMailingCampaignId(idList);
        mailMailStatisticsService.resetByMassMailingCampaignId(idList);
        mailMassMailingService.resetByMassMailingCampaignId(idList);
        surveyMailComposeMessageService.resetByMassMailingCampaignId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_mass_mailing_campaign get(Long key) {
        Mail_mass_mailing_campaign et = getById(key);
        if(et==null){
            et=new Mail_mass_mailing_campaign();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_mass_mailing_campaign getDraft(Mail_mass_mailing_campaign et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_mass_mailing_campaign et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_mass_mailing_campaign et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_mass_mailing_campaign et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_mass_mailing_campaign> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_mass_mailing_campaign> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_mass_mailing_campaign> selectByStageId(Long id) {
        return baseMapper.selectByStageId(id);
    }
    @Override
    public List<Mail_mass_mailing_campaign> selectByStageId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mail_mass_mailing_campaign>().in("id",ids));
    }

    @Override
    public void removeByStageId(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_campaign>().eq("stage_id",id));
    }

	@Override
    public List<Mail_mass_mailing_campaign> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_campaign>().eq("create_uid",id));
    }

	@Override
    public List<Mail_mass_mailing_campaign> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Mail_mass_mailing_campaign>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_mass_mailing_campaign>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_campaign>().eq("user_id",id));
    }

	@Override
    public List<Mail_mass_mailing_campaign> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_campaign>().eq("write_uid",id));
    }

	@Override
    public List<Mail_mass_mailing_campaign> selectByCampaignId(Long id) {
        return baseMapper.selectByCampaignId(id);
    }
    @Override
    public void removeByCampaignId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Mail_mass_mailing_campaign>().in("campaign_id",ids));
    }

    @Override
    public void removeByCampaignId(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_campaign>().eq("campaign_id",id));
    }

	@Override
    public List<Mail_mass_mailing_campaign> selectByMediumId(Long id) {
        return baseMapper.selectByMediumId(id);
    }
    @Override
    public void resetByMediumId(Long id) {
        this.update(new UpdateWrapper<Mail_mass_mailing_campaign>().set("medium_id",null).eq("medium_id",id));
    }

    @Override
    public void resetByMediumId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_mass_mailing_campaign>().set("medium_id",null).in("medium_id",ids));
    }

    @Override
    public void removeByMediumId(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_campaign>().eq("medium_id",id));
    }

	@Override
    public List<Mail_mass_mailing_campaign> selectBySourceId(Long id) {
        return baseMapper.selectBySourceId(id);
    }
    @Override
    public void resetBySourceId(Long id) {
        this.update(new UpdateWrapper<Mail_mass_mailing_campaign>().set("source_id",null).eq("source_id",id));
    }

    @Override
    public void resetBySourceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_mass_mailing_campaign>().set("source_id",null).in("source_id",ids));
    }

    @Override
    public void removeBySourceId(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_campaign>().eq("source_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_mass_mailing_campaign> searchDefault(Mail_mass_mailing_campaignSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_mass_mailing_campaign> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_mass_mailing_campaign>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_mass_mailing_campaign et){
        //实体关系[DER1N_MAIL_MASS_MAILING_CAMPAIGN__MAIL_MASS_MAILING_STAGE__STAGE_ID]
        if(!ObjectUtils.isEmpty(et.getStageId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_stage odooStage=et.getOdooStage();
            if(ObjectUtils.isEmpty(odooStage)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_stage majorEntity=mailMassMailingStageService.get(et.getStageId());
                et.setOdooStage(majorEntity);
                odooStage=majorEntity;
            }
            et.setStageIdText(odooStage.getName());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_CAMPAIGN__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_CAMPAIGN__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_CAMPAIGN__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_CAMPAIGN__UTM_CAMPAIGN__CAMPAIGN_ID]
        if(!ObjectUtils.isEmpty(et.getCampaignId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign=et.getOdooCampaign();
            if(ObjectUtils.isEmpty(odooCampaign)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign majorEntity=utmCampaignService.get(et.getCampaignId());
                et.setOdooCampaign(majorEntity);
                odooCampaign=majorEntity;
            }
            et.setName(odooCampaign.getName());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_CAMPAIGN__UTM_MEDIUM__MEDIUM_ID]
        if(!ObjectUtils.isEmpty(et.getMediumId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium=et.getOdooMedium();
            if(ObjectUtils.isEmpty(odooMedium)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium majorEntity=utmMediumService.get(et.getMediumId());
                et.setOdooMedium(majorEntity);
                odooMedium=majorEntity;
            }
            et.setMediumIdText(odooMedium.getName());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_CAMPAIGN__UTM_SOURCE__SOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getSourceId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource=et.getOdooSource();
            if(ObjectUtils.isEmpty(odooSource)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source majorEntity=utmSourceService.get(et.getSourceId());
                et.setOdooSource(majorEntity);
                odooSource=majorEntity;
            }
            et.setSourceIdText(odooSource.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mail_mass_mailing_campaign> getMailMassMailingCampaignByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mail_mass_mailing_campaign> getMailMassMailingCampaignByEntities(List<Mail_mass_mailing_campaign> entities) {
        List ids =new ArrayList();
        for(Mail_mass_mailing_campaign entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



