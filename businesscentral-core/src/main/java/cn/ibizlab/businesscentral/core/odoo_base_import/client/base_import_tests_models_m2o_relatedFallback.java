package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_m2o_related;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_m2o_relatedSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_m2o_related] 服务对象接口
 */
@Component
public class base_import_tests_models_m2o_relatedFallback implements base_import_tests_models_m2o_relatedFeignClient{


    public Page<Base_import_tests_models_m2o_related> search(Base_import_tests_models_m2o_relatedSearchContext context){
            return null;
     }




    public Base_import_tests_models_m2o_related update(Long id, Base_import_tests_models_m2o_related base_import_tests_models_m2o_related){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_m2o_related> base_import_tests_models_m2o_relateds){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Base_import_tests_models_m2o_related create(Base_import_tests_models_m2o_related base_import_tests_models_m2o_related){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_m2o_related> base_import_tests_models_m2o_relateds){
            return false;
     }

    public Base_import_tests_models_m2o_related get(Long id){
            return null;
     }


    public Page<Base_import_tests_models_m2o_related> select(){
            return null;
     }

    public Base_import_tests_models_m2o_related getDraft(){
            return null;
    }



    public Boolean checkKey(Base_import_tests_models_m2o_related base_import_tests_models_m2o_related){
            return false;
     }


    public Boolean save(Base_import_tests_models_m2o_related base_import_tests_models_m2o_related){
            return false;
     }
    public Boolean saveBatch(List<Base_import_tests_models_m2o_related> base_import_tests_models_m2o_relateds){
            return false;
     }

    public Page<Base_import_tests_models_m2o_related> searchDefault(Base_import_tests_models_m2o_relatedSearchContext context){
            return null;
     }


}
