package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quant_packageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_quant_package] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-quant-package", fallback = stock_quant_packageFallback.class)
public interface stock_quant_packageFeignClient {



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_quant_packages/{id}")
    Stock_quant_package update(@PathVariable("id") Long id,@RequestBody Stock_quant_package stock_quant_package);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_quant_packages/batch")
    Boolean updateBatch(@RequestBody List<Stock_quant_package> stock_quant_packages);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_quant_packages/{id}")
    Stock_quant_package get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages")
    Stock_quant_package create(@RequestBody Stock_quant_package stock_quant_package);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/batch")
    Boolean createBatch(@RequestBody List<Stock_quant_package> stock_quant_packages);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/search")
    Page<Stock_quant_package> search(@RequestBody Stock_quant_packageSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_quant_packages/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_quant_packages/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_quant_packages/select")
    Page<Stock_quant_package> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_quant_packages/getdraft")
    Stock_quant_package getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/checkkey")
    Boolean checkKey(@RequestBody Stock_quant_package stock_quant_package);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/save")
    Boolean save(@RequestBody Stock_quant_package stock_quant_package);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_quant_package> stock_quant_packages);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/searchdefault")
    Page<Stock_quant_package> searchDefault(@RequestBody Stock_quant_packageSearchContext context);


}
