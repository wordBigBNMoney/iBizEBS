package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leaveSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_leave] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-leave", fallback = hr_leaveFallback.class)
public interface hr_leaveFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_leaves/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_leaves/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/hr_leaves")
    Hr_leave create(@RequestBody Hr_leave hr_leave);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/batch")
    Boolean createBatch(@RequestBody List<Hr_leave> hr_leaves);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/search")
    Page<Hr_leave> search(@RequestBody Hr_leaveSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leaves/{id}")
    Hr_leave get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_leaves/{id}")
    Hr_leave update(@PathVariable("id") Long id,@RequestBody Hr_leave hr_leave);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_leaves/batch")
    Boolean updateBatch(@RequestBody List<Hr_leave> hr_leaves);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leaves/select")
    Page<Hr_leave> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leaves/getdraft")
    Hr_leave getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/checkkey")
    Boolean checkKey(@RequestBody Hr_leave hr_leave);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/save")
    Boolean save(@RequestBody Hr_leave hr_leave);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_leave> hr_leaves);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/searchdefault")
    Page<Hr_leave> searchDefault(@RequestBody Hr_leaveSearchContext context);


}
