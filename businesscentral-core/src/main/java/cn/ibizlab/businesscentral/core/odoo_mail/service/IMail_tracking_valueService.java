package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_tracking_valueSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_tracking_value] 服务对象接口
 */
public interface IMail_tracking_valueService extends IService<Mail_tracking_value>{

    boolean create(Mail_tracking_value et) ;
    void createBatch(List<Mail_tracking_value> list) ;
    boolean update(Mail_tracking_value et) ;
    void updateBatch(List<Mail_tracking_value> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_tracking_value get(Long key) ;
    Mail_tracking_value getDraft(Mail_tracking_value et) ;
    boolean checkKey(Mail_tracking_value et) ;
    boolean save(Mail_tracking_value et) ;
    void saveBatch(List<Mail_tracking_value> list) ;
    Page<Mail_tracking_value> searchDefault(Mail_tracking_valueSearchContext context) ;
    List<Mail_tracking_value> selectByMailMessageId(Long id);
    void removeByMailMessageId(Collection<Long> ids);
    void removeByMailMessageId(Long id);
    List<Mail_tracking_value> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_tracking_value> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_tracking_value> getMailTrackingValueByIds(List<Long> ids) ;
    List<Mail_tracking_value> getMailTrackingValueByEntities(List<Mail_tracking_value> entities) ;
}


