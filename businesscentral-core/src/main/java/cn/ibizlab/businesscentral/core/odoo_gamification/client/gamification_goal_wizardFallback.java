package cn.ibizlab.businesscentral.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goal_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[gamification_goal_wizard] 服务对象接口
 */
@Component
public class gamification_goal_wizardFallback implements gamification_goal_wizardFeignClient{


    public Gamification_goal_wizard update(Long id, Gamification_goal_wizard gamification_goal_wizard){
            return null;
     }
    public Boolean updateBatch(List<Gamification_goal_wizard> gamification_goal_wizards){
            return false;
     }


    public Gamification_goal_wizard create(Gamification_goal_wizard gamification_goal_wizard){
            return null;
     }
    public Boolean createBatch(List<Gamification_goal_wizard> gamification_goal_wizards){
            return false;
     }

    public Gamification_goal_wizard get(Long id){
            return null;
     }




    public Page<Gamification_goal_wizard> search(Gamification_goal_wizardSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Gamification_goal_wizard> select(){
            return null;
     }

    public Gamification_goal_wizard getDraft(){
            return null;
    }



    public Boolean checkKey(Gamification_goal_wizard gamification_goal_wizard){
            return false;
     }


    public Boolean save(Gamification_goal_wizard gamification_goal_wizard){
            return false;
     }
    public Boolean saveBatch(List<Gamification_goal_wizard> gamification_goal_wizards){
            return false;
     }

    public Page<Gamification_goal_wizard> searchDefault(Gamification_goal_wizardSearchContext context){
            return null;
     }


}
