package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_pickingSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_picking] 服务对象接口
 */
public interface IStock_pickingService extends IService<Stock_picking>{

    boolean create(Stock_picking et) ;
    void createBatch(List<Stock_picking> list) ;
    boolean update(Stock_picking et) ;
    void updateBatch(List<Stock_picking> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_picking get(Long key) ;
    Stock_picking getDraft(Stock_picking et) ;
    boolean checkKey(Stock_picking et) ;
    boolean save(Stock_picking et) ;
    void saveBatch(List<Stock_picking> list) ;
    Page<Stock_picking> searchDefault(Stock_pickingSearchContext context) ;
    List<Stock_picking> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_picking> selectByOwnerId(Long id);
    void resetByOwnerId(Long id);
    void resetByOwnerId(Collection<Long> ids);
    void removeByOwnerId(Long id);
    List<Stock_picking> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Stock_picking> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_picking> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_picking> selectBySaleId(Long id);
    void resetBySaleId(Long id);
    void resetBySaleId(Collection<Long> ids);
    void removeBySaleId(Long id);
    List<Stock_picking> selectByLocationDestId(Long id);
    void resetByLocationDestId(Long id);
    void resetByLocationDestId(Collection<Long> ids);
    void removeByLocationDestId(Long id);
    List<Stock_picking> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_picking> selectByPickingTypeId(Long id);
    void resetByPickingTypeId(Long id);
    void resetByPickingTypeId(Collection<Long> ids);
    void removeByPickingTypeId(Long id);
    List<Stock_picking> selectByBackorderId(Long id);
    void resetByBackorderId(Long id);
    void resetByBackorderId(Collection<Long> ids);
    void removeByBackorderId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_picking> getStockPickingByIds(List<Long> ids) ;
    List<Stock_picking> getStockPickingByEntities(List<Stock_picking> entities) ;
}


