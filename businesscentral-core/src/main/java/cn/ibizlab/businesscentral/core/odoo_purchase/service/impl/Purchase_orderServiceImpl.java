package cn.ibizlab.businesscentral.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_orderSearchContext;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_purchase.mapper.Purchase_orderMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[采购订单] 服务对象接口实现
 */
@Slf4j
@Service("Purchase_orderServiceImpl")
public class Purchase_orderServiceImpl extends EBSServiceImpl<Purchase_orderMapper, Purchase_order> implements IPurchase_orderService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IAccount_move_purchase_order_relService accountMovePurchaseOrderRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_bill_unionService purchaseBillUnionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService purchaseRequisitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService resSupplierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_incotermsService accountIncotermsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService accountPaymentTermService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_orderget_nameLogic get_nameLogic;

    protected int batchSize = 500;

    public String getIrModel(){
        return "purchase.order" ;
    }

    private boolean messageinfo = true ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Purchase_order et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        get_nameLogic.execute(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_orderService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Purchase_order> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Purchase_order et) {
        Purchase_order old = new Purchase_order() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_orderService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_orderService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Purchase_order> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountInvoiceService.resetByPurchaseId(key);
        purchaseBillUnionService.resetByPurchaseOrderId(key);
        purchaseOrderLineService.removeByOrderId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountInvoiceService.resetByPurchaseId(idList);
        purchaseBillUnionService.resetByPurchaseOrderId(idList);
        purchaseOrderLineService.removeByOrderId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Purchase_order get(Long key) {
        Purchase_order et = getById(key);
        if(et==null){
            et=new Purchase_order();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Purchase_order getDraft(Purchase_order et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Purchase_order button_approve(Purchase_order et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_order button_cancel(Purchase_order et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_order button_confirm(Purchase_order et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_order button_done(Purchase_order et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_order button_unlock(Purchase_order et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Purchase_order calc_amount(Purchase_order et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Purchase_order et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public Purchase_order get_name(Purchase_order et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(Purchase_order et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Purchase_order et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Purchase_order> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Purchase_order> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Purchase_order> selectByRequisitionId(Long id) {
        return baseMapper.selectByRequisitionId(id);
    }
    @Override
    public void removeByRequisitionId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("requisition_id",id));
    }

	@Override
    public List<Purchase_order> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("partner_id",id));
    }

	@Override
    public List<Purchase_order> selectByFiscalPositionId(Long id) {
        return baseMapper.selectByFiscalPositionId(id);
    }
    @Override
    public void resetByFiscalPositionId(Long id) {
        this.update(new UpdateWrapper<Purchase_order>().set("fiscal_position_id",null).eq("fiscal_position_id",id));
    }

    @Override
    public void resetByFiscalPositionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order>().set("fiscal_position_id",null).in("fiscal_position_id",ids));
    }

    @Override
    public void removeByFiscalPositionId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("fiscal_position_id",id));
    }

	@Override
    public List<Purchase_order> selectByIncotermId(Long id) {
        return baseMapper.selectByIncotermId(id);
    }
    @Override
    public void resetByIncotermId(Long id) {
        this.update(new UpdateWrapper<Purchase_order>().set("incoterm_id",null).eq("incoterm_id",id));
    }

    @Override
    public void resetByIncotermId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order>().set("incoterm_id",null).in("incoterm_id",ids));
    }

    @Override
    public void removeByIncotermId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("incoterm_id",id));
    }

	@Override
    public List<Purchase_order> selectByPaymentTermId(Long id) {
        return baseMapper.selectByPaymentTermId(id);
    }
    @Override
    public void resetByPaymentTermId(Long id) {
        this.update(new UpdateWrapper<Purchase_order>().set("payment_term_id",null).eq("payment_term_id",id));
    }

    @Override
    public void resetByPaymentTermId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order>().set("payment_term_id",null).in("payment_term_id",ids));
    }

    @Override
    public void removeByPaymentTermId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("payment_term_id",id));
    }

	@Override
    public List<Purchase_order> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Purchase_order>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("company_id",id));
    }

	@Override
    public List<Purchase_order> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Purchase_order>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("currency_id",id));
    }

	@Override
    public List<Purchase_order> selectByDestAddressId(Long id) {
        return baseMapper.selectByDestAddressId(id);
    }
    @Override
    public void resetByDestAddressId(Long id) {
        this.update(new UpdateWrapper<Purchase_order>().set("dest_address_id",null).eq("dest_address_id",id));
    }

    @Override
    public void resetByDestAddressId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order>().set("dest_address_id",null).in("dest_address_id",ids));
    }

    @Override
    public void removeByDestAddressId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("dest_address_id",id));
    }

	@Override
    public List<Purchase_order> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("create_uid",id));
    }

	@Override
    public List<Purchase_order> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Purchase_order>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("user_id",id));
    }

	@Override
    public List<Purchase_order> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("write_uid",id));
    }

	@Override
    public List<Purchase_order> selectByPickingTypeId(Long id) {
        return baseMapper.selectByPickingTypeId(id);
    }
    @Override
    public void resetByPickingTypeId(Long id) {
        this.update(new UpdateWrapper<Purchase_order>().set("picking_type_id",null).eq("picking_type_id",id));
    }

    @Override
    public void resetByPickingTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order>().set("picking_type_id",null).in("picking_type_id",ids));
    }

    @Override
    public void removeByPickingTypeId(Long id) {
        this.remove(new QueryWrapper<Purchase_order>().eq("picking_type_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Purchase_order> searchDefault(Purchase_orderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_order> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_order>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 首选表格
     */
    @Override
    public Page<Purchase_order> searchMaster(Purchase_orderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_order> pages=baseMapper.searchMaster(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_order>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 订单
     */
    @Override
    public Page<Purchase_order> searchOrder(Purchase_orderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_order> pages=baseMapper.searchOrder(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_order>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Purchase_order et){
        //实体关系[DER1N_PURCHASE_ORDER_PURCHASE_REQUISITION_REQUISITION_ID]
        if(!ObjectUtils.isEmpty(et.getRequisitionId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition odooRequisition=et.getOdooRequisition();
            if(ObjectUtils.isEmpty(odooRequisition)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition majorEntity=purchaseRequisitionService.get(et.getRequisitionId());
                et.setOdooRequisition(majorEntity);
                odooRequisition=majorEntity;
            }
            et.setRequisitionIdText(odooRequisition.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_RES_SUPPLIER_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier majorEntity=resSupplierService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__ACCOUNT_FISCAL_POSITION__FISCAL_POSITION_ID]
        if(!ObjectUtils.isEmpty(et.getFiscalPositionId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition=et.getOdooFiscalPosition();
            if(ObjectUtils.isEmpty(odooFiscalPosition)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position majorEntity=accountFiscalPositionService.get(et.getFiscalPositionId());
                et.setOdooFiscalPosition(majorEntity);
                odooFiscalPosition=majorEntity;
            }
            et.setFiscalPositionIdText(odooFiscalPosition.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__ACCOUNT_INCOTERMS__INCOTERM_ID]
        if(!ObjectUtils.isEmpty(et.getIncotermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterm=et.getOdooIncoterm();
            if(ObjectUtils.isEmpty(odooIncoterm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms majorEntity=accountIncotermsService.get(et.getIncotermId());
                et.setOdooIncoterm(majorEntity);
                odooIncoterm=majorEntity;
            }
            et.setIncotermIdText(odooIncoterm.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__ACCOUNT_PAYMENT_TERM__PAYMENT_TERM_ID]
        if(!ObjectUtils.isEmpty(et.getPaymentTermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooPaymentTerm=et.getOdooPaymentTerm();
            if(ObjectUtils.isEmpty(odooPaymentTerm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term majorEntity=accountPaymentTermService.get(et.getPaymentTermId());
                et.setOdooPaymentTerm(majorEntity);
                odooPaymentTerm=majorEntity;
            }
            et.setPaymentTermIdText(odooPaymentTerm.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__RES_PARTNER__DEST_ADDRESS_ID]
        if(!ObjectUtils.isEmpty(et.getDestAddressId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooDestAddress=et.getOdooDestAddress();
            if(ObjectUtils.isEmpty(odooDestAddress)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getDestAddressId());
                et.setOdooDestAddress(majorEntity);
                odooDestAddress=majorEntity;
            }
            et.setDestAddressIdText(odooDestAddress.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER__STOCK_PICKING_TYPE__PICKING_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPickingTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickingType=et.getOdooPickingType();
            if(ObjectUtils.isEmpty(odooPickingType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getPickingTypeId());
                et.setOdooPickingType(majorEntity);
                odooPickingType=majorEntity;
            }
            et.setPickingTypeIdText(odooPickingType.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Purchase_order> getPurchaseOrderByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Purchase_order> getPurchaseOrderByEntities(List<Purchase_order> entities) {
        List ids =new ArrayList();
        for(Purchase_order entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



