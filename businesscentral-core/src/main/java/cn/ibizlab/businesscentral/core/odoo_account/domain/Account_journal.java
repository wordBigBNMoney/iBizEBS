package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[日记账]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_JOURNAL",resultMap = "Account_journalResultMap")
public class Account_journal extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 信用票：下一号码
     */
    @TableField(exist = false)
    @JSONField(name = "refund_sequence_number_next")
    @JsonProperty("refund_sequence_number_next")
    private Integer refundSequenceNumberNext;
    /**
     * 允许的科目类型
     */
    @TableField(exist = false)
    @JSONField(name = "type_control_ids")
    @JsonProperty("type_control_ids")
    private String typeControlIds;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 至少一个转出
     */
    @DEField(name = "at_least_one_outbound")
    @TableField(value = "at_least_one_outbound")
    @JSONField(name = "at_least_one_outbound")
    @JsonProperty("at_least_one_outbound")
    private Boolean atLeastOneOutbound;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 分录序列
     */
    @DEField(name = "sequence_id")
    @TableField(value = "sequence_id")
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Integer sequenceId;
    /**
     * 在仪表板显示日记账
     */
    @DEField(name = "show_on_dashboard")
    @TableField(value = "show_on_dashboard")
    @JSONField(name = "show_on_dashboard")
    @JsonProperty("show_on_dashboard")
    private Boolean showOnDashboard;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 银行费用
     */
    @DEField(name = "bank_statements_source")
    @TableField(value = "bank_statements_source")
    @JSONField(name = "bank_statements_source")
    @JsonProperty("bank_statements_source")
    private String bankStatementsSource;
    /**
     * 至少一个转入
     */
    @DEField(name = "at_least_one_inbound")
    @TableField(value = "at_least_one_inbound")
    @JSONField(name = "at_least_one_inbound")
    @JsonProperty("at_least_one_inbound")
    private Boolean atLeastOneInbound;
    /**
     * 日记账名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 下一号码
     */
    @TableField(exist = false)
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    private Integer sequenceNumberNext;
    /**
     * 属于用户的当前公司
     */
    @TableField(exist = false)
    @JSONField(name = "belongs_to_company")
    @JsonProperty("belongs_to_company")
    private Boolean belongsToCompany;
    /**
     * 简码
     */
    @TableField(value = "code")
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 为收款
     */
    @TableField(exist = false)
    @JSONField(name = "inbound_payment_method_ids")
    @JsonProperty("inbound_payment_method_ids")
    private String inboundPaymentMethodIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 信用票分录序列
     */
    @DEField(name = "refund_sequence_id")
    @TableField(value = "refund_sequence_id")
    @JSONField(name = "refund_sequence_id")
    @JsonProperty("refund_sequence_id")
    private Integer refundSequenceId;
    /**
     * 别名域
     */
    @TableField(exist = false)
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;
    /**
     * 允许的科目
     */
    @TableField(exist = false)
    @JSONField(name = "account_control_ids")
    @JsonProperty("account_control_ids")
    private String accountControlIds;
    /**
     * 看板仪表板图表
     */
    @TableField(exist = false)
    @JSONField(name = "kanban_dashboard_graph")
    @JsonProperty("kanban_dashboard_graph")
    private String kanbanDashboardGraph;
    /**
     * 为付款
     */
    @TableField(exist = false)
    @JSONField(name = "outbound_payment_method_ids")
    @JsonProperty("outbound_payment_method_ids")
    private String outboundPaymentMethodIds;
    /**
     * 类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 看板仪表板
     */
    @TableField(exist = false)
    @JSONField(name = "kanban_dashboard")
    @JsonProperty("kanban_dashboard")
    private String kanbanDashboard;
    /**
     * 专用的信用票序列
     */
    @DEField(name = "refund_sequence")
    @TableField(value = "refund_sequence")
    @JSONField(name = "refund_sequence")
    @JsonProperty("refund_sequence")
    private Boolean refundSequence;
    /**
     * 颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 利润科目
     */
    @TableField(exist = false)
    @JSONField(name = "profit_account_id_text")
    @JsonProperty("profit_account_id_text")
    private String profitAccountIdText;
    /**
     * 供应商账单的别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;
    /**
     * 账户号码
     */
    @TableField(exist = false)
    @JSONField(name = "bank_acc_number")
    @JsonProperty("bank_acc_number")
    private String bankAccNumber;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 银行
     */
    @TableField(exist = false)
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    private Long bankId;
    /**
     * 损失科目
     */
    @TableField(exist = false)
    @JSONField(name = "loss_account_id_text")
    @JsonProperty("loss_account_id_text")
    private String lossAccountIdText;
    /**
     * 默认贷方科目
     */
    @TableField(exist = false)
    @JSONField(name = "default_credit_account_id_text")
    @JsonProperty("default_credit_account_id_text")
    private String defaultCreditAccountIdText;
    /**
     * 账户持有人
     */
    @TableField(exist = false)
    @JSONField(name = "company_partner_id")
    @JsonProperty("company_partner_id")
    private Long companyPartnerId;
    /**
     * 默认借方科目
     */
    @TableField(exist = false)
    @JSONField(name = "default_debit_account_id_text")
    @JsonProperty("default_debit_account_id_text")
    private String defaultDebitAccountIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 银行账户
     */
    @DEField(name = "bank_account_id")
    @TableField(value = "bank_account_id")
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    private Long bankAccountId;
    /**
     * 损失科目
     */
    @DEField(name = "loss_account_id")
    @TableField(value = "loss_account_id")
    @JSONField(name = "loss_account_id")
    @JsonProperty("loss_account_id")
    private Long lossAccountId;
    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @TableField(value = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Long aliasId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 默认贷方科目
     */
    @DEField(name = "default_credit_account_id")
    @TableField(value = "default_credit_account_id")
    @JSONField(name = "default_credit_account_id")
    @JsonProperty("default_credit_account_id")
    private Long defaultCreditAccountId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 默认借方科目
     */
    @DEField(name = "default_debit_account_id")
    @TableField(value = "default_debit_account_id")
    @JSONField(name = "default_debit_account_id")
    @JsonProperty("default_debit_account_id")
    private Long defaultDebitAccountId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 利润科目
     */
    @DEField(name = "profit_account_id")
    @TableField(value = "profit_account_id")
    @JSONField(name = "profit_account_id")
    @JsonProperty("profit_account_id")
    private Long profitAccountId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooDefaultCreditAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooDefaultDebitAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooLossAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooProfitAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank odooBankAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [至少一个转出]
     */
    public void setAtLeastOneOutbound(Boolean atLeastOneOutbound){
        this.atLeastOneOutbound = atLeastOneOutbound ;
        this.modify("at_least_one_outbound",atLeastOneOutbound);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [分录序列]
     */
    public void setSequenceId(Integer sequenceId){
        this.sequenceId = sequenceId ;
        this.modify("sequence_id",sequenceId);
    }

    /**
     * 设置 [在仪表板显示日记账]
     */
    public void setShowOnDashboard(Boolean showOnDashboard){
        this.showOnDashboard = showOnDashboard ;
        this.modify("show_on_dashboard",showOnDashboard);
    }

    /**
     * 设置 [银行费用]
     */
    public void setBankStatementsSource(String bankStatementsSource){
        this.bankStatementsSource = bankStatementsSource ;
        this.modify("bank_statements_source",bankStatementsSource);
    }

    /**
     * 设置 [至少一个转入]
     */
    public void setAtLeastOneInbound(Boolean atLeastOneInbound){
        this.atLeastOneInbound = atLeastOneInbound ;
        this.modify("at_least_one_inbound",atLeastOneInbound);
    }

    /**
     * 设置 [日记账名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [简码]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [信用票分录序列]
     */
    public void setRefundSequenceId(Integer refundSequenceId){
        this.refundSequenceId = refundSequenceId ;
        this.modify("refund_sequence_id",refundSequenceId);
    }

    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [专用的信用票序列]
     */
    public void setRefundSequence(Boolean refundSequence){
        this.refundSequence = refundSequence ;
        this.modify("refund_sequence",refundSequence);
    }

    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [银行账户]
     */
    public void setBankAccountId(Long bankAccountId){
        this.bankAccountId = bankAccountId ;
        this.modify("bank_account_id",bankAccountId);
    }

    /**
     * 设置 [损失科目]
     */
    public void setLossAccountId(Long lossAccountId){
        this.lossAccountId = lossAccountId ;
        this.modify("loss_account_id",lossAccountId);
    }

    /**
     * 设置 [别名]
     */
    public void setAliasId(Long aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [默认贷方科目]
     */
    public void setDefaultCreditAccountId(Long defaultCreditAccountId){
        this.defaultCreditAccountId = defaultCreditAccountId ;
        this.modify("default_credit_account_id",defaultCreditAccountId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [默认借方科目]
     */
    public void setDefaultDebitAccountId(Long defaultDebitAccountId){
        this.defaultDebitAccountId = defaultDebitAccountId ;
        this.modify("default_debit_account_id",defaultDebitAccountId);
    }

    /**
     * 设置 [利润科目]
     */
    public void setProfitAccountId(Long profitAccountId){
        this.profitAccountId = profitAccountId ;
        this.modify("profit_account_id",profitAccountId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


