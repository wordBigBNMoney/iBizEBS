package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_refuse_wizardSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_refuse_wizardService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_expense_refuse_wizardMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[费用拒绝原因向导] 服务对象接口实现
 */
@Slf4j
@Service("Hr_expense_refuse_wizardServiceImpl")
public class Hr_expense_refuse_wizardServiceImpl extends EBSServiceImpl<Hr_expense_refuse_wizardMapper, Hr_expense_refuse_wizard> implements IHr_expense_refuse_wizardService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.expense.refuse.wizard" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_expense_refuse_wizard et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_expense_refuse_wizardService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_expense_refuse_wizard> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_expense_refuse_wizard et) {
        Hr_expense_refuse_wizard old = new Hr_expense_refuse_wizard() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_expense_refuse_wizardService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_expense_refuse_wizardService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_expense_refuse_wizard> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_expense_refuse_wizard get(Long key) {
        Hr_expense_refuse_wizard et = getById(key);
        if(et==null){
            et=new Hr_expense_refuse_wizard();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_expense_refuse_wizard getDraft(Hr_expense_refuse_wizard et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_expense_refuse_wizard et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_expense_refuse_wizard et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_expense_refuse_wizard et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_expense_refuse_wizard> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_expense_refuse_wizard> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_expense_refuse_wizard> selectByHrExpenseSheetId(Long id) {
        return baseMapper.selectByHrExpenseSheetId(id);
    }
    @Override
    public void resetByHrExpenseSheetId(Long id) {
        this.update(new UpdateWrapper<Hr_expense_refuse_wizard>().set("hr_expense_sheet_id",null).eq("hr_expense_sheet_id",id));
    }

    @Override
    public void resetByHrExpenseSheetId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense_refuse_wizard>().set("hr_expense_sheet_id",null).in("hr_expense_sheet_id",ids));
    }

    @Override
    public void removeByHrExpenseSheetId(Long id) {
        this.remove(new QueryWrapper<Hr_expense_refuse_wizard>().eq("hr_expense_sheet_id",id));
    }

	@Override
    public List<Hr_expense_refuse_wizard> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_expense_refuse_wizard>().eq("create_uid",id));
    }

	@Override
    public List<Hr_expense_refuse_wizard> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_expense_refuse_wizard>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_expense_refuse_wizard> searchDefault(Hr_expense_refuse_wizardSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_expense_refuse_wizard> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_expense_refuse_wizard>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_expense_refuse_wizard et){
        //实体关系[DER1N_HR_EXPENSE_REFUSE_WIZARD__HR_EXPENSE_SHEET__HR_EXPENSE_SHEET_ID]
        if(!ObjectUtils.isEmpty(et.getHrExpenseSheetId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet odooHrExpenseSheet=et.getOdooHrExpenseSheet();
            if(ObjectUtils.isEmpty(odooHrExpenseSheet)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet majorEntity=hrExpenseSheetService.get(et.getHrExpenseSheetId());
                et.setOdooHrExpenseSheet(majorEntity);
                odooHrExpenseSheet=majorEntity;
            }
            et.setHrExpenseSheetIdText(odooHrExpenseSheet.getName());
        }
        //实体关系[DER1N_HR_EXPENSE_REFUSE_WIZARD__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_EXPENSE_REFUSE_WIZARD__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_expense_refuse_wizard> getHrExpenseRefuseWizardByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_expense_refuse_wizard> getHrExpenseRefuseWizardByEntities(List<Hr_expense_refuse_wizard> entities) {
        List ids =new ArrayList();
        for(Hr_expense_refuse_wizard entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



