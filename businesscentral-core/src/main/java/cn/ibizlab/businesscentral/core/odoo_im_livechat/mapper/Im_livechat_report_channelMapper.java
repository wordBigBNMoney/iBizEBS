package cn.ibizlab.businesscentral.core.odoo_im_livechat.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_channel;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_report_channelSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Im_livechat_report_channelMapper extends BaseMapper<Im_livechat_report_channel>{

    Page<Im_livechat_report_channel> searchDefault(IPage page, @Param("srf") Im_livechat_report_channelSearchContext context, @Param("ew") Wrapper<Im_livechat_report_channel> wrapper) ;
    @Override
    Im_livechat_report_channel selectById(Serializable id);
    @Override
    int insert(Im_livechat_report_channel entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Im_livechat_report_channel entity);
    @Override
    int update(@Param(Constants.ENTITY) Im_livechat_report_channel entity, @Param("ew") Wrapper<Im_livechat_report_channel> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Im_livechat_report_channel> selectByLivechatChannelId(@Param("id") Serializable id) ;

    List<Im_livechat_report_channel> selectByChannelId(@Param("id") Serializable id) ;

    List<Im_livechat_report_channel> selectByPartnerId(@Param("id") Serializable id) ;


}
