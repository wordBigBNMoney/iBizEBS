package cn.ibizlab.businesscentral.core.odoo_bus.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.businesscentral.core.odoo_bus.filter.Bus_presenceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[bus_presence] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-bus:odoo-bus}", contextId = "bus-presence", fallback = bus_presenceFallback.class)
public interface bus_presenceFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/bus_presences/{id}")
    Bus_presence get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/bus_presences")
    Bus_presence create(@RequestBody Bus_presence bus_presence);

    @RequestMapping(method = RequestMethod.POST, value = "/bus_presences/batch")
    Boolean createBatch(@RequestBody List<Bus_presence> bus_presences);





    @RequestMapping(method = RequestMethod.POST, value = "/bus_presences/search")
    Page<Bus_presence> search(@RequestBody Bus_presenceSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/bus_presences/{id}")
    Bus_presence update(@PathVariable("id") Long id,@RequestBody Bus_presence bus_presence);

    @RequestMapping(method = RequestMethod.PUT, value = "/bus_presences/batch")
    Boolean updateBatch(@RequestBody List<Bus_presence> bus_presences);


    @RequestMapping(method = RequestMethod.DELETE, value = "/bus_presences/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/bus_presences/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/bus_presences/select")
    Page<Bus_presence> select();


    @RequestMapping(method = RequestMethod.GET, value = "/bus_presences/getdraft")
    Bus_presence getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/bus_presences/checkkey")
    Boolean checkKey(@RequestBody Bus_presence bus_presence);


    @RequestMapping(method = RequestMethod.POST, value = "/bus_presences/save")
    Boolean save(@RequestBody Bus_presence bus_presence);

    @RequestMapping(method = RequestMethod.POST, value = "/bus_presences/savebatch")
    Boolean saveBatch(@RequestBody List<Bus_presence> bus_presences);



    @RequestMapping(method = RequestMethod.POST, value = "/bus_presences/searchdefault")
    Page<Bus_presence> searchDefault(@RequestBody Bus_presenceSearchContext context);


}
