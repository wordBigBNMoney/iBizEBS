package cn.ibizlab.businesscentral.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_labelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_label] 服务对象接口
 */
@Component
public class survey_labelFallback implements survey_labelFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Survey_label update(Long id, Survey_label survey_label){
            return null;
     }
    public Boolean updateBatch(List<Survey_label> survey_labels){
            return false;
     }


    public Survey_label get(Long id){
            return null;
     }




    public Page<Survey_label> search(Survey_labelSearchContext context){
            return null;
     }


    public Survey_label create(Survey_label survey_label){
            return null;
     }
    public Boolean createBatch(List<Survey_label> survey_labels){
            return false;
     }

    public Page<Survey_label> select(){
            return null;
     }

    public Survey_label getDraft(){
            return null;
    }



    public Boolean checkKey(Survey_label survey_label){
            return false;
     }


    public Boolean save(Survey_label survey_label){
            return false;
     }
    public Boolean saveBatch(List<Survey_label> survey_labels){
            return false;
     }

    public Page<Survey_label> searchDefault(Survey_labelSearchContext context){
            return null;
     }


}
