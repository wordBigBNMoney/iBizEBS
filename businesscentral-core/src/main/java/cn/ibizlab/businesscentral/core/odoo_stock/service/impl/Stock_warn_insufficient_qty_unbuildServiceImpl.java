package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_unbuildService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_warn_insufficient_qty_unbuildMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[拆解数量短缺警告] 服务对象接口实现
 */
@Slf4j
@Service("Stock_warn_insufficient_qty_unbuildServiceImpl")
public class Stock_warn_insufficient_qty_unbuildServiceImpl extends EBSServiceImpl<Stock_warn_insufficient_qty_unbuildMapper, Stock_warn_insufficient_qty_unbuild> implements IStock_warn_insufficient_qty_unbuildService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService mrpUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.warn.insufficient.qty.unbuild" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_warn_insufficient_qty_unbuild et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_warn_insufficient_qty_unbuildService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_warn_insufficient_qty_unbuild> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_warn_insufficient_qty_unbuild et) {
        Stock_warn_insufficient_qty_unbuild old = new Stock_warn_insufficient_qty_unbuild() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_warn_insufficient_qty_unbuildService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_warn_insufficient_qty_unbuildService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_warn_insufficient_qty_unbuild> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_warn_insufficient_qty_unbuild get(Long key) {
        Stock_warn_insufficient_qty_unbuild et = getById(key);
        if(et==null){
            et=new Stock_warn_insufficient_qty_unbuild();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_warn_insufficient_qty_unbuild getDraft(Stock_warn_insufficient_qty_unbuild et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_warn_insufficient_qty_unbuild et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_warn_insufficient_qty_unbuild et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_warn_insufficient_qty_unbuild et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_warn_insufficient_qty_unbuild> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_warn_insufficient_qty_unbuild> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_warn_insufficient_qty_unbuild> selectByUnbuildId(Long id) {
        return baseMapper.selectByUnbuildId(id);
    }
    @Override
    public void resetByUnbuildId(Long id) {
        this.update(new UpdateWrapper<Stock_warn_insufficient_qty_unbuild>().set("unbuild_id",null).eq("unbuild_id",id));
    }

    @Override
    public void resetByUnbuildId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warn_insufficient_qty_unbuild>().set("unbuild_id",null).in("unbuild_id",ids));
    }

    @Override
    public void removeByUnbuildId(Long id) {
        this.remove(new QueryWrapper<Stock_warn_insufficient_qty_unbuild>().eq("unbuild_id",id));
    }

	@Override
    public List<Stock_warn_insufficient_qty_unbuild> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Stock_warn_insufficient_qty_unbuild>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warn_insufficient_qty_unbuild>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Stock_warn_insufficient_qty_unbuild>().eq("product_id",id));
    }

	@Override
    public List<Stock_warn_insufficient_qty_unbuild> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_warn_insufficient_qty_unbuild>().eq("create_uid",id));
    }

	@Override
    public List<Stock_warn_insufficient_qty_unbuild> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_warn_insufficient_qty_unbuild>().eq("write_uid",id));
    }

	@Override
    public List<Stock_warn_insufficient_qty_unbuild> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_warn_insufficient_qty_unbuild>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warn_insufficient_qty_unbuild>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_warn_insufficient_qty_unbuild>().eq("location_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_warn_insufficient_qty_unbuild> searchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_warn_insufficient_qty_unbuild> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_warn_insufficient_qty_unbuild>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_warn_insufficient_qty_unbuild et){
        //实体关系[DER1N_STOCK_WARN_INSUFFICIENT_QTY_UNBUILD__MRP_UNBUILD__UNBUILD_ID]
        if(!ObjectUtils.isEmpty(et.getUnbuildId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild odooUnbuild=et.getOdooUnbuild();
            if(ObjectUtils.isEmpty(odooUnbuild)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild majorEntity=mrpUnbuildService.get(et.getUnbuildId());
                et.setOdooUnbuild(majorEntity);
                odooUnbuild=majorEntity;
            }
            et.setUnbuildIdText(odooUnbuild.getName());
        }
        //实体关系[DER1N_STOCK_WARN_INSUFFICIENT_QTY_UNBUILD__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_STOCK_WARN_INSUFFICIENT_QTY_UNBUILD__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_WARN_INSUFFICIENT_QTY_UNBUILD__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_WARN_INSUFFICIENT_QTY_UNBUILD__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_warn_insufficient_qty_unbuild> getStockWarnInsufficientQtyUnbuildByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_warn_insufficient_qty_unbuild> getStockWarnInsufficientQtyUnbuildByEntities(List<Stock_warn_insufficient_qty_unbuild> entities) {
        List ids =new ArrayList();
        for(Stock_warn_insufficient_qty_unbuild entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



