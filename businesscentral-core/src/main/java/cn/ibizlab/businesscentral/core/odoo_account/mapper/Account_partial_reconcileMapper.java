package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_partial_reconcileSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_partial_reconcileMapper extends BaseMapper<Account_partial_reconcile>{

    Page<Account_partial_reconcile> searchDefault(IPage page, @Param("srf") Account_partial_reconcileSearchContext context, @Param("ew") Wrapper<Account_partial_reconcile> wrapper) ;
    @Override
    Account_partial_reconcile selectById(Serializable id);
    @Override
    int insert(Account_partial_reconcile entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_partial_reconcile entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_partial_reconcile entity, @Param("ew") Wrapper<Account_partial_reconcile> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_partial_reconcile> selectByFullReconcileId(@Param("id") Serializable id) ;

    List<Account_partial_reconcile> selectByCreditMoveId(@Param("id") Serializable id) ;

    List<Account_partial_reconcile> selectByDebitMoveId(@Param("id") Serializable id) ;

    List<Account_partial_reconcile> selectByCompanyId(@Param("id") Serializable id) ;

    List<Account_partial_reconcile> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Account_partial_reconcile> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_partial_reconcile> selectByWriteUid(@Param("id") Serializable id) ;


}
