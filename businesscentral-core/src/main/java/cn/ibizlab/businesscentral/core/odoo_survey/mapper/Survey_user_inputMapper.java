package cn.ibizlab.businesscentral.core.odoo_survey.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_user_inputSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Survey_user_inputMapper extends BaseMapper<Survey_user_input>{

    Page<Survey_user_input> searchDefault(IPage page, @Param("srf") Survey_user_inputSearchContext context, @Param("ew") Wrapper<Survey_user_input> wrapper) ;
    @Override
    Survey_user_input selectById(Serializable id);
    @Override
    int insert(Survey_user_input entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Survey_user_input entity);
    @Override
    int update(@Param(Constants.ENTITY) Survey_user_input entity, @Param("ew") Wrapper<Survey_user_input> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Survey_user_input> selectByPartnerId(@Param("id") Serializable id) ;

    List<Survey_user_input> selectByCreateUid(@Param("id") Serializable id) ;

    List<Survey_user_input> selectByWriteUid(@Param("id") Serializable id) ;

    List<Survey_user_input> selectByLastDisplayedPageId(@Param("id") Serializable id) ;

    List<Survey_user_input> selectBySurveyId(@Param("id") Serializable id) ;


}
