package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_compose_messageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_compose_message] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-compose-message", fallback = mail_compose_messageFallback.class)
public interface mail_compose_messageFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_compose_messages/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_compose_messages/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/search")
    Page<Mail_compose_message> search(@RequestBody Mail_compose_messageSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_compose_messages/{id}")
    Mail_compose_message update(@PathVariable("id") Long id,@RequestBody Mail_compose_message mail_compose_message);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_compose_messages/batch")
    Boolean updateBatch(@RequestBody List<Mail_compose_message> mail_compose_messages);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages")
    Mail_compose_message create(@RequestBody Mail_compose_message mail_compose_message);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/batch")
    Boolean createBatch(@RequestBody List<Mail_compose_message> mail_compose_messages);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_compose_messages/{id}")
    Mail_compose_message get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_compose_messages/select")
    Page<Mail_compose_message> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_compose_messages/getdraft")
    Mail_compose_message getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/checkkey")
    Boolean checkKey(@RequestBody Mail_compose_message mail_compose_message);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/save")
    Boolean save(@RequestBody Mail_compose_message mail_compose_message);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_compose_message> mail_compose_messages);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/searchdefault")
    Page<Mail_compose_message> searchDefault(@RequestBody Mail_compose_messageSearchContext context);


}
