package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_industrySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_partner_industry] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-partner-industry", fallback = res_partner_industryFallback.class)
public interface res_partner_industryFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries")
    Res_partner_industry create(@RequestBody Res_partner_industry res_partner_industry);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/batch")
    Boolean createBatch(@RequestBody List<Res_partner_industry> res_partner_industries);



    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_industries/{id}")
    Res_partner_industry update(@PathVariable("id") Long id,@RequestBody Res_partner_industry res_partner_industry);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_industries/batch")
    Boolean updateBatch(@RequestBody List<Res_partner_industry> res_partner_industries);




    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/search")
    Page<Res_partner_industry> search(@RequestBody Res_partner_industrySearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_industries/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_industries/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_industries/{id}")
    Res_partner_industry get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_industries/select")
    Page<Res_partner_industry> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_industries/getdraft")
    Res_partner_industry getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/checkkey")
    Boolean checkKey(@RequestBody Res_partner_industry res_partner_industry);


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/save")
    Boolean save(@RequestBody Res_partner_industry res_partner_industry);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/savebatch")
    Boolean saveBatch(@RequestBody List<Res_partner_industry> res_partner_industries);



    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/searchdefault")
    Page<Res_partner_industry> searchDefault(@RequestBody Res_partner_industrySearchContext context);


}
