package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_importSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_language_import] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "base-language-import", fallback = base_language_importFallback.class)
public interface base_language_importFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/search")
    Page<Base_language_import> search(@RequestBody Base_language_importSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/base_language_imports")
    Base_language_import create(@RequestBody Base_language_import base_language_import);

    @RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/batch")
    Boolean createBatch(@RequestBody List<Base_language_import> base_language_imports);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_language_imports/{id}")
    Base_language_import update(@PathVariable("id") Long id,@RequestBody Base_language_import base_language_import);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_language_imports/batch")
    Boolean updateBatch(@RequestBody List<Base_language_import> base_language_imports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_language_imports/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_language_imports/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_language_imports/{id}")
    Base_language_import get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.GET, value = "/base_language_imports/select")
    Page<Base_language_import> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_language_imports/getdraft")
    Base_language_import getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/checkkey")
    Boolean checkKey(@RequestBody Base_language_import base_language_import);


    @RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/save")
    Boolean save(@RequestBody Base_language_import base_language_import);

    @RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/savebatch")
    Boolean saveBatch(@RequestBody List<Base_language_import> base_language_imports);



    @RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/searchdefault")
    Page<Base_language_import> searchDefault(@RequestBody Base_language_importSearchContext context);


}
