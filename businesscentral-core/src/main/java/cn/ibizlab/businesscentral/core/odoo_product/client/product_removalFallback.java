package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_removal;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_removalSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_removal] 服务对象接口
 */
@Component
public class product_removalFallback implements product_removalFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Page<Product_removal> search(Product_removalSearchContext context){
            return null;
     }


    public Product_removal create(Product_removal product_removal){
            return null;
     }
    public Boolean createBatch(List<Product_removal> product_removals){
            return false;
     }


    public Product_removal get(Long id){
            return null;
     }


    public Product_removal update(Long id, Product_removal product_removal){
            return null;
     }
    public Boolean updateBatch(List<Product_removal> product_removals){
            return false;
     }


    public Page<Product_removal> select(){
            return null;
     }

    public Product_removal getDraft(){
            return null;
    }



    public Boolean checkKey(Product_removal product_removal){
            return false;
     }


    public Boolean save(Product_removal product_removal){
            return false;
     }
    public Boolean saveBatch(List<Product_removal> product_removals){
            return false;
     }

    public Page<Product_removal> searchDefault(Product_removalSearchContext context){
            return null;
     }


}
