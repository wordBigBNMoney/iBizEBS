package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_analytic_line] 服务对象接口
 */
public interface IAccount_analytic_lineService extends IService<Account_analytic_line>{

    boolean create(Account_analytic_line et) ;
    void createBatch(List<Account_analytic_line> list) ;
    boolean update(Account_analytic_line et) ;
    void updateBatch(List<Account_analytic_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_analytic_line get(Long key) ;
    Account_analytic_line getDraft(Account_analytic_line et) ;
    boolean checkKey(Account_analytic_line et) ;
    boolean save(Account_analytic_line et) ;
    void saveBatch(List<Account_analytic_line> list) ;
    Page<Account_analytic_line> searchDefault(Account_analytic_lineSearchContext context) ;
    List<Account_analytic_line> selectByGeneralAccountId(Long id);
    List<Account_analytic_line> selectByGeneralAccountId(Collection<Long> ids);
    void removeByGeneralAccountId(Long id);
    List<Account_analytic_line> selectByAccountId(Long id);
    List<Account_analytic_line> selectByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_analytic_line> selectByGroupId(Long id);
    void resetByGroupId(Long id);
    void resetByGroupId(Collection<Long> ids);
    void removeByGroupId(Long id);
    List<Account_analytic_line> selectByMoveId(Long id);
    void removeByMoveId(Collection<Long> ids);
    void removeByMoveId(Long id);
    List<Account_analytic_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Account_analytic_line> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_analytic_line> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_analytic_line> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Account_analytic_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_analytic_line> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Account_analytic_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Account_analytic_line> selectBySoLine(Long id);
    void resetBySoLine(Long id);
    void resetBySoLine(Collection<Long> ids);
    void removeBySoLine(Long id);
    List<Account_analytic_line> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_analytic_line> getAccountAnalyticLineByIds(List<Long> ids) ;
    List<Account_analytic_line> getAccountAnalyticLineByEntities(List<Account_analytic_line> entities) ;
}


