package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_tax;
/**
 * 关系型数据实体[Account_invoice_tax] 查询条件对象
 */
@Slf4j
@Data
public class Account_invoice_taxSearchContext extends QueryWrapperContext<Account_invoice_tax> {

	private String n_name_like;//[税说明]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_tax_id_text_eq;//[税率]
	public void setN_tax_id_text_eq(String n_tax_id_text_eq) {
        this.n_tax_id_text_eq = n_tax_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_id_text_eq)){
            this.getSearchCond().eq("tax_id_text", n_tax_id_text_eq);
        }
    }
	private String n_tax_id_text_like;//[税率]
	public void setN_tax_id_text_like(String n_tax_id_text_like) {
        this.n_tax_id_text_like = n_tax_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_tax_id_text_like)){
            this.getSearchCond().like("tax_id_text", n_tax_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_invoice_id_text_eq;//[发票]
	public void setN_invoice_id_text_eq(String n_invoice_id_text_eq) {
        this.n_invoice_id_text_eq = n_invoice_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_text_eq)){
            this.getSearchCond().eq("invoice_id_text", n_invoice_id_text_eq);
        }
    }
	private String n_invoice_id_text_like;//[发票]
	public void setN_invoice_id_text_like(String n_invoice_id_text_like) {
        this.n_invoice_id_text_like = n_invoice_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_text_like)){
            this.getSearchCond().like("invoice_id_text", n_invoice_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_account_analytic_id_text_eq;//[分析账户]
	public void setN_account_analytic_id_text_eq(String n_account_analytic_id_text_eq) {
        this.n_account_analytic_id_text_eq = n_account_analytic_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_text_eq)){
            this.getSearchCond().eq("account_analytic_id_text", n_account_analytic_id_text_eq);
        }
    }
	private String n_account_analytic_id_text_like;//[分析账户]
	public void setN_account_analytic_id_text_like(String n_account_analytic_id_text_like) {
        this.n_account_analytic_id_text_like = n_account_analytic_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_text_like)){
            this.getSearchCond().like("account_analytic_id_text", n_account_analytic_id_text_like);
        }
    }
	private String n_account_id_text_eq;//[税率科目]
	public void setN_account_id_text_eq(String n_account_id_text_eq) {
        this.n_account_id_text_eq = n_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_eq)){
            this.getSearchCond().eq("account_id_text", n_account_id_text_eq);
        }
    }
	private String n_account_id_text_like;//[税率科目]
	public void setN_account_id_text_like(String n_account_id_text_like) {
        this.n_account_id_text_like = n_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_like)){
            this.getSearchCond().like("account_id_text", n_account_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_account_id_eq;//[税率科目]
	public void setN_account_id_eq(Long n_account_id_eq) {
        this.n_account_id_eq = n_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_eq)){
            this.getSearchCond().eq("account_id", n_account_id_eq);
        }
    }
	private Long n_invoice_id_eq;//[发票]
	public void setN_invoice_id_eq(Long n_invoice_id_eq) {
        this.n_invoice_id_eq = n_invoice_id_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_eq)){
            this.getSearchCond().eq("invoice_id", n_invoice_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_account_analytic_id_eq;//[分析账户]
	public void setN_account_analytic_id_eq(Long n_account_analytic_id_eq) {
        this.n_account_analytic_id_eq = n_account_analytic_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_eq)){
            this.getSearchCond().eq("account_analytic_id", n_account_analytic_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_tax_id_eq;//[税率]
	public void setN_tax_id_eq(Long n_tax_id_eq) {
        this.n_tax_id_eq = n_tax_id_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_id_eq)){
            this.getSearchCond().eq("tax_id", n_tax_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



