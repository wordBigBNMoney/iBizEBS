package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_option;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_optionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_order_option] 服务对象接口
 */
public interface ISale_order_optionService extends IService<Sale_order_option>{

    boolean create(Sale_order_option et) ;
    void createBatch(List<Sale_order_option> list) ;
    boolean update(Sale_order_option et) ;
    void updateBatch(List<Sale_order_option> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_order_option get(Long key) ;
    Sale_order_option getDraft(Sale_order_option et) ;
    boolean checkKey(Sale_order_option et) ;
    boolean save(Sale_order_option et) ;
    void saveBatch(List<Sale_order_option> list) ;
    Page<Sale_order_option> searchDefault(Sale_order_optionSearchContext context) ;
    List<Sale_order_option> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Sale_order_option> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sale_order_option> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Sale_order_option> selectByLineId(Long id);
    void resetByLineId(Long id);
    void resetByLineId(Collection<Long> ids);
    void removeByLineId(Long id);
    List<Sale_order_option> selectByOrderId(Long id);
    void removeByOrderId(Collection<Long> ids);
    void removeByOrderId(Long id);
    List<Sale_order_option> selectByUomId(Long id);
    void resetByUomId(Long id);
    void resetByUomId(Collection<Long> ids);
    void removeByUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sale_order_option> getSaleOrderOptionByIds(List<Long> ids) ;
    List<Sale_order_option> getSaleOrderOptionByEntities(List<Sale_order_option> entities) ;
}


