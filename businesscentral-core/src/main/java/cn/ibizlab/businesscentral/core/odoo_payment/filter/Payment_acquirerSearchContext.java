package cn.ibizlab.businesscentral.core.odoo_payment.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer;
/**
 * 关系型数据实体[Payment_acquirer] 查询条件对象
 */
@Slf4j
@Data
public class Payment_acquirerSearchContext extends QueryWrapperContext<Payment_acquirer> {

	private String n_environment_eq;//[环境]
	public void setN_environment_eq(String n_environment_eq) {
        this.n_environment_eq = n_environment_eq;
        if(!ObjectUtils.isEmpty(this.n_environment_eq)){
            this.getSearchCond().eq("environment", n_environment_eq);
        }
    }
	private String n_payment_flow_eq;//[立即支付]
	public void setN_payment_flow_eq(String n_payment_flow_eq) {
        this.n_payment_flow_eq = n_payment_flow_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_flow_eq)){
            this.getSearchCond().eq("payment_flow", n_payment_flow_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_so_reference_type_eq;//[交流]
	public void setN_so_reference_type_eq(String n_so_reference_type_eq) {
        this.n_so_reference_type_eq = n_so_reference_type_eq;
        if(!ObjectUtils.isEmpty(this.n_so_reference_type_eq)){
            this.getSearchCond().eq("so_reference_type", n_so_reference_type_eq);
        }
    }
	private String n_module_state_eq;//[安装状态]
	public void setN_module_state_eq(String n_module_state_eq) {
        this.n_module_state_eq = n_module_state_eq;
        if(!ObjectUtils.isEmpty(this.n_module_state_eq)){
            this.getSearchCond().eq("module_state", n_module_state_eq);
        }
    }
	private String n_provider_eq;//[服务商]
	public void setN_provider_eq(String n_provider_eq) {
        this.n_provider_eq = n_provider_eq;
        if(!ObjectUtils.isEmpty(this.n_provider_eq)){
            this.getSearchCond().eq("provider", n_provider_eq);
        }
    }
	private String n_save_token_eq;//[保存卡]
	public void setN_save_token_eq(String n_save_token_eq) {
        this.n_save_token_eq = n_save_token_eq;
        if(!ObjectUtils.isEmpty(this.n_save_token_eq)){
            this.getSearchCond().eq("save_token", n_save_token_eq);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_journal_id_text_eq;//[付款日记账]
	public void setN_journal_id_text_eq(String n_journal_id_text_eq) {
        this.n_journal_id_text_eq = n_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_eq)){
            this.getSearchCond().eq("journal_id_text", n_journal_id_text_eq);
        }
    }
	private String n_journal_id_text_like;//[付款日记账]
	public void setN_journal_id_text_like(String n_journal_id_text_like) {
        this.n_journal_id_text_like = n_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_like)){
            this.getSearchCond().like("journal_id_text", n_journal_id_text_like);
        }
    }
	private Long n_journal_id_eq;//[付款日记账]
	public void setN_journal_id_eq(Long n_journal_id_eq) {
        this.n_journal_id_eq = n_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_eq)){
            this.getSearchCond().eq("journal_id", n_journal_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



