package cn.ibizlab.businesscentral.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_eventSearchContext;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_eventService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_event.mapper.Event_eventMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[活动] 服务对象接口实现
 */
@Slf4j
@Service("Event_eventServiceImpl")
public class Event_eventServiceImpl extends EBSServiceImpl<Event_eventMapper, Event_event> implements IEvent_eventService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_event_ticketService eventEventTicketService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_mailService eventMailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService eventRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_typeService eventTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_menuService websiteMenuService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "event.event" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Event_event et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_eventService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Event_event> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Event_event et) {
        Event_event old = new Event_event() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_eventService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_eventService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Event_event> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        eventEventTicketService.removeByEventId(key);
        eventMailService.removeByEventId(key);
        eventRegistrationService.resetByEventId(key);
        saleOrderLineService.resetByEventId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        eventEventTicketService.removeByEventId(idList);
        eventMailService.removeByEventId(idList);
        eventRegistrationService.resetByEventId(idList);
        saleOrderLineService.resetByEventId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Event_event get(Long key) {
        Event_event et = getById(key);
        if(et==null){
            et=new Event_event();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Event_event getDraft(Event_event et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Event_event et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Event_event et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Event_event et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Event_event> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Event_event> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Event_event> selectByEventTypeId(Long id) {
        return baseMapper.selectByEventTypeId(id);
    }
    @Override
    public void resetByEventTypeId(Long id) {
        this.update(new UpdateWrapper<Event_event>().set("event_type_id",null).eq("event_type_id",id));
    }

    @Override
    public void resetByEventTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_event>().set("event_type_id",null).in("event_type_id",ids));
    }

    @Override
    public void removeByEventTypeId(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("event_type_id",id));
    }

	@Override
    public List<Event_event> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Event_event>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_event>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("company_id",id));
    }

	@Override
    public List<Event_event> selectByCountryId(Long id) {
        return baseMapper.selectByCountryId(id);
    }
    @Override
    public void resetByCountryId(Long id) {
        this.update(new UpdateWrapper<Event_event>().set("country_id",null).eq("country_id",id));
    }

    @Override
    public void resetByCountryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_event>().set("country_id",null).in("country_id",ids));
    }

    @Override
    public void removeByCountryId(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("country_id",id));
    }

	@Override
    public List<Event_event> selectByAddressId(Long id) {
        return baseMapper.selectByAddressId(id);
    }
    @Override
    public void resetByAddressId(Long id) {
        this.update(new UpdateWrapper<Event_event>().set("address_id",null).eq("address_id",id));
    }

    @Override
    public void resetByAddressId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_event>().set("address_id",null).in("address_id",ids));
    }

    @Override
    public void removeByAddressId(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("address_id",id));
    }

	@Override
    public List<Event_event> selectByOrganizerId(Long id) {
        return baseMapper.selectByOrganizerId(id);
    }
    @Override
    public void resetByOrganizerId(Long id) {
        this.update(new UpdateWrapper<Event_event>().set("organizer_id",null).eq("organizer_id",id));
    }

    @Override
    public void resetByOrganizerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_event>().set("organizer_id",null).in("organizer_id",ids));
    }

    @Override
    public void removeByOrganizerId(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("organizer_id",id));
    }

	@Override
    public List<Event_event> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("create_uid",id));
    }

	@Override
    public List<Event_event> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Event_event>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_event>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("user_id",id));
    }

	@Override
    public List<Event_event> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("write_uid",id));
    }

	@Override
    public List<Event_event> selectByMenuId(Long id) {
        return baseMapper.selectByMenuId(id);
    }
    @Override
    public void resetByMenuId(Long id) {
        this.update(new UpdateWrapper<Event_event>().set("menu_id",null).eq("menu_id",id));
    }

    @Override
    public void resetByMenuId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_event>().set("menu_id",null).in("menu_id",ids));
    }

    @Override
    public void removeByMenuId(Long id) {
        this.remove(new QueryWrapper<Event_event>().eq("menu_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Event_event> searchDefault(Event_eventSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Event_event> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Event_event>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Event_event et){
        //实体关系[DER1N_EVENT_EVENT__EVENT_TYPE__EVENT_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getEventTypeId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type odooEventType=et.getOdooEventType();
            if(ObjectUtils.isEmpty(odooEventType)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type majorEntity=eventTypeService.get(et.getEventTypeId());
                et.setOdooEventType(majorEntity);
                odooEventType=majorEntity;
            }
            et.setEventTypeIdText(odooEventType.getName());
        }
        //实体关系[DER1N_EVENT_EVENT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_EVENT_EVENT__RES_COUNTRY__COUNTRY_ID]
        if(!ObjectUtils.isEmpty(et.getCountryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry=et.getOdooCountry();
            if(ObjectUtils.isEmpty(odooCountry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getCountryId());
                et.setOdooCountry(majorEntity);
                odooCountry=majorEntity;
            }
            et.setCountryIdText(odooCountry.getName());
        }
        //实体关系[DER1N_EVENT_EVENT__RES_PARTNER__ADDRESS_ID]
        if(!ObjectUtils.isEmpty(et.getAddressId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAddress=et.getOdooAddress();
            if(ObjectUtils.isEmpty(odooAddress)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getAddressId());
                et.setOdooAddress(majorEntity);
                odooAddress=majorEntity;
            }
            et.setAddressIdText(odooAddress.getName());
        }
        //实体关系[DER1N_EVENT_EVENT__RES_PARTNER__ORGANIZER_ID]
        if(!ObjectUtils.isEmpty(et.getOrganizerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOrganizer=et.getOdooOrganizer();
            if(ObjectUtils.isEmpty(odooOrganizer)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getOrganizerId());
                et.setOdooOrganizer(majorEntity);
                odooOrganizer=majorEntity;
            }
            et.setOrganizerIdText(odooOrganizer.getName());
        }
        //实体关系[DER1N_EVENT_EVENT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_EVENT_EVENT__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_EVENT_EVENT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_EVENT_EVENT__WEBSITE_MENU__MENU_ID]
        if(!ObjectUtils.isEmpty(et.getMenuId())){
            cn.ibizlab.businesscentral.core.odoo_website.domain.Website_menu odooMenu=et.getOdooMenu();
            if(ObjectUtils.isEmpty(odooMenu)){
                cn.ibizlab.businesscentral.core.odoo_website.domain.Website_menu majorEntity=websiteMenuService.get(et.getMenuId());
                et.setOdooMenu(majorEntity);
                odooMenu=majorEntity;
            }
            et.setMenuIdText(odooMenu.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Event_event> getEventEventByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Event_event> getEventEventByEntities(List<Event_event> entities) {
        List ids =new ArrayList();
        for(Event_event entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



