package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_paymentSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_payment] 服务对象接口
 */
public interface IAccount_paymentService extends IService<Account_payment>{

    boolean create(Account_payment et) ;
    void createBatch(List<Account_payment> list) ;
    boolean update(Account_payment et) ;
    void updateBatch(List<Account_payment> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_payment get(Long key) ;
    Account_payment getDraft(Account_payment et) ;
    boolean checkKey(Account_payment et) ;
    boolean save(Account_payment et) ;
    void saveBatch(List<Account_payment> list) ;
    Page<Account_payment> searchDefault(Account_paymentSearchContext context) ;
    List<Account_payment> selectByWriteoffAccountId(Long id);
    void resetByWriteoffAccountId(Long id);
    void resetByWriteoffAccountId(Collection<Long> ids);
    void removeByWriteoffAccountId(Long id);
    List<Account_payment> selectByDestinationJournalId(Long id);
    void resetByDestinationJournalId(Long id);
    void resetByDestinationJournalId(Collection<Long> ids);
    void removeByDestinationJournalId(Long id);
    List<Account_payment> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_payment> selectByPaymentMethodId(Long id);
    void resetByPaymentMethodId(Long id);
    void resetByPaymentMethodId(Collection<Long> ids);
    void removeByPaymentMethodId(Long id);
    List<Account_payment> selectByPaymentTokenId(Long id);
    void resetByPaymentTokenId(Long id);
    void resetByPaymentTokenId(Collection<Long> ids);
    void removeByPaymentTokenId(Long id);
    List<Account_payment> selectByPaymentTransactionId(Long id);
    void resetByPaymentTransactionId(Long id);
    void resetByPaymentTransactionId(Collection<Long> ids);
    void removeByPaymentTransactionId(Long id);
    List<Account_payment> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_payment> selectByPartnerBankAccountId(Long id);
    void resetByPartnerBankAccountId(Long id);
    void resetByPartnerBankAccountId(Collection<Long> ids);
    void removeByPartnerBankAccountId(Long id);
    List<Account_payment> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Account_payment> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_payment> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_payment> getAccountPaymentByIds(List<Long> ids) ;
    List<Account_payment> getAccountPaymentByEntities(List<Account_payment> entities) ;
}


