package cn.ibizlab.businesscentral.core.odoo_digest.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.businesscentral.core.odoo_digest.filter.Digest_digestSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Digest_digest] 服务对象接口
 */
public interface IDigest_digestService extends IService<Digest_digest>{

    boolean create(Digest_digest et) ;
    void createBatch(List<Digest_digest> list) ;
    boolean update(Digest_digest et) ;
    void updateBatch(List<Digest_digest> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Digest_digest get(Long key) ;
    Digest_digest getDraft(Digest_digest et) ;
    boolean checkKey(Digest_digest et) ;
    boolean save(Digest_digest et) ;
    void saveBatch(List<Digest_digest> list) ;
    Page<Digest_digest> searchDefault(Digest_digestSearchContext context) ;
    List<Digest_digest> selectByTemplateId(Long id);
    void resetByTemplateId(Long id);
    void resetByTemplateId(Collection<Long> ids);
    void removeByTemplateId(Long id);
    List<Digest_digest> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Digest_digest> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Digest_digest> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Digest_digest> getDigestDigestByIds(List<Long> ids) ;
    List<Digest_digest> getDigestDigestByEntities(List<Digest_digest> entities) ;
}


