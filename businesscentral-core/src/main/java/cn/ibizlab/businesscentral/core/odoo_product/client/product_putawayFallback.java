package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_putawaySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_putaway] 服务对象接口
 */
@Component
public class product_putawayFallback implements product_putawayFeignClient{


    public Product_putaway create(Product_putaway product_putaway){
            return null;
     }
    public Boolean createBatch(List<Product_putaway> product_putaways){
            return false;
     }

    public Product_putaway update(Long id, Product_putaway product_putaway){
            return null;
     }
    public Boolean updateBatch(List<Product_putaway> product_putaways){
            return false;
     }


    public Page<Product_putaway> search(Product_putawaySearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Product_putaway get(Long id){
            return null;
     }


    public Page<Product_putaway> select(){
            return null;
     }

    public Product_putaway getDraft(){
            return null;
    }



    public Boolean checkKey(Product_putaway product_putaway){
            return false;
     }


    public Boolean save(Product_putaway product_putaway){
            return false;
     }
    public Boolean saveBatch(List<Product_putaway> product_putaways){
            return false;
     }

    public Page<Product_putaway> searchDefault(Product_putawaySearchContext context){
            return null;
     }


}
