package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_titleSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_partner_title] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-partner-title", fallback = res_partner_titleFallback.class)
public interface res_partner_titleFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_titles/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_titles/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/search")
    Page<Res_partner_title> search(@RequestBody Res_partner_titleSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_titles/{id}")
    Res_partner_title update(@PathVariable("id") Long id,@RequestBody Res_partner_title res_partner_title);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_titles/batch")
    Boolean updateBatch(@RequestBody List<Res_partner_title> res_partner_titles);


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles")
    Res_partner_title create(@RequestBody Res_partner_title res_partner_title);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/batch")
    Boolean createBatch(@RequestBody List<Res_partner_title> res_partner_titles);


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_titles/{id}")
    Res_partner_title get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_titles/select")
    Page<Res_partner_title> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_titles/getdraft")
    Res_partner_title getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/checkkey")
    Boolean checkKey(@RequestBody Res_partner_title res_partner_title);


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/save")
    Boolean save(@RequestBody Res_partner_title res_partner_title);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/savebatch")
    Boolean saveBatch(@RequestBody List<Res_partner_title> res_partner_titles);



    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/searchdefault")
    Page<Res_partner_title> searchDefault(@RequestBody Res_partner_titleSearchContext context);


}
