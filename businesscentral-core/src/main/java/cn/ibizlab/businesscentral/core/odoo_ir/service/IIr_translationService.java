package cn.ibizlab.businesscentral.core.odoo_ir.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_translation;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_translationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Ir_translation] 服务对象接口
 */
public interface IIr_translationService extends IService<Ir_translation>{

    boolean create(Ir_translation et) ;
    void createBatch(List<Ir_translation> list) ;
    boolean update(Ir_translation et) ;
    void updateBatch(List<Ir_translation> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Ir_translation get(Long key) ;
    Ir_translation getDraft(Ir_translation et) ;
    boolean checkKey(Ir_translation et) ;
    boolean save(Ir_translation et) ;
    void saveBatch(List<Ir_translation> list) ;
    Page<Ir_translation> searchDefault(Ir_translationSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


