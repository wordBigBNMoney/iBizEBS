package cn.ibizlab.businesscentral.core.odoo_website.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[网站菜单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "WEBSITE_MENU",resultMap = "Website_menuResultMap")
public class Website_menu extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主题模板
     */
    @DEField(name = "theme_template_id")
    @TableField(value = "theme_template_id")
    @JSONField(name = "theme_template_id")
    @JsonProperty("theme_template_id")
    private Integer themeTemplateId;
    /**
     * 可见
     */
    @TableField(exist = false)
    @JSONField(name = "is_visible")
    @JsonProperty("is_visible")
    private Boolean isVisible;
    /**
     * Url
     */
    @TableField(value = "url")
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;
    /**
     * 新窗口
     */
    @DEField(name = "new_window")
    @TableField(value = "new_window")
    @JSONField(name = "new_window")
    @JsonProperty("new_window")
    private Boolean newWindow;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 上级路径
     */
    @DEField(name = "parent_path")
    @TableField(value = "parent_path")
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    private String parentPath;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 下级菜单
     */
    @TableField(exist = false)
    @JSONField(name = "child_id")
    @JsonProperty("child_id")
    private String childId;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 菜单
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 相关页面
     */
    @TableField(exist = false)
    @JSONField(name = "page_id_text")
    @JsonProperty("page_id_text")
    private String pageIdText;
    /**
     * 上级菜单
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 相关页面
     */
    @DEField(name = "page_id")
    @TableField(value = "page_id")
    @JSONField(name = "page_id")
    @JsonProperty("page_id")
    private Long pageId;
    /**
     * 上级菜单
     */
    @DEField(name = "parent_id")
    @TableField(value = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_website.domain.Website_menu odooParent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_website.domain.Website_page odooPage;



    /**
     * 设置 [主题模板]
     */
    public void setThemeTemplateId(Integer themeTemplateId){
        this.themeTemplateId = themeTemplateId ;
        this.modify("theme_template_id",themeTemplateId);
    }

    /**
     * 设置 [Url]
     */
    public void setUrl(String url){
        this.url = url ;
        this.modify("url",url);
    }

    /**
     * 设置 [新窗口]
     */
    public void setNewWindow(Boolean newWindow){
        this.newWindow = newWindow ;
        this.modify("new_window",newWindow);
    }

    /**
     * 设置 [上级路径]
     */
    public void setParentPath(String parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [菜单]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [相关页面]
     */
    public void setPageId(Long pageId){
        this.pageId = pageId ;
        this.modify("page_id",pageId);
    }

    /**
     * 设置 [上级菜单]
     */
    public void setParentId(Long parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


