package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_print_journal;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_print_journalSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_print_journal] 服务对象接口
 */
public interface IAccount_print_journalService extends IService<Account_print_journal>{

    boolean create(Account_print_journal et) ;
    void createBatch(List<Account_print_journal> list) ;
    boolean update(Account_print_journal et) ;
    void updateBatch(List<Account_print_journal> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_print_journal get(Long key) ;
    Account_print_journal getDraft(Account_print_journal et) ;
    boolean checkKey(Account_print_journal et) ;
    boolean save(Account_print_journal et) ;
    void saveBatch(List<Account_print_journal> list) ;
    Page<Account_print_journal> searchDefault(Account_print_journalSearchContext context) ;
    List<Account_print_journal> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_print_journal> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_print_journal> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_print_journal> getAccountPrintJournalByIds(List<Long> ids) ;
    List<Account_print_journal> getAccountPrintJournalByEntities(List<Account_print_journal> entities) ;
}


