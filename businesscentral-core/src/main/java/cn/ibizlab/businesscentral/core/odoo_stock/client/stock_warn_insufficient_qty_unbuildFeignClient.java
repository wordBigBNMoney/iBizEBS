package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_warn_insufficient_qty_unbuild] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-warn-insufficient-qty-unbuild", fallback = stock_warn_insufficient_qty_unbuildFallback.class)
public interface stock_warn_insufficient_qty_unbuildFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_unbuilds/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_unbuilds/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_unbuilds/{id}")
    Stock_warn_insufficient_qty_unbuild get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/search")
    Page<Stock_warn_insufficient_qty_unbuild> search(@RequestBody Stock_warn_insufficient_qty_unbuildSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds")
    Stock_warn_insufficient_qty_unbuild create(@RequestBody Stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/batch")
    Boolean createBatch(@RequestBody List<Stock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds);



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_unbuilds/{id}")
    Stock_warn_insufficient_qty_unbuild update(@PathVariable("id") Long id,@RequestBody Stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_unbuilds/batch")
    Boolean updateBatch(@RequestBody List<Stock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_unbuilds/select")
    Page<Stock_warn_insufficient_qty_unbuild> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_unbuilds/getdraft")
    Stock_warn_insufficient_qty_unbuild getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/checkkey")
    Boolean checkKey(@RequestBody Stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/save")
    Boolean save(@RequestBody Stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/searchdefault")
    Page<Stock_warn_insufficient_qty_unbuild> searchDefault(@RequestBody Stock_warn_insufficient_qty_unbuildSearchContext context);


}
