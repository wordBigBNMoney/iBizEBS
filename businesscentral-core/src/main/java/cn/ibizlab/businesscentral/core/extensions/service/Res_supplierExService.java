package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.odoo_base.service.impl.Res_supplierServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[供应商] 自定义服务对象
 */
@Slf4j
@Primary
@Service("Res_supplierExService")
public class Res_supplierExService extends Res_supplierServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[MasterTabCount]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Res_supplier masterTabCount(Res_supplier et) {
        return super.masterTabCount(et);
    }
}

