package cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_orderALogic;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;

/**
 * 关系型数据实体[A] 对象
 */
@Slf4j
@Service
public class Purchase_orderALogicImpl implements IPurchase_orderALogic{

    @Autowired
    private KieContainer kieContainer;


    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Purchase_order et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("purchase_orderadefault",et);
           kieSession.setGlobal("iBzSysPurchase_orderDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.purchase_ordera");

        }catch(Exception e){
            throw new RuntimeException("执行[A]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
