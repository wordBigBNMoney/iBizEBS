package cn.ibizlab.businesscentral.core.odoo_fleet.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle;
/**
 * 关系型数据实体[Fleet_vehicle] 查询条件对象
 */
@Slf4j
@Data
public class Fleet_vehicleSearchContext extends QueryWrapperContext<Fleet_vehicle> {

	private String n_fuel_type_eq;//[燃油类型]
	public void setN_fuel_type_eq(String n_fuel_type_eq) {
        this.n_fuel_type_eq = n_fuel_type_eq;
        if(!ObjectUtils.isEmpty(this.n_fuel_type_eq)){
            this.getSearchCond().eq("fuel_type", n_fuel_type_eq);
        }
    }
	private String n_odometer_unit_eq;//[里程表单位]
	public void setN_odometer_unit_eq(String n_odometer_unit_eq) {
        this.n_odometer_unit_eq = n_odometer_unit_eq;
        if(!ObjectUtils.isEmpty(this.n_odometer_unit_eq)){
            this.getSearchCond().eq("odometer_unit", n_odometer_unit_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_transmission_eq;//[变速器]
	public void setN_transmission_eq(String n_transmission_eq) {
        this.n_transmission_eq = n_transmission_eq;
        if(!ObjectUtils.isEmpty(this.n_transmission_eq)){
            this.getSearchCond().eq("transmission", n_transmission_eq);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_driver_id_text_eq;//[驾驶员]
	public void setN_driver_id_text_eq(String n_driver_id_text_eq) {
        this.n_driver_id_text_eq = n_driver_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_driver_id_text_eq)){
            this.getSearchCond().eq("driver_id_text", n_driver_id_text_eq);
        }
    }
	private String n_driver_id_text_like;//[驾驶员]
	public void setN_driver_id_text_like(String n_driver_id_text_like) {
        this.n_driver_id_text_like = n_driver_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_driver_id_text_like)){
            this.getSearchCond().like("driver_id_text", n_driver_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_model_id_text_eq;//[型号]
	public void setN_model_id_text_eq(String n_model_id_text_eq) {
        this.n_model_id_text_eq = n_model_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_model_id_text_eq)){
            this.getSearchCond().eq("model_id_text", n_model_id_text_eq);
        }
    }
	private String n_model_id_text_like;//[型号]
	public void setN_model_id_text_like(String n_model_id_text_like) {
        this.n_model_id_text_like = n_model_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_model_id_text_like)){
            this.getSearchCond().like("model_id_text", n_model_id_text_like);
        }
    }
	private String n_brand_id_text_eq;//[品牌]
	public void setN_brand_id_text_eq(String n_brand_id_text_eq) {
        this.n_brand_id_text_eq = n_brand_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_brand_id_text_eq)){
            this.getSearchCond().eq("brand_id_text", n_brand_id_text_eq);
        }
    }
	private String n_brand_id_text_like;//[品牌]
	public void setN_brand_id_text_like(String n_brand_id_text_like) {
        this.n_brand_id_text_like = n_brand_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_brand_id_text_like)){
            this.getSearchCond().like("brand_id_text", n_brand_id_text_like);
        }
    }
	private String n_state_id_text_eq;//[状态]
	public void setN_state_id_text_eq(String n_state_id_text_eq) {
        this.n_state_id_text_eq = n_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_state_id_text_eq)){
            this.getSearchCond().eq("state_id_text", n_state_id_text_eq);
        }
    }
	private String n_state_id_text_like;//[状态]
	public void setN_state_id_text_like(String n_state_id_text_like) {
        this.n_state_id_text_like = n_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_state_id_text_like)){
            this.getSearchCond().like("state_id_text", n_state_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_model_id_eq;//[型号]
	public void setN_model_id_eq(Long n_model_id_eq) {
        this.n_model_id_eq = n_model_id_eq;
        if(!ObjectUtils.isEmpty(this.n_model_id_eq)){
            this.getSearchCond().eq("model_id", n_model_id_eq);
        }
    }
	private Long n_brand_id_eq;//[品牌]
	public void setN_brand_id_eq(Long n_brand_id_eq) {
        this.n_brand_id_eq = n_brand_id_eq;
        if(!ObjectUtils.isEmpty(this.n_brand_id_eq)){
            this.getSearchCond().eq("brand_id", n_brand_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_state_id_eq;//[状态]
	public void setN_state_id_eq(Long n_state_id_eq) {
        this.n_state_id_eq = n_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_state_id_eq)){
            this.getSearchCond().eq("state_id", n_state_id_eq);
        }
    }
	private Long n_driver_id_eq;//[驾驶员]
	public void setN_driver_id_eq(Long n_driver_id_eq) {
        this.n_driver_id_eq = n_driver_id_eq;
        if(!ObjectUtils.isEmpty(this.n_driver_id_eq)){
            this.getSearchCond().eq("driver_id", n_driver_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



