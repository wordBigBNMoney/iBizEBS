package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_bankSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_partner_bank] 服务对象接口
 */
@Component
public class res_partner_bankFallback implements res_partner_bankFeignClient{

    public Page<Res_partner_bank> search(Res_partner_bankSearchContext context){
            return null;
     }


    public Res_partner_bank update(Long id, Res_partner_bank res_partner_bank){
            return null;
     }
    public Boolean updateBatch(List<Res_partner_bank> res_partner_banks){
            return false;
     }



    public Res_partner_bank get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Res_partner_bank create(Res_partner_bank res_partner_bank){
            return null;
     }
    public Boolean createBatch(List<Res_partner_bank> res_partner_banks){
            return false;
     }

    public Page<Res_partner_bank> select(){
            return null;
     }

    public Res_partner_bank getDraft(){
            return null;
    }



    public Boolean checkKey(Res_partner_bank res_partner_bank){
            return false;
     }


    public Boolean save(Res_partner_bank res_partner_bank){
            return false;
     }
    public Boolean saveBatch(List<Res_partner_bank> res_partner_banks){
            return false;
     }

    public Page<Res_partner_bank> searchDefault(Res_partner_bankSearchContext context){
            return null;
     }


}
