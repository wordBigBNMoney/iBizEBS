package cn.ibizlab.businesscentral.core.odoo_resource.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Resource_calendar_leavesMapper extends BaseMapper<Resource_calendar_leaves>{

    Page<Resource_calendar_leaves> searchDefault(IPage page, @Param("srf") Resource_calendar_leavesSearchContext context, @Param("ew") Wrapper<Resource_calendar_leaves> wrapper) ;
    @Override
    Resource_calendar_leaves selectById(Serializable id);
    @Override
    int insert(Resource_calendar_leaves entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Resource_calendar_leaves entity);
    @Override
    int update(@Param(Constants.ENTITY) Resource_calendar_leaves entity, @Param("ew") Wrapper<Resource_calendar_leaves> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Resource_calendar_leaves> selectByHolidayId(@Param("id") Serializable id) ;

    List<Resource_calendar_leaves> selectByCalendarId(@Param("id") Serializable id) ;

    List<Resource_calendar_leaves> selectByResourceId(@Param("id") Serializable id) ;

    List<Resource_calendar_leaves> selectByCompanyId(@Param("id") Serializable id) ;

    List<Resource_calendar_leaves> selectByCreateUid(@Param("id") Serializable id) ;

    List<Resource_calendar_leaves> selectByWriteUid(@Param("id") Serializable id) ;


}
