package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_confirmSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice_confirm] 服务对象接口
 */
@Component
public class account_invoice_confirmFallback implements account_invoice_confirmFeignClient{

    public Page<Account_invoice_confirm> search(Account_invoice_confirmSearchContext context){
            return null;
     }


    public Account_invoice_confirm create(Account_invoice_confirm account_invoice_confirm){
            return null;
     }
    public Boolean createBatch(List<Account_invoice_confirm> account_invoice_confirms){
            return false;
     }



    public Account_invoice_confirm update(Long id, Account_invoice_confirm account_invoice_confirm){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice_confirm> account_invoice_confirms){
            return false;
     }



    public Account_invoice_confirm get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_invoice_confirm> select(){
            return null;
     }

    public Account_invoice_confirm getDraft(){
            return null;
    }



    public Boolean checkKey(Account_invoice_confirm account_invoice_confirm){
            return false;
     }


    public Boolean save(Account_invoice_confirm account_invoice_confirm){
            return false;
     }
    public Boolean saveBatch(List<Account_invoice_confirm> account_invoice_confirms){
            return false;
     }

    public Page<Account_invoice_confirm> searchDefault(Account_invoice_confirmSearchContext context){
            return null;
     }


}
