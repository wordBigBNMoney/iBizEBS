package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_statistics_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_statistics_report] 服务对象接口
 */
@Component
public class mail_statistics_reportFallback implements mail_statistics_reportFeignClient{

    public Mail_statistics_report get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Mail_statistics_report> search(Mail_statistics_reportSearchContext context){
            return null;
     }




    public Mail_statistics_report create(Mail_statistics_report mail_statistics_report){
            return null;
     }
    public Boolean createBatch(List<Mail_statistics_report> mail_statistics_reports){
            return false;
     }


    public Mail_statistics_report update(Long id, Mail_statistics_report mail_statistics_report){
            return null;
     }
    public Boolean updateBatch(List<Mail_statistics_report> mail_statistics_reports){
            return false;
     }


    public Page<Mail_statistics_report> select(){
            return null;
     }

    public Mail_statistics_report getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_statistics_report mail_statistics_report){
            return false;
     }


    public Boolean save(Mail_statistics_report mail_statistics_report){
            return false;
     }
    public Boolean saveBatch(List<Mail_statistics_report> mail_statistics_reports){
            return false;
     }

    public Page<Mail_statistics_report> searchDefault(Mail_statistics_reportSearchContext context){
            return null;
     }


}
