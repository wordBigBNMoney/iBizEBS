package cn.ibizlab.businesscentral.core.odoo_hr.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave;
/**
 * 关系型数据实体[Hr_leave] 查询条件对象
 */
@Slf4j
@Data
public class Hr_leaveSearchContext extends QueryWrapperContext<Hr_leave> {

	private String n_holiday_type_eq;//[分配模式]
	public void setN_holiday_type_eq(String n_holiday_type_eq) {
        this.n_holiday_type_eq = n_holiday_type_eq;
        if(!ObjectUtils.isEmpty(this.n_holiday_type_eq)){
            this.getSearchCond().eq("holiday_type", n_holiday_type_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_request_hour_from_eq;//[时间从]
	public void setN_request_hour_from_eq(String n_request_hour_from_eq) {
        this.n_request_hour_from_eq = n_request_hour_from_eq;
        if(!ObjectUtils.isEmpty(this.n_request_hour_from_eq)){
            this.getSearchCond().eq("request_hour_from", n_request_hour_from_eq);
        }
    }
	private String n_request_hour_to_eq;//[时间到]
	public void setN_request_hour_to_eq(String n_request_hour_to_eq) {
        this.n_request_hour_to_eq = n_request_hour_to_eq;
        if(!ObjectUtils.isEmpty(this.n_request_hour_to_eq)){
            this.getSearchCond().eq("request_hour_to", n_request_hour_to_eq);
        }
    }
	private String n_request_date_from_period_eq;//[日期开始]
	public void setN_request_date_from_period_eq(String n_request_date_from_period_eq) {
        this.n_request_date_from_period_eq = n_request_date_from_period_eq;
        if(!ObjectUtils.isEmpty(this.n_request_date_from_period_eq)){
            this.getSearchCond().eq("request_date_from_period", n_request_date_from_period_eq);
        }
    }
	private String n_name_like;//[说明]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_mode_company_id_text_eq;//[公司]
	public void setN_mode_company_id_text_eq(String n_mode_company_id_text_eq) {
        this.n_mode_company_id_text_eq = n_mode_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mode_company_id_text_eq)){
            this.getSearchCond().eq("mode_company_id_text", n_mode_company_id_text_eq);
        }
    }
	private String n_mode_company_id_text_like;//[公司]
	public void setN_mode_company_id_text_like(String n_mode_company_id_text_like) {
        this.n_mode_company_id_text_like = n_mode_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mode_company_id_text_like)){
            this.getSearchCond().like("mode_company_id_text", n_mode_company_id_text_like);
        }
    }
	private String n_employee_id_text_eq;//[员工]
	public void setN_employee_id_text_eq(String n_employee_id_text_eq) {
        this.n_employee_id_text_eq = n_employee_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_id_text_eq)){
            this.getSearchCond().eq("employee_id_text", n_employee_id_text_eq);
        }
    }
	private String n_employee_id_text_like;//[员工]
	public void setN_employee_id_text_like(String n_employee_id_text_like) {
        this.n_employee_id_text_like = n_employee_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_employee_id_text_like)){
            this.getSearchCond().like("employee_id_text", n_employee_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_meeting_id_text_eq;//[会议]
	public void setN_meeting_id_text_eq(String n_meeting_id_text_eq) {
        this.n_meeting_id_text_eq = n_meeting_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_meeting_id_text_eq)){
            this.getSearchCond().eq("meeting_id_text", n_meeting_id_text_eq);
        }
    }
	private String n_meeting_id_text_like;//[会议]
	public void setN_meeting_id_text_like(String n_meeting_id_text_like) {
        this.n_meeting_id_text_like = n_meeting_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_meeting_id_text_like)){
            this.getSearchCond().like("meeting_id_text", n_meeting_id_text_like);
        }
    }
	private String n_second_approver_id_text_eq;//[第二次审批]
	public void setN_second_approver_id_text_eq(String n_second_approver_id_text_eq) {
        this.n_second_approver_id_text_eq = n_second_approver_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_second_approver_id_text_eq)){
            this.getSearchCond().eq("second_approver_id_text", n_second_approver_id_text_eq);
        }
    }
	private String n_second_approver_id_text_like;//[第二次审批]
	public void setN_second_approver_id_text_like(String n_second_approver_id_text_like) {
        this.n_second_approver_id_text_like = n_second_approver_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_second_approver_id_text_like)){
            this.getSearchCond().like("second_approver_id_text", n_second_approver_id_text_like);
        }
    }
	private String n_category_id_text_eq;//[员工标签]
	public void setN_category_id_text_eq(String n_category_id_text_eq) {
        this.n_category_id_text_eq = n_category_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_text_eq)){
            this.getSearchCond().eq("category_id_text", n_category_id_text_eq);
        }
    }
	private String n_category_id_text_like;//[员工标签]
	public void setN_category_id_text_like(String n_category_id_text_like) {
        this.n_category_id_text_like = n_category_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_category_id_text_like)){
            this.getSearchCond().like("category_id_text", n_category_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[用户]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[用户]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_first_approver_id_text_eq;//[首次审批]
	public void setN_first_approver_id_text_eq(String n_first_approver_id_text_eq) {
        this.n_first_approver_id_text_eq = n_first_approver_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_first_approver_id_text_eq)){
            this.getSearchCond().eq("first_approver_id_text", n_first_approver_id_text_eq);
        }
    }
	private String n_first_approver_id_text_like;//[首次审批]
	public void setN_first_approver_id_text_like(String n_first_approver_id_text_like) {
        this.n_first_approver_id_text_like = n_first_approver_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_first_approver_id_text_like)){
            this.getSearchCond().like("first_approver_id_text", n_first_approver_id_text_like);
        }
    }
	private String n_holiday_status_id_text_eq;//[休假类型]
	public void setN_holiday_status_id_text_eq(String n_holiday_status_id_text_eq) {
        this.n_holiday_status_id_text_eq = n_holiday_status_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_holiday_status_id_text_eq)){
            this.getSearchCond().eq("holiday_status_id_text", n_holiday_status_id_text_eq);
        }
    }
	private String n_holiday_status_id_text_like;//[休假类型]
	public void setN_holiday_status_id_text_like(String n_holiday_status_id_text_like) {
        this.n_holiday_status_id_text_like = n_holiday_status_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_holiday_status_id_text_like)){
            this.getSearchCond().like("holiday_status_id_text", n_holiday_status_id_text_like);
        }
    }
	private String n_manager_id_text_eq;//[经理]
	public void setN_manager_id_text_eq(String n_manager_id_text_eq) {
        this.n_manager_id_text_eq = n_manager_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_manager_id_text_eq)){
            this.getSearchCond().eq("manager_id_text", n_manager_id_text_eq);
        }
    }
	private String n_manager_id_text_like;//[经理]
	public void setN_manager_id_text_like(String n_manager_id_text_like) {
        this.n_manager_id_text_like = n_manager_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_manager_id_text_like)){
            this.getSearchCond().like("manager_id_text", n_manager_id_text_like);
        }
    }
	private String n_parent_id_text_eq;//[上级]
	public void setN_parent_id_text_eq(String n_parent_id_text_eq) {
        this.n_parent_id_text_eq = n_parent_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_eq)){
            this.getSearchCond().eq("parent_id_text", n_parent_id_text_eq);
        }
    }
	private String n_parent_id_text_like;//[上级]
	public void setN_parent_id_text_like(String n_parent_id_text_like) {
        this.n_parent_id_text_like = n_parent_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_like)){
            this.getSearchCond().like("parent_id_text", n_parent_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_department_id_text_eq;//[部门]
	public void setN_department_id_text_eq(String n_department_id_text_eq) {
        this.n_department_id_text_eq = n_department_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_department_id_text_eq)){
            this.getSearchCond().eq("department_id_text", n_department_id_text_eq);
        }
    }
	private String n_department_id_text_like;//[部门]
	public void setN_department_id_text_like(String n_department_id_text_like) {
        this.n_department_id_text_like = n_department_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_department_id_text_like)){
            this.getSearchCond().like("department_id_text", n_department_id_text_like);
        }
    }
	private Long n_manager_id_eq;//[经理]
	public void setN_manager_id_eq(Long n_manager_id_eq) {
        this.n_manager_id_eq = n_manager_id_eq;
        if(!ObjectUtils.isEmpty(this.n_manager_id_eq)){
            this.getSearchCond().eq("manager_id", n_manager_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_user_id_eq;//[用户]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_parent_id_eq;//[上级]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_mode_company_id_eq;//[公司]
	public void setN_mode_company_id_eq(Long n_mode_company_id_eq) {
        this.n_mode_company_id_eq = n_mode_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mode_company_id_eq)){
            this.getSearchCond().eq("mode_company_id", n_mode_company_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_second_approver_id_eq;//[第二次审批]
	public void setN_second_approver_id_eq(Long n_second_approver_id_eq) {
        this.n_second_approver_id_eq = n_second_approver_id_eq;
        if(!ObjectUtils.isEmpty(this.n_second_approver_id_eq)){
            this.getSearchCond().eq("second_approver_id", n_second_approver_id_eq);
        }
    }
	private Long n_meeting_id_eq;//[会议]
	public void setN_meeting_id_eq(Long n_meeting_id_eq) {
        this.n_meeting_id_eq = n_meeting_id_eq;
        if(!ObjectUtils.isEmpty(this.n_meeting_id_eq)){
            this.getSearchCond().eq("meeting_id", n_meeting_id_eq);
        }
    }
	private Long n_holiday_status_id_eq;//[休假类型]
	public void setN_holiday_status_id_eq(Long n_holiday_status_id_eq) {
        this.n_holiday_status_id_eq = n_holiday_status_id_eq;
        if(!ObjectUtils.isEmpty(this.n_holiday_status_id_eq)){
            this.getSearchCond().eq("holiday_status_id", n_holiday_status_id_eq);
        }
    }
	private Long n_department_id_eq;//[部门]
	public void setN_department_id_eq(Long n_department_id_eq) {
        this.n_department_id_eq = n_department_id_eq;
        if(!ObjectUtils.isEmpty(this.n_department_id_eq)){
            this.getSearchCond().eq("department_id", n_department_id_eq);
        }
    }
	private Long n_employee_id_eq;//[员工]
	public void setN_employee_id_eq(Long n_employee_id_eq) {
        this.n_employee_id_eq = n_employee_id_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_id_eq)){
            this.getSearchCond().eq("employee_id", n_employee_id_eq);
        }
    }
	private Long n_category_id_eq;//[员工标签]
	public void setN_category_id_eq(Long n_category_id_eq) {
        this.n_category_id_eq = n_category_id_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_eq)){
            this.getSearchCond().eq("category_id", n_category_id_eq);
        }
    }
	private Long n_first_approver_id_eq;//[首次审批]
	public void setN_first_approver_id_eq(Long n_first_approver_id_eq) {
        this.n_first_approver_id_eq = n_first_approver_id_eq;
        if(!ObjectUtils.isEmpty(this.n_first_approver_id_eq)){
            this.getSearchCond().eq("first_approver_id", n_first_approver_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



