package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_ruleSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_rule] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-rule", fallback = stock_ruleFallback.class)
public interface stock_ruleFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_rules/{id}")
    Stock_rule update(@PathVariable("id") Long id,@RequestBody Stock_rule stock_rule);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_rules/batch")
    Boolean updateBatch(@RequestBody List<Stock_rule> stock_rules);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules")
    Stock_rule create(@RequestBody Stock_rule stock_rule);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules/batch")
    Boolean createBatch(@RequestBody List<Stock_rule> stock_rules);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules/search")
    Page<Stock_rule> search(@RequestBody Stock_ruleSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/stock_rules/{id}")
    Stock_rule get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_rules/select")
    Page<Stock_rule> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_rules/getdraft")
    Stock_rule getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules/checkkey")
    Boolean checkKey(@RequestBody Stock_rule stock_rule);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules/save")
    Boolean save(@RequestBody Stock_rule stock_rule);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_rule> stock_rules);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules/searchdefault")
    Page<Stock_rule> searchDefault(@RequestBody Stock_ruleSearchContext context);


}
