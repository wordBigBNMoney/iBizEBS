package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[库存位置]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "STOCK_LOCATION",resultMap = "Stock_locationResultMap")
public class Stock_location extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 通道(X)
     */
    @TableField(value = "posx")
    @JSONField(name = "posx")
    @JsonProperty("posx")
    private Integer posx;
    /**
     * 货架(Y)
     */
    @TableField(value = "posy")
    @JSONField(name = "posy")
    @JsonProperty("posy")
    private Integer posy;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 条码
     */
    @TableField(value = "barcode")
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 高度(Z)
     */
    @TableField(value = "posz")
    @JSONField(name = "posz")
    @JsonProperty("posz")
    private Integer posz;
    /**
     * 完整的位置名称
     */
    @DEField(name = "complete_name")
    @TableField(value = "complete_name")
    @JSONField(name = "complete_name")
    @JsonProperty("complete_name")
    private String completeName;
    /**
     * 是一个报废位置？
     */
    @DEField(name = "scrap_location")
    @TableField(value = "scrap_location")
    @JSONField(name = "scrap_location")
    @JsonProperty("scrap_location")
    private Boolean scrapLocation;
    /**
     * 是一个退回位置？
     */
    @DEField(name = "return_location")
    @TableField(value = "return_location")
    @JSONField(name = "return_location")
    @JsonProperty("return_location")
    private Boolean returnLocation;
    /**
     * 位置名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 父级路径
     */
    @DEField(name = "parent_path")
    @TableField(value = "parent_path")
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    private String parentPath;
    /**
     * 即时库存
     */
    @TableField(exist = false)
    @JSONField(name = "quant_ids")
    @JsonProperty("quant_ids")
    private String quantIds;
    /**
     * 位置类型
     */
    @TableField(value = "usage")
    @JSONField(name = "usage")
    @JsonProperty("usage")
    private String usage;
    /**
     * 额外的信息
     */
    @TableField(value = "comment")
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 包含
     */
    @TableField(exist = false)
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 库存计价科目（出向）
     */
    @TableField(exist = false)
    @JSONField(name = "valuation_out_account_id_text")
    @JsonProperty("valuation_out_account_id_text")
    private String valuationOutAccountIdText;
    /**
     * 下架策略
     */
    @TableField(exist = false)
    @JSONField(name = "removal_strategy_id_text")
    @JsonProperty("removal_strategy_id_text")
    private String removalStrategyIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 库存计价科目（入向）
     */
    @TableField(exist = false)
    @JSONField(name = "valuation_in_account_id_text")
    @JsonProperty("valuation_in_account_id_text")
    private String valuationInAccountIdText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 库存计价科目（入向）
     */
    @DEField(name = "valuation_in_account_id")
    @TableField(value = "valuation_in_account_id")
    @JSONField(name = "valuation_in_account_id")
    @JsonProperty("valuation_in_account_id")
    private Long valuationInAccountId;
    /**
     * 公司
     */
    @DEField(name = "company_id" , preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 库存计价科目（出向）
     */
    @DEField(name = "valuation_out_account_id")
    @TableField(value = "valuation_out_account_id")
    @JSONField(name = "valuation_out_account_id")
    @JsonProperty("valuation_out_account_id")
    private Long valuationOutAccountId;
    /**
     * 下架策略
     */
    @DEField(name = "removal_strategy_id")
    @TableField(value = "removal_strategy_id")
    @JSONField(name = "removal_strategy_id")
    @JsonProperty("removal_strategy_id")
    private Long removalStrategyId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooValuationInAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooValuationOutAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_removal odooRemovalStrategy;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [通道(X)]
     */
    public void setPosx(Integer posx){
        this.posx = posx ;
        this.modify("posx",posx);
    }

    /**
     * 设置 [货架(Y)]
     */
    public void setPosy(Integer posy){
        this.posy = posy ;
        this.modify("posy",posy);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [条码]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [高度(Z)]
     */
    public void setPosz(Integer posz){
        this.posz = posz ;
        this.modify("posz",posz);
    }

    /**
     * 设置 [完整的位置名称]
     */
    public void setCompleteName(String completeName){
        this.completeName = completeName ;
        this.modify("complete_name",completeName);
    }

    /**
     * 设置 [是一个报废位置？]
     */
    public void setScrapLocation(Boolean scrapLocation){
        this.scrapLocation = scrapLocation ;
        this.modify("scrap_location",scrapLocation);
    }

    /**
     * 设置 [是一个退回位置？]
     */
    public void setReturnLocation(Boolean returnLocation){
        this.returnLocation = returnLocation ;
        this.modify("return_location",returnLocation);
    }

    /**
     * 设置 [位置名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [父级路径]
     */
    public void setParentPath(String parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }

    /**
     * 设置 [位置类型]
     */
    public void setUsage(String usage){
        this.usage = usage ;
        this.modify("usage",usage);
    }

    /**
     * 设置 [额外的信息]
     */
    public void setComment(String comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }

    /**
     * 设置 [库存计价科目（入向）]
     */
    public void setValuationInAccountId(Long valuationInAccountId){
        this.valuationInAccountId = valuationInAccountId ;
        this.modify("valuation_in_account_id",valuationInAccountId);
    }

    /**
     * 设置 [库存计价科目（出向）]
     */
    public void setValuationOutAccountId(Long valuationOutAccountId){
        this.valuationOutAccountId = valuationOutAccountId ;
        this.modify("valuation_out_account_id",valuationOutAccountId);
    }

    /**
     * 设置 [下架策略]
     */
    public void setRemovalStrategyId(Long removalStrategyId){
        this.removalStrategyId = removalStrategyId ;
        this.modify("removal_strategy_id",removalStrategyId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


