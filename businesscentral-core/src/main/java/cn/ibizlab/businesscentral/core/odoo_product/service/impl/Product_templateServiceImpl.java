package cn.ibizlab.businesscentral.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_templateSearchContext;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_product.mapper.Product_templateMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[产品模板] 服务对象接口实现
 */
@Slf4j
@Service("Product_templateServiceImpl")
public class Product_templateServiceImpl extends EBSServiceImpl<Product_templateMapper, Product_template> implements IProduct_templateService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService mrpBomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_imageService productImageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelist_itemService productPricelistItemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_replenishService productReplenishService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_supplierinfoService productSupplierinfoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_supplier_taxes_relService productSupplierTaxesRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_taxes_relService productTaxesRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_exclusionService productTemplateAttributeExclusionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_lineService productTemplateAttributeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_valueService productTemplateAttributeValueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_reportService purchaseReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_product_configuratorService saleProductConfiguratorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService saleReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_change_product_qtyService stockChangeProductQtyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_rules_reportService stockRulesReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_categoryService productCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "product.template" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Product_template et) {
        String supplier_taxes_id = et.getSupplierTaxesId() ;

        String taxes_id = et.getTaxesId() ;

        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_templateService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        if(org.apache.commons.lang3.StringUtils.isNotBlank(et.getSupplierTaxesId())){
            this.baseMapper.saveRelBySupplierTaxesId(et.getId() , com.alibaba.fastjson.JSONArray.parseArray(supplier_taxes_id, cn.ibizlab.businesscentral.util.domain.MultiSelectItem.class));
        }

        if(org.apache.commons.lang3.StringUtils.isNotBlank(et.getTaxesId())){
            this.baseMapper.saveRelByTaxesId(et.getId() , com.alibaba.fastjson.JSONArray.parseArray(taxes_id, cn.ibizlab.businesscentral.util.domain.MultiSelectItem.class));
        }

        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Product_template> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Product_template et) {
        if(et.getFocusNull().contains("supplier_taxes_id") || et.getSupplierTaxesId()!=null){
            et.getFocusNull().remove("supplier_taxes_id");
            productSupplierTaxesRelService.removeByProdId(et.getId());
            if(org.apache.commons.lang3.StringUtils.isNotBlank(et.getSupplierTaxesId())){
                this.baseMapper.saveRelBySupplierTaxesId(et.getId() , com.alibaba.fastjson.JSONArray.parseArray(et.getSupplierTaxesId(), cn.ibizlab.businesscentral.util.domain.MultiSelectItem.class));
            }
        }

        if(et.getFocusNull().contains("taxes_id") || et.getTaxesId()!=null){
            et.getFocusNull().remove("taxes_id");
            productTaxesRelService.removeByProdId(et.getId());
            if(org.apache.commons.lang3.StringUtils.isNotBlank(et.getTaxesId())){
                this.baseMapper.saveRelByTaxesId(et.getId() , com.alibaba.fastjson.JSONArray.parseArray(et.getTaxesId(), cn.ibizlab.businesscentral.util.domain.MultiSelectItem.class));
            }
        }

        Product_template old = new Product_template() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_templateService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_templateService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Product_template> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        mrpBomService.resetByProductTmplId(key);
        productImageService.resetByProductTmplId(key);
        productPricelistItemService.removeByProductTmplId(key);
        productProductService.removeByProductTmplId(key);
        productReplenishService.resetByProductTmplId(key);
        productSupplierinfoService.removeByProductTmplId(key);
        productTemplateAttributeExclusionService.removeByProductTmplId(key);
        productTemplateAttributeLineService.removeByProductTmplId(key);
        productTemplateAttributeValueService.removeByProductTmplId(key);
        purchaseReportService.resetByProductTmplId(key);
        saleProductConfiguratorService.resetByProductTemplateId(key);
        saleReportService.resetByProductTmplId(key);
        stockChangeProductQtyService.resetByProductTmplId(key);
        stockRulesReportService.resetByProductTmplId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        mrpBomService.resetByProductTmplId(idList);
        productImageService.resetByProductTmplId(idList);
        productPricelistItemService.removeByProductTmplId(idList);
        productProductService.removeByProductTmplId(idList);
        productReplenishService.resetByProductTmplId(idList);
        productSupplierinfoService.removeByProductTmplId(idList);
        productTemplateAttributeExclusionService.removeByProductTmplId(idList);
        productTemplateAttributeLineService.removeByProductTmplId(idList);
        productTemplateAttributeValueService.removeByProductTmplId(idList);
        purchaseReportService.resetByProductTmplId(idList);
        saleProductConfiguratorService.resetByProductTemplateId(idList);
        saleReportService.resetByProductTmplId(idList);
        stockChangeProductQtyService.resetByProductTmplId(idList);
        stockRulesReportService.resetByProductTmplId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Product_template get(Long key) {
        Product_template et = getById(key);
        if(et==null){
            et=new Product_template();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Product_template getDraft(Product_template et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Product_template et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Product_template et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Product_template et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Product_template> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Product_template> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Product_template> selectByCategId(Long id) {
        return baseMapper.selectByCategId(id);
    }
    @Override
    public void resetByCategId(Long id) {
        this.update(new UpdateWrapper<Product_template>().set("categ_id",null).eq("categ_id",id));
    }

    @Override
    public void resetByCategId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Product_template>().set("categ_id",null).in("categ_id",ids));
    }

    @Override
    public void removeByCategId(Long id) {
        this.remove(new QueryWrapper<Product_template>().eq("categ_id",id));
    }

	@Override
    public List<Product_template> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Product_template>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Product_template>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Product_template>().eq("company_id",id));
    }

	@Override
    public List<Product_template> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Product_template>().eq("create_uid",id));
    }

	@Override
    public List<Product_template> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Product_template>().eq("write_uid",id));
    }

	@Override
    public List<Product_template> selectByUomId(Long id) {
        return baseMapper.selectByUomId(id);
    }
    @Override
    public void resetByUomId(Long id) {
        this.update(new UpdateWrapper<Product_template>().set("uom_id",null).eq("uom_id",id));
    }

    @Override
    public void resetByUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Product_template>().set("uom_id",null).in("uom_id",ids));
    }

    @Override
    public void removeByUomId(Long id) {
        this.remove(new QueryWrapper<Product_template>().eq("uom_id",id));
    }

	@Override
    public List<Product_template> selectByUomPoId(Long id) {
        return baseMapper.selectByUomPoId(id);
    }
    @Override
    public void resetByUomPoId(Long id) {
        this.update(new UpdateWrapper<Product_template>().set("uom_po_id",null).eq("uom_po_id",id));
    }

    @Override
    public void resetByUomPoId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Product_template>().set("uom_po_id",null).in("uom_po_id",ids));
    }

    @Override
    public void removeByUomPoId(Long id) {
        this.remove(new QueryWrapper<Product_template>().eq("uom_po_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Product_template> searchDefault(Product_templateSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Product_template> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Product_template>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 首选表格
     */
    @Override
    public Page<Product_template> searchMaster(Product_templateSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Product_template> pages=baseMapper.searchMaster(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Product_template>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Product_template et){
        //实体关系[DER1N_PRODUCT_TEMPLATE__PRODUCT_CATEGORY__CATEG_ID]
        if(!ObjectUtils.isEmpty(et.getCategId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category odooCateg=et.getOdooCateg();
            if(ObjectUtils.isEmpty(odooCateg)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category majorEntity=productCategoryService.get(et.getCategId());
                et.setOdooCateg(majorEntity);
                odooCateg=majorEntity;
            }
            et.setCategIdText(odooCateg.getName());
        }
        //实体关系[DER1N_PRODUCT_TEMPLATE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_PRODUCT_TEMPLATE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_PRODUCT_TEMPLATE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_PRODUCT_TEMPLATE__UOM_UOM__UOM_ID]
        if(!ObjectUtils.isEmpty(et.getUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooUom=et.getOdooUom();
            if(ObjectUtils.isEmpty(odooUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getUomId());
                et.setOdooUom(majorEntity);
                odooUom=majorEntity;
            }
            et.setUomName(odooUom.getName());
        }
        //实体关系[DER1N_PRODUCT_TEMPLATE__UOM_UOM__UOM_PO_ID]
        if(!ObjectUtils.isEmpty(et.getUomPoId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooUomPo=et.getOdooUomPo();
            if(ObjectUtils.isEmpty(odooUomPo)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getUomPoId());
                et.setOdooUomPo(majorEntity);
                odooUomPo=majorEntity;
            }
            et.setUomPoIdText(odooUomPo.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Product_template> getProductTemplateByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Product_template> getProductTemplateByEntities(List<Product_template> entities) {
        List ids =new ArrayList();
        for(Product_template entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



