package cn.ibizlab.businesscentral.core.odoo_stock.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_inventorySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Stock_inventoryMapper extends BaseMapper<Stock_inventory>{

    Page<Stock_inventory> searchDefault(IPage page, @Param("srf") Stock_inventorySearchContext context, @Param("ew") Wrapper<Stock_inventory> wrapper) ;
    @Override
    Stock_inventory selectById(Serializable id);
    @Override
    int insert(Stock_inventory entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Stock_inventory entity);
    @Override
    int update(@Param(Constants.ENTITY) Stock_inventory entity, @Param("ew") Wrapper<Stock_inventory> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Stock_inventory> selectByCategoryId(@Param("id") Serializable id) ;

    List<Stock_inventory> selectByProductId(@Param("id") Serializable id) ;

    List<Stock_inventory> selectByCompanyId(@Param("id") Serializable id) ;

    List<Stock_inventory> selectByPartnerId(@Param("id") Serializable id) ;

    List<Stock_inventory> selectByCreateUid(@Param("id") Serializable id) ;

    List<Stock_inventory> selectByWriteUid(@Param("id") Serializable id) ;

    List<Stock_inventory> selectByLocationId(@Param("id") Serializable id) ;

    List<Stock_inventory> selectByLotId(@Param("id") Serializable id) ;

    List<Stock_inventory> selectByPackageId(@Param("id") Serializable id) ;


}
