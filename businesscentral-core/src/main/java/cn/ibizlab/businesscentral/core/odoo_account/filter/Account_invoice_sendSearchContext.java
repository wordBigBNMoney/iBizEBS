package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_send;
/**
 * 关系型数据实体[Account_invoice_send] 查询条件对象
 */
@Slf4j
@Data
public class Account_invoice_sendSearchContext extends QueryWrapperContext<Account_invoice_send> {

	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_template_id_text_eq;//[使用模版]
	public void setN_template_id_text_eq(String n_template_id_text_eq) {
        this.n_template_id_text_eq = n_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_template_id_text_eq)){
            this.getSearchCond().eq("template_id_text", n_template_id_text_eq);
        }
    }
	private String n_template_id_text_like;//[使用模版]
	public void setN_template_id_text_like(String n_template_id_text_like) {
        this.n_template_id_text_like = n_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_template_id_text_like)){
            this.getSearchCond().like("template_id_text", n_template_id_text_like);
        }
    }
	private Long n_composer_id_eq;//[邮件撰写者]
	public void setN_composer_id_eq(Long n_composer_id_eq) {
        this.n_composer_id_eq = n_composer_id_eq;
        if(!ObjectUtils.isEmpty(this.n_composer_id_eq)){
            this.getSearchCond().eq("composer_id", n_composer_id_eq);
        }
    }
	private Long n_template_id_eq;//[使用模版]
	public void setN_template_id_eq(Long n_template_id_eq) {
        this.n_template_id_eq = n_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_template_id_eq)){
            this.getSearchCond().eq("template_id", n_template_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



