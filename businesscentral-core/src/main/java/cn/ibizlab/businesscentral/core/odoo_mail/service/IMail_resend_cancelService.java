package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_resend_cancelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_resend_cancel] 服务对象接口
 */
public interface IMail_resend_cancelService extends IService<Mail_resend_cancel>{

    boolean create(Mail_resend_cancel et) ;
    void createBatch(List<Mail_resend_cancel> list) ;
    boolean update(Mail_resend_cancel et) ;
    void updateBatch(List<Mail_resend_cancel> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_resend_cancel get(Long key) ;
    Mail_resend_cancel getDraft(Mail_resend_cancel et) ;
    boolean checkKey(Mail_resend_cancel et) ;
    boolean save(Mail_resend_cancel et) ;
    void saveBatch(List<Mail_resend_cancel> list) ;
    Page<Mail_resend_cancel> searchDefault(Mail_resend_cancelSearchContext context) ;
    List<Mail_resend_cancel> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_resend_cancel> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_resend_cancel> getMailResendCancelByIds(List<Long> ids) ;
    List<Mail_resend_cancel> getMailResendCancelByEntities(List<Mail_resend_cancel> entities) ;
}


