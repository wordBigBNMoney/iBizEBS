package cn.ibizlab.businesscentral.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_lead2opportunity_partner] 服务对象接口
 */
@Component
public class crm_lead2opportunity_partnerFallback implements crm_lead2opportunity_partnerFeignClient{


    public Crm_lead2opportunity_partner update(Long id, Crm_lead2opportunity_partner crm_lead2opportunity_partner){
            return null;
     }
    public Boolean updateBatch(List<Crm_lead2opportunity_partner> crm_lead2opportunity_partners){
            return false;
     }


    public Page<Crm_lead2opportunity_partner> search(Crm_lead2opportunity_partnerSearchContext context){
            return null;
     }



    public Crm_lead2opportunity_partner get(Long id){
            return null;
     }


    public Crm_lead2opportunity_partner create(Crm_lead2opportunity_partner crm_lead2opportunity_partner){
            return null;
     }
    public Boolean createBatch(List<Crm_lead2opportunity_partner> crm_lead2opportunity_partners){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Crm_lead2opportunity_partner> select(){
            return null;
     }

    public Crm_lead2opportunity_partner getDraft(){
            return null;
    }



    public Boolean checkKey(Crm_lead2opportunity_partner crm_lead2opportunity_partner){
            return false;
     }


    public Boolean save(Crm_lead2opportunity_partner crm_lead2opportunity_partner){
            return false;
     }
    public Boolean saveBatch(List<Crm_lead2opportunity_partner> crm_lead2opportunity_partners){
            return false;
     }

    public Page<Crm_lead2opportunity_partner> searchDefault(Crm_lead2opportunity_partnerSearchContext context){
            return null;
     }


}
