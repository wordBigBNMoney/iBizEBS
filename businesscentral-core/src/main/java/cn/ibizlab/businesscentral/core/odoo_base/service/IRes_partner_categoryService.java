package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_categorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_partner_category] 服务对象接口
 */
public interface IRes_partner_categoryService extends IService<Res_partner_category>{

    boolean create(Res_partner_category et) ;
    void createBatch(List<Res_partner_category> list) ;
    boolean update(Res_partner_category et) ;
    void updateBatch(List<Res_partner_category> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_partner_category get(Long key) ;
    Res_partner_category getDraft(Res_partner_category et) ;
    boolean checkKey(Res_partner_category et) ;
    boolean save(Res_partner_category et) ;
    void saveBatch(List<Res_partner_category> list) ;
    Page<Res_partner_category> searchDefault(Res_partner_categorySearchContext context) ;
    List<Res_partner_category> selectByParentId(Long id);
    void removeByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Res_partner_category> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_partner_category> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_partner_category> getResPartnerCategoryByIds(List<Long> ids) ;
    List<Res_partner_category> getResPartnerCategoryByEntities(List<Res_partner_category> entities) ;
}


