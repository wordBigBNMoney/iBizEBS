package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_request;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_requestSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_request] 服务对象接口
 */
public interface IMro_requestService extends IService<Mro_request>{

    boolean create(Mro_request et) ;
    void createBatch(List<Mro_request> list) ;
    boolean update(Mro_request et) ;
    void updateBatch(List<Mro_request> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_request get(Long key) ;
    Mro_request getDraft(Mro_request et) ;
    boolean checkKey(Mro_request et) ;
    boolean save(Mro_request et) ;
    void saveBatch(List<Mro_request> list) ;
    Page<Mro_request> searchDefault(Mro_requestSearchContext context) ;
    List<Mro_request> selectByAssetId(Long id);
    void resetByAssetId(Long id);
    void resetByAssetId(Collection<Long> ids);
    void removeByAssetId(Long id);
    List<Mro_request> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_request> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_request> getMroRequestByIds(List<Long> ids) ;
    List<Mro_request> getMroRequestByEntities(List<Mro_request> entities) ;
}


