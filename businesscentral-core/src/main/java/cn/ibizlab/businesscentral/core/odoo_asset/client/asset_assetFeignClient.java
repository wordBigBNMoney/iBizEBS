package cn.ibizlab.businesscentral.core.odoo_asset.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.businesscentral.core.odoo_asset.filter.Asset_assetSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[asset_asset] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-asset:odoo-asset}", contextId = "asset-asset", fallback = asset_assetFallback.class)
public interface asset_assetFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/asset_assets/{id}")
    Asset_asset get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/asset_assets/search")
    Page<Asset_asset> search(@RequestBody Asset_assetSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/asset_assets")
    Asset_asset create(@RequestBody Asset_asset asset_asset);

    @RequestMapping(method = RequestMethod.POST, value = "/asset_assets/batch")
    Boolean createBatch(@RequestBody List<Asset_asset> asset_assets);


    @RequestMapping(method = RequestMethod.PUT, value = "/asset_assets/{id}")
    Asset_asset update(@PathVariable("id") Long id,@RequestBody Asset_asset asset_asset);

    @RequestMapping(method = RequestMethod.PUT, value = "/asset_assets/batch")
    Boolean updateBatch(@RequestBody List<Asset_asset> asset_assets);




    @RequestMapping(method = RequestMethod.DELETE, value = "/asset_assets/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/asset_assets/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/asset_assets/select")
    Page<Asset_asset> select();


    @RequestMapping(method = RequestMethod.GET, value = "/asset_assets/getdraft")
    Asset_asset getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/asset_assets/checkkey")
    Boolean checkKey(@RequestBody Asset_asset asset_asset);


    @RequestMapping(method = RequestMethod.POST, value = "/asset_assets/save")
    Boolean save(@RequestBody Asset_asset asset_asset);

    @RequestMapping(method = RequestMethod.POST, value = "/asset_assets/savebatch")
    Boolean saveBatch(@RequestBody List<Asset_asset> asset_assets);



    @RequestMapping(method = RequestMethod.POST, value = "/asset_assets/searchdefault")
    Page<Asset_asset> searchDefault(@RequestBody Asset_assetSearchContext context);


}
