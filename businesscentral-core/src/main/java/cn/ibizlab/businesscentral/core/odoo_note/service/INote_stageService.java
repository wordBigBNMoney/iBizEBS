package cn.ibizlab.businesscentral.core.odoo_note.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_note.domain.Note_stage;
import cn.ibizlab.businesscentral.core.odoo_note.filter.Note_stageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Note_stage] 服务对象接口
 */
public interface INote_stageService extends IService<Note_stage>{

    boolean create(Note_stage et) ;
    void createBatch(List<Note_stage> list) ;
    boolean update(Note_stage et) ;
    void updateBatch(List<Note_stage> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Note_stage get(Long key) ;
    Note_stage getDraft(Note_stage et) ;
    boolean checkKey(Note_stage et) ;
    boolean save(Note_stage et) ;
    void saveBatch(List<Note_stage> list) ;
    Page<Note_stage> searchDefault(Note_stageSearchContext context) ;
    List<Note_stage> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Note_stage> selectByUserId(Long id);
    void removeByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Note_stage> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Note_stage> getNoteStageByIds(List<Long> ids) ;
    List<Note_stage> getNoteStageByEntities(List<Note_stage> entities) ;
}


