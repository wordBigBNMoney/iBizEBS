package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_moveSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_moveMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[库存移动] 服务对象接口实现
 */
@Slf4j
@Service("Stock_moveServiceImpl")
public class Stock_moveServiceImpl extends EBSServiceImpl<Stock_moveMapper, Stock_move> implements IStock_moveService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produce_lineService mrpProductProduceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService purchaseRequisitionLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService repairLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;

    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_picking_lineService stockReturnPickingLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bom_lineService mrpBomLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routing_workcenterService mrpRoutingWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService mrpUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workorderService mrpWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_packagingService productPackagingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventoryService stockInventoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_levelService stockPackageLevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService stockRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.move" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_move et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_moveService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_move> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_move et) {
        Stock_move old = new Stock_move() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_moveService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_moveService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_move> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountMoveService.resetByStockMoveId(key);
        mrpProductProduceLineService.resetByMoveId(key);
        repairLineService.resetByMoveId(key);
        repairOrderService.resetByMoveId(key);
        stockMoveLineService.resetByMoveId(key);
        stockMoveService.resetByOriginReturnedMoveId(key);
        stockReturnPickingLineService.resetByMoveId(key);
        stockScrapService.resetByMoveId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountMoveService.resetByStockMoveId(idList);
        mrpProductProduceLineService.resetByMoveId(idList);
        repairLineService.resetByMoveId(idList);
        repairOrderService.resetByMoveId(idList);
        stockMoveLineService.resetByMoveId(idList);
        stockMoveService.resetByOriginReturnedMoveId(idList);
        stockReturnPickingLineService.resetByMoveId(idList);
        stockScrapService.resetByMoveId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_move get(Long key) {
        Stock_move et = getById(key);
        if(et==null){
            et=new Stock_move();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_move getDraft(Stock_move et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_move et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_move et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_move et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_move> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_move> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_move> selectByBomLineId(Long id) {
        return baseMapper.selectByBomLineId(id);
    }
    @Override
    public void resetByBomLineId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("bom_line_id",null).eq("bom_line_id",id));
    }

    @Override
    public void resetByBomLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("bom_line_id",null).in("bom_line_id",ids));
    }

    @Override
    public void removeByBomLineId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("bom_line_id",id));
    }

	@Override
    public List<Stock_move> selectByCreatedProductionId(Long id) {
        return baseMapper.selectByCreatedProductionId(id);
    }
    @Override
    public void resetByCreatedProductionId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("created_production_id",null).eq("created_production_id",id));
    }

    @Override
    public void resetByCreatedProductionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("created_production_id",null).in("created_production_id",ids));
    }

    @Override
    public void removeByCreatedProductionId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("created_production_id",id));
    }

	@Override
    public List<Stock_move> selectByProductionId(Long id) {
        return baseMapper.selectByProductionId(id);
    }
    @Override
    public void resetByProductionId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("production_id",null).eq("production_id",id));
    }

    @Override
    public void resetByProductionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("production_id",null).in("production_id",ids));
    }

    @Override
    public void removeByProductionId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("production_id",id));
    }

	@Override
    public List<Stock_move> selectByRawMaterialProductionId(Long id) {
        return baseMapper.selectByRawMaterialProductionId(id);
    }
    @Override
    public void resetByRawMaterialProductionId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("raw_material_production_id",null).eq("raw_material_production_id",id));
    }

    @Override
    public void resetByRawMaterialProductionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("raw_material_production_id",null).in("raw_material_production_id",ids));
    }

    @Override
    public void removeByRawMaterialProductionId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("raw_material_production_id",id));
    }

	@Override
    public List<Stock_move> selectByOperationId(Long id) {
        return baseMapper.selectByOperationId(id);
    }
    @Override
    public void resetByOperationId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("operation_id",null).eq("operation_id",id));
    }

    @Override
    public void resetByOperationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("operation_id",null).in("operation_id",ids));
    }

    @Override
    public void removeByOperationId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("operation_id",id));
    }

	@Override
    public List<Stock_move> selectByConsumeUnbuildId(Long id) {
        return baseMapper.selectByConsumeUnbuildId(id);
    }
    @Override
    public void resetByConsumeUnbuildId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("consume_unbuild_id",null).eq("consume_unbuild_id",id));
    }

    @Override
    public void resetByConsumeUnbuildId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("consume_unbuild_id",null).in("consume_unbuild_id",ids));
    }

    @Override
    public void removeByConsumeUnbuildId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("consume_unbuild_id",id));
    }

	@Override
    public List<Stock_move> selectByUnbuildId(Long id) {
        return baseMapper.selectByUnbuildId(id);
    }
    @Override
    public void resetByUnbuildId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("unbuild_id",null).eq("unbuild_id",id));
    }

    @Override
    public void resetByUnbuildId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("unbuild_id",null).in("unbuild_id",ids));
    }

    @Override
    public void removeByUnbuildId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("unbuild_id",id));
    }

	@Override
    public List<Stock_move> selectByWorkorderId(Long id) {
        return baseMapper.selectByWorkorderId(id);
    }
    @Override
    public void resetByWorkorderId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("workorder_id",null).eq("workorder_id",id));
    }

    @Override
    public void resetByWorkorderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("workorder_id",null).in("workorder_id",ids));
    }

    @Override
    public void removeByWorkorderId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("workorder_id",id));
    }

	@Override
    public List<Stock_move> selectByProductPackaging(Long id) {
        return baseMapper.selectByProductPackaging(id);
    }
    @Override
    public void resetByProductPackaging(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("product_packaging",null).eq("product_packaging",id));
    }

    @Override
    public void resetByProductPackaging(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("product_packaging",null).in("product_packaging",ids));
    }

    @Override
    public void removeByProductPackaging(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("product_packaging",id));
    }

	@Override
    public List<Stock_move> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("product_id",id));
    }

	@Override
    public List<Stock_move> selectByCreatedPurchaseLineId(Long id) {
        return baseMapper.selectByCreatedPurchaseLineId(id);
    }
    @Override
    public void resetByCreatedPurchaseLineId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("created_purchase_line_id",null).eq("created_purchase_line_id",id));
    }

    @Override
    public void resetByCreatedPurchaseLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("created_purchase_line_id",null).in("created_purchase_line_id",ids));
    }

    @Override
    public void removeByCreatedPurchaseLineId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("created_purchase_line_id",id));
    }

	@Override
    public List<Stock_move> selectByPurchaseLineId(Long id) {
        return baseMapper.selectByPurchaseLineId(id);
    }
    @Override
    public void resetByPurchaseLineId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("purchase_line_id",null).eq("purchase_line_id",id));
    }

    @Override
    public void resetByPurchaseLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("purchase_line_id",null).in("purchase_line_id",ids));
    }

    @Override
    public void removeByPurchaseLineId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("purchase_line_id",id));
    }

	@Override
    public List<Stock_move> selectByRepairId(Long id) {
        return baseMapper.selectByRepairId(id);
    }
    @Override
    public void resetByRepairId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("repair_id",null).eq("repair_id",id));
    }

    @Override
    public void resetByRepairId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("repair_id",null).in("repair_id",ids));
    }

    @Override
    public void removeByRepairId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("repair_id",id));
    }

	@Override
    public List<Stock_move> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("company_id",id));
    }

	@Override
    public List<Stock_move> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("partner_id",id));
    }

	@Override
    public List<Stock_move> selectByRestrictPartnerId(Long id) {
        return baseMapper.selectByRestrictPartnerId(id);
    }
    @Override
    public void resetByRestrictPartnerId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("restrict_partner_id",null).eq("restrict_partner_id",id));
    }

    @Override
    public void resetByRestrictPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("restrict_partner_id",null).in("restrict_partner_id",ids));
    }

    @Override
    public void removeByRestrictPartnerId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("restrict_partner_id",id));
    }

	@Override
    public List<Stock_move> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("create_uid",id));
    }

	@Override
    public List<Stock_move> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("write_uid",id));
    }

	@Override
    public List<Stock_move> selectBySaleLineId(Long id) {
        return baseMapper.selectBySaleLineId(id);
    }
    @Override
    public void resetBySaleLineId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("sale_line_id",null).eq("sale_line_id",id));
    }

    @Override
    public void resetBySaleLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("sale_line_id",null).in("sale_line_id",ids));
    }

    @Override
    public void removeBySaleLineId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("sale_line_id",id));
    }

	@Override
    public List<Stock_move> selectByInventoryId(Long id) {
        return baseMapper.selectByInventoryId(id);
    }
    @Override
    public void resetByInventoryId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("inventory_id",null).eq("inventory_id",id));
    }

    @Override
    public void resetByInventoryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("inventory_id",null).in("inventory_id",ids));
    }

    @Override
    public void removeByInventoryId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("inventory_id",id));
    }

	@Override
    public List<Stock_move> selectByLocationDestId(Long id) {
        return baseMapper.selectByLocationDestId(id);
    }
    @Override
    public void resetByLocationDestId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("location_dest_id",null).eq("location_dest_id",id));
    }

    @Override
    public void resetByLocationDestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("location_dest_id",null).in("location_dest_id",ids));
    }

    @Override
    public void removeByLocationDestId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("location_dest_id",id));
    }

	@Override
    public List<Stock_move> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("location_id",id));
    }

	@Override
    public List<Stock_move> selectByOriginReturnedMoveId(Long id) {
        return baseMapper.selectByOriginReturnedMoveId(id);
    }
    @Override
    public void resetByOriginReturnedMoveId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("origin_returned_move_id",null).eq("origin_returned_move_id",id));
    }

    @Override
    public void resetByOriginReturnedMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("origin_returned_move_id",null).in("origin_returned_move_id",ids));
    }

    @Override
    public void removeByOriginReturnedMoveId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("origin_returned_move_id",id));
    }

	@Override
    public List<Stock_move> selectByPackageLevelId(Long id) {
        return baseMapper.selectByPackageLevelId(id);
    }
    @Override
    public void resetByPackageLevelId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("package_level_id",null).eq("package_level_id",id));
    }

    @Override
    public void resetByPackageLevelId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("package_level_id",null).in("package_level_id",ids));
    }

    @Override
    public void removeByPackageLevelId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("package_level_id",id));
    }

	@Override
    public List<Stock_move> selectByPickingTypeId(Long id) {
        return baseMapper.selectByPickingTypeId(id);
    }
    @Override
    public void resetByPickingTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("picking_type_id",null).eq("picking_type_id",id));
    }

    @Override
    public void resetByPickingTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("picking_type_id",null).in("picking_type_id",ids));
    }

    @Override
    public void removeByPickingTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("picking_type_id",id));
    }

	@Override
    public List<Stock_move> selectByPickingId(Long id) {
        return baseMapper.selectByPickingId(id);
    }
    @Override
    public void resetByPickingId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("picking_id",null).eq("picking_id",id));
    }

    @Override
    public void resetByPickingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("picking_id",null).in("picking_id",ids));
    }

    @Override
    public void removeByPickingId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("picking_id",id));
    }

	@Override
    public List<Stock_move> selectByRuleId(Long id) {
        return baseMapper.selectByRuleId(id);
    }
    @Override
    public List<Stock_move> selectByRuleId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_move>().in("id",ids));
    }

    @Override
    public void removeByRuleId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("rule_id",id));
    }

	@Override
    public List<Stock_move> selectByWarehouseId(Long id) {
        return baseMapper.selectByWarehouseId(id);
    }
    @Override
    public void resetByWarehouseId(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("warehouse_id",null).eq("warehouse_id",id));
    }

    @Override
    public void resetByWarehouseId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("warehouse_id",null).in("warehouse_id",ids));
    }

    @Override
    public void removeByWarehouseId(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("warehouse_id",id));
    }

	@Override
    public List<Stock_move> selectByProductUom(Long id) {
        return baseMapper.selectByProductUom(id);
    }
    @Override
    public void resetByProductUom(Long id) {
        this.update(new UpdateWrapper<Stock_move>().set("product_uom",null).eq("product_uom",id));
    }

    @Override
    public void resetByProductUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move>().set("product_uom",null).in("product_uom",ids));
    }

    @Override
    public void removeByProductUom(Long id) {
        this.remove(new QueryWrapper<Stock_move>().eq("product_uom",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_move> searchDefault(Stock_moveSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_move> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_move>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_move et){
        //实体关系[DER1N_STOCK_MOVE__MRP_PRODUCTION__CREATED_PRODUCTION_ID]
        if(!ObjectUtils.isEmpty(et.getCreatedProductionId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooCreatedProduction=et.getOdooCreatedProduction();
            if(ObjectUtils.isEmpty(odooCreatedProduction)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production majorEntity=mrpProductionService.get(et.getCreatedProductionId());
                et.setOdooCreatedProduction(majorEntity);
                odooCreatedProduction=majorEntity;
            }
            et.setCreatedProductionIdText(odooCreatedProduction.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__MRP_PRODUCTION__PRODUCTION_ID]
        if(!ObjectUtils.isEmpty(et.getProductionId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooProduction=et.getOdooProduction();
            if(ObjectUtils.isEmpty(odooProduction)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production majorEntity=mrpProductionService.get(et.getProductionId());
                et.setOdooProduction(majorEntity);
                odooProduction=majorEntity;
            }
            et.setProductionIdText(odooProduction.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__MRP_PRODUCTION__RAW_MATERIAL_PRODUCTION_ID]
        if(!ObjectUtils.isEmpty(et.getRawMaterialProductionId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooRawMaterialProduction=et.getOdooRawMaterialProduction();
            if(ObjectUtils.isEmpty(odooRawMaterialProduction)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production majorEntity=mrpProductionService.get(et.getRawMaterialProductionId());
                et.setOdooRawMaterialProduction(majorEntity);
                odooRawMaterialProduction=majorEntity;
            }
            et.setRawMaterialProductionIdText(odooRawMaterialProduction.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__MRP_ROUTING_WORKCENTER__OPERATION_ID]
        if(!ObjectUtils.isEmpty(et.getOperationId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing_workcenter odooOperation=et.getOdooOperation();
            if(ObjectUtils.isEmpty(odooOperation)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing_workcenter majorEntity=mrpRoutingWorkcenterService.get(et.getOperationId());
                et.setOdooOperation(majorEntity);
                odooOperation=majorEntity;
            }
            et.setOperationIdText(odooOperation.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__MRP_UNBUILD__CONSUME_UNBUILD_ID]
        if(!ObjectUtils.isEmpty(et.getConsumeUnbuildId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild odooConsumeUnbuild=et.getOdooConsumeUnbuild();
            if(ObjectUtils.isEmpty(odooConsumeUnbuild)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild majorEntity=mrpUnbuildService.get(et.getConsumeUnbuildId());
                et.setOdooConsumeUnbuild(majorEntity);
                odooConsumeUnbuild=majorEntity;
            }
            et.setConsumeUnbuildIdText(odooConsumeUnbuild.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__MRP_UNBUILD__UNBUILD_ID]
        if(!ObjectUtils.isEmpty(et.getUnbuildId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild odooUnbuild=et.getOdooUnbuild();
            if(ObjectUtils.isEmpty(odooUnbuild)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild majorEntity=mrpUnbuildService.get(et.getUnbuildId());
                et.setOdooUnbuild(majorEntity);
                odooUnbuild=majorEntity;
            }
            et.setUnbuildIdText(odooUnbuild.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__MRP_WORKORDER__WORKORDER_ID]
        if(!ObjectUtils.isEmpty(et.getWorkorderId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder odooWorkorder=et.getOdooWorkorder();
            if(ObjectUtils.isEmpty(odooWorkorder)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder majorEntity=mrpWorkorderService.get(et.getWorkorderId());
                et.setOdooWorkorder(majorEntity);
                odooWorkorder=majorEntity;
            }
            et.setWorkorderIdText(odooWorkorder.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__PRODUCT_PACKAGING__PRODUCT_PACKAGING]
        if(!ObjectUtils.isEmpty(et.getProductPackaging())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging odooProductPackging=et.getOdooProductPackging();
            if(ObjectUtils.isEmpty(odooProductPackging)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging majorEntity=productPackagingService.get(et.getProductPackaging());
                et.setOdooProductPackging(majorEntity);
                odooProductPackging=majorEntity;
            }
            et.setProductPackagingText(odooProductPackging.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductType(odooProduct.getType());
            et.setHasTracking(odooProduct.getTracking());
            et.setProductIdText(odooProduct.getName());
            et.setProductTmplId(odooProduct.getProductTmplId());
        }
        //实体关系[DER1N_STOCK_MOVE__PURCHASE_ORDER_LINE__CREATED_PURCHASE_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getCreatedPurchaseLineId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line odooCreatedPurchaseLine=et.getOdooCreatedPurchaseLine();
            if(ObjectUtils.isEmpty(odooCreatedPurchaseLine)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line majorEntity=purchaseOrderLineService.get(et.getCreatedPurchaseLineId());
                et.setOdooCreatedPurchaseLine(majorEntity);
                odooCreatedPurchaseLine=majorEntity;
            }
            et.setCreatedPurchaseLineIdText(odooCreatedPurchaseLine.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__PURCHASE_ORDER_LINE__PURCHASE_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getPurchaseLineId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line odooPurchaseLine=et.getOdooPurchaseLine();
            if(ObjectUtils.isEmpty(odooPurchaseLine)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line majorEntity=purchaseOrderLineService.get(et.getPurchaseLineId());
                et.setOdooPurchaseLine(majorEntity);
                odooPurchaseLine=majorEntity;
            }
            et.setPurchaseLineIdText(odooPurchaseLine.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__REPAIR_ORDER__REPAIR_ID]
        if(!ObjectUtils.isEmpty(et.getRepairId())){
            cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order odooRepair=et.getOdooRepair();
            if(ObjectUtils.isEmpty(odooRepair)){
                cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order majorEntity=repairOrderService.get(et.getRepairId());
                et.setOdooRepair(majorEntity);
                odooRepair=majorEntity;
            }
            et.setRepairIdText(odooRepair.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__RES_PARTNER__RESTRICT_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getRestrictPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooRestrictPartner=et.getOdooRestrictPartner();
            if(ObjectUtils.isEmpty(odooRestrictPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getRestrictPartnerId());
                et.setOdooRestrictPartner(majorEntity);
                odooRestrictPartner=majorEntity;
            }
            et.setRestrictPartnerIdText(odooRestrictPartner.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__SALE_ORDER_LINE__SALE_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getSaleLineId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line odooSaleLine=et.getOdooSaleLine();
            if(ObjectUtils.isEmpty(odooSaleLine)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line majorEntity=saleOrderLineService.get(et.getSaleLineId());
                et.setOdooSaleLine(majorEntity);
                odooSaleLine=majorEntity;
            }
            et.setSaleLineIdText(odooSaleLine.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__STOCK_INVENTORY__INVENTORY_ID]
        if(!ObjectUtils.isEmpty(et.getInventoryId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory odooInventory=et.getOdooInventory();
            if(ObjectUtils.isEmpty(odooInventory)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory majorEntity=stockInventoryService.get(et.getInventoryId());
                et.setOdooInventory(majorEntity);
                odooInventory=majorEntity;
            }
            et.setInventoryIdText(odooInventory.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__STOCK_LOCATION__LOCATION_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getLocationDestId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest=et.getOdooLocationDest();
            if(ObjectUtils.isEmpty(odooLocationDest)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationDestId());
                et.setOdooLocationDest(majorEntity);
                odooLocationDest=majorEntity;
            }
            et.setScrapped(odooLocationDest.getScrapLocation());
            et.setLocationDestIdText(odooLocationDest.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__STOCK_MOVE__ORIGIN_RETURNED_MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getOriginReturnedMoveId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooOriginReturnedMove=et.getOdooOriginReturnedMove();
            if(ObjectUtils.isEmpty(odooOriginReturnedMove)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move majorEntity=stockMoveService.get(et.getOriginReturnedMoveId());
                et.setOdooOriginReturnedMove(majorEntity);
                odooOriginReturnedMove=majorEntity;
            }
            et.setOriginReturnedMoveIdText(odooOriginReturnedMove.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__STOCK_PICKING_TYPE__PICKING_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPickingTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickingType=et.getOdooPickingType();
            if(ObjectUtils.isEmpty(odooPickingType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getPickingTypeId());
                et.setOdooPickingType(majorEntity);
                odooPickingType=majorEntity;
            }
            et.setShowOperations(odooPickingType.getShowOperations());
            et.setPickingTypeIdText(odooPickingType.getName());
            et.setPickingCode(odooPickingType.getCode());
            et.setPickingTypeEntirePacks(odooPickingType.getShowEntirePacks());
        }
        //实体关系[DER1N_STOCK_MOVE__STOCK_PICKING__PICKING_ID]
        if(!ObjectUtils.isEmpty(et.getPickingId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooPicking=et.getOdooPicking();
            if(ObjectUtils.isEmpty(odooPicking)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking majorEntity=stockPickingService.get(et.getPickingId());
                et.setOdooPicking(majorEntity);
                odooPicking=majorEntity;
            }
            et.setPickingPartnerId(odooPicking.getPartnerId());
            et.setBackorderId(odooPicking.getBackorderId());
            et.setPickingIdText(odooPicking.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__STOCK_RULE__RULE_ID]
        if(!ObjectUtils.isEmpty(et.getRuleId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooRule=et.getOdooRule();
            if(ObjectUtils.isEmpty(odooRule)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule majorEntity=stockRuleService.get(et.getRuleId());
                et.setOdooRule(majorEntity);
                odooRule=majorEntity;
            }
            et.setRuleIdText(odooRule.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__STOCK_WAREHOUSE__WAREHOUSE_ID]
        if(!ObjectUtils.isEmpty(et.getWarehouseId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse=et.getOdooWarehouse();
            if(ObjectUtils.isEmpty(odooWarehouse)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getWarehouseId());
                et.setOdooWarehouse(majorEntity);
                odooWarehouse=majorEntity;
            }
            et.setWarehouseIdText(odooWarehouse.getName());
        }
        //实体关系[DER1N_STOCK_MOVE__UOM_UOM__PRODUCT_UOM]
        if(!ObjectUtils.isEmpty(et.getProductUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUom());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_move> getStockMoveByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_move> getStockMoveByEntities(List<Stock_move> entities) {
        List ids =new ArrayList();
        for(Stock_move entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



