package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_template;
/**
 * 关系型数据实体[Account_tax_template] 查询条件对象
 */
@Slf4j
@Data
public class Account_tax_templateSearchContext extends QueryWrapperContext<Account_tax_template> {

	private String n_tax_exigibility_eq;//[应有税金]
	public void setN_tax_exigibility_eq(String n_tax_exigibility_eq) {
        this.n_tax_exigibility_eq = n_tax_exigibility_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_exigibility_eq)){
            this.getSearchCond().eq("tax_exigibility", n_tax_exigibility_eq);
        }
    }
	private String n_name_like;//[税率名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_amount_type_eq;//[税率计算]
	public void setN_amount_type_eq(String n_amount_type_eq) {
        this.n_amount_type_eq = n_amount_type_eq;
        if(!ObjectUtils.isEmpty(this.n_amount_type_eq)){
            this.getSearchCond().eq("amount_type", n_amount_type_eq);
        }
    }
	private String n_type_tax_use_eq;//[税范围]
	public void setN_type_tax_use_eq(String n_type_tax_use_eq) {
        this.n_type_tax_use_eq = n_type_tax_use_eq;
        if(!ObjectUtils.isEmpty(this.n_type_tax_use_eq)){
            this.getSearchCond().eq("type_tax_use", n_type_tax_use_eq);
        }
    }
	private String n_cash_basis_account_id_text_eq;//[税应收科目]
	public void setN_cash_basis_account_id_text_eq(String n_cash_basis_account_id_text_eq) {
        this.n_cash_basis_account_id_text_eq = n_cash_basis_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_cash_basis_account_id_text_eq)){
            this.getSearchCond().eq("cash_basis_account_id_text", n_cash_basis_account_id_text_eq);
        }
    }
	private String n_cash_basis_account_id_text_like;//[税应收科目]
	public void setN_cash_basis_account_id_text_like(String n_cash_basis_account_id_text_like) {
        this.n_cash_basis_account_id_text_like = n_cash_basis_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_cash_basis_account_id_text_like)){
            this.getSearchCond().like("cash_basis_account_id_text", n_cash_basis_account_id_text_like);
        }
    }
	private String n_chart_template_id_text_eq;//[表模板]
	public void setN_chart_template_id_text_eq(String n_chart_template_id_text_eq) {
        this.n_chart_template_id_text_eq = n_chart_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_text_eq)){
            this.getSearchCond().eq("chart_template_id_text", n_chart_template_id_text_eq);
        }
    }
	private String n_chart_template_id_text_like;//[表模板]
	public void setN_chart_template_id_text_like(String n_chart_template_id_text_like) {
        this.n_chart_template_id_text_like = n_chart_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_text_like)){
            this.getSearchCond().like("chart_template_id_text", n_chart_template_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_cash_basis_base_account_id_text_eq;//[基本税应收科目]
	public void setN_cash_basis_base_account_id_text_eq(String n_cash_basis_base_account_id_text_eq) {
        this.n_cash_basis_base_account_id_text_eq = n_cash_basis_base_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_cash_basis_base_account_id_text_eq)){
            this.getSearchCond().eq("cash_basis_base_account_id_text", n_cash_basis_base_account_id_text_eq);
        }
    }
	private String n_cash_basis_base_account_id_text_like;//[基本税应收科目]
	public void setN_cash_basis_base_account_id_text_like(String n_cash_basis_base_account_id_text_like) {
        this.n_cash_basis_base_account_id_text_like = n_cash_basis_base_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_cash_basis_base_account_id_text_like)){
            this.getSearchCond().like("cash_basis_base_account_id_text", n_cash_basis_base_account_id_text_like);
        }
    }
	private String n_tax_group_id_text_eq;//[税组]
	public void setN_tax_group_id_text_eq(String n_tax_group_id_text_eq) {
        this.n_tax_group_id_text_eq = n_tax_group_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_group_id_text_eq)){
            this.getSearchCond().eq("tax_group_id_text", n_tax_group_id_text_eq);
        }
    }
	private String n_tax_group_id_text_like;//[税组]
	public void setN_tax_group_id_text_like(String n_tax_group_id_text_like) {
        this.n_tax_group_id_text_like = n_tax_group_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_tax_group_id_text_like)){
            this.getSearchCond().like("tax_group_id_text", n_tax_group_id_text_like);
        }
    }
	private String n_account_id_text_eq;//[税率科目]
	public void setN_account_id_text_eq(String n_account_id_text_eq) {
        this.n_account_id_text_eq = n_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_eq)){
            this.getSearchCond().eq("account_id_text", n_account_id_text_eq);
        }
    }
	private String n_account_id_text_like;//[税率科目]
	public void setN_account_id_text_like(String n_account_id_text_like) {
        this.n_account_id_text_like = n_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_like)){
            this.getSearchCond().like("account_id_text", n_account_id_text_like);
        }
    }
	private String n_refund_account_id_text_eq;//[退款的税金科目]
	public void setN_refund_account_id_text_eq(String n_refund_account_id_text_eq) {
        this.n_refund_account_id_text_eq = n_refund_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_refund_account_id_text_eq)){
            this.getSearchCond().eq("refund_account_id_text", n_refund_account_id_text_eq);
        }
    }
	private String n_refund_account_id_text_like;//[退款的税金科目]
	public void setN_refund_account_id_text_like(String n_refund_account_id_text_like) {
        this.n_refund_account_id_text_like = n_refund_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_refund_account_id_text_like)){
            this.getSearchCond().like("refund_account_id_text", n_refund_account_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_chart_template_id_eq;//[表模板]
	public void setN_chart_template_id_eq(Long n_chart_template_id_eq) {
        this.n_chart_template_id_eq = n_chart_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_eq)){
            this.getSearchCond().eq("chart_template_id", n_chart_template_id_eq);
        }
    }
	private Long n_refund_account_id_eq;//[退款的税金科目]
	public void setN_refund_account_id_eq(Long n_refund_account_id_eq) {
        this.n_refund_account_id_eq = n_refund_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_refund_account_id_eq)){
            this.getSearchCond().eq("refund_account_id", n_refund_account_id_eq);
        }
    }
	private Long n_cash_basis_account_id_eq;//[税应收科目]
	public void setN_cash_basis_account_id_eq(Long n_cash_basis_account_id_eq) {
        this.n_cash_basis_account_id_eq = n_cash_basis_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_cash_basis_account_id_eq)){
            this.getSearchCond().eq("cash_basis_account_id", n_cash_basis_account_id_eq);
        }
    }
	private Long n_account_id_eq;//[税率科目]
	public void setN_account_id_eq(Long n_account_id_eq) {
        this.n_account_id_eq = n_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_eq)){
            this.getSearchCond().eq("account_id", n_account_id_eq);
        }
    }
	private Long n_cash_basis_base_account_id_eq;//[基本税应收科目]
	public void setN_cash_basis_base_account_id_eq(Long n_cash_basis_base_account_id_eq) {
        this.n_cash_basis_base_account_id_eq = n_cash_basis_base_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_cash_basis_base_account_id_eq)){
            this.getSearchCond().eq("cash_basis_base_account_id", n_cash_basis_base_account_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_tax_group_id_eq;//[税组]
	public void setN_tax_group_id_eq(Long n_tax_group_id_eq) {
        this.n_tax_group_id_eq = n_tax_group_id_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_group_id_eq)){
            this.getSearchCond().eq("tax_group_id", n_tax_group_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



