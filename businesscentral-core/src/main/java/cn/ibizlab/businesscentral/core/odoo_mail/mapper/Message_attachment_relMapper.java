package cn.ibizlab.businesscentral.core.odoo_mail.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Message_attachment_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Message_attachment_relSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Message_attachment_relMapper extends BaseMapper<Message_attachment_rel>{

    Page<Message_attachment_rel> searchDefault(IPage page, @Param("srf") Message_attachment_relSearchContext context, @Param("ew") Wrapper<Message_attachment_rel> wrapper) ;
    @Override
    Message_attachment_rel selectById(Serializable id);
    @Override
    int insert(Message_attachment_rel entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Message_attachment_rel entity);
    @Override
    int update(@Param(Constants.ENTITY) Message_attachment_rel entity, @Param("ew") Wrapper<Message_attachment_rel> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Message_attachment_rel> selectByAttachmentId(@Param("id") Serializable id) ;

    List<Message_attachment_rel> selectByMessageId(@Param("id") Serializable id) ;


}
