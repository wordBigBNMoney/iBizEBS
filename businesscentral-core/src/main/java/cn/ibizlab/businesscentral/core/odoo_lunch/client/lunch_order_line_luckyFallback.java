package cn.ibizlab.businesscentral.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[lunch_order_line_lucky] 服务对象接口
 */
@Component
public class lunch_order_line_luckyFallback implements lunch_order_line_luckyFeignClient{

    public Page<Lunch_order_line_lucky> search(Lunch_order_line_luckySearchContext context){
            return null;
     }


    public Lunch_order_line_lucky get(Long id){
            return null;
     }




    public Lunch_order_line_lucky create(Lunch_order_line_lucky lunch_order_line_lucky){
            return null;
     }
    public Boolean createBatch(List<Lunch_order_line_lucky> lunch_order_line_luckies){
            return false;
     }

    public Lunch_order_line_lucky update(Long id, Lunch_order_line_lucky lunch_order_line_lucky){
            return null;
     }
    public Boolean updateBatch(List<Lunch_order_line_lucky> lunch_order_line_luckies){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Lunch_order_line_lucky> select(){
            return null;
     }

    public Lunch_order_line_lucky getDraft(){
            return null;
    }



    public Boolean checkKey(Lunch_order_line_lucky lunch_order_line_lucky){
            return false;
     }


    public Boolean save(Lunch_order_line_lucky lunch_order_line_lucky){
            return false;
     }
    public Boolean saveBatch(List<Lunch_order_line_lucky> lunch_order_line_luckies){
            return false;
     }

    public Page<Lunch_order_line_lucky> searchDefault(Lunch_order_line_luckySearchContext context){
            return null;
     }


}
