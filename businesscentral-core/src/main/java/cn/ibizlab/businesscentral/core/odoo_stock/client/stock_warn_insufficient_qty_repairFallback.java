package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qty_repairSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_warn_insufficient_qty_repair] 服务对象接口
 */
@Component
public class stock_warn_insufficient_qty_repairFallback implements stock_warn_insufficient_qty_repairFeignClient{

    public Stock_warn_insufficient_qty_repair get(Long id){
            return null;
     }


    public Stock_warn_insufficient_qty_repair create(Stock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
            return null;
     }
    public Boolean createBatch(List<Stock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
            return false;
     }

    public Page<Stock_warn_insufficient_qty_repair> search(Stock_warn_insufficient_qty_repairSearchContext context){
            return null;
     }





    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Stock_warn_insufficient_qty_repair update(Long id, Stock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
            return null;
     }
    public Boolean updateBatch(List<Stock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
            return false;
     }


    public Page<Stock_warn_insufficient_qty_repair> select(){
            return null;
     }

    public Stock_warn_insufficient_qty_repair getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
            return false;
     }


    public Boolean save(Stock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair){
            return false;
     }
    public Boolean saveBatch(List<Stock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs){
            return false;
     }

    public Page<Stock_warn_insufficient_qty_repair> searchDefault(Stock_warn_insufficient_qty_repairSearchContext context){
            return null;
     }


}
