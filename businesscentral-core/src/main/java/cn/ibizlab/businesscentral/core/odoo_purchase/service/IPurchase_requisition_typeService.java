package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_type;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisition_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Purchase_requisition_type] 服务对象接口
 */
public interface IPurchase_requisition_typeService extends IService<Purchase_requisition_type>{

    boolean create(Purchase_requisition_type et) ;
    void createBatch(List<Purchase_requisition_type> list) ;
    boolean update(Purchase_requisition_type et) ;
    void updateBatch(List<Purchase_requisition_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Purchase_requisition_type get(Long key) ;
    Purchase_requisition_type getDraft(Purchase_requisition_type et) ;
    boolean checkKey(Purchase_requisition_type et) ;
    boolean save(Purchase_requisition_type et) ;
    void saveBatch(List<Purchase_requisition_type> list) ;
    Page<Purchase_requisition_type> searchDefault(Purchase_requisition_typeSearchContext context) ;
    List<Purchase_requisition_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Purchase_requisition_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


