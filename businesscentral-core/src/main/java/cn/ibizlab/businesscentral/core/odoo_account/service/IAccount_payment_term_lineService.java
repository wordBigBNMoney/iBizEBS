package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_payment_term_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_payment_term_line] 服务对象接口
 */
public interface IAccount_payment_term_lineService extends IService<Account_payment_term_line>{

    boolean create(Account_payment_term_line et) ;
    void createBatch(List<Account_payment_term_line> list) ;
    boolean update(Account_payment_term_line et) ;
    void updateBatch(List<Account_payment_term_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_payment_term_line get(Long key) ;
    Account_payment_term_line getDraft(Account_payment_term_line et) ;
    boolean checkKey(Account_payment_term_line et) ;
    boolean save(Account_payment_term_line et) ;
    void saveBatch(List<Account_payment_term_line> list) ;
    Page<Account_payment_term_line> searchDefault(Account_payment_term_lineSearchContext context) ;
    List<Account_payment_term_line> selectByPaymentId(Long id);
    void removeByPaymentId(Collection<Long> ids);
    void removeByPaymentId(Long id);
    List<Account_payment_term_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_payment_term_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_payment_term_line> getAccountPaymentTermLineByIds(List<Long> ids) ;
    List<Account_payment_term_line> getAccountPaymentTermLineByEntities(List<Account_payment_term_line> entities) ;
}


