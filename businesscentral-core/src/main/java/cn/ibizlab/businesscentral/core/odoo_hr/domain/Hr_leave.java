package cn.ibizlab.businesscentral.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[休假]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "HR_LEAVE",resultMap = "Hr_leaveResultMap")
public class Hr_leave extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分配模式
     */
    @DEField(name = "holiday_type")
    @TableField(value = "holiday_type")
    @JSONField(name = "holiday_type")
    @JsonProperty("holiday_type")
    private String holidayType;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 请假开始日期
     */
    @DEField(name = "request_date_from")
    @TableField(value = "request_date_from")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "request_date_from" , format="yyyy-MM-dd")
    @JsonProperty("request_date_from")
    private Timestamp requestDateFrom;
    /**
     * 理由
     */
    @TableField(value = "notes")
    @JSONField(name = "notes")
    @JsonProperty("notes")
    private String notes;
    /**
     * 持续时间（天）
     */
    @DEField(name = "number_of_days")
    @TableField(value = "number_of_days")
    @JSONField(name = "number_of_days")
    @JsonProperty("number_of_days")
    private Double numberOfDays;
    /**
     * 请求结束日期
     */
    @DEField(name = "request_date_to")
    @TableField(value = "request_date_to")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "request_date_to" , format="yyyy-MM-dd")
    @JsonProperty("request_date_to")
    private Timestamp requestDateTo;
    /**
     * 半天
     */
    @DEField(name = "request_unit_half")
    @TableField(value = "request_unit_half")
    @JSONField(name = "request_unit_half")
    @JsonProperty("request_unit_half")
    private Boolean requestUnitHalf;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 持续时间（天）
     */
    @TableField(exist = false)
    @JSONField(name = "number_of_days_display")
    @JsonProperty("number_of_days_display")
    private Double numberOfDaysDisplay;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 时间从
     */
    @DEField(name = "request_hour_from")
    @TableField(value = "request_hour_from")
    @JSONField(name = "request_hour_from")
    @JsonProperty("request_hour_from")
    private String requestHourFrom;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 反映在最近的工资单中
     */
    @DEField(name = "payslip_status")
    @TableField(value = "payslip_status")
    @JSONField(name = "payslip_status")
    @JsonProperty("payslip_status")
    private Boolean payslipStatus;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 自定义时间
     */
    @DEField(name = "request_unit_hours")
    @TableField(value = "request_unit_hours")
    @JSONField(name = "request_unit_hours")
    @JsonProperty("request_unit_hours")
    private Boolean requestUnitHours;
    /**
     * 结束日期
     */
    @DEField(name = "date_to")
    @TableField(value = "date_to")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_to")
    private Timestamp dateTo;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 信息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 能重置
     */
    @TableField(exist = false)
    @JSONField(name = "can_reset")
    @JsonProperty("can_reset")
    private Boolean canReset;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 持续时间（小时）
     */
    @TableField(exist = false)
    @JSONField(name = "number_of_hours_display")
    @JsonProperty("number_of_hours_display")
    private Double numberOfHoursDisplay;
    /**
     * 长达数天的定制时间
     */
    @DEField(name = "request_unit_custom")
    @TableField(value = "request_unit_custom")
    @JSONField(name = "request_unit_custom")
    @JsonProperty("request_unit_custom")
    private Boolean requestUnitCustom;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * HR 备注
     */
    @DEField(name = "report_note")
    @TableField(value = "report_note")
    @JSONField(name = "report_note")
    @JsonProperty("report_note")
    private String reportNote;
    /**
     * 时间到
     */
    @DEField(name = "request_hour_to")
    @TableField(value = "request_hour_to")
    @JSONField(name = "request_hour_to")
    @JsonProperty("request_hour_to")
    private String requestHourTo;
    /**
     * 开始日期
     */
    @DEField(name = "date_from")
    @TableField(value = "date_from")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_from")
    private Timestamp dateFrom;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 链接申请
     */
    @TableField(exist = false)
    @JSONField(name = "linked_request_ids")
    @JsonProperty("linked_request_ids")
    private String linkedRequestIds;
    /**
     * 能批准
     */
    @TableField(exist = false)
    @JSONField(name = "can_approve")
    @JsonProperty("can_approve")
    private Boolean canApprove;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 日期开始
     */
    @DEField(name = "request_date_from_period")
    @TableField(value = "request_date_from_period")
    @JSONField(name = "request_date_from_period")
    @JsonProperty("request_date_from_period")
    private String requestDateFromPeriod;
    /**
     * 说明
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 要求的（天/小时）
     */
    @TableField(exist = false)
    @JSONField(name = "duration_display")
    @JsonProperty("duration_display")
    private String durationDisplay;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "mode_company_id_text")
    @JsonProperty("mode_company_id_text")
    private String modeCompanyIdText;
    /**
     * 休假
     */
    @TableField(exist = false)
    @JSONField(name = "leave_type_request_unit")
    @JsonProperty("leave_type_request_unit")
    private String leaveTypeRequestUnit;
    /**
     * 员工
     */
    @TableField(exist = false)
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 会议
     */
    @TableField(exist = false)
    @JSONField(name = "meeting_id_text")
    @JsonProperty("meeting_id_text")
    private String meetingIdText;
    /**
     * 第二次审批
     */
    @TableField(exist = false)
    @JSONField(name = "second_approver_id_text")
    @JsonProperty("second_approver_id_text")
    private String secondApproverIdText;
    /**
     * 员工标签
     */
    @TableField(exist = false)
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;
    /**
     * 用户
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 首次审批
     */
    @TableField(exist = false)
    @JSONField(name = "first_approver_id_text")
    @JsonProperty("first_approver_id_text")
    private String firstApproverIdText;
    /**
     * 休假类型
     */
    @TableField(exist = false)
    @JSONField(name = "holiday_status_id_text")
    @JsonProperty("holiday_status_id_text")
    private String holidayStatusIdText;
    /**
     * 经理
     */
    @TableField(exist = false)
    @JSONField(name = "manager_id_text")
    @JsonProperty("manager_id_text")
    private String managerIdText;
    /**
     * 上级
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 验证人
     */
    @TableField(exist = false)
    @JSONField(name = "validation_type")
    @JsonProperty("validation_type")
    private String validationType;
    /**
     * 部门
     */
    @TableField(exist = false)
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;
    /**
     * 经理
     */
    @DEField(name = "manager_id")
    @TableField(value = "manager_id")
    @JSONField(name = "manager_id")
    @JsonProperty("manager_id")
    private Long managerId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 用户
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 上级
     */
    @DEField(name = "parent_id")
    @TableField(value = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 公司
     */
    @DEField(name = "mode_company_id")
    @TableField(value = "mode_company_id")
    @JSONField(name = "mode_company_id")
    @JsonProperty("mode_company_id")
    private Long modeCompanyId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 第二次审批
     */
    @DEField(name = "second_approver_id")
    @TableField(value = "second_approver_id")
    @JSONField(name = "second_approver_id")
    @JsonProperty("second_approver_id")
    private Long secondApproverId;
    /**
     * 会议
     */
    @DEField(name = "meeting_id")
    @TableField(value = "meeting_id")
    @JSONField(name = "meeting_id")
    @JsonProperty("meeting_id")
    private Long meetingId;
    /**
     * 休假类型
     */
    @DEField(name = "holiday_status_id")
    @TableField(value = "holiday_status_id")
    @JSONField(name = "holiday_status_id")
    @JsonProperty("holiday_status_id")
    private Long holidayStatusId;
    /**
     * 部门
     */
    @DEField(name = "department_id")
    @TableField(value = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Long departmentId;
    /**
     * 员工
     */
    @DEField(name = "employee_id")
    @TableField(value = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Long employeeId;
    /**
     * 员工标签
     */
    @DEField(name = "category_id")
    @TableField(value = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Long categoryId;
    /**
     * 首次审批
     */
    @DEField(name = "first_approver_id")
    @TableField(value = "first_approver_id")
    @JSONField(name = "first_approver_id")
    @JsonProperty("first_approver_id")
    private Long firstApproverId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event odooMeeting;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee_category odooCategory;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooFirstApprover;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooManager;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooSecondApprover;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_type odooHolidayStatus;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave odooParent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooModeCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [分配模式]
     */
    public void setHolidayType(String holidayType){
        this.holidayType = holidayType ;
        this.modify("holiday_type",holidayType);
    }

    /**
     * 设置 [请假开始日期]
     */
    public void setRequestDateFrom(Timestamp requestDateFrom){
        this.requestDateFrom = requestDateFrom ;
        this.modify("request_date_from",requestDateFrom);
    }

    /**
     * 格式化日期 [请假开始日期]
     */
    public String formatRequestDateFrom(){
        if (this.requestDateFrom == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(requestDateFrom);
    }
    /**
     * 设置 [理由]
     */
    public void setNotes(String notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [持续时间（天）]
     */
    public void setNumberOfDays(Double numberOfDays){
        this.numberOfDays = numberOfDays ;
        this.modify("number_of_days",numberOfDays);
    }

    /**
     * 设置 [请求结束日期]
     */
    public void setRequestDateTo(Timestamp requestDateTo){
        this.requestDateTo = requestDateTo ;
        this.modify("request_date_to",requestDateTo);
    }

    /**
     * 格式化日期 [请求结束日期]
     */
    public String formatRequestDateTo(){
        if (this.requestDateTo == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(requestDateTo);
    }
    /**
     * 设置 [半天]
     */
    public void setRequestUnitHalf(Boolean requestUnitHalf){
        this.requestUnitHalf = requestUnitHalf ;
        this.modify("request_unit_half",requestUnitHalf);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [时间从]
     */
    public void setRequestHourFrom(String requestHourFrom){
        this.requestHourFrom = requestHourFrom ;
        this.modify("request_hour_from",requestHourFrom);
    }

    /**
     * 设置 [反映在最近的工资单中]
     */
    public void setPayslipStatus(Boolean payslipStatus){
        this.payslipStatus = payslipStatus ;
        this.modify("payslip_status",payslipStatus);
    }

    /**
     * 设置 [自定义时间]
     */
    public void setRequestUnitHours(Boolean requestUnitHours){
        this.requestUnitHours = requestUnitHours ;
        this.modify("request_unit_hours",requestUnitHours);
    }

    /**
     * 设置 [结束日期]
     */
    public void setDateTo(Timestamp dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatDateTo(){
        if (this.dateTo == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateTo);
    }
    /**
     * 设置 [长达数天的定制时间]
     */
    public void setRequestUnitCustom(Boolean requestUnitCustom){
        this.requestUnitCustom = requestUnitCustom ;
        this.modify("request_unit_custom",requestUnitCustom);
    }

    /**
     * 设置 [HR 备注]
     */
    public void setReportNote(String reportNote){
        this.reportNote = reportNote ;
        this.modify("report_note",reportNote);
    }

    /**
     * 设置 [时间到]
     */
    public void setRequestHourTo(String requestHourTo){
        this.requestHourTo = requestHourTo ;
        this.modify("request_hour_to",requestHourTo);
    }

    /**
     * 设置 [开始日期]
     */
    public void setDateFrom(Timestamp dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatDateFrom(){
        if (this.dateFrom == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateFrom);
    }
    /**
     * 设置 [日期开始]
     */
    public void setRequestDateFromPeriod(String requestDateFromPeriod){
        this.requestDateFromPeriod = requestDateFromPeriod ;
        this.modify("request_date_from_period",requestDateFromPeriod);
    }

    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [经理]
     */
    public void setManagerId(Long managerId){
        this.managerId = managerId ;
        this.modify("manager_id",managerId);
    }

    /**
     * 设置 [用户]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [上级]
     */
    public void setParentId(Long parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [公司]
     */
    public void setModeCompanyId(Long modeCompanyId){
        this.modeCompanyId = modeCompanyId ;
        this.modify("mode_company_id",modeCompanyId);
    }

    /**
     * 设置 [第二次审批]
     */
    public void setSecondApproverId(Long secondApproverId){
        this.secondApproverId = secondApproverId ;
        this.modify("second_approver_id",secondApproverId);
    }

    /**
     * 设置 [会议]
     */
    public void setMeetingId(Long meetingId){
        this.meetingId = meetingId ;
        this.modify("meeting_id",meetingId);
    }

    /**
     * 设置 [休假类型]
     */
    public void setHolidayStatusId(Long holidayStatusId){
        this.holidayStatusId = holidayStatusId ;
        this.modify("holiday_status_id",holidayStatusId);
    }

    /**
     * 设置 [部门]
     */
    public void setDepartmentId(Long departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }

    /**
     * 设置 [员工]
     */
    public void setEmployeeId(Long employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [员工标签]
     */
    public void setCategoryId(Long categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

    /**
     * 设置 [首次审批]
     */
    public void setFirstApproverId(Long firstApproverId){
        this.firstApproverId = firstApproverId ;
        this.modify("first_approver_id",firstApproverId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


