package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_taskSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_task] 服务对象接口
 */
@Component
public class mro_taskFallback implements mro_taskFeignClient{



    public Page<Mro_task> search(Mro_taskSearchContext context){
            return null;
     }


    public Mro_task get(Long id){
            return null;
     }


    public Mro_task create(Mro_task mro_task){
            return null;
     }
    public Boolean createBatch(List<Mro_task> mro_tasks){
            return false;
     }

    public Mro_task update(Long id, Mro_task mro_task){
            return null;
     }
    public Boolean updateBatch(List<Mro_task> mro_tasks){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Mro_task> select(){
            return null;
     }

    public Mro_task getDraft(){
            return null;
    }



    public Boolean checkKey(Mro_task mro_task){
            return false;
     }


    public Boolean save(Mro_task mro_task){
            return false;
     }
    public Boolean saveBatch(List<Mro_task> mro_tasks){
            return false;
     }

    public Page<Mro_task> searchDefault(Mro_taskSearchContext context){
            return null;
     }


}
