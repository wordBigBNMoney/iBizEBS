package cn.ibizlab.businesscentral.core.odoo_purchase.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Purchase_order_lineMapper extends BaseMapper<Purchase_order_line>{

    Page<HashMap> searchCalc_order_amount(IPage page, @Param("srf") Purchase_order_lineSearchContext context, @Param("ew") Wrapper<Purchase_order_line> wrapper) ;
    Page<Purchase_order_line> searchDefault(IPage page, @Param("srf") Purchase_order_lineSearchContext context, @Param("ew") Wrapper<Purchase_order_line> wrapper) ;
    @Override
    Purchase_order_line selectById(Serializable id);
    @Override
    int insert(Purchase_order_line entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Purchase_order_line entity);
    @Override
    int update(@Param(Constants.ENTITY) Purchase_order_line entity, @Param("ew") Wrapper<Purchase_order_line> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Purchase_order_line> selectByAccountAnalyticId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByProductId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByOrderId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByCompanyId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByPartnerId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByCreateUid(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByWriteUid(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectBySaleLineId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectBySaleOrderId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByOrderpointId(@Param("id") Serializable id) ;

    List<Purchase_order_line> selectByProductUom(@Param("id") Serializable id) ;


        
    boolean saveRelByTaxesId(@Param("purchase_order_line_id") Long purchase_order_line_id, List<cn.ibizlab.businesscentral.util.domain.MultiSelectItem> account_taxes);

}
