package cn.ibizlab.businesscentral.core.odoo_stock.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse;
/**
 * 关系型数据实体[Stock_warehouse] 查询条件对象
 */
@Slf4j
@Data
public class Stock_warehouseSearchContext extends QueryWrapperContext<Stock_warehouse> {

	private String n_name_like;//[仓库]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_delivery_steps_eq;//[出向运输]
	public void setN_delivery_steps_eq(String n_delivery_steps_eq) {
        this.n_delivery_steps_eq = n_delivery_steps_eq;
        if(!ObjectUtils.isEmpty(this.n_delivery_steps_eq)){
            this.getSearchCond().eq("delivery_steps", n_delivery_steps_eq);
        }
    }
	private String n_reception_steps_eq;//[入库]
	public void setN_reception_steps_eq(String n_reception_steps_eq) {
        this.n_reception_steps_eq = n_reception_steps_eq;
        if(!ObjectUtils.isEmpty(this.n_reception_steps_eq)){
            this.getSearchCond().eq("reception_steps", n_reception_steps_eq);
        }
    }
	private String n_manufacture_steps_eq;//[制造]
	public void setN_manufacture_steps_eq(String n_manufacture_steps_eq) {
        this.n_manufacture_steps_eq = n_manufacture_steps_eq;
        if(!ObjectUtils.isEmpty(this.n_manufacture_steps_eq)){
            this.getSearchCond().eq("manufacture_steps", n_manufacture_steps_eq);
        }
    }
	private String n_view_location_id_text_eq;//[视图位置]
	public void setN_view_location_id_text_eq(String n_view_location_id_text_eq) {
        this.n_view_location_id_text_eq = n_view_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_view_location_id_text_eq)){
            this.getSearchCond().eq("view_location_id_text", n_view_location_id_text_eq);
        }
    }
	private String n_view_location_id_text_like;//[视图位置]
	public void setN_view_location_id_text_like(String n_view_location_id_text_like) {
        this.n_view_location_id_text_like = n_view_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_view_location_id_text_like)){
            this.getSearchCond().like("view_location_id_text", n_view_location_id_text_like);
        }
    }
	private String n_wh_input_stock_loc_id_text_eq;//[进货位置]
	public void setN_wh_input_stock_loc_id_text_eq(String n_wh_input_stock_loc_id_text_eq) {
        this.n_wh_input_stock_loc_id_text_eq = n_wh_input_stock_loc_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_wh_input_stock_loc_id_text_eq)){
            this.getSearchCond().eq("wh_input_stock_loc_id_text", n_wh_input_stock_loc_id_text_eq);
        }
    }
	private String n_wh_input_stock_loc_id_text_like;//[进货位置]
	public void setN_wh_input_stock_loc_id_text_like(String n_wh_input_stock_loc_id_text_like) {
        this.n_wh_input_stock_loc_id_text_like = n_wh_input_stock_loc_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_wh_input_stock_loc_id_text_like)){
            this.getSearchCond().like("wh_input_stock_loc_id_text", n_wh_input_stock_loc_id_text_like);
        }
    }
	private String n_sam_loc_id_text_eq;//[制造地点后的库存]
	public void setN_sam_loc_id_text_eq(String n_sam_loc_id_text_eq) {
        this.n_sam_loc_id_text_eq = n_sam_loc_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sam_loc_id_text_eq)){
            this.getSearchCond().eq("sam_loc_id_text", n_sam_loc_id_text_eq);
        }
    }
	private String n_sam_loc_id_text_like;//[制造地点后的库存]
	public void setN_sam_loc_id_text_like(String n_sam_loc_id_text_like) {
        this.n_sam_loc_id_text_like = n_sam_loc_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sam_loc_id_text_like)){
            this.getSearchCond().like("sam_loc_id_text", n_sam_loc_id_text_like);
        }
    }
	private String n_crossdock_route_id_text_eq;//[越库路线]
	public void setN_crossdock_route_id_text_eq(String n_crossdock_route_id_text_eq) {
        this.n_crossdock_route_id_text_eq = n_crossdock_route_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_crossdock_route_id_text_eq)){
            this.getSearchCond().eq("crossdock_route_id_text", n_crossdock_route_id_text_eq);
        }
    }
	private String n_crossdock_route_id_text_like;//[越库路线]
	public void setN_crossdock_route_id_text_like(String n_crossdock_route_id_text_like) {
        this.n_crossdock_route_id_text_like = n_crossdock_route_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_crossdock_route_id_text_like)){
            this.getSearchCond().like("crossdock_route_id_text", n_crossdock_route_id_text_like);
        }
    }
	private String n_pbm_route_id_text_eq;//[在制造路线前拣货]
	public void setN_pbm_route_id_text_eq(String n_pbm_route_id_text_eq) {
        this.n_pbm_route_id_text_eq = n_pbm_route_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pbm_route_id_text_eq)){
            this.getSearchCond().eq("pbm_route_id_text", n_pbm_route_id_text_eq);
        }
    }
	private String n_pbm_route_id_text_like;//[在制造路线前拣货]
	public void setN_pbm_route_id_text_like(String n_pbm_route_id_text_like) {
        this.n_pbm_route_id_text_like = n_pbm_route_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pbm_route_id_text_like)){
            this.getSearchCond().like("pbm_route_id_text", n_pbm_route_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_pbm_type_id_text_eq;//[在制造作业类型前拣货]
	public void setN_pbm_type_id_text_eq(String n_pbm_type_id_text_eq) {
        this.n_pbm_type_id_text_eq = n_pbm_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pbm_type_id_text_eq)){
            this.getSearchCond().eq("pbm_type_id_text", n_pbm_type_id_text_eq);
        }
    }
	private String n_pbm_type_id_text_like;//[在制造作业类型前拣货]
	public void setN_pbm_type_id_text_like(String n_pbm_type_id_text_like) {
        this.n_pbm_type_id_text_like = n_pbm_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pbm_type_id_text_like)){
            this.getSearchCond().like("pbm_type_id_text", n_pbm_type_id_text_like);
        }
    }
	private String n_pick_type_id_text_eq;//[分拣类型]
	public void setN_pick_type_id_text_eq(String n_pick_type_id_text_eq) {
        this.n_pick_type_id_text_eq = n_pick_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pick_type_id_text_eq)){
            this.getSearchCond().eq("pick_type_id_text", n_pick_type_id_text_eq);
        }
    }
	private String n_pick_type_id_text_like;//[分拣类型]
	public void setN_pick_type_id_text_like(String n_pick_type_id_text_like) {
        this.n_pick_type_id_text_like = n_pick_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pick_type_id_text_like)){
            this.getSearchCond().like("pick_type_id_text", n_pick_type_id_text_like);
        }
    }
	private String n_int_type_id_text_eq;//[内部类型]
	public void setN_int_type_id_text_eq(String n_int_type_id_text_eq) {
        this.n_int_type_id_text_eq = n_int_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_int_type_id_text_eq)){
            this.getSearchCond().eq("int_type_id_text", n_int_type_id_text_eq);
        }
    }
	private String n_int_type_id_text_like;//[内部类型]
	public void setN_int_type_id_text_like(String n_int_type_id_text_like) {
        this.n_int_type_id_text_like = n_int_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_int_type_id_text_like)){
            this.getSearchCond().like("int_type_id_text", n_int_type_id_text_like);
        }
    }
	private String n_manu_type_id_text_eq;//[生产操作类型]
	public void setN_manu_type_id_text_eq(String n_manu_type_id_text_eq) {
        this.n_manu_type_id_text_eq = n_manu_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_manu_type_id_text_eq)){
            this.getSearchCond().eq("manu_type_id_text", n_manu_type_id_text_eq);
        }
    }
	private String n_manu_type_id_text_like;//[生产操作类型]
	public void setN_manu_type_id_text_like(String n_manu_type_id_text_like) {
        this.n_manu_type_id_text_like = n_manu_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_manu_type_id_text_like)){
            this.getSearchCond().like("manu_type_id_text", n_manu_type_id_text_like);
        }
    }
	private String n_buy_pull_id_text_eq;//[购买规则]
	public void setN_buy_pull_id_text_eq(String n_buy_pull_id_text_eq) {
        this.n_buy_pull_id_text_eq = n_buy_pull_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_buy_pull_id_text_eq)){
            this.getSearchCond().eq("buy_pull_id_text", n_buy_pull_id_text_eq);
        }
    }
	private String n_buy_pull_id_text_like;//[购买规则]
	public void setN_buy_pull_id_text_like(String n_buy_pull_id_text_like) {
        this.n_buy_pull_id_text_like = n_buy_pull_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_buy_pull_id_text_like)){
            this.getSearchCond().like("buy_pull_id_text", n_buy_pull_id_text_like);
        }
    }
	private String n_wh_pack_stock_loc_id_text_eq;//[打包位置]
	public void setN_wh_pack_stock_loc_id_text_eq(String n_wh_pack_stock_loc_id_text_eq) {
        this.n_wh_pack_stock_loc_id_text_eq = n_wh_pack_stock_loc_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_wh_pack_stock_loc_id_text_eq)){
            this.getSearchCond().eq("wh_pack_stock_loc_id_text", n_wh_pack_stock_loc_id_text_eq);
        }
    }
	private String n_wh_pack_stock_loc_id_text_like;//[打包位置]
	public void setN_wh_pack_stock_loc_id_text_like(String n_wh_pack_stock_loc_id_text_like) {
        this.n_wh_pack_stock_loc_id_text_like = n_wh_pack_stock_loc_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_wh_pack_stock_loc_id_text_like)){
            this.getSearchCond().like("wh_pack_stock_loc_id_text", n_wh_pack_stock_loc_id_text_like);
        }
    }
	private String n_manufacture_pull_id_text_eq;//[制造规则]
	public void setN_manufacture_pull_id_text_eq(String n_manufacture_pull_id_text_eq) {
        this.n_manufacture_pull_id_text_eq = n_manufacture_pull_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_manufacture_pull_id_text_eq)){
            this.getSearchCond().eq("manufacture_pull_id_text", n_manufacture_pull_id_text_eq);
        }
    }
	private String n_manufacture_pull_id_text_like;//[制造规则]
	public void setN_manufacture_pull_id_text_like(String n_manufacture_pull_id_text_like) {
        this.n_manufacture_pull_id_text_like = n_manufacture_pull_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_manufacture_pull_id_text_like)){
            this.getSearchCond().like("manufacture_pull_id_text", n_manufacture_pull_id_text_like);
        }
    }
	private String n_reception_route_id_text_eq;//[收货路线]
	public void setN_reception_route_id_text_eq(String n_reception_route_id_text_eq) {
        this.n_reception_route_id_text_eq = n_reception_route_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_reception_route_id_text_eq)){
            this.getSearchCond().eq("reception_route_id_text", n_reception_route_id_text_eq);
        }
    }
	private String n_reception_route_id_text_like;//[收货路线]
	public void setN_reception_route_id_text_like(String n_reception_route_id_text_like) {
        this.n_reception_route_id_text_like = n_reception_route_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_reception_route_id_text_like)){
            this.getSearchCond().like("reception_route_id_text", n_reception_route_id_text_like);
        }
    }
	private String n_delivery_route_id_text_eq;//[交货路线]
	public void setN_delivery_route_id_text_eq(String n_delivery_route_id_text_eq) {
        this.n_delivery_route_id_text_eq = n_delivery_route_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_delivery_route_id_text_eq)){
            this.getSearchCond().eq("delivery_route_id_text", n_delivery_route_id_text_eq);
        }
    }
	private String n_delivery_route_id_text_like;//[交货路线]
	public void setN_delivery_route_id_text_like(String n_delivery_route_id_text_like) {
        this.n_delivery_route_id_text_like = n_delivery_route_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_delivery_route_id_text_like)){
            this.getSearchCond().like("delivery_route_id_text", n_delivery_route_id_text_like);
        }
    }
	private String n_sam_type_id_text_eq;//[制造运营类型后的库存]
	public void setN_sam_type_id_text_eq(String n_sam_type_id_text_eq) {
        this.n_sam_type_id_text_eq = n_sam_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sam_type_id_text_eq)){
            this.getSearchCond().eq("sam_type_id_text", n_sam_type_id_text_eq);
        }
    }
	private String n_sam_type_id_text_like;//[制造运营类型后的库存]
	public void setN_sam_type_id_text_like(String n_sam_type_id_text_like) {
        this.n_sam_type_id_text_like = n_sam_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sam_type_id_text_like)){
            this.getSearchCond().like("sam_type_id_text", n_sam_type_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[地址]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[地址]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_out_type_id_text_eq;//[出库类型]
	public void setN_out_type_id_text_eq(String n_out_type_id_text_eq) {
        this.n_out_type_id_text_eq = n_out_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_out_type_id_text_eq)){
            this.getSearchCond().eq("out_type_id_text", n_out_type_id_text_eq);
        }
    }
	private String n_out_type_id_text_like;//[出库类型]
	public void setN_out_type_id_text_like(String n_out_type_id_text_like) {
        this.n_out_type_id_text_like = n_out_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_out_type_id_text_like)){
            this.getSearchCond().like("out_type_id_text", n_out_type_id_text_like);
        }
    }
	private String n_mto_pull_id_text_eq;//[MTO规则]
	public void setN_mto_pull_id_text_eq(String n_mto_pull_id_text_eq) {
        this.n_mto_pull_id_text_eq = n_mto_pull_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mto_pull_id_text_eq)){
            this.getSearchCond().eq("mto_pull_id_text", n_mto_pull_id_text_eq);
        }
    }
	private String n_mto_pull_id_text_like;//[MTO规则]
	public void setN_mto_pull_id_text_like(String n_mto_pull_id_text_like) {
        this.n_mto_pull_id_text_like = n_mto_pull_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mto_pull_id_text_like)){
            this.getSearchCond().like("mto_pull_id_text", n_mto_pull_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_lot_stock_id_text_eq;//[库存位置]
	public void setN_lot_stock_id_text_eq(String n_lot_stock_id_text_eq) {
        this.n_lot_stock_id_text_eq = n_lot_stock_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_stock_id_text_eq)){
            this.getSearchCond().eq("lot_stock_id_text", n_lot_stock_id_text_eq);
        }
    }
	private String n_lot_stock_id_text_like;//[库存位置]
	public void setN_lot_stock_id_text_like(String n_lot_stock_id_text_like) {
        this.n_lot_stock_id_text_like = n_lot_stock_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_lot_stock_id_text_like)){
            this.getSearchCond().like("lot_stock_id_text", n_lot_stock_id_text_like);
        }
    }
	private String n_pbm_mto_pull_id_text_eq;//[在制造（按订单补货）MTO规则之前拣货]
	public void setN_pbm_mto_pull_id_text_eq(String n_pbm_mto_pull_id_text_eq) {
        this.n_pbm_mto_pull_id_text_eq = n_pbm_mto_pull_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pbm_mto_pull_id_text_eq)){
            this.getSearchCond().eq("pbm_mto_pull_id_text", n_pbm_mto_pull_id_text_eq);
        }
    }
	private String n_pbm_mto_pull_id_text_like;//[在制造（按订单补货）MTO规则之前拣货]
	public void setN_pbm_mto_pull_id_text_like(String n_pbm_mto_pull_id_text_like) {
        this.n_pbm_mto_pull_id_text_like = n_pbm_mto_pull_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pbm_mto_pull_id_text_like)){
            this.getSearchCond().like("pbm_mto_pull_id_text", n_pbm_mto_pull_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_pbm_loc_id_text_eq;//[在制造位置前拣货]
	public void setN_pbm_loc_id_text_eq(String n_pbm_loc_id_text_eq) {
        this.n_pbm_loc_id_text_eq = n_pbm_loc_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pbm_loc_id_text_eq)){
            this.getSearchCond().eq("pbm_loc_id_text", n_pbm_loc_id_text_eq);
        }
    }
	private String n_pbm_loc_id_text_like;//[在制造位置前拣货]
	public void setN_pbm_loc_id_text_like(String n_pbm_loc_id_text_like) {
        this.n_pbm_loc_id_text_like = n_pbm_loc_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pbm_loc_id_text_like)){
            this.getSearchCond().like("pbm_loc_id_text", n_pbm_loc_id_text_like);
        }
    }
	private String n_pack_type_id_text_eq;//[包裹类型]
	public void setN_pack_type_id_text_eq(String n_pack_type_id_text_eq) {
        this.n_pack_type_id_text_eq = n_pack_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pack_type_id_text_eq)){
            this.getSearchCond().eq("pack_type_id_text", n_pack_type_id_text_eq);
        }
    }
	private String n_pack_type_id_text_like;//[包裹类型]
	public void setN_pack_type_id_text_like(String n_pack_type_id_text_like) {
        this.n_pack_type_id_text_like = n_pack_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pack_type_id_text_like)){
            this.getSearchCond().like("pack_type_id_text", n_pack_type_id_text_like);
        }
    }
	private String n_sam_rule_id_text_eq;//[制造规则后的库存]
	public void setN_sam_rule_id_text_eq(String n_sam_rule_id_text_eq) {
        this.n_sam_rule_id_text_eq = n_sam_rule_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sam_rule_id_text_eq)){
            this.getSearchCond().eq("sam_rule_id_text", n_sam_rule_id_text_eq);
        }
    }
	private String n_sam_rule_id_text_like;//[制造规则后的库存]
	public void setN_sam_rule_id_text_like(String n_sam_rule_id_text_like) {
        this.n_sam_rule_id_text_like = n_sam_rule_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sam_rule_id_text_like)){
            this.getSearchCond().like("sam_rule_id_text", n_sam_rule_id_text_like);
        }
    }
	private String n_in_type_id_text_eq;//[入库类型]
	public void setN_in_type_id_text_eq(String n_in_type_id_text_eq) {
        this.n_in_type_id_text_eq = n_in_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_in_type_id_text_eq)){
            this.getSearchCond().eq("in_type_id_text", n_in_type_id_text_eq);
        }
    }
	private String n_in_type_id_text_like;//[入库类型]
	public void setN_in_type_id_text_like(String n_in_type_id_text_like) {
        this.n_in_type_id_text_like = n_in_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_in_type_id_text_like)){
            this.getSearchCond().like("in_type_id_text", n_in_type_id_text_like);
        }
    }
	private String n_wh_output_stock_loc_id_text_eq;//[出货位置]
	public void setN_wh_output_stock_loc_id_text_eq(String n_wh_output_stock_loc_id_text_eq) {
        this.n_wh_output_stock_loc_id_text_eq = n_wh_output_stock_loc_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_wh_output_stock_loc_id_text_eq)){
            this.getSearchCond().eq("wh_output_stock_loc_id_text", n_wh_output_stock_loc_id_text_eq);
        }
    }
	private String n_wh_output_stock_loc_id_text_like;//[出货位置]
	public void setN_wh_output_stock_loc_id_text_like(String n_wh_output_stock_loc_id_text_like) {
        this.n_wh_output_stock_loc_id_text_like = n_wh_output_stock_loc_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_wh_output_stock_loc_id_text_like)){
            this.getSearchCond().like("wh_output_stock_loc_id_text", n_wh_output_stock_loc_id_text_like);
        }
    }
	private String n_wh_qc_stock_loc_id_text_eq;//[质量管理位置]
	public void setN_wh_qc_stock_loc_id_text_eq(String n_wh_qc_stock_loc_id_text_eq) {
        this.n_wh_qc_stock_loc_id_text_eq = n_wh_qc_stock_loc_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_wh_qc_stock_loc_id_text_eq)){
            this.getSearchCond().eq("wh_qc_stock_loc_id_text", n_wh_qc_stock_loc_id_text_eq);
        }
    }
	private String n_wh_qc_stock_loc_id_text_like;//[质量管理位置]
	public void setN_wh_qc_stock_loc_id_text_like(String n_wh_qc_stock_loc_id_text_like) {
        this.n_wh_qc_stock_loc_id_text_like = n_wh_qc_stock_loc_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_wh_qc_stock_loc_id_text_like)){
            this.getSearchCond().like("wh_qc_stock_loc_id_text", n_wh_qc_stock_loc_id_text_like);
        }
    }
	private Long n_wh_output_stock_loc_id_eq;//[出货位置]
	public void setN_wh_output_stock_loc_id_eq(Long n_wh_output_stock_loc_id_eq) {
        this.n_wh_output_stock_loc_id_eq = n_wh_output_stock_loc_id_eq;
        if(!ObjectUtils.isEmpty(this.n_wh_output_stock_loc_id_eq)){
            this.getSearchCond().eq("wh_output_stock_loc_id", n_wh_output_stock_loc_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_pbm_loc_id_eq;//[在制造位置前拣货]
	public void setN_pbm_loc_id_eq(Long n_pbm_loc_id_eq) {
        this.n_pbm_loc_id_eq = n_pbm_loc_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pbm_loc_id_eq)){
            this.getSearchCond().eq("pbm_loc_id", n_pbm_loc_id_eq);
        }
    }
	private Long n_view_location_id_eq;//[视图位置]
	public void setN_view_location_id_eq(Long n_view_location_id_eq) {
        this.n_view_location_id_eq = n_view_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_view_location_id_eq)){
            this.getSearchCond().eq("view_location_id", n_view_location_id_eq);
        }
    }
	private Long n_out_type_id_eq;//[出库类型]
	public void setN_out_type_id_eq(Long n_out_type_id_eq) {
        this.n_out_type_id_eq = n_out_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_out_type_id_eq)){
            this.getSearchCond().eq("out_type_id", n_out_type_id_eq);
        }
    }
	private Long n_manu_type_id_eq;//[生产操作类型]
	public void setN_manu_type_id_eq(Long n_manu_type_id_eq) {
        this.n_manu_type_id_eq = n_manu_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_manu_type_id_eq)){
            this.getSearchCond().eq("manu_type_id", n_manu_type_id_eq);
        }
    }
	private Long n_pbm_mto_pull_id_eq;//[在制造（按订单补货）MTO规则之前拣货]
	public void setN_pbm_mto_pull_id_eq(Long n_pbm_mto_pull_id_eq) {
        this.n_pbm_mto_pull_id_eq = n_pbm_mto_pull_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pbm_mto_pull_id_eq)){
            this.getSearchCond().eq("pbm_mto_pull_id", n_pbm_mto_pull_id_eq);
        }
    }
	private Long n_mto_pull_id_eq;//[MTO规则]
	public void setN_mto_pull_id_eq(Long n_mto_pull_id_eq) {
        this.n_mto_pull_id_eq = n_mto_pull_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mto_pull_id_eq)){
            this.getSearchCond().eq("mto_pull_id", n_mto_pull_id_eq);
        }
    }
	private Long n_sam_type_id_eq;//[制造运营类型后的库存]
	public void setN_sam_type_id_eq(Long n_sam_type_id_eq) {
        this.n_sam_type_id_eq = n_sam_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sam_type_id_eq)){
            this.getSearchCond().eq("sam_type_id", n_sam_type_id_eq);
        }
    }
	private Long n_manufacture_pull_id_eq;//[制造规则]
	public void setN_manufacture_pull_id_eq(Long n_manufacture_pull_id_eq) {
        this.n_manufacture_pull_id_eq = n_manufacture_pull_id_eq;
        if(!ObjectUtils.isEmpty(this.n_manufacture_pull_id_eq)){
            this.getSearchCond().eq("manufacture_pull_id", n_manufacture_pull_id_eq);
        }
    }
	private Long n_sam_loc_id_eq;//[制造地点后的库存]
	public void setN_sam_loc_id_eq(Long n_sam_loc_id_eq) {
        this.n_sam_loc_id_eq = n_sam_loc_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sam_loc_id_eq)){
            this.getSearchCond().eq("sam_loc_id", n_sam_loc_id_eq);
        }
    }
	private Long n_buy_pull_id_eq;//[购买规则]
	public void setN_buy_pull_id_eq(Long n_buy_pull_id_eq) {
        this.n_buy_pull_id_eq = n_buy_pull_id_eq;
        if(!ObjectUtils.isEmpty(this.n_buy_pull_id_eq)){
            this.getSearchCond().eq("buy_pull_id", n_buy_pull_id_eq);
        }
    }
	private Long n_int_type_id_eq;//[内部类型]
	public void setN_int_type_id_eq(Long n_int_type_id_eq) {
        this.n_int_type_id_eq = n_int_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_int_type_id_eq)){
            this.getSearchCond().eq("int_type_id", n_int_type_id_eq);
        }
    }
	private Long n_wh_qc_stock_loc_id_eq;//[质量管理位置]
	public void setN_wh_qc_stock_loc_id_eq(Long n_wh_qc_stock_loc_id_eq) {
        this.n_wh_qc_stock_loc_id_eq = n_wh_qc_stock_loc_id_eq;
        if(!ObjectUtils.isEmpty(this.n_wh_qc_stock_loc_id_eq)){
            this.getSearchCond().eq("wh_qc_stock_loc_id", n_wh_qc_stock_loc_id_eq);
        }
    }
	private Long n_in_type_id_eq;//[入库类型]
	public void setN_in_type_id_eq(Long n_in_type_id_eq) {
        this.n_in_type_id_eq = n_in_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_in_type_id_eq)){
            this.getSearchCond().eq("in_type_id", n_in_type_id_eq);
        }
    }
	private Long n_pbm_route_id_eq;//[在制造路线前拣货]
	public void setN_pbm_route_id_eq(Long n_pbm_route_id_eq) {
        this.n_pbm_route_id_eq = n_pbm_route_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pbm_route_id_eq)){
            this.getSearchCond().eq("pbm_route_id", n_pbm_route_id_eq);
        }
    }
	private Long n_pick_type_id_eq;//[分拣类型]
	public void setN_pick_type_id_eq(Long n_pick_type_id_eq) {
        this.n_pick_type_id_eq = n_pick_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pick_type_id_eq)){
            this.getSearchCond().eq("pick_type_id", n_pick_type_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_partner_id_eq;//[地址]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_reception_route_id_eq;//[收货路线]
	public void setN_reception_route_id_eq(Long n_reception_route_id_eq) {
        this.n_reception_route_id_eq = n_reception_route_id_eq;
        if(!ObjectUtils.isEmpty(this.n_reception_route_id_eq)){
            this.getSearchCond().eq("reception_route_id", n_reception_route_id_eq);
        }
    }
	private Long n_pack_type_id_eq;//[包裹类型]
	public void setN_pack_type_id_eq(Long n_pack_type_id_eq) {
        this.n_pack_type_id_eq = n_pack_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pack_type_id_eq)){
            this.getSearchCond().eq("pack_type_id", n_pack_type_id_eq);
        }
    }
	private Long n_pbm_type_id_eq;//[在制造作业类型前拣货]
	public void setN_pbm_type_id_eq(Long n_pbm_type_id_eq) {
        this.n_pbm_type_id_eq = n_pbm_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pbm_type_id_eq)){
            this.getSearchCond().eq("pbm_type_id", n_pbm_type_id_eq);
        }
    }
	private Long n_delivery_route_id_eq;//[交货路线]
	public void setN_delivery_route_id_eq(Long n_delivery_route_id_eq) {
        this.n_delivery_route_id_eq = n_delivery_route_id_eq;
        if(!ObjectUtils.isEmpty(this.n_delivery_route_id_eq)){
            this.getSearchCond().eq("delivery_route_id", n_delivery_route_id_eq);
        }
    }
	private Long n_wh_input_stock_loc_id_eq;//[进货位置]
	public void setN_wh_input_stock_loc_id_eq(Long n_wh_input_stock_loc_id_eq) {
        this.n_wh_input_stock_loc_id_eq = n_wh_input_stock_loc_id_eq;
        if(!ObjectUtils.isEmpty(this.n_wh_input_stock_loc_id_eq)){
            this.getSearchCond().eq("wh_input_stock_loc_id", n_wh_input_stock_loc_id_eq);
        }
    }
	private Long n_lot_stock_id_eq;//[库存位置]
	public void setN_lot_stock_id_eq(Long n_lot_stock_id_eq) {
        this.n_lot_stock_id_eq = n_lot_stock_id_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_stock_id_eq)){
            this.getSearchCond().eq("lot_stock_id", n_lot_stock_id_eq);
        }
    }
	private Long n_sam_rule_id_eq;//[制造规则后的库存]
	public void setN_sam_rule_id_eq(Long n_sam_rule_id_eq) {
        this.n_sam_rule_id_eq = n_sam_rule_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sam_rule_id_eq)){
            this.getSearchCond().eq("sam_rule_id", n_sam_rule_id_eq);
        }
    }
	private Long n_crossdock_route_id_eq;//[越库路线]
	public void setN_crossdock_route_id_eq(Long n_crossdock_route_id_eq) {
        this.n_crossdock_route_id_eq = n_crossdock_route_id_eq;
        if(!ObjectUtils.isEmpty(this.n_crossdock_route_id_eq)){
            this.getSearchCond().eq("crossdock_route_id", n_crossdock_route_id_eq);
        }
    }
	private Long n_wh_pack_stock_loc_id_eq;//[打包位置]
	public void setN_wh_pack_stock_loc_id_eq(Long n_wh_pack_stock_loc_id_eq) {
        this.n_wh_pack_stock_loc_id_eq = n_wh_pack_stock_loc_id_eq;
        if(!ObjectUtils.isEmpty(this.n_wh_pack_stock_loc_id_eq)){
            this.getSearchCond().eq("wh_pack_stock_loc_id", n_wh_pack_stock_loc_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



