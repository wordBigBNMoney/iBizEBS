package cn.ibizlab.businesscentral.core.odoo_hr.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_sheet_register_payment_wizardSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Hr_expense_sheet_register_payment_wizardMapper extends BaseMapper<Hr_expense_sheet_register_payment_wizard>{

    Page<Hr_expense_sheet_register_payment_wizard> searchDefault(IPage page, @Param("srf") Hr_expense_sheet_register_payment_wizardSearchContext context, @Param("ew") Wrapper<Hr_expense_sheet_register_payment_wizard> wrapper) ;
    @Override
    Hr_expense_sheet_register_payment_wizard selectById(Serializable id);
    @Override
    int insert(Hr_expense_sheet_register_payment_wizard entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Hr_expense_sheet_register_payment_wizard entity);
    @Override
    int update(@Param(Constants.ENTITY) Hr_expense_sheet_register_payment_wizard entity, @Param("ew") Wrapper<Hr_expense_sheet_register_payment_wizard> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Hr_expense_sheet_register_payment_wizard> selectByJournalId(@Param("id") Serializable id) ;

    List<Hr_expense_sheet_register_payment_wizard> selectByPaymentMethodId(@Param("id") Serializable id) ;

    List<Hr_expense_sheet_register_payment_wizard> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Hr_expense_sheet_register_payment_wizard> selectByPartnerBankAccountId(@Param("id") Serializable id) ;

    List<Hr_expense_sheet_register_payment_wizard> selectByPartnerId(@Param("id") Serializable id) ;

    List<Hr_expense_sheet_register_payment_wizard> selectByCreateUid(@Param("id") Serializable id) ;

    List<Hr_expense_sheet_register_payment_wizard> selectByWriteUid(@Param("id") Serializable id) ;


}
