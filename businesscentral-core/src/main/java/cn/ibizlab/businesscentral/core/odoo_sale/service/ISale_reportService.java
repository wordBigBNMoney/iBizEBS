package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_report] 服务对象接口
 */
public interface ISale_reportService extends IService<Sale_report>{

    boolean create(Sale_report et) ;
    void createBatch(List<Sale_report> list) ;
    boolean update(Sale_report et) ;
    void updateBatch(List<Sale_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_report get(Long key) ;
    Sale_report getDraft(Sale_report et) ;
    boolean checkKey(Sale_report et) ;
    boolean save(Sale_report et) ;
    void saveBatch(List<Sale_report> list) ;
    Page<Sale_report> searchDefault(Sale_reportSearchContext context) ;
    List<Sale_report> selectByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Collection<Long> ids);
    void removeByAnalyticAccountId(Long id);
    List<Sale_report> selectByTeamId(Long id);
    void resetByTeamId(Long id);
    void resetByTeamId(Collection<Long> ids);
    void removeByTeamId(Long id);
    List<Sale_report> selectByCategId(Long id);
    void resetByCategId(Long id);
    void resetByCategId(Collection<Long> ids);
    void removeByCategId(Long id);
    List<Sale_report> selectByPricelistId(Long id);
    void resetByPricelistId(Long id);
    void resetByPricelistId(Collection<Long> ids);
    void removeByPricelistId(Long id);
    List<Sale_report> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Sale_report> selectByProductTmplId(Long id);
    void resetByProductTmplId(Long id);
    void resetByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Sale_report> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Sale_report> selectByCountryId(Long id);
    void resetByCountryId(Long id);
    void resetByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Sale_report> selectByCommercialPartnerId(Long id);
    void resetByCommercialPartnerId(Long id);
    void resetByCommercialPartnerId(Collection<Long> ids);
    void removeByCommercialPartnerId(Long id);
    List<Sale_report> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Sale_report> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Sale_report> selectByOrderId(Long id);
    void resetByOrderId(Long id);
    void resetByOrderId(Collection<Long> ids);
    void removeByOrderId(Long id);
    List<Sale_report> selectByWarehouseId(Long id);
    void resetByWarehouseId(Long id);
    void resetByWarehouseId(Collection<Long> ids);
    void removeByWarehouseId(Long id);
    List<Sale_report> selectByProductUom(Long id);
    void resetByProductUom(Long id);
    void resetByProductUom(Collection<Long> ids);
    void removeByProductUom(Long id);
    List<Sale_report> selectByCampaignId(Long id);
    void resetByCampaignId(Long id);
    void resetByCampaignId(Collection<Long> ids);
    void removeByCampaignId(Long id);
    List<Sale_report> selectByMediumId(Long id);
    void resetByMediumId(Long id);
    void resetByMediumId(Collection<Long> ids);
    void removeByMediumId(Long id);
    List<Sale_report> selectBySourceId(Long id);
    void resetBySourceId(Long id);
    void resetBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


