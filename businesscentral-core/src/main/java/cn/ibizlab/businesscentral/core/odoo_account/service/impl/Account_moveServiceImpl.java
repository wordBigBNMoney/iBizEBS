package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_moveSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_moveMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[凭证录入] 服务对象接口实现
 */
@Slf4j
@Service("Account_moveServiceImpl")
public class Account_moveServiceImpl extends EBSServiceImpl<Account_moveMapper, Account_move> implements IAccount_moveService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_full_reconcileService accountFullReconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IAccount_move_purchase_order_relService accountMovePurchaseOrderRelService;

    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_partial_reconcileService accountPartialReconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.move" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_move et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_moveService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_move> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_move et) {
        Account_move old = new Account_move() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_moveService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_moveService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_move> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountFullReconcileService.resetByExchangeMoveId(key);
        if(!ObjectUtils.isEmpty(accountInvoiceService.selectByMoveId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_invoice]数据，无法删除!","","");
        accountMoveLineService.removeByMoveId(key);
        accountMoveService.resetByReverseEntryId(key);
        if(!ObjectUtils.isEmpty(hrExpenseSheetService.selectByAccountMoveId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Hr_expense_sheet]数据，无法删除!","","");
        resCompanyService.resetByAccountOpeningMoveId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountFullReconcileService.resetByExchangeMoveId(idList);
        if(!ObjectUtils.isEmpty(accountInvoiceService.selectByMoveId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_invoice]数据，无法删除!","","");
        accountMoveLineService.removeByMoveId(idList);
        accountMoveService.resetByReverseEntryId(idList);
        if(!ObjectUtils.isEmpty(hrExpenseSheetService.selectByAccountMoveId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Hr_expense_sheet]数据，无法删除!","","");
        resCompanyService.resetByAccountOpeningMoveId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_move get(Long key) {
        Account_move et = getById(key);
        if(et==null){
            et=new Account_move();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_move getDraft(Account_move et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_move et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_move et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_move et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_move> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_move> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_move> selectByJournalId(Long id) {
        return baseMapper.selectByJournalId(id);
    }
    @Override
    public void resetByJournalId(Long id) {
        this.update(new UpdateWrapper<Account_move>().set("journal_id",null).eq("journal_id",id));
    }

    @Override
    public void resetByJournalId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move>().set("journal_id",null).in("journal_id",ids));
    }

    @Override
    public void removeByJournalId(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("journal_id",id));
    }

	@Override
    public List<Account_move> selectByReverseEntryId(Long id) {
        return baseMapper.selectByReverseEntryId(id);
    }
    @Override
    public void resetByReverseEntryId(Long id) {
        this.update(new UpdateWrapper<Account_move>().set("reverse_entry_id",null).eq("reverse_entry_id",id));
    }

    @Override
    public void resetByReverseEntryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move>().set("reverse_entry_id",null).in("reverse_entry_id",ids));
    }

    @Override
    public void removeByReverseEntryId(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("reverse_entry_id",id));
    }

	@Override
    public List<Account_move> selectByTaxCashBasisRecId(Long id) {
        return baseMapper.selectByTaxCashBasisRecId(id);
    }
    @Override
    public void resetByTaxCashBasisRecId(Long id) {
        this.update(new UpdateWrapper<Account_move>().set("tax_cash_basis_rec_id",null).eq("tax_cash_basis_rec_id",id));
    }

    @Override
    public void resetByTaxCashBasisRecId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move>().set("tax_cash_basis_rec_id",null).in("tax_cash_basis_rec_id",ids));
    }

    @Override
    public void removeByTaxCashBasisRecId(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("tax_cash_basis_rec_id",id));
    }

	@Override
    public List<Account_move> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_move>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("company_id",id));
    }

	@Override
    public List<Account_move> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_move>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("currency_id",id));
    }

	@Override
    public List<Account_move> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Account_move>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("partner_id",id));
    }

	@Override
    public List<Account_move> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("create_uid",id));
    }

	@Override
    public List<Account_move> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("write_uid",id));
    }

	@Override
    public List<Account_move> selectByStockMoveId(Long id) {
        return baseMapper.selectByStockMoveId(id);
    }
    @Override
    public void resetByStockMoveId(Long id) {
        this.update(new UpdateWrapper<Account_move>().set("stock_move_id",null).eq("stock_move_id",id));
    }

    @Override
    public void resetByStockMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move>().set("stock_move_id",null).in("stock_move_id",ids));
    }

    @Override
    public void removeByStockMoveId(Long id) {
        this.remove(new QueryWrapper<Account_move>().eq("stock_move_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_move> searchDefault(Account_moveSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_move> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_move>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_move et){
        //实体关系[DER1N_ACCOUNT_MOVE__ACCOUNT_JOURNAL__JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal=et.getOdooJournal();
            if(ObjectUtils.isEmpty(odooJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getJournalId());
                et.setOdooJournal(majorEntity);
                odooJournal=majorEntity;
            }
            et.setJournalIdText(odooJournal.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE__ACCOUNT_MOVE__REVERSE_ENTRY_ID]
        if(!ObjectUtils.isEmpty(et.getReverseEntryId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move odooReverseEntry=et.getOdooReverseEntry();
            if(ObjectUtils.isEmpty(odooReverseEntry)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move majorEntity=accountMoveService.get(et.getReverseEntryId());
                et.setOdooReverseEntry(majorEntity);
                odooReverseEntry=majorEntity;
            }
            et.setReverseEntryIdText(odooReverseEntry.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE__STOCK_MOVE__STOCK_MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getStockMoveId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooStockMove=et.getOdooStockMove();
            if(ObjectUtils.isEmpty(odooStockMove)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move majorEntity=stockMoveService.get(et.getStockMoveId());
                et.setOdooStockMove(majorEntity);
                odooStockMove=majorEntity;
            }
            et.setStockMoveIdText(odooStockMove.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_move> getAccountMoveByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_move> getAccountMoveByEntities(List<Account_move> entities) {
        List ids =new ArrayList();
        for(Account_move entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



