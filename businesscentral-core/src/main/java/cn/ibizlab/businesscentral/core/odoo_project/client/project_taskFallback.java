package cn.ibizlab.businesscentral.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_taskSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[project_task] 服务对象接口
 */
@Component
public class project_taskFallback implements project_taskFeignClient{



    public Project_task create(Project_task project_task){
            return null;
     }
    public Boolean createBatch(List<Project_task> project_tasks){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Project_task get(Long id){
            return null;
     }


    public Project_task update(Long id, Project_task project_task){
            return null;
     }
    public Boolean updateBatch(List<Project_task> project_tasks){
            return false;
     }



    public Page<Project_task> search(Project_taskSearchContext context){
            return null;
     }


    public Page<Project_task> select(){
            return null;
     }

    public Project_task getDraft(){
            return null;
    }



    public Boolean checkKey(Project_task project_task){
            return false;
     }


    public Boolean save(Project_task project_task){
            return false;
     }
    public Boolean saveBatch(List<Project_task> project_tasks){
            return false;
     }

    public Page<Project_task> searchDefault(Project_taskSearchContext context){
            return null;
     }


}
