package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_partial_reconcileSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_partial_reconcile] 服务对象接口
 */
public interface IAccount_partial_reconcileService extends IService<Account_partial_reconcile>{

    boolean create(Account_partial_reconcile et) ;
    void createBatch(List<Account_partial_reconcile> list) ;
    boolean update(Account_partial_reconcile et) ;
    void updateBatch(List<Account_partial_reconcile> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_partial_reconcile get(Long key) ;
    Account_partial_reconcile getDraft(Account_partial_reconcile et) ;
    boolean checkKey(Account_partial_reconcile et) ;
    boolean save(Account_partial_reconcile et) ;
    void saveBatch(List<Account_partial_reconcile> list) ;
    Page<Account_partial_reconcile> searchDefault(Account_partial_reconcileSearchContext context) ;
    List<Account_partial_reconcile> selectByFullReconcileId(Long id);
    void resetByFullReconcileId(Long id);
    void resetByFullReconcileId(Collection<Long> ids);
    void removeByFullReconcileId(Long id);
    List<Account_partial_reconcile> selectByCreditMoveId(Long id);
    void resetByCreditMoveId(Long id);
    void resetByCreditMoveId(Collection<Long> ids);
    void removeByCreditMoveId(Long id);
    List<Account_partial_reconcile> selectByDebitMoveId(Long id);
    void resetByDebitMoveId(Long id);
    void resetByDebitMoveId(Collection<Long> ids);
    void removeByDebitMoveId(Long id);
    List<Account_partial_reconcile> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_partial_reconcile> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_partial_reconcile> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_partial_reconcile> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_partial_reconcile> getAccountPartialReconcileByIds(List<Long> ids) ;
    List<Account_partial_reconcile> getAccountPartialReconcileByEntities(List<Account_partial_reconcile> entities) ;
}


