package cn.ibizlab.businesscentral.core.odoo_stock.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_scrap;
/**
 * 关系型数据实体[Stock_scrap] 查询条件对象
 */
@Slf4j
@Data
public class Stock_scrapSearchContext extends QueryWrapperContext<Stock_scrap> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_like;//[编号]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_workorder_id_text_eq;//[工单]
	public void setN_workorder_id_text_eq(String n_workorder_id_text_eq) {
        this.n_workorder_id_text_eq = n_workorder_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_workorder_id_text_eq)){
            this.getSearchCond().eq("workorder_id_text", n_workorder_id_text_eq);
        }
    }
	private String n_workorder_id_text_like;//[工单]
	public void setN_workorder_id_text_like(String n_workorder_id_text_like) {
        this.n_workorder_id_text_like = n_workorder_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_workorder_id_text_like)){
            this.getSearchCond().like("workorder_id_text", n_workorder_id_text_like);
        }
    }
	private String n_scrap_location_id_text_eq;//[报废位置]
	public void setN_scrap_location_id_text_eq(String n_scrap_location_id_text_eq) {
        this.n_scrap_location_id_text_eq = n_scrap_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_scrap_location_id_text_eq)){
            this.getSearchCond().eq("scrap_location_id_text", n_scrap_location_id_text_eq);
        }
    }
	private String n_scrap_location_id_text_like;//[报废位置]
	public void setN_scrap_location_id_text_like(String n_scrap_location_id_text_like) {
        this.n_scrap_location_id_text_like = n_scrap_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_scrap_location_id_text_like)){
            this.getSearchCond().like("scrap_location_id_text", n_scrap_location_id_text_like);
        }
    }
	private String n_lot_id_text_eq;//[批次]
	public void setN_lot_id_text_eq(String n_lot_id_text_eq) {
        this.n_lot_id_text_eq = n_lot_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_eq)){
            this.getSearchCond().eq("lot_id_text", n_lot_id_text_eq);
        }
    }
	private String n_lot_id_text_like;//[批次]
	public void setN_lot_id_text_like(String n_lot_id_text_like) {
        this.n_lot_id_text_like = n_lot_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_like)){
            this.getSearchCond().like("lot_id_text", n_lot_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_move_id_text_eq;//[报废移动]
	public void setN_move_id_text_eq(String n_move_id_text_eq) {
        this.n_move_id_text_eq = n_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_text_eq)){
            this.getSearchCond().eq("move_id_text", n_move_id_text_eq);
        }
    }
	private String n_move_id_text_like;//[报废移动]
	public void setN_move_id_text_like(String n_move_id_text_like) {
        this.n_move_id_text_like = n_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_move_id_text_like)){
            this.getSearchCond().like("move_id_text", n_move_id_text_like);
        }
    }
	private String n_picking_id_text_eq;//[分拣]
	public void setN_picking_id_text_eq(String n_picking_id_text_eq) {
        this.n_picking_id_text_eq = n_picking_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_id_text_eq)){
            this.getSearchCond().eq("picking_id_text", n_picking_id_text_eq);
        }
    }
	private String n_picking_id_text_like;//[分拣]
	public void setN_picking_id_text_like(String n_picking_id_text_like) {
        this.n_picking_id_text_like = n_picking_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_picking_id_text_like)){
            this.getSearchCond().like("picking_id_text", n_picking_id_text_like);
        }
    }
	private String n_production_id_text_eq;//[制造订单]
	public void setN_production_id_text_eq(String n_production_id_text_eq) {
        this.n_production_id_text_eq = n_production_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_production_id_text_eq)){
            this.getSearchCond().eq("production_id_text", n_production_id_text_eq);
        }
    }
	private String n_production_id_text_like;//[制造订单]
	public void setN_production_id_text_like(String n_production_id_text_like) {
        this.n_production_id_text_like = n_production_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_production_id_text_like)){
            this.getSearchCond().like("production_id_text", n_production_id_text_like);
        }
    }
	private String n_location_id_text_eq;//[位置]
	public void setN_location_id_text_eq(String n_location_id_text_eq) {
        this.n_location_id_text_eq = n_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_eq)){
            this.getSearchCond().eq("location_id_text", n_location_id_text_eq);
        }
    }
	private String n_location_id_text_like;//[位置]
	public void setN_location_id_text_like(String n_location_id_text_like) {
        this.n_location_id_text_like = n_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_like)){
            this.getSearchCond().like("location_id_text", n_location_id_text_like);
        }
    }
	private String n_product_uom_id_text_eq;//[单位]
	public void setN_product_uom_id_text_eq(String n_product_uom_id_text_eq) {
        this.n_product_uom_id_text_eq = n_product_uom_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_eq)){
            this.getSearchCond().eq("product_uom_id_text", n_product_uom_id_text_eq);
        }
    }
	private String n_product_uom_id_text_like;//[单位]
	public void setN_product_uom_id_text_like(String n_product_uom_id_text_like) {
        this.n_product_uom_id_text_like = n_product_uom_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_like)){
            this.getSearchCond().like("product_uom_id_text", n_product_uom_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_package_id_text_eq;//[包裹]
	public void setN_package_id_text_eq(String n_package_id_text_eq) {
        this.n_package_id_text_eq = n_package_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_package_id_text_eq)){
            this.getSearchCond().eq("package_id_text", n_package_id_text_eq);
        }
    }
	private String n_package_id_text_like;//[包裹]
	public void setN_package_id_text_like(String n_package_id_text_like) {
        this.n_package_id_text_like = n_package_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_package_id_text_like)){
            this.getSearchCond().like("package_id_text", n_package_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_owner_id_text_eq;//[所有者]
	public void setN_owner_id_text_eq(String n_owner_id_text_eq) {
        this.n_owner_id_text_eq = n_owner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_owner_id_text_eq)){
            this.getSearchCond().eq("owner_id_text", n_owner_id_text_eq);
        }
    }
	private String n_owner_id_text_like;//[所有者]
	public void setN_owner_id_text_like(String n_owner_id_text_like) {
        this.n_owner_id_text_like = n_owner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_owner_id_text_like)){
            this.getSearchCond().like("owner_id_text", n_owner_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_lot_id_eq;//[批次]
	public void setN_lot_id_eq(Long n_lot_id_eq) {
        this.n_lot_id_eq = n_lot_id_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_eq)){
            this.getSearchCond().eq("lot_id", n_lot_id_eq);
        }
    }
	private Long n_production_id_eq;//[制造订单]
	public void setN_production_id_eq(Long n_production_id_eq) {
        this.n_production_id_eq = n_production_id_eq;
        if(!ObjectUtils.isEmpty(this.n_production_id_eq)){
            this.getSearchCond().eq("production_id", n_production_id_eq);
        }
    }
	private Long n_picking_id_eq;//[分拣]
	public void setN_picking_id_eq(Long n_picking_id_eq) {
        this.n_picking_id_eq = n_picking_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_id_eq)){
            this.getSearchCond().eq("picking_id", n_picking_id_eq);
        }
    }
	private Long n_scrap_location_id_eq;//[报废位置]
	public void setN_scrap_location_id_eq(Long n_scrap_location_id_eq) {
        this.n_scrap_location_id_eq = n_scrap_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_scrap_location_id_eq)){
            this.getSearchCond().eq("scrap_location_id", n_scrap_location_id_eq);
        }
    }
	private Long n_move_id_eq;//[报废移动]
	public void setN_move_id_eq(Long n_move_id_eq) {
        this.n_move_id_eq = n_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_eq)){
            this.getSearchCond().eq("move_id", n_move_id_eq);
        }
    }
	private Long n_location_id_eq;//[位置]
	public void setN_location_id_eq(Long n_location_id_eq) {
        this.n_location_id_eq = n_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_eq)){
            this.getSearchCond().eq("location_id", n_location_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_workorder_id_eq;//[工单]
	public void setN_workorder_id_eq(Long n_workorder_id_eq) {
        this.n_workorder_id_eq = n_workorder_id_eq;
        if(!ObjectUtils.isEmpty(this.n_workorder_id_eq)){
            this.getSearchCond().eq("workorder_id", n_workorder_id_eq);
        }
    }
	private Long n_product_uom_id_eq;//[单位]
	public void setN_product_uom_id_eq(Long n_product_uom_id_eq) {
        this.n_product_uom_id_eq = n_product_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_eq)){
            this.getSearchCond().eq("product_uom_id", n_product_uom_id_eq);
        }
    }
	private Long n_package_id_eq;//[包裹]
	public void setN_package_id_eq(Long n_package_id_eq) {
        this.n_package_id_eq = n_package_id_eq;
        if(!ObjectUtils.isEmpty(this.n_package_id_eq)){
            this.getSearchCond().eq("package_id", n_package_id_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_owner_id_eq;//[所有者]
	public void setN_owner_id_eq(Long n_owner_id_eq) {
        this.n_owner_id_eq = n_owner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_owner_id_eq)){
            this.getSearchCond().eq("owner_id", n_owner_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



