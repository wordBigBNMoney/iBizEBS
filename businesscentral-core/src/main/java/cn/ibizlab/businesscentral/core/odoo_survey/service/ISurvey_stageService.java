package cn.ibizlab.businesscentral.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_stageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Survey_stage] 服务对象接口
 */
public interface ISurvey_stageService extends IService<Survey_stage>{

    boolean create(Survey_stage et) ;
    void createBatch(List<Survey_stage> list) ;
    boolean update(Survey_stage et) ;
    void updateBatch(List<Survey_stage> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Survey_stage get(Long key) ;
    Survey_stage getDraft(Survey_stage et) ;
    boolean checkKey(Survey_stage et) ;
    boolean save(Survey_stage et) ;
    void saveBatch(List<Survey_stage> list) ;
    Page<Survey_stage> searchDefault(Survey_stageSearchContext context) ;
    List<Survey_stage> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Survey_stage> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Survey_stage> getSurveyStageByIds(List<Long> ids) ;
    List<Survey_stage> getSurveyStageByEntities(List<Survey_stage> entities) ;
}


