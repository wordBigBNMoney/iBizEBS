package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier_res_partner_category_rel;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_supplier_res_partner_category_relSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Res_supplier_res_partner_category_relMapper extends BaseMapper<Res_supplier_res_partner_category_rel>{

    Page<Res_supplier_res_partner_category_rel> searchDefault(IPage page, @Param("srf") Res_supplier_res_partner_category_relSearchContext context, @Param("ew") Wrapper<Res_supplier_res_partner_category_rel> wrapper) ;
    @Override
    Res_supplier_res_partner_category_rel selectById(Serializable id);
    @Override
    int insert(Res_supplier_res_partner_category_rel entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Res_supplier_res_partner_category_rel entity);
    @Override
    int update(@Param(Constants.ENTITY) Res_supplier_res_partner_category_rel entity, @Param("ew") Wrapper<Res_supplier_res_partner_category_rel> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Res_supplier_res_partner_category_rel> selectByCategoryId(@Param("id") Serializable id) ;

    List<Res_supplier_res_partner_category_rel> selectByPartnerId(@Param("id") Serializable id) ;


}
