package cn.ibizlab.businesscentral.core.odoo_mro.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_convert_orderSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mro_convert_orderMapper extends BaseMapper<Mro_convert_order>{

    Page<Mro_convert_order> searchDefault(IPage page, @Param("srf") Mro_convert_orderSearchContext context, @Param("ew") Wrapper<Mro_convert_order> wrapper) ;
    @Override
    Mro_convert_order selectById(Serializable id);
    @Override
    int insert(Mro_convert_order entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mro_convert_order entity);
    @Override
    int update(@Param(Constants.ENTITY) Mro_convert_order entity, @Param("ew") Wrapper<Mro_convert_order> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mro_convert_order> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mro_convert_order> selectByWriteUid(@Param("id") Serializable id) ;


}
