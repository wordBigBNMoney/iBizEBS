package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_country_stateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_country_state] 服务对象接口
 */
@Component
public class res_country_stateFallback implements res_country_stateFeignClient{

    public Res_country_state update(Long id, Res_country_state res_country_state){
            return null;
     }
    public Boolean updateBatch(List<Res_country_state> res_country_states){
            return false;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Res_country_state get(Long id){
            return null;
     }


    public Page<Res_country_state> search(Res_country_stateSearchContext context){
            return null;
     }


    public Res_country_state create(Res_country_state res_country_state){
            return null;
     }
    public Boolean createBatch(List<Res_country_state> res_country_states){
            return false;
     }


    public Page<Res_country_state> select(){
            return null;
     }

    public Res_country_state getDraft(){
            return null;
    }



    public Boolean checkKey(Res_country_state res_country_state){
            return false;
     }


    public Boolean save(Res_country_state res_country_state){
            return false;
     }
    public Boolean saveBatch(List<Res_country_state> res_country_states){
            return false;
     }

    public Page<Res_country_state> searchDefault(Res_country_stateSearchContext context){
            return null;
     }


}
