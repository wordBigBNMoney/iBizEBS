package cn.ibizlab.businesscentral.core.odoo_sale.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_report;
/**
 * 关系型数据实体[Sale_report] 查询条件对象
 */
@Slf4j
@Data
public class Sale_reportSearchContext extends QueryWrapperContext<Sale_report> {

	private String n_name_like;//[订单关联]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_product_tmpl_id_text_eq;//[产品]
	public void setN_product_tmpl_id_text_eq(String n_product_tmpl_id_text_eq) {
        this.n_product_tmpl_id_text_eq = n_product_tmpl_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_text_eq)){
            this.getSearchCond().eq("product_tmpl_id_text", n_product_tmpl_id_text_eq);
        }
    }
	private String n_product_tmpl_id_text_like;//[产品]
	public void setN_product_tmpl_id_text_like(String n_product_tmpl_id_text_like) {
        this.n_product_tmpl_id_text_like = n_product_tmpl_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_text_like)){
            this.getSearchCond().like("product_tmpl_id_text", n_product_tmpl_id_text_like);
        }
    }
	private String n_warehouse_id_text_eq;//[仓库]
	public void setN_warehouse_id_text_eq(String n_warehouse_id_text_eq) {
        this.n_warehouse_id_text_eq = n_warehouse_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_text_eq)){
            this.getSearchCond().eq("warehouse_id_text", n_warehouse_id_text_eq);
        }
    }
	private String n_warehouse_id_text_like;//[仓库]
	public void setN_warehouse_id_text_like(String n_warehouse_id_text_like) {
        this.n_warehouse_id_text_like = n_warehouse_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_text_like)){
            this.getSearchCond().like("warehouse_id_text", n_warehouse_id_text_like);
        }
    }
	private String n_analytic_account_id_text_eq;//[分析账户]
	public void setN_analytic_account_id_text_eq(String n_analytic_account_id_text_eq) {
        this.n_analytic_account_id_text_eq = n_analytic_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_eq)){
            this.getSearchCond().eq("analytic_account_id_text", n_analytic_account_id_text_eq);
        }
    }
	private String n_analytic_account_id_text_like;//[分析账户]
	public void setN_analytic_account_id_text_like(String n_analytic_account_id_text_like) {
        this.n_analytic_account_id_text_like = n_analytic_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_like)){
            this.getSearchCond().like("analytic_account_id_text", n_analytic_account_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品变体]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品变体]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_country_id_text_eq;//[客户国家]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[客户国家]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private String n_categ_id_text_eq;//[产品种类]
	public void setN_categ_id_text_eq(String n_categ_id_text_eq) {
        this.n_categ_id_text_eq = n_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_categ_id_text_eq)){
            this.getSearchCond().eq("categ_id_text", n_categ_id_text_eq);
        }
    }
	private String n_categ_id_text_like;//[产品种类]
	public void setN_categ_id_text_like(String n_categ_id_text_like) {
        this.n_categ_id_text_like = n_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_categ_id_text_like)){
            this.getSearchCond().like("categ_id_text", n_categ_id_text_like);
        }
    }
	private String n_source_id_text_eq;//[来源]
	public void setN_source_id_text_eq(String n_source_id_text_eq) {
        this.n_source_id_text_eq = n_source_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_eq)){
            this.getSearchCond().eq("source_id_text", n_source_id_text_eq);
        }
    }
	private String n_source_id_text_like;//[来源]
	public void setN_source_id_text_like(String n_source_id_text_like) {
        this.n_source_id_text_like = n_source_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_like)){
            this.getSearchCond().like("source_id_text", n_source_id_text_like);
        }
    }
	private String n_campaign_id_text_eq;//[营销]
	public void setN_campaign_id_text_eq(String n_campaign_id_text_eq) {
        this.n_campaign_id_text_eq = n_campaign_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_eq)){
            this.getSearchCond().eq("campaign_id_text", n_campaign_id_text_eq);
        }
    }
	private String n_campaign_id_text_like;//[营销]
	public void setN_campaign_id_text_like(String n_campaign_id_text_like) {
        this.n_campaign_id_text_like = n_campaign_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_like)){
            this.getSearchCond().like("campaign_id_text", n_campaign_id_text_like);
        }
    }
	private String n_order_id_text_eq;//[订单 #]
	public void setN_order_id_text_eq(String n_order_id_text_eq) {
        this.n_order_id_text_eq = n_order_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_text_eq)){
            this.getSearchCond().eq("order_id_text", n_order_id_text_eq);
        }
    }
	private String n_order_id_text_like;//[订单 #]
	public void setN_order_id_text_like(String n_order_id_text_like) {
        this.n_order_id_text_like = n_order_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_order_id_text_like)){
            this.getSearchCond().like("order_id_text", n_order_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[销售员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[销售员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_commercial_partner_id_text_eq;//[客户实体]
	public void setN_commercial_partner_id_text_eq(String n_commercial_partner_id_text_eq) {
        this.n_commercial_partner_id_text_eq = n_commercial_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_eq)){
            this.getSearchCond().eq("commercial_partner_id_text", n_commercial_partner_id_text_eq);
        }
    }
	private String n_commercial_partner_id_text_like;//[客户实体]
	public void setN_commercial_partner_id_text_like(String n_commercial_partner_id_text_like) {
        this.n_commercial_partner_id_text_like = n_commercial_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_like)){
            this.getSearchCond().like("commercial_partner_id_text", n_commercial_partner_id_text_like);
        }
    }
	private String n_medium_id_text_eq;//[媒体]
	public void setN_medium_id_text_eq(String n_medium_id_text_eq) {
        this.n_medium_id_text_eq = n_medium_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_eq)){
            this.getSearchCond().eq("medium_id_text", n_medium_id_text_eq);
        }
    }
	private String n_medium_id_text_like;//[媒体]
	public void setN_medium_id_text_like(String n_medium_id_text_like) {
        this.n_medium_id_text_like = n_medium_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_like)){
            this.getSearchCond().like("medium_id_text", n_medium_id_text_like);
        }
    }
	private String n_product_uom_text_eq;//[计量单位]
	public void setN_product_uom_text_eq(String n_product_uom_text_eq) {
        this.n_product_uom_text_eq = n_product_uom_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_eq)){
            this.getSearchCond().eq("product_uom_text", n_product_uom_text_eq);
        }
    }
	private String n_product_uom_text_like;//[计量单位]
	public void setN_product_uom_text_like(String n_product_uom_text_like) {
        this.n_product_uom_text_like = n_product_uom_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_like)){
            this.getSearchCond().like("product_uom_text", n_product_uom_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_team_id_text_eq;//[销售团队]
	public void setN_team_id_text_eq(String n_team_id_text_eq) {
        this.n_team_id_text_eq = n_team_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_eq)){
            this.getSearchCond().eq("team_id_text", n_team_id_text_eq);
        }
    }
	private String n_team_id_text_like;//[销售团队]
	public void setN_team_id_text_like(String n_team_id_text_like) {
        this.n_team_id_text_like = n_team_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_like)){
            this.getSearchCond().like("team_id_text", n_team_id_text_like);
        }
    }
	private String n_pricelist_id_text_eq;//[价格表]
	public void setN_pricelist_id_text_eq(String n_pricelist_id_text_eq) {
        this.n_pricelist_id_text_eq = n_pricelist_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_eq)){
            this.getSearchCond().eq("pricelist_id_text", n_pricelist_id_text_eq);
        }
    }
	private String n_pricelist_id_text_like;//[价格表]
	public void setN_pricelist_id_text_like(String n_pricelist_id_text_like) {
        this.n_pricelist_id_text_like = n_pricelist_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_like)){
            this.getSearchCond().like("pricelist_id_text", n_pricelist_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[客户]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[客户]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private Long n_team_id_eq;//[销售团队]
	public void setN_team_id_eq(Long n_team_id_eq) {
        this.n_team_id_eq = n_team_id_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_eq)){
            this.getSearchCond().eq("team_id", n_team_id_eq);
        }
    }
	private Long n_commercial_partner_id_eq;//[客户实体]
	public void setN_commercial_partner_id_eq(Long n_commercial_partner_id_eq) {
        this.n_commercial_partner_id_eq = n_commercial_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_eq)){
            this.getSearchCond().eq("commercial_partner_id", n_commercial_partner_id_eq);
        }
    }
	private Long n_user_id_eq;//[销售员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_product_tmpl_id_eq;//[产品]
	public void setN_product_tmpl_id_eq(Long n_product_tmpl_id_eq) {
        this.n_product_tmpl_id_eq = n_product_tmpl_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_eq)){
            this.getSearchCond().eq("product_tmpl_id", n_product_tmpl_id_eq);
        }
    }
	private Long n_analytic_account_id_eq;//[分析账户]
	public void setN_analytic_account_id_eq(Long n_analytic_account_id_eq) {
        this.n_analytic_account_id_eq = n_analytic_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_eq)){
            this.getSearchCond().eq("analytic_account_id", n_analytic_account_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_medium_id_eq;//[媒体]
	public void setN_medium_id_eq(Long n_medium_id_eq) {
        this.n_medium_id_eq = n_medium_id_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_eq)){
            this.getSearchCond().eq("medium_id", n_medium_id_eq);
        }
    }
	private Long n_order_id_eq;//[订单 #]
	public void setN_order_id_eq(Long n_order_id_eq) {
        this.n_order_id_eq = n_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_eq)){
            this.getSearchCond().eq("order_id", n_order_id_eq);
        }
    }
	private Long n_partner_id_eq;//[客户]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_source_id_eq;//[来源]
	public void setN_source_id_eq(Long n_source_id_eq) {
        this.n_source_id_eq = n_source_id_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_eq)){
            this.getSearchCond().eq("source_id", n_source_id_eq);
        }
    }
	private Long n_campaign_id_eq;//[营销]
	public void setN_campaign_id_eq(Long n_campaign_id_eq) {
        this.n_campaign_id_eq = n_campaign_id_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_eq)){
            this.getSearchCond().eq("campaign_id", n_campaign_id_eq);
        }
    }
	private Long n_categ_id_eq;//[产品种类]
	public void setN_categ_id_eq(Long n_categ_id_eq) {
        this.n_categ_id_eq = n_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_categ_id_eq)){
            this.getSearchCond().eq("categ_id", n_categ_id_eq);
        }
    }
	private Long n_warehouse_id_eq;//[仓库]
	public void setN_warehouse_id_eq(Long n_warehouse_id_eq) {
        this.n_warehouse_id_eq = n_warehouse_id_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_eq)){
            this.getSearchCond().eq("warehouse_id", n_warehouse_id_eq);
        }
    }
	private Long n_product_id_eq;//[产品变体]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_pricelist_id_eq;//[价格表]
	public void setN_pricelist_id_eq(Long n_pricelist_id_eq) {
        this.n_pricelist_id_eq = n_pricelist_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_eq)){
            this.getSearchCond().eq("pricelist_id", n_pricelist_id_eq);
        }
    }
	private Long n_product_uom_eq;//[计量单位]
	public void setN_product_uom_eq(Long n_product_uom_eq) {
        this.n_product_uom_eq = n_product_uom_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_eq)){
            this.getSearchCond().eq("product_uom", n_product_uom_eq);
        }
    }
	private Long n_country_id_eq;//[客户国家]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



