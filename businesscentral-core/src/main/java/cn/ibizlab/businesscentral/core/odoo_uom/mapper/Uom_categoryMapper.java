package cn.ibizlab.businesscentral.core.odoo_uom.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_categorySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Uom_categoryMapper extends BaseMapper<Uom_category>{

    Page<Uom_category> searchDefault(IPage page, @Param("srf") Uom_categorySearchContext context, @Param("ew") Wrapper<Uom_category> wrapper) ;
    @Override
    Uom_category selectById(Serializable id);
    @Override
    int insert(Uom_category entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Uom_category entity);
    @Override
    int update(@Param(Constants.ENTITY) Uom_category entity, @Param("ew") Wrapper<Uom_category> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Uom_category> selectByCreateUid(@Param("id") Serializable id) ;

    List<Uom_category> selectByWriteUid(@Param("id") Serializable id) ;


}
