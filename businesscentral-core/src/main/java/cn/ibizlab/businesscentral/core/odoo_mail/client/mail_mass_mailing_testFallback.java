package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mass_mailing_test] 服务对象接口
 */
@Component
public class mail_mass_mailing_testFallback implements mail_mass_mailing_testFeignClient{

    public Mail_mass_mailing_test update(Long id, Mail_mass_mailing_test mail_mass_mailing_test){
            return null;
     }
    public Boolean updateBatch(List<Mail_mass_mailing_test> mail_mass_mailing_tests){
            return false;
     }


    public Mail_mass_mailing_test create(Mail_mass_mailing_test mail_mass_mailing_test){
            return null;
     }
    public Boolean createBatch(List<Mail_mass_mailing_test> mail_mass_mailing_tests){
            return false;
     }

    public Page<Mail_mass_mailing_test> search(Mail_mass_mailing_testSearchContext context){
            return null;
     }





    public Mail_mass_mailing_test get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Mail_mass_mailing_test> select(){
            return null;
     }

    public Mail_mass_mailing_test getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_mass_mailing_test mail_mass_mailing_test){
            return false;
     }


    public Boolean save(Mail_mass_mailing_test mail_mass_mailing_test){
            return false;
     }
    public Boolean saveBatch(List<Mail_mass_mailing_test> mail_mass_mailing_tests){
            return false;
     }

    public Page<Mail_mass_mailing_test> searchDefault(Mail_mass_mailing_testSearchContext context){
            return null;
     }


}
