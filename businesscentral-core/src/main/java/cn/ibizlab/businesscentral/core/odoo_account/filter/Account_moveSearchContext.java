package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move;
/**
 * 关系型数据实体[Account_move] 查询条件对象
 */
@Slf4j
@Data
public class Account_moveSearchContext extends QueryWrapperContext<Account_move> {

	private String n_name_like;//[号码]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[业务伙伴]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[业务伙伴]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_reverse_entry_id_text_eq;//[撤销分录]
	public void setN_reverse_entry_id_text_eq(String n_reverse_entry_id_text_eq) {
        this.n_reverse_entry_id_text_eq = n_reverse_entry_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_reverse_entry_id_text_eq)){
            this.getSearchCond().eq("reverse_entry_id_text", n_reverse_entry_id_text_eq);
        }
    }
	private String n_reverse_entry_id_text_like;//[撤销分录]
	public void setN_reverse_entry_id_text_like(String n_reverse_entry_id_text_like) {
        this.n_reverse_entry_id_text_like = n_reverse_entry_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_reverse_entry_id_text_like)){
            this.getSearchCond().like("reverse_entry_id_text", n_reverse_entry_id_text_like);
        }
    }
	private String n_stock_move_id_text_eq;//[库存移动]
	public void setN_stock_move_id_text_eq(String n_stock_move_id_text_eq) {
        this.n_stock_move_id_text_eq = n_stock_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_stock_move_id_text_eq)){
            this.getSearchCond().eq("stock_move_id_text", n_stock_move_id_text_eq);
        }
    }
	private String n_stock_move_id_text_like;//[库存移动]
	public void setN_stock_move_id_text_like(String n_stock_move_id_text_like) {
        this.n_stock_move_id_text_like = n_stock_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_stock_move_id_text_like)){
            this.getSearchCond().like("stock_move_id_text", n_stock_move_id_text_like);
        }
    }
	private String n_journal_id_text_eq;//[日记账]
	public void setN_journal_id_text_eq(String n_journal_id_text_eq) {
        this.n_journal_id_text_eq = n_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_eq)){
            this.getSearchCond().eq("journal_id_text", n_journal_id_text_eq);
        }
    }
	private String n_journal_id_text_like;//[日记账]
	public void setN_journal_id_text_like(String n_journal_id_text_like) {
        this.n_journal_id_text_like = n_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_like)){
            this.getSearchCond().like("journal_id_text", n_journal_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private Long n_stock_move_id_eq;//[库存移动]
	public void setN_stock_move_id_eq(Long n_stock_move_id_eq) {
        this.n_stock_move_id_eq = n_stock_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_stock_move_id_eq)){
            this.getSearchCond().eq("stock_move_id", n_stock_move_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_journal_id_eq;//[日记账]
	public void setN_journal_id_eq(Long n_journal_id_eq) {
        this.n_journal_id_eq = n_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_eq)){
            this.getSearchCond().eq("journal_id", n_journal_id_eq);
        }
    }
	private Long n_tax_cash_basis_rec_id_eq;//[税率现金收付制分录]
	public void setN_tax_cash_basis_rec_id_eq(Long n_tax_cash_basis_rec_id_eq) {
        this.n_tax_cash_basis_rec_id_eq = n_tax_cash_basis_rec_id_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_cash_basis_rec_id_eq)){
            this.getSearchCond().eq("tax_cash_basis_rec_id", n_tax_cash_basis_rec_id_eq);
        }
    }
	private Long n_partner_id_eq;//[业务伙伴]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_reverse_entry_id_eq;//[撤销分录]
	public void setN_reverse_entry_id_eq(Long n_reverse_entry_id_eq) {
        this.n_reverse_entry_id_eq = n_reverse_entry_id_eq;
        if(!ObjectUtils.isEmpty(this.n_reverse_entry_id_eq)){
            this.getSearchCond().eq("reverse_entry_id", n_reverse_entry_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



