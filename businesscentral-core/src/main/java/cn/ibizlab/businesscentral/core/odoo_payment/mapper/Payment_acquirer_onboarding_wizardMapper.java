package cn.ibizlab.businesscentral.core.odoo_payment.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Payment_acquirer_onboarding_wizardMapper extends BaseMapper<Payment_acquirer_onboarding_wizard>{

    Page<Payment_acquirer_onboarding_wizard> searchDefault(IPage page, @Param("srf") Payment_acquirer_onboarding_wizardSearchContext context, @Param("ew") Wrapper<Payment_acquirer_onboarding_wizard> wrapper) ;
    @Override
    Payment_acquirer_onboarding_wizard selectById(Serializable id);
    @Override
    int insert(Payment_acquirer_onboarding_wizard entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Payment_acquirer_onboarding_wizard entity);
    @Override
    int update(@Param(Constants.ENTITY) Payment_acquirer_onboarding_wizard entity, @Param("ew") Wrapper<Payment_acquirer_onboarding_wizard> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Payment_acquirer_onboarding_wizard> selectByCreateUid(@Param("id") Serializable id) ;

    List<Payment_acquirer_onboarding_wizard> selectByWriteUid(@Param("id") Serializable id) ;


}
