package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_warehouse_orderpoint] 服务对象接口
 */
public interface IStock_warehouse_orderpointService extends IService<Stock_warehouse_orderpoint>{

    boolean create(Stock_warehouse_orderpoint et) ;
    void createBatch(List<Stock_warehouse_orderpoint> list) ;
    boolean update(Stock_warehouse_orderpoint et) ;
    void updateBatch(List<Stock_warehouse_orderpoint> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_warehouse_orderpoint get(Long key) ;
    Stock_warehouse_orderpoint getDraft(Stock_warehouse_orderpoint et) ;
    boolean checkKey(Stock_warehouse_orderpoint et) ;
    boolean save(Stock_warehouse_orderpoint et) ;
    void saveBatch(List<Stock_warehouse_orderpoint> list) ;
    Page<Stock_warehouse_orderpoint> searchDefault(Stock_warehouse_orderpointSearchContext context) ;
    List<Stock_warehouse_orderpoint> selectByProductId(Long id);
    void removeByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_warehouse_orderpoint> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_warehouse_orderpoint> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_warehouse_orderpoint> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_warehouse_orderpoint> selectByLocationId(Long id);
    void removeByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_warehouse_orderpoint> selectByWarehouseId(Long id);
    void removeByWarehouseId(Collection<Long> ids);
    void removeByWarehouseId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_warehouse_orderpoint> getStockWarehouseOrderpointByIds(List<Long> ids) ;
    List<Stock_warehouse_orderpoint> getStockWarehouseOrderpointByEntities(List<Stock_warehouse_orderpoint> entities) ;
}


