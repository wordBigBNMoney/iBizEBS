package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_messageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_message] 服务对象接口
 */
public interface IMail_messageService extends IService<Mail_message>{

    boolean create(Mail_message et) ;
    void createBatch(List<Mail_message> list) ;
    boolean update(Mail_message et) ;
    void updateBatch(List<Mail_message> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_message get(Long key) ;
    Mail_message getDraft(Mail_message et) ;
    boolean checkKey(Mail_message et) ;
    Mail_message generate_tracking_message_id(Mail_message et) ;
    Mail_message post_message(Mail_message et) ;
    boolean save(Mail_message et) ;
    void saveBatch(List<Mail_message> list) ;
    Page<Mail_message> searchByRes(Mail_messageSearchContext context) ;
    Page<Mail_message> searchDefault(Mail_messageSearchContext context) ;
    List<Mail_message> selectByMailActivityTypeId(Long id);
    void resetByMailActivityTypeId(Long id);
    void resetByMailActivityTypeId(Collection<Long> ids);
    void removeByMailActivityTypeId(Long id);
    List<Mail_message> selectBySubtypeId(Long id);
    void resetBySubtypeId(Long id);
    void resetBySubtypeId(Collection<Long> ids);
    void removeBySubtypeId(Long id);
    List<Mail_message> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Mail_message> selectByAuthorId(Long id);
    void resetByAuthorId(Long id);
    void resetByAuthorId(Collection<Long> ids);
    void removeByAuthorId(Long id);
    List<Mail_message> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_message> selectByModeratorId(Long id);
    void resetByModeratorId(Long id);
    void resetByModeratorId(Collection<Long> ids);
    void removeByModeratorId(Long id);
    List<Mail_message> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_message> getMailMessageByIds(List<Long> ids) ;
    List<Mail_message> getMailMessageByEntities(List<Mail_message> entities) ;
}


