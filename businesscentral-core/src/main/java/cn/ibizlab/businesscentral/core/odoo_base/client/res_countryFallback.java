package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_countrySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_country] 服务对象接口
 */
@Component
public class res_countryFallback implements res_countryFeignClient{

    public Res_country update(Long id, Res_country res_country){
            return null;
     }
    public Boolean updateBatch(List<Res_country> res_countries){
            return false;
     }




    public Res_country create(Res_country res_country){
            return null;
     }
    public Boolean createBatch(List<Res_country> res_countries){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Res_country> search(Res_countrySearchContext context){
            return null;
     }


    public Res_country get(Long id){
            return null;
     }


    public Page<Res_country> select(){
            return null;
     }

    public Res_country getDraft(){
            return null;
    }



    public Boolean checkKey(Res_country res_country){
            return false;
     }


    public Boolean save(Res_country res_country){
            return false;
     }
    public Boolean saveBatch(List<Res_country> res_countries){
            return false;
     }

    public Page<Res_country> searchDefault(Res_countrySearchContext context){
            return null;
     }


}
