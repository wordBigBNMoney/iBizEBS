package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_supplierinfo;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_supplierinfoSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_supplierinfo] 服务对象接口
 */
public interface IProduct_supplierinfoService extends IService<Product_supplierinfo>{

    boolean create(Product_supplierinfo et) ;
    void createBatch(List<Product_supplierinfo> list) ;
    boolean update(Product_supplierinfo et) ;
    void updateBatch(List<Product_supplierinfo> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_supplierinfo get(Long key) ;
    Product_supplierinfo getDraft(Product_supplierinfo et) ;
    boolean checkKey(Product_supplierinfo et) ;
    boolean save(Product_supplierinfo et) ;
    void saveBatch(List<Product_supplierinfo> list) ;
    Page<Product_supplierinfo> searchDefault(Product_supplierinfoSearchContext context) ;
    List<Product_supplierinfo> selectByPurchaseRequisitionLineId(Long id);
    void removeByPurchaseRequisitionLineId(Collection<Long> ids);
    void removeByPurchaseRequisitionLineId(Long id);
    List<Product_supplierinfo> selectByName(Long id);
    void removeByName(Long id);
    List<Product_supplierinfo> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Product_supplierinfo> selectByProductTmplId(Long id);
    void removeByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Product_supplierinfo> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Product_supplierinfo> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Product_supplierinfo> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_supplierinfo> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_supplierinfo> getProductSupplierinfoByIds(List<Long> ids) ;
    List<Product_supplierinfo> getProductSupplierinfoByEntities(List<Product_supplierinfo> entities) ;
}


