package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[关注消息类型]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_FOLLOWERS_MAIL_MESSAGE_SUBTYPE_REL",resultMap = "Mail_followers_mail_message_subtype_relResultMap")
public class Mail_followers_mail_message_subtype_rel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableField(exist = false)
    @JSONField(name = "id")
    @JsonProperty("id")
    private String id;
    /**
     * ID
     */
    @DEField(name = "mail_message_subtype_id")
    @TableField(value = "mail_message_subtype_id")
    @JSONField(name = "mail_message_subtype_id")
    @JsonProperty("mail_message_subtype_id")
    private Long mailMessageSubtypeId;
    /**
     * ID
     */
    @DEField(name = "mail_followers_id")
    @TableField(value = "mail_followers_id")
    @JSONField(name = "mail_followers_id")
    @JsonProperty("mail_followers_id")
    private Long mailFollowersId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers odooMailFollowers;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message_subtype odooMailMessageSubtype;



    /**
     * 设置 [ID]
     */
    public void setMailMessageSubtypeId(Long mailMessageSubtypeId){
        this.mailMessageSubtypeId = mailMessageSubtypeId ;
        this.modify("mail_message_subtype_id",mailMessageSubtypeId);
    }

    /**
     * 设置 [ID]
     */
    public void setMailFollowersId(Long mailFollowersId){
        this.mailFollowersId = mailFollowersId ;
        this.modify("mail_followers_id",mailFollowersId);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


