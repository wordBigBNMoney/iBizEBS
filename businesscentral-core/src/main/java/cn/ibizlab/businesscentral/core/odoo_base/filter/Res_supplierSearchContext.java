package cn.ibizlab.businesscentral.core.odoo_base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier;
/**
 * 关系型数据实体[Res_supplier] 查询条件对象
 */
@Slf4j
@Data
public class Res_supplierSearchContext extends QueryWrapperContext<Res_supplier> {

	private String n_company_type_eq;//[公司类别]
	public void setN_company_type_eq(String n_company_type_eq) {
        this.n_company_type_eq = n_company_type_eq;
        if(!ObjectUtils.isEmpty(this.n_company_type_eq)){
            this.getSearchCond().eq("company_type", n_company_type_eq);
        }
    }
	private String n_type_eq;//[地址类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_property_purchase_currency_name_eq;//[供应商货币]
	public void setN_property_purchase_currency_name_eq(String n_property_purchase_currency_name_eq) {
        this.n_property_purchase_currency_name_eq = n_property_purchase_currency_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_purchase_currency_name_eq)){
            this.getSearchCond().eq("property_purchase_currency_name", n_property_purchase_currency_name_eq);
        }
    }
	private String n_property_purchase_currency_name_like;//[供应商货币]
	public void setN_property_purchase_currency_name_like(String n_property_purchase_currency_name_like) {
        this.n_property_purchase_currency_name_like = n_property_purchase_currency_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_purchase_currency_name_like)){
            this.getSearchCond().like("property_purchase_currency_name", n_property_purchase_currency_name_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_property_stock_subcontractor_name_eq;//[分包商位置]
	public void setN_property_stock_subcontractor_name_eq(String n_property_stock_subcontractor_name_eq) {
        this.n_property_stock_subcontractor_name_eq = n_property_stock_subcontractor_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_subcontractor_name_eq)){
            this.getSearchCond().eq("property_stock_subcontractor_name", n_property_stock_subcontractor_name_eq);
        }
    }
	private String n_property_stock_subcontractor_name_like;//[分包商位置]
	public void setN_property_stock_subcontractor_name_like(String n_property_stock_subcontractor_name_like) {
        this.n_property_stock_subcontractor_name_like = n_property_stock_subcontractor_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_subcontractor_name_like)){
            this.getSearchCond().like("property_stock_subcontractor_name", n_property_stock_subcontractor_name_like);
        }
    }
	private String n_property_stock_customer_name_eq;//[客户位置]
	public void setN_property_stock_customer_name_eq(String n_property_stock_customer_name_eq) {
        this.n_property_stock_customer_name_eq = n_property_stock_customer_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_customer_name_eq)){
            this.getSearchCond().eq("property_stock_customer_name", n_property_stock_customer_name_eq);
        }
    }
	private String n_property_stock_customer_name_like;//[客户位置]
	public void setN_property_stock_customer_name_like(String n_property_stock_customer_name_like) {
        this.n_property_stock_customer_name_like = n_property_stock_customer_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_customer_name_like)){
            this.getSearchCond().like("property_stock_customer_name", n_property_stock_customer_name_like);
        }
    }
	private String n_property_stock_supplier_name_eq;//[供应商位置]
	public void setN_property_stock_supplier_name_eq(String n_property_stock_supplier_name_eq) {
        this.n_property_stock_supplier_name_eq = n_property_stock_supplier_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_supplier_name_eq)){
            this.getSearchCond().eq("property_stock_supplier_name", n_property_stock_supplier_name_eq);
        }
    }
	private String n_property_stock_supplier_name_like;//[供应商位置]
	public void setN_property_stock_supplier_name_like(String n_property_stock_supplier_name_like) {
        this.n_property_stock_supplier_name_like = n_property_stock_supplier_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_supplier_name_like)){
            this.getSearchCond().like("property_stock_supplier_name", n_property_stock_supplier_name_like);
        }
    }
	private String n_parent_name_eq;//[公司]
	public void setN_parent_name_eq(String n_parent_name_eq) {
        this.n_parent_name_eq = n_parent_name_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_name_eq)){
            this.getSearchCond().eq("parent_name", n_parent_name_eq);
        }
    }
	private String n_parent_name_like;//[公司]
	public void setN_parent_name_like(String n_parent_name_like) {
        this.n_parent_name_like = n_parent_name_like;
        if(!ObjectUtils.isEmpty(this.n_parent_name_like)){
            this.getSearchCond().like("parent_name", n_parent_name_like);
        }
    }
	private String n_property_payment_term_name_eq;//[付款条款]
	public void setN_property_payment_term_name_eq(String n_property_payment_term_name_eq) {
        this.n_property_payment_term_name_eq = n_property_payment_term_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_payment_term_name_eq)){
            this.getSearchCond().eq("property_payment_term_name", n_property_payment_term_name_eq);
        }
    }
	private String n_property_payment_term_name_like;//[付款条款]
	public void setN_property_payment_term_name_like(String n_property_payment_term_name_like) {
        this.n_property_payment_term_name_like = n_property_payment_term_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_payment_term_name_like)){
            this.getSearchCond().like("property_payment_term_name", n_property_payment_term_name_like);
        }
    }
	private String n_property_delivery_carrier_name_eq;//[交货方法]
	public void setN_property_delivery_carrier_name_eq(String n_property_delivery_carrier_name_eq) {
        this.n_property_delivery_carrier_name_eq = n_property_delivery_carrier_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_delivery_carrier_name_eq)){
            this.getSearchCond().eq("property_delivery_carrier_name", n_property_delivery_carrier_name_eq);
        }
    }
	private String n_property_delivery_carrier_name_like;//[交货方法]
	public void setN_property_delivery_carrier_name_like(String n_property_delivery_carrier_name_like) {
        this.n_property_delivery_carrier_name_like = n_property_delivery_carrier_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_delivery_carrier_name_like)){
            this.getSearchCond().like("property_delivery_carrier_name", n_property_delivery_carrier_name_like);
        }
    }
	private String n_property_product_pricelist_name_eq;//[价格表]
	public void setN_property_product_pricelist_name_eq(String n_property_product_pricelist_name_eq) {
        this.n_property_product_pricelist_name_eq = n_property_product_pricelist_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_product_pricelist_name_eq)){
            this.getSearchCond().eq("property_product_pricelist_name", n_property_product_pricelist_name_eq);
        }
    }
	private String n_property_product_pricelist_name_like;//[价格表]
	public void setN_property_product_pricelist_name_like(String n_property_product_pricelist_name_like) {
        this.n_property_product_pricelist_name_like = n_property_product_pricelist_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_product_pricelist_name_like)){
            this.getSearchCond().like("property_product_pricelist_name", n_property_product_pricelist_name_like);
        }
    }
	private String n_country_id_text_eq;//[国家/地区]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[国家/地区]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private String n_property_account_position_name_eq;//[税科目调整]
	public void setN_property_account_position_name_eq(String n_property_account_position_name_eq) {
        this.n_property_account_position_name_eq = n_property_account_position_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_position_name_eq)){
            this.getSearchCond().eq("property_account_position_name", n_property_account_position_name_eq);
        }
    }
	private String n_property_account_position_name_like;//[税科目调整]
	public void setN_property_account_position_name_like(String n_property_account_position_name_like) {
        this.n_property_account_position_name_like = n_property_account_position_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_account_position_name_like)){
            this.getSearchCond().like("property_account_position_name", n_property_account_position_name_like);
        }
    }
	private String n_state_id_text_eq;//[州/省]
	public void setN_state_id_text_eq(String n_state_id_text_eq) {
        this.n_state_id_text_eq = n_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_state_id_text_eq)){
            this.getSearchCond().eq("state_id_text", n_state_id_text_eq);
        }
    }
	private String n_state_id_text_like;//[州/省]
	public void setN_state_id_text_like(String n_state_id_text_like) {
        this.n_state_id_text_like = n_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_state_id_text_like)){
            this.getSearchCond().like("state_id_text", n_state_id_text_like);
        }
    }
	private String n_title_text_eq;//[称谓]
	public void setN_title_text_eq(String n_title_text_eq) {
        this.n_title_text_eq = n_title_text_eq;
        if(!ObjectUtils.isEmpty(this.n_title_text_eq)){
            this.getSearchCond().eq("title_text", n_title_text_eq);
        }
    }
	private String n_title_text_like;//[称谓]
	public void setN_title_text_like(String n_title_text_like) {
        this.n_title_text_like = n_title_text_like;
        if(!ObjectUtils.isEmpty(this.n_title_text_like)){
            this.getSearchCond().like("title_text", n_title_text_like);
        }
    }
	private String n_property_supplier_payment_term_name_eq;//[采购付款条例]
	public void setN_property_supplier_payment_term_name_eq(String n_property_supplier_payment_term_name_eq) {
        this.n_property_supplier_payment_term_name_eq = n_property_supplier_payment_term_name_eq;
        if(!ObjectUtils.isEmpty(this.n_property_supplier_payment_term_name_eq)){
            this.getSearchCond().eq("property_supplier_payment_term_name", n_property_supplier_payment_term_name_eq);
        }
    }
	private String n_property_supplier_payment_term_name_like;//[采购付款条例]
	public void setN_property_supplier_payment_term_name_like(String n_property_supplier_payment_term_name_like) {
        this.n_property_supplier_payment_term_name_like = n_property_supplier_payment_term_name_like;
        if(!ObjectUtils.isEmpty(this.n_property_supplier_payment_term_name_like)){
            this.getSearchCond().like("property_supplier_payment_term_name", n_property_supplier_payment_term_name_like);
        }
    }
	private String n_user_name_eq;//[销售员]
	public void setN_user_name_eq(String n_user_name_eq) {
        this.n_user_name_eq = n_user_name_eq;
        if(!ObjectUtils.isEmpty(this.n_user_name_eq)){
            this.getSearchCond().eq("user_name", n_user_name_eq);
        }
    }
	private String n_user_name_like;//[销售员]
	public void setN_user_name_like(String n_user_name_like) {
        this.n_user_name_like = n_user_name_like;
        if(!ObjectUtils.isEmpty(this.n_user_name_like)){
            this.getSearchCond().like("user_name", n_user_name_like);
        }
    }
	private Long n_state_id_eq;//[ID]
	public void setN_state_id_eq(Long n_state_id_eq) {
        this.n_state_id_eq = n_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_state_id_eq)){
            this.getSearchCond().eq("state_id", n_state_id_eq);
        }
    }
	private Long n_property_purchase_currency_id_eq;//[供应商货币]
	public void setN_property_purchase_currency_id_eq(Long n_property_purchase_currency_id_eq) {
        this.n_property_purchase_currency_id_eq = n_property_purchase_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_purchase_currency_id_eq)){
            this.getSearchCond().eq("property_purchase_currency_id", n_property_purchase_currency_id_eq);
        }
    }
	private Long n_property_supplier_payment_term_id_eq;//[采购付款条例]
	public void setN_property_supplier_payment_term_id_eq(Long n_property_supplier_payment_term_id_eq) {
        this.n_property_supplier_payment_term_id_eq = n_property_supplier_payment_term_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_supplier_payment_term_id_eq)){
            this.getSearchCond().eq("property_supplier_payment_term_id", n_property_supplier_payment_term_id_eq);
        }
    }
	private Long n_property_payment_term_id_eq;//[销售付款条款]
	public void setN_property_payment_term_id_eq(Long n_property_payment_term_id_eq) {
        this.n_property_payment_term_id_eq = n_property_payment_term_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_payment_term_id_eq)){
            this.getSearchCond().eq("property_payment_term_id", n_property_payment_term_id_eq);
        }
    }
	private Long n_country_id_eq;//[ID]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }
	private Long n_property_stock_supplier_eq;//[供应商位置]
	public void setN_property_stock_supplier_eq(Long n_property_stock_supplier_eq) {
        this.n_property_stock_supplier_eq = n_property_stock_supplier_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_supplier_eq)){
            this.getSearchCond().eq("property_stock_supplier", n_property_stock_supplier_eq);
        }
    }
	private Long n_property_delivery_carrier_id_eq;//[交货方法]
	public void setN_property_delivery_carrier_id_eq(Long n_property_delivery_carrier_id_eq) {
        this.n_property_delivery_carrier_id_eq = n_property_delivery_carrier_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_delivery_carrier_id_eq)){
            this.getSearchCond().eq("property_delivery_carrier_id", n_property_delivery_carrier_id_eq);
        }
    }
	private Long n_title_eq;//[ID]
	public void setN_title_eq(Long n_title_eq) {
        this.n_title_eq = n_title_eq;
        if(!ObjectUtils.isEmpty(this.n_title_eq)){
            this.getSearchCond().eq("title", n_title_eq);
        }
    }
	private Long n_property_account_position_id_eq;//[税科目调整]
	public void setN_property_account_position_id_eq(Long n_property_account_position_id_eq) {
        this.n_property_account_position_id_eq = n_property_account_position_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_position_id_eq)){
            this.getSearchCond().eq("property_account_position_id", n_property_account_position_id_eq);
        }
    }
	private Long n_parent_id_eq;//[ID]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_property_stock_customer_eq;//[客户位置]
	public void setN_property_stock_customer_eq(Long n_property_stock_customer_eq) {
        this.n_property_stock_customer_eq = n_property_stock_customer_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_customer_eq)){
            this.getSearchCond().eq("property_stock_customer", n_property_stock_customer_eq);
        }
    }
	private Long n_user_id_eq;//[销售员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_property_stock_subcontractor_eq;//[分包商位置]
	public void setN_property_stock_subcontractor_eq(Long n_property_stock_subcontractor_eq) {
        this.n_property_stock_subcontractor_eq = n_property_stock_subcontractor_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_subcontractor_eq)){
            this.getSearchCond().eq("property_stock_subcontractor", n_property_stock_subcontractor_eq);
        }
    }
	private Long n_company_id_eq;//[ID]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_property_product_pricelist_eq;//[价格表]
	public void setN_property_product_pricelist_eq(Long n_property_product_pricelist_eq) {
        this.n_property_product_pricelist_eq = n_property_product_pricelist_eq;
        if(!ObjectUtils.isEmpty(this.n_property_product_pricelist_eq)){
            this.getSearchCond().eq("property_product_pricelist", n_property_product_pricelist_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



