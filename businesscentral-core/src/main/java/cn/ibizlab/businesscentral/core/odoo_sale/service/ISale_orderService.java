package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_orderSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_order] 服务对象接口
 */
public interface ISale_orderService extends IService<Sale_order>{

    boolean create(Sale_order et) ;
    void createBatch(List<Sale_order> list) ;
    boolean update(Sale_order et) ;
    void updateBatch(List<Sale_order> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_order get(Long key) ;
    Sale_order getDraft(Sale_order et) ;
    boolean checkKey(Sale_order et) ;
    boolean save(Sale_order et) ;
    void saveBatch(List<Sale_order> list) ;
    Page<Sale_order> searchDefault(Sale_orderSearchContext context) ;
    List<Sale_order> selectByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Collection<Long> ids);
    void removeByAnalyticAccountId(Long id);
    List<Sale_order> selectByFiscalPositionId(Long id);
    void resetByFiscalPositionId(Long id);
    void resetByFiscalPositionId(Collection<Long> ids);
    void removeByFiscalPositionId(Long id);
    List<Sale_order> selectByIncoterm(Long id);
    void resetByIncoterm(Long id);
    void resetByIncoterm(Collection<Long> ids);
    void removeByIncoterm(Long id);
    List<Sale_order> selectByPaymentTermId(Long id);
    void resetByPaymentTermId(Long id);
    void resetByPaymentTermId(Collection<Long> ids);
    void removeByPaymentTermId(Long id);
    List<Sale_order> selectByOpportunityId(Long id);
    void resetByOpportunityId(Long id);
    void resetByOpportunityId(Collection<Long> ids);
    void removeByOpportunityId(Long id);
    List<Sale_order> selectByTeamId(Long id);
    void resetByTeamId(Long id);
    void resetByTeamId(Collection<Long> ids);
    void removeByTeamId(Long id);
    List<Sale_order> selectByPricelistId(Long id);
    void resetByPricelistId(Long id);
    void resetByPricelistId(Collection<Long> ids);
    void removeByPricelistId(Long id);
    List<Sale_order> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Sale_order> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Sale_order> selectByPartnerInvoiceId(Long id);
    void resetByPartnerInvoiceId(Long id);
    void resetByPartnerInvoiceId(Collection<Long> ids);
    void removeByPartnerInvoiceId(Long id);
    List<Sale_order> selectByPartnerShippingId(Long id);
    void resetByPartnerShippingId(Long id);
    void resetByPartnerShippingId(Collection<Long> ids);
    void removeByPartnerShippingId(Long id);
    List<Sale_order> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sale_order> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Sale_order> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Sale_order> selectBySaleOrderTemplateId(Long id);
    void resetBySaleOrderTemplateId(Long id);
    void resetBySaleOrderTemplateId(Collection<Long> ids);
    void removeBySaleOrderTemplateId(Long id);
    List<Sale_order> selectByWarehouseId(Long id);
    void resetByWarehouseId(Long id);
    void resetByWarehouseId(Collection<Long> ids);
    void removeByWarehouseId(Long id);
    List<Sale_order> selectByCampaignId(Long id);
    void resetByCampaignId(Long id);
    void resetByCampaignId(Collection<Long> ids);
    void removeByCampaignId(Long id);
    List<Sale_order> selectByMediumId(Long id);
    void resetByMediumId(Long id);
    void resetByMediumId(Collection<Long> ids);
    void removeByMediumId(Long id);
    List<Sale_order> selectBySourceId(Long id);
    void resetBySourceId(Long id);
    void resetBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sale_order> getSaleOrderByIds(List<Long> ids) ;
    List<Sale_order> getSaleOrderByEntities(List<Sale_order> entities) ;
}


