package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_journalSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_journal] 服务对象接口
 */
public interface IAccount_journalService extends IService<Account_journal>{

    boolean create(Account_journal et) ;
    void createBatch(List<Account_journal> list) ;
    boolean update(Account_journal et) ;
    void updateBatch(List<Account_journal> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_journal get(Long key) ;
    Account_journal getDraft(Account_journal et) ;
    boolean checkKey(Account_journal et) ;
    boolean save(Account_journal et) ;
    void saveBatch(List<Account_journal> list) ;
    Page<Account_journal> searchDefault(Account_journalSearchContext context) ;
    List<Account_journal> selectByDefaultCreditAccountId(Long id);
    void resetByDefaultCreditAccountId(Long id);
    void resetByDefaultCreditAccountId(Collection<Long> ids);
    void removeByDefaultCreditAccountId(Long id);
    List<Account_journal> selectByDefaultDebitAccountId(Long id);
    void resetByDefaultDebitAccountId(Long id);
    void resetByDefaultDebitAccountId(Collection<Long> ids);
    void removeByDefaultDebitAccountId(Long id);
    List<Account_journal> selectByLossAccountId(Long id);
    void resetByLossAccountId(Long id);
    void resetByLossAccountId(Collection<Long> ids);
    void removeByLossAccountId(Long id);
    List<Account_journal> selectByProfitAccountId(Long id);
    void resetByProfitAccountId(Long id);
    void resetByProfitAccountId(Collection<Long> ids);
    void removeByProfitAccountId(Long id);
    List<Account_journal> selectByAliasId(Long id);
    void resetByAliasId(Long id);
    void resetByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    List<Account_journal> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_journal> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_journal> selectByBankAccountId(Long id);
    List<Account_journal> selectByBankAccountId(Collection<Long> ids);
    void removeByBankAccountId(Long id);
    List<Account_journal> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_journal> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_journal> getAccountJournalByIds(List<Long> ids) ;
    List<Account_journal> getAccountJournalByEntities(List<Account_journal> entities) ;
}


