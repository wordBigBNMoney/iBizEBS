package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_fixed_putaway_strat] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-fixed-putaway-strat", fallback = stock_fixed_putaway_stratFallback.class)
public interface stock_fixed_putaway_stratFeignClient {



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_fixed_putaway_strats/{id}")
    Stock_fixed_putaway_strat update(@PathVariable("id") Long id,@RequestBody Stock_fixed_putaway_strat stock_fixed_putaway_strat);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_fixed_putaway_strats/batch")
    Boolean updateBatch(@RequestBody List<Stock_fixed_putaway_strat> stock_fixed_putaway_strats);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats")
    Stock_fixed_putaway_strat create(@RequestBody Stock_fixed_putaway_strat stock_fixed_putaway_strat);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/batch")
    Boolean createBatch(@RequestBody List<Stock_fixed_putaway_strat> stock_fixed_putaway_strats);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_fixed_putaway_strats/{id}")
    Stock_fixed_putaway_strat get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/search")
    Page<Stock_fixed_putaway_strat> search(@RequestBody Stock_fixed_putaway_stratSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_fixed_putaway_strats/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_fixed_putaway_strats/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_fixed_putaway_strats/select")
    Page<Stock_fixed_putaway_strat> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_fixed_putaway_strats/getdraft")
    Stock_fixed_putaway_strat getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/checkkey")
    Boolean checkKey(@RequestBody Stock_fixed_putaway_strat stock_fixed_putaway_strat);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/save")
    Boolean save(@RequestBody Stock_fixed_putaway_strat stock_fixed_putaway_strat);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_fixed_putaway_strat> stock_fixed_putaway_strats);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/searchdefault")
    Page<Stock_fixed_putaway_strat> searchDefault(@RequestBody Stock_fixed_putaway_stratSearchContext context);


}
