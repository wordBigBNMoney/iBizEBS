package cn.ibizlab.businesscentral.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_orderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[lunch_order] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-lunch:odoo-lunch}", contextId = "lunch-order", fallback = lunch_orderFallback.class)
public interface lunch_orderFeignClient {



    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_orders/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_orders/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_orders")
    Lunch_order create(@RequestBody Lunch_order lunch_order);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/batch")
    Boolean createBatch(@RequestBody List<Lunch_order> lunch_orders);



    @RequestMapping(method = RequestMethod.GET, value = "/lunch_orders/{id}")
    Lunch_order get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/search")
    Page<Lunch_order> search(@RequestBody Lunch_orderSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_orders/{id}")
    Lunch_order update(@PathVariable("id") Long id,@RequestBody Lunch_order lunch_order);

    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_orders/batch")
    Boolean updateBatch(@RequestBody List<Lunch_order> lunch_orders);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_orders/select")
    Page<Lunch_order> select();


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_orders/getdraft")
    Lunch_order getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/checkkey")
    Boolean checkKey(@RequestBody Lunch_order lunch_order);


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/save")
    Boolean save(@RequestBody Lunch_order lunch_order);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/savebatch")
    Boolean saveBatch(@RequestBody List<Lunch_order> lunch_orders);



    @RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/searchdefault")
    Page<Lunch_order> searchDefault(@RequestBody Lunch_orderSearchContext context);


}
