package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_account_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_account_templateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_fiscal_position_account_template] 服务对象接口
 */
@Component
public class account_fiscal_position_account_templateFallback implements account_fiscal_position_account_templateFeignClient{


    public Account_fiscal_position_account_template create(Account_fiscal_position_account_template account_fiscal_position_account_template){
            return null;
     }
    public Boolean createBatch(List<Account_fiscal_position_account_template> account_fiscal_position_account_templates){
            return false;
     }


    public Page<Account_fiscal_position_account_template> search(Account_fiscal_position_account_templateSearchContext context){
            return null;
     }


    public Account_fiscal_position_account_template update(Long id, Account_fiscal_position_account_template account_fiscal_position_account_template){
            return null;
     }
    public Boolean updateBatch(List<Account_fiscal_position_account_template> account_fiscal_position_account_templates){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_fiscal_position_account_template get(Long id){
            return null;
     }


    public Page<Account_fiscal_position_account_template> select(){
            return null;
     }

    public Account_fiscal_position_account_template getDraft(){
            return null;
    }



    public Boolean checkKey(Account_fiscal_position_account_template account_fiscal_position_account_template){
            return false;
     }


    public Boolean save(Account_fiscal_position_account_template account_fiscal_position_account_template){
            return false;
     }
    public Boolean saveBatch(List<Account_fiscal_position_account_template> account_fiscal_position_account_templates){
            return false;
     }

    public Page<Account_fiscal_position_account_template> searchDefault(Account_fiscal_position_account_templateSearchContext context){
            return null;
     }


}
