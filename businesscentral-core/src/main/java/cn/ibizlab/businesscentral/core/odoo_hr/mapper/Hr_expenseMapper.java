package cn.ibizlab.businesscentral.core.odoo_hr.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expenseSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Hr_expenseMapper extends BaseMapper<Hr_expense>{

    Page<Hr_expense> searchDefault(IPage page, @Param("srf") Hr_expenseSearchContext context, @Param("ew") Wrapper<Hr_expense> wrapper) ;
    @Override
    Hr_expense selectById(Serializable id);
    @Override
    int insert(Hr_expense entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Hr_expense entity);
    @Override
    int update(@Param(Constants.ENTITY) Hr_expense entity, @Param("ew") Wrapper<Hr_expense> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Hr_expense> selectByAccountId(@Param("id") Serializable id) ;

    List<Hr_expense> selectByAnalyticAccountId(@Param("id") Serializable id) ;

    List<Hr_expense> selectByEmployeeId(@Param("id") Serializable id) ;

    List<Hr_expense> selectBySheetId(@Param("id") Serializable id) ;

    List<Hr_expense> selectByProductId(@Param("id") Serializable id) ;

    List<Hr_expense> selectByCompanyId(@Param("id") Serializable id) ;

    List<Hr_expense> selectByCompanyCurrencyId(@Param("id") Serializable id) ;

    List<Hr_expense> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Hr_expense> selectByCreateUid(@Param("id") Serializable id) ;

    List<Hr_expense> selectByWriteUid(@Param("id") Serializable id) ;

    List<Hr_expense> selectBySaleOrderId(@Param("id") Serializable id) ;

    List<Hr_expense> selectByProductUomId(@Param("id") Serializable id) ;


}
