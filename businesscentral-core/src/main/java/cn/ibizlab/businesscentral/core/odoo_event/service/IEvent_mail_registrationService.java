package cn.ibizlab.businesscentral.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_mail_registrationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Event_mail_registration] 服务对象接口
 */
public interface IEvent_mail_registrationService extends IService<Event_mail_registration>{

    boolean create(Event_mail_registration et) ;
    void createBatch(List<Event_mail_registration> list) ;
    boolean update(Event_mail_registration et) ;
    void updateBatch(List<Event_mail_registration> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Event_mail_registration get(Long key) ;
    Event_mail_registration getDraft(Event_mail_registration et) ;
    boolean checkKey(Event_mail_registration et) ;
    boolean save(Event_mail_registration et) ;
    void saveBatch(List<Event_mail_registration> list) ;
    Page<Event_mail_registration> searchDefault(Event_mail_registrationSearchContext context) ;
    List<Event_mail_registration> selectBySchedulerId(Long id);
    void removeBySchedulerId(Collection<Long> ids);
    void removeBySchedulerId(Long id);
    List<Event_mail_registration> selectByRegistrationId(Long id);
    void removeByRegistrationId(Collection<Long> ids);
    void removeByRegistrationId(Long id);
    List<Event_mail_registration> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Event_mail_registration> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Event_mail_registration> getEventMailRegistrationByIds(List<Long> ids) ;
    List<Event_mail_registration> getEventMailRegistrationByEntities(List<Event_mail_registration> entities) ;
}


