package cn.ibizlab.businesscentral.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_transactionSearchContext;
import cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_transactionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_payment.mapper.Payment_transactionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[付款交易] 服务对象接口实现
 */
@Slf4j
@Service("Payment_transactionServiceImpl")
public class Payment_transactionServiceImpl extends EBSServiceImpl<Payment_transactionMapper, Payment_transaction> implements IPayment_transactionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_paymentService accountPaymentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_acquirerService paymentAcquirerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_tokenService paymentTokenService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "payment.transaction" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Payment_transaction et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPayment_transactionService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Payment_transaction> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Payment_transaction et) {
        Payment_transaction old = new Payment_transaction() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPayment_transactionService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPayment_transactionService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Payment_transaction> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountPaymentService.resetByPaymentTransactionId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountPaymentService.resetByPaymentTransactionId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Payment_transaction get(Long key) {
        Payment_transaction et = getById(key);
        if(et==null){
            et=new Payment_transaction();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Payment_transaction getDraft(Payment_transaction et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Payment_transaction et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Payment_transaction et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Payment_transaction et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Payment_transaction> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Payment_transaction> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Payment_transaction> selectByPaymentId(Long id) {
        return baseMapper.selectByPaymentId(id);
    }
    @Override
    public void resetByPaymentId(Long id) {
        this.update(new UpdateWrapper<Payment_transaction>().set("payment_id",null).eq("payment_id",id));
    }

    @Override
    public void resetByPaymentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Payment_transaction>().set("payment_id",null).in("payment_id",ids));
    }

    @Override
    public void removeByPaymentId(Long id) {
        this.remove(new QueryWrapper<Payment_transaction>().eq("payment_id",id));
    }

	@Override
    public List<Payment_transaction> selectByAcquirerId(Long id) {
        return baseMapper.selectByAcquirerId(id);
    }
    @Override
    public void resetByAcquirerId(Long id) {
        this.update(new UpdateWrapper<Payment_transaction>().set("acquirer_id",null).eq("acquirer_id",id));
    }

    @Override
    public void resetByAcquirerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Payment_transaction>().set("acquirer_id",null).in("acquirer_id",ids));
    }

    @Override
    public void removeByAcquirerId(Long id) {
        this.remove(new QueryWrapper<Payment_transaction>().eq("acquirer_id",id));
    }

	@Override
    public List<Payment_transaction> selectByPaymentTokenId(Long id) {
        return baseMapper.selectByPaymentTokenId(id);
    }
    @Override
    public void resetByPaymentTokenId(Long id) {
        this.update(new UpdateWrapper<Payment_transaction>().set("payment_token_id",null).eq("payment_token_id",id));
    }

    @Override
    public void resetByPaymentTokenId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Payment_transaction>().set("payment_token_id",null).in("payment_token_id",ids));
    }

    @Override
    public void removeByPaymentTokenId(Long id) {
        this.remove(new QueryWrapper<Payment_transaction>().eq("payment_token_id",id));
    }

	@Override
    public List<Payment_transaction> selectByPartnerCountryId(Long id) {
        return baseMapper.selectByPartnerCountryId(id);
    }
    @Override
    public void resetByPartnerCountryId(Long id) {
        this.update(new UpdateWrapper<Payment_transaction>().set("partner_country_id",null).eq("partner_country_id",id));
    }

    @Override
    public void resetByPartnerCountryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Payment_transaction>().set("partner_country_id",null).in("partner_country_id",ids));
    }

    @Override
    public void removeByPartnerCountryId(Long id) {
        this.remove(new QueryWrapper<Payment_transaction>().eq("partner_country_id",id));
    }

	@Override
    public List<Payment_transaction> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Payment_transaction>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Payment_transaction>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Payment_transaction>().eq("currency_id",id));
    }

	@Override
    public List<Payment_transaction> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Payment_transaction>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Payment_transaction>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Payment_transaction>().eq("partner_id",id));
    }

	@Override
    public List<Payment_transaction> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Payment_transaction>().eq("create_uid",id));
    }

	@Override
    public List<Payment_transaction> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Payment_transaction>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Payment_transaction> searchDefault(Payment_transactionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Payment_transaction> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Payment_transaction>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Payment_transaction et){
        //实体关系[DER1N_PAYMENT_TRANSACTION__ACCOUNT_PAYMENT__PAYMENT_ID]
        if(!ObjectUtils.isEmpty(et.getPaymentId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment odooPayment=et.getOdooPayment();
            if(ObjectUtils.isEmpty(odooPayment)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment majorEntity=accountPaymentService.get(et.getPaymentId());
                et.setOdooPayment(majorEntity);
                odooPayment=majorEntity;
            }
            et.setPaymentIdText(odooPayment.getName());
        }
        //实体关系[DER1N_PAYMENT_TRANSACTION__PAYMENT_ACQUIRER__ACQUIRER_ID]
        if(!ObjectUtils.isEmpty(et.getAcquirerId())){
            cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer odooAcquirer=et.getOdooAcquirer();
            if(ObjectUtils.isEmpty(odooAcquirer)){
                cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer majorEntity=paymentAcquirerService.get(et.getAcquirerId());
                et.setOdooAcquirer(majorEntity);
                odooAcquirer=majorEntity;
            }
            et.setProvider(odooAcquirer.getProvider());
            et.setAcquirerIdText(odooAcquirer.getName());
        }
        //实体关系[DER1N_PAYMENT_TRANSACTION__PAYMENT_TOKEN__PAYMENT_TOKEN_ID]
        if(!ObjectUtils.isEmpty(et.getPaymentTokenId())){
            cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_token odooPaymentToken=et.getOdooPaymentToken();
            if(ObjectUtils.isEmpty(odooPaymentToken)){
                cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_token majorEntity=paymentTokenService.get(et.getPaymentTokenId());
                et.setOdooPaymentToken(majorEntity);
                odooPaymentToken=majorEntity;
            }
            et.setPaymentTokenIdText(odooPaymentToken.getName());
        }
        //实体关系[DER1N_PAYMENT_TRANSACTION__RES_COUNTRY__PARTNER_COUNTRY_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerCountryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooPartnerCountry=et.getOdooPartnerCountry();
            if(ObjectUtils.isEmpty(odooPartnerCountry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getPartnerCountryId());
                et.setOdooPartnerCountry(majorEntity);
                odooPartnerCountry=majorEntity;
            }
            et.setPartnerCountryIdText(odooPartnerCountry.getName());
        }
        //实体关系[DER1N_PAYMENT_TRANSACTION__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_PAYMENT_TRANSACTION__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_PAYMENT_TRANSACTION__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_PAYMENT_TRANSACTION__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Payment_transaction> getPaymentTransactionByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Payment_transaction> getPaymentTransactionByEntities(List<Payment_transaction> entities) {
        List ids =new ArrayList();
        for(Payment_transaction entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



