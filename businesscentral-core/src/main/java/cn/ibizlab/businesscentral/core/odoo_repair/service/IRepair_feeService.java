package cn.ibizlab.businesscentral.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_feeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Repair_fee] 服务对象接口
 */
public interface IRepair_feeService extends IService<Repair_fee>{

    boolean create(Repair_fee et) ;
    void createBatch(List<Repair_fee> list) ;
    boolean update(Repair_fee et) ;
    void updateBatch(List<Repair_fee> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Repair_fee get(Long key) ;
    Repair_fee getDraft(Repair_fee et) ;
    boolean checkKey(Repair_fee et) ;
    boolean save(Repair_fee et) ;
    void saveBatch(List<Repair_fee> list) ;
    Page<Repair_fee> searchDefault(Repair_feeSearchContext context) ;
    List<Repair_fee> selectByInvoiceLineId(Long id);
    void resetByInvoiceLineId(Long id);
    void resetByInvoiceLineId(Collection<Long> ids);
    void removeByInvoiceLineId(Long id);
    List<Repair_fee> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Repair_fee> selectByRepairId(Long id);
    void removeByRepairId(Collection<Long> ids);
    void removeByRepairId(Long id);
    List<Repair_fee> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Repair_fee> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Repair_fee> selectByProductUom(Long id);
    void resetByProductUom(Long id);
    void resetByProductUom(Collection<Long> ids);
    void removeByProductUom(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Repair_fee> getRepairFeeByIds(List<Long> ids) ;
    List<Repair_fee> getRepairFeeByEntities(List<Repair_fee> entities) ;
}


