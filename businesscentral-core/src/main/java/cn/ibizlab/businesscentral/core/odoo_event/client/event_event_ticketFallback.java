package cn.ibizlab.businesscentral.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_event_ticketSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_event_ticket] 服务对象接口
 */
@Component
public class event_event_ticketFallback implements event_event_ticketFeignClient{

    public Event_event_ticket create(Event_event_ticket event_event_ticket){
            return null;
     }
    public Boolean createBatch(List<Event_event_ticket> event_event_tickets){
            return false;
     }

    public Event_event_ticket get(Long id){
            return null;
     }


    public Event_event_ticket update(Long id, Event_event_ticket event_event_ticket){
            return null;
     }
    public Boolean updateBatch(List<Event_event_ticket> event_event_tickets){
            return false;
     }



    public Page<Event_event_ticket> search(Event_event_ticketSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Page<Event_event_ticket> select(){
            return null;
     }

    public Event_event_ticket getDraft(){
            return null;
    }



    public Boolean checkKey(Event_event_ticket event_event_ticket){
            return false;
     }


    public Boolean save(Event_event_ticket event_event_ticket){
            return false;
     }
    public Boolean saveBatch(List<Event_event_ticket> event_event_tickets){
            return false;
     }

    public Page<Event_event_ticket> searchDefault(Event_event_ticketSearchContext context){
            return null;
     }


}
