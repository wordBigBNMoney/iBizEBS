package cn.ibizlab.businesscentral.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_surveySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_survey] 服务对象接口
 */
@Component
public class survey_surveyFallback implements survey_surveyFeignClient{

    public Survey_survey create(Survey_survey survey_survey){
            return null;
     }
    public Boolean createBatch(List<Survey_survey> survey_surveys){
            return false;
     }

    public Page<Survey_survey> search(Survey_surveySearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Survey_survey update(Long id, Survey_survey survey_survey){
            return null;
     }
    public Boolean updateBatch(List<Survey_survey> survey_surveys){
            return false;
     }




    public Survey_survey get(Long id){
            return null;
     }


    public Page<Survey_survey> select(){
            return null;
     }

    public Survey_survey getDraft(){
            return null;
    }



    public Boolean checkKey(Survey_survey survey_survey){
            return false;
     }


    public Boolean save(Survey_survey survey_survey){
            return false;
     }
    public Boolean saveBatch(List<Survey_survey> survey_surveys){
            return false;
     }

    public Page<Survey_survey> searchDefault(Survey_surveySearchContext context){
            return null;
     }


}
