package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_applicantSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_applicant] 服务对象接口
 */
public interface IHr_applicantService extends IService<Hr_applicant>{

    boolean create(Hr_applicant et) ;
    void createBatch(List<Hr_applicant> list) ;
    boolean update(Hr_applicant et) ;
    void updateBatch(List<Hr_applicant> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_applicant get(Long key) ;
    Hr_applicant getDraft(Hr_applicant et) ;
    boolean checkKey(Hr_applicant et) ;
    boolean save(Hr_applicant et) ;
    void saveBatch(List<Hr_applicant> list) ;
    Page<Hr_applicant> searchDefault(Hr_applicantSearchContext context) ;
    List<Hr_applicant> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Hr_applicant> selectByEmpId(Long id);
    void resetByEmpId(Long id);
    void resetByEmpId(Collection<Long> ids);
    void removeByEmpId(Long id);
    List<Hr_applicant> selectByJobId(Long id);
    void resetByJobId(Long id);
    void resetByJobId(Collection<Long> ids);
    void removeByJobId(Long id);
    List<Hr_applicant> selectByTypeId(Long id);
    void resetByTypeId(Long id);
    void resetByTypeId(Collection<Long> ids);
    void removeByTypeId(Long id);
    List<Hr_applicant> selectByLastStageId(Long id);
    void resetByLastStageId(Long id);
    void resetByLastStageId(Collection<Long> ids);
    void removeByLastStageId(Long id);
    List<Hr_applicant> selectByStageId(Long id);
    List<Hr_applicant> selectByStageId(Collection<Long> ids);
    void removeByStageId(Long id);
    List<Hr_applicant> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Hr_applicant> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Hr_applicant> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_applicant> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Hr_applicant> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Hr_applicant> selectByCampaignId(Long id);
    void resetByCampaignId(Long id);
    void resetByCampaignId(Collection<Long> ids);
    void removeByCampaignId(Long id);
    List<Hr_applicant> selectByMediumId(Long id);
    void resetByMediumId(Long id);
    void resetByMediumId(Collection<Long> ids);
    void removeByMediumId(Long id);
    List<Hr_applicant> selectBySourceId(Long id);
    void resetBySourceId(Long id);
    void resetBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_applicant> getHrApplicantByIds(List<Long> ids) ;
    List<Hr_applicant> getHrApplicantByEntities(List<Hr_applicant> entities) ;
}


