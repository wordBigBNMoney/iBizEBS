package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[EMail别名]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_ALIAS",resultMap = "Mail_aliasResultMap")
public class Mail_alias extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 上级记录ID
     */
    @DEField(name = "alias_parent_thread_id")
    @TableField(value = "alias_parent_thread_id")
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;
    /**
     * 网域别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;
    /**
     * 模型别名
     */
    @DEField(name = "alias_model_id")
    @TableField(value = "alias_model_id")
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;
    /**
     * 默认值
     */
    @DEField(name = "alias_defaults")
    @TableField(value = "alias_defaults")
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 别名
     */
    @DEField(name = "alias_name")
    @TableField(value = "alias_name")
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;
    /**
     * 记录线索ID
     */
    @DEField(name = "alias_force_thread_id")
    @TableField(value = "alias_force_thread_id")
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 上级模型
     */
    @DEField(name = "alias_parent_model_id")
    @TableField(value = "alias_parent_model_id")
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;
    /**
     * 安全联系人别名
     */
    @DEField(name = "alias_contact")
    @TableField(value = "alias_contact")
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "alias_user_id_text")
    @JsonProperty("alias_user_id_text")
    private String aliasUserIdText;
    /**
     * 所有者
     */
    @DEField(name = "alias_user_id")
    @TableField(value = "alias_user_id")
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Long aliasUserId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooAliasUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [上级记录ID]
     */
    public void setAliasParentThreadId(Integer aliasParentThreadId){
        this.aliasParentThreadId = aliasParentThreadId ;
        this.modify("alias_parent_thread_id",aliasParentThreadId);
    }

    /**
     * 设置 [模型别名]
     */
    public void setAliasModelId(Integer aliasModelId){
        this.aliasModelId = aliasModelId ;
        this.modify("alias_model_id",aliasModelId);
    }

    /**
     * 设置 [默认值]
     */
    public void setAliasDefaults(String aliasDefaults){
        this.aliasDefaults = aliasDefaults ;
        this.modify("alias_defaults",aliasDefaults);
    }

    /**
     * 设置 [别名]
     */
    public void setAliasName(String aliasName){
        this.aliasName = aliasName ;
        this.modify("alias_name",aliasName);
    }

    /**
     * 设置 [记录线索ID]
     */
    public void setAliasForceThreadId(Integer aliasForceThreadId){
        this.aliasForceThreadId = aliasForceThreadId ;
        this.modify("alias_force_thread_id",aliasForceThreadId);
    }

    /**
     * 设置 [上级模型]
     */
    public void setAliasParentModelId(Integer aliasParentModelId){
        this.aliasParentModelId = aliasParentModelId ;
        this.modify("alias_parent_model_id",aliasParentModelId);
    }

    /**
     * 设置 [安全联系人别名]
     */
    public void setAliasContact(String aliasContact){
        this.aliasContact = aliasContact ;
        this.modify("alias_contact",aliasContact);
    }

    /**
     * 设置 [所有者]
     */
    public void setAliasUserId(Long aliasUserId){
        this.aliasUserId = aliasUserId ;
        this.modify("alias_user_id",aliasUserId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


