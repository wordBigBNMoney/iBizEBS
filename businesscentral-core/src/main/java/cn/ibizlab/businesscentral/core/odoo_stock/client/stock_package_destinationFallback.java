package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_package_destinationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_package_destination] 服务对象接口
 */
@Component
public class stock_package_destinationFallback implements stock_package_destinationFeignClient{

    public Stock_package_destination update(Long id, Stock_package_destination stock_package_destination){
            return null;
     }
    public Boolean updateBatch(List<Stock_package_destination> stock_package_destinations){
            return false;
     }


    public Page<Stock_package_destination> search(Stock_package_destinationSearchContext context){
            return null;
     }



    public Stock_package_destination get(Long id){
            return null;
     }


    public Stock_package_destination create(Stock_package_destination stock_package_destination){
            return null;
     }
    public Boolean createBatch(List<Stock_package_destination> stock_package_destinations){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Page<Stock_package_destination> select(){
            return null;
     }

    public Stock_package_destination getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_package_destination stock_package_destination){
            return false;
     }


    public Boolean save(Stock_package_destination stock_package_destination){
            return false;
     }
    public Boolean saveBatch(List<Stock_package_destination> stock_package_destinations){
            return false;
     }

    public Page<Stock_package_destination> searchDefault(Stock_package_destinationSearchContext context){
            return null;
     }


}
