package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_productSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_product] 服务对象接口
 */
public interface IProduct_productService extends IService<Product_product>{

    boolean create(Product_product et) ;
    void createBatch(List<Product_product> list) ;
    boolean update(Product_product et) ;
    void updateBatch(List<Product_product> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_product get(Long key) ;
    Product_product getDraft(Product_product et) ;
    boolean checkKey(Product_product et) ;
    boolean save(Product_product et) ;
    void saveBatch(List<Product_product> list) ;
    Page<Product_product> searchDefault(Product_productSearchContext context) ;
    Page<Product_product> searchMaster(Product_productSearchContext context) ;
    List<Product_product> selectByProductTmplId(Long id);
    void removeByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Product_product> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_product> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_product> getProductProductByIds(List<Long> ids) ;
    List<Product_product> getProductProductByEntities(List<Product_product> entities) ;
}


