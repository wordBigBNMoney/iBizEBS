package cn.ibizlab.businesscentral.core.odoo_mail.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_notificationSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mail_notificationMapper extends BaseMapper<Mail_notification>{

    Page<Mail_notification> searchDefault(IPage page, @Param("srf") Mail_notificationSearchContext context, @Param("ew") Wrapper<Mail_notification> wrapper) ;
    @Override
    Mail_notification selectById(Serializable id);
    @Override
    int insert(Mail_notification entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mail_notification entity);
    @Override
    int update(@Param(Constants.ENTITY) Mail_notification entity, @Param("ew") Wrapper<Mail_notification> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mail_notification> selectByMailId(@Param("id") Serializable id) ;

    List<Mail_notification> selectByMailMessageId(@Param("id") Serializable id) ;

    List<Mail_notification> selectByResPartnerId(@Param("id") Serializable id) ;


}
