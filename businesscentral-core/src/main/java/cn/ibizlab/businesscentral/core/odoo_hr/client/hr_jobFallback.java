package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_jobSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_job] 服务对象接口
 */
@Component
public class hr_jobFallback implements hr_jobFeignClient{

    public Hr_job update(Long id, Hr_job hr_job){
            return null;
     }
    public Boolean updateBatch(List<Hr_job> hr_jobs){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Hr_job create(Hr_job hr_job){
            return null;
     }
    public Boolean createBatch(List<Hr_job> hr_jobs){
            return false;
     }

    public Page<Hr_job> search(Hr_jobSearchContext context){
            return null;
     }



    public Hr_job get(Long id){
            return null;
     }


    public Page<Hr_job> select(){
            return null;
     }

    public Hr_job getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_job hr_job){
            return false;
     }


    public Boolean save(Hr_job hr_job){
            return false;
     }
    public Boolean saveBatch(List<Hr_job> hr_jobs){
            return false;
     }

    public Page<Hr_job> searchDefault(Hr_jobSearchContext context){
            return null;
     }


    public Page<Hr_job> searchMaster(Hr_jobSearchContext context){
            return null;
     }


}
