package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_inventorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_inventory] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-inventory", fallback = stock_inventoryFallback.class)
public interface stock_inventoryFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_inventories/{id}")
    Stock_inventory update(@PathVariable("id") Long id,@RequestBody Stock_inventory stock_inventory);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_inventories/batch")
    Boolean updateBatch(@RequestBody List<Stock_inventory> stock_inventories);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/search")
    Page<Stock_inventory> search(@RequestBody Stock_inventorySearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventories/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventories/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventories")
    Stock_inventory create(@RequestBody Stock_inventory stock_inventory);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/batch")
    Boolean createBatch(@RequestBody List<Stock_inventory> stock_inventories);




    @RequestMapping(method = RequestMethod.GET, value = "/stock_inventories/{id}")
    Stock_inventory get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_inventories/select")
    Page<Stock_inventory> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_inventories/getdraft")
    Stock_inventory getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/checkkey")
    Boolean checkKey(@RequestBody Stock_inventory stock_inventory);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/save")
    Boolean save(@RequestBody Stock_inventory stock_inventory);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_inventory> stock_inventories);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/searchdefault")
    Page<Stock_inventory> searchDefault(@RequestBody Stock_inventorySearchContext context);


}
