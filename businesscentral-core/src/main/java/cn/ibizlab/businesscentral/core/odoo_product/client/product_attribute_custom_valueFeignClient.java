package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute_custom_value;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_attribute_custom_valueSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_attribute_custom_value] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-attribute-custom-value", fallback = product_attribute_custom_valueFallback.class)
public interface product_attribute_custom_valueFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/product_attribute_custom_values/{id}")
    Product_attribute_custom_value get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/product_attribute_custom_values/search")
    Page<Product_attribute_custom_value> search(@RequestBody Product_attribute_custom_valueSearchContext context);




    @RequestMapping(method = RequestMethod.POST, value = "/product_attribute_custom_values")
    Product_attribute_custom_value create(@RequestBody Product_attribute_custom_value product_attribute_custom_value);

    @RequestMapping(method = RequestMethod.POST, value = "/product_attribute_custom_values/batch")
    Boolean createBatch(@RequestBody List<Product_attribute_custom_value> product_attribute_custom_values);



    @RequestMapping(method = RequestMethod.PUT, value = "/product_attribute_custom_values/{id}")
    Product_attribute_custom_value update(@PathVariable("id") Long id,@RequestBody Product_attribute_custom_value product_attribute_custom_value);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_attribute_custom_values/batch")
    Boolean updateBatch(@RequestBody List<Product_attribute_custom_value> product_attribute_custom_values);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_attribute_custom_values/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_attribute_custom_values/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/product_attribute_custom_values/select")
    Page<Product_attribute_custom_value> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_attribute_custom_values/getdraft")
    Product_attribute_custom_value getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_attribute_custom_values/checkkey")
    Boolean checkKey(@RequestBody Product_attribute_custom_value product_attribute_custom_value);


    @RequestMapping(method = RequestMethod.POST, value = "/product_attribute_custom_values/save")
    Boolean save(@RequestBody Product_attribute_custom_value product_attribute_custom_value);

    @RequestMapping(method = RequestMethod.POST, value = "/product_attribute_custom_values/savebatch")
    Boolean saveBatch(@RequestBody List<Product_attribute_custom_value> product_attribute_custom_values);



    @RequestMapping(method = RequestMethod.POST, value = "/product_attribute_custom_values/searchdefault")
    Page<Product_attribute_custom_value> searchDefault(@RequestBody Product_attribute_custom_valueSearchContext context);


}
