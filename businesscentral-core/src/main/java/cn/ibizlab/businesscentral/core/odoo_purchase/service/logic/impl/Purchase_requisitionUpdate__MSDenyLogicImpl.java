package cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_requisitionUpdate__MSDenyLogic;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition;

/**
 * 关系型数据实体[Update__MSDeny] 对象
 */
@Slf4j
@Service
public class Purchase_requisitionUpdate__MSDenyLogicImpl implements IPurchase_requisitionUpdate__MSDenyLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService purchase_requisitionservice;

    public cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService getPurchase_requisitionService() {
        return this.purchase_requisitionservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Purchase_requisition et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("purchase_requisitionupdate__msdenydefault",et);
           cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition  purchase_requisitionupdate__msdenytemp =new cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition();
           kieSession.insert(purchase_requisitionupdate__msdenytemp); 
           kieSession.setGlobal("purchase_requisitionupdate__msdenytemp",purchase_requisitionupdate__msdenytemp);
           kieSession.setGlobal("purchase_requisitionservice",purchase_requisitionservice);
           kieSession.setGlobal("iBzSysPurchase_requisitionDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.purchase_requisitionupdate__msdeny");

        }catch(Exception e){
            throw new RuntimeException("执行[行为[Update]主状态拒绝逻辑]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
