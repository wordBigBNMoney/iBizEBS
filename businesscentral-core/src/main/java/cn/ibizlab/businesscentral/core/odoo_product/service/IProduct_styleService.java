package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_style;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_styleSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_style] 服务对象接口
 */
public interface IProduct_styleService extends IService<Product_style>{

    boolean create(Product_style et) ;
    void createBatch(List<Product_style> list) ;
    boolean update(Product_style et) ;
    void updateBatch(List<Product_style> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_style get(Long key) ;
    Product_style getDraft(Product_style et) ;
    boolean checkKey(Product_style et) ;
    boolean save(Product_style et) ;
    void saveBatch(List<Product_style> list) ;
    Page<Product_style> searchDefault(Product_styleSearchContext context) ;
    List<Product_style> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_style> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_style> getProductStyleByIds(List<Long> ids) ;
    List<Product_style> getProductStyleByEntities(List<Product_style> entities) ;
}


