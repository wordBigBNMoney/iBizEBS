package cn.ibizlab.businesscentral.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Fleet_vehicle_model] 服务对象接口
 */
public interface IFleet_vehicle_modelService extends IService<Fleet_vehicle_model>{

    boolean create(Fleet_vehicle_model et) ;
    void createBatch(List<Fleet_vehicle_model> list) ;
    boolean update(Fleet_vehicle_model et) ;
    void updateBatch(List<Fleet_vehicle_model> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Fleet_vehicle_model get(Long key) ;
    Fleet_vehicle_model getDraft(Fleet_vehicle_model et) ;
    boolean checkKey(Fleet_vehicle_model et) ;
    boolean save(Fleet_vehicle_model et) ;
    void saveBatch(List<Fleet_vehicle_model> list) ;
    Page<Fleet_vehicle_model> searchDefault(Fleet_vehicle_modelSearchContext context) ;
    List<Fleet_vehicle_model> selectByBrandId(Long id);
    void resetByBrandId(Long id);
    void resetByBrandId(Collection<Long> ids);
    void removeByBrandId(Long id);
    List<Fleet_vehicle_model> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Fleet_vehicle_model> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Fleet_vehicle_model> getFleetVehicleModelByIds(List<Long> ids) ;
    List<Fleet_vehicle_model> getFleetVehicleModelByEntities(List<Fleet_vehicle_model> entities) ;
}


