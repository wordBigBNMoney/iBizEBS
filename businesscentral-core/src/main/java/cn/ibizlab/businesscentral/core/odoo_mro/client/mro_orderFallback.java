package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_order;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_orderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_order] 服务对象接口
 */
@Component
public class mro_orderFallback implements mro_orderFeignClient{

    public Mro_order update(Long id, Mro_order mro_order){
            return null;
     }
    public Boolean updateBatch(List<Mro_order> mro_orders){
            return false;
     }




    public Mro_order create(Mro_order mro_order){
            return null;
     }
    public Boolean createBatch(List<Mro_order> mro_orders){
            return false;
     }


    public Mro_order get(Long id){
            return null;
     }


    public Page<Mro_order> search(Mro_orderSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Mro_order> select(){
            return null;
     }

    public Mro_order getDraft(){
            return null;
    }



    public Boolean checkKey(Mro_order mro_order){
            return false;
     }


    public Boolean save(Mro_order mro_order){
            return false;
     }
    public Boolean saveBatch(List<Mro_order> mro_orders){
            return false;
     }

    public Page<Mro_order> searchDefault(Mro_orderSearchContext context){
            return null;
     }


}
