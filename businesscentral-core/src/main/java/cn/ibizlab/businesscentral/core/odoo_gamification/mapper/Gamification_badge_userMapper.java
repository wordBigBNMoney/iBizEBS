package cn.ibizlab.businesscentral.core.odoo_gamification.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_userSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Gamification_badge_userMapper extends BaseMapper<Gamification_badge_user>{

    Page<Gamification_badge_user> searchDefault(IPage page, @Param("srf") Gamification_badge_userSearchContext context, @Param("ew") Wrapper<Gamification_badge_user> wrapper) ;
    @Override
    Gamification_badge_user selectById(Serializable id);
    @Override
    int insert(Gamification_badge_user entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Gamification_badge_user entity);
    @Override
    int update(@Param(Constants.ENTITY) Gamification_badge_user entity, @Param("ew") Wrapper<Gamification_badge_user> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Gamification_badge_user> selectByBadgeId(@Param("id") Serializable id) ;

    List<Gamification_badge_user> selectByChallengeId(@Param("id") Serializable id) ;

    List<Gamification_badge_user> selectByEmployeeId(@Param("id") Serializable id) ;

    List<Gamification_badge_user> selectByCreateUid(@Param("id") Serializable id) ;

    List<Gamification_badge_user> selectBySenderId(@Param("id") Serializable id) ;

    List<Gamification_badge_user> selectByUserId(@Param("id") Serializable id) ;

    List<Gamification_badge_user> selectByWriteUid(@Param("id") Serializable id) ;


}
