package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicleSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle] 服务对象接口
 */
@Component
public class fleet_vehicleFallback implements fleet_vehicleFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Fleet_vehicle update(Long id, Fleet_vehicle fleet_vehicle){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle> fleet_vehicles){
            return false;
     }


    public Fleet_vehicle get(Long id){
            return null;
     }


    public Fleet_vehicle create(Fleet_vehicle fleet_vehicle){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle> fleet_vehicles){
            return false;
     }

    public Page<Fleet_vehicle> search(Fleet_vehicleSearchContext context){
            return null;
     }


    public Page<Fleet_vehicle> select(){
            return null;
     }

    public Fleet_vehicle getDraft(){
            return null;
    }



    public Boolean checkKey(Fleet_vehicle fleet_vehicle){
            return false;
     }


    public Boolean save(Fleet_vehicle fleet_vehicle){
            return false;
     }
    public Boolean saveBatch(List<Fleet_vehicle> fleet_vehicles){
            return false;
     }

    public Page<Fleet_vehicle> searchDefault(Fleet_vehicleSearchContext context){
            return null;
     }


}
