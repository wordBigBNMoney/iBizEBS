package cn.ibizlab.businesscentral.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicleSearchContext;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicleService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_fleet.mapper.Fleet_vehicleMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[车辆] 服务对象接口实现
 */
@Slf4j
@Service("Fleet_vehicleServiceImpl")
public class Fleet_vehicleServiceImpl extends EBSServiceImpl<Fleet_vehicleMapper, Fleet_vehicle> implements IFleet_vehicleService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_assignation_logService fleetVehicleAssignationLogService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_costService fleetVehicleCostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_odometerService fleetVehicleOdometerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_model_brandService fleetVehicleModelBrandService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_modelService fleetVehicleModelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_stateService fleetVehicleStateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "fleet.vehicle" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Fleet_vehicle et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicleService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Fleet_vehicle> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Fleet_vehicle et) {
        Fleet_vehicle old = new Fleet_vehicle() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicleService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicleService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Fleet_vehicle> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        fleetVehicleAssignationLogService.resetByVehicleId(key);
        fleetVehicleCostService.resetByVehicleId(key);
        fleetVehicleOdometerService.resetByVehicleId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        fleetVehicleAssignationLogService.resetByVehicleId(idList);
        fleetVehicleCostService.resetByVehicleId(idList);
        fleetVehicleOdometerService.resetByVehicleId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Fleet_vehicle get(Long key) {
        Fleet_vehicle et = getById(key);
        if(et==null){
            et=new Fleet_vehicle();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Fleet_vehicle getDraft(Fleet_vehicle et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Fleet_vehicle et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Fleet_vehicle et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Fleet_vehicle et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Fleet_vehicle> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Fleet_vehicle> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Fleet_vehicle> selectByBrandId(Long id) {
        return baseMapper.selectByBrandId(id);
    }
    @Override
    public void resetByBrandId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("brand_id",null).eq("brand_id",id));
    }

    @Override
    public void resetByBrandId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("brand_id",null).in("brand_id",ids));
    }

    @Override
    public void removeByBrandId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle>().eq("brand_id",id));
    }

	@Override
    public List<Fleet_vehicle> selectByModelId(Long id) {
        return baseMapper.selectByModelId(id);
    }
    @Override
    public void resetByModelId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("model_id",null).eq("model_id",id));
    }

    @Override
    public void resetByModelId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("model_id",null).in("model_id",ids));
    }

    @Override
    public void removeByModelId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle>().eq("model_id",id));
    }

	@Override
    public List<Fleet_vehicle> selectByStateId(Long id) {
        return baseMapper.selectByStateId(id);
    }
    @Override
    public void resetByStateId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("state_id",null).eq("state_id",id));
    }

    @Override
    public void resetByStateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("state_id",null).in("state_id",ids));
    }

    @Override
    public void removeByStateId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle>().eq("state_id",id));
    }

	@Override
    public List<Fleet_vehicle> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle>().eq("company_id",id));
    }

	@Override
    public List<Fleet_vehicle> selectByDriverId(Long id) {
        return baseMapper.selectByDriverId(id);
    }
    @Override
    public void resetByDriverId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("driver_id",null).eq("driver_id",id));
    }

    @Override
    public void resetByDriverId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle>().set("driver_id",null).in("driver_id",ids));
    }

    @Override
    public void removeByDriverId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle>().eq("driver_id",id));
    }

	@Override
    public List<Fleet_vehicle> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle>().eq("create_uid",id));
    }

	@Override
    public List<Fleet_vehicle> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Fleet_vehicle> searchDefault(Fleet_vehicleSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Fleet_vehicle> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Fleet_vehicle>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Fleet_vehicle et){
        //实体关系[DER1N_FLEET_VEHICLE__FLEET_VEHICLE_MODEL_BRAND__BRAND_ID]
        if(!ObjectUtils.isEmpty(et.getBrandId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model_brand odooBrand=et.getOdooBrand();
            if(ObjectUtils.isEmpty(odooBrand)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model_brand majorEntity=fleetVehicleModelBrandService.get(et.getBrandId());
                et.setOdooBrand(majorEntity);
                odooBrand=majorEntity;
            }
            et.setBrandIdText(odooBrand.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE__FLEET_VEHICLE_MODEL__MODEL_ID]
        if(!ObjectUtils.isEmpty(et.getModelId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model odooModel=et.getOdooModel();
            if(ObjectUtils.isEmpty(odooModel)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model majorEntity=fleetVehicleModelService.get(et.getModelId());
                et.setOdooModel(majorEntity);
                odooModel=majorEntity;
            }
            et.setImageMedium(odooModel.getImageMedium());
            et.setImage(odooModel.getImage());
            et.setModelIdText(odooModel.getName());
            et.setImageSmall(odooModel.getImageSmall());
        }
        //实体关系[DER1N_FLEET_VEHICLE__FLEET_VEHICLE_STATE__STATE_ID]
        if(!ObjectUtils.isEmpty(et.getStateId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_state odooState=et.getOdooState();
            if(ObjectUtils.isEmpty(odooState)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_state majorEntity=fleetVehicleStateService.get(et.getStateId());
                et.setOdooState(majorEntity);
                odooState=majorEntity;
            }
            et.setStateIdText(odooState.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE__RES_PARTNER__DRIVER_ID]
        if(!ObjectUtils.isEmpty(et.getDriverId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooDriver=et.getOdooDriver();
            if(ObjectUtils.isEmpty(odooDriver)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getDriverId());
                et.setOdooDriver(majorEntity);
                odooDriver=majorEntity;
            }
            et.setDriverIdText(odooDriver.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Fleet_vehicle> getFleetVehicleByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Fleet_vehicle> getFleetVehicleByEntities(List<Fleet_vehicle> entities) {
        List ids =new ArrayList();
        for(Fleet_vehicle entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



