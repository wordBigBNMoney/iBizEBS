package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_style;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_styleSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_style] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-style", fallback = product_styleFallback.class)
public interface product_styleFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/product_styles/search")
    Page<Product_style> search(@RequestBody Product_styleSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_styles/{id}")
    Product_style update(@PathVariable("id") Long id,@RequestBody Product_style product_style);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_styles/batch")
    Boolean updateBatch(@RequestBody List<Product_style> product_styles);



    @RequestMapping(method = RequestMethod.POST, value = "/product_styles")
    Product_style create(@RequestBody Product_style product_style);

    @RequestMapping(method = RequestMethod.POST, value = "/product_styles/batch")
    Boolean createBatch(@RequestBody List<Product_style> product_styles);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_styles/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_styles/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/product_styles/{id}")
    Product_style get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.GET, value = "/product_styles/select")
    Page<Product_style> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_styles/getdraft")
    Product_style getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_styles/checkkey")
    Boolean checkKey(@RequestBody Product_style product_style);


    @RequestMapping(method = RequestMethod.POST, value = "/product_styles/save")
    Boolean save(@RequestBody Product_style product_style);

    @RequestMapping(method = RequestMethod.POST, value = "/product_styles/savebatch")
    Boolean saveBatch(@RequestBody List<Product_style> product_styles);



    @RequestMapping(method = RequestMethod.POST, value = "/product_styles/searchdefault")
    Page<Product_style> searchDefault(@RequestBody Product_styleSearchContext context);


}
