package cn.ibizlab.businesscentral.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template_option;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_template_optionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sale_order_template_option] 服务对象接口
 */
@Component
public class sale_order_template_optionFallback implements sale_order_template_optionFeignClient{




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Sale_order_template_option update(Long id, Sale_order_template_option sale_order_template_option){
            return null;
     }
    public Boolean updateBatch(List<Sale_order_template_option> sale_order_template_options){
            return false;
     }


    public Page<Sale_order_template_option> search(Sale_order_template_optionSearchContext context){
            return null;
     }


    public Sale_order_template_option create(Sale_order_template_option sale_order_template_option){
            return null;
     }
    public Boolean createBatch(List<Sale_order_template_option> sale_order_template_options){
            return false;
     }

    public Sale_order_template_option get(Long id){
            return null;
     }


    public Page<Sale_order_template_option> select(){
            return null;
     }

    public Sale_order_template_option getDraft(){
            return null;
    }



    public Boolean checkKey(Sale_order_template_option sale_order_template_option){
            return false;
     }


    public Boolean save(Sale_order_template_option sale_order_template_option){
            return false;
     }
    public Boolean saveBatch(List<Sale_order_template_option> sale_order_template_options){
            return false;
     }

    public Page<Sale_order_template_option> searchDefault(Sale_order_template_optionSearchContext context){
            return null;
     }


}
