package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_resend_partner;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_resend_partnerSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_resend_partner] 服务对象接口
 */
@Component
public class mail_resend_partnerFallback implements mail_resend_partnerFeignClient{

    public Mail_resend_partner get(Long id){
            return null;
     }


    public Mail_resend_partner update(Long id, Mail_resend_partner mail_resend_partner){
            return null;
     }
    public Boolean updateBatch(List<Mail_resend_partner> mail_resend_partners){
            return false;
     }




    public Mail_resend_partner create(Mail_resend_partner mail_resend_partner){
            return null;
     }
    public Boolean createBatch(List<Mail_resend_partner> mail_resend_partners){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Mail_resend_partner> search(Mail_resend_partnerSearchContext context){
            return null;
     }


    public Page<Mail_resend_partner> select(){
            return null;
     }

    public Mail_resend_partner getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_resend_partner mail_resend_partner){
            return false;
     }


    public Boolean save(Mail_resend_partner mail_resend_partner){
            return false;
     }
    public Boolean saveBatch(List<Mail_resend_partner> mail_resend_partners){
            return false;
     }

    public Page<Mail_resend_partner> searchDefault(Mail_resend_partnerSearchContext context){
            return null;
     }


}
