package cn.ibizlab.businesscentral.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[calendar_alarm_manager] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-calendar:odoo-calendar}", contextId = "calendar-alarm-manager", fallback = calendar_alarm_managerFallback.class)
public interface calendar_alarm_managerFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarm_managers/{id}")
    Calendar_alarm_manager update(@PathVariable("id") Long id,@RequestBody Calendar_alarm_manager calendar_alarm_manager);

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarm_managers/batch")
    Boolean updateBatch(@RequestBody List<Calendar_alarm_manager> calendar_alarm_managers);



    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarm_managers/{id}")
    Calendar_alarm_manager get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/search")
    Page<Calendar_alarm_manager> search(@RequestBody Calendar_alarm_managerSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarm_managers/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarm_managers/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers")
    Calendar_alarm_manager create(@RequestBody Calendar_alarm_manager calendar_alarm_manager);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/batch")
    Boolean createBatch(@RequestBody List<Calendar_alarm_manager> calendar_alarm_managers);



    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarm_managers/select")
    Page<Calendar_alarm_manager> select();


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarm_managers/getdraft")
    Calendar_alarm_manager getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/checkkey")
    Boolean checkKey(@RequestBody Calendar_alarm_manager calendar_alarm_manager);


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/save")
    Boolean save(@RequestBody Calendar_alarm_manager calendar_alarm_manager);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/savebatch")
    Boolean saveBatch(@RequestBody List<Calendar_alarm_manager> calendar_alarm_managers);



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/searchdefault")
    Page<Calendar_alarm_manager> searchDefault(@RequestBody Calendar_alarm_managerSearchContext context);


}
