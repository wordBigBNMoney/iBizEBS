package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quant_packageSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quant_packageService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_quant_packageMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[包裹] 服务对象接口实现
 */
@Slf4j
@Service("Stock_quant_packageServiceImpl")
public class Stock_quant_packageServiceImpl extends EBSServiceImpl<Stock_quant_packageMapper, Stock_quant_package> implements IStock_quant_packageService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventory_lineService stockInventoryLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventoryService stockInventoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_levelService stockPackageLevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantService stockQuantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_packagingService productPackagingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.quant.package" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_quant_package et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_quant_packageService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_quant_package> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_quant_package et) {
        Stock_quant_package old = new Stock_quant_package() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_quant_packageService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_quant_packageService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_quant_package> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        stockInventoryLineService.resetByPackageId(key);
        stockInventoryService.resetByPackageId(key);
        if(!ObjectUtils.isEmpty(stockMoveLineService.selectByPackageId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_move_line]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(stockMoveLineService.selectByResultPackageId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_move_line]数据，无法删除!","","");
        stockPackageLevelService.resetByPackageId(key);
        if(!ObjectUtils.isEmpty(stockQuantService.selectByPackageId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_quant]数据，无法删除!","","");
        stockScrapService.resetByPackageId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        stockInventoryLineService.resetByPackageId(idList);
        stockInventoryService.resetByPackageId(idList);
        if(!ObjectUtils.isEmpty(stockMoveLineService.selectByPackageId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_move_line]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(stockMoveLineService.selectByResultPackageId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_move_line]数据，无法删除!","","");
        stockPackageLevelService.resetByPackageId(idList);
        if(!ObjectUtils.isEmpty(stockQuantService.selectByPackageId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_quant]数据，无法删除!","","");
        stockScrapService.resetByPackageId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_quant_package get(Long key) {
        Stock_quant_package et = getById(key);
        if(et==null){
            et=new Stock_quant_package();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_quant_package getDraft(Stock_quant_package et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_quant_package et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_quant_package et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_quant_package et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_quant_package> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_quant_package> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_quant_package> selectByPackagingId(Long id) {
        return baseMapper.selectByPackagingId(id);
    }
    @Override
    public void resetByPackagingId(Long id) {
        this.update(new UpdateWrapper<Stock_quant_package>().set("packaging_id",null).eq("packaging_id",id));
    }

    @Override
    public void resetByPackagingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_quant_package>().set("packaging_id",null).in("packaging_id",ids));
    }

    @Override
    public void removeByPackagingId(Long id) {
        this.remove(new QueryWrapper<Stock_quant_package>().eq("packaging_id",id));
    }

	@Override
    public List<Stock_quant_package> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Stock_quant_package>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_quant_package>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Stock_quant_package>().eq("company_id",id));
    }

	@Override
    public List<Stock_quant_package> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_quant_package>().eq("create_uid",id));
    }

	@Override
    public List<Stock_quant_package> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_quant_package>().eq("write_uid",id));
    }

	@Override
    public List<Stock_quant_package> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_quant_package>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_quant_package>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_quant_package>().eq("location_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_quant_package> searchDefault(Stock_quant_packageSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_quant_package> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_quant_package>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_quant_package et){
        //实体关系[DER1N_STOCK_QUANT_PACKAGE__PRODUCT_PACKAGING__PACKAGING_ID]
        if(!ObjectUtils.isEmpty(et.getPackagingId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging odooPackaging=et.getOdooPackaging();
            if(ObjectUtils.isEmpty(odooPackaging)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging majorEntity=productPackagingService.get(et.getPackagingId());
                et.setOdooPackaging(majorEntity);
                odooPackaging=majorEntity;
            }
            et.setPackagingIdText(odooPackaging.getName());
        }
        //实体关系[DER1N_STOCK_QUANT_PACKAGE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_STOCK_QUANT_PACKAGE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_QUANT_PACKAGE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_QUANT_PACKAGE__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_quant_package> getStockQuantPackageByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_quant_package> getStockQuantPackageByEntities(List<Stock_quant_package> entities) {
        List ids =new ArrayList();
        for(Stock_quant_package entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



