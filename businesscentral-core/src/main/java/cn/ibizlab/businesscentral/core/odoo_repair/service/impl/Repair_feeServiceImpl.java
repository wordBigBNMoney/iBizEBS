package cn.ibizlab.businesscentral.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_feeSearchContext;
import cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_feeService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_repair.mapper.Repair_feeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[修理费] 服务对象接口实现
 */
@Slf4j
@Service("Repair_feeServiceImpl")
public class Repair_feeServiceImpl extends EBSServiceImpl<Repair_feeMapper, Repair_fee> implements IRepair_feeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService accountInvoiceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "repair.fee" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Repair_fee et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_feeService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Repair_fee> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Repair_fee et) {
        Repair_fee old = new Repair_fee() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_feeService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_feeService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Repair_fee> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Repair_fee get(Long key) {
        Repair_fee et = getById(key);
        if(et==null){
            et=new Repair_fee();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Repair_fee getDraft(Repair_fee et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Repair_fee et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Repair_fee et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Repair_fee et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Repair_fee> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Repair_fee> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Repair_fee> selectByInvoiceLineId(Long id) {
        return baseMapper.selectByInvoiceLineId(id);
    }
    @Override
    public void resetByInvoiceLineId(Long id) {
        this.update(new UpdateWrapper<Repair_fee>().set("invoice_line_id",null).eq("invoice_line_id",id));
    }

    @Override
    public void resetByInvoiceLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_fee>().set("invoice_line_id",null).in("invoice_line_id",ids));
    }

    @Override
    public void removeByInvoiceLineId(Long id) {
        this.remove(new QueryWrapper<Repair_fee>().eq("invoice_line_id",id));
    }

	@Override
    public List<Repair_fee> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Repair_fee>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_fee>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Repair_fee>().eq("product_id",id));
    }

	@Override
    public List<Repair_fee> selectByRepairId(Long id) {
        return baseMapper.selectByRepairId(id);
    }
    @Override
    public void removeByRepairId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Repair_fee>().in("repair_id",ids));
    }

    @Override
    public void removeByRepairId(Long id) {
        this.remove(new QueryWrapper<Repair_fee>().eq("repair_id",id));
    }

	@Override
    public List<Repair_fee> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Repair_fee>().eq("create_uid",id));
    }

	@Override
    public List<Repair_fee> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Repair_fee>().eq("write_uid",id));
    }

	@Override
    public List<Repair_fee> selectByProductUom(Long id) {
        return baseMapper.selectByProductUom(id);
    }
    @Override
    public void resetByProductUom(Long id) {
        this.update(new UpdateWrapper<Repair_fee>().set("product_uom",null).eq("product_uom",id));
    }

    @Override
    public void resetByProductUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_fee>().set("product_uom",null).in("product_uom",ids));
    }

    @Override
    public void removeByProductUom(Long id) {
        this.remove(new QueryWrapper<Repair_fee>().eq("product_uom",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Repair_fee> searchDefault(Repair_feeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Repair_fee> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Repair_fee>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Repair_fee et){
        //实体关系[DER1N_REPAIR_FEE__ACCOUNT_INVOICE_LINE__INVOICE_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getInvoiceLineId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_line odooInvoiceLine=et.getOdooInvoiceLine();
            if(ObjectUtils.isEmpty(odooInvoiceLine)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_line majorEntity=accountInvoiceLineService.get(et.getInvoiceLineId());
                et.setOdooInvoiceLine(majorEntity);
                odooInvoiceLine=majorEntity;
            }
            et.setInvoiceLineIdText(odooInvoiceLine.getName());
        }
        //实体关系[DER1N_REPAIR_FEE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_REPAIR_FEE__REPAIR_ORDER__REPAIR_ID]
        if(!ObjectUtils.isEmpty(et.getRepairId())){
            cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order odooRepair=et.getOdooRepair();
            if(ObjectUtils.isEmpty(odooRepair)){
                cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order majorEntity=repairOrderService.get(et.getRepairId());
                et.setOdooRepair(majorEntity);
                odooRepair=majorEntity;
            }
            et.setRepairIdText(odooRepair.getName());
        }
        //实体关系[DER1N_REPAIR_FEE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_REPAIR_FEE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_REPAIR_FEE__UOM_UOM__PRODUCT_UOM]
        if(!ObjectUtils.isEmpty(et.getProductUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUom());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Repair_fee> getRepairFeeByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Repair_fee> getRepairFeeByEntities(List<Repair_fee> entities) {
        List ids =new ArrayList();
        for(Repair_fee entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



