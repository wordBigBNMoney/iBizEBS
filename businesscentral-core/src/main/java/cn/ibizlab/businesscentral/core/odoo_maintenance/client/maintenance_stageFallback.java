package cn.ibizlab.businesscentral.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_stageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[maintenance_stage] 服务对象接口
 */
@Component
public class maintenance_stageFallback implements maintenance_stageFeignClient{

    public Maintenance_stage get(Long id){
            return null;
     }


    public Maintenance_stage create(Maintenance_stage maintenance_stage){
            return null;
     }
    public Boolean createBatch(List<Maintenance_stage> maintenance_stages){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Maintenance_stage update(Long id, Maintenance_stage maintenance_stage){
            return null;
     }
    public Boolean updateBatch(List<Maintenance_stage> maintenance_stages){
            return false;
     }


    public Page<Maintenance_stage> search(Maintenance_stageSearchContext context){
            return null;
     }





    public Page<Maintenance_stage> select(){
            return null;
     }

    public Maintenance_stage getDraft(){
            return null;
    }



    public Boolean checkKey(Maintenance_stage maintenance_stage){
            return false;
     }


    public Boolean save(Maintenance_stage maintenance_stage){
            return false;
     }
    public Boolean saveBatch(List<Maintenance_stage> maintenance_stages){
            return false;
     }

    public Page<Maintenance_stage> searchDefault(Maintenance_stageSearchContext context){
            return null;
     }


}
