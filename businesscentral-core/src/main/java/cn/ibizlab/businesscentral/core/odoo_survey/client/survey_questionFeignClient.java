package cn.ibizlab.businesscentral.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_questionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[survey_question] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-survey:odoo-survey}", contextId = "survey-question", fallback = survey_questionFallback.class)
public interface survey_questionFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/survey_questions/search")
    Page<Survey_question> search(@RequestBody Survey_questionSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_questions/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_questions/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/survey_questions/{id}")
    Survey_question update(@PathVariable("id") Long id,@RequestBody Survey_question survey_question);

    @RequestMapping(method = RequestMethod.PUT, value = "/survey_questions/batch")
    Boolean updateBatch(@RequestBody List<Survey_question> survey_questions);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_questions/{id}")
    Survey_question get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/survey_questions")
    Survey_question create(@RequestBody Survey_question survey_question);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_questions/batch")
    Boolean createBatch(@RequestBody List<Survey_question> survey_questions);





    @RequestMapping(method = RequestMethod.GET, value = "/survey_questions/select")
    Page<Survey_question> select();


    @RequestMapping(method = RequestMethod.GET, value = "/survey_questions/getdraft")
    Survey_question getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/survey_questions/checkkey")
    Boolean checkKey(@RequestBody Survey_question survey_question);


    @RequestMapping(method = RequestMethod.POST, value = "/survey_questions/save")
    Boolean save(@RequestBody Survey_question survey_question);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_questions/savebatch")
    Boolean saveBatch(@RequestBody List<Survey_question> survey_questions);



    @RequestMapping(method = RequestMethod.POST, value = "/survey_questions/searchdefault")
    Page<Survey_question> searchDefault(@RequestBody Survey_questionSearchContext context);


}
