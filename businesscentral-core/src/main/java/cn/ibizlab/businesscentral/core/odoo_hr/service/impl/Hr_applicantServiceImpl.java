package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_applicantSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_applicantMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[申请人] 服务对象接口实现
 */
@Slf4j
@Service("Hr_applicantServiceImpl")
public class Hr_applicantServiceImpl extends EBSServiceImpl<Hr_applicantMapper, Hr_applicant> implements IHr_applicantService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_eventService calendarEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_degreeService hrRecruitmentDegreeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_stageService hrRecruitmentStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_campaignService utmCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mediumService utmMediumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService utmSourceService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.applicant" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_applicant et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_applicantService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_applicant> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_applicant et) {
        Hr_applicant old = new Hr_applicant() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_applicantService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_applicantService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_applicant> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        calendarEventService.resetByApplicantId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        calendarEventService.resetByApplicantId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_applicant get(Long key) {
        Hr_applicant et = getById(key);
        if(et==null){
            et=new Hr_applicant();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_applicant getDraft(Hr_applicant et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_applicant et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_applicant et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_applicant et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_applicant> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_applicant> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_applicant> selectByDepartmentId(Long id) {
        return baseMapper.selectByDepartmentId(id);
    }
    @Override
    public void resetByDepartmentId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("department_id",null).eq("department_id",id));
    }

    @Override
    public void resetByDepartmentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("department_id",null).in("department_id",ids));
    }

    @Override
    public void removeByDepartmentId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("department_id",id));
    }

	@Override
    public List<Hr_applicant> selectByEmpId(Long id) {
        return baseMapper.selectByEmpId(id);
    }
    @Override
    public void resetByEmpId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("emp_id",null).eq("emp_id",id));
    }

    @Override
    public void resetByEmpId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("emp_id",null).in("emp_id",ids));
    }

    @Override
    public void removeByEmpId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("emp_id",id));
    }

	@Override
    public List<Hr_applicant> selectByJobId(Long id) {
        return baseMapper.selectByJobId(id);
    }
    @Override
    public void resetByJobId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("job_id",null).eq("job_id",id));
    }

    @Override
    public void resetByJobId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("job_id",null).in("job_id",ids));
    }

    @Override
    public void removeByJobId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("job_id",id));
    }

	@Override
    public List<Hr_applicant> selectByTypeId(Long id) {
        return baseMapper.selectByTypeId(id);
    }
    @Override
    public void resetByTypeId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("type_id",null).eq("type_id",id));
    }

    @Override
    public void resetByTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("type_id",null).in("type_id",ids));
    }

    @Override
    public void removeByTypeId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("type_id",id));
    }

	@Override
    public List<Hr_applicant> selectByLastStageId(Long id) {
        return baseMapper.selectByLastStageId(id);
    }
    @Override
    public void resetByLastStageId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("last_stage_id",null).eq("last_stage_id",id));
    }

    @Override
    public void resetByLastStageId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("last_stage_id",null).in("last_stage_id",ids));
    }

    @Override
    public void removeByLastStageId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("last_stage_id",id));
    }

	@Override
    public List<Hr_applicant> selectByStageId(Long id) {
        return baseMapper.selectByStageId(id);
    }
    @Override
    public List<Hr_applicant> selectByStageId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Hr_applicant>().in("id",ids));
    }

    @Override
    public void removeByStageId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("stage_id",id));
    }

	@Override
    public List<Hr_applicant> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("company_id",id));
    }

	@Override
    public List<Hr_applicant> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("partner_id",id));
    }

	@Override
    public List<Hr_applicant> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("create_uid",id));
    }

	@Override
    public List<Hr_applicant> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("user_id",id));
    }

	@Override
    public List<Hr_applicant> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("write_uid",id));
    }

	@Override
    public List<Hr_applicant> selectByCampaignId(Long id) {
        return baseMapper.selectByCampaignId(id);
    }
    @Override
    public void resetByCampaignId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("campaign_id",null).eq("campaign_id",id));
    }

    @Override
    public void resetByCampaignId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("campaign_id",null).in("campaign_id",ids));
    }

    @Override
    public void removeByCampaignId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("campaign_id",id));
    }

	@Override
    public List<Hr_applicant> selectByMediumId(Long id) {
        return baseMapper.selectByMediumId(id);
    }
    @Override
    public void resetByMediumId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("medium_id",null).eq("medium_id",id));
    }

    @Override
    public void resetByMediumId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("medium_id",null).in("medium_id",ids));
    }

    @Override
    public void removeByMediumId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("medium_id",id));
    }

	@Override
    public List<Hr_applicant> selectBySourceId(Long id) {
        return baseMapper.selectBySourceId(id);
    }
    @Override
    public void resetBySourceId(Long id) {
        this.update(new UpdateWrapper<Hr_applicant>().set("source_id",null).eq("source_id",id));
    }

    @Override
    public void resetBySourceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_applicant>().set("source_id",null).in("source_id",ids));
    }

    @Override
    public void removeBySourceId(Long id) {
        this.remove(new QueryWrapper<Hr_applicant>().eq("source_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_applicant> searchDefault(Hr_applicantSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_applicant> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_applicant>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_applicant et){
        //实体关系[DER1N_HR_APPLICANT__HR_DEPARTMENT__DEPARTMENT_ID]
        if(!ObjectUtils.isEmpty(et.getDepartmentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment=et.getOdooDepartment();
            if(ObjectUtils.isEmpty(odooDepartment)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department majorEntity=hrDepartmentService.get(et.getDepartmentId());
                et.setOdooDepartment(majorEntity);
                odooDepartment=majorEntity;
            }
            et.setDepartmentIdText(odooDepartment.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__HR_EMPLOYEE__EMP_ID]
        if(!ObjectUtils.isEmpty(et.getEmpId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmp=et.getOdooEmp();
            if(ObjectUtils.isEmpty(odooEmp)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getEmpId());
                et.setOdooEmp(majorEntity);
                odooEmp=majorEntity;
            }
            et.setEmployeeName(odooEmp.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__HR_JOB__JOB_ID]
        if(!ObjectUtils.isEmpty(et.getJobId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job odooJob=et.getOdooJob();
            if(ObjectUtils.isEmpty(odooJob)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job majorEntity=hrJobService.get(et.getJobId());
                et.setOdooJob(majorEntity);
                odooJob=majorEntity;
            }
            et.setJobIdText(odooJob.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__HR_RECRUITMENT_DEGREE__TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getTypeId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_degree odooType=et.getOdooType();
            if(ObjectUtils.isEmpty(odooType)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_degree majorEntity=hrRecruitmentDegreeService.get(et.getTypeId());
                et.setOdooType(majorEntity);
                odooType=majorEntity;
            }
            et.setTypeIdText(odooType.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__HR_RECRUITMENT_STAGE__LAST_STAGE_ID]
        if(!ObjectUtils.isEmpty(et.getLastStageId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage odooLastStage=et.getOdooLastStage();
            if(ObjectUtils.isEmpty(odooLastStage)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage majorEntity=hrRecruitmentStageService.get(et.getLastStageId());
                et.setOdooLastStage(majorEntity);
                odooLastStage=majorEntity;
            }
            et.setLastStageIdText(odooLastStage.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__HR_RECRUITMENT_STAGE__STAGE_ID]
        if(!ObjectUtils.isEmpty(et.getStageId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage odooStage=et.getOdooStage();
            if(ObjectUtils.isEmpty(odooStage)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage majorEntity=hrRecruitmentStageService.get(et.getStageId());
                et.setOdooStage(majorEntity);
                odooStage=majorEntity;
            }
            et.setStageIdText(odooStage.getName());
            et.setLegendNormal(odooStage.getLegendNormal());
            et.setLegendDone(odooStage.getLegendDone());
            et.setLegendBlocked(odooStage.getLegendBlocked());
        }
        //实体关系[DER1N_HR_APPLICANT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
            et.setUserEmail(odooUser.getEmail());
        }
        //实体关系[DER1N_HR_APPLICANT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__UTM_CAMPAIGN__CAMPAIGN_ID]
        if(!ObjectUtils.isEmpty(et.getCampaignId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign=et.getOdooCampaign();
            if(ObjectUtils.isEmpty(odooCampaign)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign majorEntity=utmCampaignService.get(et.getCampaignId());
                et.setOdooCampaign(majorEntity);
                odooCampaign=majorEntity;
            }
            et.setCampaignIdText(odooCampaign.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__UTM_MEDIUM__MEDIUM_ID]
        if(!ObjectUtils.isEmpty(et.getMediumId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium=et.getOdooMedium();
            if(ObjectUtils.isEmpty(odooMedium)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium majorEntity=utmMediumService.get(et.getMediumId());
                et.setOdooMedium(majorEntity);
                odooMedium=majorEntity;
            }
            et.setMediumIdText(odooMedium.getName());
        }
        //实体关系[DER1N_HR_APPLICANT__UTM_SOURCE__SOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getSourceId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource=et.getOdooSource();
            if(ObjectUtils.isEmpty(odooSource)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source majorEntity=utmSourceService.get(et.getSourceId());
                et.setOdooSource(majorEntity);
                odooSource=majorEntity;
            }
            et.setSourceIdText(odooSource.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_applicant> getHrApplicantByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_applicant> getHrApplicantByEntities(List<Hr_applicant> entities) {
        List ids =new ArrayList();
        for(Hr_applicant entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



