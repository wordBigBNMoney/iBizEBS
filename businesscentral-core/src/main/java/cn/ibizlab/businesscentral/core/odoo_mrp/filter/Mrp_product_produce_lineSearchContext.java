package cn.ibizlab.businesscentral.core.odoo_mrp.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce_line;
/**
 * 关系型数据实体[Mrp_product_produce_line] 查询条件对象
 */
@Slf4j
@Data
public class Mrp_product_produce_lineSearchContext extends QueryWrapperContext<Mrp_product_produce_line> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_lot_id_text_eq;//[批次/序列号码]
	public void setN_lot_id_text_eq(String n_lot_id_text_eq) {
        this.n_lot_id_text_eq = n_lot_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_eq)){
            this.getSearchCond().eq("lot_id_text", n_lot_id_text_eq);
        }
    }
	private String n_lot_id_text_like;//[批次/序列号码]
	public void setN_lot_id_text_like(String n_lot_id_text_like) {
        this.n_lot_id_text_like = n_lot_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_like)){
            this.getSearchCond().like("lot_id_text", n_lot_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_product_uom_id_text_eq;//[计量单位]
	public void setN_product_uom_id_text_eq(String n_product_uom_id_text_eq) {
        this.n_product_uom_id_text_eq = n_product_uom_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_eq)){
            this.getSearchCond().eq("product_uom_id_text", n_product_uom_id_text_eq);
        }
    }
	private String n_product_uom_id_text_like;//[计量单位]
	public void setN_product_uom_id_text_like(String n_product_uom_id_text_like) {
        this.n_product_uom_id_text_like = n_product_uom_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_like)){
            this.getSearchCond().like("product_uom_id_text", n_product_uom_id_text_like);
        }
    }
	private String n_move_id_text_eq;//[分录]
	public void setN_move_id_text_eq(String n_move_id_text_eq) {
        this.n_move_id_text_eq = n_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_text_eq)){
            this.getSearchCond().eq("move_id_text", n_move_id_text_eq);
        }
    }
	private String n_move_id_text_like;//[分录]
	public void setN_move_id_text_like(String n_move_id_text_like) {
        this.n_move_id_text_like = n_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_move_id_text_like)){
            this.getSearchCond().like("move_id_text", n_move_id_text_like);
        }
    }
	private Long n_move_id_eq;//[分录]
	public void setN_move_id_eq(Long n_move_id_eq) {
        this.n_move_id_eq = n_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_eq)){
            this.getSearchCond().eq("move_id", n_move_id_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_lot_id_eq;//[批次/序列号码]
	public void setN_lot_id_eq(Long n_lot_id_eq) {
        this.n_lot_id_eq = n_lot_id_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_eq)){
            this.getSearchCond().eq("lot_id", n_lot_id_eq);
        }
    }
	private Long n_product_produce_id_eq;//[产品生产]
	public void setN_product_produce_id_eq(Long n_product_produce_id_eq) {
        this.n_product_produce_id_eq = n_product_produce_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_produce_id_eq)){
            this.getSearchCond().eq("product_produce_id", n_product_produce_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_product_uom_id_eq;//[计量单位]
	public void setN_product_uom_id_eq(Long n_product_uom_id_eq) {
        this.n_product_uom_id_eq = n_product_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_eq)){
            this.getSearchCond().eq("product_uom_id", n_product_uom_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



