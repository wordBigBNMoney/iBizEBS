package cn.ibizlab.businesscentral.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_sale.mapper.Sale_order_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[销售订单行] 服务对象接口实现
 */
@Slf4j
@Service("Sale_order_lineServiceImpl")
public class Sale_order_lineServiceImpl extends EBSServiceImpl<Sale_order_lineMapper, Sale_order_line> implements ISale_order_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_lineService accountAnalyticLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService eventRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_attribute_custom_valueService productAttributeCustomValueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;

    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_optionService saleOrderOptionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_event_ticketService eventEventTicketService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_eventService eventEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_packagingService productPackagingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_location_routeService stockLocationRouteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "sale.order.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Sale_order_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_order_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Sale_order_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Sale_order_line et) {
        Sale_order_line old = new Sale_order_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_order_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_order_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Sale_order_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountAnalyticLineService.resetBySoLine(key);
        eventRegistrationService.removeBySaleOrderLineId(key);
        productAttributeCustomValueService.resetBySaleOrderLineId(key);
        purchaseOrderLineService.resetBySaleLineId(key);
        saleOrderLineService.removeByLinkedLineId(key);
        saleOrderOptionService.resetByLineId(key);
        stockMoveService.resetBySaleLineId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountAnalyticLineService.resetBySoLine(idList);
        eventRegistrationService.removeBySaleOrderLineId(idList);
        productAttributeCustomValueService.resetBySaleOrderLineId(idList);
        purchaseOrderLineService.resetBySaleLineId(idList);
        saleOrderLineService.removeByLinkedLineId(idList);
        saleOrderOptionService.resetByLineId(idList);
        stockMoveService.resetBySaleLineId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Sale_order_line get(Long key) {
        Sale_order_line et = getById(key);
        if(et==null){
            et=new Sale_order_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Sale_order_line getDraft(Sale_order_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Sale_order_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Sale_order_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Sale_order_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Sale_order_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Sale_order_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Sale_order_line> selectByEventTicketId(Long id) {
        return baseMapper.selectByEventTicketId(id);
    }
    @Override
    public void resetByEventTicketId(Long id) {
        this.update(new UpdateWrapper<Sale_order_line>().set("event_ticket_id",null).eq("event_ticket_id",id));
    }

    @Override
    public void resetByEventTicketId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_line>().set("event_ticket_id",null).in("event_ticket_id",ids));
    }

    @Override
    public void removeByEventTicketId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("event_ticket_id",id));
    }

	@Override
    public List<Sale_order_line> selectByEventId(Long id) {
        return baseMapper.selectByEventId(id);
    }
    @Override
    public void resetByEventId(Long id) {
        this.update(new UpdateWrapper<Sale_order_line>().set("event_id",null).eq("event_id",id));
    }

    @Override
    public void resetByEventId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_line>().set("event_id",null).in("event_id",ids));
    }

    @Override
    public void removeByEventId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("event_id",id));
    }

	@Override
    public List<Sale_order_line> selectByProductPackaging(Long id) {
        return baseMapper.selectByProductPackaging(id);
    }
    @Override
    public void resetByProductPackaging(Long id) {
        this.update(new UpdateWrapper<Sale_order_line>().set("product_packaging",null).eq("product_packaging",id));
    }

    @Override
    public void resetByProductPackaging(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_line>().set("product_packaging",null).in("product_packaging",ids));
    }

    @Override
    public void removeByProductPackaging(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("product_packaging",id));
    }

	@Override
    public List<Sale_order_line> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public List<Sale_order_line> selectByProductId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Sale_order_line>().in("id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("product_id",id));
    }

	@Override
    public List<Sale_order_line> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Sale_order_line>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_line>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("company_id",id));
    }

	@Override
    public List<Sale_order_line> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Sale_order_line>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_line>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("currency_id",id));
    }

	@Override
    public List<Sale_order_line> selectByOrderPartnerId(Long id) {
        return baseMapper.selectByOrderPartnerId(id);
    }
    @Override
    public void resetByOrderPartnerId(Long id) {
        this.update(new UpdateWrapper<Sale_order_line>().set("order_partner_id",null).eq("order_partner_id",id));
    }

    @Override
    public void resetByOrderPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_line>().set("order_partner_id",null).in("order_partner_id",ids));
    }

    @Override
    public void removeByOrderPartnerId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("order_partner_id",id));
    }

	@Override
    public List<Sale_order_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("create_uid",id));
    }

	@Override
    public List<Sale_order_line> selectBySalesmanId(Long id) {
        return baseMapper.selectBySalesmanId(id);
    }
    @Override
    public void resetBySalesmanId(Long id) {
        this.update(new UpdateWrapper<Sale_order_line>().set("salesman_id",null).eq("salesman_id",id));
    }

    @Override
    public void resetBySalesmanId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_line>().set("salesman_id",null).in("salesman_id",ids));
    }

    @Override
    public void removeBySalesmanId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("salesman_id",id));
    }

	@Override
    public List<Sale_order_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("write_uid",id));
    }

	@Override
    public List<Sale_order_line> selectByLinkedLineId(Long id) {
        return baseMapper.selectByLinkedLineId(id);
    }
    @Override
    public void removeByLinkedLineId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Sale_order_line>().in("linked_line_id",ids));
    }

    @Override
    public void removeByLinkedLineId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("linked_line_id",id));
    }

	@Override
    public List<Sale_order_line> selectByOrderId(Long id) {
        return baseMapper.selectByOrderId(id);
    }
    @Override
    public void removeByOrderId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Sale_order_line>().in("order_id",ids));
    }

    @Override
    public void removeByOrderId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("order_id",id));
    }

	@Override
    public List<Sale_order_line> selectByRouteId(Long id) {
        return baseMapper.selectByRouteId(id);
    }
    @Override
    public List<Sale_order_line> selectByRouteId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Sale_order_line>().in("id",ids));
    }

    @Override
    public void removeByRouteId(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("route_id",id));
    }

	@Override
    public List<Sale_order_line> selectByProductUom(Long id) {
        return baseMapper.selectByProductUom(id);
    }
    @Override
    public void resetByProductUom(Long id) {
        this.update(new UpdateWrapper<Sale_order_line>().set("product_uom",null).eq("product_uom",id));
    }

    @Override
    public void resetByProductUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_line>().set("product_uom",null).in("product_uom",ids));
    }

    @Override
    public void removeByProductUom(Long id) {
        this.remove(new QueryWrapper<Sale_order_line>().eq("product_uom",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Sale_order_line> searchDefault(Sale_order_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Sale_order_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Sale_order_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Sale_order_line et){
        //实体关系[DER1N_SALE_ORDER_LINE__EVENT_EVENT_TICKET__EVENT_TICKET_ID]
        if(!ObjectUtils.isEmpty(et.getEventTicketId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket odooEventTicket=et.getOdooEventTicket();
            if(ObjectUtils.isEmpty(odooEventTicket)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket majorEntity=eventEventTicketService.get(et.getEventTicketId());
                et.setOdooEventTicket(majorEntity);
                odooEventTicket=majorEntity;
            }
            et.setEventTicketIdText(odooEventTicket.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__EVENT_EVENT__EVENT_ID]
        if(!ObjectUtils.isEmpty(et.getEventId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event odooEvent=et.getOdooEvent();
            if(ObjectUtils.isEmpty(odooEvent)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event majorEntity=eventEventService.get(et.getEventId());
                et.setOdooEvent(majorEntity);
                odooEvent=majorEntity;
            }
            et.setEventIdText(odooEvent.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__PRODUCT_PACKAGING__PRODUCT_PACKAGING]
        if(!ObjectUtils.isEmpty(et.getProductPackaging())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging odooProductPackaging=et.getOdooProductPackaging();
            if(ObjectUtils.isEmpty(odooProductPackaging)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging majorEntity=productPackagingService.get(et.getProductPackaging());
                et.setOdooProductPackaging(majorEntity);
                odooProductPackaging=majorEntity;
            }
            et.setProductPackagingText(odooProductPackaging.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setEventOk(odooProduct.getEventOk());
            et.setProductImage(odooProduct.getImage());
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__RES_PARTNER__ORDER_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getOrderPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOrderPartner=et.getOdooOrderPartner();
            if(ObjectUtils.isEmpty(odooOrderPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getOrderPartnerId());
                et.setOdooOrderPartner(majorEntity);
                odooOrderPartner=majorEntity;
            }
            et.setOrderPartnerIdText(odooOrderPartner.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__RES_USERS__SALESMAN_ID]
        if(!ObjectUtils.isEmpty(et.getSalesmanId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooSalesman=et.getOdooSalesman();
            if(ObjectUtils.isEmpty(odooSalesman)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getSalesmanId());
                et.setOdooSalesman(majorEntity);
                odooSalesman=majorEntity;
            }
            et.setSalesmanIdText(odooSalesman.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__SALE_ORDER_LINE__LINKED_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getLinkedLineId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line odooLinkedLine=et.getOdooLinkedLine();
            if(ObjectUtils.isEmpty(odooLinkedLine)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line majorEntity=saleOrderLineService.get(et.getLinkedLineId());
                et.setOdooLinkedLine(majorEntity);
                odooLinkedLine=majorEntity;
            }
            et.setLinkedLineIdText(odooLinkedLine.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__SALE_ORDER__ORDER_ID]
        if(!ObjectUtils.isEmpty(et.getOrderId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooOrder=et.getOdooOrder();
            if(ObjectUtils.isEmpty(odooOrder)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order majorEntity=saleOrderService.get(et.getOrderId());
                et.setOdooOrder(majorEntity);
                odooOrder=majorEntity;
            }
            et.setOrderIdText(odooOrder.getName());
            et.setState(odooOrder.getState());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__STOCK_LOCATION_ROUTE__ROUTE_ID]
        if(!ObjectUtils.isEmpty(et.getRouteId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooRoute=et.getOdooRoute();
            if(ObjectUtils.isEmpty(odooRoute)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route majorEntity=stockLocationRouteService.get(et.getRouteId());
                et.setOdooRoute(majorEntity);
                odooRoute=majorEntity;
            }
            et.setRouteIdText(odooRoute.getName());
        }
        //实体关系[DER1N_SALE_ORDER_LINE__UOM_UOM__PRODUCT_UOM]
        if(!ObjectUtils.isEmpty(et.getProductUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUom());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Sale_order_line> getSaleOrderLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Sale_order_line> getSaleOrderLineByEntities(List<Sale_order_line> entities) {
        List ids =new ArrayList();
        for(Sale_order_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



