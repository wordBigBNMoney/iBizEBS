package cn.ibizlab.businesscentral.core.odoo_ir.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[模型属性]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "IR_MODEL_FIELDS",resultMap = "Ir_model_fieldsResultMap")
public class Ir_model_fields extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 属性类型
     */
    @TableField(value = "ttype")
    @JSONField(name = "ttype")
    @JsonProperty("ttype")
    private String ttype;
    /**
     * 模型
     */
    @TableField(value = "model")
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * tracking
     */
    @TableField(value = "tracking")
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private Integer tracking;
    /**
     * 描述
     */
    @DEField(name = "field_description")
    @TableField(value = "field_description")
    @JSONField(name = "field_description")
    @JsonProperty("field_description")
    private String fieldDescription;
    /**
     * 模型
     */
    @DEField(name = "model_id")
    @TableField(value = "model_id")
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Long modelId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_model odooModel;



    /**
     * 设置 [属性类型]
     */
    public void setTtype(String ttype){
        this.ttype = ttype ;
        this.modify("ttype",ttype);
    }

    /**
     * 设置 [模型]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [tracking]
     */
    public void setTracking(Integer tracking){
        this.tracking = tracking ;
        this.modify("tracking",tracking);
    }

    /**
     * 设置 [描述]
     */
    public void setFieldDescription(String fieldDescription){
        this.fieldDescription = fieldDescription ;
        this.modify("field_description",fieldDescription);
    }

    /**
     * 设置 [模型]
     */
    public void setModelId(Long modelId){
        this.modelId = modelId ;
        this.modify("model_id",modelId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


