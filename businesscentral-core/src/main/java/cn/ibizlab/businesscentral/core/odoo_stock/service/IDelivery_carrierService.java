package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Delivery_carrier;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Delivery_carrierSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Delivery_carrier] 服务对象接口
 */
public interface IDelivery_carrierService extends IService<Delivery_carrier>{

    boolean create(Delivery_carrier et) ;
    void createBatch(List<Delivery_carrier> list) ;
    boolean update(Delivery_carrier et) ;
    void updateBatch(List<Delivery_carrier> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Delivery_carrier get(Long key) ;
    Delivery_carrier getDraft(Delivery_carrier et) ;
    boolean checkKey(Delivery_carrier et) ;
    boolean save(Delivery_carrier et) ;
    void saveBatch(List<Delivery_carrier> list) ;
    Page<Delivery_carrier> searchDefault(Delivery_carrierSearchContext context) ;
    List<Delivery_carrier> selectByProductId(Long id);
    void removeByProductId(Long id);
    List<Delivery_carrier> selectByCompanyId(Long id);
    void removeByCompanyId(Long id);
    List<Delivery_carrier> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Delivery_carrier> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


