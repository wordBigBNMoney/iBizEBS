package cn.ibizlab.businesscentral.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[联系人]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RES_PARTNER",resultMap = "Res_partnerResultMap")
public class Res_partner extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 图像
     */
    @TableField(exist = false)
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;
    /**
     * 地址类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 付款令牌
     */
    @TableField(exist = false)
    @JSONField(name = "payment_token_ids")
    @JsonProperty("payment_token_ids")
    private String paymentTokenIds;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;
    /**
     * #会议
     */
    @TableField(exist = false)
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;
    /**
     * ＃供应商账单
     */
    @TableField(exist = false)
    @JSONField(name = "supplier_invoice_count")
    @JsonProperty("supplier_invoice_count")
    private Integer supplierInvoiceCount;
    /**
     * 公司名称
     */
    @DEField(name = "company_name")
    @TableField(value = "company_name")
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    private String companyName;
    /**
     * 在当前网站显示
     */
    @TableField(exist = false)
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;
    /**
     * 最近的发票和付款匹配时间
     */
    @DEField(name = "last_time_entries_checked")
    @TableField(value = "last_time_entries_checked")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_time_entries_checked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("last_time_entries_checked")
    private Timestamp lastTimeEntriesChecked;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 对此债务人的信任度
     */
    @TableField(exist = false)
    @JSONField(name = "trust")
    @JsonProperty("trust")
    private String trust;
    /**
     * 已开票总计
     */
    @TableField(exist = false)
    @JSONField(name = "total_invoiced")
    @JsonProperty("total_invoiced")
    private BigDecimal totalInvoiced;
    /**
     * 销售点订单计数
     */
    @TableField(exist = false)
    @JSONField(name = "pos_order_count")
    @JsonProperty("pos_order_count")
    private Integer posOrderCount;
    /**
     * 完整地址
     */
    @TableField(exist = false)
    @JSONField(name = "contact_address")
    @JsonProperty("contact_address")
    private String contactAddress;
    /**
     * 发票
     */
    @DEField(name = "invoice_warn")
    @TableField(value = "invoice_warn")
    @JSONField(name = "invoice_warn")
    @JsonProperty("invoice_warn")
    private String invoiceWarn;
    /**
     * 银行
     */
    @TableField(exist = false)
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    private String bankIds;
    /**
     * 注册到期
     */
    @DEField(name = "signup_expiration")
    @TableField(value = "signup_expiration")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "signup_expiration" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("signup_expiration")
    private Timestamp signupExpiration;
    /**
     * 采购订单数
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;
    /**
     * 有未核销的分录
     */
    @TableField(exist = false)
    @JSONField(name = "has_unreconciled_entries")
    @JsonProperty("has_unreconciled_entries")
    private Boolean hasUnreconciledEntries;
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private String categoryId;
    /**
     * customer rank
     */
    @DEField(name = "customer_rank")
    @TableField(value = "customer_rank")
    @JSONField(name = "customer_rank")
    @JsonProperty("customer_rank")
    private Integer customerRank;
    /**
     * 网站业务伙伴的详细说明
     */
    @DEField(name = "website_description")
    @TableField(value = "website_description")
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 会议
     */
    @TableField(exist = false)
    @JSONField(name = "meeting_ids")
    @JsonProperty("meeting_ids")
    private String meetingIds;
    /**
     * 员工
     */
    @TableField(value = "employee")
    @JSONField(name = "employee")
    @JsonProperty("employee")
    private Boolean employee;
    /**
     * 显示名称
     */
    @DEField(name = "display_name")
    @TableField(value = "display_name")
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 联系人
     */
    @TableField(exist = false)
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;
    /**
     * 网站元说明
     */
    @DEField(name = "website_meta_description")
    @TableField(value = "website_meta_description")
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;
    /**
     * 黑名单
     */
    @TableField(exist = false)
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private Boolean isBlacklisted;
    /**
     * 价格表
     */
    @TableField(exist = false)
    @JSONField(name = "property_product_pricelist")
    @JsonProperty("property_product_pricelist")
    private Integer propertyProductPricelist;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 注册令牌 Token
     */
    @DEField(name = "signup_token")
    @TableField(value = "signup_token")
    @JSONField(name = "signup_token")
    @JsonProperty("signup_token")
    private String signupToken;
    /**
     * 公司是指业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "ref_company_ids")
    @JsonProperty("ref_company_ids")
    private String refCompanyIds;
    /**
     * 公司
     */
    @DEField(name = "is_company")
    @TableField(value = "is_company")
    @JSONField(name = "is_company")
    @JsonProperty("is_company")
    private Boolean isCompany;
    /**
     * 电话
     */
    @TableField(value = "phone")
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 时区
     */
    @TableField(value = "tz")
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "event_count")
    @JsonProperty("event_count")
    private Integer eventCount;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 最后的提醒已经标志为已读
     */
    @DEField(name = "calendar_last_notif_ack")
    @TableField(value = "calendar_last_notif_ack")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "calendar_last_notif_ack" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("calendar_last_notif_ack")
    private Timestamp calendarLastNotifAck;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 注册令牌（Token）类型
     */
    @DEField(name = "signup_type")
    @TableField(value = "signup_type")
    @JSONField(name = "signup_type")
    @JsonProperty("signup_type")
    private String signupType;
    /**
     * 格式化的邮件
     */
    @TableField(exist = false)
    @JSONField(name = "email_formatted")
    @JsonProperty("email_formatted")
    private String emailFormatted;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 共享合作伙伴
     */
    @DEField(name = "partner_share")
    @TableField(value = "partner_share")
    @JSONField(name = "partner_share")
    @JsonProperty("partner_share")
    private Boolean partnerShare;
    /**
     * 街道 2
     */
    @TableField(value = "street2")
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;
    /**
     * 工作岗位
     */
    @TableField(value = "function")
    @JSONField(name = "function")
    @JsonProperty("function")
    private String function;
    /**
     * 应付总计
     */
    @TableField(exist = false)
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private BigDecimal debit;
    /**
     * 付款令牌计数
     */
    @TableField(exist = false)
    @JSONField(name = "payment_token_count")
    @JsonProperty("payment_token_count")
    private Integer paymentTokenCount;
    /**
     * 内部参考
     */
    @TableField(value = "ref")
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;
    /**
     * 公司数据库ID
     */
    @DEField(name = "partner_gid")
    @TableField(value = "partner_gid")
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;
    /**
     * 注册令牌（ Token  ）是有效的
     */
    @TableField(exist = false)
    @JSONField(name = "signup_valid")
    @JsonProperty("signup_valid")
    private Boolean signupValid;
    /**
     * 网站opengraph图像
     */
    @DEField(name = "website_meta_og_img")
    @TableField(value = "website_meta_og_img")
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;
    /**
     * 小尺寸图像
     */
    @TableField(exist = false)
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;
    /**
     * 银行
     */
    @TableField(exist = false)
    @JSONField(name = "bank_account_count")
    @JsonProperty("bank_account_count")
    private Integer bankAccountCount;
    /**
     * 街道
     */
    @TableField(value = "street")
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;
    /**
     * 销售警告
     */
    @DEField(name = "sale_warn")
    @TableField(value = "sale_warn")
    @JSONField(name = "sale_warn")
    @JsonProperty("sale_warn")
    private String saleWarn;
    /**
     * 退回
     */
    @DEField(name = "message_bounce")
    @TableField(value = "message_bounce")
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;
    /**
     * 操作次数
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 商机
     */
    @TableField(exist = false)
    @JSONField(name = "opportunity_count")
    @JsonProperty("opportunity_count")
    private Integer opportunityCount;
    /**
     * 日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 自己
     */
    @TableField(exist = false)
    @JSONField(name = "self")
    @JsonProperty("self")
    private Integer self;
    /**
     * IM的状态
     */
    @TableField(exist = false)
    @JSONField(name = "im_status")
    @JsonProperty("im_status")
    private String imStatus;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 错误个数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 发票消息
     */
    @DEField(name = "invoice_warn_msg")
    @TableField(value = "invoice_warn_msg")
    @JSONField(name = "invoice_warn_msg")
    @JsonProperty("invoice_warn_msg")
    private String invoiceWarnMsg;
    /**
     * 前置操作
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 库存拣货
     */
    @DEField(name = "picking_warn")
    @TableField(value = "picking_warn")
    @JSONField(name = "picking_warn")
    @JsonProperty("picking_warn")
    private String pickingWarn;
    /**
     * 客户合同
     */
    @TableField(exist = false)
    @JSONField(name = "contract_ids")
    @JsonProperty("contract_ids")
    private String contractIds;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;
    /**
     * 网站
     */
    @TableField(value = "website")
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;
    /**
     * 手机
     */
    @TableField(value = "mobile")
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 城市
     */
    @TableField(value = "city")
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;
    /**
     * 客户付款条款
     */
    @TableField(exist = false)
    @JSONField(name = "property_payment_term_id")
    @JsonProperty("property_payment_term_id")
    private Integer propertyPaymentTermId;
    /**
     * 用户
     */
    @TableField(exist = false)
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;
    /**
     * 网站meta关键词
     */
    @DEField(name = "website_meta_keywords")
    @TableField(value = "website_meta_keywords")
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;
    /**
     * 渠道
     */
    @TableField(exist = false)
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;
    /**
     * 采购订单
     */
    @DEField(name = "purchase_warn")
    @TableField(value = "purchase_warn")
    @JSONField(name = "purchase_warn")
    @JsonProperty("purchase_warn")
    private String purchaseWarn;
    /**
     * 日记账项目
     */
    @TableField(exist = false)
    @JSONField(name = "journal_item_count")
    @JsonProperty("journal_item_count")
    private Integer journalItemCount;
    /**
     * 供应商位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_supplier")
    @JsonProperty("property_stock_supplier")
    private Integer propertyStockSupplier;
    /**
     * 应付账款
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    private Integer propertyAccountPayableId;
    /**
     * 网站业务伙伴简介
     */
    @DEField(name = "website_short_description")
    @TableField(value = "website_short_description")
    @JSONField(name = "website_short_description")
    @JsonProperty("website_short_description")
    private String websiteShortDescription;
    /**
     * 销售订单消息
     */
    @DEField(name = "sale_warn_msg")
    @TableField(value = "sale_warn_msg")
    @JSONField(name = "sale_warn_msg")
    @JsonProperty("sale_warn_msg")
    private String saleWarnMsg;
    /**
     * 应收总计
     */
    @TableField(exist = false)
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private BigDecimal credit;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 税号
     */
    @TableField(value = "vat")
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;
    /**
     * 供应商付款条款
     */
    @TableField(exist = false)
    @JSONField(name = "property_supplier_payment_term_id")
    @JsonProperty("property_supplier_payment_term_id")
    private Integer propertySupplierPaymentTermId;
    /**
     * supplier rank
     */
    @DEField(name = "supplier_rank")
    @TableField(value = "supplier_rank")
    @JSONField(name = "supplier_rank")
    @JsonProperty("supplier_rank")
    private Integer supplierRank;
    /**
     * 客户位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_customer")
    @JsonProperty("property_stock_customer")
    private Integer propertyStockCustomer;
    /**
     * 便签
     */
    @TableField(value = "comment")
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;
    /**
     * 任务
     */
    @TableField(exist = false)
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    private String taskIds;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * EMail
     */
    @TableField(value = "email")
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;
    /**
     * 采购订单消息
     */
    @DEField(name = "purchase_warn_msg")
    @TableField(value = "purchase_warn_msg")
    @JSONField(name = "purchase_warn_msg")
    @JsonProperty("purchase_warn_msg")
    private String purchaseWarnMsg;
    /**
     * 网站meta标题
     */
    @DEField(name = "website_meta_title")
    @TableField(value = "website_meta_title")
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;
    /**
     * 邮政编码
     */
    @TableField(value = "zip")
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;
    /**
     * 时区偏移
     */
    @TableField(exist = false)
    @JSONField(name = "tz_offset")
    @JsonProperty("tz_offset")
    private String tzOffset;
    /**
     * 公司类别
     */
    @TableField(exist = false)
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    private String companyType;
    /**
     * 下一个活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * # 任务
     */
    @TableField(exist = false)
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;
    /**
     * 信用额度
     */
    @DEField(name = "credit_limit")
    @TableField(value = "credit_limit")
    @JSONField(name = "credit_limit")
    @JsonProperty("credit_limit")
    private Double creditLimit;
    /**
     * 应收账款
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    private Integer propertyAccountReceivableId;
    /**
     * 供应商货币
     */
    @TableField(exist = false)
    @JSONField(name = "property_purchase_currency_id")
    @JsonProperty("property_purchase_currency_id")
    private Integer propertyPurchaseCurrencyId;
    /**
     * 库存拣货单消息
     */
    @DEField(name = "picking_warn_msg")
    @TableField(value = "picking_warn_msg")
    @JSONField(name = "picking_warn_msg")
    @JsonProperty("picking_warn_msg")
    private String pickingWarnMsg;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 注册网址
     */
    @TableField(exist = false)
    @JSONField(name = "signup_url")
    @JsonProperty("signup_url")
    private String signupUrl;
    /**
     * 语言
     */
    @TableField(value = "lang")
    @JSONField(name = "lang")
    @JsonProperty("lang")
    private String lang;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 税科目调整
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_position_id")
    @JsonProperty("property_account_position_id")
    private Integer propertyAccountPositionId;
    /**
     * 登记网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 条码
     */
    @TableField(value = "barcode")
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;
    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @TableField(value = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 销售订单个数
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_count")
    @JsonProperty("sale_order_count")
    private Integer saleOrderCount;
    /**
     * 中等尺寸图像
     */
    @TableField(exist = false)
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;
    /**
     * 附加信息
     */
    @DEField(name = "additional_info")
    @TableField(value = "additional_info")
    @JSONField(name = "additional_info")
    @JsonProperty("additional_info")
    private String additionalInfo;
    /**
     * 商机
     */
    @TableField(exist = false)
    @JSONField(name = "opportunity_ids")
    @JsonProperty("opportunity_ids")
    private String opportunityIds;
    /**
     * 合同统计
     */
    @TableField(exist = false)
    @JSONField(name = "contracts_count")
    @JsonProperty("contracts_count")
    private Integer contractsCount;
    /**
     * 应付限额
     */
    @DEField(name = "debit_limit")
    @TableField(value = "debit_limit")
    @JSONField(name = "debit_limit")
    @JsonProperty("debit_limit")
    private BigDecimal debitLimit;
    /**
     * 网站网址
     */
    @TableField(exist = false)
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;
    /**
     * 销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    private String saleOrderIds;
    /**
     * 最近的在线销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "last_website_so_id")
    @JsonProperty("last_website_so_id")
    private Integer lastWebsiteSoId;
    /**
     * SEO优化
     */
    @TableField(exist = false)
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private Boolean isSeoOptimized;
    /**
     * 公司名称实体
     */
    @DEField(name = "commercial_company_name")
    @TableField(value = "commercial_company_name")
    @JSONField(name = "commercial_company_name")
    @JsonProperty("commercial_company_name")
    private String commercialCompanyName;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 称谓
     */
    @TableField(exist = false)
    @JSONField(name = "title_text")
    @JsonProperty("title_text")
    private String titleText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 国家/地区
     */
    @TableField(exist = false)
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;
    /**
     * 省/ 州
     */
    @TableField(exist = false)
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    private String stateIdText;
    /**
     * 商业实体
     */
    @TableField(exist = false)
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    private String commercialPartnerIdText;
    /**
     * 上级名称
     */
    @TableField(exist = false)
    @JSONField(name = "parent_name")
    @JsonProperty("parent_name")
    private String parentName;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 工业
     */
    @TableField(exist = false)
    @JSONField(name = "industry_id_text")
    @JsonProperty("industry_id_text")
    private String industryIdText;
    /**
     * 销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;
    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @TableField(value = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Long teamId;
    /**
     * 省/ 州
     */
    @DEField(name = "state_id")
    @TableField(value = "state_id")
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Long stateId;
    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 关联公司
     */
    @DEField(name = "parent_id")
    @TableField(value = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 称谓
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private Long title;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 商业实体
     */
    @DEField(name = "commercial_partner_id")
    @TableField(value = "commercial_partner_id")
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Long commercialPartnerId;
    /**
     * 工业
     */
    @DEField(name = "industry_id")
    @TableField(value = "industry_id")
    @JSONField(name = "industry_id")
    @JsonProperty("industry_id")
    private Long industryId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 国家/地区
     */
    @DEField(name = "country_id")
    @TableField(value = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Long countryId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state odooState;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_industry odooIndustry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title odooTitle;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooCommercialPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooParent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [地址类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [公司名称]
     */
    public void setCompanyName(String companyName){
        this.companyName = companyName ;
        this.modify("company_name",companyName);
    }

    /**
     * 设置 [最近的发票和付款匹配时间]
     */
    public void setLastTimeEntriesChecked(Timestamp lastTimeEntriesChecked){
        this.lastTimeEntriesChecked = lastTimeEntriesChecked ;
        this.modify("last_time_entries_checked",lastTimeEntriesChecked);
    }

    /**
     * 格式化日期 [最近的发票和付款匹配时间]
     */
    public String formatLastTimeEntriesChecked(){
        if (this.lastTimeEntriesChecked == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(lastTimeEntriesChecked);
    }
    /**
     * 设置 [发票]
     */
    public void setInvoiceWarn(String invoiceWarn){
        this.invoiceWarn = invoiceWarn ;
        this.modify("invoice_warn",invoiceWarn);
    }

    /**
     * 设置 [注册到期]
     */
    public void setSignupExpiration(Timestamp signupExpiration){
        this.signupExpiration = signupExpiration ;
        this.modify("signup_expiration",signupExpiration);
    }

    /**
     * 格式化日期 [注册到期]
     */
    public String formatSignupExpiration(){
        if (this.signupExpiration == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(signupExpiration);
    }
    /**
     * 设置 [customer rank]
     */
    public void setCustomerRank(Integer customerRank){
        this.customerRank = customerRank ;
        this.modify("customer_rank",customerRank);
    }

    /**
     * 设置 [网站业务伙伴的详细说明]
     */
    public void setWebsiteDescription(String websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [员工]
     */
    public void setEmployee(Boolean employee){
        this.employee = employee ;
        this.modify("employee",employee);
    }

    /**
     * 设置 [显示名称]
     */
    public void setDisplayName(String displayName){
        this.displayName = displayName ;
        this.modify("display_name",displayName);
    }

    /**
     * 设置 [网站元说明]
     */
    public void setWebsiteMetaDescription(String websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }

    /**
     * 设置 [注册令牌 Token]
     */
    public void setSignupToken(String signupToken){
        this.signupToken = signupToken ;
        this.modify("signup_token",signupToken);
    }

    /**
     * 设置 [公司]
     */
    public void setIsCompany(Boolean isCompany){
        this.isCompany = isCompany ;
        this.modify("is_company",isCompany);
    }

    /**
     * 设置 [电话]
     */
    public void setPhone(String phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }

    /**
     * 设置 [时区]
     */
    public void setTz(String tz){
        this.tz = tz ;
        this.modify("tz",tz);
    }

    /**
     * 设置 [最后的提醒已经标志为已读]
     */
    public void setCalendarLastNotifAck(Timestamp calendarLastNotifAck){
        this.calendarLastNotifAck = calendarLastNotifAck ;
        this.modify("calendar_last_notif_ack",calendarLastNotifAck);
    }

    /**
     * 格式化日期 [最后的提醒已经标志为已读]
     */
    public String formatCalendarLastNotifAck(){
        if (this.calendarLastNotifAck == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(calendarLastNotifAck);
    }
    /**
     * 设置 [注册令牌（Token）类型]
     */
    public void setSignupType(String signupType){
        this.signupType = signupType ;
        this.modify("signup_type",signupType);
    }

    /**
     * 设置 [共享合作伙伴]
     */
    public void setPartnerShare(Boolean partnerShare){
        this.partnerShare = partnerShare ;
        this.modify("partner_share",partnerShare);
    }

    /**
     * 设置 [街道 2]
     */
    public void setStreet2(String street2){
        this.street2 = street2 ;
        this.modify("street2",street2);
    }

    /**
     * 设置 [工作岗位]
     */
    public void setFunction(String function){
        this.function = function ;
        this.modify("function",function);
    }

    /**
     * 设置 [内部参考]
     */
    public void setRef(String ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }

    /**
     * 设置 [公司数据库ID]
     */
    public void setPartnerGid(Integer partnerGid){
        this.partnerGid = partnerGid ;
        this.modify("partner_gid",partnerGid);
    }

    /**
     * 设置 [网站opengraph图像]
     */
    public void setWebsiteMetaOgImg(String websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }

    /**
     * 设置 [街道]
     */
    public void setStreet(String street){
        this.street = street ;
        this.modify("street",street);
    }

    /**
     * 设置 [销售警告]
     */
    public void setSaleWarn(String saleWarn){
        this.saleWarn = saleWarn ;
        this.modify("sale_warn",saleWarn);
    }

    /**
     * 设置 [退回]
     */
    public void setMessageBounce(Integer messageBounce){
        this.messageBounce = messageBounce ;
        this.modify("message_bounce",messageBounce);
    }

    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    /**
     * 设置 [发票消息]
     */
    public void setInvoiceWarnMsg(String invoiceWarnMsg){
        this.invoiceWarnMsg = invoiceWarnMsg ;
        this.modify("invoice_warn_msg",invoiceWarnMsg);
    }

    /**
     * 设置 [库存拣货]
     */
    public void setPickingWarn(String pickingWarn){
        this.pickingWarn = pickingWarn ;
        this.modify("picking_warn",pickingWarn);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsite(String website){
        this.website = website ;
        this.modify("website",website);
    }

    /**
     * 设置 [手机]
     */
    public void setMobile(String mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [城市]
     */
    public void setCity(String city){
        this.city = city ;
        this.modify("city",city);
    }

    /**
     * 设置 [网站meta关键词]
     */
    public void setWebsiteMetaKeywords(String websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }

    /**
     * 设置 [采购订单]
     */
    public void setPurchaseWarn(String purchaseWarn){
        this.purchaseWarn = purchaseWarn ;
        this.modify("purchase_warn",purchaseWarn);
    }

    /**
     * 设置 [网站业务伙伴简介]
     */
    public void setWebsiteShortDescription(String websiteShortDescription){
        this.websiteShortDescription = websiteShortDescription ;
        this.modify("website_short_description",websiteShortDescription);
    }

    /**
     * 设置 [销售订单消息]
     */
    public void setSaleWarnMsg(String saleWarnMsg){
        this.saleWarnMsg = saleWarnMsg ;
        this.modify("sale_warn_msg",saleWarnMsg);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [税号]
     */
    public void setVat(String vat){
        this.vat = vat ;
        this.modify("vat",vat);
    }

    /**
     * 设置 [supplier rank]
     */
    public void setSupplierRank(Integer supplierRank){
        this.supplierRank = supplierRank ;
        this.modify("supplier_rank",supplierRank);
    }

    /**
     * 设置 [便签]
     */
    public void setComment(String comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }

    /**
     * 设置 [EMail]
     */
    public void setEmail(String email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [采购订单消息]
     */
    public void setPurchaseWarnMsg(String purchaseWarnMsg){
        this.purchaseWarnMsg = purchaseWarnMsg ;
        this.modify("purchase_warn_msg",purchaseWarnMsg);
    }

    /**
     * 设置 [网站meta标题]
     */
    public void setWebsiteMetaTitle(String websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }

    /**
     * 设置 [邮政编码]
     */
    public void setZip(String zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }

    /**
     * 设置 [信用额度]
     */
    public void setCreditLimit(Double creditLimit){
        this.creditLimit = creditLimit ;
        this.modify("credit_limit",creditLimit);
    }

    /**
     * 设置 [库存拣货单消息]
     */
    public void setPickingWarnMsg(String pickingWarnMsg){
        this.pickingWarnMsg = pickingWarnMsg ;
        this.modify("picking_warn_msg",pickingWarnMsg);
    }

    /**
     * 设置 [语言]
     */
    public void setLang(String lang){
        this.lang = lang ;
        this.modify("lang",lang);
    }

    /**
     * 设置 [登记网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [条码]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [已发布]
     */
    public void setIsPublished(Boolean isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [附加信息]
     */
    public void setAdditionalInfo(String additionalInfo){
        this.additionalInfo = additionalInfo ;
        this.modify("additional_info",additionalInfo);
    }

    /**
     * 设置 [应付限额]
     */
    public void setDebitLimit(BigDecimal debitLimit){
        this.debitLimit = debitLimit ;
        this.modify("debit_limit",debitLimit);
    }

    /**
     * 设置 [公司名称实体]
     */
    public void setCommercialCompanyName(String commercialCompanyName){
        this.commercialCompanyName = commercialCompanyName ;
        this.modify("commercial_company_name",commercialCompanyName);
    }

    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Long teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [省/ 州]
     */
    public void setStateId(Long stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }

    /**
     * 设置 [销售员]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [关联公司]
     */
    public void setParentId(Long parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [称谓]
     */
    public void setTitle(Long title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [商业实体]
     */
    public void setCommercialPartnerId(Long commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }

    /**
     * 设置 [工业]
     */
    public void setIndustryId(Long industryId){
        this.industryId = industryId ;
        this.modify("industry_id",industryId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [国家/地区]
     */
    public void setCountryId(Long countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


