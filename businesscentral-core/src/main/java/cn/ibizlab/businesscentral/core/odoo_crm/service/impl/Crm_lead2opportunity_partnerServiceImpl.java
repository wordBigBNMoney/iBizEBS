package cn.ibizlab.businesscentral.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partnerService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_crm.mapper.Crm_lead2opportunity_partnerMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[转化线索为商机（单个）] 服务对象接口实现
 */
@Slf4j
@Service("Crm_lead2opportunity_partnerServiceImpl")
public class Crm_lead2opportunity_partnerServiceImpl extends EBSServiceImpl<Crm_lead2opportunity_partnerMapper, Crm_lead2opportunity_partner> implements ICrm_lead2opportunity_partnerService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "crm.lead2opportunity.partner" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Crm_lead2opportunity_partner et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICrm_lead2opportunity_partnerService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Crm_lead2opportunity_partner> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Crm_lead2opportunity_partner et) {
        Crm_lead2opportunity_partner old = new Crm_lead2opportunity_partner() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICrm_lead2opportunity_partnerService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICrm_lead2opportunity_partnerService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Crm_lead2opportunity_partner> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Crm_lead2opportunity_partner get(Long key) {
        Crm_lead2opportunity_partner et = getById(key);
        if(et==null){
            et=new Crm_lead2opportunity_partner();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Crm_lead2opportunity_partner getDraft(Crm_lead2opportunity_partner et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Crm_lead2opportunity_partner et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Crm_lead2opportunity_partner et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Crm_lead2opportunity_partner et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Crm_lead2opportunity_partner> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Crm_lead2opportunity_partner> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Crm_lead2opportunity_partner> selectByTeamId(Long id) {
        return baseMapper.selectByTeamId(id);
    }
    @Override
    public void resetByTeamId(Long id) {
        this.update(new UpdateWrapper<Crm_lead2opportunity_partner>().set("team_id",null).eq("team_id",id));
    }

    @Override
    public void resetByTeamId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Crm_lead2opportunity_partner>().set("team_id",null).in("team_id",ids));
    }

    @Override
    public void removeByTeamId(Long id) {
        this.remove(new QueryWrapper<Crm_lead2opportunity_partner>().eq("team_id",id));
    }

	@Override
    public List<Crm_lead2opportunity_partner> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Crm_lead2opportunity_partner>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Crm_lead2opportunity_partner>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Crm_lead2opportunity_partner>().eq("partner_id",id));
    }

	@Override
    public List<Crm_lead2opportunity_partner> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Crm_lead2opportunity_partner>().eq("create_uid",id));
    }

	@Override
    public List<Crm_lead2opportunity_partner> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Crm_lead2opportunity_partner>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Crm_lead2opportunity_partner>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Crm_lead2opportunity_partner>().eq("user_id",id));
    }

	@Override
    public List<Crm_lead2opportunity_partner> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Crm_lead2opportunity_partner>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Crm_lead2opportunity_partner> searchDefault(Crm_lead2opportunity_partnerSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Crm_lead2opportunity_partner> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Crm_lead2opportunity_partner>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Crm_lead2opportunity_partner et){
        //实体关系[DER1N_CRM_LEAD2OPPORTUNITY_PARTNER__CRM_TEAM__TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getTeamId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam=et.getOdooTeam();
            if(ObjectUtils.isEmpty(odooTeam)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team majorEntity=crmTeamService.get(et.getTeamId());
                et.setOdooTeam(majorEntity);
                odooTeam=majorEntity;
            }
            et.setTeamIdText(odooTeam.getName());
        }
        //实体关系[DER1N_CRM_LEAD2OPPORTUNITY_PARTNER__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_CRM_LEAD2OPPORTUNITY_PARTNER__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_CRM_LEAD2OPPORTUNITY_PARTNER__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_CRM_LEAD2OPPORTUNITY_PARTNER__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Crm_lead2opportunity_partner> getCrmLead2opportunityPartnerByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Crm_lead2opportunity_partner> getCrmLead2opportunityPartnerByEntities(List<Crm_lead2opportunity_partner> entities) {
        List ids =new ArrayList();
        for(Crm_lead2opportunity_partner entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



