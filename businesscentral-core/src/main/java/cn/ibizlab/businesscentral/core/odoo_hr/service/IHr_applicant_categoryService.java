package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant_category;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_applicant_categorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_applicant_category] 服务对象接口
 */
public interface IHr_applicant_categoryService extends IService<Hr_applicant_category>{

    boolean create(Hr_applicant_category et) ;
    void createBatch(List<Hr_applicant_category> list) ;
    boolean update(Hr_applicant_category et) ;
    void updateBatch(List<Hr_applicant_category> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_applicant_category get(Long key) ;
    Hr_applicant_category getDraft(Hr_applicant_category et) ;
    boolean checkKey(Hr_applicant_category et) ;
    boolean save(Hr_applicant_category et) ;
    void saveBatch(List<Hr_applicant_category> list) ;
    Page<Hr_applicant_category> searchDefault(Hr_applicant_categorySearchContext context) ;
    List<Hr_applicant_category> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_applicant_category> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_applicant_category> getHrApplicantCategoryByIds(List<Long> ids) ;
    List<Hr_applicant_category> getHrApplicantCategoryByEntities(List<Hr_applicant_category> entities) ;
}


