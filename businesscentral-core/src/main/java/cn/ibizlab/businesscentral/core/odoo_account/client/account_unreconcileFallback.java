package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_unreconcile;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_unreconcileSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_unreconcile] 服务对象接口
 */
@Component
public class account_unreconcileFallback implements account_unreconcileFeignClient{

    public Page<Account_unreconcile> search(Account_unreconcileSearchContext context){
            return null;
     }


    public Account_unreconcile get(Long id){
            return null;
     }




    public Account_unreconcile create(Account_unreconcile account_unreconcile){
            return null;
     }
    public Boolean createBatch(List<Account_unreconcile> account_unreconciles){
            return false;
     }


    public Account_unreconcile update(Long id, Account_unreconcile account_unreconcile){
            return null;
     }
    public Boolean updateBatch(List<Account_unreconcile> account_unreconciles){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_unreconcile> select(){
            return null;
     }

    public Account_unreconcile getDraft(){
            return null;
    }



    public Boolean checkKey(Account_unreconcile account_unreconcile){
            return false;
     }


    public Boolean save(Account_unreconcile account_unreconcile){
            return false;
     }
    public Boolean saveBatch(List<Account_unreconcile> account_unreconciles){
            return false;
     }

    public Page<Account_unreconcile> searchDefault(Account_unreconcileSearchContext context){
            return null;
     }


}
