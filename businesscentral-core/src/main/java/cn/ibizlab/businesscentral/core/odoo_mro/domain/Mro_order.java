package cn.ibizlab.businesscentral.core.odoo_mro.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[Maintenance Order]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MRO_ORDER",resultMap = "Mro_orderResultMap")
public class Mro_order extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 保养类型
     */
    @DEField(name = "maintenance_type")
    @TableField(value = "maintenance_type")
    @JSONField(name = "maintenance_type")
    @JsonProperty("maintenance_type")
    private String maintenanceType;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * Documentation Description
     */
    @DEField(name = "documentation_description")
    @TableField(value = "documentation_description")
    @JSONField(name = "documentation_description")
    @JsonProperty("documentation_description")
    private String documentationDescription;
    /**
     * Parts Moved Lines
     */
    @TableField(exist = false)
    @JSONField(name = "parts_moved_lines")
    @JsonProperty("parts_moved_lines")
    private String partsMovedLines;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * Asset Category
     */
    @TableField(exist = false)
    @JSONField(name = "category_ids")
    @JsonProperty("category_ids")
    private String categoryIds;
    /**
     * 计划日期
     */
    @DEField(name = "date_scheduled")
    @TableField(value = "date_scheduled")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_scheduled" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_scheduled")
    private Timestamp dateScheduled;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * Labor Description
     */
    @DEField(name = "labor_description")
    @TableField(value = "labor_description")
    @JSONField(name = "labor_description")
    @JsonProperty("labor_description")
    private String laborDescription;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 源文档
     */
    @TableField(value = "origin")
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;
    /**
     * 错误个数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * Parts Ready Lines
     */
    @TableField(exist = false)
    @JSONField(name = "parts_ready_lines")
    @JsonProperty("parts_ready_lines")
    private String partsReadyLines;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * Parts Move Lines
     */
    @TableField(exist = false)
    @JSONField(name = "parts_move_lines")
    @JsonProperty("parts_move_lines")
    private String partsMoveLines;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 操作次数
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * Operations Description
     */
    @DEField(name = "operations_description")
    @TableField(value = "operations_description")
    @JSONField(name = "operations_description")
    @JsonProperty("operations_description")
    private String operationsDescription;
    /**
     * Problem Description
     */
    @DEField(name = "problem_description")
    @TableField(value = "problem_description")
    @JSONField(name = "problem_description")
    @JsonProperty("problem_description")
    private String problemDescription;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * Planned Date
     */
    @DEField(name = "date_planned")
    @TableField(value = "date_planned")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned")
    private Timestamp datePlanned;
    /**
     * Procurement group
     */
    @DEField(name = "procurement_group_id")
    @TableField(value = "procurement_group_id")
    @JSONField(name = "procurement_group_id")
    @JsonProperty("procurement_group_id")
    private Integer procurementGroupId;
    /**
     * Execution Date
     */
    @DEField(name = "date_execution")
    @TableField(value = "date_execution")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_execution" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_execution")
    private Timestamp dateExecution;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * Planned parts
     */
    @TableField(exist = false)
    @JSONField(name = "parts_lines")
    @JsonProperty("parts_lines")
    private String partsLines;
    /**
     * 编号
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 前置操作
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * Tools Description
     */
    @DEField(name = "tools_description")
    @TableField(value = "tools_description")
    @JSONField(name = "tools_description")
    @JsonProperty("tools_description")
    private String toolsDescription;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 工单
     */
    @TableField(exist = false)
    @JSONField(name = "wo_id_text")
    @JsonProperty("wo_id_text")
    private String woIdText;
    /**
     * Asset
     */
    @TableField(exist = false)
    @JSONField(name = "asset_id_text")
    @JsonProperty("asset_id_text")
    private String assetIdText;
    /**
     * Task
     */
    @TableField(exist = false)
    @JSONField(name = "task_id_text")
    @JsonProperty("task_id_text")
    private String taskIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 请求
     */
    @TableField(exist = false)
    @JSONField(name = "request_id_text")
    @JsonProperty("request_id_text")
    private String requestIdText;
    /**
     * 负责人
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 请求
     */
    @DEField(name = "request_id")
    @TableField(value = "request_id")
    @JSONField(name = "request_id")
    @JsonProperty("request_id")
    private Long requestId;
    /**
     * Task
     */
    @DEField(name = "task_id")
    @TableField(value = "task_id")
    @JSONField(name = "task_id")
    @JsonProperty("task_id")
    private Long taskId;
    /**
     * 工单
     */
    @DEField(name = "wo_id")
    @TableField(value = "wo_id")
    @JSONField(name = "wo_id")
    @JsonProperty("wo_id")
    private Long woId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 负责人
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * Asset
     */
    @DEField(name = "asset_id")
    @TableField(value = "asset_id")
    @JSONField(name = "asset_id")
    @JsonProperty("asset_id")
    private Long assetId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_asset odooAsset;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_request odooRequest;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task odooTask;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_workorder odooWo;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [保养类型]
     */
    public void setMaintenanceType(String maintenanceType){
        this.maintenanceType = maintenanceType ;
        this.modify("maintenance_type",maintenanceType);
    }

    /**
     * 设置 [Documentation Description]
     */
    public void setDocumentationDescription(String documentationDescription){
        this.documentationDescription = documentationDescription ;
        this.modify("documentation_description",documentationDescription);
    }

    /**
     * 设置 [计划日期]
     */
    public void setDateScheduled(Timestamp dateScheduled){
        this.dateScheduled = dateScheduled ;
        this.modify("date_scheduled",dateScheduled);
    }

    /**
     * 格式化日期 [计划日期]
     */
    public String formatDateScheduled(){
        if (this.dateScheduled == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateScheduled);
    }
    /**
     * 设置 [Labor Description]
     */
    public void setLaborDescription(String laborDescription){
        this.laborDescription = laborDescription ;
        this.modify("labor_description",laborDescription);
    }

    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [Operations Description]
     */
    public void setOperationsDescription(String operationsDescription){
        this.operationsDescription = operationsDescription ;
        this.modify("operations_description",operationsDescription);
    }

    /**
     * 设置 [Problem Description]
     */
    public void setProblemDescription(String problemDescription){
        this.problemDescription = problemDescription ;
        this.modify("problem_description",problemDescription);
    }

    /**
     * 设置 [Planned Date]
     */
    public void setDatePlanned(Timestamp datePlanned){
        this.datePlanned = datePlanned ;
        this.modify("date_planned",datePlanned);
    }

    /**
     * 格式化日期 [Planned Date]
     */
    public String formatDatePlanned(){
        if (this.datePlanned == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(datePlanned);
    }
    /**
     * 设置 [Procurement group]
     */
    public void setProcurementGroupId(Integer procurementGroupId){
        this.procurementGroupId = procurementGroupId ;
        this.modify("procurement_group_id",procurementGroupId);
    }

    /**
     * 设置 [Execution Date]
     */
    public void setDateExecution(Timestamp dateExecution){
        this.dateExecution = dateExecution ;
        this.modify("date_execution",dateExecution);
    }

    /**
     * 格式化日期 [Execution Date]
     */
    public String formatDateExecution(){
        if (this.dateExecution == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateExecution);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [编号]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [Tools Description]
     */
    public void setToolsDescription(String toolsDescription){
        this.toolsDescription = toolsDescription ;
        this.modify("tools_description",toolsDescription);
    }

    /**
     * 设置 [请求]
     */
    public void setRequestId(Long requestId){
        this.requestId = requestId ;
        this.modify("request_id",requestId);
    }

    /**
     * 设置 [Task]
     */
    public void setTaskId(Long taskId){
        this.taskId = taskId ;
        this.modify("task_id",taskId);
    }

    /**
     * 设置 [工单]
     */
    public void setWoId(Long woId){
        this.woId = woId ;
        this.modify("wo_id",woId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [负责人]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [Asset]
     */
    public void setAssetId(Long assetId){
        this.assetId = assetId ;
        this.modify("asset_id",assetId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


