package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_bank;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_bankSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_bank] 服务对象接口
 */
public interface IRes_bankService extends IService<Res_bank>{

    boolean create(Res_bank et) ;
    void createBatch(List<Res_bank> list) ;
    boolean update(Res_bank et) ;
    void updateBatch(List<Res_bank> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_bank get(Long key) ;
    Res_bank getDraft(Res_bank et) ;
    boolean checkKey(Res_bank et) ;
    boolean save(Res_bank et) ;
    void saveBatch(List<Res_bank> list) ;
    Page<Res_bank> searchDefault(Res_bankSearchContext context) ;
    List<Res_bank> selectByState(Long id);
    void resetByState(Long id);
    void resetByState(Collection<Long> ids);
    void removeByState(Long id);
    List<Res_bank> selectByCountry(Long id);
    void resetByCountry(Long id);
    void resetByCountry(Collection<Long> ids);
    void removeByCountry(Long id);
    List<Res_bank> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_bank> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_bank> getResBankByIds(List<Long> ids) ;
    List<Res_bank> getResBankByEntities(List<Res_bank> entities) ;
}


