package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_pricelistSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_pricelist] 服务对象接口
 */
public interface IProduct_pricelistService extends IService<Product_pricelist>{

    boolean create(Product_pricelist et) ;
    void createBatch(List<Product_pricelist> list) ;
    boolean update(Product_pricelist et) ;
    void updateBatch(List<Product_pricelist> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_pricelist get(Long key) ;
    Product_pricelist getDraft(Product_pricelist et) ;
    boolean checkKey(Product_pricelist et) ;
    boolean save(Product_pricelist et) ;
    void saveBatch(List<Product_pricelist> list) ;
    Page<Product_pricelist> searchDefault(Product_pricelistSearchContext context) ;
    List<Product_pricelist> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Product_pricelist> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Product_pricelist> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_pricelist> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_pricelist> getProductPricelistByIds(List<Long> ids) ;
    List<Product_pricelist> getProductPricelistByEntities(List<Product_pricelist> entities) ;
}


