package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_moveSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_move] 服务对象接口
 */
@Component
public class account_moveFallback implements account_moveFeignClient{


    public Account_move update(Long id, Account_move account_move){
            return null;
     }
    public Boolean updateBatch(List<Account_move> account_moves){
            return false;
     }


    public Page<Account_move> search(Account_moveSearchContext context){
            return null;
     }


    public Account_move get(Long id){
            return null;
     }


    public Account_move create(Account_move account_move){
            return null;
     }
    public Boolean createBatch(List<Account_move> account_moves){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Page<Account_move> select(){
            return null;
     }

    public Account_move getDraft(){
            return null;
    }



    public Boolean checkKey(Account_move account_move){
            return false;
     }


    public Boolean save(Account_move account_move){
            return false;
     }
    public Boolean saveBatch(List<Account_move> account_moves){
            return false;
     }

    public Page<Account_move> searchDefault(Account_moveSearchContext context){
            return null;
     }


}
