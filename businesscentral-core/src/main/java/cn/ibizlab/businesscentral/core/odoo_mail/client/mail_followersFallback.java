package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_followersSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_followers] 服务对象接口
 */
@Component
public class mail_followersFallback implements mail_followersFeignClient{


    public Mail_followers update(Long id, Mail_followers mail_followers){
            return null;
     }
    public Boolean updateBatch(List<Mail_followers> mail_followers){
            return false;
     }


    public Mail_followers get(Long id){
            return null;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Mail_followers> search(Mail_followersSearchContext context){
            return null;
     }


    public Mail_followers create(Mail_followers mail_followers){
            return null;
     }
    public Boolean createBatch(List<Mail_followers> mail_followers){
            return false;
     }

    public Page<Mail_followers> select(){
            return null;
     }

    public Mail_followers getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_followers mail_followers){
            return false;
     }


    public Boolean save(Mail_followers mail_followers){
            return false;
     }
    public Boolean saveBatch(List<Mail_followers> mail_followers){
            return false;
     }

    public Page<Mail_followers> searchDefault(Mail_followersSearchContext context){
            return null;
     }


}
