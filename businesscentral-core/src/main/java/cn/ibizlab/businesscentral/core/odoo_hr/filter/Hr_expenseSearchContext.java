package cn.ibizlab.businesscentral.core.odoo_hr.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense;
/**
 * 关系型数据实体[Hr_expense] 查询条件对象
 */
@Slf4j
@Data
public class Hr_expenseSearchContext extends QueryWrapperContext<Hr_expense> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_like;//[说明]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_payment_mode_eq;//[支付]
	public void setN_payment_mode_eq(String n_payment_mode_eq) {
        this.n_payment_mode_eq = n_payment_mode_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_mode_eq)){
            this.getSearchCond().eq("payment_mode", n_payment_mode_eq);
        }
    }
	private String n_sheet_id_text_eq;//[费用报表]
	public void setN_sheet_id_text_eq(String n_sheet_id_text_eq) {
        this.n_sheet_id_text_eq = n_sheet_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sheet_id_text_eq)){
            this.getSearchCond().eq("sheet_id_text", n_sheet_id_text_eq);
        }
    }
	private String n_sheet_id_text_like;//[费用报表]
	public void setN_sheet_id_text_like(String n_sheet_id_text_like) {
        this.n_sheet_id_text_like = n_sheet_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sheet_id_text_like)){
            this.getSearchCond().like("sheet_id_text", n_sheet_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_company_currency_id_text_eq;//[公司货币报告]
	public void setN_company_currency_id_text_eq(String n_company_currency_id_text_eq) {
        this.n_company_currency_id_text_eq = n_company_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_currency_id_text_eq)){
            this.getSearchCond().eq("company_currency_id_text", n_company_currency_id_text_eq);
        }
    }
	private String n_company_currency_id_text_like;//[公司货币报告]
	public void setN_company_currency_id_text_like(String n_company_currency_id_text_like) {
        this.n_company_currency_id_text_like = n_company_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_currency_id_text_like)){
            this.getSearchCond().like("company_currency_id_text", n_company_currency_id_text_like);
        }
    }
	private String n_employee_id_text_eq;//[员工]
	public void setN_employee_id_text_eq(String n_employee_id_text_eq) {
        this.n_employee_id_text_eq = n_employee_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_id_text_eq)){
            this.getSearchCond().eq("employee_id_text", n_employee_id_text_eq);
        }
    }
	private String n_employee_id_text_like;//[员工]
	public void setN_employee_id_text_like(String n_employee_id_text_like) {
        this.n_employee_id_text_like = n_employee_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_employee_id_text_like)){
            this.getSearchCond().like("employee_id_text", n_employee_id_text_like);
        }
    }
	private String n_product_uom_id_text_eq;//[单位]
	public void setN_product_uom_id_text_eq(String n_product_uom_id_text_eq) {
        this.n_product_uom_id_text_eq = n_product_uom_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_eq)){
            this.getSearchCond().eq("product_uom_id_text", n_product_uom_id_text_eq);
        }
    }
	private String n_product_uom_id_text_like;//[单位]
	public void setN_product_uom_id_text_like(String n_product_uom_id_text_like) {
        this.n_product_uom_id_text_like = n_product_uom_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_like)){
            this.getSearchCond().like("product_uom_id_text", n_product_uom_id_text_like);
        }
    }
	private String n_sale_order_id_text_eq;//[销售订单]
	public void setN_sale_order_id_text_eq(String n_sale_order_id_text_eq) {
        this.n_sale_order_id_text_eq = n_sale_order_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_text_eq)){
            this.getSearchCond().eq("sale_order_id_text", n_sale_order_id_text_eq);
        }
    }
	private String n_sale_order_id_text_like;//[销售订单]
	public void setN_sale_order_id_text_like(String n_sale_order_id_text_like) {
        this.n_sale_order_id_text_like = n_sale_order_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_text_like)){
            this.getSearchCond().like("sale_order_id_text", n_sale_order_id_text_like);
        }
    }
	private String n_analytic_account_id_text_eq;//[分析账户]
	public void setN_analytic_account_id_text_eq(String n_analytic_account_id_text_eq) {
        this.n_analytic_account_id_text_eq = n_analytic_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_eq)){
            this.getSearchCond().eq("analytic_account_id_text", n_analytic_account_id_text_eq);
        }
    }
	private String n_analytic_account_id_text_like;//[分析账户]
	public void setN_analytic_account_id_text_like(String n_analytic_account_id_text_like) {
        this.n_analytic_account_id_text_like = n_analytic_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_like)){
            this.getSearchCond().like("analytic_account_id_text", n_analytic_account_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_account_id_text_eq;//[科目]
	public void setN_account_id_text_eq(String n_account_id_text_eq) {
        this.n_account_id_text_eq = n_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_eq)){
            this.getSearchCond().eq("account_id_text", n_account_id_text_eq);
        }
    }
	private String n_account_id_text_like;//[科目]
	public void setN_account_id_text_like(String n_account_id_text_like) {
        this.n_account_id_text_like = n_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_like)){
            this.getSearchCond().like("account_id_text", n_account_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_employee_id_eq;//[员工]
	public void setN_employee_id_eq(Long n_employee_id_eq) {
        this.n_employee_id_eq = n_employee_id_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_id_eq)){
            this.getSearchCond().eq("employee_id", n_employee_id_eq);
        }
    }
	private Long n_sale_order_id_eq;//[销售订单]
	public void setN_sale_order_id_eq(Long n_sale_order_id_eq) {
        this.n_sale_order_id_eq = n_sale_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_eq)){
            this.getSearchCond().eq("sale_order_id", n_sale_order_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_product_uom_id_eq;//[单位]
	public void setN_product_uom_id_eq(Long n_product_uom_id_eq) {
        this.n_product_uom_id_eq = n_product_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_eq)){
            this.getSearchCond().eq("product_uom_id", n_product_uom_id_eq);
        }
    }
	private Long n_sheet_id_eq;//[费用报表]
	public void setN_sheet_id_eq(Long n_sheet_id_eq) {
        this.n_sheet_id_eq = n_sheet_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sheet_id_eq)){
            this.getSearchCond().eq("sheet_id", n_sheet_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_account_id_eq;//[科目]
	public void setN_account_id_eq(Long n_account_id_eq) {
        this.n_account_id_eq = n_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_eq)){
            this.getSearchCond().eq("account_id", n_account_id_eq);
        }
    }
	private Long n_company_currency_id_eq;//[公司货币报告]
	public void setN_company_currency_id_eq(Long n_company_currency_id_eq) {
        this.n_company_currency_id_eq = n_company_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_currency_id_eq)){
            this.getSearchCond().eq("company_currency_id", n_company_currency_id_eq);
        }
    }
	private Long n_analytic_account_id_eq;//[分析账户]
	public void setN_analytic_account_id_eq(Long n_analytic_account_id_eq) {
        this.n_analytic_account_id_eq = n_analytic_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_eq)){
            this.getSearchCond().eq("analytic_account_id", n_analytic_account_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



