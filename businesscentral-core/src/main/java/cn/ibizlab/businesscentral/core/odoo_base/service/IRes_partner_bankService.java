package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_bankSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_partner_bank] 服务对象接口
 */
public interface IRes_partner_bankService extends IService<Res_partner_bank>{

    boolean create(Res_partner_bank et) ;
    void createBatch(List<Res_partner_bank> list) ;
    boolean update(Res_partner_bank et) ;
    void updateBatch(List<Res_partner_bank> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_partner_bank get(Long key) ;
    Res_partner_bank getDraft(Res_partner_bank et) ;
    boolean checkKey(Res_partner_bank et) ;
    boolean save(Res_partner_bank et) ;
    void saveBatch(List<Res_partner_bank> list) ;
    Page<Res_partner_bank> searchDefault(Res_partner_bankSearchContext context) ;
    List<Res_partner_bank> selectByBankId(Long id);
    void resetByBankId(Long id);
    void resetByBankId(Collection<Long> ids);
    void removeByBankId(Long id);
    List<Res_partner_bank> selectByCompanyId(Long id);
    void removeByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Res_partner_bank> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Res_partner_bank> selectByPartnerId(Long id);
    void removeByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Res_partner_bank> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_partner_bank> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_partner_bank> getResPartnerBankByIds(List<Long> ids) ;
    List<Res_partner_bank> getResPartnerBankByEntities(List<Res_partner_bank> entities) ;
}


