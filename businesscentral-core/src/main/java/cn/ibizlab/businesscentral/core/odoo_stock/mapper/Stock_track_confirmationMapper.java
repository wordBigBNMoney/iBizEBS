package cn.ibizlab.businesscentral.core.odoo_stock.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_track_confirmationSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Stock_track_confirmationMapper extends BaseMapper<Stock_track_confirmation>{

    Page<Stock_track_confirmation> searchDefault(IPage page, @Param("srf") Stock_track_confirmationSearchContext context, @Param("ew") Wrapper<Stock_track_confirmation> wrapper) ;
    @Override
    Stock_track_confirmation selectById(Serializable id);
    @Override
    int insert(Stock_track_confirmation entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Stock_track_confirmation entity);
    @Override
    int update(@Param(Constants.ENTITY) Stock_track_confirmation entity, @Param("ew") Wrapper<Stock_track_confirmation> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Stock_track_confirmation> selectByCreateUid(@Param("id") Serializable id) ;

    List<Stock_track_confirmation> selectByWriteUid(@Param("id") Serializable id) ;

    List<Stock_track_confirmation> selectByInventoryId(@Param("id") Serializable id) ;


}
