package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_move_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_move_line] 服务对象接口
 */
public interface IAccount_move_lineService extends IService<Account_move_line>{

    boolean create(Account_move_line et) ;
    void createBatch(List<Account_move_line> list) ;
    boolean update(Account_move_line et) ;
    void updateBatch(List<Account_move_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_move_line get(Long key) ;
    Account_move_line getDraft(Account_move_line et) ;
    boolean checkKey(Account_move_line et) ;
    boolean save(Account_move_line et) ;
    void saveBatch(List<Account_move_line> list) ;
    Page<Account_move_line> searchDefault(Account_move_lineSearchContext context) ;
    List<Account_move_line> selectByUserTypeId(Long id);
    void resetByUserTypeId(Long id);
    void resetByUserTypeId(Collection<Long> ids);
    void removeByUserTypeId(Long id);
    List<Account_move_line> selectByAccountId(Long id);
    void removeByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_move_line> selectByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Collection<Long> ids);
    void removeByAnalyticAccountId(Long id);
    List<Account_move_line> selectByStatementLineId(Long id);
    void resetByStatementLineId(Long id);
    void resetByStatementLineId(Collection<Long> ids);
    void removeByStatementLineId(Long id);
    List<Account_move_line> selectByStatementId(Long id);
    void resetByStatementId(Long id);
    void resetByStatementId(Collection<Long> ids);
    void removeByStatementId(Long id);
    List<Account_move_line> selectByFullReconcileId(Long id);
    void resetByFullReconcileId(Long id);
    void resetByFullReconcileId(Collection<Long> ids);
    void removeByFullReconcileId(Long id);
    List<Account_move_line> selectByInvoiceId(Long id);
    void resetByInvoiceId(Long id);
    void resetByInvoiceId(Collection<Long> ids);
    void removeByInvoiceId(Long id);
    List<Account_move_line> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_move_line> selectByMoveId(Long id);
    void removeByMoveId(Collection<Long> ids);
    void removeByMoveId(Long id);
    List<Account_move_line> selectByPaymentId(Long id);
    void resetByPaymentId(Long id);
    void resetByPaymentId(Collection<Long> ids);
    void removeByPaymentId(Long id);
    List<Account_move_line> selectByTaxLineId(Long id);
    List<Account_move_line> selectByTaxLineId(Collection<Long> ids);
    void removeByTaxLineId(Long id);
    List<Account_move_line> selectByExpenseId(Long id);
    void resetByExpenseId(Long id);
    void resetByExpenseId(Collection<Long> ids);
    void removeByExpenseId(Long id);
    List<Account_move_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Account_move_line> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_move_line> selectByCompanyCurrencyId(Long id);
    void resetByCompanyCurrencyId(Long id);
    void resetByCompanyCurrencyId(Collection<Long> ids);
    void removeByCompanyCurrencyId(Long id);
    List<Account_move_line> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_move_line> selectByPartnerId(Long id);
    List<Account_move_line> selectByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Account_move_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_move_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Account_move_line> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_move_line> getAccountMoveLineByIds(List<Long> ids) ;
    List<Account_move_line> getAccountMoveLineByEntities(List<Account_move_line> entities) ;
}


