package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_refuse_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_expense_refuse_wizard] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-expense-refuse-wizard", fallback = hr_expense_refuse_wizardFallback.class)
public interface hr_expense_refuse_wizardFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_refuse_wizards/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_refuse_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards/search")
    Page<Hr_expense_refuse_wizard> search(@RequestBody Hr_expense_refuse_wizardSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_refuse_wizards/{id}")
    Hr_expense_refuse_wizard get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_refuse_wizards/{id}")
    Hr_expense_refuse_wizard update(@PathVariable("id") Long id,@RequestBody Hr_expense_refuse_wizard hr_expense_refuse_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_refuse_wizards/batch")
    Boolean updateBatch(@RequestBody List<Hr_expense_refuse_wizard> hr_expense_refuse_wizards);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards")
    Hr_expense_refuse_wizard create(@RequestBody Hr_expense_refuse_wizard hr_expense_refuse_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards/batch")
    Boolean createBatch(@RequestBody List<Hr_expense_refuse_wizard> hr_expense_refuse_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_refuse_wizards/select")
    Page<Hr_expense_refuse_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expense_refuse_wizards/getdraft")
    Hr_expense_refuse_wizard getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards/checkkey")
    Boolean checkKey(@RequestBody Hr_expense_refuse_wizard hr_expense_refuse_wizard);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards/save")
    Boolean save(@RequestBody Hr_expense_refuse_wizard hr_expense_refuse_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_expense_refuse_wizard> hr_expense_refuse_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards/searchdefault")
    Page<Hr_expense_refuse_wizard> searchDefault(@RequestBody Hr_expense_refuse_wizardSearchContext context);


}
