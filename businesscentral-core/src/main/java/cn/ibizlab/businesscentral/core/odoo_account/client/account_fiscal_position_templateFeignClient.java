package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_templateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_fiscal_position_template] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-fiscal-position-template", fallback = account_fiscal_position_templateFallback.class)
public interface account_fiscal_position_templateFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_templates/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_templates/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_templates/{id}")
    Account_fiscal_position_template update(@PathVariable("id") Long id,@RequestBody Account_fiscal_position_template account_fiscal_position_template);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_templates/batch")
    Boolean updateBatch(@RequestBody List<Account_fiscal_position_template> account_fiscal_position_templates);



    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_templates/{id}")
    Account_fiscal_position_template get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_templates/search")
    Page<Account_fiscal_position_template> search(@RequestBody Account_fiscal_position_templateSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_templates")
    Account_fiscal_position_template create(@RequestBody Account_fiscal_position_template account_fiscal_position_template);

    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_templates/batch")
    Boolean createBatch(@RequestBody List<Account_fiscal_position_template> account_fiscal_position_templates);



    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_templates/select")
    Page<Account_fiscal_position_template> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_templates/getdraft")
    Account_fiscal_position_template getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_templates/checkkey")
    Boolean checkKey(@RequestBody Account_fiscal_position_template account_fiscal_position_template);


    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_templates/save")
    Boolean save(@RequestBody Account_fiscal_position_template account_fiscal_position_template);

    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_templates/savebatch")
    Boolean saveBatch(@RequestBody List<Account_fiscal_position_template> account_fiscal_position_templates);



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_templates/searchdefault")
    Page<Account_fiscal_position_template> searchDefault(@RequestBody Account_fiscal_position_templateSearchContext context);


}
