package cn.ibizlab.businesscentral.core.odoo_web_editor.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Web_editor_converter_test_sub] 服务对象接口
 */
public interface IWeb_editor_converter_test_subService extends IService<Web_editor_converter_test_sub>{

    boolean create(Web_editor_converter_test_sub et) ;
    void createBatch(List<Web_editor_converter_test_sub> list) ;
    boolean update(Web_editor_converter_test_sub et) ;
    void updateBatch(List<Web_editor_converter_test_sub> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Web_editor_converter_test_sub get(Long key) ;
    Web_editor_converter_test_sub getDraft(Web_editor_converter_test_sub et) ;
    boolean checkKey(Web_editor_converter_test_sub et) ;
    boolean save(Web_editor_converter_test_sub et) ;
    void saveBatch(List<Web_editor_converter_test_sub> list) ;
    Page<Web_editor_converter_test_sub> searchDefault(Web_editor_converter_test_subSearchContext context) ;
    List<Web_editor_converter_test_sub> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Web_editor_converter_test_sub> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Web_editor_converter_test_sub> getWebEditorConverterTestSubByIds(List<Long> ids) ;
    List<Web_editor_converter_test_sub> getWebEditorConverterTestSubByEntities(List<Web_editor_converter_test_sub> entities) ;
}


