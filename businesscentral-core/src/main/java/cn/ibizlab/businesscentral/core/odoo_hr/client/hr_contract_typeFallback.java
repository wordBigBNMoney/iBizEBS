package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_contract_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_contract_type] 服务对象接口
 */
@Component
public class hr_contract_typeFallback implements hr_contract_typeFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Hr_contract_type update(Long id, Hr_contract_type hr_contract_type){
            return null;
     }
    public Boolean updateBatch(List<Hr_contract_type> hr_contract_types){
            return false;
     }


    public Hr_contract_type create(Hr_contract_type hr_contract_type){
            return null;
     }
    public Boolean createBatch(List<Hr_contract_type> hr_contract_types){
            return false;
     }

    public Hr_contract_type get(Long id){
            return null;
     }


    public Page<Hr_contract_type> search(Hr_contract_typeSearchContext context){
            return null;
     }


    public Page<Hr_contract_type> select(){
            return null;
     }

    public Hr_contract_type getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_contract_type hr_contract_type){
            return false;
     }


    public Boolean save(Hr_contract_type hr_contract_type){
            return false;
     }
    public Boolean saveBatch(List<Hr_contract_type> hr_contract_types){
            return false;
     }

    public Page<Hr_contract_type> searchDefault(Hr_contract_typeSearchContext context){
            return null;
     }


}
