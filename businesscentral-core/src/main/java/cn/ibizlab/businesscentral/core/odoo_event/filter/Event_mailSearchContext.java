package cn.ibizlab.businesscentral.core.odoo_event.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_mail;
/**
 * 关系型数据实体[Event_mail] 查询条件对象
 */
@Slf4j
@Data
public class Event_mailSearchContext extends QueryWrapperContext<Event_mail> {

	private String n_interval_type_eq;//[触发器]
	public void setN_interval_type_eq(String n_interval_type_eq) {
        this.n_interval_type_eq = n_interval_type_eq;
        if(!ObjectUtils.isEmpty(this.n_interval_type_eq)){
            this.getSearchCond().eq("interval_type", n_interval_type_eq);
        }
    }
	private String n_interval_unit_eq;//[单位]
	public void setN_interval_unit_eq(String n_interval_unit_eq) {
        this.n_interval_unit_eq = n_interval_unit_eq;
        if(!ObjectUtils.isEmpty(this.n_interval_unit_eq)){
            this.getSearchCond().eq("interval_unit", n_interval_unit_eq);
        }
    }
	private String n_event_id_text_eq;//[活动]
	public void setN_event_id_text_eq(String n_event_id_text_eq) {
        this.n_event_id_text_eq = n_event_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_event_id_text_eq)){
            this.getSearchCond().eq("event_id_text", n_event_id_text_eq);
        }
    }
	private String n_event_id_text_like;//[活动]
	public void setN_event_id_text_like(String n_event_id_text_like) {
        this.n_event_id_text_like = n_event_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_event_id_text_like)){
            this.getSearchCond().like("event_id_text", n_event_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_template_id_text_eq;//[EMail模板]
	public void setN_template_id_text_eq(String n_template_id_text_eq) {
        this.n_template_id_text_eq = n_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_template_id_text_eq)){
            this.getSearchCond().eq("template_id_text", n_template_id_text_eq);
        }
    }
	private String n_template_id_text_like;//[EMail模板]
	public void setN_template_id_text_like(String n_template_id_text_like) {
        this.n_template_id_text_like = n_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_template_id_text_like)){
            this.getSearchCond().like("template_id_text", n_template_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_event_id_eq;//[活动]
	public void setN_event_id_eq(Long n_event_id_eq) {
        this.n_event_id_eq = n_event_id_eq;
        if(!ObjectUtils.isEmpty(this.n_event_id_eq)){
            this.getSearchCond().eq("event_id", n_event_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_template_id_eq;//[EMail模板]
	public void setN_template_id_eq(Long n_template_id_eq) {
        this.n_template_id_eq = n_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_template_id_eq)){
            this.getSearchCond().eq("template_id", n_template_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



