package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mass_mailing_list] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-mass-mailing-list", fallback = mail_mass_mailing_listFallback.class)
public interface mail_mass_mailing_listFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_lists/{id}")
    Mail_mass_mailing_list update(@PathVariable("id") Long id,@RequestBody Mail_mass_mailing_list mail_mass_mailing_list);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_lists/batch")
    Boolean updateBatch(@RequestBody List<Mail_mass_mailing_list> mail_mass_mailing_lists);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/search")
    Page<Mail_mass_mailing_list> search(@RequestBody Mail_mass_mailing_listSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists")
    Mail_mass_mailing_list create(@RequestBody Mail_mass_mailing_list mail_mass_mailing_list);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/batch")
    Boolean createBatch(@RequestBody List<Mail_mass_mailing_list> mail_mass_mailing_lists);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_lists/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_lists/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_lists/{id}")
    Mail_mass_mailing_list get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_lists/select")
    Page<Mail_mass_mailing_list> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_lists/getdraft")
    Mail_mass_mailing_list getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/checkkey")
    Boolean checkKey(@RequestBody Mail_mass_mailing_list mail_mass_mailing_list);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/save")
    Boolean save(@RequestBody Mail_mass_mailing_list mail_mass_mailing_list);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_mass_mailing_list> mail_mass_mailing_lists);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/searchdefault")
    Page<Mail_mass_mailing_list> searchDefault(@RequestBody Mail_mass_mailing_listSearchContext context);


}
