package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_lang;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_langSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_lang] 服务对象接口
 */
@Component
public class res_langFallback implements res_langFeignClient{

    public Res_lang get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Res_lang update(Long id, Res_lang res_lang){
            return null;
     }
    public Boolean updateBatch(List<Res_lang> res_langs){
            return false;
     }




    public Res_lang create(Res_lang res_lang){
            return null;
     }
    public Boolean createBatch(List<Res_lang> res_langs){
            return false;
     }


    public Page<Res_lang> search(Res_langSearchContext context){
            return null;
     }


    public Page<Res_lang> select(){
            return null;
     }

    public Res_lang getDraft(){
            return null;
    }



    public Boolean checkKey(Res_lang res_lang){
            return false;
     }


    public Boolean save(Res_lang res_lang){
            return false;
     }
    public Boolean saveBatch(List<Res_lang> res_langs){
            return false;
     }

    public Page<Res_lang> searchDefault(Res_langSearchContext context){
            return null;
     }


}
