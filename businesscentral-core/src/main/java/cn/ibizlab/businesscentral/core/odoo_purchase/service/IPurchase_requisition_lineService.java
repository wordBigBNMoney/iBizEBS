package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisition_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Purchase_requisition_line] 服务对象接口
 */
public interface IPurchase_requisition_lineService extends IService<Purchase_requisition_line>{

    boolean create(Purchase_requisition_line et) ;
    void createBatch(List<Purchase_requisition_line> list) ;
    boolean update(Purchase_requisition_line et) ;
    void updateBatch(List<Purchase_requisition_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Purchase_requisition_line get(Long key) ;
    Purchase_requisition_line getDraft(Purchase_requisition_line et) ;
    Purchase_requisition_line calc_price(Purchase_requisition_line et) ;
    boolean checkKey(Purchase_requisition_line et) ;
    Purchase_requisition_line product_change(Purchase_requisition_line et) ;
    boolean save(Purchase_requisition_line et) ;
    void saveBatch(List<Purchase_requisition_line> list) ;
    Page<Purchase_requisition_line> searchDefault(Purchase_requisition_lineSearchContext context) ;
    List<Purchase_requisition_line> selectByAccountAnalyticId(Long id);
    void removeByAccountAnalyticId(Long id);
    List<Purchase_requisition_line> selectByProductId(Long id);
    void removeByProductId(Long id);
    List<Purchase_requisition_line> selectByRequisitionId(Long id);
    void removeByRequisitionId(Collection<Long> ids);
    void removeByRequisitionId(Long id);
    List<Purchase_requisition_line> selectByCompanyId(Long id);
    void removeByCompanyId(Long id);
    List<Purchase_requisition_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Purchase_requisition_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Purchase_requisition_line> selectByMoveDestId(Long id);
    void removeByMoveDestId(Long id);
    List<Purchase_requisition_line> selectByProductUomId(Long id);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Purchase_requisition_line> getPurchaseRequisitionLineByIds(List<Long> ids) ;
    List<Purchase_requisition_line> getPurchaseRequisitionLineByEntities(List<Purchase_requisition_line> entities) ;
}


