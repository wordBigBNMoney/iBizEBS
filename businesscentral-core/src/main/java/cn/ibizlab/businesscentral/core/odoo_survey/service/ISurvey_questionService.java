package cn.ibizlab.businesscentral.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_questionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Survey_question] 服务对象接口
 */
public interface ISurvey_questionService extends IService<Survey_question>{

    boolean create(Survey_question et) ;
    void createBatch(List<Survey_question> list) ;
    boolean update(Survey_question et) ;
    void updateBatch(List<Survey_question> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Survey_question get(Long key) ;
    Survey_question getDraft(Survey_question et) ;
    boolean checkKey(Survey_question et) ;
    boolean save(Survey_question et) ;
    void saveBatch(List<Survey_question> list) ;
    Page<Survey_question> searchDefault(Survey_questionSearchContext context) ;
    List<Survey_question> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Survey_question> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Survey_question> selectByPageId(Long id);
    void removeByPageId(Collection<Long> ids);
    void removeByPageId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Survey_question> getSurveyQuestionByIds(List<Long> ids) ;
    List<Survey_question> getSurveyQuestionByEntities(List<Survey_question> entities) ;
}


