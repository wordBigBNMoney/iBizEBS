package cn.ibizlab.businesscentral.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_mail;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_mailSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Event_mail] 服务对象接口
 */
public interface IEvent_mailService extends IService<Event_mail>{

    boolean create(Event_mail et) ;
    void createBatch(List<Event_mail> list) ;
    boolean update(Event_mail et) ;
    void updateBatch(List<Event_mail> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Event_mail get(Long key) ;
    Event_mail getDraft(Event_mail et) ;
    boolean checkKey(Event_mail et) ;
    boolean save(Event_mail et) ;
    void saveBatch(List<Event_mail> list) ;
    Page<Event_mail> searchDefault(Event_mailSearchContext context) ;
    List<Event_mail> selectByEventId(Long id);
    void removeByEventId(Collection<Long> ids);
    void removeByEventId(Long id);
    List<Event_mail> selectByTemplateId(Long id);
    List<Event_mail> selectByTemplateId(Collection<Long> ids);
    void removeByTemplateId(Long id);
    List<Event_mail> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Event_mail> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Event_mail> getEventMailByIds(List<Long> ids) ;
    List<Event_mail> getEventMailByEntities(List<Event_mail> entities) ;
}


