package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_incotermsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_incoterms] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-incoterms", fallback = account_incotermsFallback.class)
public interface account_incotermsFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/search")
    Page<Account_incoterms> search(@RequestBody Account_incotermsSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_incoterms/{id}")
    Account_incoterms update(@PathVariable("id") Long id,@RequestBody Account_incoterms account_incoterms);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_incoterms/batch")
    Boolean updateBatch(@RequestBody List<Account_incoterms> account_incoterms);


    @RequestMapping(method = RequestMethod.POST, value = "/account_incoterms")
    Account_incoterms create(@RequestBody Account_incoterms account_incoterms);

    @RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/batch")
    Boolean createBatch(@RequestBody List<Account_incoterms> account_incoterms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_incoterms/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_incoterms/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_incoterms/{id}")
    Account_incoterms get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.GET, value = "/account_incoterms/select")
    Page<Account_incoterms> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_incoterms/getdraft")
    Account_incoterms getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/checkkey")
    Boolean checkKey(@RequestBody Account_incoterms account_incoterms);


    @RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/save")
    Boolean save(@RequestBody Account_incoterms account_incoterms);

    @RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/savebatch")
    Boolean saveBatch(@RequestBody List<Account_incoterms> account_incoterms);



    @RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/searchdefault")
    Page<Account_incoterms> searchDefault(@RequestBody Account_incotermsSearchContext context);


}
