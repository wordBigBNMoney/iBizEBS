package cn.ibizlab.businesscentral.core.odoo_repair.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_orderSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Repair_orderMapper extends BaseMapper<Repair_order>{

    Page<Repair_order> searchDefault(IPage page, @Param("srf") Repair_orderSearchContext context, @Param("ew") Wrapper<Repair_order> wrapper) ;
    @Override
    Repair_order selectById(Serializable id);
    @Override
    int insert(Repair_order entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Repair_order entity);
    @Override
    int update(@Param(Constants.ENTITY) Repair_order entity, @Param("ew") Wrapper<Repair_order> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Repair_order> selectByInvoiceId(@Param("id") Serializable id) ;

    List<Repair_order> selectByPricelistId(@Param("id") Serializable id) ;

    List<Repair_order> selectByProductId(@Param("id") Serializable id) ;

    List<Repair_order> selectByCompanyId(@Param("id") Serializable id) ;

    List<Repair_order> selectByAddressId(@Param("id") Serializable id) ;

    List<Repair_order> selectByPartnerId(@Param("id") Serializable id) ;

    List<Repair_order> selectByPartnerInvoiceId(@Param("id") Serializable id) ;

    List<Repair_order> selectByCreateUid(@Param("id") Serializable id) ;

    List<Repair_order> selectByWriteUid(@Param("id") Serializable id) ;

    List<Repair_order> selectByLocationId(@Param("id") Serializable id) ;

    List<Repair_order> selectByMoveId(@Param("id") Serializable id) ;

    List<Repair_order> selectByLotId(@Param("id") Serializable id) ;

    List<Repair_order> selectByProductUom(@Param("id") Serializable id) ;


}
