package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_move_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_move_line] 服务对象接口
 */
@Component
public class account_move_lineFallback implements account_move_lineFeignClient{

    public Page<Account_move_line> search(Account_move_lineSearchContext context){
            return null;
     }


    public Account_move_line update(Long id, Account_move_line account_move_line){
            return null;
     }
    public Boolean updateBatch(List<Account_move_line> account_move_lines){
            return false;
     }



    public Account_move_line create(Account_move_line account_move_line){
            return null;
     }
    public Boolean createBatch(List<Account_move_line> account_move_lines){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_move_line get(Long id){
            return null;
     }




    public Page<Account_move_line> select(){
            return null;
     }

    public Account_move_line getDraft(){
            return null;
    }



    public Boolean checkKey(Account_move_line account_move_line){
            return false;
     }


    public Boolean save(Account_move_line account_move_line){
            return false;
     }
    public Boolean saveBatch(List<Account_move_line> account_move_lines){
            return false;
     }

    public Page<Account_move_line> searchDefault(Account_move_lineSearchContext context){
            return null;
     }


}
