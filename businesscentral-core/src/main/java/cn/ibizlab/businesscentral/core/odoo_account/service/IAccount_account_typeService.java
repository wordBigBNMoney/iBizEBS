package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_account_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_account_type] 服务对象接口
 */
public interface IAccount_account_typeService extends IService<Account_account_type>{

    boolean create(Account_account_type et) ;
    void createBatch(List<Account_account_type> list) ;
    boolean update(Account_account_type et) ;
    void updateBatch(List<Account_account_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_account_type get(Long key) ;
    Account_account_type getDraft(Account_account_type et) ;
    boolean checkKey(Account_account_type et) ;
    boolean save(Account_account_type et) ;
    void saveBatch(List<Account_account_type> list) ;
    Page<Account_account_type> searchDefault(Account_account_typeSearchContext context) ;
    List<Account_account_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_account_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_account_type> getAccountAccountTypeByIds(List<Long> ids) ;
    List<Account_account_type> getAccountAccountTypeByEntities(List<Account_account_type> entities) ;
}


