package cn.ibizlab.businesscentral.core.odoo_mro.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter;
/**
 * 关系型数据实体[Mro_pm_meter] 查询条件对象
 */
@Slf4j
@Data
public class Mro_pm_meterSearchContext extends QueryWrapperContext<Mro_pm_meter> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_reading_type_eq;//[Reading Type]
	public void setN_reading_type_eq(String n_reading_type_eq) {
        this.n_reading_type_eq = n_reading_type_eq;
        if(!ObjectUtils.isEmpty(this.n_reading_type_eq)){
            this.getSearchCond().eq("reading_type", n_reading_type_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_text_eq;//[Meter]
	public void setN_name_text_eq(String n_name_text_eq) {
        this.n_name_text_eq = n_name_text_eq;
        if(!ObjectUtils.isEmpty(this.n_name_text_eq)){
            this.getSearchCond().eq("name_text", n_name_text_eq);
        }
    }
	private String n_name_text_like;//[Meter]
	public void setN_name_text_like(String n_name_text_like) {
        this.n_name_text_like = n_name_text_like;
        if(!ObjectUtils.isEmpty(this.n_name_text_like)){
            this.getSearchCond().like("name_text", n_name_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_asset_id_text_eq;//[Asset]
	public void setN_asset_id_text_eq(String n_asset_id_text_eq) {
        this.n_asset_id_text_eq = n_asset_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_asset_id_text_eq)){
            this.getSearchCond().eq("asset_id_text", n_asset_id_text_eq);
        }
    }
	private String n_asset_id_text_like;//[Asset]
	public void setN_asset_id_text_like(String n_asset_id_text_like) {
        this.n_asset_id_text_like = n_asset_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_asset_id_text_like)){
            this.getSearchCond().like("asset_id_text", n_asset_id_text_like);
        }
    }
	private String n_parent_ratio_id_text_eq;//[Ratio to Source]
	public void setN_parent_ratio_id_text_eq(String n_parent_ratio_id_text_eq) {
        this.n_parent_ratio_id_text_eq = n_parent_ratio_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_ratio_id_text_eq)){
            this.getSearchCond().eq("parent_ratio_id_text", n_parent_ratio_id_text_eq);
        }
    }
	private String n_parent_ratio_id_text_like;//[Ratio to Source]
	public void setN_parent_ratio_id_text_like(String n_parent_ratio_id_text_like) {
        this.n_parent_ratio_id_text_like = n_parent_ratio_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_parent_ratio_id_text_like)){
            this.getSearchCond().like("parent_ratio_id_text", n_parent_ratio_id_text_like);
        }
    }
	private Long n_parent_meter_id_eq;//[Source Meter]
	public void setN_parent_meter_id_eq(Long n_parent_meter_id_eq) {
        this.n_parent_meter_id_eq = n_parent_meter_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_meter_id_eq)){
            this.getSearchCond().eq("parent_meter_id", n_parent_meter_id_eq);
        }
    }
	private Long n_parent_ratio_id_eq;//[Ratio to Source]
	public void setN_parent_ratio_id_eq(Long n_parent_ratio_id_eq) {
        this.n_parent_ratio_id_eq = n_parent_ratio_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_ratio_id_eq)){
            this.getSearchCond().eq("parent_ratio_id", n_parent_ratio_id_eq);
        }
    }
	private Long n_name_eq;//[Meter]
	public void setN_name_eq(Long n_name_eq) {
        this.n_name_eq = n_name_eq;
        if(!ObjectUtils.isEmpty(this.n_name_eq)){
            this.getSearchCond().eq("name", n_name_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_asset_id_eq;//[Asset]
	public void setN_asset_id_eq(Long n_asset_id_eq) {
        this.n_asset_id_eq = n_asset_id_eq;
        if(!ObjectUtils.isEmpty(this.n_asset_id_eq)){
            this.getSearchCond().eq("asset_id", n_asset_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



