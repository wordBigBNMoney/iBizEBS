package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_contractSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contractService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_contractMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[合同] 服务对象接口实现
 */
@Slf4j
@Service("Hr_contractServiceImpl")
public class Hr_contractServiceImpl extends EBSServiceImpl<Hr_contractMapper, Hr_contract> implements IHr_contractService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendarService resourceCalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.contract" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_contract et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_contractService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_contract> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_contract et) {
        Hr_contract old = new Hr_contract() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_contractService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_contractService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_contract> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_contract get(Long key) {
        Hr_contract et = getById(key);
        if(et==null){
            et=new Hr_contract();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_contract getDraft(Hr_contract et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_contract et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_contract et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_contract et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_contract> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_contract> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_contract> selectByHrResponsibleId(Long id) {
        return baseMapper.selectByHrResponsibleId(id);
    }
    @Override
    public void removeByHrResponsibleId(Long id) {
        this.remove(new QueryWrapper<Hr_contract>().eq("hr_responsible_id",id));
    }

	@Override
    public List<Hr_contract> selectByDepartmentId(Long id) {
        return baseMapper.selectByDepartmentId(id);
    }
    @Override
    public void resetByDepartmentId(Long id) {
        this.update(new UpdateWrapper<Hr_contract>().set("department_id",null).eq("department_id",id));
    }

    @Override
    public void resetByDepartmentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_contract>().set("department_id",null).in("department_id",ids));
    }

    @Override
    public void removeByDepartmentId(Long id) {
        this.remove(new QueryWrapper<Hr_contract>().eq("department_id",id));
    }

	@Override
    public List<Hr_contract> selectByEmployeeId(Long id) {
        return baseMapper.selectByEmployeeId(id);
    }
    @Override
    public void resetByEmployeeId(Long id) {
        this.update(new UpdateWrapper<Hr_contract>().set("employee_id",null).eq("employee_id",id));
    }

    @Override
    public void resetByEmployeeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_contract>().set("employee_id",null).in("employee_id",ids));
    }

    @Override
    public void removeByEmployeeId(Long id) {
        this.remove(new QueryWrapper<Hr_contract>().eq("employee_id",id));
    }

	@Override
    public List<Hr_contract> selectByJobId(Long id) {
        return baseMapper.selectByJobId(id);
    }
    @Override
    public void resetByJobId(Long id) {
        this.update(new UpdateWrapper<Hr_contract>().set("job_id",null).eq("job_id",id));
    }

    @Override
    public void resetByJobId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_contract>().set("job_id",null).in("job_id",ids));
    }

    @Override
    public void removeByJobId(Long id) {
        this.remove(new QueryWrapper<Hr_contract>().eq("job_id",id));
    }

	@Override
    public List<Hr_contract> selectByResourceCalendarId(Long id) {
        return baseMapper.selectByResourceCalendarId(id);
    }
    @Override
    public void resetByResourceCalendarId(Long id) {
        this.update(new UpdateWrapper<Hr_contract>().set("resource_calendar_id",null).eq("resource_calendar_id",id));
    }

    @Override
    public void resetByResourceCalendarId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_contract>().set("resource_calendar_id",null).in("resource_calendar_id",ids));
    }

    @Override
    public void removeByResourceCalendarId(Long id) {
        this.remove(new QueryWrapper<Hr_contract>().eq("resource_calendar_id",id));
    }

	@Override
    public List<Hr_contract> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Hr_contract>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_contract>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Hr_contract>().eq("company_id",id));
    }

	@Override
    public List<Hr_contract> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_contract>().eq("create_uid",id));
    }

	@Override
    public List<Hr_contract> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_contract>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_contract> searchDefault(Hr_contractSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_contract> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_contract>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_contract et){
        //实体关系[DER1N_HR_CONTRACT_RES_USERS_HR_RESPONSIBLE_ID]
        if(!ObjectUtils.isEmpty(et.getHrResponsibleId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooHrResponsible=et.getOdooHrResponsible();
            if(ObjectUtils.isEmpty(odooHrResponsible)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getHrResponsibleId());
                et.setOdooHrResponsible(majorEntity);
                odooHrResponsible=majorEntity;
            }
            et.setHrResponsibleName(odooHrResponsible.getName());
        }
        //实体关系[DER1N_HR_CONTRACT__HR_DEPARTMENT__DEPARTMENT_ID]
        if(!ObjectUtils.isEmpty(et.getDepartmentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment=et.getOdooDepartment();
            if(ObjectUtils.isEmpty(odooDepartment)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department majorEntity=hrDepartmentService.get(et.getDepartmentId());
                et.setOdooDepartment(majorEntity);
                odooDepartment=majorEntity;
            }
            et.setDepartmentIdText(odooDepartment.getName());
        }
        //实体关系[DER1N_HR_CONTRACT__HR_EMPLOYEE__EMPLOYEE_ID]
        if(!ObjectUtils.isEmpty(et.getEmployeeId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee=et.getOdooEmployee();
            if(ObjectUtils.isEmpty(odooEmployee)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getEmployeeId());
                et.setOdooEmployee(majorEntity);
                odooEmployee=majorEntity;
            }
            et.setPermitNo(odooEmployee.getPermitNo());
            et.setEmployeeIdText(odooEmployee.getName());
            et.setVisaExpire(odooEmployee.getVisaExpire());
            et.setVisaNo(odooEmployee.getVisaNo());
        }
        //实体关系[DER1N_HR_CONTRACT__HR_JOB__JOB_ID]
        if(!ObjectUtils.isEmpty(et.getJobId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job odooJob=et.getOdooJob();
            if(ObjectUtils.isEmpty(odooJob)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job majorEntity=hrJobService.get(et.getJobId());
                et.setOdooJob(majorEntity);
                odooJob=majorEntity;
            }
            et.setJobIdText(odooJob.getName());
        }
        //实体关系[DER1N_HR_CONTRACT__RESOURCE_CALENDAR__RESOURCE_CALENDAR_ID]
        if(!ObjectUtils.isEmpty(et.getResourceCalendarId())){
            cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooResourceCalendar=et.getOdooResourceCalendar();
            if(ObjectUtils.isEmpty(odooResourceCalendar)){
                cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar majorEntity=resourceCalendarService.get(et.getResourceCalendarId());
                et.setOdooResourceCalendar(majorEntity);
                odooResourceCalendar=majorEntity;
            }
            et.setResourceCalendarIdText(odooResourceCalendar.getName());
        }
        //实体关系[DER1N_HR_CONTRACT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
            et.setCurrencyId(odooCompany.getCurrencyId());
        }
        //实体关系[DER1N_HR_CONTRACT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_CONTRACT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_contract> getHrContractByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_contract> getHrContractByEntities(List<Hr_contract> entities) {
        List ids =new ArrayList();
        for(Hr_contract entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



