package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_price_listSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_price_list] 服务对象接口
 */
public interface IProduct_price_listService extends IService<Product_price_list>{

    boolean create(Product_price_list et) ;
    void createBatch(List<Product_price_list> list) ;
    boolean update(Product_price_list et) ;
    void updateBatch(List<Product_price_list> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_price_list get(Long key) ;
    Product_price_list getDraft(Product_price_list et) ;
    boolean checkKey(Product_price_list et) ;
    boolean save(Product_price_list et) ;
    void saveBatch(List<Product_price_list> list) ;
    Page<Product_price_list> searchDefault(Product_price_listSearchContext context) ;
    List<Product_price_list> selectByPriceList(Long id);
    void resetByPriceList(Long id);
    void resetByPriceList(Collection<Long> ids);
    void removeByPriceList(Long id);
    List<Product_price_list> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_price_list> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_price_list> getProductPriceListByIds(List<Long> ids) ;
    List<Product_price_list> getProductPriceListByEntities(List<Product_price_list> entities) ;
}


