package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_backorder_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_backorder_confirmation] 服务对象接口
 */
public interface IStock_backorder_confirmationService extends IService<Stock_backorder_confirmation>{

    boolean create(Stock_backorder_confirmation et) ;
    void createBatch(List<Stock_backorder_confirmation> list) ;
    boolean update(Stock_backorder_confirmation et) ;
    void updateBatch(List<Stock_backorder_confirmation> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_backorder_confirmation get(Long key) ;
    Stock_backorder_confirmation getDraft(Stock_backorder_confirmation et) ;
    boolean checkKey(Stock_backorder_confirmation et) ;
    boolean save(Stock_backorder_confirmation et) ;
    void saveBatch(List<Stock_backorder_confirmation> list) ;
    Page<Stock_backorder_confirmation> searchDefault(Stock_backorder_confirmationSearchContext context) ;
    List<Stock_backorder_confirmation> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_backorder_confirmation> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_backorder_confirmation> getStockBackorderConfirmationByIds(List<Long> ids) ;
    List<Stock_backorder_confirmation> getStockBackorderConfirmationByEntities(List<Stock_backorder_confirmation> entities) ;
}


