package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_report;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leave_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_leave_report] 服务对象接口
 */
@Component
public class hr_leave_reportFallback implements hr_leave_reportFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Hr_leave_report update(Long id, Hr_leave_report hr_leave_report){
            return null;
     }
    public Boolean updateBatch(List<Hr_leave_report> hr_leave_reports){
            return false;
     }



    public Page<Hr_leave_report> search(Hr_leave_reportSearchContext context){
            return null;
     }



    public Hr_leave_report create(Hr_leave_report hr_leave_report){
            return null;
     }
    public Boolean createBatch(List<Hr_leave_report> hr_leave_reports){
            return false;
     }

    public Hr_leave_report get(Long id){
            return null;
     }



    public Page<Hr_leave_report> select(){
            return null;
     }

    public Hr_leave_report getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_leave_report hr_leave_report){
            return false;
     }


    public Boolean save(Hr_leave_report hr_leave_report){
            return false;
     }
    public Boolean saveBatch(List<Hr_leave_report> hr_leave_reports){
            return false;
     }

    public Page<Hr_leave_report> searchDefault(Hr_leave_reportSearchContext context){
            return null;
     }


}
