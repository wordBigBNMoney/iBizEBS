package cn.ibizlab.businesscentral.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_contactsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Calendar_contacts] 服务对象接口
 */
public interface ICalendar_contactsService extends IService<Calendar_contacts>{

    boolean create(Calendar_contacts et) ;
    void createBatch(List<Calendar_contacts> list) ;
    boolean update(Calendar_contacts et) ;
    void updateBatch(List<Calendar_contacts> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Calendar_contacts get(Long key) ;
    Calendar_contacts getDraft(Calendar_contacts et) ;
    boolean checkKey(Calendar_contacts et) ;
    boolean save(Calendar_contacts et) ;
    void saveBatch(List<Calendar_contacts> list) ;
    Page<Calendar_contacts> searchDefault(Calendar_contactsSearchContext context) ;
    List<Calendar_contacts> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Calendar_contacts> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Calendar_contacts> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Calendar_contacts> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Calendar_contacts> getCalendarContactsByIds(List<Long> ids) ;
    List<Calendar_contacts> getCalendarContactsByEntities(List<Calendar_contacts> entities) ;
}


