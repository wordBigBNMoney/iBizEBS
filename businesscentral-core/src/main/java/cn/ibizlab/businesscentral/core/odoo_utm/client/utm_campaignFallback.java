package cn.ibizlab.businesscentral.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_campaignSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[utm_campaign] 服务对象接口
 */
@Component
public class utm_campaignFallback implements utm_campaignFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Utm_campaign get(Long id){
            return null;
     }


    public Page<Utm_campaign> search(Utm_campaignSearchContext context){
            return null;
     }


    public Utm_campaign update(Long id, Utm_campaign utm_campaign){
            return null;
     }
    public Boolean updateBatch(List<Utm_campaign> utm_campaigns){
            return false;
     }



    public Utm_campaign create(Utm_campaign utm_campaign){
            return null;
     }
    public Boolean createBatch(List<Utm_campaign> utm_campaigns){
            return false;
     }



    public Page<Utm_campaign> select(){
            return null;
     }

    public Utm_campaign getDraft(){
            return null;
    }



    public Boolean checkKey(Utm_campaign utm_campaign){
            return false;
     }


    public Boolean save(Utm_campaign utm_campaign){
            return false;
     }
    public Boolean saveBatch(List<Utm_campaign> utm_campaigns){
            return false;
     }

    public Page<Utm_campaign> searchDefault(Utm_campaignSearchContext context){
            return null;
     }


}
