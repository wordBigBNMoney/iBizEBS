package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_move_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_move_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[日记账项目] 服务对象接口实现
 */
@Slf4j
@Service("Account_move_lineServiceImpl")
public class Account_move_lineServiceImpl extends EBSServiceImpl<Account_move_lineMapper, Account_move_line> implements IAccount_move_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_lineService accountAnalyticLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_partial_reconcileService accountPartialReconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_typeService accountAccountTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_lineService accountBankStatementLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statementService accountBankStatementService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_full_reconcileService accountFullReconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_paymentService accountPaymentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_taxService accountTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expenseService hrExpenseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.move.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_move_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_move_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_move_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_move_line et) {
        Account_move_line old = new Account_move_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_move_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_move_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_move_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountAnalyticLineService.removeByMoveId(key);
        accountPartialReconcileService.resetByCreditMoveId(key);
        accountPartialReconcileService.resetByDebitMoveId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountAnalyticLineService.removeByMoveId(idList);
        accountPartialReconcileService.resetByCreditMoveId(idList);
        accountPartialReconcileService.resetByDebitMoveId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_move_line get(Long key) {
        Account_move_line et = getById(key);
        if(et==null){
            et=new Account_move_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_move_line getDraft(Account_move_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_move_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_move_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_move_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_move_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_move_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_move_line> selectByUserTypeId(Long id) {
        return baseMapper.selectByUserTypeId(id);
    }
    @Override
    public void resetByUserTypeId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("user_type_id",null).eq("user_type_id",id));
    }

    @Override
    public void resetByUserTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("user_type_id",null).in("user_type_id",ids));
    }

    @Override
    public void removeByUserTypeId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("user_type_id",id));
    }

	@Override
    public List<Account_move_line> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public void removeByAccountId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_move_line>().in("account_id",ids));
    }

    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("account_id",id));
    }

	@Override
    public List<Account_move_line> selectByAnalyticAccountId(Long id) {
        return baseMapper.selectByAnalyticAccountId(id);
    }
    @Override
    public void resetByAnalyticAccountId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("analytic_account_id",null).eq("analytic_account_id",id));
    }

    @Override
    public void resetByAnalyticAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("analytic_account_id",null).in("analytic_account_id",ids));
    }

    @Override
    public void removeByAnalyticAccountId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("analytic_account_id",id));
    }

	@Override
    public List<Account_move_line> selectByStatementLineId(Long id) {
        return baseMapper.selectByStatementLineId(id);
    }
    @Override
    public void resetByStatementLineId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("statement_line_id",null).eq("statement_line_id",id));
    }

    @Override
    public void resetByStatementLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("statement_line_id",null).in("statement_line_id",ids));
    }

    @Override
    public void removeByStatementLineId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("statement_line_id",id));
    }

	@Override
    public List<Account_move_line> selectByStatementId(Long id) {
        return baseMapper.selectByStatementId(id);
    }
    @Override
    public void resetByStatementId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("statement_id",null).eq("statement_id",id));
    }

    @Override
    public void resetByStatementId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("statement_id",null).in("statement_id",ids));
    }

    @Override
    public void removeByStatementId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("statement_id",id));
    }

	@Override
    public List<Account_move_line> selectByFullReconcileId(Long id) {
        return baseMapper.selectByFullReconcileId(id);
    }
    @Override
    public void resetByFullReconcileId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("full_reconcile_id",null).eq("full_reconcile_id",id));
    }

    @Override
    public void resetByFullReconcileId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("full_reconcile_id",null).in("full_reconcile_id",ids));
    }

    @Override
    public void removeByFullReconcileId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("full_reconcile_id",id));
    }

	@Override
    public List<Account_move_line> selectByInvoiceId(Long id) {
        return baseMapper.selectByInvoiceId(id);
    }
    @Override
    public void resetByInvoiceId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("invoice_id",null).eq("invoice_id",id));
    }

    @Override
    public void resetByInvoiceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("invoice_id",null).in("invoice_id",ids));
    }

    @Override
    public void removeByInvoiceId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("invoice_id",id));
    }

	@Override
    public List<Account_move_line> selectByJournalId(Long id) {
        return baseMapper.selectByJournalId(id);
    }
    @Override
    public void resetByJournalId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("journal_id",null).eq("journal_id",id));
    }

    @Override
    public void resetByJournalId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("journal_id",null).in("journal_id",ids));
    }

    @Override
    public void removeByJournalId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("journal_id",id));
    }

	@Override
    public List<Account_move_line> selectByMoveId(Long id) {
        return baseMapper.selectByMoveId(id);
    }
    @Override
    public void removeByMoveId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_move_line>().in("move_id",ids));
    }

    @Override
    public void removeByMoveId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("move_id",id));
    }

	@Override
    public List<Account_move_line> selectByPaymentId(Long id) {
        return baseMapper.selectByPaymentId(id);
    }
    @Override
    public void resetByPaymentId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("payment_id",null).eq("payment_id",id));
    }

    @Override
    public void resetByPaymentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("payment_id",null).in("payment_id",ids));
    }

    @Override
    public void removeByPaymentId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("payment_id",id));
    }

	@Override
    public List<Account_move_line> selectByTaxLineId(Long id) {
        return baseMapper.selectByTaxLineId(id);
    }
    @Override
    public List<Account_move_line> selectByTaxLineId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_move_line>().in("id",ids));
    }

    @Override
    public void removeByTaxLineId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("tax_line_id",id));
    }

	@Override
    public List<Account_move_line> selectByExpenseId(Long id) {
        return baseMapper.selectByExpenseId(id);
    }
    @Override
    public void resetByExpenseId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("expense_id",null).eq("expense_id",id));
    }

    @Override
    public void resetByExpenseId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("expense_id",null).in("expense_id",ids));
    }

    @Override
    public void removeByExpenseId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("expense_id",id));
    }

	@Override
    public List<Account_move_line> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("product_id",id));
    }

	@Override
    public List<Account_move_line> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("company_id",id));
    }

	@Override
    public List<Account_move_line> selectByCompanyCurrencyId(Long id) {
        return baseMapper.selectByCompanyCurrencyId(id);
    }
    @Override
    public void resetByCompanyCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("company_currency_id",null).eq("company_currency_id",id));
    }

    @Override
    public void resetByCompanyCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("company_currency_id",null).in("company_currency_id",ids));
    }

    @Override
    public void removeByCompanyCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("company_currency_id",id));
    }

	@Override
    public List<Account_move_line> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("currency_id",id));
    }

	@Override
    public List<Account_move_line> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public List<Account_move_line> selectByPartnerId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_move_line>().in("id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("partner_id",id));
    }

	@Override
    public List<Account_move_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("create_uid",id));
    }

	@Override
    public List<Account_move_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("write_uid",id));
    }

	@Override
    public List<Account_move_line> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void resetByProductUomId(Long id) {
        this.update(new UpdateWrapper<Account_move_line>().set("product_uom_id",null).eq("product_uom_id",id));
    }

    @Override
    public void resetByProductUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_move_line>().set("product_uom_id",null).in("product_uom_id",ids));
    }

    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Account_move_line>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_move_line> searchDefault(Account_move_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_move_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_move_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_move_line et){
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_ACCOUNT_TYPE__USER_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getUserTypeId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_type odooUserType=et.getOdooUserType();
            if(ObjectUtils.isEmpty(odooUserType)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_type majorEntity=accountAccountTypeService.get(et.getUserTypeId());
                et.setOdooUserType(majorEntity);
                odooUserType=majorEntity;
            }
            et.setUserTypeIdText(odooUserType.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_ACCOUNT__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_ANALYTIC_ACCOUNT__ANALYTIC_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAnalyticAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount=et.getOdooAnalyticAccount();
            if(ObjectUtils.isEmpty(odooAnalyticAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAnalyticAccountId());
                et.setOdooAnalyticAccount(majorEntity);
                odooAnalyticAccount=majorEntity;
            }
            et.setAnalyticAccountIdText(odooAnalyticAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_BANK_STATEMENT_LINE__STATEMENT_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getStatementLineId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_line odooStatementLine=et.getOdooStatementLine();
            if(ObjectUtils.isEmpty(odooStatementLine)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_line majorEntity=accountBankStatementLineService.get(et.getStatementLineId());
                et.setOdooStatementLine(majorEntity);
                odooStatementLine=majorEntity;
            }
            et.setStatementLineIdText(odooStatementLine.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_BANK_STATEMENT__STATEMENT_ID]
        if(!ObjectUtils.isEmpty(et.getStatementId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement odooStatement=et.getOdooStatement();
            if(ObjectUtils.isEmpty(odooStatement)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement majorEntity=accountBankStatementService.get(et.getStatementId());
                et.setOdooStatement(majorEntity);
                odooStatement=majorEntity;
            }
            et.setStatementIdText(odooStatement.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_FULL_RECONCILE__FULL_RECONCILE_ID]
        if(!ObjectUtils.isEmpty(et.getFullReconcileId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_full_reconcile odooFullReconcile=et.getOdooFullReconcile();
            if(ObjectUtils.isEmpty(odooFullReconcile)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_full_reconcile majorEntity=accountFullReconcileService.get(et.getFullReconcileId());
                et.setOdooFullReconcile(majorEntity);
                odooFullReconcile=majorEntity;
            }
            et.setFullReconcileIdText(odooFullReconcile.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_INVOICE__INVOICE_ID]
        if(!ObjectUtils.isEmpty(et.getInvoiceId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooInvoice=et.getOdooInvoice();
            if(ObjectUtils.isEmpty(odooInvoice)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice majorEntity=accountInvoiceService.get(et.getInvoiceId());
                et.setOdooInvoice(majorEntity);
                odooInvoice=majorEntity;
            }
            et.setInvoiceIdText(odooInvoice.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_JOURNAL__JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal=et.getOdooJournal();
            if(ObjectUtils.isEmpty(odooJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getJournalId());
                et.setOdooJournal(majorEntity);
                odooJournal=majorEntity;
            }
            et.setJournalIdText(odooJournal.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_MOVE__MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getMoveId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move odooMove=et.getOdooMove();
            if(ObjectUtils.isEmpty(odooMove)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move majorEntity=accountMoveService.get(et.getMoveId());
                et.setOdooMove(majorEntity);
                odooMove=majorEntity;
            }
            et.setNarration(odooMove.getNarration());
            et.setMoveIdText(odooMove.getName());
            et.setRef(odooMove.getRef());
            et.setDate(odooMove.getDate());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_PAYMENT__PAYMENT_ID]
        if(!ObjectUtils.isEmpty(et.getPaymentId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment odooPayment=et.getOdooPayment();
            if(ObjectUtils.isEmpty(odooPayment)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment majorEntity=accountPaymentService.get(et.getPaymentId());
                et.setOdooPayment(majorEntity);
                odooPayment=majorEntity;
            }
            et.setPaymentIdText(odooPayment.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__ACCOUNT_TAX__TAX_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getTaxLineId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooTaxLine=et.getOdooTaxLine();
            if(ObjectUtils.isEmpty(odooTaxLine)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax majorEntity=accountTaxService.get(et.getTaxLineId());
                et.setOdooTaxLine(majorEntity);
                odooTaxLine=majorEntity;
            }
            et.setTaxLineIdText(odooTaxLine.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__HR_EXPENSE__EXPENSE_ID]
        if(!ObjectUtils.isEmpty(et.getExpenseId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense odooExpense=et.getOdooExpense();
            if(ObjectUtils.isEmpty(odooExpense)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense majorEntity=hrExpenseService.get(et.getExpenseId());
                et.setOdooExpense(majorEntity);
                odooExpense=majorEntity;
            }
            et.setExpenseIdText(odooExpense.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__RES_CURRENCY__COMPANY_CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCompanyCurrency=et.getOdooCompanyCurrency();
            if(ObjectUtils.isEmpty(odooCompanyCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCompanyCurrencyId());
                et.setOdooCompanyCurrency(majorEntity);
                odooCompanyCurrency=majorEntity;
            }
            et.setCompanyCurrencyIdText(odooCompanyCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_ACCOUNT_MOVE_LINE__UOM_UOM__PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomIdText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_move_line> getAccountMoveLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_move_line> getAccountMoveLineByEntities(List<Account_move_line> entities) {
        List ids =new ArrayList();
        for(Account_move_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



