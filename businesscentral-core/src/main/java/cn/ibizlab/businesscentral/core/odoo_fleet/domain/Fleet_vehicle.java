package cn.ibizlab.businesscentral.core.odoo_fleet.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[车辆]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "FLEET_VEHICLE",resultMap = "Fleet_vehicleResultMap")
public class Fleet_vehicle extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 燃油类型
     */
    @DEField(name = "fuel_type")
    @TableField(value = "fuel_type")
    @JSONField(name = "fuel_type")
    @JsonProperty("fuel_type")
    private String fuelType;
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 动力
     */
    @TableField(value = "power")
    @JSONField(name = "power")
    @JsonProperty("power")
    private Integer power;
    /**
     * 需要激活
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 残余价值
     */
    @DEField(name = "residual_value")
    @TableField(value = "residual_value")
    @JSONField(name = "residual_value")
    @JsonProperty("residual_value")
    private Double residualValue;
    /**
     * 最新里程表
     */
    @TableField(exist = false)
    @JSONField(name = "odometer")
    @JsonProperty("odometer")
    private Double odometer;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 加油记录统计
     */
    @TableField(exist = false)
    @JSONField(name = "fuel_logs_count")
    @JsonProperty("fuel_logs_count")
    private Integer fuelLogsCount;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 型号年份
     */
    @DEField(name = "model_year")
    @TableField(value = "model_year")
    @JSONField(name = "model_year")
    @JsonProperty("model_year")
    private String modelYear;
    /**
     * 目录值（包括增值税）
     */
    @DEField(name = "car_value")
    @TableField(value = "car_value")
    @JSONField(name = "car_value")
    @JsonProperty("car_value")
    private Double carValue;
    /**
     * 车辆牌照
     */
    @DEField(name = "license_plate")
    @TableField(value = "license_plate")
    @JSONField(name = "license_plate")
    @JsonProperty("license_plate")
    private String licensePlate;
    /**
     * 有逾期合同
     */
    @TableField(exist = false)
    @JSONField(name = "contract_renewal_overdue")
    @JsonProperty("contract_renewal_overdue")
    private Boolean contractRenewalOverdue;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 注册日期
     */
    @DEField(name = "acquisition_date")
    @TableField(value = "acquisition_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "acquisition_date" , format="yyyy-MM-dd")
    @JsonProperty("acquisition_date")
    private Timestamp acquisitionDate;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 车船税
     */
    @DEField(name = "horsepower_tax")
    @TableField(value = "horsepower_tax")
    @JSONField(name = "horsepower_tax")
    @JsonProperty("horsepower_tax")
    private Double horsepowerTax;
    /**
     * 地点
     */
    @TableField(value = "location")
    @JSONField(name = "location")
    @JsonProperty("location")
    private String location;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 马力
     */
    @TableField(value = "horsepower")
    @JSONField(name = "horsepower")
    @JsonProperty("horsepower")
    private Integer horsepower;
    /**
     * 车架号
     */
    @DEField(name = "vin_sn")
    @TableField(value = "vin_sn")
    @JSONField(name = "vin_sn")
    @JsonProperty("vin_sn")
    private String vinSn;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 里程表
     */
    @TableField(exist = false)
    @JSONField(name = "odometer_count")
    @JsonProperty("odometer_count")
    private Integer odometerCount;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 需马上续签合同的名称
     */
    @TableField(exist = false)
    @JSONField(name = "contract_renewal_name")
    @JsonProperty("contract_renewal_name")
    private String contractRenewalName;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 座位数
     */
    @TableField(value = "seats")
    @JSONField(name = "seats")
    @JsonProperty("seats")
    private Integer seats;
    /**
     * 燃油记录
     */
    @TableField(exist = false)
    @JSONField(name = "log_fuel")
    @JsonProperty("log_fuel")
    private String logFuel;
    /**
     * 费用
     */
    @TableField(exist = false)
    @JSONField(name = "cost_count")
    @JsonProperty("cost_count")
    private Integer costCount;
    /**
     * 指派记录
     */
    @TableField(exist = false)
    @JSONField(name = "log_drivers")
    @JsonProperty("log_drivers")
    private String logDrivers;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 有合同待续签
     */
    @TableField(exist = false)
    @JSONField(name = "contract_renewal_due_soon")
    @JsonProperty("contract_renewal_due_soon")
    private Boolean contractRenewalDueSoon;
    /**
     * 里程表单位
     */
    @DEField(name = "odometer_unit")
    @TableField(value = "odometer_unit")
    @JSONField(name = "odometer_unit")
    @JsonProperty("odometer_unit")
    private String odometerUnit;
    /**
     * 二氧化碳排放量
     */
    @TableField(value = "co2")
    @JSONField(name = "co2")
    @JsonProperty("co2")
    private Double co2;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 合同
     */
    @TableField(exist = false)
    @JSONField(name = "log_contracts")
    @JsonProperty("log_contracts")
    private String logContracts;
    /**
     * 首次合同日期
     */
    @DEField(name = "first_contract_date")
    @TableField(value = "first_contract_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "first_contract_date" , format="yyyy-MM-dd")
    @JsonProperty("first_contract_date")
    private Timestamp firstContractDate;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 颜色
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private String color;
    /**
     * 车门数量
     */
    @TableField(value = "doors")
    @JSONField(name = "doors")
    @JsonProperty("doors")
    private Integer doors;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 服务记录
     */
    @TableField(exist = false)
    @JSONField(name = "log_services")
    @JsonProperty("log_services")
    private String logServices;
    /**
     * 合同统计
     */
    @TableField(exist = false)
    @JSONField(name = "contract_count")
    @JsonProperty("contract_count")
    private Integer contractCount;
    /**
     * 变速器
     */
    @TableField(value = "transmission")
    @JSONField(name = "transmission")
    @JsonProperty("transmission")
    private String transmission;
    /**
     * 服务
     */
    @TableField(exist = false)
    @JSONField(name = "service_count")
    @JsonProperty("service_count")
    private Integer serviceCount;
    /**
     * 截止或者逾期减一的合同总计
     */
    @TableField(exist = false)
    @JSONField(name = "contract_renewal_total")
    @JsonProperty("contract_renewal_total")
    private String contractRenewalTotal;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 驾驶员
     */
    @TableField(exist = false)
    @JSONField(name = "driver_id_text")
    @JsonProperty("driver_id_text")
    private String driverIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 车辆标志图片(普通大小)
     */
    @TableField(exist = false)
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;
    /**
     * 徽标
     */
    @TableField(exist = false)
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;
    /**
     * 型号
     */
    @TableField(exist = false)
    @JSONField(name = "model_id_text")
    @JsonProperty("model_id_text")
    private String modelIdText;
    /**
     * 车辆标志图片(小图片)
     */
    @TableField(exist = false)
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;
    /**
     * 品牌
     */
    @TableField(exist = false)
    @JSONField(name = "brand_id_text")
    @JsonProperty("brand_id_text")
    private String brandIdText;
    /**
     * 状态
     */
    @TableField(exist = false)
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    private String stateIdText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 型号
     */
    @DEField(name = "model_id")
    @TableField(value = "model_id")
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Long modelId;
    /**
     * 品牌
     */
    @DEField(name = "brand_id")
    @TableField(value = "brand_id")
    @JSONField(name = "brand_id")
    @JsonProperty("brand_id")
    private Long brandId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 状态
     */
    @DEField(name = "state_id")
    @TableField(value = "state_id")
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Long stateId;
    /**
     * 驾驶员
     */
    @DEField(name = "driver_id")
    @TableField(value = "driver_id")
    @JSONField(name = "driver_id")
    @JsonProperty("driver_id")
    private Long driverId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model_brand odooBrand;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model odooModel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_state odooState;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooDriver;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [燃油类型]
     */
    public void setFuelType(String fuelType){
        this.fuelType = fuelType ;
        this.modify("fuel_type",fuelType);
    }

    /**
     * 设置 [动力]
     */
    public void setPower(Integer power){
        this.power = power ;
        this.modify("power",power);
    }

    /**
     * 设置 [残余价值]
     */
    public void setResidualValue(Double residualValue){
        this.residualValue = residualValue ;
        this.modify("residual_value",residualValue);
    }

    /**
     * 设置 [型号年份]
     */
    public void setModelYear(String modelYear){
        this.modelYear = modelYear ;
        this.modify("model_year",modelYear);
    }

    /**
     * 设置 [目录值（包括增值税）]
     */
    public void setCarValue(Double carValue){
        this.carValue = carValue ;
        this.modify("car_value",carValue);
    }

    /**
     * 设置 [车辆牌照]
     */
    public void setLicensePlate(String licensePlate){
        this.licensePlate = licensePlate ;
        this.modify("license_plate",licensePlate);
    }

    /**
     * 设置 [注册日期]
     */
    public void setAcquisitionDate(Timestamp acquisitionDate){
        this.acquisitionDate = acquisitionDate ;
        this.modify("acquisition_date",acquisitionDate);
    }

    /**
     * 格式化日期 [注册日期]
     */
    public String formatAcquisitionDate(){
        if (this.acquisitionDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(acquisitionDate);
    }
    /**
     * 设置 [车船税]
     */
    public void setHorsepowerTax(Double horsepowerTax){
        this.horsepowerTax = horsepowerTax ;
        this.modify("horsepower_tax",horsepowerTax);
    }

    /**
     * 设置 [地点]
     */
    public void setLocation(String location){
        this.location = location ;
        this.modify("location",location);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [马力]
     */
    public void setHorsepower(Integer horsepower){
        this.horsepower = horsepower ;
        this.modify("horsepower",horsepower);
    }

    /**
     * 设置 [车架号]
     */
    public void setVinSn(String vinSn){
        this.vinSn = vinSn ;
        this.modify("vin_sn",vinSn);
    }

    /**
     * 设置 [座位数]
     */
    public void setSeats(Integer seats){
        this.seats = seats ;
        this.modify("seats",seats);
    }

    /**
     * 设置 [里程表单位]
     */
    public void setOdometerUnit(String odometerUnit){
        this.odometerUnit = odometerUnit ;
        this.modify("odometer_unit",odometerUnit);
    }

    /**
     * 设置 [二氧化碳排放量]
     */
    public void setCo2(Double co2){
        this.co2 = co2 ;
        this.modify("co2",co2);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [首次合同日期]
     */
    public void setFirstContractDate(Timestamp firstContractDate){
        this.firstContractDate = firstContractDate ;
        this.modify("first_contract_date",firstContractDate);
    }

    /**
     * 格式化日期 [首次合同日期]
     */
    public String formatFirstContractDate(){
        if (this.firstContractDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(firstContractDate);
    }
    /**
     * 设置 [颜色]
     */
    public void setColor(String color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [车门数量]
     */
    public void setDoors(Integer doors){
        this.doors = doors ;
        this.modify("doors",doors);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [变速器]
     */
    public void setTransmission(String transmission){
        this.transmission = transmission ;
        this.modify("transmission",transmission);
    }

    /**
     * 设置 [型号]
     */
    public void setModelId(Long modelId){
        this.modelId = modelId ;
        this.modify("model_id",modelId);
    }

    /**
     * 设置 [品牌]
     */
    public void setBrandId(Long brandId){
        this.brandId = brandId ;
        this.modify("brand_id",brandId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [状态]
     */
    public void setStateId(Long stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }

    /**
     * 设置 [驾驶员]
     */
    public void setDriverId(Long driverId){
        this.driverId = driverId ;
        this.modify("driver_id",driverId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


