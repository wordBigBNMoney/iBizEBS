package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quantSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_quantMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[即时库存] 服务对象接口实现
 */
@Slf4j
@Service("Stock_quantServiceImpl")
public class Stock_quantServiceImpl extends EBSServiceImpl<Stock_quantMapper, Stock_quant> implements IStock_quantService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService stockProductionLotService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quant_packageService stockQuantPackageService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.quant" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_quant et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_quantService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_quant> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_quant et) {
        Stock_quant old = new Stock_quant() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_quantService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_quantService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_quant> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_quant get(Long key) {
        Stock_quant et = getById(key);
        if(et==null){
            et=new Stock_quant();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_quant getDraft(Stock_quant et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_quant et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_quant et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_quant et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_quant> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_quant> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_quant> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public List<Stock_quant> selectByProductId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_quant>().in("id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Stock_quant>().eq("product_id",id));
    }

	@Override
    public List<Stock_quant> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Stock_quant>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_quant>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Stock_quant>().eq("company_id",id));
    }

	@Override
    public List<Stock_quant> selectByOwnerId(Long id) {
        return baseMapper.selectByOwnerId(id);
    }
    @Override
    public void resetByOwnerId(Long id) {
        this.update(new UpdateWrapper<Stock_quant>().set("owner_id",null).eq("owner_id",id));
    }

    @Override
    public void resetByOwnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_quant>().set("owner_id",null).in("owner_id",ids));
    }

    @Override
    public void removeByOwnerId(Long id) {
        this.remove(new QueryWrapper<Stock_quant>().eq("owner_id",id));
    }

	@Override
    public List<Stock_quant> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_quant>().eq("create_uid",id));
    }

	@Override
    public List<Stock_quant> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_quant>().eq("write_uid",id));
    }

	@Override
    public List<Stock_quant> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public List<Stock_quant> selectByLocationId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_quant>().in("id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_quant>().eq("location_id",id));
    }

	@Override
    public List<Stock_quant> selectByLotId(Long id) {
        return baseMapper.selectByLotId(id);
    }
    @Override
    public List<Stock_quant> selectByLotId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_quant>().in("id",ids));
    }

    @Override
    public void removeByLotId(Long id) {
        this.remove(new QueryWrapper<Stock_quant>().eq("lot_id",id));
    }

	@Override
    public List<Stock_quant> selectByPackageId(Long id) {
        return baseMapper.selectByPackageId(id);
    }
    @Override
    public List<Stock_quant> selectByPackageId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_quant>().in("id",ids));
    }

    @Override
    public void removeByPackageId(Long id) {
        this.remove(new QueryWrapper<Stock_quant>().eq("package_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_quant> searchDefault(Stock_quantSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_quant> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_quant>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_quant et){
        //实体关系[DER1N_STOCK_QUANT__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductTmplId(odooProduct.getProductTmplId());
            et.setProductIdText(odooProduct.getName());
            et.setProductUomId(odooProduct.getUomId());
        }
        //实体关系[DER1N_STOCK_QUANT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_STOCK_QUANT__RES_PARTNER__OWNER_ID]
        if(!ObjectUtils.isEmpty(et.getOwnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOwner=et.getOdooOwner();
            if(ObjectUtils.isEmpty(odooOwner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getOwnerId());
                et.setOdooOwner(majorEntity);
                odooOwner=majorEntity;
            }
            et.setOwnerIdText(odooOwner.getName());
        }
        //实体关系[DER1N_STOCK_QUANT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_QUANT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_QUANT__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_STOCK_QUANT__STOCK_PRODUCTION_LOT__LOT_ID]
        if(!ObjectUtils.isEmpty(et.getLotId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLot=et.getOdooLot();
            if(ObjectUtils.isEmpty(odooLot)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot majorEntity=stockProductionLotService.get(et.getLotId());
                et.setOdooLot(majorEntity);
                odooLot=majorEntity;
            }
            et.setLotIdText(odooLot.getName());
        }
        //实体关系[DER1N_STOCK_QUANT__STOCK_QUANT_PACKAGE__PACKAGE_ID]
        if(!ObjectUtils.isEmpty(et.getPackageId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package odooPackage=et.getOdooPackage();
            if(ObjectUtils.isEmpty(odooPackage)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package majorEntity=stockQuantPackageService.get(et.getPackageId());
                et.setOdooPackage(majorEntity);
                odooPackage=majorEntity;
            }
            et.setPackageIdText(odooPackage.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_quant> getStockQuantByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_quant> getStockQuantByEntities(List<Stock_quant> entities) {
        List ids =new ArrayList();
        for(Stock_quant entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



