package cn.ibizlab.businesscentral.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_config_settingsSearchContext;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_config_settingsService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_base.mapper.Res_config_settingsMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[配置设定] 服务对象接口实现
 */
@Slf4j
@Service("Res_config_settingsServiceImpl")
public class Res_config_settingsServiceImpl extends EBSServiceImpl<Res_config_settingsMapper, Res_config_settings> implements IRes_config_settingsService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_chart_templateService accountChartTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_digest.service.IDigest_digestService digestDigestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService mailTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_templateService saleOrderTemplateService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "res.config.settings" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Res_config_settings et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_config_settingsService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Res_config_settings> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Res_config_settings et) {
        Res_config_settings old = new Res_config_settings() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_config_settingsService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_config_settingsService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Res_config_settings> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Res_config_settings get(Long key) {
        Res_config_settings et = getById(key);
        if(et==null){
            et=new Res_config_settings();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Res_config_settings getDraft(Res_config_settings et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Res_config_settings et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public Res_config_settings getLatestSettings(Res_config_settings et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Res_config_settings get_default(Res_config_settings et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(Res_config_settings et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Res_config_settings et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Res_config_settings> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Res_config_settings> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Res_config_settings> selectByChartTemplateId(Long id) {
        return baseMapper.selectByChartTemplateId(id);
    }
    @Override
    public void resetByChartTemplateId(Long id) {
        this.update(new UpdateWrapper<Res_config_settings>().set("chart_template_id",null).eq("chart_template_id",id));
    }

    @Override
    public void resetByChartTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_config_settings>().set("chart_template_id",null).in("chart_template_id",ids));
    }

    @Override
    public void removeByChartTemplateId(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("chart_template_id",id));
    }

	@Override
    public List<Res_config_settings> selectByDigestId(Long id) {
        return baseMapper.selectByDigestId(id);
    }
    @Override
    public void resetByDigestId(Long id) {
        this.update(new UpdateWrapper<Res_config_settings>().set("digest_id",null).eq("digest_id",id));
    }

    @Override
    public void resetByDigestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_config_settings>().set("digest_id",null).in("digest_id",ids));
    }

    @Override
    public void removeByDigestId(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("digest_id",id));
    }

	@Override
    public List<Res_config_settings> selectByTemplateId(Long id) {
        return baseMapper.selectByTemplateId(id);
    }
    @Override
    public void resetByTemplateId(Long id) {
        this.update(new UpdateWrapper<Res_config_settings>().set("template_id",null).eq("template_id",id));
    }

    @Override
    public void resetByTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_config_settings>().set("template_id",null).in("template_id",ids));
    }

    @Override
    public void removeByTemplateId(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("template_id",id));
    }

	@Override
    public List<Res_config_settings> selectByDepositDefaultProductId(Long id) {
        return baseMapper.selectByDepositDefaultProductId(id);
    }
    @Override
    public void resetByDepositDefaultProductId(Long id) {
        this.update(new UpdateWrapper<Res_config_settings>().set("deposit_default_product_id",null).eq("deposit_default_product_id",id));
    }

    @Override
    public void resetByDepositDefaultProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_config_settings>().set("deposit_default_product_id",null).in("deposit_default_product_id",ids));
    }

    @Override
    public void removeByDepositDefaultProductId(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("deposit_default_product_id",id));
    }

	@Override
    public List<Res_config_settings> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Res_config_settings>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_config_settings>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("company_id",id));
    }

	@Override
    public List<Res_config_settings> selectByAuthSignupTemplateUserId(Long id) {
        return baseMapper.selectByAuthSignupTemplateUserId(id);
    }
    @Override
    public void resetByAuthSignupTemplateUserId(Long id) {
        this.update(new UpdateWrapper<Res_config_settings>().set("auth_signup_template_user_id",null).eq("auth_signup_template_user_id",id));
    }

    @Override
    public void resetByAuthSignupTemplateUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_config_settings>().set("auth_signup_template_user_id",null).in("auth_signup_template_user_id",ids));
    }

    @Override
    public void removeByAuthSignupTemplateUserId(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("auth_signup_template_user_id",id));
    }

	@Override
    public List<Res_config_settings> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("create_uid",id));
    }

	@Override
    public List<Res_config_settings> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("write_uid",id));
    }

	@Override
    public List<Res_config_settings> selectByDefaultSaleOrderTemplateId(Long id) {
        return baseMapper.selectByDefaultSaleOrderTemplateId(id);
    }
    @Override
    public void resetByDefaultSaleOrderTemplateId(Long id) {
        this.update(new UpdateWrapper<Res_config_settings>().set("default_sale_order_template_id",null).eq("default_sale_order_template_id",id));
    }

    @Override
    public void resetByDefaultSaleOrderTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_config_settings>().set("default_sale_order_template_id",null).in("default_sale_order_template_id",ids));
    }

    @Override
    public void removeByDefaultSaleOrderTemplateId(Long id) {
        this.remove(new QueryWrapper<Res_config_settings>().eq("default_sale_order_template_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Res_config_settings> searchDefault(Res_config_settingsSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_config_settings> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_config_settings>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Res_config_settings et){
        //实体关系[DER1N_RES_CONFIG_SETTINGS__ACCOUNT_CHART_TEMPLATE__CHART_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getChartTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate=et.getOdooChartTemplate();
            if(ObjectUtils.isEmpty(odooChartTemplate)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template majorEntity=accountChartTemplateService.get(et.getChartTemplateId());
                et.setOdooChartTemplate(majorEntity);
                odooChartTemplate=majorEntity;
            }
            et.setChartTemplateIdText(odooChartTemplate.getName());
        }
        //实体关系[DER1N_RES_CONFIG_SETTINGS__DIGEST_DIGEST__DIGEST_ID]
        if(!ObjectUtils.isEmpty(et.getDigestId())){
            cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_digest odooDigest=et.getOdooDigest();
            if(ObjectUtils.isEmpty(odooDigest)){
                cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_digest majorEntity=digestDigestService.get(et.getDigestId());
                et.setOdooDigest(majorEntity);
                odooDigest=majorEntity;
            }
            et.setDigestIdText(odooDigest.getName());
        }
        //实体关系[DER1N_RES_CONFIG_SETTINGS__MAIL_TEMPLATE__TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate=et.getOdooTemplate();
            if(ObjectUtils.isEmpty(odooTemplate)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template majorEntity=mailTemplateService.get(et.getTemplateId());
                et.setOdooTemplate(majorEntity);
                odooTemplate=majorEntity;
            }
            et.setTemplateIdText(odooTemplate.getName());
        }
        //实体关系[DER1N_RES_CONFIG_SETTINGS__PRODUCT_PRODUCT__DEPOSIT_DEFAULT_PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getDepositDefaultProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooDepositDefaultProduct=et.getOdooDepositDefaultProduct();
            if(ObjectUtils.isEmpty(odooDepositDefaultProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getDepositDefaultProductId());
                et.setOdooDepositDefaultProduct(majorEntity);
                odooDepositDefaultProduct=majorEntity;
            }
            et.setDepositDefaultProductIdText(odooDepositDefaultProduct.getName());
        }
        //实体关系[DER1N_RES_CONFIG_SETTINGS__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyCurrencyId(odooCompany.getCurrencyId());
            et.setTaxExigibility(odooCompany.getTaxExigibility());
            et.setAccountBankReconciliationStart(odooCompany.getAccountBankReconciliationStart());
            et.setTaxCashBasisJournalId(odooCompany.getTaxCashBasisJournalId());
            et.setPoLead(odooCompany.getPoLead());
            et.setSnailmailColor(odooCompany.getSnailmailColor());
            et.setPaperformatId(odooCompany.getPaperformatId());
            et.setPortalConfirmationSign(odooCompany.getPortalConfirmationSign());
            et.setCurrencyId(odooCompany.getCurrencyId());
            et.setInvoiceIsPrint(odooCompany.getInvoiceIsPrint());
            et.setResourceCalendarId(odooCompany.getResourceCalendarId());
            et.setPoLock(odooCompany.getPoLock());
            et.setInvoiceIsSnailmail(odooCompany.getInvoiceIsSnailmail());
            et.setManufacturingLead(odooCompany.getManufacturingLead());
            et.setPoDoubleValidation(odooCompany.getPoDoubleValidation());
            et.setReportFooter(odooCompany.getReportFooter());
            et.setInvoiceIsEmail(odooCompany.getInvoiceIsEmail());
            et.setPortalConfirmationPay(odooCompany.getPortalConfirmationPay());
            et.setQrCode(odooCompany.getQrCode());
            et.setSnailmailDuplex(odooCompany.getSnailmailDuplex());
            et.setCompanyIdText(odooCompany.getName());
            et.setSecurityLead(odooCompany.getSecurityLead());
            et.setExternalReportLayoutId(odooCompany.getExternalReportLayoutId());
            et.setPurchaseTaxId(odooCompany.getAccountPurchaseTaxId());
            et.setQuotationValidityDays(odooCompany.getQuotationValidityDays());
            et.setTaxCalculationRoundingMethod(odooCompany.getTaxCalculationRoundingMethod());
            et.setCurrencyExchangeJournalId(odooCompany.getCurrencyExchangeJournalId());
            et.setPoDoubleValidationAmount(odooCompany.getPoDoubleValidationAmount());
            et.setSaleTaxId(odooCompany.getAccountSaleTaxId());
        }
        //实体关系[DER1N_RES_CONFIG_SETTINGS__RES_USERS__AUTH_SIGNUP_TEMPLATE_USER_ID]
        if(!ObjectUtils.isEmpty(et.getAuthSignupTemplateUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooAuthSignupTemplateUser=et.getOdooAuthSignupTemplateUser();
            if(ObjectUtils.isEmpty(odooAuthSignupTemplateUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getAuthSignupTemplateUserId());
                et.setOdooAuthSignupTemplateUser(majorEntity);
                odooAuthSignupTemplateUser=majorEntity;
            }
            et.setAuthSignupTemplateUserIdText(odooAuthSignupTemplateUser.getName());
        }
        //实体关系[DER1N_RES_CONFIG_SETTINGS__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_RES_CONFIG_SETTINGS__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_RES_CONFIG_SETTINGS__SALE_ORDER_TEMPLATE__DEFAULT_SALE_ORDER_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getDefaultSaleOrderTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template odooDefaultSaleOrderTemplate=et.getOdooDefaultSaleOrderTemplate();
            if(ObjectUtils.isEmpty(odooDefaultSaleOrderTemplate)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template majorEntity=saleOrderTemplateService.get(et.getDefaultSaleOrderTemplateId());
                et.setOdooDefaultSaleOrderTemplate(majorEntity);
                odooDefaultSaleOrderTemplate=majorEntity;
            }
            et.setDefaultSaleOrderTemplateIdText(odooDefaultSaleOrderTemplate.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Res_config_settings> getResConfigSettingsByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Res_config_settings> getResConfigSettingsByEntities(List<Res_config_settings> entities) {
        List ids =new ArrayList();
        for(Res_config_settings entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



