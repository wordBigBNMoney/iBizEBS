package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[消息通知]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_NOTIFICATION",resultMap = "Mail_notificationResultMap")
public class Mail_notification extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 失败原因
     */
    @DEField(name = "failure_reason")
    @TableField(value = "failure_reason")
    @JSONField(name = "failure_reason")
    @JsonProperty("failure_reason")
    private String failureReason;
    /**
     * 以邮件发送
     */
    @DEField(name = "is_email")
    @TableField(value = "is_email")
    @JSONField(name = "is_email")
    @JsonProperty("is_email")
    private Boolean isEmail;
    /**
     * 已读
     */
    @DEField(name = "is_read")
    @TableField(value = "is_read")
    @JSONField(name = "is_read")
    @JsonProperty("is_read")
    private Boolean isRead;
    /**
     * EMail状态
     */
    @DEField(name = "email_status")
    @TableField(value = "email_status")
    @JSONField(name = "email_status")
    @JsonProperty("email_status")
    private String emailStatus;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 失败类型
     */
    @DEField(name = "failure_type")
    @TableField(value = "failure_type")
    @JSONField(name = "failure_type")
    @JsonProperty("failure_type")
    private String failureType;
    /**
     * 需收件人
     */
    @TableField(exist = false)
    @JSONField(name = "res_partner_id_text")
    @JsonProperty("res_partner_id_text")
    private String resPartnerIdText;
    /**
     * 邮件
     */
    @DEField(name = "mail_id")
    @TableField(value = "mail_id")
    @JSONField(name = "mail_id")
    @JsonProperty("mail_id")
    private Long mailId;
    /**
     * 消息
     */
    @DEField(name = "mail_message_id")
    @TableField(value = "mail_message_id")
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    private Long mailMessageId;
    /**
     * 需收件人
     */
    @DEField(name = "res_partner_id")
    @TableField(value = "res_partner_id")
    @JSONField(name = "res_partner_id")
    @JsonProperty("res_partner_id")
    private Long resPartnerId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail odooMail;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message odooMailMessage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooResPartner;



    /**
     * 设置 [失败原因]
     */
    public void setFailureReason(String failureReason){
        this.failureReason = failureReason ;
        this.modify("failure_reason",failureReason);
    }

    /**
     * 设置 [以邮件发送]
     */
    public void setIsEmail(Boolean isEmail){
        this.isEmail = isEmail ;
        this.modify("is_email",isEmail);
    }

    /**
     * 设置 [已读]
     */
    public void setIsRead(Boolean isRead){
        this.isRead = isRead ;
        this.modify("is_read",isRead);
    }

    /**
     * 设置 [EMail状态]
     */
    public void setEmailStatus(String emailStatus){
        this.emailStatus = emailStatus ;
        this.modify("email_status",emailStatus);
    }

    /**
     * 设置 [失败类型]
     */
    public void setFailureType(String failureType){
        this.failureType = failureType ;
        this.modify("failure_type",failureType);
    }

    /**
     * 设置 [邮件]
     */
    public void setMailId(Long mailId){
        this.mailId = mailId ;
        this.modify("mail_id",mailId);
    }

    /**
     * 设置 [消息]
     */
    public void setMailMessageId(Long mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }

    /**
     * 设置 [需收件人]
     */
    public void setResPartnerId(Long resPartnerId){
        this.resPartnerId = resPartnerId ;
        this.modify("res_partner_id",resPartnerId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


