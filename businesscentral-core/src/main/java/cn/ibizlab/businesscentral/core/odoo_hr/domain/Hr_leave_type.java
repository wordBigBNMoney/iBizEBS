package cn.ibizlab.businesscentral.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[休假类型]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "HR_LEAVE_TYPE",resultMap = "Hr_leave_typeResultMap")
public class Hr_leave_type extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 有效
     */
    @TableField(exist = false)
    @JSONField(name = "valid")
    @JsonProperty("valid")
    private Boolean valid;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 虚拟剩余休假
     */
    @TableField(exist = false)
    @JSONField(name = "virtual_remaining_leaves")
    @JsonProperty("virtual_remaining_leaves")
    private Double virtualRemainingLeaves;
    /**
     * 休假早已使用
     */
    @TableField(exist = false)
    @JSONField(name = "leaves_taken")
    @JsonProperty("leaves_taken")
    private Double leavesTaken;
    /**
     * 分配天数
     */
    @TableField(exist = false)
    @JSONField(name = "group_days_allocation")
    @JsonProperty("group_days_allocation")
    private Double groupDaysAllocation;
    /**
     * 应用双重验证
     */
    @TableField(exist = false)
    @JSONField(name = "double_validation")
    @JsonProperty("double_validation")
    private Boolean doubleValidation;
    /**
     * 模式
     */
    @DEField(name = "allocation_type")
    @TableField(value = "allocation_type")
    @JSONField(name = "allocation_type")
    @JsonProperty("allocation_type")
    private String allocationType;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 没付款
     */
    @TableField(value = "unpaid")
    @JSONField(name = "unpaid")
    @JsonProperty("unpaid")
    private Boolean unpaid;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最大允许
     */
    @TableField(exist = false)
    @JSONField(name = "max_leaves")
    @JsonProperty("max_leaves")
    private Double maxLeaves;
    /**
     * 结束日期
     */
    @DEField(name = "validity_stop")
    @TableField(value = "validity_stop")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_stop" , format="yyyy-MM-dd")
    @JsonProperty("validity_stop")
    private Timestamp validityStop;
    /**
     * 验证人
     */
    @DEField(name = "validation_type")
    @TableField(value = "validation_type")
    @JSONField(name = "validation_type")
    @JsonProperty("validation_type")
    private String validationType;
    /**
     * 请假
     */
    @DEField(name = "time_type")
    @TableField(value = "time_type")
    @JSONField(name = "time_type")
    @JsonProperty("time_type")
    private String timeType;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 休假
     */
    @DEField(name = "request_unit")
    @TableField(value = "request_unit")
    @JSONField(name = "request_unit")
    @JsonProperty("request_unit")
    private String requestUnit;
    /**
     * 集团假期
     */
    @TableField(exist = false)
    @JSONField(name = "group_days_leave")
    @JsonProperty("group_days_leave")
    private Double groupDaysLeave;
    /**
     * 报表中的颜色
     */
    @DEField(name = "color_name")
    @TableField(value = "color_name")
    @JSONField(name = "color_name")
    @JsonProperty("color_name")
    private String colorName;
    /**
     * 剩余休假
     */
    @TableField(exist = false)
    @JSONField(name = "remaining_leaves")
    @JsonProperty("remaining_leaves")
    private Double remainingLeaves;
    /**
     * 休假类型
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 开始日期
     */
    @DEField(name = "validity_start")
    @TableField(value = "validity_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_start" , format="yyyy-MM-dd")
    @JsonProperty("validity_start")
    private Timestamp validityStart;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 会议类型
     */
    @TableField(exist = false)
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    private String categIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 会议类型
     */
    @DEField(name = "categ_id")
    @TableField(value = "categ_id")
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    private Long categId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event_type odooCateg;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [模式]
     */
    public void setAllocationType(String allocationType){
        this.allocationType = allocationType ;
        this.modify("allocation_type",allocationType);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [没付款]
     */
    public void setUnpaid(Boolean unpaid){
        this.unpaid = unpaid ;
        this.modify("unpaid",unpaid);
    }

    /**
     * 设置 [结束日期]
     */
    public void setValidityStop(Timestamp validityStop){
        this.validityStop = validityStop ;
        this.modify("validity_stop",validityStop);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatValidityStop(){
        if (this.validityStop == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(validityStop);
    }
    /**
     * 设置 [验证人]
     */
    public void setValidationType(String validationType){
        this.validationType = validationType ;
        this.modify("validation_type",validationType);
    }

    /**
     * 设置 [请假]
     */
    public void setTimeType(String timeType){
        this.timeType = timeType ;
        this.modify("time_type",timeType);
    }

    /**
     * 设置 [休假]
     */
    public void setRequestUnit(String requestUnit){
        this.requestUnit = requestUnit ;
        this.modify("request_unit",requestUnit);
    }

    /**
     * 设置 [报表中的颜色]
     */
    public void setColorName(String colorName){
        this.colorName = colorName ;
        this.modify("color_name",colorName);
    }

    /**
     * 设置 [休假类型]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [开始日期]
     */
    public void setValidityStart(Timestamp validityStart){
        this.validityStart = validityStart ;
        this.modify("validity_start",validityStart);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatValidityStart(){
        if (this.validityStart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(validityStart);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [会议类型]
     */
    public void setCategId(Long categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


