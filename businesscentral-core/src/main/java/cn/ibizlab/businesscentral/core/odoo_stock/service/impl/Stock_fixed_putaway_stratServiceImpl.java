package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_fixed_putaway_stratService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_fixed_putaway_stratMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[位置固定上架策略] 服务对象接口实现
 */
@Slf4j
@Service("Stock_fixed_putaway_stratServiceImpl")
public class Stock_fixed_putaway_stratServiceImpl extends EBSServiceImpl<Stock_fixed_putaway_stratMapper, Stock_fixed_putaway_strat> implements IStock_fixed_putaway_stratService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_categoryService productCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_putawayService productPutawayService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.fixed.putaway.strat" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_fixed_putaway_strat et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_fixed_putaway_stratService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_fixed_putaway_strat> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_fixed_putaway_strat et) {
        Stock_fixed_putaway_strat old = new Stock_fixed_putaway_strat() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_fixed_putaway_stratService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_fixed_putaway_stratService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_fixed_putaway_strat> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_fixed_putaway_strat get(Long key) {
        Stock_fixed_putaway_strat et = getById(key);
        if(et==null){
            et=new Stock_fixed_putaway_strat();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_fixed_putaway_strat getDraft(Stock_fixed_putaway_strat et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_fixed_putaway_strat et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_fixed_putaway_strat et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_fixed_putaway_strat et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_fixed_putaway_strat> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_fixed_putaway_strat> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_fixed_putaway_strat> selectByCategoryId(Long id) {
        return baseMapper.selectByCategoryId(id);
    }
    @Override
    public void resetByCategoryId(Long id) {
        this.update(new UpdateWrapper<Stock_fixed_putaway_strat>().set("category_id",null).eq("category_id",id));
    }

    @Override
    public void resetByCategoryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_fixed_putaway_strat>().set("category_id",null).in("category_id",ids));
    }

    @Override
    public void removeByCategoryId(Long id) {
        this.remove(new QueryWrapper<Stock_fixed_putaway_strat>().eq("category_id",id));
    }

	@Override
    public List<Stock_fixed_putaway_strat> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Stock_fixed_putaway_strat>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_fixed_putaway_strat>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Stock_fixed_putaway_strat>().eq("product_id",id));
    }

	@Override
    public List<Stock_fixed_putaway_strat> selectByPutawayId(Long id) {
        return baseMapper.selectByPutawayId(id);
    }
    @Override
    public void resetByPutawayId(Long id) {
        this.update(new UpdateWrapper<Stock_fixed_putaway_strat>().set("putaway_id",null).eq("putaway_id",id));
    }

    @Override
    public void resetByPutawayId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_fixed_putaway_strat>().set("putaway_id",null).in("putaway_id",ids));
    }

    @Override
    public void removeByPutawayId(Long id) {
        this.remove(new QueryWrapper<Stock_fixed_putaway_strat>().eq("putaway_id",id));
    }

	@Override
    public List<Stock_fixed_putaway_strat> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_fixed_putaway_strat>().eq("create_uid",id));
    }

	@Override
    public List<Stock_fixed_putaway_strat> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_fixed_putaway_strat>().eq("write_uid",id));
    }

	@Override
    public List<Stock_fixed_putaway_strat> selectByFixedLocationId(Long id) {
        return baseMapper.selectByFixedLocationId(id);
    }
    @Override
    public void resetByFixedLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_fixed_putaway_strat>().set("fixed_location_id",null).eq("fixed_location_id",id));
    }

    @Override
    public void resetByFixedLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_fixed_putaway_strat>().set("fixed_location_id",null).in("fixed_location_id",ids));
    }

    @Override
    public void removeByFixedLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_fixed_putaway_strat>().eq("fixed_location_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_fixed_putaway_strat> searchDefault(Stock_fixed_putaway_stratSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_fixed_putaway_strat> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_fixed_putaway_strat>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_fixed_putaway_strat et){
        //实体关系[DER1N_STOCK_FIXED_PUTAWAY_STRAT__PRODUCT_CATEGORY__CATEGORY_ID]
        if(!ObjectUtils.isEmpty(et.getCategoryId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category odooCategory=et.getOdooCategory();
            if(ObjectUtils.isEmpty(odooCategory)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category majorEntity=productCategoryService.get(et.getCategoryId());
                et.setOdooCategory(majorEntity);
                odooCategory=majorEntity;
            }
            et.setCategoryIdText(odooCategory.getName());
        }
        //实体关系[DER1N_STOCK_FIXED_PUTAWAY_STRAT__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_STOCK_FIXED_PUTAWAY_STRAT__PRODUCT_PUTAWAY__PUTAWAY_ID]
        if(!ObjectUtils.isEmpty(et.getPutawayId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_putaway odooPutaway=et.getOdooPutaway();
            if(ObjectUtils.isEmpty(odooPutaway)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_putaway majorEntity=productPutawayService.get(et.getPutawayId());
                et.setOdooPutaway(majorEntity);
                odooPutaway=majorEntity;
            }
            et.setPutawayIdText(odooPutaway.getName());
        }
        //实体关系[DER1N_STOCK_FIXED_PUTAWAY_STRAT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_FIXED_PUTAWAY_STRAT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_FIXED_PUTAWAY_STRAT__STOCK_LOCATION__FIXED_LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getFixedLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooFixedLocation=et.getOdooFixedLocation();
            if(ObjectUtils.isEmpty(odooFixedLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getFixedLocationId());
                et.setOdooFixedLocation(majorEntity);
                odooFixedLocation=majorEntity;
            }
            et.setFixedLocationIdText(odooFixedLocation.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_fixed_putaway_strat> getStockFixedPutawayStratByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_fixed_putaway_strat> getStockFixedPutawayStratByEntities(List<Stock_fixed_putaway_strat> entities) {
        List ids =new ArrayList();
        for(Stock_fixed_putaway_strat entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



