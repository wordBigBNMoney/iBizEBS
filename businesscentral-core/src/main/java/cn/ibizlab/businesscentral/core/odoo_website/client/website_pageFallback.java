package cn.ibizlab.businesscentral.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_page;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_pageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_page] 服务对象接口
 */
@Component
public class website_pageFallback implements website_pageFeignClient{


    public Website_page update(Long id, Website_page website_page){
            return null;
     }
    public Boolean updateBatch(List<Website_page> website_pages){
            return false;
     }


    public Website_page get(Long id){
            return null;
     }


    public Website_page create(Website_page website_page){
            return null;
     }
    public Boolean createBatch(List<Website_page> website_pages){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Website_page> search(Website_pageSearchContext context){
            return null;
     }


    public Page<Website_page> select(){
            return null;
     }

    public Website_page getDraft(){
            return null;
    }



    public Boolean checkKey(Website_page website_page){
            return false;
     }


    public Boolean save(Website_page website_page){
            return false;
     }
    public Boolean saveBatch(List<Website_page> website_pages){
            return false;
     }

    public Page<Website_page> searchDefault(Website_pageSearchContext context){
            return null;
     }


}
