package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[群发邮件]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_MASS_MAILING",resultMap = "Mail_mass_mailingResultMap")
public class Mail_mass_mailing extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发送日期
     */
    @DEField(name = "sent_date")
    @TableField(value = "sent_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sent_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sent_date")
    private Timestamp sentDate;
    /**
     * 报价个数
     */
    @TableField(exist = false)
    @JSONField(name = "sale_quotation_count")
    @JsonProperty("sale_quotation_count")
    private Integer saleQuotationCount;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 域
     */
    @DEField(name = "mailing_domain")
    @TableField(value = "mailing_domain")
    @JSONField(name = "mailing_domain")
    @JsonProperty("mailing_domain")
    private String mailingDomain;
    /**
     * 已开启
     */
    @TableField(exist = false)
    @JSONField(name = "opened")
    @JsonProperty("opened")
    private Integer opened;
    /**
     * 回复
     */
    @DEField(name = "reply_to")
    @TableField(value = "reply_to")
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;
    /**
     * 已回复
     */
    @TableField(exist = false)
    @JSONField(name = "replied")
    @JsonProperty("replied")
    private Integer replied;
    /**
     * 线索总数
     */
    @TableField(exist = false)
    @JSONField(name = "crm_lead_count")
    @JsonProperty("crm_lead_count")
    private Integer crmLeadCount;
    /**
     * 收件人实物模型
     */
    @TableField(exist = false)
    @JSONField(name = "mailing_model_real")
    @JsonProperty("mailing_model_real")
    private String mailingModelReal;
    /**
     * 点击数
     */
    @TableField(exist = false)
    @JSONField(name = "clicks_ratio")
    @JsonProperty("clicks_ratio")
    private Integer clicksRatio;
    /**
     * 已送货
     */
    @TableField(exist = false)
    @JSONField(name = "delivered")
    @JsonProperty("delivered")
    private Integer delivered;
    /**
     * 收件人模型
     */
    @DEField(name = "mailing_model_id")
    @TableField(value = "mailing_model_id")
    @JSONField(name = "mailing_model_id")
    @JsonProperty("mailing_model_id")
    private Integer mailingModelId;
    /**
     * 回复模式
     */
    @DEField(name = "reply_to_mode")
    @TableField(value = "reply_to_mode")
    @JSONField(name = "reply_to_mode")
    @JsonProperty("reply_to_mode")
    private String replyToMode;
    /**
     * 在将来计划
     */
    @DEField(name = "schedule_date")
    @TableField(value = "schedule_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedule_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedule_date")
    private Timestamp scheduleDate;
    /**
     * 从
     */
    @DEField(name = "email_from")
    @TableField(value = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 开票金额
     */
    @TableField(exist = false)
    @JSONField(name = "sale_invoiced_amount")
    @JsonProperty("sale_invoiced_amount")
    private Integer saleInvoicedAmount;
    /**
     * 附件
     */
    @TableField(exist = false)
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;
    /**
     * 总计
     */
    @TableField(exist = false)
    @JSONField(name = "total")
    @JsonProperty("total")
    private Integer total;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 被退回的比率
     */
    @TableField(exist = false)
    @JSONField(name = "bounced_ratio")
    @JsonProperty("bounced_ratio")
    private Integer bouncedRatio;
    /**
     * 安排的日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_departure" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("next_departure")
    private Timestamp nextDeparture;
    /**
     * 邮件服务器
     */
    @DEField(name = "mail_server_id")
    @TableField(value = "mail_server_id")
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;
    /**
     * 忽略
     */
    @TableField(exist = false)
    @JSONField(name = "ignored")
    @JsonProperty("ignored")
    private Integer ignored;
    /**
     * 安排
     */
    @TableField(exist = false)
    @JSONField(name = "scheduled")
    @JsonProperty("scheduled")
    private Integer scheduled;
    /**
     * 使用线索
     */
    @TableField(exist = false)
    @JSONField(name = "crm_lead_activated")
    @JsonProperty("crm_lead_activated")
    private Boolean crmLeadActivated;
    /**
     * 点击率
     */
    @TableField(exist = false)
    @JSONField(name = "clicked")
    @JsonProperty("clicked")
    private Integer clicked;
    /**
     * 回复比例
     */
    @TableField(exist = false)
    @JSONField(name = "replied_ratio")
    @JsonProperty("replied_ratio")
    private Integer repliedRatio;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 邮件统计
     */
    @TableField(exist = false)
    @JSONField(name = "statistics_ids")
    @JsonProperty("statistics_ids")
    private String statisticsIds;
    /**
     * 打开比例
     */
    @TableField(exist = false)
    @JSONField(name = "opened_ratio")
    @JsonProperty("opened_ratio")
    private Integer openedRatio;
    /**
     * 正文
     */
    @DEField(name = "body_html")
    @TableField(value = "body_html")
    @JSONField(name = "body_html")
    @JsonProperty("body_html")
    private String bodyHtml;
    /**
     * 被退回
     */
    @TableField(exist = false)
    @JSONField(name = "bounced")
    @JsonProperty("bounced")
    private Integer bounced;
    /**
     * 颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 收件人模型
     */
    @TableField(exist = false)
    @JSONField(name = "mailing_model_name")
    @JsonProperty("mailing_model_name")
    private String mailingModelName;
    /**
     * 邮件列表
     */
    @TableField(exist = false)
    @JSONField(name = "contact_list_ids")
    @JsonProperty("contact_list_ids")
    private String contactListIds;
    /**
     * 预期
     */
    @TableField(exist = false)
    @JSONField(name = "expected")
    @JsonProperty("expected")
    private Integer expected;
    /**
     * 失败的
     */
    @TableField(exist = false)
    @JSONField(name = "failed")
    @JsonProperty("failed")
    private Integer failed;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 已接收比例
     */
    @TableField(exist = false)
    @JSONField(name = "received_ratio")
    @JsonProperty("received_ratio")
    private Integer receivedRatio;
    /**
     * 商机个数
     */
    @TableField(exist = false)
    @JSONField(name = "crm_opportunities_count")
    @JsonProperty("crm_opportunities_count")
    private Integer crmOpportunitiesCount;
    /**
     * 归档
     */
    @DEField(name = "keep_archives")
    @TableField(value = "keep_archives")
    @JSONField(name = "keep_archives")
    @JsonProperty("keep_archives")
    private Boolean keepArchives;
    /**
     * A/B 测试百分比
     */
    @DEField(name = "contact_ab_pc")
    @TableField(value = "contact_ab_pc")
    @JSONField(name = "contact_ab_pc")
    @JsonProperty("contact_ab_pc")
    private Integer contactAbPc;
    /**
     * 已汇
     */
    @TableField(exist = false)
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Integer sent;
    /**
     * 营销
     */
    @TableField(exist = false)
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;
    /**
     * 邮件管理器
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 来源名称
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 群发邮件营销
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_campaign_id_text")
    @JsonProperty("mass_mailing_campaign_id_text")
    private String massMailingCampaignIdText;
    /**
     * 媒体
     */
    @TableField(exist = false)
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 邮件管理器
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 群发邮件营销
     */
    @DEField(name = "mass_mailing_campaign_id")
    @TableField(value = "mass_mailing_campaign_id")
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    private Long massMailingCampaignId;
    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @TableField(value = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Long campaignId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @TableField(value = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Long mediumId;
    /**
     * 主题
     */
    @DEField(name = "source_id")
    @TableField(value = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Long sourceId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign odooMassMailingCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource;



    /**
     * 设置 [发送日期]
     */
    public void setSentDate(Timestamp sentDate){
        this.sentDate = sentDate ;
        this.modify("sent_date",sentDate);
    }

    /**
     * 格式化日期 [发送日期]
     */
    public String formatSentDate(){
        if (this.sentDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sentDate);
    }
    /**
     * 设置 [域]
     */
    public void setMailingDomain(String mailingDomain){
        this.mailingDomain = mailingDomain ;
        this.modify("mailing_domain",mailingDomain);
    }

    /**
     * 设置 [回复]
     */
    public void setReplyTo(String replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }

    /**
     * 设置 [收件人模型]
     */
    public void setMailingModelId(Integer mailingModelId){
        this.mailingModelId = mailingModelId ;
        this.modify("mailing_model_id",mailingModelId);
    }

    /**
     * 设置 [回复模式]
     */
    public void setReplyToMode(String replyToMode){
        this.replyToMode = replyToMode ;
        this.modify("reply_to_mode",replyToMode);
    }

    /**
     * 设置 [在将来计划]
     */
    public void setScheduleDate(Timestamp scheduleDate){
        this.scheduleDate = scheduleDate ;
        this.modify("schedule_date",scheduleDate);
    }

    /**
     * 格式化日期 [在将来计划]
     */
    public String formatScheduleDate(){
        if (this.scheduleDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduleDate);
    }
    /**
     * 设置 [从]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [邮件服务器]
     */
    public void setMailServerId(Integer mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [正文]
     */
    public void setBodyHtml(String bodyHtml){
        this.bodyHtml = bodyHtml ;
        this.modify("body_html",bodyHtml);
    }

    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [归档]
     */
    public void setKeepArchives(Boolean keepArchives){
        this.keepArchives = keepArchives ;
        this.modify("keep_archives",keepArchives);
    }

    /**
     * 设置 [A/B 测试百分比]
     */
    public void setContactAbPc(Integer contactAbPc){
        this.contactAbPc = contactAbPc ;
        this.modify("contact_ab_pc",contactAbPc);
    }

    /**
     * 设置 [邮件管理器]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [群发邮件营销]
     */
    public void setMassMailingCampaignId(Long massMailingCampaignId){
        this.massMailingCampaignId = massMailingCampaignId ;
        this.modify("mass_mailing_campaign_id",massMailingCampaignId);
    }

    /**
     * 设置 [营销]
     */
    public void setCampaignId(Long campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [媒体]
     */
    public void setMediumId(Long mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [主题]
     */
    public void setSourceId(Long sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


