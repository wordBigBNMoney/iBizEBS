package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_resume_line;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_resume_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_resume_line] 服务对象接口
 */
public interface IHr_resume_lineService extends IService<Hr_resume_line>{

    boolean create(Hr_resume_line et) ;
    void createBatch(List<Hr_resume_line> list) ;
    boolean update(Hr_resume_line et) ;
    void updateBatch(List<Hr_resume_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_resume_line get(Long key) ;
    Hr_resume_line getDraft(Hr_resume_line et) ;
    boolean checkKey(Hr_resume_line et) ;
    boolean save(Hr_resume_line et) ;
    void saveBatch(List<Hr_resume_line> list) ;
    Page<Hr_resume_line> searchDefault(Hr_resume_lineSearchContext context) ;
    List<Hr_resume_line> selectByEmployeeId(Long id);
    void removeByEmployeeId(Long id);
    List<Hr_resume_line> selectByLineTypeId(Long id);
    void removeByLineTypeId(Long id);
    List<Hr_resume_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_resume_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


