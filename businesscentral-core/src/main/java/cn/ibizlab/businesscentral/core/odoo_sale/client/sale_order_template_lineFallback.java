package cn.ibizlab.businesscentral.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_template_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sale_order_template_line] 服务对象接口
 */
@Component
public class sale_order_template_lineFallback implements sale_order_template_lineFeignClient{


    public Sale_order_template_line update(Long id, Sale_order_template_line sale_order_template_line){
            return null;
     }
    public Boolean updateBatch(List<Sale_order_template_line> sale_order_template_lines){
            return false;
     }


    public Page<Sale_order_template_line> search(Sale_order_template_lineSearchContext context){
            return null;
     }


    public Sale_order_template_line get(Long id){
            return null;
     }


    public Sale_order_template_line create(Sale_order_template_line sale_order_template_line){
            return null;
     }
    public Boolean createBatch(List<Sale_order_template_line> sale_order_template_lines){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Sale_order_template_line> select(){
            return null;
     }

    public Sale_order_template_line getDraft(){
            return null;
    }



    public Boolean checkKey(Sale_order_template_line sale_order_template_line){
            return false;
     }


    public Boolean save(Sale_order_template_line sale_order_template_line){
            return false;
     }
    public Boolean saveBatch(List<Sale_order_template_line> sale_order_template_lines){
            return false;
     }

    public Page<Sale_order_template_line> searchDefault(Sale_order_template_lineSearchContext context){
            return null;
     }


}
