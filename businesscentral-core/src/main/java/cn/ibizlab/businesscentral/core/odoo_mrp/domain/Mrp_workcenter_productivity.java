package cn.ibizlab.businesscentral.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[工作中心生产力日志]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MRP_WORKCENTER_PRODUCTIVITY",resultMap = "Mrp_workcenter_productivityResultMap")
public class Mrp_workcenter_productivity extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 时长
     */
    @TableField(value = "duration")
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;
    /**
     * 结束日期
     */
    @DEField(name = "date_end")
    @TableField(value = "date_end")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_end")
    private Timestamp dateEnd;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 开始日期
     */
    @DEField(name = "date_start")
    @TableField(value = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_start")
    private Timestamp dateStart;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 工作中心
     */
    @TableField(exist = false)
    @JSONField(name = "workcenter_id_text")
    @JsonProperty("workcenter_id_text")
    private String workcenterIdText;
    /**
     * 用户
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 有效性类别
     */
    @TableField(exist = false)
    @JSONField(name = "loss_type")
    @JsonProperty("loss_type")
    private String lossType;
    /**
     * 工单
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;
    /**
     * 损失原因
     */
    @TableField(exist = false)
    @JSONField(name = "loss_id_text")
    @JsonProperty("loss_id_text")
    private String lossIdText;
    /**
     * 制造订单
     */
    @TableField(exist = false)
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Long productionId;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 用户
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 工单
     */
    @DEField(name = "workorder_id")
    @TableField(value = "workorder_id")
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Long workorderId;
    /**
     * 工作中心
     */
    @DEField(name = "workcenter_id")
    @TableField(value = "workcenter_id")
    @JSONField(name = "workcenter_id")
    @JsonProperty("workcenter_id")
    private Long workcenterId;
    /**
     * 损失原因
     */
    @DEField(name = "loss_id")
    @TableField(value = "loss_id")
    @JSONField(name = "loss_id")
    @JsonProperty("loss_id")
    private Long lossId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss odooLoss;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter odooWorkcenter;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder odooWorkorder;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [时长]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [结束日期]
     */
    public void setDateEnd(Timestamp dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatDateEnd(){
        if (this.dateEnd == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateEnd);
    }
    /**
     * 设置 [开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatDateStart(){
        if (this.dateStart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateStart);
    }
    /**
     * 设置 [用户]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [工单]
     */
    public void setWorkorderId(Long workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }

    /**
     * 设置 [工作中心]
     */
    public void setWorkcenterId(Long workcenterId){
        this.workcenterId = workcenterId ;
        this.modify("workcenter_id",workcenterId);
    }

    /**
     * 设置 [损失原因]
     */
    public void setLossId(Long lossId){
        this.lossId = lossId ;
        this.modify("loss_id",lossId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


