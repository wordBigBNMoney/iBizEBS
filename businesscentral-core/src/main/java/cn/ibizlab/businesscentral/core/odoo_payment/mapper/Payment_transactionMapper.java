package cn.ibizlab.businesscentral.core.odoo_payment.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_transactionSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Payment_transactionMapper extends BaseMapper<Payment_transaction>{

    Page<Payment_transaction> searchDefault(IPage page, @Param("srf") Payment_transactionSearchContext context, @Param("ew") Wrapper<Payment_transaction> wrapper) ;
    @Override
    Payment_transaction selectById(Serializable id);
    @Override
    int insert(Payment_transaction entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Payment_transaction entity);
    @Override
    int update(@Param(Constants.ENTITY) Payment_transaction entity, @Param("ew") Wrapper<Payment_transaction> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Payment_transaction> selectByPaymentId(@Param("id") Serializable id) ;

    List<Payment_transaction> selectByAcquirerId(@Param("id") Serializable id) ;

    List<Payment_transaction> selectByPaymentTokenId(@Param("id") Serializable id) ;

    List<Payment_transaction> selectByPartnerCountryId(@Param("id") Serializable id) ;

    List<Payment_transaction> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Payment_transaction> selectByPartnerId(@Param("id") Serializable id) ;

    List<Payment_transaction> selectByCreateUid(@Param("id") Serializable id) ;

    List<Payment_transaction> selectByWriteUid(@Param("id") Serializable id) ;


}
