package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_tax_templateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_tax_template] 服务对象接口
 */
@Component
public class account_tax_templateFallback implements account_tax_templateFeignClient{

    public Account_tax_template update(Long id, Account_tax_template account_tax_template){
            return null;
     }
    public Boolean updateBatch(List<Account_tax_template> account_tax_templates){
            return false;
     }


    public Account_tax_template create(Account_tax_template account_tax_template){
            return null;
     }
    public Boolean createBatch(List<Account_tax_template> account_tax_templates){
            return false;
     }

    public Account_tax_template get(Long id){
            return null;
     }


    public Page<Account_tax_template> search(Account_tax_templateSearchContext context){
            return null;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Account_tax_template> select(){
            return null;
     }

    public Account_tax_template getDraft(){
            return null;
    }



    public Boolean checkKey(Account_tax_template account_tax_template){
            return false;
     }


    public Boolean save(Account_tax_template account_tax_template){
            return false;
     }
    public Boolean saveBatch(List<Account_tax_template> account_tax_templates){
            return false;
     }

    public Page<Account_tax_template> searchDefault(Account_tax_templateSearchContext context){
            return null;
     }


}
