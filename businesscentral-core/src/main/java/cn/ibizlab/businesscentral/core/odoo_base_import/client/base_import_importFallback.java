package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_import;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_importSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_import] 服务对象接口
 */
@Component
public class base_import_importFallback implements base_import_importFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Base_import_import update(Long id, Base_import_import base_import_import){
            return null;
     }
    public Boolean updateBatch(List<Base_import_import> base_import_imports){
            return false;
     }


    public Page<Base_import_import> search(Base_import_importSearchContext context){
            return null;
     }


    public Base_import_import create(Base_import_import base_import_import){
            return null;
     }
    public Boolean createBatch(List<Base_import_import> base_import_imports){
            return false;
     }

    public Base_import_import get(Long id){
            return null;
     }





    public Page<Base_import_import> select(){
            return null;
     }

    public Base_import_import getDraft(){
            return null;
    }



    public Boolean checkKey(Base_import_import base_import_import){
            return false;
     }


    public Boolean save(Base_import_import base_import_import){
            return false;
     }
    public Boolean saveBatch(List<Base_import_import> base_import_imports){
            return false;
     }

    public Page<Base_import_import> searchDefault(Base_import_importSearchContext context){
            return null;
     }


}
