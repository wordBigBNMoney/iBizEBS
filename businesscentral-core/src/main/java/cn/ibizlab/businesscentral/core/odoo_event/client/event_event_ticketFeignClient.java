package cn.ibizlab.businesscentral.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_event_ticketSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[event_event_ticket] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-event:odoo-event}", contextId = "event-event-ticket", fallback = event_event_ticketFallback.class)
public interface event_event_ticketFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets")
    Event_event_ticket create(@RequestBody Event_event_ticket event_event_ticket);

    @RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/batch")
    Boolean createBatch(@RequestBody List<Event_event_ticket> event_event_tickets);


    @RequestMapping(method = RequestMethod.GET, value = "/event_event_tickets/{id}")
    Event_event_ticket get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/event_event_tickets/{id}")
    Event_event_ticket update(@PathVariable("id") Long id,@RequestBody Event_event_ticket event_event_ticket);

    @RequestMapping(method = RequestMethod.PUT, value = "/event_event_tickets/batch")
    Boolean updateBatch(@RequestBody List<Event_event_ticket> event_event_tickets);




    @RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/search")
    Page<Event_event_ticket> search(@RequestBody Event_event_ticketSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/event_event_tickets/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/event_event_tickets/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/event_event_tickets/select")
    Page<Event_event_ticket> select();


    @RequestMapping(method = RequestMethod.GET, value = "/event_event_tickets/getdraft")
    Event_event_ticket getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/checkkey")
    Boolean checkKey(@RequestBody Event_event_ticket event_event_ticket);


    @RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/save")
    Boolean save(@RequestBody Event_event_ticket event_event_ticket);

    @RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/savebatch")
    Boolean saveBatch(@RequestBody List<Event_event_ticket> event_event_tickets);



    @RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/searchdefault")
    Page<Event_event_ticket> searchDefault(@RequestBody Event_event_ticketSearchContext context);


}
