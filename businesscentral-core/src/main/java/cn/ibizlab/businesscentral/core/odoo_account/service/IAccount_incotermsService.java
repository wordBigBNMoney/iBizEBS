package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_incotermsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_incoterms] 服务对象接口
 */
public interface IAccount_incotermsService extends IService<Account_incoterms>{

    boolean create(Account_incoterms et) ;
    void createBatch(List<Account_incoterms> list) ;
    boolean update(Account_incoterms et) ;
    void updateBatch(List<Account_incoterms> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_incoterms get(Long key) ;
    Account_incoterms getDraft(Account_incoterms et) ;
    boolean checkKey(Account_incoterms et) ;
    boolean save(Account_incoterms et) ;
    void saveBatch(List<Account_incoterms> list) ;
    Page<Account_incoterms> searchDefault(Account_incotermsSearchContext context) ;
    List<Account_incoterms> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_incoterms> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_incoterms> getAccountIncotermsByIds(List<Long> ids) ;
    List<Account_incoterms> getAccountIncotermsByEntities(List<Account_incoterms> entities) ;
}


