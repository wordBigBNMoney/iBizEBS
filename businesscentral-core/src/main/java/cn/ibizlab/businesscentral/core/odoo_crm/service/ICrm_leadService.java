package cn.ibizlab.businesscentral.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_leadSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Crm_lead] 服务对象接口
 */
public interface ICrm_leadService extends IService<Crm_lead>{

    boolean create(Crm_lead et) ;
    void createBatch(List<Crm_lead> list) ;
    boolean update(Crm_lead et) ;
    void updateBatch(List<Crm_lead> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Crm_lead get(Long key) ;
    Crm_lead getDraft(Crm_lead et) ;
    boolean checkKey(Crm_lead et) ;
    boolean save(Crm_lead et) ;
    void saveBatch(List<Crm_lead> list) ;
    Page<Crm_lead> searchDefault(Crm_leadSearchContext context) ;
    List<Crm_lead> selectByLostReason(Long id);
    void resetByLostReason(Long id);
    void resetByLostReason(Collection<Long> ids);
    void removeByLostReason(Long id);
    List<Crm_lead> selectByStageId(Long id);
    List<Crm_lead> selectByStageId(Collection<Long> ids);
    void removeByStageId(Long id);
    List<Crm_lead> selectByTeamId(Long id);
    void resetByTeamId(Long id);
    void resetByTeamId(Collection<Long> ids);
    void removeByTeamId(Long id);
    List<Crm_lead> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Crm_lead> selectByStateId(Long id);
    void resetByStateId(Long id);
    void resetByStateId(Collection<Long> ids);
    void removeByStateId(Long id);
    List<Crm_lead> selectByCountryId(Long id);
    void resetByCountryId(Long id);
    void resetByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Crm_lead> selectByTitle(Long id);
    void resetByTitle(Long id);
    void resetByTitle(Collection<Long> ids);
    void removeByTitle(Long id);
    List<Crm_lead> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Crm_lead> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Crm_lead> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Crm_lead> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Crm_lead> selectByCampaignId(Long id);
    void resetByCampaignId(Long id);
    void resetByCampaignId(Collection<Long> ids);
    void removeByCampaignId(Long id);
    List<Crm_lead> selectByMediumId(Long id);
    void resetByMediumId(Long id);
    void resetByMediumId(Collection<Long> ids);
    void removeByMediumId(Long id);
    List<Crm_lead> selectBySourceId(Long id);
    void resetBySourceId(Long id);
    void resetBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Crm_lead> getCrmLeadByIds(List<Long> ids) ;
    List<Crm_lead> getCrmLeadByEntities(List<Crm_lead> entities) ;
}


