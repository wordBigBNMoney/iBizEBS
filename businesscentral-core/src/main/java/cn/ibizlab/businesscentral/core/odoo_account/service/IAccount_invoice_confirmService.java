package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_confirmSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_invoice_confirm] 服务对象接口
 */
public interface IAccount_invoice_confirmService extends IService<Account_invoice_confirm>{

    boolean create(Account_invoice_confirm et) ;
    void createBatch(List<Account_invoice_confirm> list) ;
    boolean update(Account_invoice_confirm et) ;
    void updateBatch(List<Account_invoice_confirm> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_invoice_confirm get(Long key) ;
    Account_invoice_confirm getDraft(Account_invoice_confirm et) ;
    boolean checkKey(Account_invoice_confirm et) ;
    boolean save(Account_invoice_confirm et) ;
    void saveBatch(List<Account_invoice_confirm> list) ;
    Page<Account_invoice_confirm> searchDefault(Account_invoice_confirmSearchContext context) ;
    List<Account_invoice_confirm> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_invoice_confirm> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_invoice_confirm> getAccountInvoiceConfirmByIds(List<Long> ids) ;
    List<Account_invoice_confirm> getAccountInvoiceConfirmByEntities(List<Account_invoice_confirm> entities) ;
}


