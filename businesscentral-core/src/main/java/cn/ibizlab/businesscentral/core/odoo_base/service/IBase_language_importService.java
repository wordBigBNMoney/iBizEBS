package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_importSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_language_import] 服务对象接口
 */
public interface IBase_language_importService extends IService<Base_language_import>{

    boolean create(Base_language_import et) ;
    void createBatch(List<Base_language_import> list) ;
    boolean update(Base_language_import et) ;
    void updateBatch(List<Base_language_import> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_language_import get(Long key) ;
    Base_language_import getDraft(Base_language_import et) ;
    boolean checkKey(Base_language_import et) ;
    boolean save(Base_language_import et) ;
    void saveBatch(List<Base_language_import> list) ;
    Page<Base_language_import> searchDefault(Base_language_importSearchContext context) ;
    List<Base_language_import> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_language_import> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_language_import> getBaseLanguageImportByIds(List<Long> ids) ;
    List<Base_language_import> getBaseLanguageImportByEntities(List<Base_language_import> entities) ;
}


