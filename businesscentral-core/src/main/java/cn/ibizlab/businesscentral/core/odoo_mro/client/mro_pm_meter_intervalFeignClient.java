package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_pm_meter_interval] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mro:odoo-mro}", contextId = "mro-pm-meter-interval", fallback = mro_pm_meter_intervalFallback.class)
public interface mro_pm_meter_intervalFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_intervals/{id}")
    Mro_pm_meter_interval update(@PathVariable("id") Long id,@RequestBody Mro_pm_meter_interval mro_pm_meter_interval);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_intervals/batch")
    Boolean updateBatch(@RequestBody List<Mro_pm_meter_interval> mro_pm_meter_intervals);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_intervals/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_intervals/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals")
    Mro_pm_meter_interval create(@RequestBody Mro_pm_meter_interval mro_pm_meter_interval);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/batch")
    Boolean createBatch(@RequestBody List<Mro_pm_meter_interval> mro_pm_meter_intervals);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/search")
    Page<Mro_pm_meter_interval> search(@RequestBody Mro_pm_meter_intervalSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_intervals/{id}")
    Mro_pm_meter_interval get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_intervals/select")
    Page<Mro_pm_meter_interval> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_intervals/getdraft")
    Mro_pm_meter_interval getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/checkkey")
    Boolean checkKey(@RequestBody Mro_pm_meter_interval mro_pm_meter_interval);


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/save")
    Boolean save(@RequestBody Mro_pm_meter_interval mro_pm_meter_interval);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/savebatch")
    Boolean saveBatch(@RequestBody List<Mro_pm_meter_interval> mro_pm_meter_intervals);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/searchdefault")
    Page<Mro_pm_meter_interval> searchDefault(@RequestBody Mro_pm_meter_intervalSearchContext context);


}
