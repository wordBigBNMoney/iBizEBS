package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_distributionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_analytic_distribution] 服务对象接口
 */
public interface IAccount_analytic_distributionService extends IService<Account_analytic_distribution>{

    boolean create(Account_analytic_distribution et) ;
    void createBatch(List<Account_analytic_distribution> list) ;
    boolean update(Account_analytic_distribution et) ;
    void updateBatch(List<Account_analytic_distribution> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_analytic_distribution get(Long key) ;
    Account_analytic_distribution getDraft(Account_analytic_distribution et) ;
    boolean checkKey(Account_analytic_distribution et) ;
    boolean save(Account_analytic_distribution et) ;
    void saveBatch(List<Account_analytic_distribution> list) ;
    Page<Account_analytic_distribution> searchDefault(Account_analytic_distributionSearchContext context) ;
    List<Account_analytic_distribution> selectByAccountId(Long id);
    void resetByAccountId(Long id);
    void resetByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_analytic_distribution> selectByTagId(Long id);
    void resetByTagId(Long id);
    void resetByTagId(Collection<Long> ids);
    void removeByTagId(Long id);
    List<Account_analytic_distribution> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_analytic_distribution> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_analytic_distribution> getAccountAnalyticDistributionByIds(List<Long> ids) ;
    List<Account_analytic_distribution> getAccountAnalyticDistributionByEntities(List<Account_analytic_distribution> entities) ;
}


