package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_journalSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_journal] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-journal", fallback = account_journalFallback.class)
public interface account_journalFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/account_journals/{id}")
    Account_journal get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_journals")
    Account_journal create(@RequestBody Account_journal account_journal);

    @RequestMapping(method = RequestMethod.POST, value = "/account_journals/batch")
    Boolean createBatch(@RequestBody List<Account_journal> account_journals);




    @RequestMapping(method = RequestMethod.POST, value = "/account_journals/search")
    Page<Account_journal> search(@RequestBody Account_journalSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_journals/{id}")
    Account_journal update(@PathVariable("id") Long id,@RequestBody Account_journal account_journal);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_journals/batch")
    Boolean updateBatch(@RequestBody List<Account_journal> account_journals);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_journals/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_journals/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/account_journals/select")
    Page<Account_journal> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_journals/getdraft")
    Account_journal getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_journals/checkkey")
    Boolean checkKey(@RequestBody Account_journal account_journal);


    @RequestMapping(method = RequestMethod.POST, value = "/account_journals/save")
    Boolean save(@RequestBody Account_journal account_journal);

    @RequestMapping(method = RequestMethod.POST, value = "/account_journals/savebatch")
    Boolean saveBatch(@RequestBody List<Account_journal> account_journals);



    @RequestMapping(method = RequestMethod.POST, value = "/account_journals/searchdefault")
    Page<Account_journal> searchDefault(@RequestBody Account_journalSearchContext context);


}
