package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_supplierinfo;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_supplierinfoSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_supplierinfo] 服务对象接口
 */
@Component
public class product_supplierinfoFallback implements product_supplierinfoFeignClient{


    public Product_supplierinfo update(Long id, Product_supplierinfo product_supplierinfo){
            return null;
     }
    public Boolean updateBatch(List<Product_supplierinfo> product_supplierinfos){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Page<Product_supplierinfo> search(Product_supplierinfoSearchContext context){
            return null;
     }


    public Product_supplierinfo create(Product_supplierinfo product_supplierinfo){
            return null;
     }
    public Boolean createBatch(List<Product_supplierinfo> product_supplierinfos){
            return false;
     }

    public Product_supplierinfo get(Long id){
            return null;
     }


    public Page<Product_supplierinfo> select(){
            return null;
     }

    public Product_supplierinfo getDraft(){
            return null;
    }



    public Boolean checkKey(Product_supplierinfo product_supplierinfo){
            return false;
     }


    public Boolean save(Product_supplierinfo product_supplierinfo){
            return false;
     }
    public Boolean saveBatch(List<Product_supplierinfo> product_supplierinfos){
            return false;
     }

    public Page<Product_supplierinfo> searchDefault(Product_supplierinfoSearchContext context){
            return null;
     }


}
