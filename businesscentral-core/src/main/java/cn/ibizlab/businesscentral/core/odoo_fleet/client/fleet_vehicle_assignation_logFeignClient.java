package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_assignation_log] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-vehicle-assignation-log", fallback = fleet_vehicle_assignation_logFallback.class)
public interface fleet_vehicle_assignation_logFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/search")
    Page<Fleet_vehicle_assignation_log> search(@RequestBody Fleet_vehicle_assignation_logSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_assignation_logs/{id}")
    Fleet_vehicle_assignation_log update(@PathVariable("id") Long id,@RequestBody Fleet_vehicle_assignation_log fleet_vehicle_assignation_log);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_assignation_logs/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_assignation_log> fleet_vehicle_assignation_logs);



    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_assignation_logs/{id}")
    Fleet_vehicle_assignation_log get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs")
    Fleet_vehicle_assignation_log create(@RequestBody Fleet_vehicle_assignation_log fleet_vehicle_assignation_log);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_assignation_log> fleet_vehicle_assignation_logs);


    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_assignation_logs/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_assignation_logs/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_assignation_logs/select")
    Page<Fleet_vehicle_assignation_log> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_assignation_logs/getdraft")
    Fleet_vehicle_assignation_log getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/checkkey")
    Boolean checkKey(@RequestBody Fleet_vehicle_assignation_log fleet_vehicle_assignation_log);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/save")
    Boolean save(@RequestBody Fleet_vehicle_assignation_log fleet_vehicle_assignation_log);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_vehicle_assignation_log> fleet_vehicle_assignation_logs);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/searchdefault")
    Page<Fleet_vehicle_assignation_log> searchDefault(@RequestBody Fleet_vehicle_assignation_logSearchContext context);


}
