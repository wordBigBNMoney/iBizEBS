package cn.ibizlab.businesscentral.core.odoo_website.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_menu;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_menuSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Website_menuMapper extends BaseMapper<Website_menu>{

    Page<Website_menu> searchDefault(IPage page, @Param("srf") Website_menuSearchContext context, @Param("ew") Wrapper<Website_menu> wrapper) ;
    @Override
    Website_menu selectById(Serializable id);
    @Override
    int insert(Website_menu entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Website_menu entity);
    @Override
    int update(@Param(Constants.ENTITY) Website_menu entity, @Param("ew") Wrapper<Website_menu> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Website_menu> selectByCreateUid(@Param("id") Serializable id) ;

    List<Website_menu> selectByWriteUid(@Param("id") Serializable id) ;

    List<Website_menu> selectByParentId(@Param("id") Serializable id) ;

    List<Website_menu> selectByPageId(@Param("id") Serializable id) ;


}
