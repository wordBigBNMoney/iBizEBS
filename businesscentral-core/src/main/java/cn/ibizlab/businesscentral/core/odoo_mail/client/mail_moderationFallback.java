package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_moderationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_moderation] 服务对象接口
 */
@Component
public class mail_moderationFallback implements mail_moderationFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Mail_moderation> search(Mail_moderationSearchContext context){
            return null;
     }


    public Mail_moderation update(Long id, Mail_moderation mail_moderation){
            return null;
     }
    public Boolean updateBatch(List<Mail_moderation> mail_moderations){
            return false;
     }


    public Mail_moderation get(Long id){
            return null;
     }


    public Mail_moderation create(Mail_moderation mail_moderation){
            return null;
     }
    public Boolean createBatch(List<Mail_moderation> mail_moderations){
            return false;
     }



    public Page<Mail_moderation> select(){
            return null;
     }

    public Mail_moderation getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_moderation mail_moderation){
            return false;
     }


    public Boolean save(Mail_moderation mail_moderation){
            return false;
     }
    public Boolean saveBatch(List<Mail_moderation> mail_moderations){
            return false;
     }

    public Page<Mail_moderation> searchDefault(Mail_moderationSearchContext context){
            return null;
     }


}
