package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_orderSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Purchase_order] 服务对象接口
 */
public interface IPurchase_orderService extends IService<Purchase_order>{

    boolean create(Purchase_order et) ;
    void createBatch(List<Purchase_order> list) ;
    boolean update(Purchase_order et) ;
    void updateBatch(List<Purchase_order> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Purchase_order get(Long key) ;
    Purchase_order getDraft(Purchase_order et) ;
    Purchase_order button_approve(Purchase_order et) ;
    Purchase_order button_cancel(Purchase_order et) ;
    Purchase_order button_confirm(Purchase_order et) ;
    Purchase_order button_done(Purchase_order et) ;
    Purchase_order button_unlock(Purchase_order et) ;
    Purchase_order calc_amount(Purchase_order et) ;
    boolean checkKey(Purchase_order et) ;
    Purchase_order get_name(Purchase_order et) ;
    boolean save(Purchase_order et) ;
    void saveBatch(List<Purchase_order> list) ;
    Page<Purchase_order> searchDefault(Purchase_orderSearchContext context) ;
    Page<Purchase_order> searchMaster(Purchase_orderSearchContext context) ;
    Page<Purchase_order> searchOrder(Purchase_orderSearchContext context) ;
    List<Purchase_order> selectByRequisitionId(Long id);
    void removeByRequisitionId(Long id);
    List<Purchase_order> selectByPartnerId(Long id);
    void removeByPartnerId(Long id);
    List<Purchase_order> selectByFiscalPositionId(Long id);
    void resetByFiscalPositionId(Long id);
    void resetByFiscalPositionId(Collection<Long> ids);
    void removeByFiscalPositionId(Long id);
    List<Purchase_order> selectByIncotermId(Long id);
    void resetByIncotermId(Long id);
    void resetByIncotermId(Collection<Long> ids);
    void removeByIncotermId(Long id);
    List<Purchase_order> selectByPaymentTermId(Long id);
    void resetByPaymentTermId(Long id);
    void resetByPaymentTermId(Collection<Long> ids);
    void removeByPaymentTermId(Long id);
    List<Purchase_order> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Purchase_order> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Purchase_order> selectByDestAddressId(Long id);
    void resetByDestAddressId(Long id);
    void resetByDestAddressId(Collection<Long> ids);
    void removeByDestAddressId(Long id);
    List<Purchase_order> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Purchase_order> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Purchase_order> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Purchase_order> selectByPickingTypeId(Long id);
    void resetByPickingTypeId(Long id);
    void resetByPickingTypeId(Collection<Long> ids);
    void removeByPickingTypeId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Purchase_order> getPurchaseOrderByIds(List<Long> ids) ;
    List<Purchase_order> getPurchaseOrderByEntities(List<Purchase_order> entities) ;
}


