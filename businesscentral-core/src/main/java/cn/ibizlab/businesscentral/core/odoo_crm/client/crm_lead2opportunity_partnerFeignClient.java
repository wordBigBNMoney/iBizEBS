package cn.ibizlab.businesscentral.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_lead2opportunity_partner] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-crm:odoo-crm}", contextId = "crm-lead2opportunity-partner", fallback = crm_lead2opportunity_partnerFallback.class)
public interface crm_lead2opportunity_partnerFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partners/{id}")
    Crm_lead2opportunity_partner update(@PathVariable("id") Long id,@RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partners/batch")
    Boolean updateBatch(@RequestBody List<Crm_lead2opportunity_partner> crm_lead2opportunity_partners);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/search")
    Page<Crm_lead2opportunity_partner> search(@RequestBody Crm_lead2opportunity_partnerSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/{id}")
    Crm_lead2opportunity_partner get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners")
    Crm_lead2opportunity_partner create(@RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/batch")
    Boolean createBatch(@RequestBody List<Crm_lead2opportunity_partner> crm_lead2opportunity_partners);



    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partners/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partners/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/select")
    Page<Crm_lead2opportunity_partner> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/getdraft")
    Crm_lead2opportunity_partner getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/checkkey")
    Boolean checkKey(@RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/save")
    Boolean save(@RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/savebatch")
    Boolean saveBatch(@RequestBody List<Crm_lead2opportunity_partner> crm_lead2opportunity_partners);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/searchdefault")
    Page<Crm_lead2opportunity_partner> searchDefault(@RequestBody Crm_lead2opportunity_partnerSearchContext context);


}
