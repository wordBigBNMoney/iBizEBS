package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char_states;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_char_statesSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_char_states] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base-import:odoo-base-import}", contextId = "base-import-tests-models-char-states", fallback = base_import_tests_models_char_statesFallback.class)
public interface base_import_tests_models_char_statesFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states")
    Base_import_tests_models_char_states create(@RequestBody Base_import_tests_models_char_states base_import_tests_models_char_states);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_char_states> base_import_tests_models_char_states);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_states/{id}")
    Base_import_tests_models_char_states update(@PathVariable("id") Long id,@RequestBody Base_import_tests_models_char_states base_import_tests_models_char_states);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_states/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_char_states> base_import_tests_models_char_states);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_states/{id}")
    Base_import_tests_models_char_states get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_states/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_states/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/search")
    Page<Base_import_tests_models_char_states> search(@RequestBody Base_import_tests_models_char_statesSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_states/select")
    Page<Base_import_tests_models_char_states> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_states/getdraft")
    Base_import_tests_models_char_states getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/checkkey")
    Boolean checkKey(@RequestBody Base_import_tests_models_char_states base_import_tests_models_char_states);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/save")
    Boolean save(@RequestBody Base_import_tests_models_char_states base_import_tests_models_char_states);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/savebatch")
    Boolean saveBatch(@RequestBody List<Base_import_tests_models_char_states> base_import_tests_models_char_states);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/searchdefault")
    Page<Base_import_tests_models_char_states> searchDefault(@RequestBody Base_import_tests_models_char_statesSearchContext context);


}
