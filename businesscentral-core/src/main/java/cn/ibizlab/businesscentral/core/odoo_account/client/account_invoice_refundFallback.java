package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_refund;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_refundSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice_refund] 服务对象接口
 */
@Component
public class account_invoice_refundFallback implements account_invoice_refundFeignClient{


    public Account_invoice_refund get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_invoice_refund> search(Account_invoice_refundSearchContext context){
            return null;
     }


    public Account_invoice_refund update(Long id, Account_invoice_refund account_invoice_refund){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice_refund> account_invoice_refunds){
            return false;
     }


    public Account_invoice_refund create(Account_invoice_refund account_invoice_refund){
            return null;
     }
    public Boolean createBatch(List<Account_invoice_refund> account_invoice_refunds){
            return false;
     }


    public Page<Account_invoice_refund> select(){
            return null;
     }

    public Account_invoice_refund getDraft(){
            return null;
    }



    public Boolean checkKey(Account_invoice_refund account_invoice_refund){
            return false;
     }


    public Boolean save(Account_invoice_refund account_invoice_refund){
            return false;
     }
    public Boolean saveBatch(List<Account_invoice_refund> account_invoice_refunds){
            return false;
     }

    public Page<Account_invoice_refund> searchDefault(Account_invoice_refundSearchContext context){
            return null;
     }


}
