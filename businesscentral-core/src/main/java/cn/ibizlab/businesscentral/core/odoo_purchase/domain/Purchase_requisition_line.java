package cn.ibizlab.businesscentral.core.odoo_purchase.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[采购申请行]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PURCHASE_REQUISITION_LINE",resultMap = "Purchase_requisition_lineResultMap")
public class Purchase_requisition_line extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数量
     */
    @DEField(name = "product_qty")
    @TableField(value = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date")
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 创建时间
     */
    @DEField(name = "create_date")
    @TableField(value = "create_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 安排的日期
     */
    @DEField(name = "schedule_date")
    @TableField(value = "schedule_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedule_date" , format="yyyy-MM-dd")
    @JsonProperty("schedule_date")
    private Timestamp scheduleDate;
    /**
     * 已订购数量
     */
    @TableField(exist = false)
    @JSONField(name = "qty_ordered")
    @JsonProperty("qty_ordered")
    private Integer qtyOrdered;
    /**
     * 价格
     */
    @DEField(name = "price_unit")
    @TableField(value = "price_unit")
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;
    /**
     * 公司名称
     */
    @TableField(exist = false)
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    private String companyName;
    /**
     * 采购申请
     */
    @TableField(exist = false)
    @JSONField(name = "requisition_name")
    @JsonProperty("requisition_name")
    private String requisitionName;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_name")
    @JsonProperty("product_uom_name")
    private String productUomName;
    /**
     * 说明
     */
    @TableField(exist = false)
    @JSONField(name = "move_dest_name")
    @JsonProperty("move_dest_name")
    private String moveDestName;
    /**
     * 分析账户
     */
    @TableField(exist = false)
    @JSONField(name = "account_analytic_name")
    @JsonProperty("account_analytic_name")
    private String accountAnalyticName;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    private String writeUname;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_name")
    @JsonProperty("product_name")
    private String productName;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    private String createUname;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid")
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 产品
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 单位
     */
    @DEField(name = "product_uom_id")
    @TableField(value = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Long productUomId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid")
    @TableField(value = "create_uid")
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 说明
     */
    @DEField(name = "move_dest_id")
    @TableField(value = "move_dest_id")
    @JSONField(name = "move_dest_id")
    @JsonProperty("move_dest_id")
    private Long moveDestId;
    /**
     * 分析账户
     */
    @DEField(name = "account_analytic_id")
    @TableField(value = "account_analytic_id")
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    private Long accountAnalyticId;
    /**
     * 采购申请
     */
    @DEField(name = "requisition_id")
    @TableField(value = "requisition_id")
    @JSONField(name = "requisition_id")
    @JsonProperty("requisition_id")
    private Long requisitionId;
    /**
     * 公司
     */
    @DEField(name = "company_id" , preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account accountanalytic;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product product;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition requisition;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company company;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move movedest;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom productuom;



    /**
     * 设置 [数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [最后更新时间]
     */
    public void setWriteDate(Timestamp writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 格式化日期 [最后更新时间]
     */
    public String formatWriteDate(){
        if (this.writeDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(writeDate);
    }
    /**
     * 设置 [创建时间]
     */
    public void setCreateDate(Timestamp createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 格式化日期 [创建时间]
     */
    public String formatCreateDate(){
        if (this.createDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(createDate);
    }
    /**
     * 设置 [安排的日期]
     */
    public void setScheduleDate(Timestamp scheduleDate){
        this.scheduleDate = scheduleDate ;
        this.modify("schedule_date",scheduleDate);
    }

    /**
     * 格式化日期 [安排的日期]
     */
    public String formatScheduleDate(){
        if (this.scheduleDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(scheduleDate);
    }
    /**
     * 设置 [价格]
     */
    public void setPriceUnit(Double priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [最后更新人]
     */
    public void setWriteUid(Long writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [产品]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [单位]
     */
    public void setProductUomId(Long productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [创建人]
     */
    public void setCreateUid(Long createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }

    /**
     * 设置 [说明]
     */
    public void setMoveDestId(Long moveDestId){
        this.moveDestId = moveDestId ;
        this.modify("move_dest_id",moveDestId);
    }

    /**
     * 设置 [分析账户]
     */
    public void setAccountAnalyticId(Long accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }

    /**
     * 设置 [采购申请]
     */
    public void setRequisitionId(Long requisitionId){
        this.requisitionId = requisitionId ;
        this.modify("requisition_id",requisitionId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


