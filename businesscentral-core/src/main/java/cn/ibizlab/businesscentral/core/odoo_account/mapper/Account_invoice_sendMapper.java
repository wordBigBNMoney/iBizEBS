package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_sendSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_invoice_sendMapper extends BaseMapper<Account_invoice_send>{

    Page<Account_invoice_send> searchDefault(IPage page, @Param("srf") Account_invoice_sendSearchContext context, @Param("ew") Wrapper<Account_invoice_send> wrapper) ;
    @Override
    Account_invoice_send selectById(Serializable id);
    @Override
    int insert(Account_invoice_send entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_invoice_send entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_invoice_send entity, @Param("ew") Wrapper<Account_invoice_send> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_invoice_send> selectByComposerId(@Param("id") Serializable id) ;

    List<Account_invoice_send> selectByTemplateId(@Param("id") Serializable id) ;

    List<Account_invoice_send> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_invoice_send> selectByWriteUid(@Param("id") Serializable id) ;


}
