package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_picking_typeSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_picking_typeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[拣货类型] 服务对象接口实现
 */
@Slf4j
@Service("Stock_picking_typeServiceImpl")
public class Stock_picking_typeServiceImpl extends EBSServiceImpl<Stock_picking_typeMapper, Stock_picking_type> implements IStock_picking_typeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService mrpBomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService purchaseRequisitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;

    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService stockRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.picking.type" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_picking_type et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_picking_typeService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_picking_type> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_picking_type et) {
        Stock_picking_type old = new Stock_picking_type() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_picking_typeService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_picking_typeService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_picking_type> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        mrpBomService.resetByPickingTypeId(key);
        mrpProductionService.resetByPickingTypeId(key);
        purchaseOrderService.resetByPickingTypeId(key);
        stockMoveService.resetByPickingTypeId(key);
        stockPickingTypeService.resetByReturnPickingTypeId(key);
        stockPickingService.resetByPickingTypeId(key);
        stockRuleService.resetByPickingTypeId(key);
        stockWarehouseService.resetByIntTypeId(key);
        stockWarehouseService.resetByInTypeId(key);
        stockWarehouseService.resetByManuTypeId(key);
        stockWarehouseService.resetByOutTypeId(key);
        stockWarehouseService.resetByPackTypeId(key);
        stockWarehouseService.resetByPbmTypeId(key);
        stockWarehouseService.resetByPickTypeId(key);
        stockWarehouseService.resetBySamTypeId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        mrpBomService.resetByPickingTypeId(idList);
        mrpProductionService.resetByPickingTypeId(idList);
        purchaseOrderService.resetByPickingTypeId(idList);
        stockMoveService.resetByPickingTypeId(idList);
        stockPickingTypeService.resetByReturnPickingTypeId(idList);
        stockPickingService.resetByPickingTypeId(idList);
        stockRuleService.resetByPickingTypeId(idList);
        stockWarehouseService.resetByIntTypeId(idList);
        stockWarehouseService.resetByInTypeId(idList);
        stockWarehouseService.resetByManuTypeId(idList);
        stockWarehouseService.resetByOutTypeId(idList);
        stockWarehouseService.resetByPackTypeId(idList);
        stockWarehouseService.resetByPbmTypeId(idList);
        stockWarehouseService.resetByPickTypeId(idList);
        stockWarehouseService.resetBySamTypeId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_picking_type get(Long key) {
        Stock_picking_type et = getById(key);
        if(et==null){
            et=new Stock_picking_type();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_picking_type getDraft(Stock_picking_type et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_picking_type et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_picking_type et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_picking_type et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_picking_type> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_picking_type> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_picking_type> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_picking_type>().eq("create_uid",id));
    }

	@Override
    public List<Stock_picking_type> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_picking_type>().eq("write_uid",id));
    }

	@Override
    public List<Stock_picking_type> selectByDefaultLocationDestId(Long id) {
        return baseMapper.selectByDefaultLocationDestId(id);
    }
    @Override
    public void resetByDefaultLocationDestId(Long id) {
        this.update(new UpdateWrapper<Stock_picking_type>().set("default_location_dest_id",null).eq("default_location_dest_id",id));
    }

    @Override
    public void resetByDefaultLocationDestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking_type>().set("default_location_dest_id",null).in("default_location_dest_id",ids));
    }

    @Override
    public void removeByDefaultLocationDestId(Long id) {
        this.remove(new QueryWrapper<Stock_picking_type>().eq("default_location_dest_id",id));
    }

	@Override
    public List<Stock_picking_type> selectByDefaultLocationSrcId(Long id) {
        return baseMapper.selectByDefaultLocationSrcId(id);
    }
    @Override
    public void resetByDefaultLocationSrcId(Long id) {
        this.update(new UpdateWrapper<Stock_picking_type>().set("default_location_src_id",null).eq("default_location_src_id",id));
    }

    @Override
    public void resetByDefaultLocationSrcId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking_type>().set("default_location_src_id",null).in("default_location_src_id",ids));
    }

    @Override
    public void removeByDefaultLocationSrcId(Long id) {
        this.remove(new QueryWrapper<Stock_picking_type>().eq("default_location_src_id",id));
    }

	@Override
    public List<Stock_picking_type> selectByReturnPickingTypeId(Long id) {
        return baseMapper.selectByReturnPickingTypeId(id);
    }
    @Override
    public void resetByReturnPickingTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_picking_type>().set("return_picking_type_id",null).eq("return_picking_type_id",id));
    }

    @Override
    public void resetByReturnPickingTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking_type>().set("return_picking_type_id",null).in("return_picking_type_id",ids));
    }

    @Override
    public void removeByReturnPickingTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_picking_type>().eq("return_picking_type_id",id));
    }

	@Override
    public List<Stock_picking_type> selectByWarehouseId(Long id) {
        return baseMapper.selectByWarehouseId(id);
    }
    @Override
    public void removeByWarehouseId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Stock_picking_type>().in("warehouse_id",ids));
    }

    @Override
    public void removeByWarehouseId(Long id) {
        this.remove(new QueryWrapper<Stock_picking_type>().eq("warehouse_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_picking_type> searchDefault(Stock_picking_typeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_picking_type> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_picking_type>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_picking_type et){
        //实体关系[DER1N_STOCK_PICKING_TYPE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_PICKING_TYPE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_PICKING_TYPE__STOCK_LOCATION__DEFAULT_LOCATION_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getDefaultLocationDestId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooDefaultLocationDest=et.getOdooDefaultLocationDest();
            if(ObjectUtils.isEmpty(odooDefaultLocationDest)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getDefaultLocationDestId());
                et.setOdooDefaultLocationDest(majorEntity);
                odooDefaultLocationDest=majorEntity;
            }
            et.setDefaultLocationDestIdText(odooDefaultLocationDest.getName());
        }
        //实体关系[DER1N_STOCK_PICKING_TYPE__STOCK_LOCATION__DEFAULT_LOCATION_SRC_ID]
        if(!ObjectUtils.isEmpty(et.getDefaultLocationSrcId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooDefaultLocationSrc=et.getOdooDefaultLocationSrc();
            if(ObjectUtils.isEmpty(odooDefaultLocationSrc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getDefaultLocationSrcId());
                et.setOdooDefaultLocationSrc(majorEntity);
                odooDefaultLocationSrc=majorEntity;
            }
            et.setDefaultLocationSrcIdText(odooDefaultLocationSrc.getName());
        }
        //实体关系[DER1N_STOCK_PICKING_TYPE__STOCK_PICKING_TYPE__RETURN_PICKING_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getReturnPickingTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooReturnPickingType=et.getOdooReturnPickingType();
            if(ObjectUtils.isEmpty(odooReturnPickingType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getReturnPickingTypeId());
                et.setOdooReturnPickingType(majorEntity);
                odooReturnPickingType=majorEntity;
            }
            et.setReturnPickingTypeIdText(odooReturnPickingType.getName());
        }
        //实体关系[DER1N_STOCK_PICKING_TYPE__STOCK_WAREHOUSE__WAREHOUSE_ID]
        if(!ObjectUtils.isEmpty(et.getWarehouseId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse=et.getOdooWarehouse();
            if(ObjectUtils.isEmpty(odooWarehouse)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getWarehouseId());
                et.setOdooWarehouse(majorEntity);
                odooWarehouse=majorEntity;
            }
            et.setWarehouseIdText(odooWarehouse.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_picking_type> getStockPickingTypeByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_picking_type> getStockPickingTypeByEntities(List<Stock_picking_type> entities) {
        List ids =new ArrayList();
        for(Stock_picking_type entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



