package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_blacklist;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_blacklistSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_blacklist] 服务对象接口
 */
public interface IMail_blacklistService extends IService<Mail_blacklist>{

    boolean create(Mail_blacklist et) ;
    void createBatch(List<Mail_blacklist> list) ;
    boolean update(Mail_blacklist et) ;
    void updateBatch(List<Mail_blacklist> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_blacklist get(Long key) ;
    Mail_blacklist getDraft(Mail_blacklist et) ;
    boolean checkKey(Mail_blacklist et) ;
    boolean save(Mail_blacklist et) ;
    void saveBatch(List<Mail_blacklist> list) ;
    Page<Mail_blacklist> searchDefault(Mail_blacklistSearchContext context) ;
    List<Mail_blacklist> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_blacklist> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_blacklist> getMailBlacklistByIds(List<Long> ids) ;
    List<Mail_blacklist> getMailBlacklistByEntities(List<Mail_blacklist> entities) ;
}


