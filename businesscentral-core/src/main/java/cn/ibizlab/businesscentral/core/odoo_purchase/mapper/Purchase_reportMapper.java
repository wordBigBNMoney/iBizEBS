package cn.ibizlab.businesscentral.core.odoo_purchase.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_reportSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Purchase_reportMapper extends BaseMapper<Purchase_report>{

    Page<Purchase_report> searchDefault(IPage page, @Param("srf") Purchase_reportSearchContext context, @Param("ew") Wrapper<Purchase_report> wrapper) ;
    @Override
    Purchase_report selectById(Serializable id);
    @Override
    int insert(Purchase_report entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Purchase_report entity);
    @Override
    int update(@Param(Constants.ENTITY) Purchase_report entity, @Param("ew") Wrapper<Purchase_report> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Purchase_report> selectByAccountAnalyticId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByFiscalPositionId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByCategoryId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByProductId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByProductTmplId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByCompanyId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByCountryId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByCommercialPartnerId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByPartnerId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByUserId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByPickingTypeId(@Param("id") Serializable id) ;

    List<Purchase_report> selectByProductUom(@Param("id") Serializable id) ;


}
