package cn.ibizlab.businesscentral.core.odoo_product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[产品属性]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PRODUCT_ATTRIBUTE_VALUE_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_REL",resultMap = "Product_attribute_value_product_template_attribute_line_relResultMap")
public class Product_attribute_value_product_template_attribute_line_rel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableField(exist = false)
    @JSONField(name = "id")
    @JsonProperty("id")
    private String id;
    /**
     * ID
     */
    @DEField(name = "product_template_attribute_line_id")
    @TableField(value = "product_template_attribute_line_id")
    @JSONField(name = "product_template_attribute_line_id")
    @JsonProperty("product_template_attribute_line_id")
    private Long productTemplateAttributeLineId;
    /**
     * ID
     */
    @DEField(name = "product_attribute_value_id")
    @TableField(value = "product_attribute_value_id")
    @JSONField(name = "product_attribute_value_id")
    @JsonProperty("product_attribute_value_id")
    private Long productAttributeValueId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute_value odooProductAttributeValue;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_line odooProductTemplateAttributeLine;



    /**
     * 设置 [ID]
     */
    public void setProductTemplateAttributeLineId(Long productTemplateAttributeLineId){
        this.productTemplateAttributeLineId = productTemplateAttributeLineId ;
        this.modify("product_template_attribute_line_id",productTemplateAttributeLineId);
    }

    /**
     * 设置 [ID]
     */
    public void setProductAttributeValueId(Long productAttributeValueId){
        this.productAttributeValueId = productAttributeValueId ;
        this.modify("product_attribute_value_id",productAttributeValueId);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


