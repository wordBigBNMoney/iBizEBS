package cn.ibizlab.businesscentral.core.odoo_hr.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_skill;
/**
 * 关系型数据实体[Hr_skill] 查询条件对象
 */
@Slf4j
@Data
public class Hr_skillSearchContext extends QueryWrapperContext<Hr_skill> {

	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_skill_type_name_eq;//[类型]
	public void setN_skill_type_name_eq(String n_skill_type_name_eq) {
        this.n_skill_type_name_eq = n_skill_type_name_eq;
        if(!ObjectUtils.isEmpty(this.n_skill_type_name_eq)){
            this.getSearchCond().eq("skill_type_name", n_skill_type_name_eq);
        }
    }
	private String n_skill_type_name_like;//[类型]
	public void setN_skill_type_name_like(String n_skill_type_name_like) {
        this.n_skill_type_name_like = n_skill_type_name_like;
        if(!ObjectUtils.isEmpty(this.n_skill_type_name_like)){
            this.getSearchCond().like("skill_type_name", n_skill_type_name_like);
        }
    }
	private Long n_create_uid_eq;//[ID]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_write_uid_eq;//[ID]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_skill_type_id_eq;//[技能类型]
	public void setN_skill_type_id_eq(Long n_skill_type_id_eq) {
        this.n_skill_type_id_eq = n_skill_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_skill_type_id_eq)){
            this.getSearchCond().eq("skill_type_id", n_skill_type_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



