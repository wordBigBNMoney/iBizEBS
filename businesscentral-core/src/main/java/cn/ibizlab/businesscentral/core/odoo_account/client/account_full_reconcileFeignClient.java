package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_full_reconcileSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_full_reconcile] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-full-reconcile", fallback = account_full_reconcileFallback.class)
public interface account_full_reconcileFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles")
    Account_full_reconcile create(@RequestBody Account_full_reconcile account_full_reconcile);

    @RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/batch")
    Boolean createBatch(@RequestBody List<Account_full_reconcile> account_full_reconciles);


    @RequestMapping(method = RequestMethod.GET, value = "/account_full_reconciles/{id}")
    Account_full_reconcile get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_full_reconciles/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_full_reconciles/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/search")
    Page<Account_full_reconcile> search(@RequestBody Account_full_reconcileSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_full_reconciles/{id}")
    Account_full_reconcile update(@PathVariable("id") Long id,@RequestBody Account_full_reconcile account_full_reconcile);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_full_reconciles/batch")
    Boolean updateBatch(@RequestBody List<Account_full_reconcile> account_full_reconciles);



    @RequestMapping(method = RequestMethod.GET, value = "/account_full_reconciles/select")
    Page<Account_full_reconcile> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_full_reconciles/getdraft")
    Account_full_reconcile getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/checkkey")
    Boolean checkKey(@RequestBody Account_full_reconcile account_full_reconcile);


    @RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/save")
    Boolean save(@RequestBody Account_full_reconcile account_full_reconcile);

    @RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/savebatch")
    Boolean saveBatch(@RequestBody List<Account_full_reconcile> account_full_reconciles);



    @RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/searchdefault")
    Page<Account_full_reconcile> searchDefault(@RequestBody Account_full_reconcileSearchContext context);


}
