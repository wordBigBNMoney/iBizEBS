package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_return_picking_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_return_picking_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_return_picking_line] 服务对象接口
 */
@Component
public class stock_return_picking_lineFallback implements stock_return_picking_lineFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Stock_return_picking_line get(Long id){
            return null;
     }


    public Page<Stock_return_picking_line> search(Stock_return_picking_lineSearchContext context){
            return null;
     }


    public Stock_return_picking_line update(Long id, Stock_return_picking_line stock_return_picking_line){
            return null;
     }
    public Boolean updateBatch(List<Stock_return_picking_line> stock_return_picking_lines){
            return false;
     }



    public Stock_return_picking_line create(Stock_return_picking_line stock_return_picking_line){
            return null;
     }
    public Boolean createBatch(List<Stock_return_picking_line> stock_return_picking_lines){
            return false;
     }

    public Page<Stock_return_picking_line> select(){
            return null;
     }

    public Stock_return_picking_line getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_return_picking_line stock_return_picking_line){
            return false;
     }


    public Boolean save(Stock_return_picking_line stock_return_picking_line){
            return false;
     }
    public Boolean saveBatch(List<Stock_return_picking_line> stock_return_picking_lines){
            return false;
     }

    public Page<Stock_return_picking_line> searchDefault(Stock_return_picking_lineSearchContext context){
            return null;
     }


}
