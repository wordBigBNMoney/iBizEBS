package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailingSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_mass_mailing] 服务对象接口
 */
public interface IMail_mass_mailingService extends IService<Mail_mass_mailing>{

    boolean create(Mail_mass_mailing et) ;
    void createBatch(List<Mail_mass_mailing> list) ;
    boolean update(Mail_mass_mailing et) ;
    void updateBatch(List<Mail_mass_mailing> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_mass_mailing get(Long key) ;
    Mail_mass_mailing getDraft(Mail_mass_mailing et) ;
    boolean checkKey(Mail_mass_mailing et) ;
    boolean save(Mail_mass_mailing et) ;
    void saveBatch(List<Mail_mass_mailing> list) ;
    Page<Mail_mass_mailing> searchDefault(Mail_mass_mailingSearchContext context) ;
    List<Mail_mass_mailing> selectByMassMailingCampaignId(Long id);
    void resetByMassMailingCampaignId(Long id);
    void resetByMassMailingCampaignId(Collection<Long> ids);
    void removeByMassMailingCampaignId(Long id);
    List<Mail_mass_mailing> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_mass_mailing> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Mail_mass_mailing> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mail_mass_mailing> selectByCampaignId(Long id);
    void resetByCampaignId(Long id);
    void resetByCampaignId(Collection<Long> ids);
    void removeByCampaignId(Long id);
    List<Mail_mass_mailing> selectByMediumId(Long id);
    void resetByMediumId(Long id);
    void resetByMediumId(Collection<Long> ids);
    void removeByMediumId(Long id);
    List<Mail_mass_mailing> selectBySourceId(Long id);
    void removeBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_mass_mailing> getMailMassMailingByIds(List<Long> ids) ;
    List<Mail_mass_mailing> getMailMassMailingByEntities(List<Mail_mass_mailing> entities) ;
}


