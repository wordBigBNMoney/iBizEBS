package cn.ibizlab.businesscentral.core.odoo_resource.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_leaves;
/**
 * 关系型数据实体[Resource_calendar_leaves] 查询条件对象
 */
@Slf4j
@Data
public class Resource_calendar_leavesSearchContext extends QueryWrapperContext<Resource_calendar_leaves> {

	private String n_name_like;//[原因]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_time_type_eq;//[时间类型]
	public void setN_time_type_eq(String n_time_type_eq) {
        this.n_time_type_eq = n_time_type_eq;
        if(!ObjectUtils.isEmpty(this.n_time_type_eq)){
            this.getSearchCond().eq("time_type", n_time_type_eq);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_calendar_id_text_eq;//[工作时间]
	public void setN_calendar_id_text_eq(String n_calendar_id_text_eq) {
        this.n_calendar_id_text_eq = n_calendar_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_calendar_id_text_eq)){
            this.getSearchCond().eq("calendar_id_text", n_calendar_id_text_eq);
        }
    }
	private String n_calendar_id_text_like;//[工作时间]
	public void setN_calendar_id_text_like(String n_calendar_id_text_like) {
        this.n_calendar_id_text_like = n_calendar_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_calendar_id_text_like)){
            this.getSearchCond().like("calendar_id_text", n_calendar_id_text_like);
        }
    }
	private String n_holiday_id_text_eq;//[休假申请]
	public void setN_holiday_id_text_eq(String n_holiday_id_text_eq) {
        this.n_holiday_id_text_eq = n_holiday_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_holiday_id_text_eq)){
            this.getSearchCond().eq("holiday_id_text", n_holiday_id_text_eq);
        }
    }
	private String n_holiday_id_text_like;//[休假申请]
	public void setN_holiday_id_text_like(String n_holiday_id_text_like) {
        this.n_holiday_id_text_like = n_holiday_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_holiday_id_text_like)){
            this.getSearchCond().like("holiday_id_text", n_holiday_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_resource_id_text_eq;//[资源]
	public void setN_resource_id_text_eq(String n_resource_id_text_eq) {
        this.n_resource_id_text_eq = n_resource_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_id_text_eq)){
            this.getSearchCond().eq("resource_id_text", n_resource_id_text_eq);
        }
    }
	private String n_resource_id_text_like;//[资源]
	public void setN_resource_id_text_like(String n_resource_id_text_like) {
        this.n_resource_id_text_like = n_resource_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_resource_id_text_like)){
            this.getSearchCond().like("resource_id_text", n_resource_id_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_holiday_id_eq;//[休假申请]
	public void setN_holiday_id_eq(Long n_holiday_id_eq) {
        this.n_holiday_id_eq = n_holiday_id_eq;
        if(!ObjectUtils.isEmpty(this.n_holiday_id_eq)){
            this.getSearchCond().eq("holiday_id", n_holiday_id_eq);
        }
    }
	private Long n_calendar_id_eq;//[工作时间]
	public void setN_calendar_id_eq(Long n_calendar_id_eq) {
        this.n_calendar_id_eq = n_calendar_id_eq;
        if(!ObjectUtils.isEmpty(this.n_calendar_id_eq)){
            this.getSearchCond().eq("calendar_id", n_calendar_id_eq);
        }
    }
	private Long n_resource_id_eq;//[资源]
	public void setN_resource_id_eq(Long n_resource_id_eq) {
        this.n_resource_id_eq = n_resource_id_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_id_eq)){
            this.getSearchCond().eq("resource_id", n_resource_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



