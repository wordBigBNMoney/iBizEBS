package cn.ibizlab.businesscentral.core.odoo_crm.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Crm_merge_opportunityMapper extends BaseMapper<Crm_merge_opportunity>{

    Page<Crm_merge_opportunity> searchDefault(IPage page, @Param("srf") Crm_merge_opportunitySearchContext context, @Param("ew") Wrapper<Crm_merge_opportunity> wrapper) ;
    @Override
    Crm_merge_opportunity selectById(Serializable id);
    @Override
    int insert(Crm_merge_opportunity entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Crm_merge_opportunity entity);
    @Override
    int update(@Param(Constants.ENTITY) Crm_merge_opportunity entity, @Param("ew") Wrapper<Crm_merge_opportunity> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Crm_merge_opportunity> selectByTeamId(@Param("id") Serializable id) ;

    List<Crm_merge_opportunity> selectByCreateUid(@Param("id") Serializable id) ;

    List<Crm_merge_opportunity> selectByUserId(@Param("id") Serializable id) ;

    List<Crm_merge_opportunity> selectByWriteUid(@Param("id") Serializable id) ;


}
