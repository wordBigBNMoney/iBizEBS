package cn.ibizlab.businesscentral.core.odoo_survey.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[调查的功能EMail撰写向导]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SURVEY_MAIL_COMPOSE_MESSAGE",resultMap = "Survey_mail_compose_messageResultMap")
public class Survey_mail_compose_message extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 删除邮件
     */
    @DEField(name = "auto_delete")
    @TableField(value = "auto_delete")
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private Boolean autoDelete;
    /**
     * 相关评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;
    /**
     * 待处理的业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    private String needactionPartnerIds;
    /**
     * 评级值
     */
    @TableField(exist = false)
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * Message-Id
     */
    @DEField(name = "message_id")
    @TableField(value = "message_id")
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;
    /**
     * 发件服务器
     */
    @DEField(name = "mail_server_id")
    @TableField(value = "mail_server_id")
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;
    /**
     * 内容
     */
    @TableField(value = "body")
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;
    /**
     * 收藏夹
     */
    @TableField(exist = false)
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    private String starredPartnerIds;
    /**
     * 频道
     */
    @TableField(exist = false)
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;
    /**
     * 星标邮件
     */
    @TableField(exist = false)
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private Boolean starred;
    /**
     * 需要审核
     */
    @TableField(exist = false)
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private Boolean needModeration;
    /**
     * 追踪值
     */
    @TableField(exist = false)
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    private String trackingValueIds;
    /**
     * 邀请回馈的截止日期有效
     */
    @DEField(name = "date_deadline")
    @TableField(value = "date_deadline")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    private Timestamp dateDeadline;
    /**
     * 线程无应答
     */
    @DEField(name = "no_auto_thread")
    @TableField(value = "no_auto_thread")
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private Boolean noAutoThread;
    /**
     * 主题
     */
    @TableField(value = "subject")
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * 回复 至
     */
    @DEField(name = "reply_to")
    @TableField(value = "reply_to")
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;
    /**
     * 说明
     */
    @TableField(exist = false)
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 现有联系人
     */
    @TableField(exist = false)
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 有误差
     */
    @TableField(exist = false)
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private Boolean hasError;
    /**
     * 邮件列表
     */
    @DEField(name = "multi_email")
    @TableField(value = "multi_email")
    @JSONField(name = "multi_email")
    @JsonProperty("multi_email")
    private String multiEmail;
    /**
     * 使用有效域
     */
    @DEField(name = "use_active_domain")
    @TableField(value = "use_active_domain")
    @JSONField(name = "use_active_domain")
    @JsonProperty("use_active_domain")
    private Boolean useActiveDomain;
    /**
     * 相关的文档模型
     */
    @TableField(value = "model")
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;
    /**
     * 待处理
     */
    @TableField(exist = false)
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private Boolean needaction;
    /**
     * 布局
     */
    @TableField(value = "layout")
    @JSONField(name = "layout")
    @JsonProperty("layout")
    private String layout;
    /**
     * 群发邮件标题
     */
    @DEField(name = "mass_mailing_name")
    @TableField(value = "mass_mailing_name")
    @JSONField(name = "mass_mailing_name")
    @JsonProperty("mass_mailing_name")
    private String massMailingName;
    /**
     * 删除消息副本
     */
    @DEField(name = "auto_delete_message")
    @TableField(value = "auto_delete_message")
    @JSONField(name = "auto_delete_message")
    @JsonProperty("auto_delete_message")
    private Boolean autoDeleteMessage;
    /**
     * 公开HTML网站链接
     */
    @TableField(exist = false)
    @JSONField(name = "public_url_html")
    @JsonProperty("public_url_html")
    private String publicUrlHtml;
    /**
     * 消息记录名称
     */
    @DEField(name = "record_name")
    @TableField(value = "record_name")
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    private String recordName;
    /**
     * 公开网址
     */
    @TableField(exist = false)
    @JSONField(name = "public_url")
    @JsonProperty("public_url")
    private String publicUrl;
    /**
     * 下级消息
     */
    @TableField(exist = false)
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;
    /**
     * 通知
     */
    @TableField(exist = false)
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    private String notificationIds;
    /**
     * 从
     */
    @DEField(name = "email_from")
    @TableField(value = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;
    /**
     * 记录内部备注
     */
    @DEField(name = "is_log")
    @TableField(value = "is_log")
    @JSONField(name = "is_log")
    @JsonProperty("is_log")
    private Boolean isLog;
    /**
     * 附件
     */
    @TableField(exist = false)
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;
    /**
     * 相关文档编号
     */
    @DEField(name = "res_id")
    @TableField(value = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * 日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 添加签名
     */
    @DEField(name = "add_sign")
    @TableField(value = "add_sign")
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private Boolean addSign;
    /**
     * 审核状态
     */
    @DEField(name = "moderation_status")
    @TableField(value = "moderation_status")
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    private String moderationStatus;
    /**
     * 类型
     */
    @DEField(name = "message_type")
    @TableField(value = "message_type")
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    private String messageType;
    /**
     * 分享选项
     */
    @TableField(value = "ibizpublic")
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private String ibizpublic;
    /**
     * 有效域
     */
    @DEField(name = "active_domain")
    @TableField(value = "active_domain")
    @JSONField(name = "active_domain")
    @JsonProperty("active_domain")
    private String activeDomain;
    /**
     * 通知关注者
     */
    @TableField(value = "notify")
    @JSONField(name = "notify")
    @JsonProperty("notify")
    private Boolean notify;
    /**
     * 写作模式
     */
    @DEField(name = "composition_mode")
    @TableField(value = "composition_mode")
    @JSONField(name = "composition_mode")
    @JsonProperty("composition_mode")
    private String compositionMode;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 已发布
     */
    @DEField(name = "website_published")
    @TableField(value = "website_published")
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;
    /**
     * 邮件列表
     */
    @TableField(exist = false)
    @JSONField(name = "mailing_list_ids")
    @JsonProperty("mailing_list_ids")
    private String mailingListIds;
    /**
     * 作者
     */
    @TableField(exist = false)
    @JSONField(name = "author_id_text")
    @JsonProperty("author_id_text")
    private String authorIdText;
    /**
     * 子类型
     */
    @TableField(exist = false)
    @JSONField(name = "subtype_id_text")
    @JsonProperty("subtype_id_text")
    private String subtypeIdText;
    /**
     * 使用模版
     */
    @TableField(exist = false)
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;
    /**
     * 作者头像
     */
    @TableField(exist = false)
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;
    /**
     * 邮件活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "mail_activity_type_id_text")
    @JsonProperty("mail_activity_type_id_text")
    private String mailActivityTypeIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 审核人
     */
    @TableField(exist = false)
    @JSONField(name = "moderator_id_text")
    @JsonProperty("moderator_id_text")
    private String moderatorIdText;
    /**
     * 群发邮件
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_id_text")
    @JsonProperty("mass_mailing_id_text")
    private String massMailingIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 群发邮件营销
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_campaign_id_text")
    @JsonProperty("mass_mailing_campaign_id_text")
    private String massMailingCampaignIdText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 问卷
     */
    @DEField(name = "survey_id")
    @TableField(value = "survey_id")
    @JSONField(name = "survey_id")
    @JsonProperty("survey_id")
    private Long surveyId;
    /**
     * 作者
     */
    @DEField(name = "author_id")
    @TableField(value = "author_id")
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    private Long authorId;
    /**
     * 群发邮件营销
     */
    @DEField(name = "mass_mailing_campaign_id")
    @TableField(value = "mass_mailing_campaign_id")
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    private Long massMailingCampaignId;
    /**
     * 上级消息
     */
    @DEField(name = "parent_id")
    @TableField(value = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 子类型
     */
    @DEField(name = "subtype_id")
    @TableField(value = "subtype_id")
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    private Long subtypeId;
    /**
     * 使用模版
     */
    @DEField(name = "template_id")
    @TableField(value = "template_id")
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Long templateId;
    /**
     * 群发邮件
     */
    @DEField(name = "mass_mailing_id")
    @TableField(value = "mass_mailing_id")
    @JSONField(name = "mass_mailing_id")
    @JsonProperty("mass_mailing_id")
    private Long massMailingId;
    /**
     * 审核人
     */
    @DEField(name = "moderator_id")
    @TableField(value = "moderator_id")
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    private Long moderatorId;
    /**
     * 邮件活动类型
     */
    @DEField(name = "mail_activity_type_id")
    @TableField(value = "mail_activity_type_id")
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    private Long mailActivityTypeId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type odooMailActivityType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign odooMassMailingCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing odooMassMailing;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message_subtype odooSubtype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message odooParent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAuthor;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooModerator;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey odooSurvey;



    /**
     * 设置 [删除邮件]
     */
    public void setAutoDelete(Boolean autoDelete){
        this.autoDelete = autoDelete ;
        this.modify("auto_delete",autoDelete);
    }

    /**
     * 设置 [Message-Id]
     */
    public void setMessageId(String messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }

    /**
     * 设置 [发件服务器]
     */
    public void setMailServerId(Integer mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }

    /**
     * 设置 [内容]
     */
    public void setBody(String body){
        this.body = body ;
        this.modify("body",body);
    }

    /**
     * 设置 [邀请回馈的截止日期有效]
     */
    public void setDateDeadline(Timestamp dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }

    /**
     * 格式化日期 [邀请回馈的截止日期有效]
     */
    public String formatDateDeadline(){
        if (this.dateDeadline == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateDeadline);
    }
    /**
     * 设置 [线程无应答]
     */
    public void setNoAutoThread(Boolean noAutoThread){
        this.noAutoThread = noAutoThread ;
        this.modify("no_auto_thread",noAutoThread);
    }

    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [回复 至]
     */
    public void setReplyTo(String replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }

    /**
     * 设置 [邮件列表]
     */
    public void setMultiEmail(String multiEmail){
        this.multiEmail = multiEmail ;
        this.modify("multi_email",multiEmail);
    }

    /**
     * 设置 [使用有效域]
     */
    public void setUseActiveDomain(Boolean useActiveDomain){
        this.useActiveDomain = useActiveDomain ;
        this.modify("use_active_domain",useActiveDomain);
    }

    /**
     * 设置 [相关的文档模型]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [布局]
     */
    public void setLayout(String layout){
        this.layout = layout ;
        this.modify("layout",layout);
    }

    /**
     * 设置 [群发邮件标题]
     */
    public void setMassMailingName(String massMailingName){
        this.massMailingName = massMailingName ;
        this.modify("mass_mailing_name",massMailingName);
    }

    /**
     * 设置 [删除消息副本]
     */
    public void setAutoDeleteMessage(Boolean autoDeleteMessage){
        this.autoDeleteMessage = autoDeleteMessage ;
        this.modify("auto_delete_message",autoDeleteMessage);
    }

    /**
     * 设置 [消息记录名称]
     */
    public void setRecordName(String recordName){
        this.recordName = recordName ;
        this.modify("record_name",recordName);
    }

    /**
     * 设置 [从]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [记录内部备注]
     */
    public void setIsLog(Boolean isLog){
        this.isLog = isLog ;
        this.modify("is_log",isLog);
    }

    /**
     * 设置 [相关文档编号]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
    /**
     * 设置 [添加签名]
     */
    public void setAddSign(Boolean addSign){
        this.addSign = addSign ;
        this.modify("add_sign",addSign);
    }

    /**
     * 设置 [审核状态]
     */
    public void setModerationStatus(String moderationStatus){
        this.moderationStatus = moderationStatus ;
        this.modify("moderation_status",moderationStatus);
    }

    /**
     * 设置 [类型]
     */
    public void setMessageType(String messageType){
        this.messageType = messageType ;
        this.modify("message_type",messageType);
    }

    /**
     * 设置 [分享选项]
     */
    public void setIbizpublic(String ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.modify("ibizpublic",ibizpublic);
    }

    /**
     * 设置 [有效域]
     */
    public void setActiveDomain(String activeDomain){
        this.activeDomain = activeDomain ;
        this.modify("active_domain",activeDomain);
    }

    /**
     * 设置 [通知关注者]
     */
    public void setNotify(Boolean notify){
        this.notify = notify ;
        this.modify("notify",notify);
    }

    /**
     * 设置 [写作模式]
     */
    public void setCompositionMode(String compositionMode){
        this.compositionMode = compositionMode ;
        this.modify("composition_mode",compositionMode);
    }

    /**
     * 设置 [已发布]
     */
    public void setWebsitePublished(Boolean websitePublished){
        this.websitePublished = websitePublished ;
        this.modify("website_published",websitePublished);
    }

    /**
     * 设置 [问卷]
     */
    public void setSurveyId(Long surveyId){
        this.surveyId = surveyId ;
        this.modify("survey_id",surveyId);
    }

    /**
     * 设置 [作者]
     */
    public void setAuthorId(Long authorId){
        this.authorId = authorId ;
        this.modify("author_id",authorId);
    }

    /**
     * 设置 [群发邮件营销]
     */
    public void setMassMailingCampaignId(Long massMailingCampaignId){
        this.massMailingCampaignId = massMailingCampaignId ;
        this.modify("mass_mailing_campaign_id",massMailingCampaignId);
    }

    /**
     * 设置 [上级消息]
     */
    public void setParentId(Long parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [子类型]
     */
    public void setSubtypeId(Long subtypeId){
        this.subtypeId = subtypeId ;
        this.modify("subtype_id",subtypeId);
    }

    /**
     * 设置 [使用模版]
     */
    public void setTemplateId(Long templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }

    /**
     * 设置 [群发邮件]
     */
    public void setMassMailingId(Long massMailingId){
        this.massMailingId = massMailingId ;
        this.modify("mass_mailing_id",massMailingId);
    }

    /**
     * 设置 [审核人]
     */
    public void setModeratorId(Long moderatorId){
        this.moderatorId = moderatorId ;
        this.modify("moderator_id",moderatorId);
    }

    /**
     * 设置 [邮件活动类型]
     */
    public void setMailActivityTypeId(Long mailActivityTypeId){
        this.mailActivityTypeId = mailActivityTypeId ;
        this.modify("mail_activity_type_id",mailActivityTypeId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


