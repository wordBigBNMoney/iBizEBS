package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_template_attribute_exclusion] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-template-attribute-exclusion", fallback = product_template_attribute_exclusionFallback.class)
public interface product_template_attribute_exclusionFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_exclusions/{id}")
    Product_template_attribute_exclusion get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions")
    Product_template_attribute_exclusion create(@RequestBody Product_template_attribute_exclusion product_template_attribute_exclusion);

    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/batch")
    Boolean createBatch(@RequestBody List<Product_template_attribute_exclusion> product_template_attribute_exclusions);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_exclusions/{id}")
    Product_template_attribute_exclusion update(@PathVariable("id") Long id,@RequestBody Product_template_attribute_exclusion product_template_attribute_exclusion);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_exclusions/batch")
    Boolean updateBatch(@RequestBody List<Product_template_attribute_exclusion> product_template_attribute_exclusions);



    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/search")
    Page<Product_template_attribute_exclusion> search(@RequestBody Product_template_attribute_exclusionSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_exclusions/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_exclusions/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_exclusions/select")
    Page<Product_template_attribute_exclusion> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_exclusions/getdraft")
    Product_template_attribute_exclusion getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/checkkey")
    Boolean checkKey(@RequestBody Product_template_attribute_exclusion product_template_attribute_exclusion);


    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/save")
    Boolean save(@RequestBody Product_template_attribute_exclusion product_template_attribute_exclusion);

    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/savebatch")
    Boolean saveBatch(@RequestBody List<Product_template_attribute_exclusion> product_template_attribute_exclusions);



    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/searchdefault")
    Page<Product_template_attribute_exclusion> searchDefault(@RequestBody Product_template_attribute_exclusionSearchContext context);


}
