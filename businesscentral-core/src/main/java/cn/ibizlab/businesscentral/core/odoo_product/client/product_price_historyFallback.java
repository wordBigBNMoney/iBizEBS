package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_price_history;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_price_historySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_price_history] 服务对象接口
 */
@Component
public class product_price_historyFallback implements product_price_historyFeignClient{

    public Page<Product_price_history> search(Product_price_historySearchContext context){
            return null;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Product_price_history create(Product_price_history product_price_history){
            return null;
     }
    public Boolean createBatch(List<Product_price_history> product_price_histories){
            return false;
     }

    public Product_price_history get(Long id){
            return null;
     }


    public Product_price_history update(Long id, Product_price_history product_price_history){
            return null;
     }
    public Boolean updateBatch(List<Product_price_history> product_price_histories){
            return false;
     }



    public Page<Product_price_history> select(){
            return null;
     }

    public Product_price_history getDraft(){
            return null;
    }



    public Boolean checkKey(Product_price_history product_price_history){
            return false;
     }


    public Boolean save(Product_price_history product_price_history){
            return false;
     }
    public Boolean saveBatch(List<Product_price_history> product_price_histories){
            return false;
     }

    public Page<Product_price_history> searchDefault(Product_price_historySearchContext context){
            return null;
     }


}
