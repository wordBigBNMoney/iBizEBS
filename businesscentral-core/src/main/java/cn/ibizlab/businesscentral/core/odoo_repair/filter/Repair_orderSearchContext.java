package cn.ibizlab.businesscentral.core.odoo_repair.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order;
/**
 * 关系型数据实体[Repair_order] 查询条件对象
 */
@Slf4j
@Data
public class Repair_orderSearchContext extends QueryWrapperContext<Repair_order> {

	private String n_invoice_method_eq;//[开票方式]
	public void setN_invoice_method_eq(String n_invoice_method_eq) {
        this.n_invoice_method_eq = n_invoice_method_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_method_eq)){
            this.getSearchCond().eq("invoice_method", n_invoice_method_eq);
        }
    }
	private String n_name_like;//[维修参照]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_pricelist_id_text_eq;//[价格表]
	public void setN_pricelist_id_text_eq(String n_pricelist_id_text_eq) {
        this.n_pricelist_id_text_eq = n_pricelist_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_eq)){
            this.getSearchCond().eq("pricelist_id_text", n_pricelist_id_text_eq);
        }
    }
	private String n_pricelist_id_text_like;//[价格表]
	public void setN_pricelist_id_text_like(String n_pricelist_id_text_like) {
        this.n_pricelist_id_text_like = n_pricelist_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_like)){
            this.getSearchCond().like("pricelist_id_text", n_pricelist_id_text_like);
        }
    }
	private String n_lot_id_text_eq;//[批次/序列号]
	public void setN_lot_id_text_eq(String n_lot_id_text_eq) {
        this.n_lot_id_text_eq = n_lot_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_eq)){
            this.getSearchCond().eq("lot_id_text", n_lot_id_text_eq);
        }
    }
	private String n_lot_id_text_like;//[批次/序列号]
	public void setN_lot_id_text_like(String n_lot_id_text_like) {
        this.n_lot_id_text_like = n_lot_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_like)){
            this.getSearchCond().like("lot_id_text", n_lot_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[客户]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[客户]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[待维修产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[待维修产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_invoice_id_text_eq;//[发票]
	public void setN_invoice_id_text_eq(String n_invoice_id_text_eq) {
        this.n_invoice_id_text_eq = n_invoice_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_text_eq)){
            this.getSearchCond().eq("invoice_id_text", n_invoice_id_text_eq);
        }
    }
	private String n_invoice_id_text_like;//[发票]
	public void setN_invoice_id_text_like(String n_invoice_id_text_like) {
        this.n_invoice_id_text_like = n_invoice_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_text_like)){
            this.getSearchCond().like("invoice_id_text", n_invoice_id_text_like);
        }
    }
	private String n_product_uom_text_eq;//[产品量度单位]
	public void setN_product_uom_text_eq(String n_product_uom_text_eq) {
        this.n_product_uom_text_eq = n_product_uom_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_eq)){
            this.getSearchCond().eq("product_uom_text", n_product_uom_text_eq);
        }
    }
	private String n_product_uom_text_like;//[产品量度单位]
	public void setN_product_uom_text_like(String n_product_uom_text_like) {
        this.n_product_uom_text_like = n_product_uom_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_like)){
            this.getSearchCond().like("product_uom_text", n_product_uom_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建者]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建者]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_partner_invoice_id_text_eq;//[开票地址]
	public void setN_partner_invoice_id_text_eq(String n_partner_invoice_id_text_eq) {
        this.n_partner_invoice_id_text_eq = n_partner_invoice_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_invoice_id_text_eq)){
            this.getSearchCond().eq("partner_invoice_id_text", n_partner_invoice_id_text_eq);
        }
    }
	private String n_partner_invoice_id_text_like;//[开票地址]
	public void setN_partner_invoice_id_text_like(String n_partner_invoice_id_text_like) {
        this.n_partner_invoice_id_text_like = n_partner_invoice_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_invoice_id_text_like)){
            this.getSearchCond().like("partner_invoice_id_text", n_partner_invoice_id_text_like);
        }
    }
	private String n_address_id_text_eq;//[收货地址]
	public void setN_address_id_text_eq(String n_address_id_text_eq) {
        this.n_address_id_text_eq = n_address_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_address_id_text_eq)){
            this.getSearchCond().eq("address_id_text", n_address_id_text_eq);
        }
    }
	private String n_address_id_text_like;//[收货地址]
	public void setN_address_id_text_like(String n_address_id_text_like) {
        this.n_address_id_text_like = n_address_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_address_id_text_like)){
            this.getSearchCond().like("address_id_text", n_address_id_text_like);
        }
    }
	private String n_location_id_text_eq;//[地点]
	public void setN_location_id_text_eq(String n_location_id_text_eq) {
        this.n_location_id_text_eq = n_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_eq)){
            this.getSearchCond().eq("location_id_text", n_location_id_text_eq);
        }
    }
	private String n_location_id_text_like;//[地点]
	public void setN_location_id_text_like(String n_location_id_text_like) {
        this.n_location_id_text_like = n_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_like)){
            this.getSearchCond().like("location_id_text", n_location_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_move_id_text_eq;//[移动]
	public void setN_move_id_text_eq(String n_move_id_text_eq) {
        this.n_move_id_text_eq = n_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_text_eq)){
            this.getSearchCond().eq("move_id_text", n_move_id_text_eq);
        }
    }
	private String n_move_id_text_like;//[移动]
	public void setN_move_id_text_like(String n_move_id_text_like) {
        this.n_move_id_text_like = n_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_move_id_text_like)){
            this.getSearchCond().like("move_id_text", n_move_id_text_like);
        }
    }
	private Long n_move_id_eq;//[移动]
	public void setN_move_id_eq(Long n_move_id_eq) {
        this.n_move_id_eq = n_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_eq)){
            this.getSearchCond().eq("move_id", n_move_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_partner_id_eq;//[客户]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_pricelist_id_eq;//[价格表]
	public void setN_pricelist_id_eq(Long n_pricelist_id_eq) {
        this.n_pricelist_id_eq = n_pricelist_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_eq)){
            this.getSearchCond().eq("pricelist_id", n_pricelist_id_eq);
        }
    }
	private Long n_product_id_eq;//[待维修产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_partner_invoice_id_eq;//[开票地址]
	public void setN_partner_invoice_id_eq(Long n_partner_invoice_id_eq) {
        this.n_partner_invoice_id_eq = n_partner_invoice_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_invoice_id_eq)){
            this.getSearchCond().eq("partner_invoice_id", n_partner_invoice_id_eq);
        }
    }
	private Long n_location_id_eq;//[地点]
	public void setN_location_id_eq(Long n_location_id_eq) {
        this.n_location_id_eq = n_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_eq)){
            this.getSearchCond().eq("location_id", n_location_id_eq);
        }
    }
	private Long n_product_uom_eq;//[产品量度单位]
	public void setN_product_uom_eq(Long n_product_uom_eq) {
        this.n_product_uom_eq = n_product_uom_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_eq)){
            this.getSearchCond().eq("product_uom", n_product_uom_eq);
        }
    }
	private Long n_create_uid_eq;//[创建者]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_address_id_eq;//[收货地址]
	public void setN_address_id_eq(Long n_address_id_eq) {
        this.n_address_id_eq = n_address_id_eq;
        if(!ObjectUtils.isEmpty(this.n_address_id_eq)){
            this.getSearchCond().eq("address_id", n_address_id_eq);
        }
    }
	private Long n_invoice_id_eq;//[发票]
	public void setN_invoice_id_eq(Long n_invoice_id_eq) {
        this.n_invoice_id_eq = n_invoice_id_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_id_eq)){
            this.getSearchCond().eq("invoice_id", n_invoice_id_eq);
        }
    }
	private Long n_lot_id_eq;//[批次/序列号]
	public void setN_lot_id_eq(Long n_lot_id_eq) {
        this.n_lot_id_eq = n_lot_id_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_eq)){
            this.getSearchCond().eq("lot_id", n_lot_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



