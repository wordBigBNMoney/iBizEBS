package cn.ibizlab.businesscentral.core.odoo_web_editor.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[web_editor_converter_test] 服务对象接口
 */
@Component
public class web_editor_converter_testFallback implements web_editor_converter_testFeignClient{

    public Page<Web_editor_converter_test> search(Web_editor_converter_testSearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Web_editor_converter_test update(Long id, Web_editor_converter_test web_editor_converter_test){
            return null;
     }
    public Boolean updateBatch(List<Web_editor_converter_test> web_editor_converter_tests){
            return false;
     }



    public Web_editor_converter_test create(Web_editor_converter_test web_editor_converter_test){
            return null;
     }
    public Boolean createBatch(List<Web_editor_converter_test> web_editor_converter_tests){
            return false;
     }

    public Web_editor_converter_test get(Long id){
            return null;
     }



    public Page<Web_editor_converter_test> select(){
            return null;
     }

    public Web_editor_converter_test getDraft(){
            return null;
    }



    public Boolean checkKey(Web_editor_converter_test web_editor_converter_test){
            return false;
     }


    public Boolean save(Web_editor_converter_test web_editor_converter_test){
            return false;
     }
    public Boolean saveBatch(List<Web_editor_converter_test> web_editor_converter_tests){
            return false;
     }

    public Page<Web_editor_converter_test> searchDefault(Web_editor_converter_testSearchContext context){
            return null;
     }


}
