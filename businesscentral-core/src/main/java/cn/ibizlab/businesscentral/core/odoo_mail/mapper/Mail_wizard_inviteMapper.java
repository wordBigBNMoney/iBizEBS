package cn.ibizlab.businesscentral.core.odoo_mail.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mail_wizard_inviteMapper extends BaseMapper<Mail_wizard_invite>{

    Page<Mail_wizard_invite> searchDefault(IPage page, @Param("srf") Mail_wizard_inviteSearchContext context, @Param("ew") Wrapper<Mail_wizard_invite> wrapper) ;
    @Override
    Mail_wizard_invite selectById(Serializable id);
    @Override
    int insert(Mail_wizard_invite entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mail_wizard_invite entity);
    @Override
    int update(@Param(Constants.ENTITY) Mail_wizard_invite entity, @Param("ew") Wrapper<Mail_wizard_invite> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mail_wizard_invite> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mail_wizard_invite> selectByWriteUid(@Param("id") Serializable id) ;


}
