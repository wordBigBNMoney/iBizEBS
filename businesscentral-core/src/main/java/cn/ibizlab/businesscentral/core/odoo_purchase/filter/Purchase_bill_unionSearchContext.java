package cn.ibizlab.businesscentral.core.odoo_purchase.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_bill_union;
/**
 * 关系型数据实体[Purchase_bill_union] 查询条件对象
 */
@Slf4j
@Data
public class Purchase_bill_unionSearchContext extends QueryWrapperContext<Purchase_bill_union> {

	private String n_name_like;//[参考]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_purchase_order_id_text_eq;//[采购订单]
	public void setN_purchase_order_id_text_eq(String n_purchase_order_id_text_eq) {
        this.n_purchase_order_id_text_eq = n_purchase_order_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_order_id_text_eq)){
            this.getSearchCond().eq("purchase_order_id_text", n_purchase_order_id_text_eq);
        }
    }
	private String n_purchase_order_id_text_like;//[采购订单]
	public void setN_purchase_order_id_text_like(String n_purchase_order_id_text_like) {
        this.n_purchase_order_id_text_like = n_purchase_order_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_purchase_order_id_text_like)){
            this.getSearchCond().like("purchase_order_id_text", n_purchase_order_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[供应商]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[供应商]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_vendor_bill_id_text_eq;//[供应商账单]
	public void setN_vendor_bill_id_text_eq(String n_vendor_bill_id_text_eq) {
        this.n_vendor_bill_id_text_eq = n_vendor_bill_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_id_text_eq)){
            this.getSearchCond().eq("vendor_bill_id_text", n_vendor_bill_id_text_eq);
        }
    }
	private String n_vendor_bill_id_text_like;//[供应商账单]
	public void setN_vendor_bill_id_text_like(String n_vendor_bill_id_text_like) {
        this.n_vendor_bill_id_text_like = n_vendor_bill_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_id_text_like)){
            this.getSearchCond().like("vendor_bill_id_text", n_vendor_bill_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private Long n_vendor_bill_id_eq;//[供应商账单]
	public void setN_vendor_bill_id_eq(Long n_vendor_bill_id_eq) {
        this.n_vendor_bill_id_eq = n_vendor_bill_id_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_id_eq)){
            this.getSearchCond().eq("vendor_bill_id", n_vendor_bill_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_purchase_order_id_eq;//[采购订单]
	public void setN_purchase_order_id_eq(Long n_purchase_order_id_eq) {
        this.n_purchase_order_id_eq = n_purchase_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_order_id_eq)){
            this.getSearchCond().eq("purchase_order_id", n_purchase_order_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_partner_id_eq;//[供应商]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



