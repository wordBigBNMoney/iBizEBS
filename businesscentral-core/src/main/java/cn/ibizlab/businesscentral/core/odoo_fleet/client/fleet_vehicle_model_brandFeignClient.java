package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_model_brand] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-vehicle-model-brand", fallback = fleet_vehicle_model_brandFallback.class)
public interface fleet_vehicle_model_brandFeignClient {




    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_model_brands/{id}")
    Fleet_vehicle_model_brand get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/search")
    Page<Fleet_vehicle_model_brand> search(@RequestBody Fleet_vehicle_model_brandSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_model_brands/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_model_brands/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_model_brands/{id}")
    Fleet_vehicle_model_brand update(@PathVariable("id") Long id,@RequestBody Fleet_vehicle_model_brand fleet_vehicle_model_brand);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_model_brands/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_model_brand> fleet_vehicle_model_brands);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands")
    Fleet_vehicle_model_brand create(@RequestBody Fleet_vehicle_model_brand fleet_vehicle_model_brand);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_model_brand> fleet_vehicle_model_brands);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_model_brands/select")
    Page<Fleet_vehicle_model_brand> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_model_brands/getdraft")
    Fleet_vehicle_model_brand getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/checkkey")
    Boolean checkKey(@RequestBody Fleet_vehicle_model_brand fleet_vehicle_model_brand);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/save")
    Boolean save(@RequestBody Fleet_vehicle_model_brand fleet_vehicle_model_brand);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_vehicle_model_brand> fleet_vehicle_model_brands);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/searchdefault")
    Page<Fleet_vehicle_model_brand> searchDefault(@RequestBody Fleet_vehicle_model_brandSearchContext context);


}
