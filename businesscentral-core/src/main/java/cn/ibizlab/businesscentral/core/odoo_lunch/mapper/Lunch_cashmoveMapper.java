package cn.ibizlab.businesscentral.core.odoo_lunch.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Lunch_cashmoveMapper extends BaseMapper<Lunch_cashmove>{

    Page<Lunch_cashmove> searchDefault(IPage page, @Param("srf") Lunch_cashmoveSearchContext context, @Param("ew") Wrapper<Lunch_cashmove> wrapper) ;
    @Override
    Lunch_cashmove selectById(Serializable id);
    @Override
    int insert(Lunch_cashmove entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Lunch_cashmove entity);
    @Override
    int update(@Param(Constants.ENTITY) Lunch_cashmove entity, @Param("ew") Wrapper<Lunch_cashmove> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Lunch_cashmove> selectByOrderId(@Param("id") Serializable id) ;

    List<Lunch_cashmove> selectByCreateUid(@Param("id") Serializable id) ;

    List<Lunch_cashmove> selectByUserId(@Param("id") Serializable id) ;

    List<Lunch_cashmove> selectByWriteUid(@Param("id") Serializable id) ;


}
