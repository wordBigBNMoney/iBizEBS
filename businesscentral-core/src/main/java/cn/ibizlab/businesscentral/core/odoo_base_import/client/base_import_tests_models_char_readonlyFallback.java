package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char_readonly;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_char_readonlySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_char_readonly] 服务对象接口
 */
@Component
public class base_import_tests_models_char_readonlyFallback implements base_import_tests_models_char_readonlyFeignClient{

    public Base_import_tests_models_char_readonly update(Long id, Base_import_tests_models_char_readonly base_import_tests_models_char_readonly){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
            return false;
     }




    public Base_import_tests_models_char_readonly create(Base_import_tests_models_char_readonly base_import_tests_models_char_readonly){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
            return false;
     }


    public Page<Base_import_tests_models_char_readonly> search(Base_import_tests_models_char_readonlySearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Base_import_tests_models_char_readonly get(Long id){
            return null;
     }


    public Page<Base_import_tests_models_char_readonly> select(){
            return null;
     }

    public Base_import_tests_models_char_readonly getDraft(){
            return null;
    }



    public Boolean checkKey(Base_import_tests_models_char_readonly base_import_tests_models_char_readonly){
            return false;
     }


    public Boolean save(Base_import_tests_models_char_readonly base_import_tests_models_char_readonly){
            return false;
     }
    public Boolean saveBatch(List<Base_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
            return false;
     }

    public Page<Base_import_tests_models_char_readonly> searchDefault(Base_import_tests_models_char_readonlySearchContext context){
            return null;
     }


}
