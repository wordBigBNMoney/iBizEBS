package cn.ibizlab.businesscentral.core.odoo_utm.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_mixinSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Utm_mixinMapper extends BaseMapper<Utm_mixin>{

    Page<Utm_mixin> searchDefault(IPage page, @Param("srf") Utm_mixinSearchContext context, @Param("ew") Wrapper<Utm_mixin> wrapper) ;
    @Override
    Utm_mixin selectById(Serializable id);
    @Override
    int insert(Utm_mixin entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Utm_mixin entity);
    @Override
    int update(@Param(Constants.ENTITY) Utm_mixin entity, @Param("ew") Wrapper<Utm_mixin> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Utm_mixin> selectByCampaignId(@Param("id") Serializable id) ;

    List<Utm_mixin> selectByMediumId(@Param("id") Serializable id) ;

    List<Utm_mixin> selectBySourceId(@Param("id") Serializable id) ;


}
