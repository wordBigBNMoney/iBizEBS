package cn.ibizlab.businesscentral.core.odoo_purchase.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_report;
/**
 * 关系型数据实体[Purchase_report] 查询条件对象
 */
@Slf4j
@Data
public class Purchase_reportSearchContext extends QueryWrapperContext<Purchase_report> {

	private String n_state_eq;//[订单状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_category_id_text_eq;//[产品种类]
	public void setN_category_id_text_eq(String n_category_id_text_eq) {
        this.n_category_id_text_eq = n_category_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_text_eq)){
            this.getSearchCond().eq("category_id_text", n_category_id_text_eq);
        }
    }
	private String n_category_id_text_like;//[产品种类]
	public void setN_category_id_text_like(String n_category_id_text_like) {
        this.n_category_id_text_like = n_category_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_category_id_text_like)){
            this.getSearchCond().like("category_id_text", n_category_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[供应商]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[供应商]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_product_tmpl_id_text_eq;//[产品模板]
	public void setN_product_tmpl_id_text_eq(String n_product_tmpl_id_text_eq) {
        this.n_product_tmpl_id_text_eq = n_product_tmpl_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_text_eq)){
            this.getSearchCond().eq("product_tmpl_id_text", n_product_tmpl_id_text_eq);
        }
    }
	private String n_product_tmpl_id_text_like;//[产品模板]
	public void setN_product_tmpl_id_text_like(String n_product_tmpl_id_text_like) {
        this.n_product_tmpl_id_text_like = n_product_tmpl_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_text_like)){
            this.getSearchCond().like("product_tmpl_id_text", n_product_tmpl_id_text_like);
        }
    }
	private String n_country_id_text_eq;//[业务伙伴国家]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[业务伙伴国家]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private String n_picking_type_id_text_eq;//[仓库]
	public void setN_picking_type_id_text_eq(String n_picking_type_id_text_eq) {
        this.n_picking_type_id_text_eq = n_picking_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_eq)){
            this.getSearchCond().eq("picking_type_id_text", n_picking_type_id_text_eq);
        }
    }
	private String n_picking_type_id_text_like;//[仓库]
	public void setN_picking_type_id_text_like(String n_picking_type_id_text_like) {
        this.n_picking_type_id_text_like = n_picking_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_like)){
            this.getSearchCond().like("picking_type_id_text", n_picking_type_id_text_like);
        }
    }
	private String n_fiscal_position_id_text_eq;//[税科目调整]
	public void setN_fiscal_position_id_text_eq(String n_fiscal_position_id_text_eq) {
        this.n_fiscal_position_id_text_eq = n_fiscal_position_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_eq)){
            this.getSearchCond().eq("fiscal_position_id_text", n_fiscal_position_id_text_eq);
        }
    }
	private String n_fiscal_position_id_text_like;//[税科目调整]
	public void setN_fiscal_position_id_text_like(String n_fiscal_position_id_text_like) {
        this.n_fiscal_position_id_text_like = n_fiscal_position_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_like)){
            this.getSearchCond().like("fiscal_position_id_text", n_fiscal_position_id_text_like);
        }
    }
	private String n_account_analytic_id_text_eq;//[分析账户]
	public void setN_account_analytic_id_text_eq(String n_account_analytic_id_text_eq) {
        this.n_account_analytic_id_text_eq = n_account_analytic_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_text_eq)){
            this.getSearchCond().eq("account_analytic_id_text", n_account_analytic_id_text_eq);
        }
    }
	private String n_account_analytic_id_text_like;//[分析账户]
	public void setN_account_analytic_id_text_like(String n_account_analytic_id_text_like) {
        this.n_account_analytic_id_text_like = n_account_analytic_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_text_like)){
            this.getSearchCond().like("account_analytic_id_text", n_account_analytic_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[采购员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[采购员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_commercial_partner_id_text_eq;//[商业实体]
	public void setN_commercial_partner_id_text_eq(String n_commercial_partner_id_text_eq) {
        this.n_commercial_partner_id_text_eq = n_commercial_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_eq)){
            this.getSearchCond().eq("commercial_partner_id_text", n_commercial_partner_id_text_eq);
        }
    }
	private String n_commercial_partner_id_text_like;//[商业实体]
	public void setN_commercial_partner_id_text_like(String n_commercial_partner_id_text_like) {
        this.n_commercial_partner_id_text_like = n_commercial_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_like)){
            this.getSearchCond().like("commercial_partner_id_text", n_commercial_partner_id_text_like);
        }
    }
	private String n_product_uom_text_eq;//[参考计量单位]
	public void setN_product_uom_text_eq(String n_product_uom_text_eq) {
        this.n_product_uom_text_eq = n_product_uom_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_eq)){
            this.getSearchCond().eq("product_uom_text", n_product_uom_text_eq);
        }
    }
	private String n_product_uom_text_like;//[参考计量单位]
	public void setN_product_uom_text_like(String n_product_uom_text_like) {
        this.n_product_uom_text_like = n_product_uom_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_like)){
            this.getSearchCond().like("product_uom_text", n_product_uom_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private Long n_category_id_eq;//[产品种类]
	public void setN_category_id_eq(Long n_category_id_eq) {
        this.n_category_id_eq = n_category_id_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_eq)){
            this.getSearchCond().eq("category_id", n_category_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_picking_type_id_eq;//[仓库]
	public void setN_picking_type_id_eq(Long n_picking_type_id_eq) {
        this.n_picking_type_id_eq = n_picking_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_eq)){
            this.getSearchCond().eq("picking_type_id", n_picking_type_id_eq);
        }
    }
	private Long n_user_id_eq;//[采购员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_commercial_partner_id_eq;//[商业实体]
	public void setN_commercial_partner_id_eq(Long n_commercial_partner_id_eq) {
        this.n_commercial_partner_id_eq = n_commercial_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_eq)){
            this.getSearchCond().eq("commercial_partner_id", n_commercial_partner_id_eq);
        }
    }
	private Long n_product_tmpl_id_eq;//[产品模板]
	public void setN_product_tmpl_id_eq(Long n_product_tmpl_id_eq) {
        this.n_product_tmpl_id_eq = n_product_tmpl_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_eq)){
            this.getSearchCond().eq("product_tmpl_id", n_product_tmpl_id_eq);
        }
    }
	private Long n_partner_id_eq;//[供应商]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_product_uom_eq;//[参考计量单位]
	public void setN_product_uom_eq(Long n_product_uom_eq) {
        this.n_product_uom_eq = n_product_uom_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_eq)){
            this.getSearchCond().eq("product_uom", n_product_uom_eq);
        }
    }
	private Long n_country_id_eq;//[业务伙伴国家]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }
	private Long n_fiscal_position_id_eq;//[税科目调整]
	public void setN_fiscal_position_id_eq(Long n_fiscal_position_id_eq) {
        this.n_fiscal_position_id_eq = n_fiscal_position_id_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_eq)){
            this.getSearchCond().eq("fiscal_position_id", n_fiscal_position_id_eq);
        }
    }
	private Long n_account_analytic_id_eq;//[分析账户]
	public void setN_account_analytic_id_eq(Long n_account_analytic_id_eq) {
        this.n_account_analytic_id_eq = n_account_analytic_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_eq)){
            this.getSearchCond().eq("account_analytic_id", n_account_analytic_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



