package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_packagingSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_packaging] 服务对象接口
 */
public interface IProduct_packagingService extends IService<Product_packaging>{

    boolean create(Product_packaging et) ;
    void createBatch(List<Product_packaging> list) ;
    boolean update(Product_packaging et) ;
    void updateBatch(List<Product_packaging> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_packaging get(Long key) ;
    Product_packaging getDraft(Product_packaging et) ;
    boolean checkKey(Product_packaging et) ;
    boolean save(Product_packaging et) ;
    void saveBatch(List<Product_packaging> list) ;
    Page<Product_packaging> searchDefault(Product_packagingSearchContext context) ;
    List<Product_packaging> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Product_packaging> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_packaging> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_packaging> getProductPackagingByIds(List<Long> ids) ;
    List<Product_packaging> getProductPackagingByEntities(List<Product_packaging> entities) ;
}


