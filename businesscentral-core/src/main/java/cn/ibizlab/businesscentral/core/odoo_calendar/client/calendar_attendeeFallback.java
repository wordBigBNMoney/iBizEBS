package cn.ibizlab.businesscentral.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_attendeeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[calendar_attendee] 服务对象接口
 */
@Component
public class calendar_attendeeFallback implements calendar_attendeeFeignClient{

    public Calendar_attendee update(Long id, Calendar_attendee calendar_attendee){
            return null;
     }
    public Boolean updateBatch(List<Calendar_attendee> calendar_attendees){
            return false;
     }



    public Calendar_attendee create(Calendar_attendee calendar_attendee){
            return null;
     }
    public Boolean createBatch(List<Calendar_attendee> calendar_attendees){
            return false;
     }


    public Calendar_attendee get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Calendar_attendee> search(Calendar_attendeeSearchContext context){
            return null;
     }


    public Page<Calendar_attendee> select(){
            return null;
     }

    public Calendar_attendee getDraft(){
            return null;
    }



    public Boolean checkKey(Calendar_attendee calendar_attendee){
            return false;
     }


    public Boolean save(Calendar_attendee calendar_attendee){
            return false;
     }
    public Boolean saveBatch(List<Calendar_attendee> calendar_attendees){
            return false;
     }

    public Page<Calendar_attendee> searchDefault(Calendar_attendeeSearchContext context){
            return null;
     }


}
