package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers_mail_message_subtype_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_followers_mail_message_subtype_relSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_followers_mail_message_subtype_relService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_followers_mail_message_subtype_relMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[关注消息类型] 服务对象接口实现
 */
@Slf4j
@Service("Mail_followers_mail_message_subtype_relServiceImpl")
public class Mail_followers_mail_message_subtype_relServiceImpl extends EBSServiceImpl<Mail_followers_mail_message_subtype_relMapper, Mail_followers_mail_message_subtype_rel> implements IMail_followers_mail_message_subtype_relService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_followersService mailFollowersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_message_subtypeService mailMessageSubtypeService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.followers.mail.message.subtype.rel" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_followers_mail_message_subtype_rel et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;

        if (messageinfo && !mail_create_nosubscribe && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack && false) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_followers_mail_message_subtype_rel> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_followers_mail_message_subtype_rel et) {
        Mail_followers_mail_message_subtype_rel old = new Mail_followers_mail_message_subtype_rel() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_followers_mail_message_subtype_relService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_followers_mail_message_subtype_relService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_followers_mail_message_subtype_rel> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_followers_mail_message_subtype_rel get(String key) {
        Mail_followers_mail_message_subtype_rel et = getById(key);
        if(et==null){
            et=new Mail_followers_mail_message_subtype_rel();
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_followers_mail_message_subtype_rel getDraft(Mail_followers_mail_message_subtype_rel et) {
        return et;
    }

    @Override
    public boolean checkKey(Mail_followers_mail_message_subtype_rel et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_followers_mail_message_subtype_rel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_followers_mail_message_subtype_rel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_followers_mail_message_subtype_rel> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_followers_mail_message_subtype_rel> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_followers_mail_message_subtype_rel> selectByMailFollowersId(Long id) {
        return baseMapper.selectByMailFollowersId(id);
    }
    @Override
    public void removeByMailFollowersId(Long id) {
        this.remove(new QueryWrapper<Mail_followers_mail_message_subtype_rel>().eq("mail_followers_id",id));
    }

	@Override
    public List<Mail_followers_mail_message_subtype_rel> selectByMailMessageSubtypeId(Long id) {
        return baseMapper.selectByMailMessageSubtypeId(id);
    }
    @Override
    public void removeByMailMessageSubtypeId(Long id) {
        this.remove(new QueryWrapper<Mail_followers_mail_message_subtype_rel>().eq("mail_message_subtype_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_followers_mail_message_subtype_rel> searchDefault(Mail_followers_mail_message_subtype_relSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_followers_mail_message_subtype_rel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_followers_mail_message_subtype_rel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



