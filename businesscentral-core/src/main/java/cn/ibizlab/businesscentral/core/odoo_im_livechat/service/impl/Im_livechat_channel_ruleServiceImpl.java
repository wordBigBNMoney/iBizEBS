package cn.ibizlab.businesscentral.core.odoo_im_livechat.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_channel_ruleService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.mapper.Im_livechat_channel_ruleMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[实时聊天频道规则] 服务对象接口实现
 */
@Slf4j
@Service("Im_livechat_channel_ruleServiceImpl")
public class Im_livechat_channel_ruleServiceImpl extends EBSServiceImpl<Im_livechat_channel_ruleMapper, Im_livechat_channel_rule> implements IIm_livechat_channel_ruleService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_channelService imLivechatChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "im.livechat.channel.rule" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Im_livechat_channel_rule et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIm_livechat_channel_ruleService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Im_livechat_channel_rule> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Im_livechat_channel_rule et) {
        Im_livechat_channel_rule old = new Im_livechat_channel_rule() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIm_livechat_channel_ruleService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIm_livechat_channel_ruleService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Im_livechat_channel_rule> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Im_livechat_channel_rule get(Long key) {
        Im_livechat_channel_rule et = getById(key);
        if(et==null){
            et=new Im_livechat_channel_rule();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Im_livechat_channel_rule getDraft(Im_livechat_channel_rule et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Im_livechat_channel_rule et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Im_livechat_channel_rule et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Im_livechat_channel_rule et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Im_livechat_channel_rule> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Im_livechat_channel_rule> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Im_livechat_channel_rule> selectByChannelId(Long id) {
        return baseMapper.selectByChannelId(id);
    }
    @Override
    public void resetByChannelId(Long id) {
        this.update(new UpdateWrapper<Im_livechat_channel_rule>().set("channel_id",null).eq("channel_id",id));
    }

    @Override
    public void resetByChannelId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Im_livechat_channel_rule>().set("channel_id",null).in("channel_id",ids));
    }

    @Override
    public void removeByChannelId(Long id) {
        this.remove(new QueryWrapper<Im_livechat_channel_rule>().eq("channel_id",id));
    }

	@Override
    public List<Im_livechat_channel_rule> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Im_livechat_channel_rule>().eq("create_uid",id));
    }

	@Override
    public List<Im_livechat_channel_rule> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Im_livechat_channel_rule>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Im_livechat_channel_rule> searchDefault(Im_livechat_channel_ruleSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Im_livechat_channel_rule> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Im_livechat_channel_rule>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Im_livechat_channel_rule et){
        //实体关系[DER1N_IM_LIVECHAT_CHANNEL_RULE__IM_LIVECHAT_CHANNEL__CHANNEL_ID]
        if(!ObjectUtils.isEmpty(et.getChannelId())){
            cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel odooChannel=et.getOdooChannel();
            if(ObjectUtils.isEmpty(odooChannel)){
                cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel majorEntity=imLivechatChannelService.get(et.getChannelId());
                et.setOdooChannel(majorEntity);
                odooChannel=majorEntity;
            }
            et.setChannelIdText(odooChannel.getName());
        }
        //实体关系[DER1N_IM_LIVECHAT_CHANNEL_RULE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_IM_LIVECHAT_CHANNEL_RULE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Im_livechat_channel_rule> getImLivechatChannelRuleByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Im_livechat_channel_rule> getImLivechatChannelRuleByEntities(List<Im_livechat_channel_rule> entities) {
        List ids =new ArrayList();
        for(Im_livechat_channel_rule entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



