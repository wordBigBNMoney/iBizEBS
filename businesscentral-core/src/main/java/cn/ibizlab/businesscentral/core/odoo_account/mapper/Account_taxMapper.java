package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_taxSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_taxMapper extends BaseMapper<Account_tax>{

    Page<Account_tax> searchDefault(IPage page, @Param("srf") Account_taxSearchContext context, @Param("ew") Wrapper<Account_tax> wrapper) ;
    Page<Account_tax> searchPurchase(IPage page, @Param("srf") Account_taxSearchContext context, @Param("ew") Wrapper<Account_tax> wrapper) ;
    Page<Account_tax> searchSale(IPage page, @Param("srf") Account_taxSearchContext context, @Param("ew") Wrapper<Account_tax> wrapper) ;
    @Override
    Account_tax selectById(Serializable id);
    @Override
    int insert(Account_tax entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_tax entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_tax entity, @Param("ew") Wrapper<Account_tax> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_tax> selectByCashBasisBaseAccountId(@Param("id") Serializable id) ;

    List<Account_tax> selectByTaxGroupId(@Param("id") Serializable id) ;

    List<Account_tax> selectByCompanyId(@Param("id") Serializable id) ;

    List<Account_tax> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_tax> selectByWriteUid(@Param("id") Serializable id) ;


}
