package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_aliasSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_alias] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-alias", fallback = mail_aliasFallback.class)
public interface mail_aliasFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_aliases/{id}")
    Mail_alias update(@PathVariable("id") Long id,@RequestBody Mail_alias mail_alias);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_aliases/batch")
    Boolean updateBatch(@RequestBody List<Mail_alias> mail_aliases);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_aliases/{id}")
    Mail_alias get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_aliases/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_aliases/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_aliases")
    Mail_alias create(@RequestBody Mail_alias mail_alias);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/batch")
    Boolean createBatch(@RequestBody List<Mail_alias> mail_aliases);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/search")
    Page<Mail_alias> search(@RequestBody Mail_aliasSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_aliases/select")
    Page<Mail_alias> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_aliases/getdraft")
    Mail_alias getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/checkkey")
    Boolean checkKey(@RequestBody Mail_alias mail_alias);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/save")
    Boolean save(@RequestBody Mail_alias mail_alias);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_alias> mail_aliases);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/searchdefault")
    Page<Mail_alias> searchDefault(@RequestBody Mail_aliasSearchContext context);


}
