package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_bomSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_bom] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-bom", fallback = mrp_bomFallback.class)
public interface mrp_bomFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_boms")
    Mrp_bom create(@RequestBody Mrp_bom mrp_bom);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/batch")
    Boolean createBatch(@RequestBody List<Mrp_bom> mrp_boms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_boms/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_boms/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_boms/{id}")
    Mrp_bom update(@PathVariable("id") Long id,@RequestBody Mrp_bom mrp_bom);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_boms/batch")
    Boolean updateBatch(@RequestBody List<Mrp_bom> mrp_boms);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_boms/{id}")
    Mrp_bom get(@PathVariable("id") Long id);






    @RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/search")
    Page<Mrp_bom> search(@RequestBody Mrp_bomSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_boms/select")
    Page<Mrp_bom> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_boms/getdraft")
    Mrp_bom getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/checkkey")
    Boolean checkKey(@RequestBody Mrp_bom mrp_bom);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/save")
    Boolean save(@RequestBody Mrp_bom mrp_bom);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_bom> mrp_boms);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/searchdefault")
    Page<Mrp_bom> searchDefault(@RequestBody Mrp_bomSearchContext context);


}
