package cn.ibizlab.businesscentral.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task_parts_line;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_task_parts_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mro.mapper.Mro_task_parts_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[Maintenance Planned Parts] 服务对象接口实现
 */
@Slf4j
@Service("Mro_task_parts_lineServiceImpl")
public class Mro_task_parts_lineServiceImpl extends EBSServiceImpl<Mro_task_parts_lineMapper, Mro_task_parts_line> implements IMro_task_parts_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_taskService mroTaskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mro.task.parts.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mro_task_parts_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_task_parts_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mro_task_parts_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mro_task_parts_line et) {
        Mro_task_parts_line old = new Mro_task_parts_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_task_parts_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_task_parts_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mro_task_parts_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mro_task_parts_line get(Long key) {
        Mro_task_parts_line et = getById(key);
        if(et==null){
            et=new Mro_task_parts_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mro_task_parts_line getDraft(Mro_task_parts_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mro_task_parts_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mro_task_parts_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mro_task_parts_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mro_task_parts_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mro_task_parts_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mro_task_parts_line> selectByTaskId(Long id) {
        return baseMapper.selectByTaskId(id);
    }
    @Override
    public void resetByTaskId(Long id) {
        this.update(new UpdateWrapper<Mro_task_parts_line>().set("task_id",null).eq("task_id",id));
    }

    @Override
    public void resetByTaskId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mro_task_parts_line>().set("task_id",null).in("task_id",ids));
    }

    @Override
    public void removeByTaskId(Long id) {
        this.remove(new QueryWrapper<Mro_task_parts_line>().eq("task_id",id));
    }

	@Override
    public List<Mro_task_parts_line> selectByPartsId(Long id) {
        return baseMapper.selectByPartsId(id);
    }
    @Override
    public void resetByPartsId(Long id) {
        this.update(new UpdateWrapper<Mro_task_parts_line>().set("parts_id",null).eq("parts_id",id));
    }

    @Override
    public void resetByPartsId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mro_task_parts_line>().set("parts_id",null).in("parts_id",ids));
    }

    @Override
    public void removeByPartsId(Long id) {
        this.remove(new QueryWrapper<Mro_task_parts_line>().eq("parts_id",id));
    }

	@Override
    public List<Mro_task_parts_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mro_task_parts_line>().eq("create_uid",id));
    }

	@Override
    public List<Mro_task_parts_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mro_task_parts_line>().eq("write_uid",id));
    }

	@Override
    public List<Mro_task_parts_line> selectByPartsUom(Long id) {
        return baseMapper.selectByPartsUom(id);
    }
    @Override
    public void resetByPartsUom(Long id) {
        this.update(new UpdateWrapper<Mro_task_parts_line>().set("parts_uom",null).eq("parts_uom",id));
    }

    @Override
    public void resetByPartsUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mro_task_parts_line>().set("parts_uom",null).in("parts_uom",ids));
    }

    @Override
    public void removeByPartsUom(Long id) {
        this.remove(new QueryWrapper<Mro_task_parts_line>().eq("parts_uom",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mro_task_parts_line> searchDefault(Mro_task_parts_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mro_task_parts_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mro_task_parts_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mro_task_parts_line et){
        //实体关系[DER1N_MRO_TASK_PARTS_LINE__MRO_TASK__TASK_ID]
        if(!ObjectUtils.isEmpty(et.getTaskId())){
            cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task odooTask=et.getOdooTask();
            if(ObjectUtils.isEmpty(odooTask)){
                cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task majorEntity=mroTaskService.get(et.getTaskId());
                et.setOdooTask(majorEntity);
                odooTask=majorEntity;
            }
            et.setTaskIdText(odooTask.getName());
        }
        //实体关系[DER1N_MRO_TASK_PARTS_LINE__PRODUCT_PRODUCT__PARTS_ID]
        if(!ObjectUtils.isEmpty(et.getPartsId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooParts=et.getOdooParts();
            if(ObjectUtils.isEmpty(odooParts)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getPartsId());
                et.setOdooParts(majorEntity);
                odooParts=majorEntity;
            }
            et.setPartsIdText(odooParts.getName());
        }
        //实体关系[DER1N_MRO_TASK_PARTS_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MRO_TASK_PARTS_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_MRO_TASK_PARTS_LINE__UOM_UOM__PARTS_UOM]
        if(!ObjectUtils.isEmpty(et.getPartsUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooPartsUom=et.getOdooPartsUom();
            if(ObjectUtils.isEmpty(odooPartsUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getPartsUom());
                et.setOdooPartsUom(majorEntity);
                odooPartsUom=majorEntity;
            }
            et.setPartsUomText(odooPartsUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mro_task_parts_line> getMroTaskPartsLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mro_task_parts_line> getMroTaskPartsLineByEntities(List<Mro_task_parts_line> entities) {
        List ids =new ArrayList();
        for(Mro_task_parts_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



