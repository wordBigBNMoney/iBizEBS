package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_accountSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_analytic_account] 服务对象接口
 */
@Component
public class account_analytic_accountFallback implements account_analytic_accountFeignClient{


    public Page<Account_analytic_account> search(Account_analytic_accountSearchContext context){
            return null;
     }


    public Account_analytic_account create(Account_analytic_account account_analytic_account){
            return null;
     }
    public Boolean createBatch(List<Account_analytic_account> account_analytic_accounts){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Account_analytic_account get(Long id){
            return null;
     }


    public Account_analytic_account update(Long id, Account_analytic_account account_analytic_account){
            return null;
     }
    public Boolean updateBatch(List<Account_analytic_account> account_analytic_accounts){
            return false;
     }


    public Page<Account_analytic_account> select(){
            return null;
     }

    public Account_analytic_account getDraft(){
            return null;
    }



    public Boolean checkKey(Account_analytic_account account_analytic_account){
            return false;
     }


    public Boolean save(Account_analytic_account account_analytic_account){
            return false;
     }
    public Boolean saveBatch(List<Account_analytic_account> account_analytic_accounts){
            return false;
     }

    public Page<Account_analytic_account> searchDefault(Account_analytic_accountSearchContext context){
            return null;
     }


}
