package cn.ibizlab.businesscentral.core.odoo_utm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_mixinSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Utm_mixin] 服务对象接口
 */
public interface IUtm_mixinService extends IService<Utm_mixin>{

    boolean create(Utm_mixin et) ;
    void createBatch(List<Utm_mixin> list) ;
    boolean update(Utm_mixin et) ;
    void updateBatch(List<Utm_mixin> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Utm_mixin get(Long key) ;
    Utm_mixin getDraft(Utm_mixin et) ;
    boolean checkKey(Utm_mixin et) ;
    boolean save(Utm_mixin et) ;
    void saveBatch(List<Utm_mixin> list) ;
    Page<Utm_mixin> searchDefault(Utm_mixinSearchContext context) ;
    List<Utm_mixin> selectByCampaignId(Long id);
    void resetByCampaignId(Long id);
    void resetByCampaignId(Collection<Long> ids);
    void removeByCampaignId(Long id);
    List<Utm_mixin> selectByMediumId(Long id);
    void resetByMediumId(Long id);
    void resetByMediumId(Collection<Long> ids);
    void removeByMediumId(Long id);
    List<Utm_mixin> selectBySourceId(Long id);
    void resetBySourceId(Long id);
    void resetBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


