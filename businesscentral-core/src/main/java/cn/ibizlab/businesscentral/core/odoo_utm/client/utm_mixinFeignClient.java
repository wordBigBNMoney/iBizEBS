package cn.ibizlab.businesscentral.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[utm_mixin] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-utm:odoo-utm}", contextId = "utm-mixin", fallback = utm_mixinFallback.class)
public interface utm_mixinFeignClient {



    @RequestMapping(method = RequestMethod.DELETE, value = "/utm_mixins/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/utm_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/utm_mixins")
    Utm_mixin create(@RequestBody Utm_mixin utm_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/batch")
    Boolean createBatch(@RequestBody List<Utm_mixin> utm_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/utm_mixins/{id}")
    Utm_mixin get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/search")
    Page<Utm_mixin> search(@RequestBody Utm_mixinSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/utm_mixins/{id}")
    Utm_mixin update(@PathVariable("id") Long id,@RequestBody Utm_mixin utm_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/utm_mixins/batch")
    Boolean updateBatch(@RequestBody List<Utm_mixin> utm_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/utm_mixins/select")
    Page<Utm_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/utm_mixins/getdraft")
    Utm_mixin getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/checkkey")
    Boolean checkKey(@RequestBody Utm_mixin utm_mixin);


    @RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/save")
    Boolean save(@RequestBody Utm_mixin utm_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/savebatch")
    Boolean saveBatch(@RequestBody List<Utm_mixin> utm_mixins);



    @RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/searchdefault")
    Page<Utm_mixin> searchDefault(@RequestBody Utm_mixinSearchContext context);


}
