package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_image;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_imageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_image] 服务对象接口
 */
public interface IProduct_imageService extends IService<Product_image>{

    boolean create(Product_image et) ;
    void createBatch(List<Product_image> list) ;
    boolean update(Product_image et) ;
    void updateBatch(List<Product_image> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_image get(Long key) ;
    Product_image getDraft(Product_image et) ;
    boolean checkKey(Product_image et) ;
    boolean save(Product_image et) ;
    void saveBatch(List<Product_image> list) ;
    Page<Product_image> searchDefault(Product_imageSearchContext context) ;
    List<Product_image> selectByProductTmplId(Long id);
    void resetByProductTmplId(Long id);
    void resetByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Product_image> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_image> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_image> getProductImageByIds(List<Long> ids) ;
    List<Product_image> getProductImageByEntities(List<Product_image> entities) ;
}


