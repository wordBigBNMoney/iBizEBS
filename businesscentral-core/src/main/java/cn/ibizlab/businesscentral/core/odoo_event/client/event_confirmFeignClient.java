package cn.ibizlab.businesscentral.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_confirmSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[event_confirm] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-event:odoo-event}", contextId = "event-confirm", fallback = event_confirmFallback.class)
public interface event_confirmFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/event_confirms/search")
    Page<Event_confirm> search(@RequestBody Event_confirmSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/event_confirms")
    Event_confirm create(@RequestBody Event_confirm event_confirm);

    @RequestMapping(method = RequestMethod.POST, value = "/event_confirms/batch")
    Boolean createBatch(@RequestBody List<Event_confirm> event_confirms);


    @RequestMapping(method = RequestMethod.PUT, value = "/event_confirms/{id}")
    Event_confirm update(@PathVariable("id") Long id,@RequestBody Event_confirm event_confirm);

    @RequestMapping(method = RequestMethod.PUT, value = "/event_confirms/batch")
    Boolean updateBatch(@RequestBody List<Event_confirm> event_confirms);




    @RequestMapping(method = RequestMethod.GET, value = "/event_confirms/{id}")
    Event_confirm get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/event_confirms/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/event_confirms/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/event_confirms/select")
    Page<Event_confirm> select();


    @RequestMapping(method = RequestMethod.GET, value = "/event_confirms/getdraft")
    Event_confirm getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/event_confirms/checkkey")
    Boolean checkKey(@RequestBody Event_confirm event_confirm);


    @RequestMapping(method = RequestMethod.POST, value = "/event_confirms/save")
    Boolean save(@RequestBody Event_confirm event_confirm);

    @RequestMapping(method = RequestMethod.POST, value = "/event_confirms/savebatch")
    Boolean saveBatch(@RequestBody List<Event_confirm> event_confirms);



    @RequestMapping(method = RequestMethod.POST, value = "/event_confirms/searchdefault")
    Page<Event_confirm> searchDefault(@RequestBody Event_confirmSearchContext context);


}
