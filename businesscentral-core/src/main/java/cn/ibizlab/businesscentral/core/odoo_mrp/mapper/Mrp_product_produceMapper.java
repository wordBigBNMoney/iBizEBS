package cn.ibizlab.businesscentral.core.odoo_mrp.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_product_produceSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mrp_product_produceMapper extends BaseMapper<Mrp_product_produce>{

    Page<Mrp_product_produce> searchDefault(IPage page, @Param("srf") Mrp_product_produceSearchContext context, @Param("ew") Wrapper<Mrp_product_produce> wrapper) ;
    @Override
    Mrp_product_produce selectById(Serializable id);
    @Override
    int insert(Mrp_product_produce entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mrp_product_produce entity);
    @Override
    int update(@Param(Constants.ENTITY) Mrp_product_produce entity, @Param("ew") Wrapper<Mrp_product_produce> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mrp_product_produce> selectByProductionId(@Param("id") Serializable id) ;

    List<Mrp_product_produce> selectByProductId(@Param("id") Serializable id) ;

    List<Mrp_product_produce> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mrp_product_produce> selectByWriteUid(@Param("id") Serializable id) ;

    List<Mrp_product_produce> selectByLotId(@Param("id") Serializable id) ;

    List<Mrp_product_produce> selectByProductUomId(@Param("id") Serializable id) ;


}
