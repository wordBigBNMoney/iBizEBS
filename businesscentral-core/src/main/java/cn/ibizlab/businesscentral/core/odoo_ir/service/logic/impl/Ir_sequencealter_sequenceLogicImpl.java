package cn.ibizlab.businesscentral.core.odoo_ir.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.odoo_ir.service.logic.IIr_sequencealter_sequenceLogic;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence;

/**
 * 关系型数据实体[alter_sequence] 对象
 */
@Slf4j
@Service
public class Ir_sequencealter_sequenceLogicImpl implements IIr_sequencealter_sequenceLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequenceService ir_sequenceservice;

    public cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequenceService getIr_sequenceService() {
        return this.ir_sequenceservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequenceService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequenceService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Ir_sequence et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("ir_sequencealter_sequencedefault",et);
           kieSession.setGlobal("ir_sequenceservice",ir_sequenceservice);
           kieSession.setGlobal("iBzSysIr_sequenceDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.odoo_ir.service.logic.ir_sequencealter_sequence");

        }catch(Exception e){
            throw new RuntimeException("执行[alter_sequence]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
