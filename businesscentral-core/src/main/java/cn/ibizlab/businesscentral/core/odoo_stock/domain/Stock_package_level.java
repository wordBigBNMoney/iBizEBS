package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[库存包装层级]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "STOCK_PACKAGE_LEVEL",resultMap = "Stock_package_levelResultMap")
public class Stock_package_level extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 凭证明细
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;
    /**
     * 移动
     */
    @TableField(exist = false)
    @JSONField(name = "move_ids")
    @JsonProperty("move_ids")
    private String moveIds;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 从
     */
    @TableField(exist = false)
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;
    /**
     * 显示批次文本
     */
    @TableField(exist = false)
    @JSONField(name = "show_lots_text")
    @JsonProperty("show_lots_text")
    private Boolean showLotsText;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 完成
     */
    @TableField(exist = false)
    @JSONField(name = "is_done")
    @JsonProperty("is_done")
    private Boolean isDone;
    /**
     * 显示批次M2O
     */
    @TableField(exist = false)
    @JSONField(name = "show_lots_m2o")
    @JsonProperty("show_lots_m2o")
    private Boolean showLotsM2o;
    /**
     * 状态
     */
    @TableField(exist = false)
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 是新鲜包裹
     */
    @TableField(exist = false)
    @JSONField(name = "is_fresh_package")
    @JsonProperty("is_fresh_package")
    private Boolean isFreshPackage;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 分拣
     */
    @TableField(exist = false)
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 包裹
     */
    @TableField(exist = false)
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    private String packageIdText;
    /**
     * 至
     */
    @TableField(exist = false)
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 源位置
     */
    @TableField(exist = false)
    @JSONField(name = "picking_source_location")
    @JsonProperty("picking_source_location")
    private Long pickingSourceLocation;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 分拣
     */
    @DEField(name = "picking_id")
    @TableField(value = "picking_id")
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Long pickingId;
    /**
     * 包裹
     */
    @DEField(name = "package_id")
    @TableField(value = "package_id")
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    private Long packageId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 至
     */
    @DEField(name = "location_dest_id")
    @TableField(value = "location_dest_id")
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Long locationDestId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooPicking;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package odooPackage;



    /**
     * 设置 [分拣]
     */
    public void setPickingId(Long pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }

    /**
     * 设置 [包裹]
     */
    public void setPackageId(Long packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }

    /**
     * 设置 [至]
     */
    public void setLocationDestId(Long locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


