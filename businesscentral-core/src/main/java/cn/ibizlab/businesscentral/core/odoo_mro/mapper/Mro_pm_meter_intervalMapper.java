package cn.ibizlab.businesscentral.core.odoo_mro.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mro_pm_meter_intervalMapper extends BaseMapper<Mro_pm_meter_interval>{

    Page<Mro_pm_meter_interval> searchDefault(IPage page, @Param("srf") Mro_pm_meter_intervalSearchContext context, @Param("ew") Wrapper<Mro_pm_meter_interval> wrapper) ;
    @Override
    Mro_pm_meter_interval selectById(Serializable id);
    @Override
    int insert(Mro_pm_meter_interval entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mro_pm_meter_interval entity);
    @Override
    int update(@Param(Constants.ENTITY) Mro_pm_meter_interval entity, @Param("ew") Wrapper<Mro_pm_meter_interval> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mro_pm_meter_interval> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mro_pm_meter_interval> selectByWriteUid(@Param("id") Serializable id) ;


}
