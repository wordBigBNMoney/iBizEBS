package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mail_statistics] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-mail-statistics", fallback = mail_mail_statisticsFallback.class)
public interface mail_mail_statisticsFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mail_statistics/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mail_statistics/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/search")
    Page<Mail_mail_statistics> search(@RequestBody Mail_mail_statisticsSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics")
    Mail_mail_statistics create(@RequestBody Mail_mail_statistics mail_mail_statistics);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/batch")
    Boolean createBatch(@RequestBody List<Mail_mail_statistics> mail_mail_statistics);




    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mail_statistics/{id}")
    Mail_mail_statistics update(@PathVariable("id") Long id,@RequestBody Mail_mail_statistics mail_mail_statistics);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mail_statistics/batch")
    Boolean updateBatch(@RequestBody List<Mail_mail_statistics> mail_mail_statistics);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/{id}")
    Mail_mail_statistics get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/select")
    Page<Mail_mail_statistics> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/getdraft")
    Mail_mail_statistics getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/checkkey")
    Boolean checkKey(@RequestBody Mail_mail_statistics mail_mail_statistics);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/save")
    Boolean save(@RequestBody Mail_mail_statistics mail_mail_statistics);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_mail_statistics> mail_mail_statistics);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/searchdefault")
    Page<Mail_mail_statistics> searchDefault(@RequestBody Mail_mail_statisticsSearchContext context);


}
