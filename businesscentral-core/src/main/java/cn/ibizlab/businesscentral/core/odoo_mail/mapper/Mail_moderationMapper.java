package cn.ibizlab.businesscentral.core.odoo_mail.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_moderationSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mail_moderationMapper extends BaseMapper<Mail_moderation>{

    Page<Mail_moderation> searchDefault(IPage page, @Param("srf") Mail_moderationSearchContext context, @Param("ew") Wrapper<Mail_moderation> wrapper) ;
    @Override
    Mail_moderation selectById(Serializable id);
    @Override
    int insert(Mail_moderation entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mail_moderation entity);
    @Override
    int update(@Param(Constants.ENTITY) Mail_moderation entity, @Param("ew") Wrapper<Mail_moderation> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mail_moderation> selectByChannelId(@Param("id") Serializable id) ;

    List<Mail_moderation> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mail_moderation> selectByWriteUid(@Param("id") Serializable id) ;


}
