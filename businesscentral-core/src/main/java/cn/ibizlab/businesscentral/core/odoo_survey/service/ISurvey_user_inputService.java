package cn.ibizlab.businesscentral.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_user_inputSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Survey_user_input] 服务对象接口
 */
public interface ISurvey_user_inputService extends IService<Survey_user_input>{

    boolean create(Survey_user_input et) ;
    void createBatch(List<Survey_user_input> list) ;
    boolean update(Survey_user_input et) ;
    void updateBatch(List<Survey_user_input> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Survey_user_input get(Long key) ;
    Survey_user_input getDraft(Survey_user_input et) ;
    boolean checkKey(Survey_user_input et) ;
    boolean save(Survey_user_input et) ;
    void saveBatch(List<Survey_user_input> list) ;
    Page<Survey_user_input> searchDefault(Survey_user_inputSearchContext context) ;
    List<Survey_user_input> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Survey_user_input> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Survey_user_input> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Survey_user_input> selectByLastDisplayedPageId(Long id);
    void resetByLastDisplayedPageId(Long id);
    void resetByLastDisplayedPageId(Collection<Long> ids);
    void removeByLastDisplayedPageId(Long id);
    List<Survey_user_input> selectBySurveyId(Long id);
    List<Survey_user_input> selectBySurveyId(Collection<Long> ids);
    void removeBySurveyId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Survey_user_input> getSurveyUserInputByIds(List<Long> ids) ;
    List<Survey_user_input> getSurveyUserInputByEntities(List<Survey_user_input> entities) ;
}


