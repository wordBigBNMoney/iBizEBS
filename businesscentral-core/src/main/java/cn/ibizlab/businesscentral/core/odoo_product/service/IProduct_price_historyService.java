package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_price_history;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_price_historySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_price_history] 服务对象接口
 */
public interface IProduct_price_historyService extends IService<Product_price_history>{

    boolean create(Product_price_history et) ;
    void createBatch(List<Product_price_history> list) ;
    boolean update(Product_price_history et) ;
    void updateBatch(List<Product_price_history> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_price_history get(Long key) ;
    Product_price_history getDraft(Product_price_history et) ;
    boolean checkKey(Product_price_history et) ;
    boolean save(Product_price_history et) ;
    void saveBatch(List<Product_price_history> list) ;
    Page<Product_price_history> searchDefault(Product_price_historySearchContext context) ;
    List<Product_price_history> selectByProductId(Long id);
    void removeByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Product_price_history> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Product_price_history> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_price_history> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_price_history> getProductPriceHistoryByIds(List<Long> ids) ;
    List<Product_price_history> getProductPriceHistoryByEntities(List<Product_price_history> entities) ;
}


