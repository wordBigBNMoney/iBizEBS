package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task_parts_line;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_task_parts_line] 服务对象接口
 */
public interface IMro_task_parts_lineService extends IService<Mro_task_parts_line>{

    boolean create(Mro_task_parts_line et) ;
    void createBatch(List<Mro_task_parts_line> list) ;
    boolean update(Mro_task_parts_line et) ;
    void updateBatch(List<Mro_task_parts_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_task_parts_line get(Long key) ;
    Mro_task_parts_line getDraft(Mro_task_parts_line et) ;
    boolean checkKey(Mro_task_parts_line et) ;
    boolean save(Mro_task_parts_line et) ;
    void saveBatch(List<Mro_task_parts_line> list) ;
    Page<Mro_task_parts_line> searchDefault(Mro_task_parts_lineSearchContext context) ;
    List<Mro_task_parts_line> selectByTaskId(Long id);
    void resetByTaskId(Long id);
    void resetByTaskId(Collection<Long> ids);
    void removeByTaskId(Long id);
    List<Mro_task_parts_line> selectByPartsId(Long id);
    void resetByPartsId(Long id);
    void resetByPartsId(Collection<Long> ids);
    void removeByPartsId(Long id);
    List<Mro_task_parts_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_task_parts_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mro_task_parts_line> selectByPartsUom(Long id);
    void resetByPartsUom(Long id);
    void resetByPartsUom(Collection<Long> ids);
    void removeByPartsUom(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_task_parts_line> getMroTaskPartsLineByIds(List<Long> ids) ;
    List<Mro_task_parts_line> getMroTaskPartsLineByEntities(List<Mro_task_parts_line> entities) ;
}


