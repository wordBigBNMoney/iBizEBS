package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_o2m_child;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_o2m_childSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_o2m_child] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base-import:odoo-base-import}", contextId = "base-import-tests-models-o2m-child", fallback = base_import_tests_models_o2m_childFallback.class)
public interface base_import_tests_models_o2m_childFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children")
    Base_import_tests_models_o2m_child create(@RequestBody Base_import_tests_models_o2m_child base_import_tests_models_o2m_child);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_o2m_child> base_import_tests_models_o2m_children);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2m_children/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2m_children/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);





    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2m_children/{id}")
    Base_import_tests_models_o2m_child get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2m_children/{id}")
    Base_import_tests_models_o2m_child update(@PathVariable("id") Long id,@RequestBody Base_import_tests_models_o2m_child base_import_tests_models_o2m_child);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2m_children/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_o2m_child> base_import_tests_models_o2m_children);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/search")
    Page<Base_import_tests_models_o2m_child> search(@RequestBody Base_import_tests_models_o2m_childSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2m_children/select")
    Page<Base_import_tests_models_o2m_child> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2m_children/getdraft")
    Base_import_tests_models_o2m_child getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/checkkey")
    Boolean checkKey(@RequestBody Base_import_tests_models_o2m_child base_import_tests_models_o2m_child);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/save")
    Boolean save(@RequestBody Base_import_tests_models_o2m_child base_import_tests_models_o2m_child);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/savebatch")
    Boolean saveBatch(@RequestBody List<Base_import_tests_models_o2m_child> base_import_tests_models_o2m_children);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/searchdefault")
    Page<Base_import_tests_models_o2m_child> searchDefault(@RequestBody Base_import_tests_models_o2m_childSearchContext context);


}
