package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_log_contract] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-vehicle-log-contract", fallback = fleet_vehicle_log_contractFallback.class)
public interface fleet_vehicle_log_contractFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_contracts/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_contracts/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/search")
    Page<Fleet_vehicle_log_contract> search(@RequestBody Fleet_vehicle_log_contractSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_contracts/{id}")
    Fleet_vehicle_log_contract update(@PathVariable("id") Long id,@RequestBody Fleet_vehicle_log_contract fleet_vehicle_log_contract);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_contracts/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_log_contract> fleet_vehicle_log_contracts);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_contracts/{id}")
    Fleet_vehicle_log_contract get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts")
    Fleet_vehicle_log_contract create(@RequestBody Fleet_vehicle_log_contract fleet_vehicle_log_contract);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_log_contract> fleet_vehicle_log_contracts);



    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_contracts/select")
    Page<Fleet_vehicle_log_contract> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_contracts/getdraft")
    Fleet_vehicle_log_contract getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/checkkey")
    Boolean checkKey(@RequestBody Fleet_vehicle_log_contract fleet_vehicle_log_contract);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/save")
    Boolean save(@RequestBody Fleet_vehicle_log_contract fleet_vehicle_log_contract);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_vehicle_log_contract> fleet_vehicle_log_contracts);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/searchdefault")
    Page<Fleet_vehicle_log_contract> searchDefault(@RequestBody Fleet_vehicle_log_contractSearchContext context);


}
