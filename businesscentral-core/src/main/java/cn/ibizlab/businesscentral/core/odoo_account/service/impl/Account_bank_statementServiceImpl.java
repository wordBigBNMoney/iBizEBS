package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statementSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statementService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_bank_statementMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[银行对账单] 服务对象接口实现
 */
@Slf4j
@Service("Account_bank_statementServiceImpl")
public class Account_bank_statementServiceImpl extends EBSServiceImpl<Account_bank_statementMapper, Account_bank_statement> implements IAccount_bank_statementService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_lineService accountBankStatementLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_cashboxService accountBankStatementCashboxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.bank.statement" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_bank_statement et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_bank_statementService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_bank_statement> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_bank_statement et) {
        Account_bank_statement old = new Account_bank_statement() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_bank_statementService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_bank_statementService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_bank_statement> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountBankStatementLineService.removeByStatementId(key);
        accountMoveLineService.resetByStatementId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountBankStatementLineService.removeByStatementId(idList);
        accountMoveLineService.resetByStatementId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_bank_statement get(Long key) {
        Account_bank_statement et = getById(key);
        if(et==null){
            et=new Account_bank_statement();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_bank_statement getDraft(Account_bank_statement et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_bank_statement et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_bank_statement et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_bank_statement et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_bank_statement> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_bank_statement> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_bank_statement> selectByCashboxEndId(Long id) {
        return baseMapper.selectByCashboxEndId(id);
    }
    @Override
    public void resetByCashboxEndId(Long id) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("cashbox_end_id",null).eq("cashbox_end_id",id));
    }

    @Override
    public void resetByCashboxEndId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("cashbox_end_id",null).in("cashbox_end_id",ids));
    }

    @Override
    public void removeByCashboxEndId(Long id) {
        this.remove(new QueryWrapper<Account_bank_statement>().eq("cashbox_end_id",id));
    }

	@Override
    public List<Account_bank_statement> selectByCashboxStartId(Long id) {
        return baseMapper.selectByCashboxStartId(id);
    }
    @Override
    public void resetByCashboxStartId(Long id) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("cashbox_start_id",null).eq("cashbox_start_id",id));
    }

    @Override
    public void resetByCashboxStartId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("cashbox_start_id",null).in("cashbox_start_id",ids));
    }

    @Override
    public void removeByCashboxStartId(Long id) {
        this.remove(new QueryWrapper<Account_bank_statement>().eq("cashbox_start_id",id));
    }

	@Override
    public List<Account_bank_statement> selectByJournalId(Long id) {
        return baseMapper.selectByJournalId(id);
    }
    @Override
    public void resetByJournalId(Long id) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("journal_id",null).eq("journal_id",id));
    }

    @Override
    public void resetByJournalId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("journal_id",null).in("journal_id",ids));
    }

    @Override
    public void removeByJournalId(Long id) {
        this.remove(new QueryWrapper<Account_bank_statement>().eq("journal_id",id));
    }

	@Override
    public List<Account_bank_statement> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_bank_statement>().eq("company_id",id));
    }

	@Override
    public List<Account_bank_statement> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_bank_statement>().eq("create_uid",id));
    }

	@Override
    public List<Account_bank_statement> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_bank_statement>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Account_bank_statement>().eq("user_id",id));
    }

	@Override
    public List<Account_bank_statement> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_bank_statement>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_bank_statement> searchDefault(Account_bank_statementSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_bank_statement> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_bank_statement>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_bank_statement et){
        //实体关系[DER1N_ACCOUNT_BANK_STATEMENT__ACCOUNT_JOURNAL__JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal=et.getOdooJournal();
            if(ObjectUtils.isEmpty(odooJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getJournalId());
                et.setOdooJournal(majorEntity);
                odooJournal=majorEntity;
            }
            et.setJournalIdText(odooJournal.getName());
            et.setJournalType(odooJournal.getType());
            et.setAccountId(odooJournal.getDefaultDebitAccountId());
        }
        //实体关系[DER1N_ACCOUNT_BANK_STATEMENT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_BANK_STATEMENT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_BANK_STATEMENT__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_ACCOUNT_BANK_STATEMENT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_bank_statement> getAccountBankStatementByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_bank_statement> getAccountBankStatementByEntities(List<Account_bank_statement> entities) {
        List ids =new ArrayList();
        for(Account_bank_statement entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



