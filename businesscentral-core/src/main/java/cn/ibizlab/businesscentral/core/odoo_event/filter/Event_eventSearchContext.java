package cn.ibizlab.businesscentral.core.odoo_event.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event;
/**
 * 关系型数据实体[Event_event] 查询条件对象
 */
@Slf4j
@Data
public class Event_eventSearchContext extends QueryWrapperContext<Event_event> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_seats_availability_eq;//[与会者最多人数]
	public void setN_seats_availability_eq(String n_seats_availability_eq) {
        this.n_seats_availability_eq = n_seats_availability_eq;
        if(!ObjectUtils.isEmpty(this.n_seats_availability_eq)){
            this.getSearchCond().eq("seats_availability", n_seats_availability_eq);
        }
    }
	private String n_name_like;//[活动]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_date_tz_eq;//[时区]
	public void setN_date_tz_eq(String n_date_tz_eq) {
        this.n_date_tz_eq = n_date_tz_eq;
        if(!ObjectUtils.isEmpty(this.n_date_tz_eq)){
            this.getSearchCond().eq("date_tz", n_date_tz_eq);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_event_type_id_text_eq;//[类别]
	public void setN_event_type_id_text_eq(String n_event_type_id_text_eq) {
        this.n_event_type_id_text_eq = n_event_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_event_type_id_text_eq)){
            this.getSearchCond().eq("event_type_id_text", n_event_type_id_text_eq);
        }
    }
	private String n_event_type_id_text_like;//[类别]
	public void setN_event_type_id_text_like(String n_event_type_id_text_like) {
        this.n_event_type_id_text_like = n_event_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_event_type_id_text_like)){
            this.getSearchCond().like("event_type_id_text", n_event_type_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_address_id_text_eq;//[地点]
	public void setN_address_id_text_eq(String n_address_id_text_eq) {
        this.n_address_id_text_eq = n_address_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_address_id_text_eq)){
            this.getSearchCond().eq("address_id_text", n_address_id_text_eq);
        }
    }
	private String n_address_id_text_like;//[地点]
	public void setN_address_id_text_like(String n_address_id_text_like) {
        this.n_address_id_text_like = n_address_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_address_id_text_like)){
            this.getSearchCond().like("address_id_text", n_address_id_text_like);
        }
    }
	private String n_country_id_text_eq;//[国家]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[国家]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private String n_menu_id_text_eq;//[活动菜单]
	public void setN_menu_id_text_eq(String n_menu_id_text_eq) {
        this.n_menu_id_text_eq = n_menu_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_menu_id_text_eq)){
            this.getSearchCond().eq("menu_id_text", n_menu_id_text_eq);
        }
    }
	private String n_menu_id_text_like;//[活动菜单]
	public void setN_menu_id_text_like(String n_menu_id_text_like) {
        this.n_menu_id_text_like = n_menu_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_menu_id_text_like)){
            this.getSearchCond().like("menu_id_text", n_menu_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[负责人]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[负责人]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_organizer_id_text_eq;//[组织者]
	public void setN_organizer_id_text_eq(String n_organizer_id_text_eq) {
        this.n_organizer_id_text_eq = n_organizer_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_organizer_id_text_eq)){
            this.getSearchCond().eq("organizer_id_text", n_organizer_id_text_eq);
        }
    }
	private String n_organizer_id_text_like;//[组织者]
	public void setN_organizer_id_text_like(String n_organizer_id_text_like) {
        this.n_organizer_id_text_like = n_organizer_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_organizer_id_text_like)){
            this.getSearchCond().like("organizer_id_text", n_organizer_id_text_like);
        }
    }
	private Long n_user_id_eq;//[负责人]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_country_id_eq;//[国家]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_organizer_id_eq;//[组织者]
	public void setN_organizer_id_eq(Long n_organizer_id_eq) {
        this.n_organizer_id_eq = n_organizer_id_eq;
        if(!ObjectUtils.isEmpty(this.n_organizer_id_eq)){
            this.getSearchCond().eq("organizer_id", n_organizer_id_eq);
        }
    }
	private Long n_menu_id_eq;//[活动菜单]
	public void setN_menu_id_eq(Long n_menu_id_eq) {
        this.n_menu_id_eq = n_menu_id_eq;
        if(!ObjectUtils.isEmpty(this.n_menu_id_eq)){
            this.getSearchCond().eq("menu_id", n_menu_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_address_id_eq;//[地点]
	public void setN_address_id_eq(Long n_address_id_eq) {
        this.n_address_id_eq = n_address_id_eq;
        if(!ObjectUtils.isEmpty(this.n_address_id_eq)){
            this.getSearchCond().eq("address_id", n_address_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_event_type_id_eq;//[类别]
	public void setN_event_type_id_eq(Long n_event_type_id_eq) {
        this.n_event_type_id_eq = n_event_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_event_type_id_eq)){
            this.getSearchCond().eq("event_type_id", n_event_type_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



