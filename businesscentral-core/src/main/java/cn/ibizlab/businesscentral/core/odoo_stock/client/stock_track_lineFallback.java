package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_track_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_track_line] 服务对象接口
 */
@Component
public class stock_track_lineFallback implements stock_track_lineFeignClient{


    public Stock_track_line get(Long id){
            return null;
     }


    public Stock_track_line create(Stock_track_line stock_track_line){
            return null;
     }
    public Boolean createBatch(List<Stock_track_line> stock_track_lines){
            return false;
     }


    public Stock_track_line update(Long id, Stock_track_line stock_track_line){
            return null;
     }
    public Boolean updateBatch(List<Stock_track_line> stock_track_lines){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Stock_track_line> search(Stock_track_lineSearchContext context){
            return null;
     }


    public Page<Stock_track_line> select(){
            return null;
     }

    public Stock_track_line getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_track_line stock_track_line){
            return false;
     }


    public Boolean save(Stock_track_line stock_track_line){
            return false;
     }
    public Boolean saveBatch(List<Stock_track_line> stock_track_lines){
            return false;
     }

    public Page<Stock_track_line> searchDefault(Stock_track_lineSearchContext context){
            return null;
     }


}
