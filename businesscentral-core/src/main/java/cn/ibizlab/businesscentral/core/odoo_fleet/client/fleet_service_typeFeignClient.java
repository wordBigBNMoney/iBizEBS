package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_service_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_service_type] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-service-type", fallback = fleet_service_typeFallback.class)
public interface fleet_service_typeFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_service_types/{id}")
    Fleet_service_type update(@PathVariable("id") Long id,@RequestBody Fleet_service_type fleet_service_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_service_types/batch")
    Boolean updateBatch(@RequestBody List<Fleet_service_type> fleet_service_types);



    @RequestMapping(method = RequestMethod.GET, value = "/fleet_service_types/{id}")
    Fleet_service_type get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types")
    Fleet_service_type create(@RequestBody Fleet_service_type fleet_service_type);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/batch")
    Boolean createBatch(@RequestBody List<Fleet_service_type> fleet_service_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_service_types/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_service_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/search")
    Page<Fleet_service_type> search(@RequestBody Fleet_service_typeSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_service_types/select")
    Page<Fleet_service_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_service_types/getdraft")
    Fleet_service_type getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/checkkey")
    Boolean checkKey(@RequestBody Fleet_service_type fleet_service_type);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/save")
    Boolean save(@RequestBody Fleet_service_type fleet_service_type);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_service_type> fleet_service_types);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/searchdefault")
    Page<Fleet_service_type> searchDefault(@RequestBody Fleet_service_typeSearchContext context);


}
