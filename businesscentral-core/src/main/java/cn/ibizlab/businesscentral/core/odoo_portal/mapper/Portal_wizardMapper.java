package cn.ibizlab.businesscentral.core.odoo_portal.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_wizardSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Portal_wizardMapper extends BaseMapper<Portal_wizard>{

    Page<Portal_wizard> searchDefault(IPage page, @Param("srf") Portal_wizardSearchContext context, @Param("ew") Wrapper<Portal_wizard> wrapper) ;
    @Override
    Portal_wizard selectById(Serializable id);
    @Override
    int insert(Portal_wizard entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Portal_wizard entity);
    @Override
    int update(@Param(Constants.ENTITY) Portal_wizard entity, @Param("ew") Wrapper<Portal_wizard> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Portal_wizard> selectByCreateUid(@Param("id") Serializable id) ;

    List<Portal_wizard> selectByWriteUid(@Param("id") Serializable id) ;


}
