package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[活动]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_ACTIVITY",resultMap = "Mail_activityResultMap")
public class Mail_activity extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 邮件模板
     */
    @TableField(exist = false)
    @JSONField(name = "mail_template_ids")
    @JsonProperty("mail_template_ids")
    private String mailTemplateIds;
    /**
     * 自动活动
     */
    @TableField(value = "automated")
    @JSONField(name = "automated")
    @JsonProperty("automated")
    private Boolean automated;
    /**
     * 文档名称
     */
    @DEField(name = "res_name")
    @TableField(value = "res_name")
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    private String resName;
    /**
     * 状态
     */
    @TableField(exist = false)
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 到期时间
     */
    @DEField(name = "date_deadline")
    @TableField(value = "date_deadline")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    private Timestamp dateDeadline;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 摘要
     */
    @TableField(value = "summary")
    @JSONField(name = "summary")
    @JsonProperty("summary")
    private String summary;
    /**
     * 下一活动可用
     */
    @TableField(exist = false)
    @JSONField(name = "has_recommended_activities")
    @JsonProperty("has_recommended_activities")
    private Boolean hasRecommendedActivities;
    /**
     * 相关文档编号
     */
    @DEField(name = "res_id")
    @TableField(value = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * 备注
     */
    @TableField(value = "note")
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;
    /**
     * 文档模型
     */
    @DEField(name = "res_model_id")
    @TableField(value = "res_model_id")
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;
    /**
     * 相关的文档模型
     */
    @DEField(name = "res_model")
    @TableField(value = "res_model")
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;
    /**
     * 分派给
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 类别
     */
    @TableField(exist = false)
    @JSONField(name = "activity_category")
    @JsonProperty("activity_category")
    private String activityCategory;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id_text")
    @JsonProperty("activity_type_id_text")
    private String activityTypeIdText;
    /**
     * 前一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "previous_activity_type_id_text")
    @JsonProperty("previous_activity_type_id_text")
    private String previousActivityTypeIdText;
    /**
     * 相关便签
     */
    @TableField(exist = false)
    @JSONField(name = "note_id_text")
    @JsonProperty("note_id_text")
    private String noteIdText;
    /**
     * 图标
     */
    @TableField(exist = false)
    @JSONField(name = "icon")
    @JsonProperty("icon")
    private String icon;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 排版类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_decoration")
    @JsonProperty("activity_decoration")
    private String activityDecoration;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 自动安排下一个活动
     */
    @TableField(exist = false)
    @JSONField(name = "force_next")
    @JsonProperty("force_next")
    private Boolean forceNext;
    /**
     * 推荐的活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "recommended_activity_type_id_text")
    @JsonProperty("recommended_activity_type_id_text")
    private String recommendedActivityTypeIdText;
    /**
     * 日历会议
     */
    @TableField(exist = false)
    @JSONField(name = "calendar_event_id_text")
    @JsonProperty("calendar_event_id_text")
    private String calendarEventIdText;
    /**
     * 推荐的活动类型
     */
    @DEField(name = "recommended_activity_type_id")
    @TableField(value = "recommended_activity_type_id")
    @JSONField(name = "recommended_activity_type_id")
    @JsonProperty("recommended_activity_type_id")
    private Long recommendedActivityTypeId;
    /**
     * 活动
     */
    @DEField(name = "activity_type_id")
    @TableField(value = "activity_type_id")
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Long activityTypeId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 分派给
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 相关便签
     */
    @DEField(name = "note_id")
    @TableField(value = "note_id")
    @JSONField(name = "note_id")
    @JsonProperty("note_id")
    private Long noteId;
    /**
     * 前一活动类型
     */
    @DEField(name = "previous_activity_type_id")
    @TableField(value = "previous_activity_type_id")
    @JSONField(name = "previous_activity_type_id")
    @JsonProperty("previous_activity_type_id")
    private Long previousActivityTypeId;
    /**
     * 日历会议
     */
    @DEField(name = "calendar_event_id")
    @TableField(value = "calendar_event_id")
    @JSONField(name = "calendar_event_id")
    @JsonProperty("calendar_event_id")
    private Long calendarEventId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event odooCalendarEvent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type odooActivityType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type odooPreviousActivityType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type odooRecommendedActivityType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_note.domain.Note_note odooNote;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [自动活动]
     */
    public void setAutomated(Boolean automated){
        this.automated = automated ;
        this.modify("automated",automated);
    }

    /**
     * 设置 [文档名称]
     */
    public void setResName(String resName){
        this.resName = resName ;
        this.modify("res_name",resName);
    }

    /**
     * 设置 [到期时间]
     */
    public void setDateDeadline(Timestamp dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }

    /**
     * 格式化日期 [到期时间]
     */
    public String formatDateDeadline(){
        if (this.dateDeadline == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateDeadline);
    }
    /**
     * 设置 [摘要]
     */
    public void setSummary(String summary){
        this.summary = summary ;
        this.modify("summary",summary);
    }

    /**
     * 设置 [相关文档编号]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [文档模型]
     */
    public void setResModelId(Integer resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }

    /**
     * 设置 [相关的文档模型]
     */
    public void setResModel(String resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [推荐的活动类型]
     */
    public void setRecommendedActivityTypeId(Long recommendedActivityTypeId){
        this.recommendedActivityTypeId = recommendedActivityTypeId ;
        this.modify("recommended_activity_type_id",recommendedActivityTypeId);
    }

    /**
     * 设置 [活动]
     */
    public void setActivityTypeId(Long activityTypeId){
        this.activityTypeId = activityTypeId ;
        this.modify("activity_type_id",activityTypeId);
    }

    /**
     * 设置 [分派给]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [相关便签]
     */
    public void setNoteId(Long noteId){
        this.noteId = noteId ;
        this.modify("note_id",noteId);
    }

    /**
     * 设置 [前一活动类型]
     */
    public void setPreviousActivityTypeId(Long previousActivityTypeId){
        this.previousActivityTypeId = previousActivityTypeId ;
        this.modify("previous_activity_type_id",previousActivityTypeId);
    }

    /**
     * 设置 [日历会议]
     */
    public void setCalendarEventId(Long calendarEventId){
        this.calendarEventId = calendarEventId ;
        this.modify("calendar_event_id",calendarEventId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


