package cn.ibizlab.businesscentral.core.odoo_base_import.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_charSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_import_tests_models_char] 服务对象接口
 */
public interface IBase_import_tests_models_charService extends IService<Base_import_tests_models_char>{

    boolean create(Base_import_tests_models_char et) ;
    void createBatch(List<Base_import_tests_models_char> list) ;
    boolean update(Base_import_tests_models_char et) ;
    void updateBatch(List<Base_import_tests_models_char> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_import_tests_models_char get(Long key) ;
    Base_import_tests_models_char getDraft(Base_import_tests_models_char et) ;
    boolean checkKey(Base_import_tests_models_char et) ;
    boolean save(Base_import_tests_models_char et) ;
    void saveBatch(List<Base_import_tests_models_char> list) ;
    Page<Base_import_tests_models_char> searchDefault(Base_import_tests_models_charSearchContext context) ;
    List<Base_import_tests_models_char> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_import_tests_models_char> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_import_tests_models_char> getBaseImportTestsModelsCharByIds(List<Long> ids) ;
    List<Base_import_tests_models_char> getBaseImportTestsModelsCharByEntities(List<Base_import_tests_models_char> entities) ;
}


