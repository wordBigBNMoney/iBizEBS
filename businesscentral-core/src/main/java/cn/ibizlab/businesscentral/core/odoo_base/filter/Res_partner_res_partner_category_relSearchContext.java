package cn.ibizlab.businesscentral.core.odoo_base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_res_partner_category_rel;
/**
 * 关系型数据实体[Res_partner_res_partner_category_rel] 查询条件对象
 */
@Slf4j
@Data
public class Res_partner_res_partner_category_relSearchContext extends QueryWrapperContext<Res_partner_res_partner_category_rel> {

	private String n_id_like;//[ID]
	public void setN_id_like(String n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private Long n_partner_id_eq;//[ID]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_category_id_eq;//[ID]
	public void setN_category_id_eq(Long n_category_id_eq) {
        this.n_category_id_eq = n_category_id_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_eq)){
            this.getSearchCond().eq("category_id", n_category_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



