package cn.ibizlab.businesscentral.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface IPayment_acquirer_onboarding_wizardService extends IService<Payment_acquirer_onboarding_wizard>{

    boolean create(Payment_acquirer_onboarding_wizard et) ;
    void createBatch(List<Payment_acquirer_onboarding_wizard> list) ;
    boolean update(Payment_acquirer_onboarding_wizard et) ;
    void updateBatch(List<Payment_acquirer_onboarding_wizard> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Payment_acquirer_onboarding_wizard get(Long key) ;
    Payment_acquirer_onboarding_wizard getDraft(Payment_acquirer_onboarding_wizard et) ;
    boolean checkKey(Payment_acquirer_onboarding_wizard et) ;
    boolean save(Payment_acquirer_onboarding_wizard et) ;
    void saveBatch(List<Payment_acquirer_onboarding_wizard> list) ;
    Page<Payment_acquirer_onboarding_wizard> searchDefault(Payment_acquirer_onboarding_wizardSearchContext context) ;
    List<Payment_acquirer_onboarding_wizard> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Payment_acquirer_onboarding_wizard> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Payment_acquirer_onboarding_wizard> getPaymentAcquirerOnboardingWizardByIds(List<Long> ids) ;
    List<Payment_acquirer_onboarding_wizard> getPaymentAcquirerOnboardingWizardByEntities(List<Payment_acquirer_onboarding_wizard> entities) ;
}


