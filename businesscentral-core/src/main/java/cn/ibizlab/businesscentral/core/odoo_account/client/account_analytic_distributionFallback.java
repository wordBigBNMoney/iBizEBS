package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_distributionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_analytic_distribution] 服务对象接口
 */
@Component
public class account_analytic_distributionFallback implements account_analytic_distributionFeignClient{



    public Account_analytic_distribution create(Account_analytic_distribution account_analytic_distribution){
            return null;
     }
    public Boolean createBatch(List<Account_analytic_distribution> account_analytic_distributions){
            return false;
     }

    public Account_analytic_distribution update(Long id, Account_analytic_distribution account_analytic_distribution){
            return null;
     }
    public Boolean updateBatch(List<Account_analytic_distribution> account_analytic_distributions){
            return false;
     }



    public Page<Account_analytic_distribution> search(Account_analytic_distributionSearchContext context){
            return null;
     }


    public Account_analytic_distribution get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_analytic_distribution> select(){
            return null;
     }

    public Account_analytic_distribution getDraft(){
            return null;
    }



    public Boolean checkKey(Account_analytic_distribution account_analytic_distribution){
            return false;
     }


    public Boolean save(Account_analytic_distribution account_analytic_distribution){
            return false;
     }
    public Boolean saveBatch(List<Account_analytic_distribution> account_analytic_distributions){
            return false;
     }

    public Page<Account_analytic_distribution> searchDefault(Account_analytic_distributionSearchContext context){
            return null;
     }


}
