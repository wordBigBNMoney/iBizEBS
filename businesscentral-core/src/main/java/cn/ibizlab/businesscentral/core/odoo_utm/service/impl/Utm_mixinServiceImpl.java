package cn.ibizlab.businesscentral.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_mixinSearchContext;
import cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mixinService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_utm.mapper.Utm_mixinMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[UTM Mixin] 服务对象接口实现
 */
@Slf4j
@Service("Utm_mixinServiceImpl")
public class Utm_mixinServiceImpl extends EBSServiceImpl<Utm_mixinMapper, Utm_mixin> implements IUtm_mixinService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_campaignService utmCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mediumService utmMediumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService utmSourceService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "utm.mixin" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Utm_mixin et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IUtm_mixinService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Utm_mixin> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Utm_mixin et) {
        Utm_mixin old = new Utm_mixin() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IUtm_mixinService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IUtm_mixinService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Utm_mixin> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Utm_mixin get(Long key) {
        Utm_mixin et = getById(key);
        if(et==null){
            et=new Utm_mixin();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Utm_mixin getDraft(Utm_mixin et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Utm_mixin et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Utm_mixin et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Utm_mixin et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Utm_mixin> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Utm_mixin> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Utm_mixin> selectByCampaignId(Long id) {
        return baseMapper.selectByCampaignId(id);
    }
    @Override
    public void resetByCampaignId(Long id) {
        this.update(new UpdateWrapper<Utm_mixin>().set("campaign_id",null).eq("campaign_id",id));
    }

    @Override
    public void resetByCampaignId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Utm_mixin>().set("campaign_id",null).in("campaign_id",ids));
    }

    @Override
    public void removeByCampaignId(Long id) {
        this.remove(new QueryWrapper<Utm_mixin>().eq("campaign_id",id));
    }

	@Override
    public List<Utm_mixin> selectByMediumId(Long id) {
        return baseMapper.selectByMediumId(id);
    }
    @Override
    public void resetByMediumId(Long id) {
        this.update(new UpdateWrapper<Utm_mixin>().set("medium_id",null).eq("medium_id",id));
    }

    @Override
    public void resetByMediumId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Utm_mixin>().set("medium_id",null).in("medium_id",ids));
    }

    @Override
    public void removeByMediumId(Long id) {
        this.remove(new QueryWrapper<Utm_mixin>().eq("medium_id",id));
    }

	@Override
    public List<Utm_mixin> selectBySourceId(Long id) {
        return baseMapper.selectBySourceId(id);
    }
    @Override
    public void resetBySourceId(Long id) {
        this.update(new UpdateWrapper<Utm_mixin>().set("source_id",null).eq("source_id",id));
    }

    @Override
    public void resetBySourceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Utm_mixin>().set("source_id",null).in("source_id",ids));
    }

    @Override
    public void removeBySourceId(Long id) {
        this.remove(new QueryWrapper<Utm_mixin>().eq("source_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Utm_mixin> searchDefault(Utm_mixinSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Utm_mixin> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Utm_mixin>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Utm_mixin et){
        //实体关系[DER1N_UTM_MIXIN__UTM_CAMPAIGN__CAMPAIGN_ID]
        if(!ObjectUtils.isEmpty(et.getCampaignId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign=et.getOdooCampaign();
            if(ObjectUtils.isEmpty(odooCampaign)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign majorEntity=utmCampaignService.get(et.getCampaignId());
                et.setOdooCampaign(majorEntity);
                odooCampaign=majorEntity;
            }
            et.setCampaignIdText(odooCampaign.getName());
        }
        //实体关系[DER1N_UTM_MIXIN__UTM_MEDIUM__MEDIUM_ID]
        if(!ObjectUtils.isEmpty(et.getMediumId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium=et.getOdooMedium();
            if(ObjectUtils.isEmpty(odooMedium)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium majorEntity=utmMediumService.get(et.getMediumId());
                et.setOdooMedium(majorEntity);
                odooMedium=majorEntity;
            }
            et.setMediumIdText(odooMedium.getName());
        }
        //实体关系[DER1N_UTM_MIXIN__UTM_SOURCE__SOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getSourceId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource=et.getOdooSource();
            if(ObjectUtils.isEmpty(odooSource)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source majorEntity=utmSourceService.get(et.getSourceId());
                et.setOdooSource(majorEntity);
                odooSource=majorEntity;
            }
            et.setSourceIdText(odooSource.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



