package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_usersSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Res_usersMapper extends BaseMapper<Res_users>{

    Page<Res_users> searchActive(IPage page, @Param("srf") Res_usersSearchContext context, @Param("ew") Wrapper<Res_users> wrapper) ;
    Page<Res_users> searchDefault(IPage page, @Param("srf") Res_usersSearchContext context, @Param("ew") Wrapper<Res_users> wrapper) ;
    @Override
    Res_users selectById(Serializable id);
    @Override
    int insert(Res_users entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Res_users entity);
    @Override
    int update(@Param(Constants.ENTITY) Res_users entity, @Param("ew") Wrapper<Res_users> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Res_users> selectBySaleTeamId(@Param("id") Serializable id) ;

    List<Res_users> selectByAliasId(@Param("id") Serializable id) ;

    List<Res_users> selectByCompanyId(@Param("id") Serializable id) ;

    List<Res_users> selectByPartnerId(@Param("id") Serializable id) ;

    List<Res_users> selectByCreateUid(@Param("id") Serializable id) ;

    List<Res_users> selectByWriteUid(@Param("id") Serializable id) ;


}
