package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_cashbox_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_cashbox_lineSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_cashbox_lineMapper extends BaseMapper<Account_cashbox_line>{

    Page<Account_cashbox_line> searchDefault(IPage page, @Param("srf") Account_cashbox_lineSearchContext context, @Param("ew") Wrapper<Account_cashbox_line> wrapper) ;
    @Override
    Account_cashbox_line selectById(Serializable id);
    @Override
    int insert(Account_cashbox_line entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_cashbox_line entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_cashbox_line entity, @Param("ew") Wrapper<Account_cashbox_line> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_cashbox_line> selectByCashboxId(@Param("id") Serializable id) ;

    List<Account_cashbox_line> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_cashbox_line> selectByWriteUid(@Param("id") Serializable id) ;


}
