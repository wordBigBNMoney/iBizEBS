package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_taxSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_invoice_tax] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-invoice-tax", fallback = account_invoice_taxFallback.class)
public interface account_invoice_taxFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/search")
    Page<Account_invoice_tax> search(@RequestBody Account_invoice_taxSearchContext context);





    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_taxes/{id}")
    Account_invoice_tax update(@PathVariable("id") Long id,@RequestBody Account_invoice_tax account_invoice_tax);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_taxes/batch")
    Boolean updateBatch(@RequestBody List<Account_invoice_tax> account_invoice_taxes);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_taxes/{id}")
    Account_invoice_tax get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_taxes/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_taxes/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes")
    Account_invoice_tax create(@RequestBody Account_invoice_tax account_invoice_tax);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/batch")
    Boolean createBatch(@RequestBody List<Account_invoice_tax> account_invoice_taxes);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_taxes/select")
    Page<Account_invoice_tax> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_taxes/getdraft")
    Account_invoice_tax getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/checkkey")
    Boolean checkKey(@RequestBody Account_invoice_tax account_invoice_tax);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/save")
    Boolean save(@RequestBody Account_invoice_tax account_invoice_tax);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/savebatch")
    Boolean saveBatch(@RequestBody List<Account_invoice_tax> account_invoice_taxes);



    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/searchdefault")
    Page<Account_invoice_tax> searchDefault(@RequestBody Account_invoice_taxSearchContext context);


}
