package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_supplier_taxes_rel;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_supplier_taxes_relSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_supplier_taxes_rel] 服务对象接口
 */
public interface IProduct_supplier_taxes_relService extends IService<Product_supplier_taxes_rel>{

    boolean create(Product_supplier_taxes_rel et) ;
    void createBatch(List<Product_supplier_taxes_rel> list) ;
    boolean update(Product_supplier_taxes_rel et) ;
    void updateBatch(List<Product_supplier_taxes_rel> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_supplier_taxes_rel get(Long key) ;
    Product_supplier_taxes_rel getDraft(Product_supplier_taxes_rel et) ;
    boolean checkKey(Product_supplier_taxes_rel et) ;
    boolean save(Product_supplier_taxes_rel et) ;
    void saveBatch(List<Product_supplier_taxes_rel> list) ;
    Page<Product_supplier_taxes_rel> searchDefault(Product_supplier_taxes_relSearchContext context) ;
    List<Product_supplier_taxes_rel> selectByTaxId(Long id);
    void removeByTaxId(Long id);
    List<Product_supplier_taxes_rel> selectByProdId(Long id);
    void removeByProdId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


