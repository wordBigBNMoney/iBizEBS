package cn.ibizlab.businesscentral.core.odoo_repair.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[维修单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "REPAIR_ORDER",resultMap = "Repair_orderResultMap")
public class Repair_order extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 内部备注
     */
    @DEField(name = "internal_notes")
    @TableField(value = "internal_notes")
    @JSONField(name = "internal_notes")
    @JsonProperty("internal_notes")
    private String internalNotes;
    /**
     * 默认地址
     */
    @TableField(exist = false)
    @JSONField(name = "default_address_id")
    @JsonProperty("default_address_id")
    private Integer defaultAddressId;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 开票方式
     */
    @DEField(name = "invoice_method")
    @TableField(value = "invoice_method")
    @JSONField(name = "invoice_method")
    @JsonProperty("invoice_method")
    private String invoiceMethod;
    /**
     * 已开票
     */
    @TableField(value = "invoiced")
    @JSONField(name = "invoiced")
    @JsonProperty("invoiced")
    private Boolean invoiced;
    /**
     * 报价单说明
     */
    @DEField(name = "quotation_notes")
    @TableField(value = "quotation_notes")
    @JSONField(name = "quotation_notes")
    @JsonProperty("quotation_notes")
    private String quotationNotes;
    /**
     * 维修参照
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 合计
     */
    @DEField(name = "amount_total")
    @TableField(value = "amount_total")
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private Double amountTotal;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 网站信息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 零件
     */
    @TableField(exist = false)
    @JSONField(name = "operations")
    @JsonProperty("operations")
    private String operations;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 作业
     */
    @TableField(exist = false)
    @JSONField(name = "fees_lines")
    @JsonProperty("fees_lines")
    private String feesLines;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 已维修
     */
    @TableField(value = "repaired")
    @JSONField(name = "repaired")
    @JsonProperty("repaired")
    private Boolean repaired;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 未税金额
     */
    @DEField(name = "amount_untaxed")
    @TableField(value = "amount_untaxed")
    @JSONField(name = "amount_untaxed")
    @JsonProperty("amount_untaxed")
    private Double amountUntaxed;
    /**
     * 质保到期
     */
    @DEField(name = "guarantee_limit")
    @TableField(value = "guarantee_limit")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "guarantee_limit" , format="yyyy-MM-dd")
    @JsonProperty("guarantee_limit")
    private Timestamp guaranteeLimit;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后修改时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 税
     */
    @DEField(name = "amount_tax")
    @TableField(value = "amount_tax")
    @JSONField(name = "amount_tax")
    @JsonProperty("amount_tax")
    private Double amountTax;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 数量
     */
    @DEField(name = "product_qty")
    @TableField(value = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;
    /**
     * 价格表
     */
    @TableField(exist = false)
    @JSONField(name = "pricelist_id_text")
    @JsonProperty("pricelist_id_text")
    private String pricelistIdText;
    /**
     * 批次/序列号
     */
    @TableField(exist = false)
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    private String lotIdText;
    /**
     * 客户
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 待维修产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;
    /**
     * 产品量度单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 创建者
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 开票地址
     */
    @TableField(exist = false)
    @JSONField(name = "partner_invoice_id_text")
    @JsonProperty("partner_invoice_id_text")
    private String partnerInvoiceIdText;
    /**
     * 收货地址
     */
    @TableField(exist = false)
    @JSONField(name = "address_id_text")
    @JsonProperty("address_id_text")
    private String addressIdText;
    /**
     * 地点
     */
    @TableField(exist = false)
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;
    /**
     * 追踪
     */
    @TableField(exist = false)
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 移动
     */
    @TableField(exist = false)
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;
    /**
     * 移动
     */
    @DEField(name = "move_id")
    @TableField(value = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Long moveId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 价格表
     */
    @DEField(name = "pricelist_id")
    @TableField(value = "pricelist_id")
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Long pricelistId;
    /**
     * 待维修产品
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 开票地址
     */
    @DEField(name = "partner_invoice_id")
    @TableField(value = "partner_invoice_id")
    @JSONField(name = "partner_invoice_id")
    @JsonProperty("partner_invoice_id")
    private Long partnerInvoiceId;
    /**
     * 地点
     */
    @DEField(name = "location_id")
    @TableField(value = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Long locationId;
    /**
     * 产品量度单位
     */
    @DEField(name = "product_uom")
    @TableField(value = "product_uom")
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Long productUom;
    /**
     * 创建者
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 收货地址
     */
    @DEField(name = "address_id")
    @TableField(value = "address_id")
    @JSONField(name = "address_id")
    @JsonProperty("address_id")
    private Long addressId;
    /**
     * 发票
     */
    @DEField(name = "invoice_id")
    @TableField(value = "invoice_id")
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Long invoiceId;
    /**
     * 批次/序列号
     */
    @DEField(name = "lot_id")
    @TableField(value = "lot_id")
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    private Long lotId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooInvoice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAddress;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerInvoice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooMove;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLot;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom;



    /**
     * 设置 [内部备注]
     */
    public void setInternalNotes(String internalNotes){
        this.internalNotes = internalNotes ;
        this.modify("internal_notes",internalNotes);
    }

    /**
     * 设置 [开票方式]
     */
    public void setInvoiceMethod(String invoiceMethod){
        this.invoiceMethod = invoiceMethod ;
        this.modify("invoice_method",invoiceMethod);
    }

    /**
     * 设置 [已开票]
     */
    public void setInvoiced(Boolean invoiced){
        this.invoiced = invoiced ;
        this.modify("invoiced",invoiced);
    }

    /**
     * 设置 [报价单说明]
     */
    public void setQuotationNotes(String quotationNotes){
        this.quotationNotes = quotationNotes ;
        this.modify("quotation_notes",quotationNotes);
    }

    /**
     * 设置 [维修参照]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [合计]
     */
    public void setAmountTotal(Double amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [已维修]
     */
    public void setRepaired(Boolean repaired){
        this.repaired = repaired ;
        this.modify("repaired",repaired);
    }

    /**
     * 设置 [未税金额]
     */
    public void setAmountUntaxed(Double amountUntaxed){
        this.amountUntaxed = amountUntaxed ;
        this.modify("amount_untaxed",amountUntaxed);
    }

    /**
     * 设置 [质保到期]
     */
    public void setGuaranteeLimit(Timestamp guaranteeLimit){
        this.guaranteeLimit = guaranteeLimit ;
        this.modify("guarantee_limit",guaranteeLimit);
    }

    /**
     * 格式化日期 [质保到期]
     */
    public String formatGuaranteeLimit(){
        if (this.guaranteeLimit == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(guaranteeLimit);
    }
    /**
     * 设置 [税]
     */
    public void setAmountTax(Double amountTax){
        this.amountTax = amountTax ;
        this.modify("amount_tax",amountTax);
    }

    /**
     * 设置 [数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [移动]
     */
    public void setMoveId(Long moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [客户]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [价格表]
     */
    public void setPricelistId(Long pricelistId){
        this.pricelistId = pricelistId ;
        this.modify("pricelist_id",pricelistId);
    }

    /**
     * 设置 [待维修产品]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [开票地址]
     */
    public void setPartnerInvoiceId(Long partnerInvoiceId){
        this.partnerInvoiceId = partnerInvoiceId ;
        this.modify("partner_invoice_id",partnerInvoiceId);
    }

    /**
     * 设置 [地点]
     */
    public void setLocationId(Long locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [产品量度单位]
     */
    public void setProductUom(Long productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [收货地址]
     */
    public void setAddressId(Long addressId){
        this.addressId = addressId ;
        this.modify("address_id",addressId);
    }

    /**
     * 设置 [发票]
     */
    public void setInvoiceId(Long invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }

    /**
     * 设置 [批次/序列号]
     */
    public void setLotId(Long lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


