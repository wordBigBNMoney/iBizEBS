package cn.ibizlab.businesscentral.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[resource_calendar_leaves] 服务对象接口
 */
@Component
public class resource_calendar_leavesFallback implements resource_calendar_leavesFeignClient{


    public Resource_calendar_leaves update(Long id, Resource_calendar_leaves resource_calendar_leaves){
            return null;
     }
    public Boolean updateBatch(List<Resource_calendar_leaves> resource_calendar_leaves){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Resource_calendar_leaves get(Long id){
            return null;
     }


    public Page<Resource_calendar_leaves> search(Resource_calendar_leavesSearchContext context){
            return null;
     }


    public Resource_calendar_leaves create(Resource_calendar_leaves resource_calendar_leaves){
            return null;
     }
    public Boolean createBatch(List<Resource_calendar_leaves> resource_calendar_leaves){
            return false;
     }



    public Page<Resource_calendar_leaves> select(){
            return null;
     }

    public Resource_calendar_leaves getDraft(){
            return null;
    }



    public Boolean checkKey(Resource_calendar_leaves resource_calendar_leaves){
            return false;
     }


    public Boolean save(Resource_calendar_leaves resource_calendar_leaves){
            return false;
     }
    public Boolean saveBatch(List<Resource_calendar_leaves> resource_calendar_leaves){
            return false;
     }

    public Page<Resource_calendar_leaves> searchDefault(Resource_calendar_leavesSearchContext context){
            return null;
     }


}
