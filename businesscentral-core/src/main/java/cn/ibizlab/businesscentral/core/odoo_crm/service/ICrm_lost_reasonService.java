package cn.ibizlab.businesscentral.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lost_reasonSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Crm_lost_reason] 服务对象接口
 */
public interface ICrm_lost_reasonService extends IService<Crm_lost_reason>{

    boolean create(Crm_lost_reason et) ;
    void createBatch(List<Crm_lost_reason> list) ;
    boolean update(Crm_lost_reason et) ;
    void updateBatch(List<Crm_lost_reason> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Crm_lost_reason get(Long key) ;
    Crm_lost_reason getDraft(Crm_lost_reason et) ;
    boolean checkKey(Crm_lost_reason et) ;
    boolean save(Crm_lost_reason et) ;
    void saveBatch(List<Crm_lost_reason> list) ;
    Page<Crm_lost_reason> searchDefault(Crm_lost_reasonSearchContext context) ;
    List<Crm_lost_reason> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Crm_lost_reason> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Crm_lost_reason> getCrmLostReasonByIds(List<Long> ids) ;
    List<Crm_lost_reason> getCrmLostReasonByEntities(List<Crm_lost_reason> entities) ;
}


