package cn.ibizlab.businesscentral.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Survey_mail_compose_message] 服务对象接口
 */
public interface ISurvey_mail_compose_messageService extends IService<Survey_mail_compose_message>{

    boolean create(Survey_mail_compose_message et) ;
    void createBatch(List<Survey_mail_compose_message> list) ;
    boolean update(Survey_mail_compose_message et) ;
    void updateBatch(List<Survey_mail_compose_message> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Survey_mail_compose_message get(Long key) ;
    Survey_mail_compose_message getDraft(Survey_mail_compose_message et) ;
    boolean checkKey(Survey_mail_compose_message et) ;
    boolean save(Survey_mail_compose_message et) ;
    void saveBatch(List<Survey_mail_compose_message> list) ;
    Page<Survey_mail_compose_message> searchDefault(Survey_mail_compose_messageSearchContext context) ;
    List<Survey_mail_compose_message> selectByMailActivityTypeId(Long id);
    void resetByMailActivityTypeId(Long id);
    void resetByMailActivityTypeId(Collection<Long> ids);
    void removeByMailActivityTypeId(Long id);
    List<Survey_mail_compose_message> selectByMassMailingCampaignId(Long id);
    void resetByMassMailingCampaignId(Long id);
    void resetByMassMailingCampaignId(Collection<Long> ids);
    void removeByMassMailingCampaignId(Long id);
    List<Survey_mail_compose_message> selectByMassMailingId(Long id);
    void removeByMassMailingId(Collection<Long> ids);
    void removeByMassMailingId(Long id);
    List<Survey_mail_compose_message> selectBySubtypeId(Long id);
    void resetBySubtypeId(Long id);
    void resetBySubtypeId(Collection<Long> ids);
    void removeBySubtypeId(Long id);
    List<Survey_mail_compose_message> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Survey_mail_compose_message> selectByTemplateId(Long id);
    void resetByTemplateId(Long id);
    void resetByTemplateId(Collection<Long> ids);
    void removeByTemplateId(Long id);
    List<Survey_mail_compose_message> selectByAuthorId(Long id);
    void resetByAuthorId(Long id);
    void resetByAuthorId(Collection<Long> ids);
    void removeByAuthorId(Long id);
    List<Survey_mail_compose_message> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Survey_mail_compose_message> selectByModeratorId(Long id);
    void resetByModeratorId(Long id);
    void resetByModeratorId(Collection<Long> ids);
    void removeByModeratorId(Long id);
    List<Survey_mail_compose_message> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Survey_mail_compose_message> selectBySurveyId(Long id);
    void resetBySurveyId(Long id);
    void resetBySurveyId(Collection<Long> ids);
    void removeBySurveyId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Survey_mail_compose_message> getSurveyMailComposeMessageByIds(List<Long> ids) ;
    List<Survey_mail_compose_message> getSurveyMailComposeMessageByEntities(List<Survey_mail_compose_message> entities) ;
}


