package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_backorder_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_backorder_confirmation] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-backorder-confirmation", fallback = stock_backorder_confirmationFallback.class)
public interface stock_backorder_confirmationFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations")
    Stock_backorder_confirmation create(@RequestBody Stock_backorder_confirmation stock_backorder_confirmation);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/batch")
    Boolean createBatch(@RequestBody List<Stock_backorder_confirmation> stock_backorder_confirmations);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_backorder_confirmations/{id}")
    Stock_backorder_confirmation get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_backorder_confirmations/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_backorder_confirmations/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/search")
    Page<Stock_backorder_confirmation> search(@RequestBody Stock_backorder_confirmationSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_backorder_confirmations/{id}")
    Stock_backorder_confirmation update(@PathVariable("id") Long id,@RequestBody Stock_backorder_confirmation stock_backorder_confirmation);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_backorder_confirmations/batch")
    Boolean updateBatch(@RequestBody List<Stock_backorder_confirmation> stock_backorder_confirmations);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_backorder_confirmations/select")
    Page<Stock_backorder_confirmation> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_backorder_confirmations/getdraft")
    Stock_backorder_confirmation getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/checkkey")
    Boolean checkKey(@RequestBody Stock_backorder_confirmation stock_backorder_confirmation);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/save")
    Boolean save(@RequestBody Stock_backorder_confirmation stock_backorder_confirmation);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_backorder_confirmation> stock_backorder_confirmations);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/searchdefault")
    Page<Stock_backorder_confirmation> searchDefault(@RequestBody Stock_backorder_confirmationSearchContext context);


}
