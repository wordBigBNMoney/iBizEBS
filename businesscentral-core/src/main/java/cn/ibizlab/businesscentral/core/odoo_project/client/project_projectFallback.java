package cn.ibizlab.businesscentral.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_project;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_projectSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[project_project] 服务对象接口
 */
@Component
public class project_projectFallback implements project_projectFeignClient{


    public Page<Project_project> search(Project_projectSearchContext context){
            return null;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Project_project create(Project_project project_project){
            return null;
     }
    public Boolean createBatch(List<Project_project> project_projects){
            return false;
     }

    public Project_project get(Long id){
            return null;
     }


    public Project_project update(Long id, Project_project project_project){
            return null;
     }
    public Boolean updateBatch(List<Project_project> project_projects){
            return false;
     }


    public Page<Project_project> select(){
            return null;
     }

    public Project_project getDraft(){
            return null;
    }



    public Boolean checkKey(Project_project project_project){
            return false;
     }


    public Boolean save(Project_project project_project){
            return false;
     }
    public Boolean saveBatch(List<Project_project> project_projects){
            return false;
     }

    public Page<Project_project> searchDefault(Project_projectSearchContext context){
            return null;
     }


}
