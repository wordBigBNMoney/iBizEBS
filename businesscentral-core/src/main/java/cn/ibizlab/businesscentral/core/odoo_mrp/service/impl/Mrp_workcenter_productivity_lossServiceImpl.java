package cn.ibizlab.businesscentral.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivity_lossService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mrp.mapper.Mrp_workcenter_productivity_lossMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[工作中心生产力损失] 服务对象接口实现
 */
@Slf4j
@Service("Mrp_workcenter_productivity_lossServiceImpl")
public class Mrp_workcenter_productivity_lossServiceImpl extends EBSServiceImpl<Mrp_workcenter_productivity_lossMapper, Mrp_workcenter_productivity_loss> implements IMrp_workcenter_productivity_lossService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivityService mrpWorkcenterProductivityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivity_loss_typeService mrpWorkcenterProductivityLossTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mrp.workcenter.productivity.loss" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mrp_workcenter_productivity_loss et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_workcenter_productivity_lossService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mrp_workcenter_productivity_loss> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mrp_workcenter_productivity_loss et) {
        Mrp_workcenter_productivity_loss old = new Mrp_workcenter_productivity_loss() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_workcenter_productivity_lossService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_workcenter_productivity_lossService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mrp_workcenter_productivity_loss> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        if(!ObjectUtils.isEmpty(mrpWorkcenterProductivityService.selectByLossId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mrp_workcenter_productivity]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        if(!ObjectUtils.isEmpty(mrpWorkcenterProductivityService.selectByLossId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mrp_workcenter_productivity]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mrp_workcenter_productivity_loss get(Long key) {
        Mrp_workcenter_productivity_loss et = getById(key);
        if(et==null){
            et=new Mrp_workcenter_productivity_loss();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mrp_workcenter_productivity_loss getDraft(Mrp_workcenter_productivity_loss et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mrp_workcenter_productivity_loss et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mrp_workcenter_productivity_loss et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mrp_workcenter_productivity_loss et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mrp_workcenter_productivity_loss> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mrp_workcenter_productivity_loss> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mrp_workcenter_productivity_loss> selectByLossId(Long id) {
        return baseMapper.selectByLossId(id);
    }
    @Override
    public void resetByLossId(Long id) {
        this.update(new UpdateWrapper<Mrp_workcenter_productivity_loss>().set("loss_id",null).eq("loss_id",id));
    }

    @Override
    public void resetByLossId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_workcenter_productivity_loss>().set("loss_id",null).in("loss_id",ids));
    }

    @Override
    public void removeByLossId(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity_loss>().eq("loss_id",id));
    }

	@Override
    public List<Mrp_workcenter_productivity_loss> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity_loss>().eq("create_uid",id));
    }

	@Override
    public List<Mrp_workcenter_productivity_loss> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity_loss>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mrp_workcenter_productivity_loss> searchDefault(Mrp_workcenter_productivity_lossSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mrp_workcenter_productivity_loss> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mrp_workcenter_productivity_loss>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mrp_workcenter_productivity_loss et){
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY_LOSS__MRP_WORKCENTER_PRODUCTIVITY_LOSS_TYPE__LOSS_ID]
        if(!ObjectUtils.isEmpty(et.getLossId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type odooLoss=et.getOdooLoss();
            if(ObjectUtils.isEmpty(odooLoss)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type majorEntity=mrpWorkcenterProductivityLossTypeService.get(et.getLossId());
                et.setOdooLoss(majorEntity);
                odooLoss=majorEntity;
            }
            et.setLossType(odooLoss.getLossType());
        }
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY_LOSS__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY_LOSS__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mrp_workcenter_productivity_loss> getMrpWorkcenterProductivityLossByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mrp_workcenter_productivity_loss> getMrpWorkcenterProductivityLossByEntities(List<Mrp_workcenter_productivity_loss> entities) {
        List ids =new ArrayList();
        for(Mrp_workcenter_productivity_loss entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



