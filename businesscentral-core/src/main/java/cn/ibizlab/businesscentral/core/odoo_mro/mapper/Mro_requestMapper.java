package cn.ibizlab.businesscentral.core.odoo_mro.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_request;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_requestSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mro_requestMapper extends BaseMapper<Mro_request>{

    Page<Mro_request> searchDefault(IPage page, @Param("srf") Mro_requestSearchContext context, @Param("ew") Wrapper<Mro_request> wrapper) ;
    @Override
    Mro_request selectById(Serializable id);
    @Override
    int insert(Mro_request entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mro_request entity);
    @Override
    int update(@Param(Constants.ENTITY) Mro_request entity, @Param("ew") Wrapper<Mro_request> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mro_request> selectByAssetId(@Param("id") Serializable id) ;

    List<Mro_request> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mro_request> selectByWriteUid(@Param("id") Serializable id) ;


}
