package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_ruleSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_rule] 服务对象接口
 */
public interface IStock_ruleService extends IService<Stock_rule>{

    boolean create(Stock_rule et) ;
    void createBatch(List<Stock_rule> list) ;
    boolean update(Stock_rule et) ;
    void updateBatch(List<Stock_rule> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_rule get(Long key) ;
    Stock_rule getDraft(Stock_rule et) ;
    boolean checkKey(Stock_rule et) ;
    boolean save(Stock_rule et) ;
    void saveBatch(List<Stock_rule> list) ;
    Page<Stock_rule> searchDefault(Stock_ruleSearchContext context) ;
    List<Stock_rule> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_rule> selectByPartnerAddressId(Long id);
    void resetByPartnerAddressId(Long id);
    void resetByPartnerAddressId(Collection<Long> ids);
    void removeByPartnerAddressId(Long id);
    List<Stock_rule> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_rule> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_rule> selectByRouteId(Long id);
    void removeByRouteId(Collection<Long> ids);
    void removeByRouteId(Long id);
    List<Stock_rule> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_rule> selectByLocationSrcId(Long id);
    void resetByLocationSrcId(Long id);
    void resetByLocationSrcId(Collection<Long> ids);
    void removeByLocationSrcId(Long id);
    List<Stock_rule> selectByPickingTypeId(Long id);
    void resetByPickingTypeId(Long id);
    void resetByPickingTypeId(Collection<Long> ids);
    void removeByPickingTypeId(Long id);
    List<Stock_rule> selectByPropagateWarehouseId(Long id);
    void resetByPropagateWarehouseId(Long id);
    void resetByPropagateWarehouseId(Collection<Long> ids);
    void removeByPropagateWarehouseId(Long id);
    List<Stock_rule> selectByWarehouseId(Long id);
    void resetByWarehouseId(Long id);
    void resetByWarehouseId(Collection<Long> ids);
    void removeByWarehouseId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_rule> getStockRuleByIds(List<Long> ids) ;
    List<Stock_rule> getStockRuleByEntities(List<Stock_rule> entities) ;
}


