package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_currencySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_currency] 服务对象接口
 */
@Component
public class res_currencyFallback implements res_currencyFeignClient{

    public Res_currency get(Long id){
            return null;
     }



    public Page<Res_currency> search(Res_currencySearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Res_currency update(Long id, Res_currency res_currency){
            return null;
     }
    public Boolean updateBatch(List<Res_currency> res_currencies){
            return false;
     }


    public Res_currency create(Res_currency res_currency){
            return null;
     }
    public Boolean createBatch(List<Res_currency> res_currencies){
            return false;
     }



    public Page<Res_currency> select(){
            return null;
     }

    public Res_currency getDraft(){
            return null;
    }



    public Boolean checkKey(Res_currency res_currency){
            return false;
     }


    public Boolean save(Res_currency res_currency){
            return false;
     }
    public Boolean saveBatch(List<Res_currency> res_currencies){
            return false;
     }

    public Page<Res_currency> searchDEFAULT(Res_currencySearchContext context){
            return null;
     }


}
