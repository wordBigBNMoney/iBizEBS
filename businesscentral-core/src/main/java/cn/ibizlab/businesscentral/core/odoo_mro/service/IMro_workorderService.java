package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_workorder;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_workorderSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_workorder] 服务对象接口
 */
public interface IMro_workorderService extends IService<Mro_workorder>{

    boolean create(Mro_workorder et) ;
    void createBatch(List<Mro_workorder> list) ;
    boolean update(Mro_workorder et) ;
    void updateBatch(List<Mro_workorder> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_workorder get(Long key) ;
    Mro_workorder getDraft(Mro_workorder et) ;
    boolean checkKey(Mro_workorder et) ;
    boolean save(Mro_workorder et) ;
    void saveBatch(List<Mro_workorder> list) ;
    Page<Mro_workorder> searchDefault(Mro_workorderSearchContext context) ;
    List<Mro_workorder> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Mro_workorder> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_workorder> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Mro_workorder> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_workorder> getMroWorkorderByIds(List<Long> ids) ;
    List<Mro_workorder> getMroWorkorderByEntities(List<Mro_workorder> entities) ;
}


