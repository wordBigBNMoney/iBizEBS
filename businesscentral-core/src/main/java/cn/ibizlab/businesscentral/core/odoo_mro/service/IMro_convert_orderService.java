package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_convert_orderSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_convert_order] 服务对象接口
 */
public interface IMro_convert_orderService extends IService<Mro_convert_order>{

    boolean create(Mro_convert_order et) ;
    void createBatch(List<Mro_convert_order> list) ;
    boolean update(Mro_convert_order et) ;
    void updateBatch(List<Mro_convert_order> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_convert_order get(Long key) ;
    Mro_convert_order getDraft(Mro_convert_order et) ;
    boolean checkKey(Mro_convert_order et) ;
    boolean save(Mro_convert_order et) ;
    void saveBatch(List<Mro_convert_order> list) ;
    Page<Mro_convert_order> searchDefault(Mro_convert_orderSearchContext context) ;
    List<Mro_convert_order> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_convert_order> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_convert_order> getMroConvertOrderByIds(List<Long> ids) ;
    List<Mro_convert_order> getMroConvertOrderByEntities(List<Mro_convert_order> entities) ;
}


