package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel_partner;
/**
 * 关系型数据实体[Mail_channel_partner] 查询条件对象
 */
@Slf4j
@Data
public class Mail_channel_partnerSearchContext extends QueryWrapperContext<Mail_channel_partner> {

	private String n_fold_state_eq;//[对话收拢状态]
	public void setN_fold_state_eq(String n_fold_state_eq) {
        this.n_fold_state_eq = n_fold_state_eq;
        if(!ObjectUtils.isEmpty(this.n_fold_state_eq)){
            this.getSearchCond().eq("fold_state", n_fold_state_eq);
        }
    }
	private String n_partner_id_text_eq;//[收件人]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[收件人]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_channel_id_text_eq;//[渠道]
	public void setN_channel_id_text_eq(String n_channel_id_text_eq) {
        this.n_channel_id_text_eq = n_channel_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_id_text_eq)){
            this.getSearchCond().eq("channel_id_text", n_channel_id_text_eq);
        }
    }
	private String n_channel_id_text_like;//[渠道]
	public void setN_channel_id_text_like(String n_channel_id_text_like) {
        this.n_channel_id_text_like = n_channel_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_channel_id_text_like)){
            this.getSearchCond().like("channel_id_text", n_channel_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_channel_id_eq;//[渠道]
	public void setN_channel_id_eq(Long n_channel_id_eq) {
        this.n_channel_id_eq = n_channel_id_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_id_eq)){
            this.getSearchCond().eq("channel_id", n_channel_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_partner_id_eq;//[收件人]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_seen_message_id_eq;//[最近一次查阅]
	public void setN_seen_message_id_eq(Long n_seen_message_id_eq) {
        this.n_seen_message_id_eq = n_seen_message_id_eq;
        if(!ObjectUtils.isEmpty(this.n_seen_message_id_eq)){
            this.getSearchCond().eq("seen_message_id", n_seen_message_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



