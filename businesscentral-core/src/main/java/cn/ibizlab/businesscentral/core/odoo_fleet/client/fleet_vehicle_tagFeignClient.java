package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_tag;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_tagSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_tag] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-vehicle-tag", fallback = fleet_vehicle_tagFallback.class)
public interface fleet_vehicle_tagFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/search")
    Page<Fleet_vehicle_tag> search(@RequestBody Fleet_vehicle_tagSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_tags/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_tags/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags")
    Fleet_vehicle_tag create(@RequestBody Fleet_vehicle_tag fleet_vehicle_tag);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_tag> fleet_vehicle_tags);



    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_tags/{id}")
    Fleet_vehicle_tag update(@PathVariable("id") Long id,@RequestBody Fleet_vehicle_tag fleet_vehicle_tag);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_tags/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_tag> fleet_vehicle_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_tags/{id}")
    Fleet_vehicle_tag get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_tags/select")
    Page<Fleet_vehicle_tag> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_tags/getdraft")
    Fleet_vehicle_tag getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/checkkey")
    Boolean checkKey(@RequestBody Fleet_vehicle_tag fleet_vehicle_tag);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/save")
    Boolean save(@RequestBody Fleet_vehicle_tag fleet_vehicle_tag);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_vehicle_tag> fleet_vehicle_tags);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/searchdefault")
    Page<Fleet_vehicle_tag> searchDefault(@RequestBody Fleet_vehicle_tagSearchContext context);


}
