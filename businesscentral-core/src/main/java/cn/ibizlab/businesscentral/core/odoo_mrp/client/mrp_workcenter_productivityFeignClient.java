package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_workcenter_productivity] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-workcenter-productivity", fallback = mrp_workcenter_productivityFallback.class)
public interface mrp_workcenter_productivityFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivities/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivities/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivities/{id}")
    Mrp_workcenter_productivity update(@PathVariable("id") Long id,@RequestBody Mrp_workcenter_productivity mrp_workcenter_productivity);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivities/batch")
    Boolean updateBatch(@RequestBody List<Mrp_workcenter_productivity> mrp_workcenter_productivities);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/search")
    Page<Mrp_workcenter_productivity> search(@RequestBody Mrp_workcenter_productivitySearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities")
    Mrp_workcenter_productivity create(@RequestBody Mrp_workcenter_productivity mrp_workcenter_productivity);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/batch")
    Boolean createBatch(@RequestBody List<Mrp_workcenter_productivity> mrp_workcenter_productivities);




    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivities/{id}")
    Mrp_workcenter_productivity get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivities/select")
    Page<Mrp_workcenter_productivity> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivities/getdraft")
    Mrp_workcenter_productivity getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/checkkey")
    Boolean checkKey(@RequestBody Mrp_workcenter_productivity mrp_workcenter_productivity);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/save")
    Boolean save(@RequestBody Mrp_workcenter_productivity mrp_workcenter_productivity);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_workcenter_productivity> mrp_workcenter_productivities);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/searchdefault")
    Page<Mrp_workcenter_productivity> searchDefault(@RequestBody Mrp_workcenter_productivitySearchContext context);


}
