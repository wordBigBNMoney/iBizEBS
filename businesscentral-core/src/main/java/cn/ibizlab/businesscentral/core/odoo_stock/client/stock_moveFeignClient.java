package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_moveSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_move] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-move", fallback = stock_moveFallback.class)
public interface stock_moveFeignClient {



    @RequestMapping(method = RequestMethod.GET, value = "/stock_moves/{id}")
    Stock_move get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_moves")
    Stock_move create(@RequestBody Stock_move stock_move);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_moves/batch")
    Boolean createBatch(@RequestBody List<Stock_move> stock_moves);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_moves/{id}")
    Stock_move update(@PathVariable("id") Long id,@RequestBody Stock_move stock_move);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_moves/batch")
    Boolean updateBatch(@RequestBody List<Stock_move> stock_moves);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_moves/search")
    Page<Stock_move> search(@RequestBody Stock_moveSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_moves/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_moves/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_moves/select")
    Page<Stock_move> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_moves/getdraft")
    Stock_move getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_moves/checkkey")
    Boolean checkKey(@RequestBody Stock_move stock_move);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_moves/save")
    Boolean save(@RequestBody Stock_move stock_move);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_moves/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_move> stock_moves);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_moves/searchdefault")
    Page<Stock_move> searchDefault(@RequestBody Stock_moveSearchContext context);


}
