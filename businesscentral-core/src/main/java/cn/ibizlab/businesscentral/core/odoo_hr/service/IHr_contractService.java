package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_contractSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_contract] 服务对象接口
 */
public interface IHr_contractService extends IService<Hr_contract>{

    boolean create(Hr_contract et) ;
    void createBatch(List<Hr_contract> list) ;
    boolean update(Hr_contract et) ;
    void updateBatch(List<Hr_contract> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_contract get(Long key) ;
    Hr_contract getDraft(Hr_contract et) ;
    boolean checkKey(Hr_contract et) ;
    boolean save(Hr_contract et) ;
    void saveBatch(List<Hr_contract> list) ;
    Page<Hr_contract> searchDefault(Hr_contractSearchContext context) ;
    List<Hr_contract> selectByHrResponsibleId(Long id);
    void removeByHrResponsibleId(Long id);
    List<Hr_contract> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Hr_contract> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Hr_contract> selectByJobId(Long id);
    void resetByJobId(Long id);
    void resetByJobId(Collection<Long> ids);
    void removeByJobId(Long id);
    List<Hr_contract> selectByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Collection<Long> ids);
    void removeByResourceCalendarId(Long id);
    List<Hr_contract> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Hr_contract> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_contract> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_contract> getHrContractByIds(List<Long> ids) ;
    List<Hr_contract> getHrContractByEntities(List<Hr_contract> entities) ;
}


