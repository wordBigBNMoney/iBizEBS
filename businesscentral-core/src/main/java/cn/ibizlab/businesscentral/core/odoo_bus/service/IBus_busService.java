package cn.ibizlab.businesscentral.core.odoo_bus.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.businesscentral.core.odoo_bus.filter.Bus_busSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Bus_bus] 服务对象接口
 */
public interface IBus_busService extends IService<Bus_bus>{

    boolean create(Bus_bus et) ;
    void createBatch(List<Bus_bus> list) ;
    boolean update(Bus_bus et) ;
    void updateBatch(List<Bus_bus> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Bus_bus get(Long key) ;
    Bus_bus getDraft(Bus_bus et) ;
    boolean checkKey(Bus_bus et) ;
    boolean save(Bus_bus et) ;
    void saveBatch(List<Bus_bus> list) ;
    Page<Bus_bus> searchDefault(Bus_busSearchContext context) ;
    List<Bus_bus> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Bus_bus> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Bus_bus> getBusBusByIds(List<Long> ids) ;
    List<Bus_bus> getBusBusByEntities(List<Bus_bus> entities) ;
}


