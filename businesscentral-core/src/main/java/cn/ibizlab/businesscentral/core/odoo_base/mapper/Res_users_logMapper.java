package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_users_logSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Res_users_logMapper extends BaseMapper<Res_users_log>{

    Page<Res_users_log> searchDefault(IPage page, @Param("srf") Res_users_logSearchContext context, @Param("ew") Wrapper<Res_users_log> wrapper) ;
    @Override
    Res_users_log selectById(Serializable id);
    @Override
    int insert(Res_users_log entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Res_users_log entity);
    @Override
    int update(@Param(Constants.ENTITY) Res_users_log entity, @Param("ew") Wrapper<Res_users_log> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Res_users_log> selectByCreateUid(@Param("id") Serializable id) ;

    List<Res_users_log> selectByWriteUid(@Param("id") Serializable id) ;


}
