package cn.ibizlab.businesscentral.core.odoo_purchase.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Purchase_bill_unionMapper extends BaseMapper<Purchase_bill_union>{

    Page<Purchase_bill_union> searchDefault(IPage page, @Param("srf") Purchase_bill_unionSearchContext context, @Param("ew") Wrapper<Purchase_bill_union> wrapper) ;
    @Override
    Purchase_bill_union selectById(Serializable id);
    @Override
    int insert(Purchase_bill_union entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Purchase_bill_union entity);
    @Override
    int update(@Param(Constants.ENTITY) Purchase_bill_union entity, @Param("ew") Wrapper<Purchase_bill_union> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Purchase_bill_union> selectByVendorBillId(@Param("id") Serializable id) ;

    List<Purchase_bill_union> selectByPurchaseOrderId(@Param("id") Serializable id) ;

    List<Purchase_bill_union> selectByCompanyId(@Param("id") Serializable id) ;

    List<Purchase_bill_union> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Purchase_bill_union> selectByPartnerId(@Param("id") Serializable id) ;


}
