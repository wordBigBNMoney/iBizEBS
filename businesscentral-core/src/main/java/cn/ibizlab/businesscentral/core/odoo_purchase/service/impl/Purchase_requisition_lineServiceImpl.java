package cn.ibizlab.businesscentral.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisition_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_purchase.mapper.Purchase_requisition_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[采购申请行] 服务对象接口实现
 */
@Slf4j
@Service("Purchase_requisition_lineServiceImpl")
public class Purchase_requisition_lineServiceImpl extends EBSServiceImpl<Purchase_requisition_lineMapper, Purchase_requisition_line> implements IPurchase_requisition_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_supplierinfoService productSupplierinfoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService purchaseRequisitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_requisition_lineproduct_changeLogic product_changeLogic;

    protected int batchSize = 500;

    public String getIrModel(){
        return "purchase.requisition.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Purchase_requisition_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_requisition_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Purchase_requisition_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Purchase_requisition_line et) {
        Purchase_requisition_line old = new Purchase_requisition_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_requisition_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_requisition_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Purchase_requisition_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        productSupplierinfoService.removeByPurchaseRequisitionLineId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        productSupplierinfoService.removeByPurchaseRequisitionLineId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Purchase_requisition_line get(Long key) {
        Purchase_requisition_line et = getById(key);
        if(et==null){
            et=new Purchase_requisition_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Purchase_requisition_line getDraft(Purchase_requisition_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Purchase_requisition_line calc_price(Purchase_requisition_line et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Purchase_requisition_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public Purchase_requisition_line product_change(Purchase_requisition_line et) {
        product_changeLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public boolean save(Purchase_requisition_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Purchase_requisition_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Purchase_requisition_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Purchase_requisition_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Purchase_requisition_line> selectByAccountAnalyticId(Long id) {
        return baseMapper.selectByAccountAnalyticId(id);
    }
    @Override
    public void removeByAccountAnalyticId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().eq("account_analytic_id",id));
    }

	@Override
    public List<Purchase_requisition_line> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().eq("product_id",id));
    }

	@Override
    public List<Purchase_requisition_line> selectByRequisitionId(Long id) {
        return baseMapper.selectByRequisitionId(id);
    }
    @Override
    public void removeByRequisitionId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().in("requisition_id",ids));
    }

    @Override
    public void removeByRequisitionId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().eq("requisition_id",id));
    }

	@Override
    public List<Purchase_requisition_line> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().eq("company_id",id));
    }

	@Override
    public List<Purchase_requisition_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().eq("create_uid",id));
    }

	@Override
    public List<Purchase_requisition_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().eq("write_uid",id));
    }

	@Override
    public List<Purchase_requisition_line> selectByMoveDestId(Long id) {
        return baseMapper.selectByMoveDestId(id);
    }
    @Override
    public void removeByMoveDestId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().eq("move_dest_id",id));
    }

	@Override
    public List<Purchase_requisition_line> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Purchase_requisition_line>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Purchase_requisition_line> searchDefault(Purchase_requisition_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_requisition_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_requisition_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Purchase_requisition_line et){
        //实体关系[DER1N_PURCHASE_REQUISITION_LINE_ACCOUNT_ANALYTIC_ACCOUNT_ACCOUNT_ANALYTIC_ID]
        if(!ObjectUtils.isEmpty(et.getAccountAnalyticId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account accountanalytic=et.getAccountanalytic();
            if(ObjectUtils.isEmpty(accountanalytic)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAccountAnalyticId());
                et.setAccountanalytic(majorEntity);
                accountanalytic=majorEntity;
            }
            et.setAccountAnalyticName(accountanalytic.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_LINE_PRODUCT_PRODUCT_PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product product=et.getProduct();
            if(ObjectUtils.isEmpty(product)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setProduct(majorEntity);
                product=majorEntity;
            }
            et.setProductName(product.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_LINE_PURCHASE_REQUISITION_REQUISITION_ID]
        if(!ObjectUtils.isEmpty(et.getRequisitionId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition requisition=et.getRequisition();
            if(ObjectUtils.isEmpty(requisition)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition majorEntity=purchaseRequisitionService.get(et.getRequisitionId());
                et.setRequisition(majorEntity);
                requisition=majorEntity;
            }
            et.setRequisitionName(requisition.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_LINE_RES_COMPANY_COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company company=et.getCompany();
            if(ObjectUtils.isEmpty(company)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setCompany(majorEntity);
                company=majorEntity;
            }
            et.setCompanyName(company.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_LINE_RES_USERS_CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUname(odooCreate.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_LINE_RES_USERS_WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUname(odooWrite.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_LINE_STOCK_MOVE_MOVE_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getMoveDestId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move movedest=et.getMovedest();
            if(ObjectUtils.isEmpty(movedest)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move majorEntity=stockMoveService.get(et.getMoveDestId());
                et.setMovedest(majorEntity);
                movedest=majorEntity;
            }
            et.setMoveDestName(movedest.getName());
        }
        //实体关系[DER1N_PURCHASE_REQUISITION_LINE_UOM_UOM_PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom productuom=et.getProductuom();
            if(ObjectUtils.isEmpty(productuom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setProductuom(majorEntity);
                productuom=majorEntity;
            }
            et.setProductUomName(productuom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Purchase_requisition_line> getPurchaseRequisitionLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Purchase_requisition_line> getPurchaseRequisitionLineByEntities(List<Purchase_requisition_line> entities) {
        List ids =new ArrayList();
        for(Purchase_requisition_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



