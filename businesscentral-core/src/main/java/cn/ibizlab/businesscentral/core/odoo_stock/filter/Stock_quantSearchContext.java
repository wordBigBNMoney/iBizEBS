package cn.ibizlab.businesscentral.core.odoo_stock.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant;
/**
 * 关系型数据实体[Stock_quant] 查询条件对象
 */
@Slf4j
@Data
public class Stock_quantSearchContext extends QueryWrapperContext<Stock_quant> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_location_id_text_eq;//[位置]
	public void setN_location_id_text_eq(String n_location_id_text_eq) {
        this.n_location_id_text_eq = n_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_eq)){
            this.getSearchCond().eq("location_id_text", n_location_id_text_eq);
        }
    }
	private String n_location_id_text_like;//[位置]
	public void setN_location_id_text_like(String n_location_id_text_like) {
        this.n_location_id_text_like = n_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_like)){
            this.getSearchCond().like("location_id_text", n_location_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_owner_id_text_eq;//[所有者]
	public void setN_owner_id_text_eq(String n_owner_id_text_eq) {
        this.n_owner_id_text_eq = n_owner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_owner_id_text_eq)){
            this.getSearchCond().eq("owner_id_text", n_owner_id_text_eq);
        }
    }
	private String n_owner_id_text_like;//[所有者]
	public void setN_owner_id_text_like(String n_owner_id_text_like) {
        this.n_owner_id_text_like = n_owner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_owner_id_text_like)){
            this.getSearchCond().like("owner_id_text", n_owner_id_text_like);
        }
    }
	private String n_lot_id_text_eq;//[批次/序列号码]
	public void setN_lot_id_text_eq(String n_lot_id_text_eq) {
        this.n_lot_id_text_eq = n_lot_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_eq)){
            this.getSearchCond().eq("lot_id_text", n_lot_id_text_eq);
        }
    }
	private String n_lot_id_text_like;//[批次/序列号码]
	public void setN_lot_id_text_like(String n_lot_id_text_like) {
        this.n_lot_id_text_like = n_lot_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_like)){
            this.getSearchCond().like("lot_id_text", n_lot_id_text_like);
        }
    }
	private String n_package_id_text_eq;//[包裹]
	public void setN_package_id_text_eq(String n_package_id_text_eq) {
        this.n_package_id_text_eq = n_package_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_package_id_text_eq)){
            this.getSearchCond().eq("package_id_text", n_package_id_text_eq);
        }
    }
	private String n_package_id_text_like;//[包裹]
	public void setN_package_id_text_like(String n_package_id_text_like) {
        this.n_package_id_text_like = n_package_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_package_id_text_like)){
            this.getSearchCond().like("package_id_text", n_package_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private Long n_owner_id_eq;//[所有者]
	public void setN_owner_id_eq(Long n_owner_id_eq) {
        this.n_owner_id_eq = n_owner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_owner_id_eq)){
            this.getSearchCond().eq("owner_id", n_owner_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_location_id_eq;//[位置]
	public void setN_location_id_eq(Long n_location_id_eq) {
        this.n_location_id_eq = n_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_eq)){
            this.getSearchCond().eq("location_id", n_location_id_eq);
        }
    }
	private Long n_package_id_eq;//[包裹]
	public void setN_package_id_eq(Long n_package_id_eq) {
        this.n_package_id_eq = n_package_id_eq;
        if(!ObjectUtils.isEmpty(this.n_package_id_eq)){
            this.getSearchCond().eq("package_id", n_package_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_lot_id_eq;//[批次/序列号码]
	public void setN_lot_id_eq(Long n_lot_id_eq) {
        this.n_lot_id_eq = n_lot_id_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_eq)){
            this.getSearchCond().eq("lot_id", n_lot_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



