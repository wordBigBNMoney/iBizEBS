package cn.ibizlab.businesscentral.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template_option;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_template_optionSearchContext;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_template_optionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_sale.mapper.Sale_order_template_optionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[报价模板选项] 服务对象接口实现
 */
@Slf4j
@Service("Sale_order_template_optionServiceImpl")
public class Sale_order_template_optionServiceImpl extends EBSServiceImpl<Sale_order_template_optionMapper, Sale_order_template_option> implements ISale_order_template_optionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_templateService saleOrderTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "sale.order.template.option" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Sale_order_template_option et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_order_template_optionService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Sale_order_template_option> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Sale_order_template_option et) {
        Sale_order_template_option old = new Sale_order_template_option() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_order_template_optionService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_order_template_optionService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Sale_order_template_option> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Sale_order_template_option get(Long key) {
        Sale_order_template_option et = getById(key);
        if(et==null){
            et=new Sale_order_template_option();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Sale_order_template_option getDraft(Sale_order_template_option et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Sale_order_template_option et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Sale_order_template_option et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Sale_order_template_option et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Sale_order_template_option> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Sale_order_template_option> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Sale_order_template_option> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Sale_order_template_option>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_template_option>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Sale_order_template_option>().eq("product_id",id));
    }

	@Override
    public List<Sale_order_template_option> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Sale_order_template_option>().eq("create_uid",id));
    }

	@Override
    public List<Sale_order_template_option> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Sale_order_template_option>().eq("write_uid",id));
    }

	@Override
    public List<Sale_order_template_option> selectBySaleOrderTemplateId(Long id) {
        return baseMapper.selectBySaleOrderTemplateId(id);
    }
    @Override
    public void removeBySaleOrderTemplateId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Sale_order_template_option>().in("sale_order_template_id",ids));
    }

    @Override
    public void removeBySaleOrderTemplateId(Long id) {
        this.remove(new QueryWrapper<Sale_order_template_option>().eq("sale_order_template_id",id));
    }

	@Override
    public List<Sale_order_template_option> selectByUomId(Long id) {
        return baseMapper.selectByUomId(id);
    }
    @Override
    public void resetByUomId(Long id) {
        this.update(new UpdateWrapper<Sale_order_template_option>().set("uom_id",null).eq("uom_id",id));
    }

    @Override
    public void resetByUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order_template_option>().set("uom_id",null).in("uom_id",ids));
    }

    @Override
    public void removeByUomId(Long id) {
        this.remove(new QueryWrapper<Sale_order_template_option>().eq("uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Sale_order_template_option> searchDefault(Sale_order_template_optionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Sale_order_template_option> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Sale_order_template_option>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Sale_order_template_option et){
        //实体关系[DER1N_SALE_ORDER_TEMPLATE_OPTION__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_SALE_ORDER_TEMPLATE_OPTION__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_SALE_ORDER_TEMPLATE_OPTION__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_SALE_ORDER_TEMPLATE_OPTION__SALE_ORDER_TEMPLATE__SALE_ORDER_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getSaleOrderTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template odooSaleOrderTemplate=et.getOdooSaleOrderTemplate();
            if(ObjectUtils.isEmpty(odooSaleOrderTemplate)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template majorEntity=saleOrderTemplateService.get(et.getSaleOrderTemplateId());
                et.setOdooSaleOrderTemplate(majorEntity);
                odooSaleOrderTemplate=majorEntity;
            }
            et.setSaleOrderTemplateIdText(odooSaleOrderTemplate.getName());
        }
        //实体关系[DER1N_SALE_ORDER_TEMPLATE_OPTION__UOM_UOM__UOM_ID]
        if(!ObjectUtils.isEmpty(et.getUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooUom=et.getOdooUom();
            if(ObjectUtils.isEmpty(odooUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getUomId());
                et.setOdooUom(majorEntity);
                odooUom=majorEntity;
            }
            et.setUomIdText(odooUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Sale_order_template_option> getSaleOrderTemplateOptionByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Sale_order_template_option> getSaleOrderTemplateOptionByEntities(List<Sale_order_template_option> entities) {
        List ids =new ArrayList();
        for(Sale_order_template_option entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



