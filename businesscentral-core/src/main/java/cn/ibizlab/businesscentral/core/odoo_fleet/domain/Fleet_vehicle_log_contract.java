package cn.ibizlab.businesscentral.core.odoo_fleet.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[车辆合同信息]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "FLEET_VEHICLE_LOG_CONTRACT",resultMap = "Fleet_vehicle_log_contractResultMap")
public class Fleet_vehicle_log_contract extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 合同参考
     */
    @DEField(name = "ins_ref")
    @TableField(value = "ins_ref")
    @JSONField(name = "ins_ref")
    @JsonProperty("ins_ref")
    private String insRef;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 指标性成本总计
     */
    @TableField(exist = false)
    @JSONField(name = "sum_cost")
    @JsonProperty("sum_cost")
    private Double sumCost;
    /**
     * 条款和条件
     */
    @TableField(value = "notes")
    @JSONField(name = "notes")
    @JsonProperty("notes")
    private String notes;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 合同到期日期
     */
    @DEField(name = "expiration_date")
    @TableField(value = "expiration_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expiration_date" , format="yyyy-MM-dd")
    @JsonProperty("expiration_date")
    private Timestamp expirationDate;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 警告日期
     */
    @TableField(exist = false)
    @JSONField(name = "days_left")
    @JsonProperty("days_left")
    private Integer daysLeft;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 创建时的里程表
     */
    @TableField(value = "odometer")
    @JSONField(name = "odometer")
    @JsonProperty("odometer")
    private Double odometer;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 包括服务
     */
    @TableField(exist = false)
    @JSONField(name = "cost_ids")
    @JsonProperty("cost_ids")
    private String costIds;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 经常成本数量
     */
    @DEField(name = "cost_generated")
    @TableField(value = "cost_generated")
    @JSONField(name = "cost_generated")
    @JsonProperty("cost_generated")
    private Double costGenerated;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 需要激活
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 已发生费用
     */
    @TableField(exist = false)
    @JSONField(name = "generated_cost_ids")
    @JsonProperty("generated_cost_ids")
    private String generatedCostIds;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 经常成本频率
     */
    @DEField(name = "cost_frequency")
    @TableField(value = "cost_frequency")
    @JSONField(name = "cost_frequency")
    @JsonProperty("cost_frequency")
    private String costFrequency;
    /**
     * 合同开始日期
     */
    @DEField(name = "start_date")
    @TableField(value = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 车辆
     */
    @TableField(exist = false)
    @JSONField(name = "vehicle_id")
    @JsonProperty("vehicle_id")
    private Long vehicleId;
    /**
     * 自动生成
     */
    @TableField(exist = false)
    @JSONField(name = "auto_generated")
    @JsonProperty("auto_generated")
    private Boolean autoGenerated;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 里程表
     */
    @TableField(exist = false)
    @JSONField(name = "odometer_id")
    @JsonProperty("odometer_id")
    private Long odometerId;
    /**
     * 费用所属类别
     */
    @TableField(exist = false)
    @JSONField(name = "cost_type")
    @JsonProperty("cost_type")
    private String costType;
    /**
     * 上级
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 驾驶员
     */
    @TableField(exist = false)
    @JSONField(name = "purchaser_id_text")
    @JsonProperty("purchaser_id_text")
    private String purchaserIdText;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "cost_subtype_id")
    @JsonProperty("cost_subtype_id")
    private Long costSubtypeId;
    /**
     * 总额
     */
    @TableField(exist = false)
    @JSONField(name = "cost_amount")
    @JsonProperty("cost_amount")
    private Double costAmount;
    /**
     * 总价
     */
    @TableField(exist = false)
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "insurer_id_text")
    @JsonProperty("insurer_id_text")
    private String insurerIdText;
    /**
     * 成本
     */
    @TableField(exist = false)
    @JSONField(name = "cost_id_text")
    @JsonProperty("cost_id_text")
    private String costIdText;
    /**
     * 成本说明
     */
    @TableField(exist = false)
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 负责
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "odometer_unit")
    @JsonProperty("odometer_unit")
    private String odometerUnit;
    /**
     * 合同
     */
    @TableField(exist = false)
    @JSONField(name = "contract_id")
    @JsonProperty("contract_id")
    private Long contractId;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 成本
     */
    @DEField(name = "cost_id")
    @TableField(value = "cost_id")
    @JSONField(name = "cost_id")
    @JsonProperty("cost_id")
    private Long costId;
    /**
     * 供应商
     */
    @DEField(name = "insurer_id")
    @TableField(value = "insurer_id")
    @JSONField(name = "insurer_id")
    @JsonProperty("insurer_id")
    private Long insurerId;
    /**
     * 负责
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 驾驶员
     */
    @DEField(name = "purchaser_id")
    @TableField(value = "purchaser_id")
    @JSONField(name = "purchaser_id")
    @JsonProperty("purchaser_id")
    private Long purchaserId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost odooCost;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooInsurer;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPurchaser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [合同参考]
     */
    public void setInsRef(String insRef){
        this.insRef = insRef ;
        this.modify("ins_ref",insRef);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [条款和条件]
     */
    public void setNotes(String notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [合同到期日期]
     */
    public void setExpirationDate(Timestamp expirationDate){
        this.expirationDate = expirationDate ;
        this.modify("expiration_date",expirationDate);
    }

    /**
     * 格式化日期 [合同到期日期]
     */
    public String formatExpirationDate(){
        if (this.expirationDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(expirationDate);
    }
    /**
     * 设置 [创建时的里程表]
     */
    public void setOdometer(Double odometer){
        this.odometer = odometer ;
        this.modify("odometer",odometer);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [经常成本数量]
     */
    public void setCostGenerated(Double costGenerated){
        this.costGenerated = costGenerated ;
        this.modify("cost_generated",costGenerated);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [经常成本频率]
     */
    public void setCostFrequency(String costFrequency){
        this.costFrequency = costFrequency ;
        this.modify("cost_frequency",costFrequency);
    }

    /**
     * 设置 [合同开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 格式化日期 [合同开始日期]
     */
    public String formatStartDate(){
        if (this.startDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(startDate);
    }
    /**
     * 设置 [成本]
     */
    public void setCostId(Long costId){
        this.costId = costId ;
        this.modify("cost_id",costId);
    }

    /**
     * 设置 [供应商]
     */
    public void setInsurerId(Long insurerId){
        this.insurerId = insurerId ;
        this.modify("insurer_id",insurerId);
    }

    /**
     * 设置 [负责]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [驾驶员]
     */
    public void setPurchaserId(Long purchaserId){
        this.purchaserId = purchaserId ;
        this.modify("purchaser_id",purchaserId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


