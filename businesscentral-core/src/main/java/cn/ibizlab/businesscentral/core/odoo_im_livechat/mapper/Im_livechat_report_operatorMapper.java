package cn.ibizlab.businesscentral.core.odoo_im_livechat.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Im_livechat_report_operatorMapper extends BaseMapper<Im_livechat_report_operator>{

    Page<Im_livechat_report_operator> searchDefault(IPage page, @Param("srf") Im_livechat_report_operatorSearchContext context, @Param("ew") Wrapper<Im_livechat_report_operator> wrapper) ;
    @Override
    Im_livechat_report_operator selectById(Serializable id);
    @Override
    int insert(Im_livechat_report_operator entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Im_livechat_report_operator entity);
    @Override
    int update(@Param(Constants.ENTITY) Im_livechat_report_operator entity, @Param("ew") Wrapper<Im_livechat_report_operator> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Im_livechat_report_operator> selectByLivechatChannelId(@Param("id") Serializable id) ;

    List<Im_livechat_report_operator> selectByChannelId(@Param("id") Serializable id) ;

    List<Im_livechat_report_operator> selectByPartnerId(@Param("id") Serializable id) ;


}
