package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_unbuildSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_unbuild] 服务对象接口
 */
public interface IMrp_unbuildService extends IService<Mrp_unbuild>{

    boolean create(Mrp_unbuild et) ;
    void createBatch(List<Mrp_unbuild> list) ;
    boolean update(Mrp_unbuild et) ;
    void updateBatch(List<Mrp_unbuild> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_unbuild get(Long key) ;
    Mrp_unbuild getDraft(Mrp_unbuild et) ;
    boolean checkKey(Mrp_unbuild et) ;
    boolean save(Mrp_unbuild et) ;
    void saveBatch(List<Mrp_unbuild> list) ;
    Page<Mrp_unbuild> searchDefault(Mrp_unbuildSearchContext context) ;
    List<Mrp_unbuild> selectByBomId(Long id);
    void resetByBomId(Long id);
    void resetByBomId(Collection<Long> ids);
    void removeByBomId(Long id);
    List<Mrp_unbuild> selectByMoId(Long id);
    void resetByMoId(Long id);
    void resetByMoId(Collection<Long> ids);
    void removeByMoId(Long id);
    List<Mrp_unbuild> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Mrp_unbuild> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_unbuild> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mrp_unbuild> selectByLocationDestId(Long id);
    void resetByLocationDestId(Long id);
    void resetByLocationDestId(Collection<Long> ids);
    void removeByLocationDestId(Long id);
    List<Mrp_unbuild> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Mrp_unbuild> selectByLotId(Long id);
    void resetByLotId(Long id);
    void resetByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Mrp_unbuild> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_unbuild> getMrpUnbuildByIds(List<Long> ids) ;
    List<Mrp_unbuild> getMrpUnbuildByEntities(List<Mrp_unbuild> entities) ;
}


