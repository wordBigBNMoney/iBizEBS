package cn.ibizlab.businesscentral.core.odoo_purchase.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_line;
/**
 * 关系型数据实体[Purchase_requisition_line] 查询条件对象
 */
@Slf4j
@Data
public class Purchase_requisition_lineSearchContext extends QueryWrapperContext<Purchase_requisition_line> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_company_name_eq;//[公司名称]
	public void setN_company_name_eq(String n_company_name_eq) {
        this.n_company_name_eq = n_company_name_eq;
        if(!ObjectUtils.isEmpty(this.n_company_name_eq)){
            this.getSearchCond().eq("company_name", n_company_name_eq);
        }
    }
	private String n_company_name_like;//[公司名称]
	public void setN_company_name_like(String n_company_name_like) {
        this.n_company_name_like = n_company_name_like;
        if(!ObjectUtils.isEmpty(this.n_company_name_like)){
            this.getSearchCond().like("company_name", n_company_name_like);
        }
    }
	private String n_requisition_name_eq;//[采购申请]
	public void setN_requisition_name_eq(String n_requisition_name_eq) {
        this.n_requisition_name_eq = n_requisition_name_eq;
        if(!ObjectUtils.isEmpty(this.n_requisition_name_eq)){
            this.getSearchCond().eq("requisition_name", n_requisition_name_eq);
        }
    }
	private String n_requisition_name_like;//[采购申请]
	public void setN_requisition_name_like(String n_requisition_name_like) {
        this.n_requisition_name_like = n_requisition_name_like;
        if(!ObjectUtils.isEmpty(this.n_requisition_name_like)){
            this.getSearchCond().like("requisition_name", n_requisition_name_like);
        }
    }
	private String n_product_uom_name_eq;//[单位]
	public void setN_product_uom_name_eq(String n_product_uom_name_eq) {
        this.n_product_uom_name_eq = n_product_uom_name_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_name_eq)){
            this.getSearchCond().eq("product_uom_name", n_product_uom_name_eq);
        }
    }
	private String n_product_uom_name_like;//[单位]
	public void setN_product_uom_name_like(String n_product_uom_name_like) {
        this.n_product_uom_name_like = n_product_uom_name_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_name_like)){
            this.getSearchCond().like("product_uom_name", n_product_uom_name_like);
        }
    }
	private String n_move_dest_name_eq;//[说明]
	public void setN_move_dest_name_eq(String n_move_dest_name_eq) {
        this.n_move_dest_name_eq = n_move_dest_name_eq;
        if(!ObjectUtils.isEmpty(this.n_move_dest_name_eq)){
            this.getSearchCond().eq("move_dest_name", n_move_dest_name_eq);
        }
    }
	private String n_move_dest_name_like;//[说明]
	public void setN_move_dest_name_like(String n_move_dest_name_like) {
        this.n_move_dest_name_like = n_move_dest_name_like;
        if(!ObjectUtils.isEmpty(this.n_move_dest_name_like)){
            this.getSearchCond().like("move_dest_name", n_move_dest_name_like);
        }
    }
	private String n_account_analytic_name_eq;//[分析账户]
	public void setN_account_analytic_name_eq(String n_account_analytic_name_eq) {
        this.n_account_analytic_name_eq = n_account_analytic_name_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_name_eq)){
            this.getSearchCond().eq("account_analytic_name", n_account_analytic_name_eq);
        }
    }
	private String n_account_analytic_name_like;//[分析账户]
	public void setN_account_analytic_name_like(String n_account_analytic_name_like) {
        this.n_account_analytic_name_like = n_account_analytic_name_like;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_name_like)){
            this.getSearchCond().like("account_analytic_name", n_account_analytic_name_like);
        }
    }
	private String n_write_uname_eq;//[最后更新人]
	public void setN_write_uname_eq(String n_write_uname_eq) {
        this.n_write_uname_eq = n_write_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uname_eq)){
            this.getSearchCond().eq("write_uname", n_write_uname_eq);
        }
    }
	private String n_write_uname_like;//[最后更新人]
	public void setN_write_uname_like(String n_write_uname_like) {
        this.n_write_uname_like = n_write_uname_like;
        if(!ObjectUtils.isEmpty(this.n_write_uname_like)){
            this.getSearchCond().like("write_uname", n_write_uname_like);
        }
    }
	private String n_product_name_eq;//[产品]
	public void setN_product_name_eq(String n_product_name_eq) {
        this.n_product_name_eq = n_product_name_eq;
        if(!ObjectUtils.isEmpty(this.n_product_name_eq)){
            this.getSearchCond().eq("product_name", n_product_name_eq);
        }
    }
	private String n_product_name_like;//[产品]
	public void setN_product_name_like(String n_product_name_like) {
        this.n_product_name_like = n_product_name_like;
        if(!ObjectUtils.isEmpty(this.n_product_name_like)){
            this.getSearchCond().like("product_name", n_product_name_like);
        }
    }
	private String n_create_uname_eq;//[创建人]
	public void setN_create_uname_eq(String n_create_uname_eq) {
        this.n_create_uname_eq = n_create_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uname_eq)){
            this.getSearchCond().eq("create_uname", n_create_uname_eq);
        }
    }
	private String n_create_uname_like;//[创建人]
	public void setN_create_uname_like(String n_create_uname_like) {
        this.n_create_uname_like = n_create_uname_like;
        if(!ObjectUtils.isEmpty(this.n_create_uname_like)){
            this.getSearchCond().like("create_uname", n_create_uname_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_product_uom_id_eq;//[单位]
	public void setN_product_uom_id_eq(Long n_product_uom_id_eq) {
        this.n_product_uom_id_eq = n_product_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_eq)){
            this.getSearchCond().eq("product_uom_id", n_product_uom_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_move_dest_id_eq;//[说明]
	public void setN_move_dest_id_eq(Long n_move_dest_id_eq) {
        this.n_move_dest_id_eq = n_move_dest_id_eq;
        if(!ObjectUtils.isEmpty(this.n_move_dest_id_eq)){
            this.getSearchCond().eq("move_dest_id", n_move_dest_id_eq);
        }
    }
	private Long n_account_analytic_id_eq;//[分析账户]
	public void setN_account_analytic_id_eq(Long n_account_analytic_id_eq) {
        this.n_account_analytic_id_eq = n_account_analytic_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_eq)){
            this.getSearchCond().eq("account_analytic_id", n_account_analytic_id_eq);
        }
    }
	private Long n_requisition_id_eq;//[采购申请]
	public void setN_requisition_id_eq(Long n_requisition_id_eq) {
        this.n_requisition_id_eq = n_requisition_id_eq;
        if(!ObjectUtils.isEmpty(this.n_requisition_id_eq)){
            this.getSearchCond().eq("requisition_id", n_requisition_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



