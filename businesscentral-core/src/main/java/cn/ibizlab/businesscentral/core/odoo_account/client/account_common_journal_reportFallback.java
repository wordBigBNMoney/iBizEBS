package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_common_journal_report;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_common_journal_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_common_journal_report] 服务对象接口
 */
@Component
public class account_common_journal_reportFallback implements account_common_journal_reportFeignClient{

    public Page<Account_common_journal_report> search(Account_common_journal_reportSearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_common_journal_report get(Long id){
            return null;
     }



    public Account_common_journal_report create(Account_common_journal_report account_common_journal_report){
            return null;
     }
    public Boolean createBatch(List<Account_common_journal_report> account_common_journal_reports){
            return false;
     }


    public Account_common_journal_report update(Long id, Account_common_journal_report account_common_journal_report){
            return null;
     }
    public Boolean updateBatch(List<Account_common_journal_report> account_common_journal_reports){
            return false;
     }


    public Page<Account_common_journal_report> select(){
            return null;
     }

    public Account_common_journal_report getDraft(){
            return null;
    }



    public Boolean checkKey(Account_common_journal_report account_common_journal_report){
            return false;
     }


    public Boolean save(Account_common_journal_report account_common_journal_report){
            return false;
     }
    public Boolean saveBatch(List<Account_common_journal_report> account_common_journal_reports){
            return false;
     }

    public Page<Account_common_journal_report> searchDefault(Account_common_journal_reportSearchContext context){
            return null;
     }


}
