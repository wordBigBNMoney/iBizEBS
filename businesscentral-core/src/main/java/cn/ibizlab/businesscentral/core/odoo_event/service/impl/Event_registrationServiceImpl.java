package cn.ibizlab.businesscentral.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_registration;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_registrationSearchContext;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_event.mapper.Event_registrationMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[事件记录] 服务对象接口实现
 */
@Slf4j
@Service("Event_registrationServiceImpl")
public class Event_registrationServiceImpl extends EBSServiceImpl<Event_registrationMapper, Event_registration> implements IEvent_registrationService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_mail_registrationService eventMailRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_event_ticketService eventEventTicketService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_eventService eventEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "event.registration" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Event_registration et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_registrationService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Event_registration> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Event_registration et) {
        Event_registration old = new Event_registration() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_registrationService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_registrationService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Event_registration> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        eventMailRegistrationService.removeByRegistrationId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        eventMailRegistrationService.removeByRegistrationId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Event_registration get(Long key) {
        Event_registration et = getById(key);
        if(et==null){
            et=new Event_registration();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Event_registration getDraft(Event_registration et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Event_registration et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Event_registration et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Event_registration et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Event_registration> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Event_registration> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Event_registration> selectByEventTicketId(Long id) {
        return baseMapper.selectByEventTicketId(id);
    }
    @Override
    public void resetByEventTicketId(Long id) {
        this.update(new UpdateWrapper<Event_registration>().set("event_ticket_id",null).eq("event_ticket_id",id));
    }

    @Override
    public void resetByEventTicketId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_registration>().set("event_ticket_id",null).in("event_ticket_id",ids));
    }

    @Override
    public void removeByEventTicketId(Long id) {
        this.remove(new QueryWrapper<Event_registration>().eq("event_ticket_id",id));
    }

	@Override
    public List<Event_registration> selectByEventId(Long id) {
        return baseMapper.selectByEventId(id);
    }
    @Override
    public void resetByEventId(Long id) {
        this.update(new UpdateWrapper<Event_registration>().set("event_id",null).eq("event_id",id));
    }

    @Override
    public void resetByEventId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_registration>().set("event_id",null).in("event_id",ids));
    }

    @Override
    public void removeByEventId(Long id) {
        this.remove(new QueryWrapper<Event_registration>().eq("event_id",id));
    }

	@Override
    public List<Event_registration> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Event_registration>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_registration>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Event_registration>().eq("company_id",id));
    }

	@Override
    public List<Event_registration> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Event_registration>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_registration>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Event_registration>().eq("partner_id",id));
    }

	@Override
    public List<Event_registration> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Event_registration>().eq("create_uid",id));
    }

	@Override
    public List<Event_registration> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Event_registration>().eq("write_uid",id));
    }

	@Override
    public List<Event_registration> selectBySaleOrderLineId(Long id) {
        return baseMapper.selectBySaleOrderLineId(id);
    }
    @Override
    public void removeBySaleOrderLineId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Event_registration>().in("sale_order_line_id",ids));
    }

    @Override
    public void removeBySaleOrderLineId(Long id) {
        this.remove(new QueryWrapper<Event_registration>().eq("sale_order_line_id",id));
    }

	@Override
    public List<Event_registration> selectBySaleOrderId(Long id) {
        return baseMapper.selectBySaleOrderId(id);
    }
    @Override
    public void removeBySaleOrderId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Event_registration>().in("sale_order_id",ids));
    }

    @Override
    public void removeBySaleOrderId(Long id) {
        this.remove(new QueryWrapper<Event_registration>().eq("sale_order_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Event_registration> searchDefault(Event_registrationSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Event_registration> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Event_registration>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Event_registration et){
        //实体关系[DER1N_EVENT_REGISTRATION__EVENT_EVENT_TICKET__EVENT_TICKET_ID]
        if(!ObjectUtils.isEmpty(et.getEventTicketId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket odooEventTicket=et.getOdooEventTicket();
            if(ObjectUtils.isEmpty(odooEventTicket)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket majorEntity=eventEventTicketService.get(et.getEventTicketId());
                et.setOdooEventTicket(majorEntity);
                odooEventTicket=majorEntity;
            }
            et.setEventTicketIdText(odooEventTicket.getName());
        }
        //实体关系[DER1N_EVENT_REGISTRATION__EVENT_EVENT__EVENT_ID]
        if(!ObjectUtils.isEmpty(et.getEventId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event odooEvent=et.getOdooEvent();
            if(ObjectUtils.isEmpty(odooEvent)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event majorEntity=eventEventService.get(et.getEventId());
                et.setOdooEvent(majorEntity);
                odooEvent=majorEntity;
            }
            et.setEventBeginDate(odooEvent.getDateBegin());
            et.setEventEndDate(odooEvent.getDateEnd());
            et.setEventIdText(odooEvent.getName());
        }
        //实体关系[DER1N_EVENT_REGISTRATION__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_EVENT_REGISTRATION__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_EVENT_REGISTRATION__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_EVENT_REGISTRATION__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_EVENT_REGISTRATION__SALE_ORDER_LINE__SALE_ORDER_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getSaleOrderLineId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line odooSaleOrderLine=et.getOdooSaleOrderLine();
            if(ObjectUtils.isEmpty(odooSaleOrderLine)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line majorEntity=saleOrderLineService.get(et.getSaleOrderLineId());
                et.setOdooSaleOrderLine(majorEntity);
                odooSaleOrderLine=majorEntity;
            }
            et.setSaleOrderLineIdText(odooSaleOrderLine.getName());
        }
        //实体关系[DER1N_EVENT_REGISTRATION__SALE_ORDER__SALE_ORDER_ID]
        if(!ObjectUtils.isEmpty(et.getSaleOrderId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooSaleOrder=et.getOdooSaleOrder();
            if(ObjectUtils.isEmpty(odooSaleOrder)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order majorEntity=saleOrderService.get(et.getSaleOrderId());
                et.setOdooSaleOrder(majorEntity);
                odooSaleOrder=majorEntity;
            }
            et.setSaleOrderIdText(odooSaleOrder.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Event_registration> getEventRegistrationByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Event_registration> getEventRegistrationByEntities(List<Event_registration> entities) {
        List ids =new ArrayList();
        for(Event_registration entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



