package cn.ibizlab.businesscentral.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_user_input_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Survey_user_input_line] 服务对象接口
 */
public interface ISurvey_user_input_lineService extends IService<Survey_user_input_line>{

    boolean create(Survey_user_input_line et) ;
    void createBatch(List<Survey_user_input_line> list) ;
    boolean update(Survey_user_input_line et) ;
    void updateBatch(List<Survey_user_input_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Survey_user_input_line get(Long key) ;
    Survey_user_input_line getDraft(Survey_user_input_line et) ;
    boolean checkKey(Survey_user_input_line et) ;
    boolean save(Survey_user_input_line et) ;
    void saveBatch(List<Survey_user_input_line> list) ;
    Page<Survey_user_input_line> searchDefault(Survey_user_input_lineSearchContext context) ;
    List<Survey_user_input_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Survey_user_input_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Survey_user_input_line> selectByValueSuggested(Long id);
    void resetByValueSuggested(Long id);
    void resetByValueSuggested(Collection<Long> ids);
    void removeByValueSuggested(Long id);
    List<Survey_user_input_line> selectByValueSuggestedRow(Long id);
    void resetByValueSuggestedRow(Long id);
    void resetByValueSuggestedRow(Collection<Long> ids);
    void removeByValueSuggestedRow(Long id);
    List<Survey_user_input_line> selectByQuestionId(Long id);
    List<Survey_user_input_line> selectByQuestionId(Collection<Long> ids);
    void removeByQuestionId(Long id);
    List<Survey_user_input_line> selectBySurveyId(Long id);
    void resetBySurveyId(Long id);
    void resetBySurveyId(Collection<Long> ids);
    void removeBySurveyId(Long id);
    List<Survey_user_input_line> selectByUserInputId(Long id);
    void removeByUserInputId(Collection<Long> ids);
    void removeByUserInputId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Survey_user_input_line> getSurveyUserInputLineByIds(List<Long> ids) ;
    List<Survey_user_input_line> getSurveyUserInputLineByEntities(List<Survey_user_input_line> entities) ;
}


