package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenterSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_workcenter] 服务对象接口
 */
public interface IMrp_workcenterService extends IService<Mrp_workcenter>{

    boolean create(Mrp_workcenter et) ;
    void createBatch(List<Mrp_workcenter> list) ;
    boolean update(Mrp_workcenter et) ;
    void updateBatch(List<Mrp_workcenter> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_workcenter get(Long key) ;
    Mrp_workcenter getDraft(Mrp_workcenter et) ;
    boolean checkKey(Mrp_workcenter et) ;
    boolean save(Mrp_workcenter et) ;
    void saveBatch(List<Mrp_workcenter> list) ;
    Page<Mrp_workcenter> searchDefault(Mrp_workcenterSearchContext context) ;
    List<Mrp_workcenter> selectByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Collection<Long> ids);
    void removeByResourceCalendarId(Long id);
    List<Mrp_workcenter> selectByResourceId(Long id);
    List<Mrp_workcenter> selectByResourceId(Collection<Long> ids);
    void removeByResourceId(Long id);
    List<Mrp_workcenter> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Mrp_workcenter> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_workcenter> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_workcenter> getMrpWorkcenterByIds(List<Long> ids) ;
    List<Mrp_workcenter> getMrpWorkcenterByEntities(List<Mrp_workcenter> entities) ;
}


