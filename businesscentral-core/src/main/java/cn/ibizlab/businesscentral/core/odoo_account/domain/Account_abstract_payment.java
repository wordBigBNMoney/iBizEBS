package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[包含在允许登记付款的模块之间共享的逻辑]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_ABSTRACT_PAYMENT",resultMap = "Account_abstract_paymentResultMap")
public class Account_abstract_payment extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 付款差异处理
     */
    @DEField(name = "payment_difference_handling")
    @TableField(value = "payment_difference_handling")
    @JSONField(name = "payment_difference_handling")
    @JsonProperty("payment_difference_handling")
    private String paymentDifferenceHandling;
    /**
     * 业务伙伴类型
     */
    @DEField(name = "partner_type")
    @TableField(value = "partner_type")
    @JSONField(name = "partner_type")
    @JsonProperty("partner_type")
    private String partnerType;
    /**
     * 付款金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private BigDecimal amount;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;
    /**
     * 付款差异
     */
    @TableField(exist = false)
    @JSONField(name = "payment_difference")
    @JsonProperty("payment_difference")
    private BigDecimal paymentDifference;
    /**
     * 多
     */
    @TableField(value = "multi")
    @JSONField(name = "multi")
    @JsonProperty("multi")
    private Boolean multi;
    /**
     * 日记账项目标签
     */
    @DEField(name = "writeoff_label")
    @TableField(value = "writeoff_label")
    @JSONField(name = "writeoff_label")
    @JsonProperty("writeoff_label")
    private String writeoffLabel;
    /**
     * 显示合作伙伴银行账户
     */
    @TableField(exist = false)
    @JSONField(name = "show_partner_bank_account")
    @JsonProperty("show_partner_bank_account")
    private Boolean showPartnerBankAccount;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 隐藏付款方式
     */
    @TableField(exist = false)
    @JSONField(name = "hide_payment_method")
    @JsonProperty("hide_payment_method")
    private Boolean hidePaymentMethod;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 备忘
     */
    @TableField(value = "communication")
    @JSONField(name = "communication")
    @JsonProperty("communication")
    private String communication;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 付款类型
     */
    @DEField(name = "payment_type")
    @TableField(value = "payment_type")
    @JSONField(name = "payment_type")
    @JsonProperty("payment_type")
    private String paymentType;
    /**
     * 付款日期
     */
    @DEField(name = "payment_date")
    @TableField(value = "payment_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "payment_date" , format="yyyy-MM-dd")
    @JsonProperty("payment_date")
    private Timestamp paymentDate;
    /**
     * 付款日记账
     */
    @TableField(exist = false)
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 代码
     */
    @TableField(exist = false)
    @JSONField(name = "payment_method_code")
    @JsonProperty("payment_method_code")
    private String paymentMethodCode;
    /**
     * 差异科目
     */
    @TableField(exist = false)
    @JSONField(name = "writeoff_account_id_text")
    @JsonProperty("writeoff_account_id_text")
    private String writeoffAccountIdText;
    /**
     * 业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 付款方法类型
     */
    @TableField(exist = false)
    @JSONField(name = "payment_method_id_text")
    @JsonProperty("payment_method_id_text")
    private String paymentMethodIdText;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 付款日记账
     */
    @DEField(name = "journal_id")
    @TableField(value = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Long journalId;
    /**
     * 收款银行账号
     */
    @DEField(name = "partner_bank_account_id")
    @TableField(value = "partner_bank_account_id")
    @JSONField(name = "partner_bank_account_id")
    @JsonProperty("partner_bank_account_id")
    private Long partnerBankAccountId;
    /**
     * 付款方法类型
     */
    @DEField(name = "payment_method_id")
    @TableField(value = "payment_method_id")
    @JSONField(name = "payment_method_id")
    @JsonProperty("payment_method_id")
    private Long paymentMethodId;
    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 差异科目
     */
    @DEField(name = "writeoff_account_id")
    @TableField(value = "writeoff_account_id")
    @JSONField(name = "writeoff_account_id")
    @JsonProperty("writeoff_account_id")
    private Long writeoffAccountId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooWriteoffAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_method odooPaymentMethod;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank odooPartnerBankAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;



    /**
     * 设置 [付款差异处理]
     */
    public void setPaymentDifferenceHandling(String paymentDifferenceHandling){
        this.paymentDifferenceHandling = paymentDifferenceHandling ;
        this.modify("payment_difference_handling",paymentDifferenceHandling);
    }

    /**
     * 设置 [业务伙伴类型]
     */
    public void setPartnerType(String partnerType){
        this.partnerType = partnerType ;
        this.modify("partner_type",partnerType);
    }

    /**
     * 设置 [付款金额]
     */
    public void setAmount(BigDecimal amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [多]
     */
    public void setMulti(Boolean multi){
        this.multi = multi ;
        this.modify("multi",multi);
    }

    /**
     * 设置 [日记账项目标签]
     */
    public void setWriteoffLabel(String writeoffLabel){
        this.writeoffLabel = writeoffLabel ;
        this.modify("writeoff_label",writeoffLabel);
    }

    /**
     * 设置 [备忘]
     */
    public void setCommunication(String communication){
        this.communication = communication ;
        this.modify("communication",communication);
    }

    /**
     * 设置 [付款类型]
     */
    public void setPaymentType(String paymentType){
        this.paymentType = paymentType ;
        this.modify("payment_type",paymentType);
    }

    /**
     * 设置 [付款日期]
     */
    public void setPaymentDate(Timestamp paymentDate){
        this.paymentDate = paymentDate ;
        this.modify("payment_date",paymentDate);
    }

    /**
     * 格式化日期 [付款日期]
     */
    public String formatPaymentDate(){
        if (this.paymentDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(paymentDate);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [付款日记账]
     */
    public void setJournalId(Long journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [收款银行账号]
     */
    public void setPartnerBankAccountId(Long partnerBankAccountId){
        this.partnerBankAccountId = partnerBankAccountId ;
        this.modify("partner_bank_account_id",partnerBankAccountId);
    }

    /**
     * 设置 [付款方法类型]
     */
    public void setPaymentMethodId(Long paymentMethodId){
        this.paymentMethodId = paymentMethodId ;
        this.modify("payment_method_id",paymentMethodId);
    }

    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [差异科目]
     */
    public void setWriteoffAccountId(Long writeoffAccountId){
        this.writeoffAccountId = writeoffAccountId ;
        this.modify("writeoff_account_id",writeoffAccountId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


