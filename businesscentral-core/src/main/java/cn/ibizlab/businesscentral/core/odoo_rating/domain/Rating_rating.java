package cn.ibizlab.businesscentral.core.odoo_rating.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[评级]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RATING_RATING",resultMap = "Rating_ratingResultMap")
public class Rating_rating extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 备注
     */
    @TableField(value = "feedback")
    @JSONField(name = "feedback")
    @JsonProperty("feedback")
    private String feedback;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 评级数值
     */
    @TableField(value = "rating")
    @JSONField(name = "rating")
    @JsonProperty("rating")
    private Double rating;
    /**
     * 评级
     */
    @DEField(name = "rating_text")
    @TableField(value = "rating_text")
    @JSONField(name = "rating_text")
    @JsonProperty("rating_text")
    private String ratingText;
    /**
     * 父级文档名称
     */
    @DEField(name = "parent_res_name")
    @TableField(value = "parent_res_name")
    @JSONField(name = "parent_res_name")
    @JsonProperty("parent_res_name")
    private String parentResName;
    /**
     * 父级文档
     */
    @DEField(name = "parent_res_id")
    @TableField(value = "parent_res_id")
    @JSONField(name = "parent_res_id")
    @JsonProperty("parent_res_id")
    private Integer parentResId;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 资源名称
     */
    @DEField(name = "res_name")
    @TableField(value = "res_name")
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    private String resName;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 父级相关文档模型
     */
    @DEField(name = "parent_res_model_id")
    @TableField(value = "parent_res_model_id")
    @JSONField(name = "parent_res_model_id")
    @JsonProperty("parent_res_model_id")
    private Integer parentResModelId;
    /**
     * 文档
     */
    @DEField(name = "res_id")
    @TableField(value = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * 已填写的评级
     */
    @TableField(value = "consumed")
    @JSONField(name = "consumed")
    @JsonProperty("consumed")
    private Boolean consumed;
    /**
     * 图像
     */
    @TableField(exist = false)
    @JSONField(name = "rating_image")
    @JsonProperty("rating_image")
    private byte[] ratingImage;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 相关的文档模型
     */
    @DEField(name = "res_model_id")
    @TableField(value = "res_model_id")
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;
    /**
     * 文档模型
     */
    @DEField(name = "res_model")
    @TableField(value = "res_model")
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;
    /**
     * 安全令牌
     */
    @DEField(name = "access_token")
    @TableField(value = "access_token")
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;
    /**
     * 父级文档模型
     */
    @DEField(name = "parent_res_model")
    @TableField(value = "parent_res_model")
    @JSONField(name = "parent_res_model")
    @JsonProperty("parent_res_model")
    private String parentResModel;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 客户
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 评级人员
     */
    @TableField(exist = false)
    @JSONField(name = "rated_partner_id_text")
    @JsonProperty("rated_partner_id_text")
    private String ratedPartnerIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 已发布
     */
    @TableField(exist = false)
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 链接信息
     */
    @DEField(name = "message_id")
    @TableField(value = "message_id")
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private Long messageId;
    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 评级人员
     */
    @DEField(name = "rated_partner_id")
    @TableField(value = "rated_partner_id")
    @JSONField(name = "rated_partner_id")
    @JsonProperty("rated_partner_id")
    private Long ratedPartnerId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message odooMessage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooRatedPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [备注]
     */
    public void setFeedback(String feedback){
        this.feedback = feedback ;
        this.modify("feedback",feedback);
    }

    /**
     * 设置 [评级数值]
     */
    public void setRating(Double rating){
        this.rating = rating ;
        this.modify("rating",rating);
    }

    /**
     * 设置 [评级]
     */
    public void setRatingText(String ratingText){
        this.ratingText = ratingText ;
        this.modify("rating_text",ratingText);
    }

    /**
     * 设置 [父级文档名称]
     */
    public void setParentResName(String parentResName){
        this.parentResName = parentResName ;
        this.modify("parent_res_name",parentResName);
    }

    /**
     * 设置 [父级文档]
     */
    public void setParentResId(Integer parentResId){
        this.parentResId = parentResId ;
        this.modify("parent_res_id",parentResId);
    }

    /**
     * 设置 [资源名称]
     */
    public void setResName(String resName){
        this.resName = resName ;
        this.modify("res_name",resName);
    }

    /**
     * 设置 [父级相关文档模型]
     */
    public void setParentResModelId(Integer parentResModelId){
        this.parentResModelId = parentResModelId ;
        this.modify("parent_res_model_id",parentResModelId);
    }

    /**
     * 设置 [文档]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [已填写的评级]
     */
    public void setConsumed(Boolean consumed){
        this.consumed = consumed ;
        this.modify("consumed",consumed);
    }

    /**
     * 设置 [相关的文档模型]
     */
    public void setResModelId(Integer resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }

    /**
     * 设置 [文档模型]
     */
    public void setResModel(String resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [安全令牌]
     */
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [父级文档模型]
     */
    public void setParentResModel(String parentResModel){
        this.parentResModel = parentResModel ;
        this.modify("parent_res_model",parentResModel);
    }

    /**
     * 设置 [链接信息]
     */
    public void setMessageId(Long messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }

    /**
     * 设置 [客户]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [评级人员]
     */
    public void setRatedPartnerId(Long ratedPartnerId){
        this.ratedPartnerId = ratedPartnerId ;
        this.modify("rated_partner_id",ratedPartnerId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


