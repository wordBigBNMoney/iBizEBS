package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_chart_templateSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_chart_templateMapper extends BaseMapper<Account_chart_template>{

    Page<Account_chart_template> searchDefault(IPage page, @Param("srf") Account_chart_templateSearchContext context, @Param("ew") Wrapper<Account_chart_template> wrapper) ;
    @Override
    Account_chart_template selectById(Serializable id);
    @Override
    int insert(Account_chart_template entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_chart_template entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_chart_template entity, @Param("ew") Wrapper<Account_chart_template> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_chart_template> selectByExpenseCurrencyExchangeAccountId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByIncomeCurrencyExchangeAccountId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyAccountExpenseCategId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyAccountExpenseId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyAccountIncomeCategId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyAccountIncomeId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyAccountPayableId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyAccountReceivableId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyStockAccountInputCategId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyStockAccountOutputCategId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByPropertyStockValuationAccountId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByParentId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_chart_template> selectByWriteUid(@Param("id") Serializable id) ;


}
