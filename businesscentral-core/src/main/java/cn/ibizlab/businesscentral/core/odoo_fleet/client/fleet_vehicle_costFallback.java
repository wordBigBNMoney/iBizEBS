package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_cost] 服务对象接口
 */
@Component
public class fleet_vehicle_costFallback implements fleet_vehicle_costFeignClient{


    public Page<Fleet_vehicle_cost> search(Fleet_vehicle_costSearchContext context){
            return null;
     }


    public Fleet_vehicle_cost create(Fleet_vehicle_cost fleet_vehicle_cost){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_cost> fleet_vehicle_costs){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Fleet_vehicle_cost update(Long id, Fleet_vehicle_cost fleet_vehicle_cost){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_cost> fleet_vehicle_costs){
            return false;
     }


    public Fleet_vehicle_cost get(Long id){
            return null;
     }




    public Page<Fleet_vehicle_cost> select(){
            return null;
     }

    public Fleet_vehicle_cost getDraft(){
            return null;
    }



    public Boolean checkKey(Fleet_vehicle_cost fleet_vehicle_cost){
            return false;
     }


    public Boolean save(Fleet_vehicle_cost fleet_vehicle_cost){
            return false;
     }
    public Boolean saveBatch(List<Fleet_vehicle_cost> fleet_vehicle_costs){
            return false;
     }

    public Page<Fleet_vehicle_cost> searchDefault(Fleet_vehicle_costSearchContext context){
            return null;
     }


}
