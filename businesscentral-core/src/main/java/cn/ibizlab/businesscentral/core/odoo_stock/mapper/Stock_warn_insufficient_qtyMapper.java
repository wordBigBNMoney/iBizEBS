package cn.ibizlab.businesscentral.core.odoo_stock.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qtySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Stock_warn_insufficient_qtyMapper extends BaseMapper<Stock_warn_insufficient_qty>{

    Page<Stock_warn_insufficient_qty> searchDefault(IPage page, @Param("srf") Stock_warn_insufficient_qtySearchContext context, @Param("ew") Wrapper<Stock_warn_insufficient_qty> wrapper) ;
    @Override
    Stock_warn_insufficient_qty selectById(Serializable id);
    @Override
    int insert(Stock_warn_insufficient_qty entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Stock_warn_insufficient_qty entity);
    @Override
    int update(@Param(Constants.ENTITY) Stock_warn_insufficient_qty entity, @Param("ew") Wrapper<Stock_warn_insufficient_qty> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Stock_warn_insufficient_qty> selectByProductId(@Param("id") Serializable id) ;

    List<Stock_warn_insufficient_qty> selectByLocationId(@Param("id") Serializable id) ;


}
