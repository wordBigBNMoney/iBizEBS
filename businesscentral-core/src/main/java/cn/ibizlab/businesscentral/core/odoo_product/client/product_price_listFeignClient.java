package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_price_listSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_price_list] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-price-list", fallback = product_price_listFallback.class)
public interface product_price_listFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/product_price_lists")
    Product_price_list create(@RequestBody Product_price_list product_price_list);

    @RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/batch")
    Boolean createBatch(@RequestBody List<Product_price_list> product_price_lists);



    @RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/{id}")
    Product_price_list get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_price_lists/{id}")
    Product_price_list update(@PathVariable("id") Long id,@RequestBody Product_price_list product_price_list);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_price_lists/batch")
    Boolean updateBatch(@RequestBody List<Product_price_list> product_price_lists);



    @RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/search")
    Page<Product_price_list> search(@RequestBody Product_price_listSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_price_lists/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_price_lists/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/select")
    Page<Product_price_list> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/getdraft")
    Product_price_list getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/checkkey")
    Boolean checkKey(@RequestBody Product_price_list product_price_list);


    @RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/save")
    Boolean save(@RequestBody Product_price_list product_price_list);

    @RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/savebatch")
    Boolean saveBatch(@RequestBody List<Product_price_list> product_price_lists);



    @RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/searchdefault")
    Page<Product_price_list> searchDefault(@RequestBody Product_price_listSearchContext context);


}
