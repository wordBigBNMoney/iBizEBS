package cn.ibizlab.businesscentral.util.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "ibiz.client.odoo")
@Data
public class OdooClientProperties {

    private String url;

    private String db;

    private int userId;

    private String pass;

}