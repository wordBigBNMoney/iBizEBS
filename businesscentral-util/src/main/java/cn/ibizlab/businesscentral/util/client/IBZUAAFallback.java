package cn.ibizlab.businesscentral.util.client;

import cn.ibizlab.businesscentral.util.security.AuthenticationUser;
import cn.ibizlab.businesscentral.util.security.AuthorizationLogin;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;

@Component
public class IBZUAAFallback implements IBZUAAFeignClient {

    @Override
    public Boolean syncSysAuthority(JSONObject system) {
        return null;
    }

    @Override
    public AuthenticationUser login(AuthorizationLogin authorizationLogin) {
        return null;
    }

	@Override
    public AuthenticationUser loginByUsername(String username) {
        return null;
    }

    @Override
    public String getPublicKey() {
        return null;
    }
}
