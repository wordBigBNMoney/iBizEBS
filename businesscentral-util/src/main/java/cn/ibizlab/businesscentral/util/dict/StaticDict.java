package cn.ibizlab.businesscentral.util.dict;

import lombok.Getter;

public class StaticDict {


    /**
     * 代码表[HR_LEAVE_TYPE__ALLOCATION_TYPE]
     */
    @Getter
    public enum HR_LEAVE_TYPE__ALLOCATION_TYPE {
        NO("no","无需分配"),
        FIXED_ALLOCATION("fixed_allocation","自由的分配请求"),
        FIXED("fixed","仅由人力资源分配");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_TYPE__ALLOCATION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[WEBSITE_REWRITE__REDIRECT_TYPE]
     */
    @Getter
    public enum WEBSITE_REWRITE__REDIRECT_TYPE {
        ITEM_404("404","404 Not Found"),
        ITEM_301("301","301 Moved permanently"),
        ITEM_302("302","302 Moved temporarily"),
        ITEM_308("308","308 Redirect / Rewrite");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        WEBSITE_REWRITE__REDIRECT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__DEFAULT_PURCHASE_METHOD]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__DEFAULT_PURCHASE_METHOD {
        PURCHASE("purchase","订购数量"),
        RECEIVE("receive","收到数量");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__DEFAULT_PURCHASE_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }






    /**
     * 代码表[RES_COMPANY__ACCOUNT_SETUP_COA_STATE]
     */
    @Getter
    public enum RES_COMPANY__ACCOUNT_SETUP_COA_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__ACCOUNT_SETUP_COA_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[FLEET_VEHICLE_LOG_CONTRACT__ACTIVITY_STATE]
     */
    @Getter
    public enum FLEET_VEHICLE_LOG_CONTRACT__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE_LOG_CONTRACT__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_COMMON_REPORT__TARGET_MOVE]
     */
    @Getter
    public enum ACCOUNT_COMMON_REPORT__TARGET_MOVE {
        POSTED("posted","所有已过账分录"),
        ALL("all","所有分录");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_COMMON_REPORT__TARGET_MOVE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_COMPANY__ACCOUNT_SETUP_FY_DATA_STATE]
     */
    @Getter
    public enum RES_COMPANY__ACCOUNT_SETUP_FY_DATA_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__ACCOUNT_SETUP_FY_DATA_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RATING_RATING__RATING_TEXT]
     */
    @Getter
    public enum RATING_RATING__RATING_TEXT {
        SATISFIED("satisfied","满意"),
        NOT_SATISFIED("not_satisfied","不满意"),
        HIGHLY_DISSATISFIED("highly_dissatisfied","非常不满意"),
        NO_RATING("no_rating","尚未评级");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RATING_RATING__RATING_TEXT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__SALE_ONBOARDING_ORDER_CONFIRMATION_STATE]
     */
    @Getter
    public enum RES_COMPANY__SALE_ONBOARDING_ORDER_CONFIRMATION_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__SALE_ONBOARDING_ORDER_CONFIRMATION_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[BASE_LANGUAGE_EXPORT__FORMAT]
     */
    @Getter
    public enum BASE_LANGUAGE_EXPORT__FORMAT {
        CSV("csv","CSV文件"),
        PO("po","PO文件"),
        TGZ("tgz","TGZ 存档");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        BASE_LANGUAGE_EXPORT__FORMAT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PRODUCT_TEMPLATE__PURCHASE_METHOD]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__PURCHASE_METHOD {
        PURCHASE("purchase","订购数量"),
        RECEIVE("receive","收到数量");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__PURCHASE_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SMS_COMPOSER__COMPOSITION_MODE]
     */
    @Getter
    public enum SMS_COMPOSER__COMPOSITION_MODE {
        NUMBERS("numbers","发送到数字"),
        COMMENT("comment","提交在文件上"),
        MASS("mass","批处理发送短信");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SMS_COMPOSER__COMPOSITION_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_PICKING__MOVE_TYPE]
     */
    @Getter
    public enum STOCK_PICKING__MOVE_TYPE {
        DIRECT("direct","尽快"),
        ONE("one","当所有产品就绪时");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PICKING__MOVE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[GAMIFICATION_CHALLENGE__VISIBILITY_MODE]
     */
    @Getter
    public enum GAMIFICATION_CHALLENGE__VISIBILITY_MODE {
        PERSONAL("personal","个人目标"),
        RANKING("ranking","排行榜(小组排行)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_CHALLENGE__VISIBILITY_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EXPENSE_SHEET__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum HR_EXPENSE_SHEET__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EXPENSE_SHEET__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_INVENTORY__STATE]
     */
    @Getter
    public enum STOCK_INVENTORY__STATE {
        DRAFT("draft","草稿"),
        CANCEL("cancel","已取消"),
        CONFIRM("confirm","进行中"),
        DONE("done","已验证");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_INVENTORY__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[LUNCH_ALERT__RECIPIENTS]
     */
    @Getter
    public enum LUNCH_ALERT__RECIPIENTS {
        EVERYONE("everyone","所有人"),
        LAST_WEEK("last_week","上周订购的员工"),
        LAST_MONTH("last_month","上个月订购的员工"),
        LAST_YEAR("last_year","去年订购的员工");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_ALERT__RECIPIENTS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[IR_SEQUENCE__IMPLEMENTATION]
     */
    @Getter
    public enum IR_SEQUENCE__IMPLEMENTATION {
        STANDARD("standard","标准"),
        NO_GAP("no_gap","无间隔");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        IR_SEQUENCE__IMPLEMENTATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[真假（true,false）]
     */
    @Getter
    public enum Odoo_truefalse {
        FALSE("false","否"),
        TRUE("true","是");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        Odoo_truefalse(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[扩展表格单元格垂直对齐方式]
     */
    @Getter
    public enum CodeList75 {
        TOP("TOP","上对齐"),
        MIDDLE("MIDDLE","居中对齐"),
        BOTTOM("BOTTOM","下对齐");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList75(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[GAMIFICATION_CHALLENGE__PERIOD]
     */
    @Getter
    public enum GAMIFICATION_CHALLENGE__PERIOD {
        ONCE("once","非经常性"),
        DAILY("daily","每天"),
        WEEKLY("weekly","每周"),
        MONTHLY("monthly","每月"),
        YEARLY("yearly","每年");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_CHALLENGE__PERIOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_TYPE__COLOR_NAME]
     */
    @Getter
    public enum HR_LEAVE_TYPE__COLOR_NAME {
        RED("red","红色"),
        BLUE("blue","蓝色"),
        LIGHTGREEN("lightgreen","浅绿色"),
        LIGHTBLUE("lightblue","浅蓝"),
        LIGHTYELLOW("lightyellow","浅黄色"),
        MAGENTA("magenta","洋红色"),
        LIGHTCYAN("lightcyan","浅青色"),
        BLACK("black","黑色"),
        LIGHTPINK("lightpink","浅粉红色"),
        BROWN("brown","棕色"),
        VIOLET("violet","紫色"),
        LIGHTCORAL("lightcoral","浅珊瑚色"),
        LIGHTSALMON("lightsalmon","浅橙红色"),
        LAVENDER("lavender","淡紫色"),
        WHEAT("wheat","麦黄色"),
        IVORY("ivory","象牙白");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_TYPE__COLOR_NAME(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[数据库触发器目标]
     */
    @Getter
    public enum CodeList68 {
        TABLE("TABLE","主表"),
        VIEW("VIEW","视图");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList68(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_WORKORDER__STATE]
     */
    @Getter
    public enum MRP_WORKORDER__STATE {
        PENDING("pending","Waiting for another WO"),
        READY("ready","就绪"),
        PROGRESS("progress","进行中"),
        DONE("done","已完成"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_WORKORDER__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_PRICELIST__DISCOUNT_POLICY]
     */
    @Getter
    public enum PRODUCT_PRICELIST__DISCOUNT_POLICY {
        WITH_DISCOUNT("with_discount","折扣包含在价格中"),
        WITHOUT_DISCOUNT("without_discount","给客户显示公开的价格和折扣");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_PRICELIST__DISCOUNT_POLICY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[PAYMENT_ACQUIRER__STATE]
     */
    @Getter
    public enum PAYMENT_ACQUIRER__STATE {
        DISABLED("disabled","禁用"),
        ENABLED("enabled","启用"),
        TEST("test","测试模式");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_ACQUIRER__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_RULE__GROUP_PROPAGATION_OPTION]
     */
    @Getter
    public enum STOCK_RULE__GROUP_PROPAGATION_OPTION {
        NONE("none","留空"),
        PROPAGATE("propagate","传导"),
        FIXED("fixed","固定");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_RULE__GROUP_PROPAGATION_OPTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_HOLIDAYS_SUMMARY_EMPLOYEE__HOLIDAY_TYPE]
     */
    @Getter
    public enum HR_HOLIDAYS_SUMMARY_EMPLOYEE__HOLIDAY_TYPE {
        APPROVED("Approved","已批准"),
        CONFIRMED("Confirmed","已确认"),
        BOTH("both","批准和确认");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_HOLIDAYS_SUMMARY_EMPLOYEE__HOLIDAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[审计行为]
     */
    @Getter
    public enum CodeList27 {
        CREATE("CREATE","建立"),
        UPDATE("UPDATE","更新"),
        DELETE("DELETE","删除");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList27(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_MOVE__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum ACCOUNT_MOVE__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_MOVE__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[REPAIR_LINE__STATE]
     */
    @Getter
    public enum REPAIR_LINE__STATE {
        DRAFT("draft","草稿"),
        CONFIRMED("confirmed","已确认"),
        DONE("done","完成"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        REPAIR_LINE__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_CONTRACT__ACTIVITY_STATE]
     */
    @Getter
    public enum HR_CONTRACT__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_CONTRACT__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_SURVEY__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum SURVEY_SURVEY__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_SURVEY__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[平台内置处理组件类型]
     */
    @Getter
    public enum CodeList118 {
        CODELISTFILLER("CODELISTFILLER","代码表填充器"),
        WFPROCESS("WFPROCESS","工作流嵌入处理"),
        DGACTIONHELPER("DGACTIONHELPER","表格后台处理对象"),
        FORMACTIONHELPER("FORMACTIONHELPER","表单后台处理类"),
        PAGE("PAGE","页面对象");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList118(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_ALARM__ALARM_TYPE]
     */
    @Getter
    public enum CALENDAR_ALARM__ALARM_TYPE {
        NOTIFICATION("notification","通知"),
        EMAIL("email","EMail"),
        SMS("sms","短信");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_ALARM__ALARM_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CRM_LEAD__PRIORITY]
     */
    @Getter
    public enum CRM_LEAD__PRIORITY {
        ITEM_0("0","低"),
        ITEM_1("1","媒介"),
        ITEM_2("2","高"),
        ITEM_3("3","非常高");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD__PRIORITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[MAINTENANCE_REQUEST__KANBAN_STATE]
     */
    @Getter
    public enum MAINTENANCE_REQUEST__KANBAN_STATE {
        NORMAL("normal","进行中"),
        BLOCKED("blocked","阻塞"),
        DONE("done","下一阶段就绪");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAINTENANCE_REQUEST__KANBAN_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CRM_LEAD__PHONE_STATE]
     */
    @Getter
    public enum CRM_LEAD__PHONE_STATE {
        CORRECT("correct","正确的"),
        INCORRECT("incorrect","不正确");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD__PHONE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[消息模板内容类型]
     */
    @Getter
    public enum CodeList42 {
        TEXT("TEXT","纯文本"),
        HTML("HTML","HTML网页");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList42(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PROJECT_PROJECT__RATING_STATUS_PERIOD]
     */
    @Getter
    public enum PROJECT_PROJECT__RATING_STATUS_PERIOD {
        DAILY("daily","每天"),
        WEEKLY("weekly","每周"),
        BIMONTHLY("bimonthly","一个月两次"),
        MONTHLY("monthly","一个月一次"),
        QUARTERLY("quarterly","季度"),
        YEARLY("yearly","每年");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PROJECT_PROJECT__RATING_STATUS_PERIOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[STOCK_PRODUCTION_LOT__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum STOCK_PRODUCTION_LOT__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PRODUCTION_LOT__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_ATTENDEE__AVAILABILITY]
     */
    @Getter
    public enum CALENDAR_ATTENDEE__AVAILABILITY {
        FREE("free","空闲"),
        BUSY("busy","忙碌");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_ATTENDEE__AVAILABILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE]
     */
    @Getter
    public enum SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE {
        NEW_USER("new_user","我没有Paypal账户"),
        EXISTING_USER("existing_user","我有Paypal账户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_SURVEY__QUESTIONS_SELECTION]
     */
    @Getter
    public enum SURVEY_SURVEY__QUESTIONS_SELECTION {
        ALL("all","所有问题"),
        RANDOM("random","每节随机");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_SURVEY__QUESTIONS_SELECTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_WORKORDER__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum MRP_WORKORDER__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_WORKORDER__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_PRODUCTION__ACTIVITY_STATE]
     */
    @Getter
    public enum MRP_PRODUCTION__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_PRODUCTION__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL__MATCH_NOTE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL__MATCH_NOTE {
        CONTAINS("contains","包含"),
        NOT_CONTAINS("not_contains","不包含"),
        MATCH_REGEX("match_regex","正则匹配");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL__MATCH_NOTE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[MAINTENANCE_REQUEST__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum MAINTENANCE_REQUEST__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAINTENANCE_REQUEST__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[字段查询扩展选项]
     */
    @Getter
    public enum CodeList60 {
        LIKE("LIKE","LIKE大小写敏感"),
        EQ("=","=（含其它）大小写敏感"),
        LIKESPLIT("LIKESPLIT","LIKE分解");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList60(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[EVENT_TYPE_MAIL__NOTIFICATION_TYPE]
     */
    @Getter
    public enum EVENT_TYPE_MAIL__NOTIFICATION_TYPE {
        MAIL("mail","邮件"),
        SMS("sms","短信");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_TYPE_MAIL__NOTIFICATION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_ALLOCATION__ACTIVITY_STATE]
     */
    @Getter
    public enum HR_LEAVE_ALLOCATION__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_ALLOCATION__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PAYMENT_ACQUIRER__PAYMENT_FLOW]
     */
    @Getter
    public enum PAYMENT_ACQUIRER__PAYMENT_FLOW {
        FORM("form","重定向跳转至所请求的网站"),
        S2S("s2s","Odoo付款");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_ACQUIRER__PAYMENT_FLOW(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MRP_UNBUILD__ACTIVITY_STATE]
     */
    @Getter
    public enum MRP_UNBUILD__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_UNBUILD__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[年份（2010～2020）]
     */
    @Getter
    public enum CodeList81 {
        ITEM_2010("2010","2010年"),
        ITEM_2011("2011","2011年"),
        ITEM_2012("2012","2012年"),
        ITEM_2013("2013","2013年"),
        ITEM_2014("2014","2014年"),
        ITEM_2015("2015","2015年"),
        ITEM_2016("2016","2016年"),
        ITEM_2017("2017","2017年"),
        ITEM_2018("2018","2018年"),
        ITEM_2019("2019","2019年"),
        ITEM_2020("2020","2020年");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList81(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[季度（1～4）]
     */
    @Getter
    public enum CodeList83 {
        ITEM_1("1","1季度"),
        ITEM_2("2","2季度"),
        ITEM_3("3","3季度"),
        ITEM_4("4","4季度");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList83(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__SALE_DELIVERY_SETTINGS]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__SALE_DELIVERY_SETTINGS {
        NONE("none","在网站中没有运输管理"),
        INTERNAL("internal","交货方式仅在内部使用：客户无须支付运费"),
        WEBSITE("website","交付方式可在网站上选择：客户支付运费");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__SALE_DELIVERY_SETTINGS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_ORDER__STATE]
     */
    @Getter
    public enum PURCHASE_ORDER__STATE {
        DRAFT("draft","询价单"),
        SENT("sent","发送询价单"),
        TO_APPROVE("to approve","待批准"),
        PURCHASE("purchase","采购订单"),
        DONE("done","已锁定"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_ORDER__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[FLEET_VEHICLE_LOG_CONTRACT__STATE]
     */
    @Getter
    public enum FLEET_VEHICLE_LOG_CONTRACT__STATE {
        FUTUR("futur","入库"),
        OPEN("open","进行中"),
        DIESOON("diesoon","即将过期"),
        EXPIRED("expired","过期"),
        CLOSED("closed","已关闭");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE_LOG_CONTRACT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[树视图节点类型]
     */
    @Getter
    public enum CodeList94 {
        STATIC("STATIC","静态"),
        DE("DE","动态（实体）"),
        CODELIST("CODELIST","动态（代码表）");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList94(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[WEBSITE_SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE]
     */
    @Getter
    public enum WEBSITE_SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE {
        NEW_USER("new_user","我没有Paypal账户"),
        EXISTING_USER("existing_user","我有Paypal账户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        WEBSITE_SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_PARTNER__COMPANY_TYPE]
     */
    @Getter
    public enum RES_PARTNER__COMPANY_TYPE {
        PERSON("person","个人"),
        COMPANY("company","公司");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__COMPANY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CRM_QUOTATION_PARTNER__ACTION]
     */
    @Getter
    public enum CRM_QUOTATION_PARTNER__ACTION {
        CREATE("create","创建客户"),
        EXIST("exist","链接到现有客户"),
        NOTHING("nothing","不要链接到某个客户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_QUOTATION_PARTNER__ACTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PROJECT_PROJECT__PRIVACY_VISIBILITY]
     */
    @Getter
    public enum PROJECT_PROJECT__PRIVACY_VISIBILITY {
        FOLLOWERS("followers","Invited employees"),
        EMPLOYEES("employees","All employees"),
        PORTAL("portal","Portal users and all employees");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PROJECT_PROJECT__PRIVACY_VISIBILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[SURVEY_QUESTION__MATRIX_SUBTYPE]
     */
    @Getter
    public enum SURVEY_QUESTION__MATRIX_SUBTYPE {
        SIMPLE("simple","每行一个选项"),
        MULTIPLE("multiple","每行有多个选项");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_QUESTION__MATRIX_SUBTYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_MOVE__INVOICE_PAYMENT_STATE]
     */
    @Getter
    public enum ACCOUNT_MOVE__INVOICE_PAYMENT_STATE {
        NOT_PAID("not_paid","未付款"),
        IN_PAYMENT("in_payment","付款中"),
        PAID("paid","已支付");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_MOVE__INVOICE_PAYMENT_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[EVENT_REGISTRATION__ACTIVITY_STATE]
     */
    @Getter
    public enum EVENT_REGISTRATION__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_REGISTRATION__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL_TEMPLATE__RULE_TYPE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL_TEMPLATE__RULE_TYPE {
        WRITEOFF_BUTTON("writeoff_button","在单击按钮上手动创建注销。"),
        WRITEOFF_SUGGESTION("writeoff_suggestion","建议核销"),
        INVOICE_MATCHING("invoice_matching","匹配发票／账单");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL_TEMPLATE__RULE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[FLEET_VEHICLE_LOG_CONTRACT__COST_FREQUENCY]
     */
    @Getter
    public enum FLEET_VEHICLE_LOG_CONTRACT__COST_FREQUENCY {
        NO("no","否"),
        DAILY("daily","每天"),
        WEEKLY("weekly","每周"),
        MONTHLY("monthly","每月"),
        YEARLY("yearly","每年");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE_LOG_CONTRACT__COST_FREQUENCY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[ACCOUNT_MOVE_LINE__DISPLAY_TYPE]
     */
    @Getter
    public enum ACCOUNT_MOVE_LINE__DISPLAY_TYPE {
        LINE_SECTION("line_section","章节"),
        LINE_NOTE("line_note","备注");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_MOVE_LINE__DISPLAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__PO_DOUBLE_VALIDATION]
     */
    @Getter
    public enum RES_COMPANY__PO_DOUBLE_VALIDATION {
        ONE_STEP("one_step","直接确认订单"),
        TWO_STEP("two_step","2级审批确认采购订单");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__PO_DOUBLE_VALIDATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[补字应用场合类型]
     */
    @Getter
    public enum CodeList30 {
        GG("GG","公共"),
        SH("SH","审核"),
        PG("PG","派工"),
        SABTYY("SABTYY","SABTYY"),
        RKDSH("RKDSH","入库单审核"),
        CKDSH("CKDSH","出库单审核"),
        BSDSH("BSDSH","报损单审核"),
        SAOA("SAOA","SAOA"),
        FW("FW","发文"),
        SW("SW","收文");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList30(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[HR_PLAN_ACTIVITY_TYPE__RESPONSIBLE]
     */
    @Getter
    public enum HR_PLAN_ACTIVITY_TYPE__RESPONSIBLE {
        COACH("coach","教练"),
        MANAGER("manager","管理员"),
        EMPLOYEE("employee","员工"),
        OTHER("other","其他");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_PLAN_ACTIVITY_TYPE__RESPONSIBLE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[页面跳转处理_页面类型]
     */
    @Getter
    public enum CodeList113 {
        PAGE("PAGE","内置页面"),
        URL("URL","网页路径"),
        SCRIPT("SCRIPT","脚本");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList113(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[CALENDAR_ATTENDEE__STATE]
     */
    @Getter
    public enum CALENDAR_ATTENDEE__STATE {
        NEEDSACTION("needsAction","待处理"),
        TENTATIVE("tentative","不确定"),
        DECLINED("declined","已拒绝"),
        ACCEPTED("accepted","已接收");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_ATTENDEE__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAINTENANCE_EQUIPMENT__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum MAINTENANCE_EQUIPMENT__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAINTENANCE_EQUIPMENT__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EXPENSE__STATE]
     */
    @Getter
    public enum HR_EXPENSE__STATE {
        DRAFT("draft","待提交"),
        REPORTED("reported","发送"),
        APPROVED("approved","已批准"),
        DONE("done","已支付"),
        REFUSED("refused","已拒绝");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EXPENSE__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_TAX_TEMPLATE__TAX_EXIGIBILITY]
     */
    @Getter
    public enum ACCOUNT_TAX_TEMPLATE__TAX_EXIGIBILITY {
        ON_INVOICE("on_invoice","开票基于"),
        ON_PAYMENT("on_payment","基于付款");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_TAX_TEMPLATE__TAX_EXIGIBILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_ACTIVITY__STATE]
     */
    @Getter
    public enum MAIL_ACTIVITY__STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_ACTIVITY__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_ACTIVITY_TYPE__CATEGORY]
     */
    @Getter
    public enum MAIL_ACTIVITY_TYPE__CATEGORY {
        DEFAULT("default","无"),
        UPLOAD_FILE("upload_file","上传单据"),
        MEETING("meeting","会议"),
        REMINDER("reminder","提醒");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_ACTIVITY_TYPE__CATEGORY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PRINT_JOURNAL__SORT_SELECTION]
     */
    @Getter
    public enum ACCOUNT_PRINT_JOURNAL__SORT_SELECTION {
        DATE("date","日期"),
        MOVE_NAME("move_name","日记账分录号码");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PRINT_JOURNAL__SORT_SELECTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__TAX_CALCULATION_ROUNDING_METHOD]
     */
    @Getter
    public enum RES_COMPANY__TAX_CALCULATION_ROUNDING_METHOD {
        ROUND_PER_LINE("round_per_line","每行舍入"),
        ROUND_GLOBALLY("round_globally","整体舍入");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__TAX_CALCULATION_ROUNDING_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[SURVEY_QUESTION__QUESTION_TYPE]
     */
    @Getter
    public enum SURVEY_QUESTION__QUESTION_TYPE {
        FREE_TEXT("free_text","多行文本框"),
        TEXTBOX("textbox","单行文本框"),
        NUMERICAL_BOX("numerical_box","数值"),
        DATE("date","日期"),
        DATETIME("datetime","日期时间"),
        SIMPLE_CHOICE("simple_choice","选择题：只允许选择一个答案"),
        MULTIPLE_CHOICE("multiple_choice","多选：允许选择多个答案"),
        MATRIX("matrix","矩阵");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_QUESTION__QUESTION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[LUNCH_SUPPLIER__DELIVERY]
     */
    @Getter
    public enum LUNCH_SUPPLIER__DELIVERY {
        DELIVERY("delivery","交货"),
        NO_DELIVERY("no_delivery","没有交货");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_SUPPLIER__DELIVERY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据操作步骤]
     */
    @Getter
    public enum CodeList11 {
        USERDECLARE("USERDECLARE","变量定义"),
        USERINIT("USERINIT","变量初始化"),
        INPUTCHECK("INPUTCHECK","数据检查"),
        BEFOREACTION("BEFOREACTION","操作之前"),
        EXECUTEACTION("EXECUTEACTION","执行操作"),
        AFTERACTION("AFTERACTION","操作之后");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList11(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[REPAIR_LINE__TYPE]
     */
    @Getter
    public enum REPAIR_LINE__TYPE {
        ADD("add","添加"),
        REMOVE("remove","移除");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        REPAIR_LINE__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[代码发布路径]
     */
    @Getter
    public enum CodeList116 {
        DEFAULT("DEFAULT","默认");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList116(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_CONTRACT__STATE]
     */
    @Getter
    public enum HR_CONTRACT__STATE {
        DRAFT("draft","新建"),
        OPEN("open","运行中"),
        CLOSE("close","过期"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_CONTRACT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_TEMPLATE__EXPENSE_POLICY]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__EXPENSE_POLICY {
        NO("no","否"),
        COST("cost","成本"),
        SALES_PRICE("sales_price","销售价格");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__EXPENSE_POLICY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[年周（1～52）]
     */
    @Getter
    public enum CodeList84 {
        ITEM_01("01","第1周"),
        ITEM_02("02","第2周"),
        ITEM_03("03","第3周"),
        ITEM_04("04","第4周"),
        ITEM_05("05","第5周"),
        ITEM_06("06","第6周"),
        ITEM_07("07","第7周"),
        ITEM_08("08","第8周"),
        ITEM_09("09","第9周"),
        ITEM_10("10","第10周"),
        ITEM_11("11","第11周"),
        ITEM_12("12","第12周"),
        ITEM_13("13","第13周"),
        ITEM_14("14","第14周"),
        ITEM_15("15","第15周"),
        ITEM_16("16","第16周"),
        ITEM_17("17","第17周"),
        ITEM_18("18","第18周"),
        ITEM_19("19","第19周"),
        ITEM_20("20","第20周"),
        ITEM_21("21","第21周"),
        ITEM_22("22","第22周"),
        ITEM_23("23","第23周"),
        ITEM_24("24","第24周"),
        ITEM_25("25","第25周"),
        ITEM_26("26","第26周"),
        ITEM_27("27","第27周"),
        ITEM_28("28","第28周"),
        ITEM_29("29","第29周"),
        ITEM_30("30","第30周"),
        ITEM_31("31","第31周"),
        ITEM_32("32","第32周"),
        ITEM_33("33","第33周"),
        ITEM_34("34","第34周"),
        ITEM_35("35","第35周"),
        ITEM_36("36","第36周"),
        ITEM_37("37","第37周"),
        ITEM_38("38","第38周"),
        ITEM_39("39","第39周"),
        ITEM_40("40","第40周"),
        ITEM_41("41","第41周"),
        ITEM_42("42","第42周"),
        ITEM_43("43","第43周"),
        ITEM_44("44","第44周"),
        ITEM_45("45","第45周"),
        ITEM_46("46","第46周"),
        ITEM_47("47","第47周"),
        ITEM_48("48","第48周"),
        ITEM_49("49","第49周"),
        ITEM_50("50","第50周"),
        ITEM_51("51","第51周"),
        ITEM_52("52","第52周");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList84(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据库触发器操作]
     */
    @Getter
    public enum CodeList65 {
        BEFORE("BEFORE","Before"),
        AFTER("AFTER","After"),
        INSTEADOF("INSTEADOF","Instead of");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList65(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_TAX_TEMPLATE__TYPE_TAX_USE]
     */
    @Getter
    public enum ACCOUNT_TAX_TEMPLATE__TYPE_TAX_USE {
        SALE("sale","销售"),
        PURCHASE("purchase","采购"),
        NONE("none","无");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_TAX_TEMPLATE__TYPE_TAX_USE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[LUNCH_SUPPLIER__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum LUNCH_SUPPLIER__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_SUPPLIER__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_APPLICANT__ACTIVITY_STATE]
     */
    @Getter
    public enum HR_APPLICANT__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_APPLICANT__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[平台内置流程状态]
     */
    @Getter
    public enum WFStates {
        ITEM_0("0","未启动"),
        ITEM_1("1","流程中"),
        ITEM_2("2","已完成"),
        ITEM_3("3","已取消"),
        ITEM_31("31","已取消(人工)"),
        ITEM_32("32","已取消(超时)"),
        ITEM_4("4","处理故障");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        WFStates(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PRODUCT_ATTRIBUTE__CREATE_VARIANT]
     */
    @Getter
    public enum PRODUCT_ATTRIBUTE__CREATE_VARIANT {
        ALWAYS("always","即时"),
        DYNAMIC("dynamic","动态"),
        NO_VARIANT("no_variant","从不");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_ATTRIBUTE__CREATE_VARIANT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[表单嵌入表格工具栏能力]
     */
    @Getter
    public enum CodeList77 {
        INSERT("INSERT","新建"),
        UPDATE("UPDATE","更新"),
        DELETE("DELETE","删除"),
        ROWEDIT("ROWEDIT","启用行编辑");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList77(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PAYMENT__PAYMENT_DIFFERENCE_HANDLING]
     */
    @Getter
    public enum ACCOUNT_PAYMENT__PAYMENT_DIFFERENCE_HANDLING {
        OPEN("open","保持打开"),
        RECONCILE("reconcile","标记这个全部付款发票");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT__PAYMENT_DIFFERENCE_HANDLING(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE__CERTIFICATE]
     */
    @Getter
    public enum HR_EMPLOYEE__CERTIFICATE {
        BACHELOR("bachelor","学士"),
        MASTER("master","主版本"),
        OTHER("other","其他");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__CERTIFICATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_INVOICE_REPORT__STATE]
     */
    @Getter
    public enum ACCOUNT_INVOICE_REPORT__STATE {
        DRAFT("draft","草稿"),
        POSTED("posted","打开"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_INVOICE_REPORT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_TAX_REPARTITION_LINE_TEMPLATE__REPARTITION_TYPE]
     */
    @Getter
    public enum ACCOUNT_TAX_REPARTITION_LINE_TEMPLATE__REPARTITION_TYPE {
        BASE("base","基础"),
        TAX("tax","税收");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_TAX_REPARTITION_LINE_TEMPLATE__REPARTITION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[CALENDAR_EVENT__RRULE_TYPE]
     */
    @Getter
    public enum CALENDAR_EVENT__RRULE_TYPE {
        DAILY("daily","天"),
        WEEKLY("weekly","周"),
        MONTHLY("monthly","月"),
        YEARLY("yearly","年");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__RRULE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_ORDER__INVOICE_STATUS]
     */
    @Getter
    public enum SALE_ORDER__INVOICE_STATUS {
        UPSELLING("upselling","超卖商机"),
        INVOICED("invoiced","已开具全额发票"),
        TO_INVOICE("to invoice","待开票"),
        NO("no","没有要开票的");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER__INVOICE_STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[FLEET_VEHICLE__ODOMETER_UNIT]
     */
    @Getter
    public enum FLEET_VEHICLE__ODOMETER_UNIT {
        KILOMETERS("kilometers","公里"),
        MILES("miles","里程");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE__ODOMETER_UNIT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_USERS__STATE]
     */
    @Getter
    public enum RES_USERS__STATE {
        NEW("new","从未连接"),
        ACTIVE("active","已确认");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_USERS__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[页面处理逻辑类型]
     */
    @Getter
    public enum CodeList70 {
        AFTERINITPAGEPARAM("AFTERINITPAGEPARAM","页面变量初始化之后");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList70(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_PRICELIST_ITEM__BASE]
     */
    @Getter
    public enum PRODUCT_PRICELIST_ITEM__BASE {
        LIST_PRICE("list_price","销售价格"),
        STANDARD_PRICE("standard_price","成本"),
        PRICELIST("pricelist","其他价格表");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_PRICELIST_ITEM__BASE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[EVENT_EVENT__SEATS_AVAILABILITY]
     */
    @Getter
    public enum EVENT_EVENT__SEATS_AVAILABILITY {
        LIMITED("limited","限制的"),
        UNLIMITED("unlimited","无限制的");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_EVENT__SEATS_AVAILABILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_LABEL]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_LABEL {
        CONTAINS("contains","包含"),
        NOT_CONTAINS("not_contains","不包含"),
        MATCH_REGEX("match_regex","正则匹配");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_LABEL(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_TAX__TYPE_TAX_USE]
     */
    @Getter
    public enum ACCOUNT_TAX__TYPE_TAX_USE {
        SALE("sale","销售"),
        PURCHASE("purchase","采购"),
        NONE("none","无");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_TAX__TYPE_TAX_USE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[DIGEST_DIGEST__STATE]
     */
    @Getter
    public enum DIGEST_DIGEST__STATE {
        ACTIVATED("activated","已激活"),
        DEACTIVATED("deactivated","禁用");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        DIGEST_DIGEST__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_ACTIVITY_TYPE__DELAY_FROM]
     */
    @Getter
    public enum MAIL_ACTIVITY_TYPE__DELAY_FROM {
        CURRENT_DATE("current_date","在验证日期后"),
        PREVIOUS_ACTIVITY("previous_activity","上次活动结束后");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_ACTIVITY_TYPE__DELAY_FROM(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[STOCK_MOVE__STATE]
     */
    @Getter
    public enum STOCK_MOVE__STATE {
        DRAFT("draft","新建"),
        CANCEL("cancel","已取消"),
        WAITING("waiting","等待其它移动"),
        CONFIRMED("confirmed","等待可用量"),
        PARTIALLY_AVAILABLE("partially_available","部分可用"),
        ASSIGNED("assigned","可用"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_MOVE__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_APPLICANT__PRIORITY]
     */
    @Getter
    public enum HR_APPLICANT__PRIORITY {
        ITEM_0("0","一般"),
        ITEM_1("1","好"),
        ITEM_2("2","非常好"),
        ITEM_3("3","杰出");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_APPLICANT__PRIORITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[日期类型]
     */
    @Getter
    public enum CodeList48 {
        ITEM_1("1","每周"),
        ITEM_2("2","每月");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList48(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_COMPANY__WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE]
     */
    @Getter
    public enum RES_COMPANY__WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_SURVEY__STATE]
     */
    @Getter
    public enum SURVEY_SURVEY__STATE {
        DRAFT("draft","草稿"),
        OPEN("open","进行中"),
        CLOSED("closed","已关闭");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_SURVEY__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[服务运行状态]
     */
    @Getter
    public enum CodeList38 {
        START("START","启动"),
        STOP("STOP","停止");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList38(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD]
     */
    @Getter
    public enum PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD {
        PAYPAL("paypal","PayPal"),
        STRIPE("stripe","信用卡（通过 Stripe)"),
        OTHER("other","其他支付收购方"),
        MANUAL("manual","自定义付款说明");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_QUESTION__COLUMN_NB]
     */
    @Getter
    public enum SURVEY_QUESTION__COLUMN_NB {
        ITEM_12("12","1"),
        ITEM_6("6","2"),
        ITEM_4("4","3"),
        ITEM_3("3","4"),
        ITEM_2("2","6");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_QUESTION__COLUMN_NB(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE__DEPARTURE_REASON]
     */
    @Getter
    public enum HR_EMPLOYEE__DEPARTURE_REASON {
        FIRED("fired","被解雇"),
        RESIGNED("resigned","已辞职"),
        RETIRED("retired","已退休");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__DEPARTURE_REASON(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[LUNCH_ALERT__MODE]
     */
    @Getter
    public enum LUNCH_ALERT__MODE {
        ALERT("alert","在应用程序中提醒"),
        CHAT("chat","聊天通知");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_ALERT__MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_WORKORDER__ACTIVITY_STATE]
     */
    @Getter
    public enum MRP_WORKORDER__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_WORKORDER__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_EMPLOYEE_PUBLIC__ATTENDANCE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE_PUBLIC__ATTENDANCE_STATE {
        CHECKED_OUT("checked_out","签出"),
        CHECKED_IN("checked_in","签到");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE_PUBLIC__ATTENDANCE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[EVENT_MAIL__INTERVAL_UNIT]
     */
    @Getter
    public enum EVENT_MAIL__INTERVAL_UNIT {
        NOW("now","马上"),
        HOURS("hours","小时"),
        DAYS("days","天"),
        WEEKS("weeks","周"),
        MONTHS("months","月");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_MAIL__INTERVAL_UNIT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_TEMPLATE__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[用户对象类型]
     */
    @Getter
    public enum CodeList5 {
        USER("USER","用户"),
        USERGROUP("USERGROUP","用户组");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList5(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_EMPLOYEE__CURRENT_LEAVE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE__CURRENT_LEAVE_STATE {
        DRAFT("draft","新建"),
        CONFIRM("confirm","等待审批"),
        REFUSE("refuse","已拒绝"),
        VALIDATE1("validate1","等待第二次审批"),
        VALIDATE("validate","已批准"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__CURRENT_LEAVE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_REQUISITION_TYPE__LINE_COPY]
     */
    @Getter
    public enum PURCHASE_REQUISITION_TYPE__LINE_COPY {
        COPY("copy","使用申请单的明细行"),
        NONE("none","不自动创建采购订单明细行");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_REQUISITION_TYPE__LINE_COPY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[DA日志操作类型]
     */
    @Getter
    public enum CodeList24 {
        CREATE("CREATE","新建"),
        UPDATE("UPDATE","更新"),
        DELETE("DELETE","删除");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList24(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE_BASE__HR_PRESENCE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE_BASE__HR_PRESENCE_STATE {
        PRESENT("present","出勤"),
        ABSENT("absent","缺勤"),
        TO_DEFINE("to_define","界定");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE_BASE__HR_PRESENCE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__PO_LOCK]
     */
    @Getter
    public enum RES_COMPANY__PO_LOCK {
        EDIT("edit","允许修改采购订单"),
        LOCK("lock","禁止修改已确认订单");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__PO_LOCK(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_APPLICANT__KANBAN_STATE]
     */
    @Getter
    public enum HR_APPLICANT__KANBAN_STATE {
        NORMAL("normal","灰色"),
        DONE("done","绿色"),
        BLOCKED("blocked","红色");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_APPLICANT__KANBAN_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[脚本功能]
     */
    @Getter
    public enum CodeList114 {
        ITEM_1("1","基础"),
        ITEM_128("128","树视图常规"),
        ITEM_2("2","树视图高级"),
        ITEM_4("4","TAB视图"),
        ITEM_8("8","动态面板"),
        ITEM_16("16","搜索面板"),
        ITEM_32("32","表格视图常规"),
        ITEM_64("64","表格视图高级"),
        ITEM_256("256","数据视图");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList114(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[实体快捷应用范围]
     */
    @Getter
    public enum CodeList41 {
        ITEM_1("1","拾取链接");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList41(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[是否（TRUE，FALSE）]
     */
    @Getter
    public enum TrueFalse {
        TRUE("TRUE","是"),
        FALSE("FALSE","否");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        TrueFalse(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[服务启动模式]
     */
    @Getter
    public enum CodeList37 {
        AUTO("AUTO","自动"),
        MANUAL("MANUAL","手动");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList37(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_REQUISITION_TYPE__QUANTITY_COPY]
     */
    @Getter
    public enum PURCHASE_REQUISITION_TYPE__QUANTITY_COPY {
        COPY("copy","使用申请单的数量"),
        NONE("none","手动设置数量");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_REQUISITION_TYPE__QUANTITY_COPY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_PACKAGE_LEVEL__STATE]
     */
    @Getter
    public enum STOCK_PACKAGE_LEVEL__STATE {
        DRAFT("draft","草稿"),
        CONFIRMED("confirmed","已确认"),
        ASSIGNED("assigned","已保留"),
        NEW("new","新建"),
        DONE("done","完成"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PACKAGE_LEVEL__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[FLEET_SERVICE_TYPE__CATEGORY]
     */
    @Getter
    public enum FLEET_SERVICE_TYPE__CATEGORY {
        CONTRACT("contract","合同"),
        SERVICE("service","服务");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_SERVICE_TYPE__CATEGORY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[BASE_LANGUAGE_EXPORT__STATE]
     */
    @Getter
    public enum BASE_LANGUAGE_EXPORT__STATE {
        CHOOSE("choose","选取"),
        GET("get","获取");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        BASE_LANGUAGE_EXPORT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CRM_LEAD2OPPORTUNITY_PARTNER_MASS__ACTION]
     */
    @Getter
    public enum CRM_LEAD2OPPORTUNITY_PARTNER_MASS__ACTION {
        CREATE("create","创建客户"),
        EXIST("exist","链接到现有客户"),
        EACH_EXIST_OR_CREATE("each_exist_or_create","使用已有的业务伙伴或新建"),
        NOTHING("nothing","不要链接到某个客户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD2OPPORTUNITY_PARTNER_MASS__ACTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_COMPANY__ACCOUNT_INVOICE_ONBOARDING_STATE]
     */
    @Getter
    public enum RES_COMPANY__ACCOUNT_INVOICE_ONBOARDING_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成"),
        CLOSED("closed","已关闭");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__ACCOUNT_INVOICE_ONBOARDING_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[GAMIFICATION_GOAL_DEFINITION__COMPUTATION_MODE]
     */
    @Getter
    public enum GAMIFICATION_GOAL_DEFINITION__COMPUTATION_MODE {
        MANUALLY("manually","手动记录"),
        COUNT("count","自动：记录数"),
        SUM("sum","自动：字段加总"),
        PYTHON("python","自动：执行特定Python代码");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_GOAL_DEFINITION__COMPUTATION_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[LUNCH_SUPPLIER__SEND_BY]
     */
    @Getter
    public enum LUNCH_SUPPLIER__SEND_BY {
        PHONE("phone","电话"),
        MAIL("mail","EMail");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_SUPPLIER__SEND_BY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[预定义属性类型]
     */
    @Getter
    public enum CodeList34 {
        LOGICVALID("LOGICVALID","逻辑有效标识"),
        CREATEMAN("CREATEMAN","建立人"),
        CREATEDATE("CREATEDATE","建立时间"),
        UPDATEMAN("UPDATEMAN","更新人"),
        UPDATEDATE("UPDATEDATE","更新时间"),
        ORGUNITID("ORGUNITID","组织单元标识"),
        ORGUNITNAME("ORGUNITNAME","组织单元名称");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList34(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PRODUCT_TEMPLATE__INVENTORY_AVAILABILITY]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__INVENTORY_AVAILABILITY {
        NEVER("never","不考虑库存的销售"),
        ALWAYS("always","在网站上显示库存并且如果没有足够的库存，则阻止销售"),
        THRESHOLD("threshold","显示的库存低于阈值，如果没有足够的库存，则阻止销售"),
        CUSTOM("custom","显示具体产品的通知");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__INVENTORY_AVAILABILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_MOVE__PROCURE_METHOD]
     */
    @Getter
    public enum STOCK_MOVE__PROCURE_METHOD {
        MAKE_TO_STOCK("make_to_stock","默认：从库存获取"),
        MAKE_TO_ORDER("make_to_order","高级：应用补货规则");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_MOVE__PROCURE_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_ORDER_LINE__DISPLAY_TYPE]
     */
    @Getter
    public enum SALE_ORDER_LINE__DISPLAY_TYPE {
        LINE_SECTION("line_section","章节"),
        LINE_NOTE("line_note","备注");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER_LINE__DISPLAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_LEAVE_REPORT__HOLIDAY_TYPE]
     */
    @Getter
    public enum HR_LEAVE_REPORT__HOLIDAY_TYPE {
        EMPLOYEE("employee","按员工"),
        CATEGORY("category","按员工标签");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_REPORT__HOLIDAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[SALE_ORDER__PICKING_POLICY]
     */
    @Getter
    public enum SALE_ORDER__PICKING_POLICY {
        DIRECT("direct","尽快"),
        ONE("one","当所有产品就绪时");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER__PICKING_POLICY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__PRODUCT_PRICELIST_SETTING]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__PRODUCT_PRICELIST_SETTING {
        BASIC("basic","Multiple prices per product"),
        ADVANCED("advanced","Advanced price rules (discounts, formulas)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__PRODUCT_PRICELIST_SETTING(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PROJECT_TASK__KANBAN_STATE]
     */
    @Getter
    public enum PROJECT_TASK__KANBAN_STATE {
        NORMAL("normal","灰色"),
        DONE("done","绿色"),
        BLOCKED("blocked","红色");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PROJECT_TASK__KANBAN_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[列编辑器样式]
     */
    @Getter
    public enum CodeList28 {
        DROPDOWNLIST("DROPDOWNLIST","下拉列表框"),
        PICKER("PICKER","数据选择框");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList28(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MAIL_ACTIVITY_MIXIN__ACTIVITY_STATE]
     */
    @Getter
    public enum MAIL_ACTIVITY_MIXIN__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_ACTIVITY_MIXIN__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[MRP_UNBUILD__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum MRP_UNBUILD__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_UNBUILD__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE__ACTIVITY_STATE]
     */
    @Getter
    public enum HR_LEAVE__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_PARTNER__PURCHASE_WARN]
     */
    @Getter
    public enum RES_PARTNER__PURCHASE_WARN {
        NO_SUB_MESSAGE("no-message","没消息"),
        WARNING("warning","警告"),
        BLOCK("block","受阻消息");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__PURCHASE_WARN(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据通知_时间条件]
     */
    @Getter
    public enum CodeList62 {
        BEFORE("BEFORE","之前"),
        AFTER("AFTER","之后");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList62(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MAINTENANCE_REQUEST__ACTIVITY_STATE]
     */
    @Getter
    public enum MAINTENANCE_REQUEST__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAINTENANCE_REQUEST__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[网页部件类型]
     */
    @Getter
    public enum CodeList6 {
        CHART("CHART","图形部件"),
        LIST("LIST","列表"),
        CUSTOMWP("CUSTOMWP","自定义");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList6(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PROJECT_PROJECT__RATING_STATUS]
     */
    @Getter
    public enum PROJECT_PROJECT__RATING_STATUS {
        STAGE("stage","在变更阶段时点评"),
        PERIODIC("periodic","定期点评"),
        NO("no","没点评");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PROJECT_PROJECT__RATING_STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE_PUBLIC__CURRENT_LEAVE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE_PUBLIC__CURRENT_LEAVE_STATE {
        DRAFT("draft","新建"),
        CONFIRM("confirm","等待审批"),
        REFUSE("refuse","已拒绝"),
        VALIDATE1("validate1","等待第二次审批"),
        VALIDATE("validate","已批准"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE_PUBLIC__CURRENT_LEAVE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[星期（1～7）]
     */
    @Getter
    public enum CodeList46 {
        ITEM_2("2","星期一"),
        ITEM_3("3","星期二"),
        ITEM_4("4","星期三"),
        ITEM_5("5","星期四"),
        ITEM_6("6","星期五"),
        ITEM_7("7","星期六"),
        ITEM_1("1","星期日");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList46(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[STOCK_MOVE__PRIORITY]
     */
    @Getter
    public enum STOCK_MOVE__PRIORITY {
        ITEM_0("0","不紧急"),
        ITEM_1("1","一般"),
        ITEM_2("2","紧急"),
        ITEM_3("3","非常紧急");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_MOVE__PRIORITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[BUS_PRESENCE__STATUS]
     */
    @Getter
    public enum BUS_PRESENCE__STATUS {
        ONLINE("online","在线"),
        AWAY("away","离开"),
        OFFLINE("offline","离线");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        BUS_PRESENCE__STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[DA日志对象类型]
     */
    @Getter
    public enum CodeList23 {
        DATAENTITY("DATAENTITY","实体"),
        DEFIELD("DEFIELD","实体属性"),
        DER1N("DER1N","实体关系");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList23(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_COMPANY__SALE_QUOTATION_ONBOARDING_STATE]
     */
    @Getter
    public enum RES_COMPANY__SALE_QUOTATION_ONBOARDING_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成"),
        CLOSED("closed","已关闭");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__SALE_QUOTATION_ONBOARDING_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[HR_EMPLOYEE_PUBLIC__HR_PRESENCE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE_PUBLIC__HR_PRESENCE_STATE {
        PRESENT("present","出勤"),
        ABSENT("absent","缺勤"),
        TO_DEFINE("to_define","界定");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE_PUBLIC__HR_PRESENCE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_EVENT__BYDAY]
     */
    @Getter
    public enum CALENDAR_EVENT__BYDAY {
        ITEM_1("1","第一个"),
        ITEM_2("2","秒"),
        ITEM_3("3","第三个"),
        ITEM_4("4","第四"),
        ITEM_5("5","第五个"),
        _SUB_1("-1","最后");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__BYDAY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[月周（1～5）]
     */
    @Getter
    public enum CodeList86 {
        ITEM_1("1","1周"),
        ITEM_2("2","2周"),
        ITEM_3("3","3周"),
        ITEM_4("4","4周"),
        ITEM_5("5","5周");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList86(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_ORDER__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum PURCHASE_ORDER__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_ORDER__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_EVENT__MONTH_BY]
     */
    @Getter
    public enum CALENDAR_EVENT__MONTH_BY {
        DATE("date","日期"),
        DAY("day","日期");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__MONTH_BY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SMS_SMS__ERROR_CODE]
     */
    @Getter
    public enum SMS_SMS__ERROR_CODE {
        SMS_NUMBER_MISSING("sms_number_missing","遗失号码"),
        SMS_NUMBER_FORMAT("sms_number_format","错误的数字格式"),
        SMS_CREDIT("sms_credit","信用不足"),
        SMS_SERVER("sms_server","服务器错误"),
        SMS_BLACKLIST("sms_blacklist","列入黑名单"),
        SMS_DUPLICATE("sms_duplicate","复制");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SMS_SMS__ERROR_CODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL__SECOND_AMOUNT_TYPE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL__SECOND_AMOUNT_TYPE {
        FIXED("fixed","固定"),
        PERCENTAGE("percentage","余额百分比"),
        REGEX("regex","来自标签");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL__SECOND_AMOUNT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_JOB__STATE]
     */
    @Getter
    public enum HR_JOB__STATE {
        RECRUIT("recruit","正在招聘"),
        OPEN("open","停止招聘");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_JOB__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_COMPANY__FISCALYEAR_LAST_MONTH]
     */
    @Getter
    public enum RES_COMPANY__FISCALYEAR_LAST_MONTH {
        ITEM_1("1","一月"),
        ITEM_2("2","二月"),
        ITEM_3("3","三月"),
        ITEM_4("4","四月"),
        ITEM_5("5","五月"),
        ITEM_6("6","六月"),
        ITEM_7("7","七月"),
        ITEM_8("8","八月"),
        ITEM_9("9","九月"),
        ITEM_10("10","十月"),
        ITEM_11("11","十一月"),
        ITEM_12("12","十二月");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__FISCALYEAR_LAST_MONTH(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据库操作]
     */
    @Getter
    public enum CodeList89 {
        INSERT("INSERT","插入"),
        UPDATE("UPDATE","更新"),
        DELETE("DELETE","删除");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList89(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_ALARM__INTERVAL]
     */
    @Getter
    public enum CALENDAR_ALARM__INTERVAL {
        MINUTES("minutes","分钟"),
        HOURS("hours","小时"),
        DAYS("days","天");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_ALARM__INTERVAL(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体属性更新模式]
     */
    @Getter
    public enum CodeList2 {
        VERSION("VERSION","版本模式");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList2(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_EMPLOYEE__MARITAL]
     */
    @Getter
    public enum HR_EMPLOYEE__MARITAL {
        SINGLE("single","单身"),
        MARRIED("married","已婚"),
        COHABITANT("cohabitant","合法同居者"),
        WIDOWER("widower","丧偶"),
        DIVORCED("divorced","离异");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__MARITAL(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_JOURNAL__INVOICE_REFERENCE_MODEL]
     */
    @Getter
    public enum ACCOUNT_JOURNAL__INVOICE_REFERENCE_MODEL {
        ODOO("odoo","Odoo"),
        EURO("euro","欧洲");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_JOURNAL__INVOICE_REFERENCE_MODEL(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[EVENT_EVENT__STATE]
     */
    @Getter
    public enum EVENT_EVENT__STATE {
        DRAFT("draft","未确认"),
        CANCEL("cancel","已取消"),
        CONFIRM("confirm","已确认"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_EVENT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RESOURCE_CALENDAR_ATTENDANCE__DISPLAY_TYPE]
     */
    @Getter
    public enum RESOURCE_CALENDAR_ATTENDANCE__DISPLAY_TYPE {
        LINE_SECTION("line_section","章节");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RESOURCE_CALENDAR_ATTENDANCE__DISPLAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[属性引用关系类型]
     */
    @Getter
    public enum CodeList90 {
        DER1N("DER1N","1:N关系"),
        DERCUSTOM("DERCUSTOM","自定义关系");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList90(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_PICKING__ACTIVITY_STATE]
     */
    @Getter
    public enum STOCK_PICKING__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PICKING__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_MOVE__STATE]
     */
    @Getter
    public enum ACCOUNT_MOVE__STATE {
        DRAFT("draft","草稿"),
        POSTED("posted","已过账"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_MOVE__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_USER_INPUT_LINE__ANSWER_TYPE]
     */
    @Getter
    public enum SURVEY_USER_INPUT_LINE__ANSWER_TYPE {
        TEXT("text","文本"),
        NUMBER("number","号码"),
        DATE("date","日期"),
        DATETIME("datetime","日期时间"),
        FREE_TEXT("free_text","自由文本"),
        SUGGESTION("suggestion","建议");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_USER_INPUT_LINE__ANSWER_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体归属]
     */
    @Getter
    public enum CodeList19 {
        SRFDA("SRFDA","系统"),
        APPLICATION("APPLICATION","应用"),
        USER("USER","用户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList19(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_COMPANY__ACCOUNT_SETUP_BANK_DATA_STATE]
     */
    @Getter
    public enum RES_COMPANY__ACCOUNT_SETUP_BANK_DATA_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__ACCOUNT_SETUP_BANK_DATA_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[DB2触发器代码模式]
     */
    @Getter
    public enum CodeList67 {
        FOR_EACH_ROW("For Each Row","For Each Row"),
        FOR_EACH_STATEMENT("For Each Statement","For Each Statement");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList67(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CRM_LEAD__ACTIVITY_STATE]
     */
    @Getter
    public enum CRM_LEAD__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_NOTIFICATION__FAILURE_TYPE]
     */
    @Getter
    public enum MAIL_NOTIFICATION__FAILURE_TYPE {
        SMTP("SMTP","连接失败(发件服务器问题)"),
        RECIPIENT("RECIPIENT","无效的EMail地址"),
        BOUNCE("BOUNCE","您的消息被拒绝。"),
        UNKNOWN("UNKNOWN","未知错误"),
        SMS_NUMBER_MISSING("sms_number_missing","遗失号码"),
        SMS_NUMBER_FORMAT("sms_number_format","错误的数字格式"),
        SMS_CREDIT("sms_credit","信用不足"),
        SMS_SERVER("sms_server","服务器错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_NOTIFICATION__FAILURE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_CATEGORY__PROPERTY_COST_METHOD]
     */
    @Getter
    public enum PRODUCT_CATEGORY__PROPERTY_COST_METHOD {
        STANDARD("standard","标准价格"),
        FIFO("fifo","先进先出(FIFO)"),
        AVERAGE("average","平均成本(AVCO)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_CATEGORY__PROPERTY_COST_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MAIL_NOTIFICATION__NOTIFICATION_TYPE]
     */
    @Getter
    public enum MAIL_NOTIFICATION__NOTIFICATION_TYPE {
        INBOX("inbox","收件箱"),
        EMAIL("email","EMail"),
        SMS("sms","短信");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_NOTIFICATION__NOTIFICATION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[功能类型]
     */
    @Getter
    public enum CodeList4 {
        DEDATAGRID("DEDATAGRID","默认实体表格视图"),
        PAGELINK("PAGELINK","页面链接"),
        JSCODE("JSCODE","纯JS代码"),
        DEGRIDVIEW("DEGRIDVIEW","指定实体表格视图"),
        PAGE("PAGE","内置页面");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList4(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PAYMENT_ACQUIRER__PROVIDER]
     */
    @Getter
    public enum PAYMENT_ACQUIRER__PROVIDER {
        MANUAL("manual","自定义付款表格"),
        TRANSFER("transfer","手动付款");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_ACQUIRER__PROVIDER(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_UNBUILD__STATE]
     */
    @Getter
    public enum MRP_UNBUILD__STATE {
        DRAFT("draft","草稿"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_UNBUILD__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_ROUTING_WORKCENTER__BATCH]
     */
    @Getter
    public enum MRP_ROUTING_WORKCENTER__BATCH {
        NO("no","所有产品处理完"),
        YES("yes","Once some products are processed");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_ROUTING_WORKCENTER__BATCH(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_ORDER__ACTIVITY_STATE]
     */
    @Getter
    public enum PURCHASE_ORDER__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_ORDER__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_NOTE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_NOTE {
        CONTAINS("contains","包含"),
        NOT_CONTAINS("not_contains","不包含"),
        MATCH_REGEX("match_regex","正则匹配");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_NOTE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_ROUTING_WORKCENTER__WORKSHEET_TYPE]
     */
    @Getter
    public enum MRP_ROUTING_WORKCENTER__WORKSHEET_TYPE {
        PDF("pdf","PDF"),
        GOOGLE_SLIDE("google_slide","Google Slide");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_ROUTING_WORKCENTER__WORKSHEET_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[STOCK_WAREHOUSE__DELIVERY_STEPS]
     */
    @Getter
    public enum STOCK_WAREHOUSE__DELIVERY_STEPS {
        SHIP_ONLY("ship_only","直接出货（1步）"),
        PICK_SHIP("pick_ship","送到待出货区，再送货（2步发货)"),
        PICK_PACK_SHIP("pick_pack_ship","包装产品， 发送到待出货区，再送货（3步发货)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_WAREHOUSE__DELIVERY_STEPS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_ORDER_LINE__INVOICE_STATUS]
     */
    @Getter
    public enum SALE_ORDER_LINE__INVOICE_STATUS {
        UPSELLING("upselling","超卖商机"),
        INVOICED("invoiced","已开具全额发票"),
        TO_INVOICE("to invoice","待开票"),
        NO("no","没有要开票的");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER_LINE__INVOICE_STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_TAX_REPARTITION_LINE__REPARTITION_TYPE]
     */
    @Getter
    public enum ACCOUNT_TAX_REPARTITION_LINE__REPARTITION_TYPE {
        BASE("base","基础"),
        TAX("tax","税收");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_TAX_REPARTITION_LINE__REPARTITION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[输入辅助_消息模板宏]
     */
    @Getter
    public enum CodeList102 {
        CARETTEMPLGROUP_SRFMSG_MSGTEMPLATE("CARETTEMPLGROUP_SRFMSG_MSGTEMPLATE","消息模板"),
        CARETTEMPLGROUP_SRFDA_FILLENTITYPARAM("CARETTEMPLGROUP_SRFDA_FILLENTITYPARAM","系统属性");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList102(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MAIL_CHANNEL__CHANNEL_TYPE]
     */
    @Getter
    public enum MAIL_CHANNEL__CHANNEL_TYPE {
        CHAT("chat","聊天讨论"),
        CHANNEL("channel","频道"),
        LIVECHAT("livechat","在线对话");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_CHANNEL__CHANNEL_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SNAILMAIL_LETTER__STATE]
     */
    @Getter
    public enum SNAILMAIL_LETTER__STATE {
        PENDING("pending","排队"),
        SENT("sent","已发送"),
        ERROR("error","错误"),
        CANCELED("canceled","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SNAILMAIL_LETTER__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_USERS__ODOOBOT_STATE]
     */
    @Getter
    public enum RES_USERS__ODOOBOT_STATE {
        NOT_INITIALIZED("not_initialized","未初始化"),
        ONBOARDING_EMOJI("onboarding_emoji","Onboarding emoji"),
        ONBOARDING_ATTACHEMENT("onboarding_attachement","Onboarding 附件"),
        ONBOARDING_COMMAND("onboarding_command","Onboarding 命令"),
        ONBOARDING_PING("onboarding_ping","Onboarding ping"),
        IDLE("idle","闲置"),
        DISABLED("disabled","禁用"),
        ONBOARDING_CANNED("onboarding_canned","Onboarding 录制");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_USERS__ODOOBOT_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PRODUCT_TEMPLATE__SALE_LINE_WARN]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__SALE_LINE_WARN {
        NO_SUB_MESSAGE("no-message","没消息"),
        WARNING("warning","警告"),
        BLOCK("block","受阻消息");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__SALE_LINE_WARN(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[MRP_PRODUCTION__RESERVATION_STATE]
     */
    @Getter
    public enum MRP_PRODUCTION__RESERVATION_STATE {
        CONFIRMED("confirmed","正在等待"),
        ASSIGNED("assigned","就绪"),
        WAITING("waiting","等待其它作业");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_PRODUCTION__RESERVATION_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[代码表或模式]
     */
    @Getter
    public enum CodeList20 {
        NUMBERORMODE("NUMBERORMODE","数字或处理"),
        STRINGORMODE("STRINGORMODE","文本或模式");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList20(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[SURVEY_SURVEY__ACCESS_MODE]
     */
    @Getter
    public enum SURVEY_SURVEY__ACCESS_MODE {
        PUBLIC("public","任何有链接的人"),
        TOKEN("token","仅邀请人员");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_SURVEY__ACCESS_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[是否（蓝、红）]
     */
    @Getter
    public enum CodeList50 {
        ITEM_1("1","是"),
        ITEM_0("0","否");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList50(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[STOCK_WAREHOUSE__RECEPTION_STEPS]
     */
    @Getter
    public enum STOCK_WAREHOUSE__RECEPTION_STEPS {
        ONE_STEP("one_step","直接接收产品(1步收货）"),
        TWO_STEPS("two_steps","接到产品到收料区， 再入库（2步收货）"),
        THREE_STEPS("three_steps","接收产品到收料区， 检验， 然后入库（3步收货）");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_WAREHOUSE__RECEPTION_STEPS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[表格列构建器]
     */
    @Getter
    public enum CodeList9 {
        NUMBER("NUMBER","数值"),
        CODELIST("CODELIST","代码表");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList9(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_CONTRACT__KANBAN_STATE]
     */
    @Getter
    public enum HR_CONTRACT__KANBAN_STATE {
        NORMAL("normal","灰色"),
        DONE("done","绿色"),
        BLOCKED("blocked","红色");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_CONTRACT__KANBAN_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[ACCOUNT_INVOICE_REPORT__TYPE]
     */
    @Getter
    public enum ACCOUNT_INVOICE_REPORT__TYPE {
        OUT_INVOICE("out_invoice","客户发票"),
        IN_INVOICE("in_invoice","供应商账单"),
        OUT_REFUND("out_refund","客户贷项发票"),
        IN_REFUND("in_refund","供应商信用票");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_INVOICE_REPORT__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[MAINTENANCE_REQUEST__PRIORITY]
     */
    @Getter
    public enum MAINTENANCE_REQUEST__PRIORITY {
        ITEM_0("0","非常低"),
        ITEM_1("1","低"),
        ITEM_2("2","一般"),
        ITEM_3("3","高");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAINTENANCE_REQUEST__PRIORITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PAYMENT__ACTIVITY_STATE]
     */
    @Getter
    public enum ACCOUNT_PAYMENT__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[IM_LIVECHAT_CHANNEL_RULE__ACTION]
     */
    @Getter
    public enum IM_LIVECHAT_CHANNEL_RULE__ACTION {
        DISPLAY_BUTTON("display_button","显示按钮"),
        AUTO_POPUP("auto_popup","自动弹出"),
        HIDE_BUTTON("hide_button","隐藏按钮");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        IM_LIVECHAT_CHANNEL_RULE__ACTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[消息类型]
     */
    @Getter
    public enum CodeList29 {
        ITEM_1("1","系统消息"),
        ITEM_2("2","电子邮件"),
        ITEM_4("4","手机短信"),
        ITEM_8("8","MSN消息"),
        ITEM_16("16","检务通消息"),
        ITEM_32("32","微信");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList29(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[工作日类型]
     */
    @Getter
    public enum CodeList45 {
        ITEM_1("1","工作日"),
        ITEM_2("2","非工作日"),
        ITEM_3("3","自定义工作日");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList45(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[扩展表格单元格水平对齐方式]
     */
    @Getter
    public enum CodeList74 {
        LEFT("LEFT","左对齐"),
        CENTER("CENTER","居中"),
        RIGHT("RIGHT","右对齐");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList74(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MRP_PRODUCTION__STATE]
     */
    @Getter
    public enum MRP_PRODUCTION__STATE {
        DRAFT("draft","草稿"),
        CONFIRMED("confirmed","已确认"),
        PLANNED("planned","已计划"),
        PROGRESS("progress","进行中"),
        TO_CLOSE("to_close","待关闭"),
        DONE("done","完成"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_PRODUCTION__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PAYMENT__STATE]
     */
    @Getter
    public enum ACCOUNT_PAYMENT__STATE {
        DRAFT("draft","草稿"),
        POSTED("posted","已验证"),
        SENT("sent","已发送"),
        RECONCILED("reconciled","已核销"),
        CANCELLED("cancelled","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CRM_LEAD__EMAIL_STATE]
     */
    @Getter
    public enum CRM_LEAD__EMAIL_STATE {
        CORRECT("correct","正确的"),
        INCORRECT("incorrect","不正确");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD__EMAIL_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_3]
     */
    @Getter
    public enum LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_3 {
        NONE_OR_MORE("0_more","没有或更多"),
        ONE_OR_MORE("1_more","一个或多个"),
        ONE("1","只有一个");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_3(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[CRM_LEAD__TYPE]
     */
    @Getter
    public enum CRM_LEAD__TYPE {
        LEAD("lead","线索"),
        OPPORTUNITY("opportunity","商机");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[文件编码]
     */
    @Getter
    public enum CodeList39 {
        ANSI("ANSI","ANSI"),
        UTF_SUB_8("UTF-8","UTF-8");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList39(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_USERS__NOTIFICATION_TYPE]
     */
    @Getter
    public enum RES_USERS__NOTIFICATION_TYPE {
        EMAIL("email","用邮件处理"),
        INBOX("inbox","在Odoo内处理");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_USERS__NOTIFICATION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SMS_SMS__STATE]
     */
    @Getter
    public enum SMS_SMS__STATE {
        OUTGOING("outgoing","排队"),
        SENT("sent","已发送"),
        ERROR("error","错误"),
        CANCELED("canceled","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SMS_SMS__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[输入辅助_控件参数]
     */
    @Getter
    public enum CodeList101 {
        CARETTEMPLGROUP_SRFDA_CONTROLPARAM("CARETTEMPLGROUP_SRFDA_CONTROLPARAM","控件基本参数"),
        CARETTEMPLGROUP_SRFDA_CONTROLPARAM_CARET("CARETTEMPLGROUP_SRFDA_CONTROLPARAM_CARET","辅助输入控件参数"),
        CARETTEMPLGROUP_SRFDA_CONTROLPARAM_PICKUP("CARETTEMPLGROUP_SRFDA_CONTROLPARAM_PICKUP","选择控件参数"),
        CARETTEMPLGROUP_SRFDA_CONTROLPARAM_TEXTBOX("CARETTEMPLGROUP_SRFDA_CONTROLPARAM_TEXTBOX","文本控件参数"),
        CARETTEMPLGROUP_SRFDA_CONTROLPARAM_PICKUPLISTBOX("CARETTEMPLGROUP_SRFDA_CONTROLPARAM_PICKUPLISTBOX","选择列表控件参数");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList101(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PROJECT_TASK__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum PROJECT_TASK__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PROJECT_TASK__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体属性表单默认值类型]
     */
    @Getter
    public enum CodeList3 {
        SESSION("SESSION","用户全局对象"),
        APPLICATION("APPLICATION","系统全局对象"),
        UNIQUEID("UNIQUEID","唯一编码"),
        CONTEXT("CONTEXT","网页请求"),
        PARAM("PARAM","数据对象属性"),
        OPERATOR("OPERATOR","当前操作用户(编号)"),
        OPERATORNAME("OPERATORNAME","当前操作用户(名称)"),
        CURTIME("CURTIME","当前时间");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList3(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[实体属性插入模式]
     */
    @Getter
    public enum CodeList1 {
        VERSION("VERSION","版本模式");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList1(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[CRM_PARTNER_BINDING__ACTION]
     */
    @Getter
    public enum CRM_PARTNER_BINDING__ACTION {
        CREATE("create","创建客户"),
        EXIST("exist","链接到现有客户"),
        NOTHING("nothing","不要链接到某个客户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_PARTNER_BINDING__ACTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CRM_LEAD__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum CRM_LEAD__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__PRODUCT_VOLUME_VOLUME_IN_CUBIC_FEET]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__PRODUCT_VOLUME_VOLUME_IN_CUBIC_FEET {
        ITEM_0("0","Cubic Meters"),
        ITEM_1("1","Cubic Feet");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__PRODUCT_VOLUME_VOLUME_IN_CUBIC_FEET(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[日志级别]
     */
    @Getter
    public enum CodeList32 {
        ITEM_50000("50000","致命(FATAL)"),
        ITEM_40000("40000","错误(ERROR)"),
        ITEM_30000("30000","警告(WARN)"),
        ITEM_20000("20000","信息(INFO)"),
        ITEM_10000("10000","调试(DEBUG)"),
        ITEM_5000("5000","调试(TRACE)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList32(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_RULE__PROCURE_METHOD]
     */
    @Getter
    public enum STOCK_RULE__PROCURE_METHOD {
        MAKE_TO_STOCK("make_to_stock","从库存获取"),
        MAKE_TO_ORDER("make_to_order","触发其他规则"),
        MTS_ELSE_MTO("mts_else_mto","Take From Stock, if unavailable, Trigger Another Rule");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_RULE__PROCURE_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[时间维度类型]
     */
    @Getter
    public enum CodeList87 {
        YM("YM","年、月"),
        YMW("YMW","年、月、周"),
        YMWD("YMWD","年、月、周、天"),
        YMWDH("YMWDH","年、月、周、天、小时"),
        YMD("YMD","年、月、天"),
        YMDH("YMDH","年、月、天、小时"),
        YW("YW","年、周"),
        YWD("YWD","年、周、天"),
        YWDH("YWDH","年、周、天、小时");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList87(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_EVENT__ATTENDEE_STATUS]
     */
    @Getter
    public enum CALENDAR_EVENT__ATTENDEE_STATUS {
        NEEDSACTION("needsAction","待处理"),
        TENTATIVE("tentative","不确定"),
        DECLINED("declined","已拒绝"),
        ACCEPTED("accepted","已接收");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__ATTENDEE_STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_ACTIVITY_TYPE__DECORATION_TYPE]
     */
    @Getter
    public enum MAIL_ACTIVITY_TYPE__DECORATION_TYPE {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_ACTIVITY_TYPE__DECORATION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAINTENANCE_REQUEST__MAINTENANCE_TYPE]
     */
    @Getter
    public enum MAINTENANCE_REQUEST__MAINTENANCE_TYPE {
        CORRECTIVE("corrective","纠正"),
        PREVENTIVE("preventive","预防措施");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAINTENANCE_REQUEST__MAINTENANCE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_ACCOUNT_TAG__APPLICABILITY]
     */
    @Getter
    public enum ACCOUNT_ACCOUNT_TAG__APPLICABILITY {
        ACCOUNTS("accounts","用户"),
        TAXES("taxes","税率设置");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_ACCOUNT_TAG__APPLICABILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[GAMIFICATION_BADGE__LEVEL]
     */
    @Getter
    public enum GAMIFICATION_BADGE__LEVEL {
        BRONZE("bronze","青铜"),
        SILVER("silver","银色"),
        GOLD("gold","黄金");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_BADGE__LEVEL(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE__HR_PRESENCE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE__HR_PRESENCE_STATE {
        PRESENT("present","出勤"),
        ABSENT("absent","缺勤"),
        TO_DEFINE("to_define","界定");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__HR_PRESENCE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[CRM_LEAD2OPPORTUNITY_PARTNER__ACTION]
     */
    @Getter
    public enum CRM_LEAD2OPPORTUNITY_PARTNER__ACTION {
        CREATE("create","创建客户"),
        EXIST("exist","链接到现有客户"),
        NOTHING("nothing","不要链接到某个客户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD2OPPORTUNITY_PARTNER__ACTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL_TEMPLATE__AMOUNT_TYPE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL_TEMPLATE__AMOUNT_TYPE {
        FIXED("fixed","固定"),
        PERCENTAGE("percentage","余额百分比"),
        REGEX("regex","来自标签");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL_TEMPLATE__AMOUNT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[物理信息更新模式]
     */
    @Getter
    public enum CodeList57 {
        UPDATEWHENMODIFY("UPDATEWHENMODIFY","变更时更新");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList57(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_RULE__ACTION]
     */
    @Getter
    public enum STOCK_RULE__ACTION {
        PULL("pull","拉"),
        PUSH("push","推"),
        PULL_PUSH("pull_push","拉并推"),
        MANUFACTURE("manufacture","制造"),
        BUY("buy","购买");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_RULE__ACTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[RES_PARTNER__TRUST]
     */
    @Getter
    public enum RES_PARTNER__TRUST {
        GOOD("good","信用好的债务人"),
        NORMAL("normal","普通的债务人"),
        BAD("bad","信用差的债务人");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__TRUST(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_PAYMENT__PARTNER_TYPE]
     */
    @Getter
    public enum ACCOUNT_PAYMENT__PARTNER_TYPE {
        CUSTOMER("customer","客户"),
        SUPPLIER("supplier","供应商");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT__PARTNER_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_ALLOCATION__HOLIDAY_TYPE]
     */
    @Getter
    public enum HR_LEAVE_ALLOCATION__HOLIDAY_TYPE {
        EMPLOYEE("employee","按员工"),
        COMPANY("company","由公司"),
        DEPARTMENT("department","部门"),
        CATEGORY("category","按员工标签");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_ALLOCATION__HOLIDAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__AUTH_SIGNUP_UNINVITED]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__AUTH_SIGNUP_UNINVITED {
        B2B("b2b","应邀"),
        B2C("b2c","免费注册");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__AUTH_SIGNUP_UNINVITED(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[FLEET_VEHICLE_LOG_CONTRACT__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum FLEET_VEHICLE_LOG_CONTRACT__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE_LOG_CONTRACT__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[CRM_LEAD2OPPORTUNITY_PARTNER_MASS__NAME]
     */
    @Getter
    public enum CRM_LEAD2OPPORTUNITY_PARTNER_MASS__NAME {
        CONVERT("convert","转换为商机"),
        MERGE("merge","与已存在的商机合并");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD2OPPORTUNITY_PARTNER_MASS__NAME(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[EVENT_MAIL__INTERVAL_TYPE]
     */
    @Getter
    public enum EVENT_MAIL__INTERVAL_TYPE {
        AFTER_SUB("after_sub","注册之后"),
        BEFORE_EVENT("before_event","在事件之前"),
        AFTER_EVENT("after_event","事件之后");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_MAIL__INTERVAL_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__PAYMENT_ACQUIRER_ONBOARDING_STATE]
     */
    @Getter
    public enum RES_COMPANY__PAYMENT_ACQUIRER_ONBOARDING_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__PAYMENT_ACQUIRER_ONBOARDING_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_REQUISITION__STATE]
     */
    @Getter
    public enum PURCHASE_REQUISITION__STATE {
        DRAFT("draft","草稿"),
        ONGOING("ongoing","正在进行"),
        IN_PROGRESS("in_progress","已确认"),
        OPEN("open","出价选择"),
        DONE("done","已关闭"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_REQUISITION__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[GAMIFICATION_BADGE__RULE_AUTH]
     */
    @Getter
    public enum GAMIFICATION_BADGE__RULE_AUTH {
        EVERYONE("everyone","所有人"),
        USERS("users","已选的用户列表"),
        HAVING("having","拥有徽章的用户"),
        NOBODY("nobody","没有人完成已分配的挑战");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_BADGE__RULE_AUTH(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[统一资源类型]
     */
    @Getter
    public enum CodeList16 {
        PAGE("PAGE","内置页面"),
        REPORT("REPORT","报表"),
        CUSTOM("CUSTOM","自定义");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList16(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL__MATCH_LABEL]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL__MATCH_LABEL {
        CONTAINS("contains","包含"),
        NOT_CONTAINS("not_contains","不包含"),
        MATCH_REGEX("match_regex","正则匹配");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL__MATCH_LABEL(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[缩略界面类型]
     */
    @Getter
    public enum CodeList7 {
        FORM("FORM","表单"),
        PAGE("PAGE","内置页面");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList7(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_RESUME_LINE__DISPLAY_TYPE]
     */
    @Getter
    public enum HR_RESUME_LINE__DISPLAY_TYPE {
        CLASSIC("classic","Classic"),
        COURSE("course","课程"),
        CERTIFICATION("certification","认证");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_RESUME_LINE__DISPLAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PROJECT_TASK__ACTIVITY_STATE]
     */
    @Getter
    public enum PROJECT_TASK__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PROJECT_TASK__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__ACCOUNT_ONBOARDING_SALE_TAX_STATE]
     */
    @Getter
    public enum RES_COMPANY__ACCOUNT_ONBOARDING_SALE_TAX_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__ACCOUNT_ONBOARDING_SALE_TAX_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL__RULE_TYPE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL__RULE_TYPE {
        WRITEOFF_BUTTON("writeoff_button","在单击按钮上手动创建注销。"),
        WRITEOFF_SUGGESTION("writeoff_suggestion","建议对应值"),
        INVOICE_MATCHING("invoice_matching","匹配发票／账单");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL__RULE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[FLEET_VEHICLE__TRANSMISSION]
     */
    @Getter
    public enum FLEET_VEHICLE__TRANSMISSION {
        MANUAL("manual","手动"),
        AUTOMATIC("automatic","自动的");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE__TRANSMISSION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_LEAVE__STATE]
     */
    @Getter
    public enum HR_LEAVE__STATE {
        DRAFT("draft","待提交"),
        CANCEL("cancel","已取消"),
        CONFIRM("confirm","待批准"),
        REFUSE("refuse","已拒绝"),
        VALIDATE1("validate1","第二次审批"),
        VALIDATE("validate","已批准");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE]
     */
    @Getter
    public enum RES_COMPANY__ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PROJECT_TASK__PRIORITY]
     */
    @Getter
    public enum PROJECT_TASK__PRIORITY {
        ITEM_0("0","一般"),
        ITEM_1("1","重要");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PROJECT_TASK__PRIORITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CRM_LEAD2OPPORTUNITY_PARTNER__NAME]
     */
    @Getter
    public enum CRM_LEAD2OPPORTUNITY_PARTNER__NAME {
        CONVERT("convert","转换为商机"),
        MERGE("merge","与已存在的商机合并");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD2OPPORTUNITY_PARTNER__NAME(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE__REQUEST_DATE_FROM_PERIOD]
     */
    @Getter
    public enum HR_LEAVE__REQUEST_DATE_FROM_PERIOD {
        AM("am","上午"),
        PM("pm","下午");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE__REQUEST_DATE_FROM_PERIOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_TYPE__REQUEST_UNIT]
     */
    @Getter
    public enum HR_LEAVE_TYPE__REQUEST_UNIT {
        DAY("day","天"),
        HALF_DAY("half_day","半天"),
        HOUR("hour","小时");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_TYPE__REQUEST_UNIT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[LUNCH_ORDER__STATE]
     */
    @Getter
    public enum LUNCH_ORDER__STATE {
        NEW("new","订购"),
        ORDERED("ordered","已订购"),
        CONFIRMED("confirmed","已接收"),
        CANCELLED("cancelled","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_ORDER__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_AMOUNT]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_AMOUNT {
        LOWER("lower","大于"),
        GREATER("greater","大于"),
        BETWEEN("between","介于");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_AMOUNT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[RES_COMPANY__SALE_ONBOARDING_PAYMENT_METHOD]
     */
    @Getter
    public enum RES_COMPANY__SALE_ONBOARDING_PAYMENT_METHOD {
        DIGITAL_SIGNATURE("digital_signature","在线签名"),
        PAYPAL("paypal","PayPal"),
        STRIPE("stripe","条纹"),
        OTHER("other","其它方式支付"),
        MANUAL("manual","手动付款");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__SALE_ONBOARDING_PAYMENT_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_BOM__READY_TO_PRODUCE]
     */
    @Getter
    public enum MRP_BOM__READY_TO_PRODUCE {
        ALL_AVAILABLE("all_available","当所有组件都可用时"),
        ASAP("asap","当第一次操作的组件可用时");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_BOM__READY_TO_PRODUCE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_MODERATION__STATUS]
     */
    @Getter
    public enum MAIL_MODERATION__STATUS {
        ALLOW("allow","总是允许"),
        BAN("ban","永久禁止");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_MODERATION__STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_LEAVE_ALLOCATION__ALLOCATION_TYPE]
     */
    @Getter
    public enum HR_LEAVE_ALLOCATION__ALLOCATION_TYPE {
        REGULAR("regular","常规的配置"),
        ACCRUAL("accrual","应计分配");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_ALLOCATION__ALLOCATION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_JOURNAL__TYPE]
     */
    @Getter
    public enum ACCOUNT_JOURNAL__TYPE {
        SALE("sale","销售"),
        PURCHASE("purchase","采购"),
        CASH("cash","现金"),
        BANK("bank","银行"),
        GENERAL("general","杂项");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_JOURNAL__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE]
     */
    @Getter
    public enum PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE {
        NEW_USER("new_user","我没有Paypal账户"),
        EXISTING_USER("existing_user","我有Paypal账户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYPAL_USER_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[周期时间类型]
     */
    @Getter
    public enum CodeList40 {
        MONTH("MONTH","月度"),
        SEASON("SEASON","季度"),
        WEEK("WEEK","周"),
        DAY("DAY","天");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList40(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PAYMENT_TERM_LINE__OPTION]
     */
    @Getter
    public enum ACCOUNT_PAYMENT_TERM_LINE__OPTION {
        DAY_AFTER_INVOICE_DATE("day_after_invoice_date","发票日期后的几天"),
        DAY_FOLLOWING_MONTH("day_following_month","下月"),
        DAY_CURRENT_MONTH("day_current_month","当月");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT_TERM_LINE__OPTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }






    /**
     * 代码表[MRP_PRODUCT_PRODUCE__CONSUMPTION]
     */
    @Getter
    public enum MRP_PRODUCT_PRODUCE__CONSUMPTION {
        STRICT("strict","Strict"),
        FLEXIBLE("flexible","Flexible");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_PRODUCT_PRODUCE__CONSUMPTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_EVENT__SHOW_AS]
     */
    @Getter
    public enum CALENDAR_EVENT__SHOW_AS {
        FREE("free","空闲"),
        BUSY("busy","忙碌");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__SHOW_AS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_EXPENSE__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum HR_EXPENSE__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EXPENSE__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[REPAIR_ORDER__STATE]
     */
    @Getter
    public enum REPAIR_ORDER__STATE {
        DRAFT("draft","报价单"),
        CANCEL("cancel","已取消"),
        CONFIRMED("confirmed","已确认"),
        UNDER_REPAIR("under_repair","维修中"),
        READY("ready","准备维修"),
        ITEM_6("2binvoiced","要开发票的"),
        INVOICE_EXCEPT("invoice_except","发票异常"),
        DONE("done","已维修");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        REPAIR_ORDER__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[实体数据操作]
     */
    @Getter
    public enum CodeList10 {
        INSERT("INSERT","插入"),
        UPDATE("UPDATE","更新"),
        DELETE("DELETE","删除"),
        SELECT("SELECT","简单查询"),
        CUSTOMCALL("CUSTOMCALL","自定义"),
        CUSTOMPROCCALL("CUSTOMPROCCALL","自定义存储过程");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList10(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[补丁归属]
     */
    @Getter
    public enum CodeList36 {
        ITEM_1("1","框架基本"),
        ITEM_2("2","框架高级"),
        ITEM_4("4","工作流"),
        ITEM_8("8","EAI"),
        ITEM_16("16","UAC"),
        ITEM_32("32","全文检索"),
        ITEM_64("64","数据分析"),
        ITEM_128("128","基础网盘"),
        ITEM_256("256","基础组织");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList36(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[动态面板分区缩放样式]
     */
    @Getter
    public enum CodeList93 {
        EXPAND("EXPAND","展开"),
        COLLAPSE("COLLAPSE","收缩");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList93(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_WAREHOUSE_ORDERPOINT__LEAD_TYPE]
     */
    @Getter
    public enum STOCK_WAREHOUSE_ORDERPOINT__LEAD_TYPE {
        NET("net","获得产品的天数"),
        SUPPLIER("supplier","采购的天数");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_WAREHOUSE_ORDERPOINT__LEAD_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[STOCK_PICKING__STATE]
     */
    @Getter
    public enum STOCK_PICKING__STATE {
        DRAFT("draft","草稿"),
        WAITING("waiting","等待其它作业"),
        CONFIRMED("confirmed","正在等待"),
        ASSIGNED("assigned","就绪"),
        DONE("done","完成"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PICKING__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[时间分组类型]
     */
    @Getter
    public enum CodeList92 {
        Q("Q","季度"),
        M("M","月份"),
        D("D","月天"),
        H("H","小时");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList92(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[LUNCH_ALERT__NOTIFICATION_MOMENT]
     */
    @Getter
    public enum LUNCH_ALERT__NOTIFICATION_MOMENT {
        AM("am","上午"),
        PM("pm","下午");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_ALERT__NOTIFICATION_MOMENT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_LOCATION__USAGE]
     */
    @Getter
    public enum STOCK_LOCATION__USAGE {
        SUPPLIER("supplier","供应商位置"),
        VIEW("view","视图"),
        INTERNAL("internal","内部位置"),
        CUSTOMER("customer","客户位置"),
        INVENTORY("inventory","库存损失"),
        PRODUCTION("production","生产"),
        TRANSIT("transit","中转位置");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_LOCATION__USAGE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体数据处理_数据对象操作]
     */
    @Getter
    public enum CodeList107 {
        CREATENEW("CREATENEW","新建数据对象"),
        CREATEFROM("CREATEFROM","拷贝新建数据对象"),
        COPY("COPY","拷贝数据对象"),
        COPYRESET("COPYRESET","拷贝数据对象(重置)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList107(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[STOCK_SCRAP__STATE]
     */
    @Getter
    public enum STOCK_SCRAP__STATE {
        DRAFT("draft","草稿"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_SCRAP__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum HR_LEAVE__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE__HOLIDAY_TYPE]
     */
    @Getter
    public enum HR_LEAVE__HOLIDAY_TYPE {
        EMPLOYEE("employee","按员工"),
        COMPANY("company","由公司"),
        DEPARTMENT("department","部门"),
        CATEGORY("category","按员工标签");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE__HOLIDAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_PARTNER__PICKING_WARN]
     */
    @Getter
    public enum RES_PARTNER__PICKING_WARN {
        NO_SUB_MESSAGE("no-message","没消息"),
        WARNING("warning","警告"),
        BLOCK("block","受阻消息");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__PICKING_WARN(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__LEAD_ENRICH_AUTO]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__LEAD_ENRICH_AUTO {
        MANUAL("manual","只根据需要丰富线索"),
        AUTO("auto","自动丰富所有线索");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__LEAD_ENRICH_AUTO(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体属性访问控制]
     */
    @Getter
    public enum CodeList88 {
        NONE("NONE","无"),
        READ("READ","读取"),
        UPDATE("UPDATE","更新");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList88(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_CHANNEL_PARTNER__FOLD_STATE]
     */
    @Getter
    public enum MAIL_CHANNEL_PARTNER__FOLD_STATE {
        OPEN("open","打开"),
        FOLDED("folded","收起"),
        CLOSED("closed","已关闭");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_CHANNEL_PARTNER__FOLD_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_CATEGORY__PROPERTY_VALUATION]
     */
    @Getter
    public enum PRODUCT_CATEGORY__PROPERTY_VALUATION {
        MANUAL_PERIODIC("manual_periodic","手动"),
        REAL_TIME("real_time","自动");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_CATEGORY__PROPERTY_VALUATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_WORKCENTER_PRODUCTIVITY_LOSS_TYPE__LOSS_TYPE]
     */
    @Getter
    public enum MRP_WORKCENTER_PRODUCTIVITY_LOSS_TYPE__LOSS_TYPE {
        AVAILABILITY("availability","可用量"),
        PERFORMANCE("performance","效能"),
        QUALITY("quality","质量"),
        PRODUCTIVE("productive","生产性");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_WORKCENTER_PRODUCTIVITY_LOSS_TYPE__LOSS_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_TEMPLATE__SERVICE_TYPE]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__SERVICE_TYPE {
        MANUAL("manual","在订单中手动设置数量");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__SERVICE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_2]
     */
    @Getter
    public enum LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_2 {
        NONE_OR_MORE("0_more","没有或更多"),
        ONE_OR_MORE("1_more","一个或多个"),
        ONE("1","只有一个");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_2(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[MAIL_MAIL__STATE]
     */
    @Getter
    public enum MAIL_MAIL__STATE {
        OUTGOING("outgoing","发出"),
        SENT("sent","已发送"),
        RECEIVED("received","已接收"),
        EXCEPTION("exception","投递失败"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_MAIL__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_SURVEY__CATEGORY]
     */
    @Getter
    public enum SURVEY_SURVEY__CATEGORY {
        DEFAULT("default","通用的调查");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_SURVEY__CATEGORY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAINTENANCE_EQUIPMENT__EQUIPMENT_ASSIGN_TO]
     */
    @Getter
    public enum MAINTENANCE_EQUIPMENT__EQUIPMENT_ASSIGN_TO {
        DEPARTMENT("department","部门"),
        EMPLOYEE("employee","员工"),
        OTHER("other","其他");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAINTENANCE_EQUIPMENT__EQUIPMENT_ASSIGN_TO(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[CRM_ACTIVITY_REPORT__LEAD_TYPE]
     */
    @Getter
    public enum CRM_ACTIVITY_REPORT__LEAD_TYPE {
        LEAD("lead","线索"),
        OPPORTUNITY("opportunity","商机");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_ACTIVITY_REPORT__LEAD_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_INVITE__EXISTING_MODE]
     */
    @Getter
    public enum SURVEY_INVITE__EXISTING_MODE {
        NEW("new","新的邀请"),
        RESEND("resend","重新发送邀请");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_INVITE__EXISTING_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_LANG__DIRECTION]
     */
    @Getter
    public enum RES_LANG__DIRECTION {
        LTR("ltr","从左到右"),
        RTL("rtl","从右到左");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_LANG__DIRECTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[MRP_ROUTING_WORKCENTER__TIME_MODE]
     */
    @Getter
    public enum MRP_ROUTING_WORKCENTER__TIME_MODE {
        AUTO("auto","基于实际时间计算"),
        MANUAL("manual","手动设置时长");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_ROUTING_WORKCENTER__TIME_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_ORDER_LINE__QTY_DELIVERED_METHOD]
     */
    @Getter
    public enum SALE_ORDER_LINE__QTY_DELIVERED_METHOD {
        MANUAL("manual","手动"),
        ANALYTIC("analytic","分析费用"),
        STOCK_MOVE("stock_move","库存移动");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER_LINE__QTY_DELIVERED_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[FLEET_VEHICLE__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum FLEET_VEHICLE__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RESOURCE_CALENDAR_ATTENDANCE__WEEK_TYPE]
     */
    @Getter
    public enum RESOURCE_CALENDAR_ATTENDANCE__WEEK_TYPE {
        ITEM_1("1","Odd week"),
        ITEM_0("0","Even week");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RESOURCE_CALENDAR_ATTENDANCE__WEEK_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_ORDER_LINE__DISPLAY_TYPE]
     */
    @Getter
    public enum PURCHASE_ORDER_LINE__DISPLAY_TYPE {
        LINE_SECTION("line_section","章节"),
        LINE_NOTE("line_note","备注");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_ORDER_LINE__DISPLAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[WEBSITE_SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD]
     */
    @Getter
    public enum WEBSITE_SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD {
        PAYPAL("paypal","PayPal"),
        STRIPE("stripe","信用卡（通过 Stripe)"),
        OTHER("other","其他支付收购方"),
        MANUAL("manual","自定义付款说明");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        WEBSITE_SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体存储类型]
     */
    @Getter
    public enum CodeList80 {
        STATIC("STATIC","静态存储"),
        DYNAMIC("DYNAMIC","动态存储"),
        NONE("NONE","无存储");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList80(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_EVENT__PRIVACY]
     */
    @Getter
    public enum CALENDAR_EVENT__PRIVACY {
        PUBLIC("public","所有人"),
        PRIVATE("private","仅我"),
        CONFIDENTIAL("confidential","仅内部用户");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__PRIVACY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[图表控件_表格位置]
     */
    @Getter
    public enum CodeList52 {
        NONE("NONE","无表格"),
        TOPLEFT("TOPLEFT","上左"),
        TOP("TOP","上中"),
        TOPRIGHT("TOPRIGHT","上右"),
        BOTTOMLEFT("BOTTOMLEFT","下左"),
        BOTTOM("BOTTOM","下中"),
        BOTTOMRIGHT("BOTTOMRIGHT","下右"),
        LEFTTOP("LEFTTOP","左上"),
        LEFT("LEFT","左中"),
        LEFTBOTTOM("LEFTBOTTOM","左下"),
        RIGHTTOP("RIGHTTOP","右上"),
        RIGHT("RIGHT","右中"),
        RIGHTBOTTOM("RIGHTBOTTOM","右下");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList52(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[FLEET_VEHICLE__FUEL_TYPE]
     */
    @Getter
    public enum FLEET_VEHICLE__FUEL_TYPE {
        GASOLINE("gasoline","汽油"),
        DIESEL("diesel","柴油"),
        LPG("lpg","液化石油气"),
        ELECTRIC("electric","电子启动"),
        HYBRID("hybrid","混合动力");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE__FUEL_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体规则处理_操作逻辑]
     */
    @Getter
    public enum CodeList111 {
        EQ("=","等于"),
        LTGT("<>","不等于"),
        GTEQ(">=","大于等于"),
        GT(">","大于"),
        LTEQ("<=","小于等于"),
        LT("<","小于"),
        LIKE("LIKE","文本匹配"),
        ISNULL("ISNULL","为空"),
        ISNOTNULL("ISNOTNULL","不为空");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList111(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_ORDER__INVOICE_STATUS]
     */
    @Getter
    public enum PURCHASE_ORDER__INVOICE_STATUS {
        NO("no","无需开单"),
        TO_INVOICE("to invoice","未收到账单"),
        INVOICED("invoiced","已完全开单");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_ORDER__INVOICE_STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_PRICELIST_ITEM__COMPUTE_PRICE]
     */
    @Getter
    public enum PRODUCT_PRICELIST_ITEM__COMPUTE_PRICE {
        FIXED("fixed","固定价格"),
        PERCENTAGE("percentage","百分比 ( 折扣 )"),
        FORMULA("formula","公式");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_PRICELIST_ITEM__COMPUTE_PRICE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[日历、邮件重要程度]
     */
    @Getter
    public enum CodeList8 {
        HIGH("HIGH","高"),
        NORMAL("NORMAL","普通"),
        LOW("LOW","低");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList8(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[输入辅助_实体处理逻辑代码]
     */
    @Getter
    public enum CodeList103 {
        CARETTEMPLGROUP_SRFDA_DEDCCONTEXT("CARETTEMPLGROUP_SRFDA_DEDCCONTEXT","实体处理逻辑引擎"),
        CARETTEMPLGROUP_SRFDA_DEACTION("CARETTEMPLGROUP_SRFDA_DEACTION","实体属性操作");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList103(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_EXPENSE_SHEET__ACTIVITY_STATE]
     */
    @Getter
    public enum HR_EXPENSE_SHEET__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EXPENSE_SHEET__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[系统错误代码]
     */
    @Getter
    public enum CodeList35 {
        ITEM_0("0","正确(0)"),
        ITEM_1("1","内部发生错误(INTERNALERROR)"),
        ITEM_2("2","访问被拒绝(ACCESSDENY)"),
        ITEM_3("3","无效的数据(INVALIDDATA)"),
        ITEM_4("4","无效的数据键(INVALIDDATAKEYS)"),
        ITEM_5("5","输入的信息有误(INPUTERROR)"),
        ITEM_6("6","重复的数据键值(DUPLICATEKEY)"),
        ITEM_7("7","重复的数据(DUPLICATEDATA)"),
        ITEM_8("8","删除拒绝(DELETEREJECT)"),
        ITEM_9("9","逻辑处理错误(LOGICERROR)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList35(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_LEAVE__REQUEST_HOUR_TO]
     */
    @Getter
    public enum HR_LEAVE__REQUEST_HOUR_TO {
        ITEM_0("0","12:00 AM"),
        ITEM_0_5("0.5","0:30 AM"),
        ITEM_1("1","1:00 AM"),
        ITEM_1_5("1.5","1:30 AM"),
        ITEM_2("2","2:00 AM"),
        ITEM_2_5("2.5","2:30 AM"),
        ITEM_3("3","3:00 AM"),
        ITEM_3_5("3.5","3:30 AM"),
        ITEM_4("4","4:00 AM"),
        ITEM_4_5("4.5","4:30 AM"),
        ITEM_5("5","5:00 AM"),
        ITEM_5_5("5.5","5:30 AM"),
        ITEM_6("6","6:00 AM"),
        ITEM_6_5("6.5","6:30 AM"),
        ITEM_7("7","7:00 AM"),
        ITEM_7_5("7.5","7:30 AM"),
        ITEM_8("8","8:00 AM"),
        ITEM_8_5("8.5","8:30 AM"),
        ITEM_9("9","9:00 AM"),
        ITEM_9_5("9.5","9:30 AM"),
        ITEM_10("10","10:00 AM"),
        ITEM_10_5("10.5","10:30 AM"),
        ITEM_11("11","11:00 AM"),
        ITEM_11_5("11.5","11:30 AM"),
        ITEM_12("12","12:00 PM"),
        ITEM_12_5("12.5","0:30 PM"),
        ITEM_13("13","1:00 PM"),
        ITEM_13_5("13.5","1:30 PM"),
        ITEM_14("14","2:00 PM"),
        ITEM_14_5("14.5","2:30 PM"),
        ITEM_15("15","3:00 PM"),
        ITEM_15_5("15.5","3:30 PM"),
        ITEM_16("16","4:00 PM"),
        ITEM_16_5("16.5","4:30 PM"),
        ITEM_17("17","5:00 PM"),
        ITEM_17_5("17.5","5:30 PM"),
        ITEM_18("18","6:00 PM"),
        ITEM_18_5("18.5","6:30 PM"),
        ITEM_19("19","7:00 PM"),
        ITEM_19_5("19.5","7:30 PM"),
        ITEM_20("20","8:00 PM"),
        ITEM_20_5("20.5","8:30 PM"),
        ITEM_21("21","9:00 PM"),
        ITEM_21_5("21.5","9:30 PM"),
        ITEM_22("22","10:00 PM"),
        ITEM_22_5("22.5","10:30 PM"),
        ITEM_23("23","11:00 PM"),
        ITEM_23_5("23.5","11:30 PM");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE__REQUEST_HOUR_TO(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[语言资源类型]
     */
    @Getter
    public enum CodeList55 {
        DEF_LNAME("DEF.LNAME","实体属性逻辑名称"),
        CL_ITEM_LNAME("CL.ITEM.LNAME","代码表项"),
        TBB_TEXT("TBB.TEXT","工具栏按钮文本"),
        TBB_TOOLTIP("TBB.TOOLTIP","工具栏按钮提示"),
        MENUITEM_CAPTION("MENUITEM.CAPTION","菜单项文本"),
        PAGE_HEADER("PAGE.HEADER","界面头部标题"),
        PAGE_COMMON("PAGE.COMMON","界面常规"),
        CONTROL("CONTROL","控件文本"),
        ERROR_STD("ERROR.STD","标准错误"),
        CTRL("CTRL","处理逻辑"),
        COMMON("COMMON","通用"),
        OTHER("OTHER","其它");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList55(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[EVENT_EVENT_TICKET__SEATS_AVAILABILITY]
     */
    @Getter
    public enum EVENT_EVENT_TICKET__SEATS_AVAILABILITY {
        LIMITED("limited","限制的"),
        UNLIMITED("unlimited","无限制的");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_EVENT_TICKET__SEATS_AVAILABILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[GAMIFICATION_GOAL_DEFINITION__DISPLAY_MODE]
     */
    @Getter
    public enum GAMIFICATION_GOAL_DEFINITION__DISPLAY_MODE {
        PROGRESS("progress","逐行（使用数值化值）"),
        BOOLEAN("boolean","独占（完成或未完成）");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_GOAL_DEFINITION__DISPLAY_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE__ACTIVITY_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_ALIAS__ALIAS_CONTACT]
     */
    @Getter
    public enum MAIL_ALIAS__ALIAS_CONTACT {
        EVERYONE("everyone","所有人"),
        PARTNERS("partners","身份验证过的业务伙伴"),
        FOLLOWERS("followers","仅关注者"),
        EMPLOYEES("employees","授权的员工");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_ALIAS__ALIAS_CONTACT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EXPENSE__PAYMENT_MODE]
     */
    @Getter
    public enum HR_EXPENSE__PAYMENT_MODE {
        OWN_ACCOUNT("own_account","报销"),
        COMPANY_ACCOUNT("company_account","公司");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EXPENSE__PAYMENT_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_USER_INPUT__STATE]
     */
    @Getter
    public enum SURVEY_USER_INPUT__STATE {
        NEW("new","尚未开始"),
        SKIP("skip","部分完成"),
        DONE("done","已完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_USER_INPUT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据库类型]
     */
    @Getter
    public enum CodeList33 {
        DB2("DB2","DB2"),
        ORACLE("ORACLE","ORACLE"),
        MSSQL("MSSQL","MSSQLSERVER"),
        MYSQL("MYSQL","MySQL"),
        SYBASE("SYBASE","SYBASE"),
        INFORMIX("INFORMIX","INFORMIX");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList33(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_ORDER__STATE]
     */
    @Getter
    public enum SALE_ORDER__STATE {
        DRAFT("draft","报价单"),
        SENT("sent","报价已发送"),
        SALE("sale","销售订单"),
        DONE("done","已锁定"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[时区]
     */
    @Getter
    public enum CodeList59 {
        ETC_GMT_12("Etc/GMT+12","(UTC-12:00)GMT-12:00"),
        ETC_GMT_11("Etc/GMT+11","(UTC-11:00)GMT-11:00"),
        MIT("MIT","(UTC-11:00)West Samoa Time"),
        PACIFIC_MIDWAY("Pacific/Midway","(UTC-11:00)Samoa Standard Time"),
        PACIFIC_NIUE("Pacific/Niue","(UTC-11:00)Niue Time"),
        AMERICA_ADAK("America/Adak","(UTC-10:00)Hawaii-Aleutian Standard Time"),
        ETC_GMT_10("Etc/GMT+10","(UTC-10:00)GMT-10:00"),
        HST("HST","(UTC-10:00)Hawaii Standard Time"),
        PACIFIC_FAKAOFO("Pacific/Fakaofo","(UTC-10:00)Tokelau Time"),
        PACIFIC_RAROTONGA("Pacific/Rarotonga","(UTC-10:00)Cook Is. Time"),
        PACIFIC_TAHITI("Pacific/Tahiti","(UTC-10:00)Tahiti Time"),
        PACIFIC_MARQUESAS("Pacific/Marquesas","(UTC-9:00)Marquesas Time"),
        AST("AST","(UTC-9:00)Alaska Standard Time"),
        ETC_GMT_9("Etc/GMT+9","(UTC-9:00)GMT-09:00"),
        PACIFIC_GAMBIER("Pacific/Gambier","(UTC-9:00)Gambier Time"),
        AMERICA_DAWSON("America/Dawson","(UTC-8:00)Pacific Standard Time"),
        ETC_GMT_8("Etc/GMT+8","(UTC-8:00)GMT-08:00"),
        PACIFIC_PITCAIRN("Pacific/Pitcairn","(UTC-8:00)Pitcairn Standard Time"),
        AMERICA_BOISE("America/Boise","(UTC-7:00)Mountain Standard Time"),
        ETC_GMT_7("Etc/GMT+7","(UTC-7:00)GMT-07:00"),
        AMERICA_BELIZE("America/Belize","(UTC-6:00)Central Standard Time"),
        CHILE_EASTERISLAND("Chile/EasterIsland","(UTC-6:00)Easter Is. Time"),
        ETC_GMT_6("Etc/GMT+6","(UTC-6:00)GMT-06:00"),
        PACIFIC_GALAPAGOS("Pacific/Galapagos","(UTC-6:00)Galapagos Time"),
        AMERICA_ATIKOKAN("America/Atikokan","(UTC-5:00)Eastern Standard Time"),
        AMERICA_BOGOTA("America/Bogota","(UTC-5:00)Colombia Time"),
        AMERICA_EIRUNEPE("America/Eirunepe","(UTC-5:00)Acre Time"),
        AMERICA_GUAYAQUIL("America/Guayaquil","(UTC-5:00)Ecuador Time"),
        AMERICA_HAVANA("America/Havana","(UTC-5:00)Cuba Standard Time"),
        AMERICA_LIMA("America/Lima","(UTC-5:00)Peru Time"),
        ETC_GMT_5("Etc/GMT+5","(UTC-5:00)GMT-05:00"),
        AMERICA_CARACAS("America/Caracas","(UTC-4:00)Venezuela Time"),
        AMERICA_ANGUILLA("America/Anguilla","(UTC-4:00)Atlantic Standard Time"),
        AMERICA_ASUNCION("America/Asuncion","(UTC-4:00)Paraguay Time"),
        AMERICA_BOA_VISTA("America/Boa_Vista","(UTC-4:00)Amazon Time"),
        AMERICA_GUYANA("America/Guyana","(UTC-4:00)Guyana Time"),
        AMERICA_LA_PAZ("America/La_Paz","(UTC-4:00)Bolivia Time"),
        AMERICA_SANTIAGO("America/Santiago","(UTC-4:00)Chile Time"),
        ATLANTIC_STANLEY("Atlantic/Stanley","(UTC-4:00)Falkland Is. Time"),
        ETC_GMT_4("Etc/GMT+4","(UTC-4:00)GMT-04:00"),
        AMERICA_ST_JOHNS("America/St_Johns","(UTC-3:00)Newfoundland Standard Time"),
        AGT("AGT","(UTC-3:00)Argentine Time"),
        AMERICA_ARAGUAINA("America/Araguaina","(UTC-3:00)Brasilia Time"),
        AMERICA_CAYENNE("America/Cayenne","(UTC-3:00)French Guiana Time"),
        AMERICA_GODTHAB("America/Godthab","(UTC-3:00)Western Greenland Time"),
        AMERICA_MIQUELON("America/Miquelon","(UTC-3:00)Pierre & Miquelon Standard Time"),
        AMERICA_MONTEVIDEO("America/Montevideo","(UTC-3:00)Uruguay Time"),
        AMERICA_PARAMARIBO("America/Paramaribo","(UTC-3:00)Suriname Time"),
        ANTARCTICA_ROTHERA("Antarctica/Rothera","(UTC-3:00)Rothera Time"),
        ETC_GMT_3("Etc/GMT+3","(UTC-3:00)GMT-03:00"),
        AMERICA_NORONHA("America/Noronha","(UTC-2:00)Fernando de Noronha Time"),
        ATLANTIC_SOUTH_GEORGIA("Atlantic/South_Georgia","(UTC-2:00)South Georgia Standard Time"),
        ETC_GMT_2("Etc/GMT+2","(UTC-2:00)GMT-02:00"),
        AMERICA_SCORESBYSUND("America/Scoresbysund","(UTC-1:00)Eastern Greenland Time"),
        ATLANTIC_AZORES("Atlantic/Azores","(UTC-1:00)Azores Time"),
        ATLANTIC_CAPE_VERDE("Atlantic/Cape_Verde","(UTC-1:00)Cape Verde Time"),
        ETC_GMT_1("Etc/GMT+1","(UTC-1:00)GMT-01:00"),
        AFRICA_ABIDJAN("Africa/Abidjan","(UTC0:00)Greenwich Mean Time"),
        AFRICA_ACCRA("Africa/Accra","(UTC0:00)Ghana Mean Time"),
        AFRICA_CASABLANCA("Africa/Casablanca","(UTC0:00)Western European Time"),
        ETC_GMT("Etc/GMT","(UTC0:00)GMT+00:00"),
        ETC_UCT("Etc/UCT","(UTC0:00)Coordinated Universal Time"),
        AFRICA_ALGIERS("Africa/Algiers","(UTC+1:00)Central European Time"),
        AFRICA_BANGUI("Africa/Bangui","(UTC+1:00)Western African Time"),
        ETC_GMT__1("Etc/GMT-1","(UTC+1:00)GMT+01:00"),
        MET("MET","(UTC+1:00)Middle Europe Time"),
        ART("ART","(UTC+2:00)Eastern European Time"),
        AFRICA_BLANTYRE("Africa/Blantyre","(UTC+2:00)Central African Time"),
        AFRICA_JOHANNESBURG("Africa/Johannesburg","(UTC+2:00)South Africa Standard Time"),
        ASIA_JERUSALEM("Asia/Jerusalem","(UTC+2:00)Israel Standard Time"),
        ETC_GMT__2("Etc/GMT-2","(UTC+2:00)GMT+02:00"),
        AFRICA_ADDIS_ABABA("Africa/Addis_Ababa","(UTC+3:00)Eastern African Time"),
        ANTARCTICA_SYOWA("Antarctica/Syowa","(UTC+3:00)Syowa Time"),
        ASIA_ADEN("Asia/Aden","(UTC+3:00)Arabia Standard Time"),
        ETC_GMT__3("Etc/GMT-3","(UTC+3:00)GMT+03:00"),
        EUROPE_MOSCOW("Europe/Moscow","(UTC+3:00)Moscow Standard Time"),
        EUROPE_VOLGOGRAD("Europe/Volgograd","(UTC+3:00)Volgograd Time"),
        ASIA_RIYADH87("Asia/Riyadh87","(UTC+3:00)GMT+03:07"),
        ASIA_TEHRAN("Asia/Tehran","(UTC+3:00)Iran Standard Time"),
        ASIA_BAKU("Asia/Baku","(UTC+4:00)Azerbaijan Time"),
        ASIA_DUBAI("Asia/Dubai","(UTC+4:00)Gulf Standard Time"),
        ASIA_TBILISI("Asia/Tbilisi","(UTC+4:00)Georgia Time"),
        ASIA_YEREVAN("Asia/Yerevan","(UTC+4:00)Armenia Time"),
        ETC_GMT__4("Etc/GMT-4","(UTC+4:00)GMT+04:00"),
        EUROPE_SAMARA("Europe/Samara","(UTC+4:00)Samara Time"),
        INDIAN_MAHE("Indian/Mahe","(UTC+4:00)Seychelles Time"),
        INDIAN_MAURITIUS("Indian/Mauritius","(UTC+4:00)Mauritius Time"),
        INDIAN_REUNION("Indian/Reunion","(UTC+4:00)Reunion Time"),
        ASIA_KABUL("Asia/Kabul","(UTC+4:00)Afghanistan Time"),
        ASIA_AQTAU("Asia/Aqtau","(UTC+5:00)Aqtau Time"),
        ASIA_AQTOBE("Asia/Aqtobe","(UTC+5:00)Aqtobe Time"),
        ASIA_ASHGABAT("Asia/Ashgabat","(UTC+5:00)Turkmenistan Time"),
        ASIA_DUSHANBE("Asia/Dushanbe","(UTC+5:00)Tajikistan Time"),
        ASIA_KARACHI("Asia/Karachi","(UTC+5:00)Pakistan Time"),
        ASIA_ORAL("Asia/Oral","(UTC+5:00)Oral Time"),
        ASIA_SAMARKAND("Asia/Samarkand","(UTC+5:00)Uzbekistan Time"),
        ASIA_YEKATERINBURG("Asia/Yekaterinburg","(UTC+5:00)Yekaterinburg Time"),
        ETC_GMT__5("Etc/GMT-5","(UTC+5:00)GMT+05:00"),
        INDIAN_KERGUELEN("Indian/Kerguelen","(UTC+5:00)French Southern & Antarctic Lands Time"),
        INDIAN_MALDIVES("Indian/Maldives","(UTC+5:00)Maldives Time"),
        ASIA_CALCUTTA("Asia/Calcutta","(UTC+5:00)India Standard Time"),
        ASIA_KATMANDU("Asia/Katmandu","(UTC+5:00)Nepal Time"),
        ANTARCTICA_MAWSON("Antarctica/Mawson","(UTC+6:00)Mawson Time"),
        ANTARCTICA_VOSTOK("Antarctica/Vostok","(UTC+6:00)Vostok Time"),
        ASIA_ALMATY("Asia/Almaty","(UTC+6:00)Alma-Ata Time"),
        ASIA_BISHKEK("Asia/Bishkek","(UTC+6:00)Kirgizstan Time"),
        ASIA_DACCA("Asia/Dacca","(UTC+6:00)Bangladesh Time"),
        ASIA_NOVOSIBIRSK("Asia/Novosibirsk","(UTC+6:00)Novosibirsk Time"),
        ASIA_OMSK("Asia/Omsk","(UTC+6:00)Omsk Time"),
        ASIA_QYZYLORDA("Asia/Qyzylorda","(UTC+6:00)Qyzylorda Time"),
        ASIA_THIMBU("Asia/Thimbu","(UTC+6:00)Bhutan Time"),
        ETC_GMT__6("Etc/GMT-6","(UTC+6:00)GMT+06:00"),
        INDIAN_CHAGOS("Indian/Chagos","(UTC+6:00)Indian Ocean Territory Time"),
        ASIA_RANGOON("Asia/Rangoon","(UTC+6:00)Myanmar Time"),
        INDIAN_COCOS("Indian/Cocos","(UTC+6:00)Cocos Islands Time"),
        ANTARCTICA_DAVIS("Antarctica/Davis","(UTC+7:00)Davis Time"),
        ASIA_BANGKOK("Asia/Bangkok","(UTC+7:00)Indochina Time"),
        ASIA_HOVD("Asia/Hovd","(UTC+7:00)Hovd Time"),
        ASIA_JAKARTA("Asia/Jakarta","(UTC+7:00)West Indonesia Time"),
        ASIA_KRASNOYARSK("Asia/Krasnoyarsk","(UTC+7:00)Krasnoyarsk Time"),
        ETC_GMT__7("Etc/GMT-7","(UTC+7:00)GMT+07:00"),
        INDIAN_CHRISTMAS("Indian/Christmas","(UTC+7:00)Christmas Island Time"),
        ANTARCTICA_CASEY("Antarctica/Casey","(UTC+8:00)Western Standard Time (Australia)"),
        ASIA_BRUNEI("Asia/Brunei","(UTC+8:00)Brunei Time"),
        ASIA_CHOIBALSAN("Asia/Choibalsan","(UTC+8:00)Choibalsan Time"),
        ASIA_SHANGHAI("Asia/Shanghai","(UTC+8:00)China Standard Time"),
        ASIA_HONG_KONG("Asia/Hong_Kong","(UTC+8:00)Hong Kong Time"),
        ASIA_IRKUTSK("Asia/Irkutsk","(UTC+8:00)Irkutsk Time"),
        ASIA_KUALA_LUMPUR("Asia/Kuala_Lumpur","(UTC+8:00)Malaysia Time"),
        ASIA_MAKASSAR("Asia/Makassar","(UTC+8:00)Central Indonesia Time"),
        ASIA_MANILA("Asia/Manila","(UTC+8:00)Philippines Time"),
        ASIA_SINGAPORE("Asia/Singapore","(UTC+8:00)Singapore Time"),
        ASIA_ULAANBAATAR("Asia/Ulaanbaatar","(UTC+8:00)Ulaanbaatar Time"),
        ETC_GMT__8("Etc/GMT-8","(UTC+8:00)GMT+08:00"),
        AUSTRALIA_EUCLA("Australia/Eucla","(UTC+8:00)Central Western Standard Time (Australia)"),
        ASIA_DILI("Asia/Dili","(UTC+9:00)Timor-Leste Time"),
        ASIA_JAYAPURA("Asia/Jayapura","(UTC+9:00)East Indonesia Time"),
        ASIA_PYONGYANG("Asia/Pyongyang","(UTC+9:00)Korea Standard Time"),
        ASIA_TOKYO("Asia/Tokyo","(UTC+9:00)Japan Standard Time"),
        ASIA_YAKUTSK("Asia/Yakutsk","(UTC+9:00)Yakutsk Time"),
        ETC_GMT__9("Etc/GMT-9","(UTC+9:00)GMT+09:00"),
        PACIFIC_PALAU("Pacific/Palau","(UTC+9:00)Palau Time"),
        ACT("ACT","(UTC+9:00)Central Standard Time (Northern Territory)"),
        AUSTRALIA_ADELAIDE("Australia/Adelaide","(UTC+9:00)Central Standard Time (South Australia)"),
        AUSTRALIA_BROKEN_HILL("Australia/Broken_Hill","(UTC+9:00)Central Standard Time (South Australia/New South Wales)"),
        AET("AET","(UTC+10:00)Eastern Standard Time (New South Wales)"),
        ANTARCTICA_DUMONTDURVILLE("Antarctica/DumontDUrville","(UTC+10:00)Dumont-d|Urville Time"),
        ASIA_SAKHALIN("Asia/Sakhalin","(UTC+10:00)Sakhalin Time"),
        ASIA_VLADIVOSTOK("Asia/Vladivostok","(UTC+10:00)Vladivostok Time"),
        AUSTRALIA_BRISBANE("Australia/Brisbane","(UTC+10:00)Eastern Standard Time (Queensland)"),
        AUSTRALIA_HOBART("Australia/Hobart","(UTC+10:00)Eastern Standard Time (Tasmania)"),
        AUSTRALIA_MELBOURNE("Australia/Melbourne","(UTC+10:00)Eastern Standard Time (Victoria)"),
        ETC_GMT__10("Etc/GMT-10","(UTC+10:00)GMT+10:00"),
        PACIFIC_GUAM("Pacific/Guam","(UTC+10:00)Chamorro Standard Time"),
        PACIFIC_PORT_MORESBY("Pacific/Port_Moresby","(UTC+10:00)Papua New Guinea Time"),
        PACIFIC_TRUK("Pacific/Truk","(UTC+10:00)Truk Time"),
        AUSTRALIA_LHI("Australia/LHI","(UTC+10:00)Lord Howe Standard Time"),
        ASIA_MAGADAN("Asia/Magadan","(UTC+11:00)Magadan Time"),
        ETC_GMT__11("Etc/GMT-11","(UTC+11:00)GMT+11:00"),
        PACIFIC_EFATE("Pacific/Efate","(UTC+11:00)Vanuatu Time"),
        PACIFIC_GUADALCANAL("Pacific/Guadalcanal","(UTC+11:00)Solomon Is. Time"),
        PACIFIC_KOSRAE("Pacific/Kosrae","(UTC+11:00)Kosrae Time"),
        PACIFIC_NOUMEA("Pacific/Noumea","(UTC+11:00)New Caledonia Time"),
        PACIFIC_PONAPE("Pacific/Ponape","(UTC+11:00)Ponape Time"),
        PACIFIC_NORFOLK("Pacific/Norfolk","(UTC+11:00)Norfolk Time"),
        ANTARCTICA_MCMURDO("Antarctica/McMurdo","(UTC+12:00)New Zealand Standard Time"),
        ASIA_ANADYR("Asia/Anadyr","(UTC+12:00)Anadyr Time"),
        ASIA_KAMCHATKA("Asia/Kamchatka","(UTC+12:00)Petropavlovsk-Kamchatski Time"),
        ETC_GMT__12("Etc/GMT-12","(UTC+12:00)GMT+12:00"),
        KWAJALEIN("Kwajalein","(UTC+12:00)Marshall Islands Time"),
        PACIFIC_FIJI("Pacific/Fiji","(UTC+12:00)Fiji Time"),
        PACIFIC_FUNAFUTI("Pacific/Funafuti","(UTC+12:00)Tuvalu Time"),
        PACIFIC_NAURU("Pacific/Nauru","(UTC+12:00)Nauru Time"),
        PACIFIC_TARAWA("Pacific/Tarawa","(UTC+12:00)Gilbert Is. Time"),
        PACIFIC_WAKE("Pacific/Wake","(UTC+12:00)Wake Time"),
        PACIFIC_WALLIS("Pacific/Wallis","(UTC+12:00)Wallis & Futuna Time"),
        NZ_SUB_CHAT("NZ-CHAT","(UTC+12:00)Chatham Standard Time"),
        ETC_GMT__13("Etc/GMT-13","(UTC+13:00)GMT+13:00"),
        PACIFIC_ENDERBURY("Pacific/Enderbury","(UTC+13:00)Phoenix Is. Time"),
        PACIFIC_TONGATAPU("Pacific/Tongatapu","(UTC+13:00)Tonga Time"),
        ETC_GMT__14("Etc/GMT-14","(UTC+14:00)GMT+14:00"),
        PACIFIC_KIRITIMATI("Pacific/Kiritimati","(UTC+14:00)Line Is. Time");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList59(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PAYMENT_TRANSACTION__TYPE]
     */
    @Getter
    public enum PAYMENT_TRANSACTION__TYPE {
        VALIDATION("validation","银行卡验证"),
        SERVER2SERVER("server2server","服务器到服务器"),
        FORM("form","表单"),
        FORM_SAVE("form_save","使用标记化技术的表单");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_TRANSACTION__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[PRODUCT_PRICELIST_ITEM__APPLIED_ON]
     */
    @Getter
    public enum PRODUCT_PRICELIST_ITEM__APPLIED_ON {
        ITEM_1("3_global","所有产品"),
        ITEM_2("2_product_category","产品类别"),
        ITEM_3("1_product","产品"),
        ITEM_4("0_product_variant","产品变体");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_PRICELIST_ITEM__APPLIED_ON(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_PARTNER__INVOICE_WARN]
     */
    @Getter
    public enum RES_PARTNER__INVOICE_WARN {
        NO_SUB_MESSAGE("no-message","没消息"),
        WARNING("warning","警告"),
        BLOCK("block","受阻消息");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__INVOICE_WARN(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[CALENDAR_EVENT__STATE]
     */
    @Getter
    public enum CALENDAR_EVENT__STATE {
        DRAFT("draft","未确认"),
        OPEN("open","已确认");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_ACCOUNT_TYPE__INTERNAL_GROUP]
     */
    @Getter
    public enum ACCOUNT_ACCOUNT_TYPE__INTERNAL_GROUP {
        EQUITY("equity","权益"),
        ASSET("asset","资产"),
        LIABILITY("liability","负债"),
        INCOME("income","收入"),
        EXPENSE("expense","费用"),
        OFF_BALANCE("off_balance","不平衡");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_ACCOUNT_TYPE__INTERNAL_GROUP(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[输入辅助_图表参数]
     */
    @Getter
    public enum CodeList104 {
        CARETTEMPLGROUP_SRFREPORT_CHARTDATA("CARETTEMPLGROUP_SRFREPORT_CHARTDATA","图表数据定义"),
        CARETTEMPLGROUP_SRFREPORT_CHART("CARETTEMPLGROUP_SRFREPORT_CHART","图表表现定义");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList104(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[FLEET_VEHICLE__ACTIVITY_STATE]
     */
    @Getter
    public enum FLEET_VEHICLE__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[民族（公安部身份证）]
     */
    @Getter
    public enum CodeList99 {
        ITEM_01("01","汉"),
        ITEM_02("02","蒙古"),
        ITEM_03("03","回"),
        ITEM_04("04","藏"),
        ITEM_05("05","维吾尔"),
        ITEM_06("06","苗"),
        ITEM_07("07","彝"),
        ITEM_08("08","壮"),
        ITEM_09("09","布依"),
        ITEM_10("10","朝鲜"),
        ITEM_11("11","满"),
        ITEM_12("12","侗"),
        ITEM_13("13","瑶"),
        ITEM_14("14","白"),
        ITEM_15("15","土家"),
        ITEM_16("16","哈尼"),
        ITEM_17("17","哈萨克"),
        ITEM_18("18","傣"),
        ITEM_19("19","黎"),
        ITEM_20("20","傈僳"),
        ITEM_21("21","佤"),
        ITEM_22("22","畲"),
        ITEM_23("23","高山"),
        ITEM_24("24","拉祜"),
        ITEM_25("25","水"),
        ITEM_26("26","东乡"),
        ITEM_27("27","纳西"),
        ITEM_28("28","景颇"),
        ITEM_29("29","柯尔克孜"),
        ITEM_30("30","土"),
        ITEM_31("31","达斡尔"),
        ITEM_32("32","仫佬"),
        ITEM_33("33","羌"),
        ITEM_34("34","布朗"),
        ITEM_35("35","撒拉"),
        ITEM_36("36","毛南"),
        ITEM_37("37","仡佬"),
        ITEM_38("38","锡伯"),
        ITEM_39("39","阿昌"),
        ITEM_40("40","普米"),
        ITEM_41("41","塔吉克"),
        ITEM_42("42","怒"),
        ITEM_43("43","乌孜别克"),
        ITEM_44("44","俄罗斯"),
        ITEM_45("45","鄂温克"),
        ITEM_46("46","德昂"),
        ITEM_47("47","保安"),
        ITEM_48("48","裕固"),
        ITEM_49("49","京"),
        ITEM_50("50","塔塔尔"),
        ITEM_51("51","独龙"),
        ITEM_52("52","鄂伦春"),
        ITEM_53("53","赫哲"),
        ITEM_54("54","门巴"),
        ITEM_55("55","珞巴"),
        ITEM_56("56","基诺");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList99(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[主实体删除关系实体操作]
     */
    @Getter
    public enum CodeList21 {
        ITEM_1("1","同时删除"),
        ITEM_2("2","置空"),
        ITEM_3("3","限制删除");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList21(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体数据处理_数据操作]
     */
    @Getter
    public enum CodeList108 {
        INSERT("INSERT","新建"),
        UPDATE("UPDATE","更新"),
        SAVE("SAVE","保存（自动判断）"),
        DELETE("DELETE","删除"),
        CUSTOMCALL("CUSTOMCALL","自定义调用"),
        CUSTOMPROCCALL("CUSTOMPROCCALL","自定义存储过程调用"),
        CUSTOMRAWPROCCALL("CUSTOMRAWPROCCALL","自定义存储过程调用（全称）"),
        GET("GET","获取(GET)"),
        CHECKKEYSTATE("CHECKKEYSTATE","检查主键状态(CHECKKEYSTATE)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList108(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_PARTNER__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum RES_PARTNER__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_CASH_ROUNDING__STRATEGY]
     */
    @Getter
    public enum ACCOUNT_CASH_ROUNDING__STRATEGY {
        BIGGEST_TAX("biggest_tax","修改税率金额"),
        ADD_INVOICE_LINE("add_invoice_line","舍入添加取整明细");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_CASH_ROUNDING__STRATEGY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[前端展现技术]
     */
    @Getter
    public enum CodeList96 {
        HTML("HTML","HTML"),
        SL("SL","SilverLight"),
        WINRT("WinRT","WinRT"),
        ANDROID("Android","Android"),
        IOS("IOS","IOS");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList96(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体规则处理_数据类型]
     */
    @Getter
    public enum CodeList112 {
        VARCHAR("VARCHAR","文本"),
        INT("INT","整形"),
        FLOAT("FLOAT","浮点"),
        DATETIME("DATETIME","日期");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList112(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_MOVE__TYPE]
     */
    @Getter
    public enum ACCOUNT_MOVE__TYPE {
        ENTRY("entry","日记账分录"),
        OUT_INVOICE("out_invoice","客户发票"),
        OUT_REFUND("out_refund","客户贷项发票"),
        IN_INVOICE("in_invoice","供应商账单"),
        IN_REFUND("in_refund","供应商信用票"),
        OUT_RECEIPT("out_receipt","销售收据"),
        IN_RECEIPT("in_receipt","采购收据");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_MOVE__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_TAX__AMOUNT_TYPE]
     */
    @Getter
    public enum ACCOUNT_TAX__AMOUNT_TYPE {
        GROUP("group","税组"),
        FIXED("fixed","固定"),
        PERCENT("percent","价格百分比"),
        DIVISION("division","含税价格百分比");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_TAX__AMOUNT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[实体关系明细类型]
     */
    @Getter
    public enum CodeList14 {
        PAGE("PAGE","内建页面"),
        PAGEPATH("PAGEPATH","页面路径"),
        DER1N("DER1N","1:N关系"),
        DER11("DER11","1:1关系"),
        WFSTEP("WFSTEP","工作流处理步骤"),
        WFSTEPACTOR("WFSTEPACTOR","工作流当前处理用户"),
        FILELIST("FILELIST","附件列表"),
        DATAAUDIT("DATAAUDIT","行为审计"),
        DERTYPE("DERTYPE","实体关系分组");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList14(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[BASE_LANGUAGE_INSTALL__STATE]
     */
    @Getter
    public enum BASE_LANGUAGE_INSTALL__STATE {
        INIT("init","初始化"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        BASE_LANGUAGE_INSTALL__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[EVENT_TYPE_MAIL__INTERVAL_TYPE]
     */
    @Getter
    public enum EVENT_TYPE_MAIL__INTERVAL_TYPE {
        AFTER_SUB("after_sub","注册之后"),
        BEFORE_EVENT("before_event","在事件之前"),
        AFTER_EVENT("after_event","事件之后");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_TYPE_MAIL__INTERVAL_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL__MATCH_NATURE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL__MATCH_NATURE {
        AMOUNT_RECEIVED("amount_received","收入金额"),
        AMOUNT_PAID("amount_paid","已付金额"),
        BOTH("both","收/付金额");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL__MATCH_NATURE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MRP_BOM__CONSUMPTION]
     */
    @Getter
    public enum MRP_BOM__CONSUMPTION {
        STRICT("strict","Strict"),
        FLEXIBLE("flexible","Flexible");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_BOM__CONSUMPTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PRODUCT_TEMPLATE__TYPE]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__TYPE {
        CONSU("consu","可消耗"),
        SERVICE("service","服务"),
        PRODUCT("product","可库存产品");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_ALLOCATION__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum HR_LEAVE_ALLOCATION__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_ALLOCATION__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_MOVE_REVERSAL__REFUND_METHOD]
     */
    @Getter
    public enum ACCOUNT_MOVE_REVERSAL__REFUND_METHOD {
        REFUND("refund","部分退款"),
        CANCEL("cancel","全部退款"),
        MODIFY("modify","全部退款并创建新的草稿发票");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_MOVE_REVERSAL__REFUND_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[LUNCH_SUPPLIER__ACTIVITY_STATE]
     */
    @Getter
    public enum LUNCH_SUPPLIER__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_SUPPLIER__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[合同状态快速分组]
     */
    @Getter
    public enum HR_CONTRACT__STATE_GridGrouping {
        ALL("all","所有"),
        DRAFT("draft","新建"),
        OPEN("open","运行中"),
        CLOSE("close","过期"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_CONTRACT__STATE_GridGrouping(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_PICKING_TYPE__CODE]
     */
    @Getter
    public enum STOCK_PICKING_TYPE__CODE {
        INCOMING("incoming","入库"),
        OUTGOING("outgoing","交货"),
        INTERNAL("internal","内部调拨"),
        MRP_OPERATION("mrp_operation","制造");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PICKING_TYPE__CODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[本地语言]
     */
    @Getter
    public enum CodeList56 {
        EN("EN","英文"),
        ZH_CN("ZH_CN","中文简体"),
        ZH_TW("ZH_TW","中文繁体（台湾）");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList56(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_1]
     */
    @Getter
    public enum LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_1 {
        NONE_OR_MORE("0_more","没有或更多"),
        ONE_OR_MORE("1_more","一个或多个"),
        ONE("1","只有一个");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_PRODUCT_CATEGORY__TOPPING_QUANTITY_1(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[EVENT_MAIL__NOTIFICATION_TYPE]
     */
    @Getter
    public enum EVENT_MAIL__NOTIFICATION_TYPE {
        MAIL("mail","邮件"),
        SMS("sms","短信");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_MAIL__NOTIFICATION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_ATTRIBUTE__DISPLAY_TYPE]
     */
    @Getter
    public enum PRODUCT_ATTRIBUTE__DISPLAY_TYPE {
        RADIO("radio","单选"),
        SELECT("select","选择"),
        COLOR("color","颜色");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_ATTRIBUTE__DISPLAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COUNTRY__NAME_POSITION]
     */
    @Getter
    public enum RES_COUNTRY__NAME_POSITION {
        BEFORE("before","设置在地址前面"),
        AFTER("after","设置在地址后面");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COUNTRY__NAME_POSITION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PRINT_JOURNAL__TARGET_MOVE]
     */
    @Getter
    public enum ACCOUNT_PRINT_JOURNAL__TARGET_MOVE {
        POSTED("posted","所有已过账分录"),
        ALL("all","所有分录");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PRINT_JOURNAL__TARGET_MOVE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MRP_WORKORDER__CONSUMPTION]
     */
    @Getter
    public enum MRP_WORKORDER__CONSUMPTION {
        STRICT("strict","Strict"),
        FLEXIBLE("flexible","Flexible");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_WORKORDER__CONSUMPTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[SALE_REPORT__STATE]
     */
    @Getter
    public enum SALE_REPORT__STATE {
        DRAFT("draft","草稿报价单"),
        SENT("sent","报价已发送"),
        SALE("sale","销售订单"),
        DONE("done","销售完成"),
        CANCEL("cancel","已取消"),
        POS_DRAFT("pos_draft","新建"),
        PAID("paid","已支付"),
        POS_DONE("pos_done","已过账"),
        INVOICED("invoiced","已开票");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_REPORT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_PARTNER__TYPE]
     */
    @Getter
    public enum RES_PARTNER__TYPE {
        CONTACT("contact","联系人"),
        INVOICE("invoice","发票地址"),
        DELIVERY("delivery","送货地址"),
        OTHER("other","其他地址"),
        PRIVATE("private","家庭住址");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_CASH_ROUNDING__ROUNDING_METHOD]
     */
    @Getter
    public enum ACCOUNT_CASH_ROUNDING__ROUNDING_METHOD {
        UP("UP","向上"),
        DOWN("DOWN","向下"),
        HALF_SUB_UP("HALF-UP","一半");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_CASH_ROUNDING__ROUNDING_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__DEFAULT_PICKING_POLICY]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__DEFAULT_PICKING_POLICY {
        DIRECT("direct","尽快运送产品，并退回订单"),
        ONE("one","一次运送所有产品");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__DEFAULT_PICKING_POLICY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[输入辅助_页面参数]
     */
    @Getter
    public enum CodeList100 {
        CARETTEMPLGROUP_SRFDA_PAGEPARAM("CARETTEMPLGROUP_SRFDA_PAGEPARAM","常规页面参数"),
        CARETTEMPLGROUP_SRFDA_GRIDVIEWPAGEPARAM("CARETTEMPLGROUP_SRFDA_GRIDVIEWPAGEPARAM","表格页面参数"),
        CARETTEMPLGROUP_SRFDA_EDITVIEWPAGEPARAM("CARETTEMPLGROUP_SRFDA_EDITVIEWPAGEPARAM","编辑页面参数");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList100(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RESOURCE_RESOURCE__RESOURCE_TYPE]
     */
    @Getter
    public enum RESOURCE_RESOURCE__RESOURCE_TYPE {
        USER("user","人员"),
        MATERIAL("material","物料");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RESOURCE_RESOURCE__RESOURCE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_TEMPLATE__ACTIVITY_STATE]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[STOCK_PRODUCTION_LOT__ACTIVITY_STATE]
     */
    @Getter
    public enum STOCK_PRODUCTION_LOT__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PRODUCTION_LOT__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum HR_EMPLOYEE__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL__MATCH_AMOUNT]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL__MATCH_AMOUNT {
        LOWER("lower","大于"),
        GREATER("greater","大于"),
        BETWEEN("between","介于");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL__MATCH_AMOUNT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_BANK_STATEMENT__STATE]
     */
    @Getter
    public enum ACCOUNT_BANK_STATEMENT__STATE {
        OPEN("open","新建"),
        CONFIRM("confirm","已验证");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_BANK_STATEMENT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_ADVANCE_PAYMENT_INV__ADVANCE_PAYMENT_METHOD]
     */
    @Getter
    public enum SALE_ADVANCE_PAYMENT_INV__ADVANCE_PAYMENT_METHOD {
        DELIVERED("delivered","普通发票"),
        PERCENTAGE("percentage","预付款(百分比)"),
        FIXED("fixed","预付款(固定金额)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ADVANCE_PAYMENT_INV__ADVANCE_PAYMENT_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_JOURNAL__ACTIVITY_STATE]
     */
    @Getter
    public enum ACCOUNT_JOURNAL__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_JOURNAL__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[RESOURCE_CALENDAR_ATTENDANCE__DAY_PERIOD]
     */
    @Getter
    public enum RESOURCE_CALENDAR_ATTENDANCE__DAY_PERIOD {
        MORNING("morning","上午"),
        AFTERNOON("afternoon","下午");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RESOURCE_CALENDAR_ATTENDANCE__DAY_PERIOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }






    /**
     * 代码表[HR_EMPLOYEE_BASE__ATTENDANCE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE_BASE__ATTENDANCE_STATE {
        CHECKED_OUT("checked_out","签出"),
        CHECKED_IN("checked_in","签到");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE_BASE__ATTENDANCE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PAYMENT_TERM_LINE__VALUE]
     */
    @Getter
    public enum ACCOUNT_PAYMENT_TERM_LINE__VALUE {
        BALANCE("balance","余额"),
        PERCENT("percent","百分比"),
        FIXED("fixed","固定金额");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT_TERM_LINE__VALUE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[BASE_MODULE_UPDATE__STATE]
     */
    @Getter
    public enum BASE_MODULE_UPDATE__STATE {
        INIT("init","初始化"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        BASE_MODULE_UPDATE__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[性能分析指标]
     */
    @Getter
    public enum CodeList117 {
        PODBACTION("PODBACTION","数据库操作性能"),
        PODBQUERY("PODBQUERY","数据库查询性能"),
        PODEDC("PODEDC","实体处理逻辑性能"),
        POWORKFLOW("POWORKFLOW","工作流性能"),
        POPAGE("POPAGE","页面性能"),
        POPAGEBACKEND("POPAGEBACKEND","页面性能(后台)"),
        POPAGESESSION("POPAGESESSION","并发性能");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList117(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[NOTE_NOTE__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum NOTE_NOTE__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        NOTE_NOTE__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_ACCOUNT_TYPE__TYPE]
     */
    @Getter
    public enum ACCOUNT_ACCOUNT_TYPE__TYPE {
        OTHER("other","常规科目"),
        RECEIVABLE("receivable","应收"),
        PAYABLE("payable","应付"),
        LIQUIDITY("liquidity","流动性");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_ACCOUNT_TYPE__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_DOCUMENT__PRIORITY]
     */
    @Getter
    public enum MRP_DOCUMENT__PRIORITY {
        ITEM_0("0","一般"),
        ITEM_1("1","低"),
        ITEM_2("2","高"),
        ITEM_3("3","非常高");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_DOCUMENT__PRIORITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[GAMIFICATION_CHALLENGE__REPORT_MESSAGE_FREQUENCY]
     */
    @Getter
    public enum GAMIFICATION_CHALLENGE__REPORT_MESSAGE_FREQUENCY {
        NEVER("never","从不"),
        ONCHANGE("onchange","改变"),
        DAILY("daily","每天"),
        WEEKLY("weekly","每周"),
        MONTHLY("monthly","每月"),
        YEARLY("yearly","每年");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_CHALLENGE__REPORT_MESSAGE_FREQUENCY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据库触发器事件]
     */
    @Getter
    public enum CodeList66 {
        INSERT("INSERT","Insert"),
        UPDATE("UPDATE","Update"),
        DELETE("DELETE","Delete");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList66(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_TEMPLATE__PURCHASE_LINE_WARN]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__PURCHASE_LINE_WARN {
        NO_SUB_MESSAGE("no-message","没消息"),
        WARNING("warning","警告"),
        BLOCK("block","受阻消息");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__PURCHASE_LINE_WARN(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[是否]
     */
    @Getter
    public enum YesNo {
        ITEM_1("1","是"),
        ITEM_0("0","否");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        YesNo(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[GAMIFICATION_GOAL__STATE]
     */
    @Getter
    public enum GAMIFICATION_GOAL__STATE {
        DRAFT("draft","草稿"),
        INPROGRESS("inprogress","进行中"),
        REACHED("reached","达到"),
        FAILED("failed","失败的"),
        CANCELED("canceled","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_GOAL__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE_BASE__CURRENT_LEAVE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE_BASE__CURRENT_LEAVE_STATE {
        DRAFT("draft","新建"),
        CONFIRM("confirm","等待审批"),
        REFUSE("refuse","已拒绝"),
        VALIDATE1("validate1","等待第二次审批"),
        VALIDATE("validate","已批准"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE_BASE__CURRENT_LEAVE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[RES_PARTNER__ACTIVITY_STATE]
     */
    @Getter
    public enum RES_PARTNER__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_COMPANY__ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE]
     */
    @Getter
    public enum RES_COMPANY__ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__PAYMENT_ONBOARDING_PAYMENT_METHOD]
     */
    @Getter
    public enum RES_COMPANY__PAYMENT_ONBOARDING_PAYMENT_METHOD {
        PAYPAL("paypal","PayPal"),
        STRIPE("stripe","条纹"),
        MANUAL("manual","手动"),
        OTHER("other","其他");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__PAYMENT_ONBOARDING_PAYMENT_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[属性预定义值规则]
     */
    @Getter
    public enum CodeList13 {
        INT("INT","整数"),
        POSITIVEINT("POSITIVEINT","正整数"),
        STRING("STRING","字符串"),
        STRING_EMAIL("STRING_EMAIL","电子邮件"),
        FLOAT("FLOAT","浮点数"),
        FLOAT_PERCENT("FLOAT_PERCENT","百分比数值(0~100)"),
        DATETIME("DATETIME","日期时间"),
        DATETIME_GTNOW("DATETIME_GTNOW","大于当天时间"),
        DATETIME_GTNOWNOHOUR("DATETIME_GTNOWNOHOUR","大于当天日期"),
        DATETIME_GTNOW3DAY("DATETIME_GTNOW3DAY","后3天");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList13(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MRP_WORKCENTER__WORKING_STATE]
     */
    @Getter
    public enum MRP_WORKCENTER__WORKING_STATE {
        NORMAL("normal","一般"),
        BLOCKED("blocked","阻塞"),
        DONE("done","进行中");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_WORKCENTER__WORKING_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD]
     */
    @Getter
    public enum SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD {
        DIGITAL_SIGNATURE("digital_signature","在线签名"),
        PAYPAL("paypal","PayPal"),
        STRIPE("stripe","信用卡（通过 Stripe)"),
        OTHER("other","其他支付收购方"),
        MANUAL("manual","自定义付款说明");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_PAYMENT_ACQUIRER_ONBOARDING_WIZARD__PAYMENT_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_PICKING__PRIORITY]
     */
    @Getter
    public enum STOCK_PICKING__PRIORITY {
        ITEM_0("0","不紧急"),
        ITEM_1("1","一般"),
        ITEM_2("2","紧急"),
        ITEM_3("3","非常紧急");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PICKING__PRIORITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[报表输出格式]
     */
    @Getter
    public enum CodeList54 {
        PDF("PDF","PDF"),
        EXCEL("EXCEL","EXCEL"),
        HTML("HTML","HTML");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList54(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[EVENT_REGISTRATION__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum EVENT_REGISTRATION__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_REGISTRATION__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[FETCHMAIL_SERVER__STATE]
     */
    @Getter
    public enum FETCHMAIL_SERVER__STATE {
        DRAFT("draft","未确认"),
        DONE("done","已确认");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FETCHMAIL_SERVER__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_DEPARTURE_WIZARD__DEPARTURE_REASON]
     */
    @Getter
    public enum HR_DEPARTURE_WIZARD__DEPARTURE_REASON {
        FIRED("fired","被解雇"),
        RESIGNED("resigned","已辞职"),
        RETIRED("retired","已退休");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_DEPARTURE_WIZARD__DEPARTURE_REASON(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RESOURCE_CALENDAR_ATTENDANCE__DAYOFWEEK]
     */
    @Getter
    public enum RESOURCE_CALENDAR_ATTENDANCE__DAYOFWEEK {
        ITEM_0("0","周一"),
        ITEM_1("1","周二"),
        ITEM_2("2","周三"),
        ITEM_3("3","周四"),
        ITEM_4("4","周五"),
        ITEM_5("5","周六"),
        ITEM_6("6","周日");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RESOURCE_CALENDAR_ATTENDANCE__DAYOFWEEK(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[GAMIFICATION_CHALLENGE__CATEGORY]
     */
    @Getter
    public enum GAMIFICATION_CHALLENGE__CATEGORY {
        HR("hr","人力资源 / 聘用"),
        OTHER("other","设置 / 积分制管理"),
        CERTIFICATION("certification","认证"),
        SLIDES("slides","网站/幻灯片"),
        FORUM("forum","网站/论坛");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_CHALLENGE__CATEGORY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EXPENSE__ACTIVITY_STATE]
     */
    @Getter
    public enum HR_EXPENSE__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EXPENSE__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_TAX__TAX_EXIGIBILITY]
     */
    @Getter
    public enum ACCOUNT_TAX__TAX_EXIGIBILITY {
        ON_INVOICE("on_invoice","开票基于"),
        ON_PAYMENT("on_payment","基于付款");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_TAX__TAX_EXIGIBILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[FLEET_VEHICLE_COST__COST_TYPE]
     */
    @Getter
    public enum FLEET_VEHICLE_COST__COST_TYPE {
        CONTRACT("contract","合同"),
        SERVICES("services","服务"),
        FUEL("fuel","燃油"),
        OTHER("other","其他");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FLEET_VEHICLE_COST__COST_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE__REQUEST_HOUR_FROM]
     */
    @Getter
    public enum HR_LEAVE__REQUEST_HOUR_FROM {
        ITEM_0("0","12:00 AM"),
        ITEM_0_5("0.5","0:30 AM"),
        ITEM_1("1","1:00 AM"),
        ITEM_1_5("1.5","1:30 AM"),
        ITEM_2("2","2:00 AM"),
        ITEM_2_5("2.5","2:30 AM"),
        ITEM_3("3","3:00 AM"),
        ITEM_3_5("3.5","3:30 AM"),
        ITEM_4("4","4:00 AM"),
        ITEM_4_5("4.5","4:30 AM"),
        ITEM_5("5","5:00 AM"),
        ITEM_5_5("5.5","5:30 AM"),
        ITEM_6("6","6:00 AM"),
        ITEM_6_5("6.5","6:30 AM"),
        ITEM_7("7","7:00 AM"),
        ITEM_7_5("7.5","7:30 AM"),
        ITEM_8("8","8:00 AM"),
        ITEM_8_5("8.5","8:30 AM"),
        ITEM_9("9","9:00 AM"),
        ITEM_9_5("9.5","9:30 AM"),
        ITEM_10("10","10:00 AM"),
        ITEM_10_5("10.5","10:30 AM"),
        ITEM_11("11","11:00 AM"),
        ITEM_11_5("11.5","11:30 AM"),
        ITEM_12("12","12:00 PM"),
        ITEM_12_5("12.5","0:30 PM"),
        ITEM_13("13","1:00 PM"),
        ITEM_13_5("13.5","1:30 PM"),
        ITEM_14("14","2:00 PM"),
        ITEM_14_5("14.5","2:30 PM"),
        ITEM_15("15","3:00 PM"),
        ITEM_15_5("15.5","3:30 PM"),
        ITEM_16("16","4:00 PM"),
        ITEM_16_5("16.5","4:30 PM"),
        ITEM_17("17","5:00 PM"),
        ITEM_17_5("17.5","5:30 PM"),
        ITEM_18("18","6:00 PM"),
        ITEM_18_5("18.5","6:30 PM"),
        ITEM_19("19","7:00 PM"),
        ITEM_19_5("19.5","7:30 PM"),
        ITEM_20("20","8:00 PM"),
        ITEM_20_5("20.5","8:30 PM"),
        ITEM_21("21","9:00 PM"),
        ITEM_21_5("21.5","9:30 PM"),
        ITEM_22("22","10:00 PM"),
        ITEM_22_5("22.5","10:30 PM"),
        ITEM_23("23","11:00 PM"),
        ITEM_23_5("23.5","11:30 PM");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE__REQUEST_HOUR_FROM(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__SALE_ONBOARDING_SAMPLE_QUOTATION_STATE]
     */
    @Getter
    public enum RES_COMPANY__SALE_ONBOARDING_SAMPLE_QUOTATION_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__SALE_ONBOARDING_SAMPLE_QUOTATION_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_CONTRACT__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum HR_CONTRACT__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_CONTRACT__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_REQUISITION_TYPE__EXCLUSIVE]
     */
    @Getter
    public enum PURCHASE_REQUISITION_TYPE__EXCLUSIVE {
        EXCLUSIVE("exclusive","仅选择一个询价单(单一供应商)"),
        MULTIPLE("multiple","选择多个询价单");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_REQUISITION_TYPE__EXCLUSIVE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_PRODUCTION__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum MRP_PRODUCTION__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_PRODUCTION__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[报表分类]
     */
    @Getter
    public enum CodeList12 {
        REPORTFOLDER_1("REPORTFOLDER_1","经营性报表"),
        REPORTFOLDER_2("REPORTFOLDER_2","财务报表");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList12(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[EVENT_EVENT__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum EVENT_EVENT__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_EVENT__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }






    /**
     * 代码表[用户数据行为]
     */
    @Getter
    public enum CodeList71 {
        ITEM_1("1","无建立"),
        ITEM_2("2","无更新"),
        ITEM_4("4","无删除"),
        ITEM_8("8","无查看");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList71(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__SHOW_LINE_SUBTOTALS_TAX_SELECTION]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__SHOW_LINE_SUBTOTALS_TAX_SELECTION {
        TAX_EXCLUDED("tax_excluded","不含税"),
        TAX_INCLUDED("tax_included","含税");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__SHOW_LINE_SUBTOTALS_TAX_SELECTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL_TEMPLATE__SECOND_AMOUNT_TYPE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL_TEMPLATE__SECOND_AMOUNT_TYPE {
        FIXED("fixed","固定"),
        PERCENTAGE("percentage","金额的百分比"),
        REGEX("regex","来自标签");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL_TEMPLATE__SECOND_AMOUNT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据通知监控行为（新建、更新、删除）]
     */
    @Getter
    public enum CodeList98 {
        ITEM_1("1","新建"),
        ITEM_2("2","更新"),
        ITEM_4("4","删除");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList98(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_ORDER__ACTIVITY_STATE]
     */
    @Getter
    public enum SALE_ORDER__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[开发帮助重要程度]
     */
    @Getter
    public enum CodeList31 {
        LOW("LOW","低"),
        NORMAIL("NORMAIL","中"),
        HIGH("HIGH","高");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList31(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[STOCK_INVENTORY__PREFILL_COUNTED_QUANTITY]
     */
    @Getter
    public enum STOCK_INVENTORY__PREFILL_COUNTED_QUANTITY {
        COUNTED("counted","默认为在手库存"),
        ZERO("zero","默认为零");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_INVENTORY__PREFILL_COUNTED_QUANTITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[LUNCH_SUPPLIER__MOMENT]
     */
    @Getter
    public enum LUNCH_SUPPLIER__MOMENT {
        AM("am","上午"),
        PM("pm","下午");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        LUNCH_SUPPLIER__MOMENT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_BOM__TYPE]
     */
    @Getter
    public enum MRP_BOM__TYPE {
        NORMAL("normal","制造此产品"),
        PHANTOM("phantom","套件");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_BOM__TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[REPAIR_ORDER__INVOICE_METHOD]
     */
    @Getter
    public enum REPAIR_ORDER__INVOICE_METHOD {
        NONE("none","不开票"),
        B4REPAIR("b4repair","维修前"),
        AFTER_REPAIR("after_repair","维修后");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        REPAIR_ORDER__INVOICE_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_LEAVE_ALLOCATION__INTERVAL_UNIT]
     */
    @Getter
    public enum HR_LEAVE_ALLOCATION__INTERVAL_UNIT {
        WEEKS("weeks","周"),
        MONTHS("months","月"),
        YEARS("years","年");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_ALLOCATION__INTERVAL_UNIT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_TYPE__TIME_TYPE]
     */
    @Getter
    public enum HR_LEAVE_TYPE__TIME_TYPE {
        LEAVE("leave","休息时间"),
        OTHER("other","其他");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_TYPE__TIME_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EXPENSE_SHEET__STATE]
     */
    @Getter
    public enum HR_EXPENSE_SHEET__STATE {
        DRAFT("draft","草稿"),
        SUBMIT("submit","发送"),
        APPROVE("approve","已批准"),
        POST("post","已过账"),
        DONE("done","已支付"),
        CANCEL("cancel","已拒绝");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EXPENSE_SHEET__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE__ATTENDANCE_STATE]
     */
    @Getter
    public enum HR_EMPLOYEE__ATTENDANCE_STATE {
        CHECKED_OUT("checked_out","签出"),
        CHECKED_IN("checked_in","签到");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__ATTENDANCE_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_MOVE__ACTIVITY_STATE]
     */
    @Getter
    public enum ACCOUNT_MOVE__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_MOVE__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[CALENDAR_EVENT__END_TYPE]
     */
    @Getter
    public enum CALENDAR_EVENT__END_TYPE {
        COUNT("count","收件人数量"),
        END_DATE("end_date","终止日期");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__END_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAINTENANCE_EQUIPMENT__ACTIVITY_STATE]
     */
    @Getter
    public enum MAINTENANCE_EQUIPMENT__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAINTENANCE_EQUIPMENT__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_ABSTRACT_WORKORDER__CONSUMPTION]
     */
    @Getter
    public enum MRP_ABSTRACT_WORKORDER__CONSUMPTION {
        STRICT("strict","Strict"),
        FLEXIBLE("flexible","Flexible");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_ABSTRACT_WORKORDER__CONSUMPTION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_TRACK_LINE__TRACKING]
     */
    @Getter
    public enum STOCK_TRACK_LINE__TRACKING {
        LOT("lot","使用批次进行追踪"),
        SERIAL("serial","使用序列号进行追踪");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_TRACK_LINE__TRACKING(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[HR_LEAVE_REPORT__STATE]
     */
    @Getter
    public enum HR_LEAVE_REPORT__STATE {
        DRAFT("draft","待提交"),
        CANCEL("cancel","已取消"),
        CONFIRM("confirm","待批准"),
        REFUSE("refuse","已拒绝"),
        VALIDATE1("validate1","第二次审批"),
        VALIDATE("validate","已批准");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_REPORT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[GAMIFICATION_CHALLENGE__STATE]
     */
    @Getter
    public enum GAMIFICATION_CHALLENGE__STATE {
        DRAFT("draft","草稿"),
        INPROGRESS("inprogress","进行中"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_CHALLENGE__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[REPAIR_ORDER__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum REPAIR_ORDER__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        REPAIR_ORDER__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[页面样式]
     */
    @Getter
    public enum CodeList22 {
        ITEM_0("0","跳转处理界面"),
        ITEM_1("1","数据编辑界面"),
        ITEM_2("2","数据表格界面"),
        ITEM_9("9","数据树形界面"),
        ITEM_3("3","数据选择界面"),
        ITEM_4("4","工作流数据编辑界面"),
        ITEM_5("5","工作流表格界面"),
        ITEM_6("6","工作流管理表格界面"),
        ITEM_7("7","导航界面"),
        ITEM_8("8","信息展示界面");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList22(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[UOM_CATEGORY__MEASURE_TYPE]
     */
    @Getter
    public enum UOM_CATEGORY__MEASURE_TYPE {
        UNIT("unit","默认计数"),
        WEIGHT("weight","默认重量"),
        WORKING_TIME("working_time","默认工作时间"),
        LENGTH("length","默认长度"),
        VOLUME("volume","默认体积");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        UOM_CATEGORY__MEASURE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_SURVEY__ACTIVITY_STATE]
     */
    @Getter
    public enum SURVEY_SURVEY__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_SURVEY__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[页面资源类型]
     */
    @Getter
    public enum CodeList17 {
        NONE("NONE","无资源"),
        DEDATA("DEDATA","数据操作"),
        PAGE("PAGE","页面对象"),
        CUSTOM("CUSTOM","自定义");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList17(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_COMPOSE_MESSAGE__MESSAGE_TYPE]
     */
    @Getter
    public enum MAIL_COMPOSE_MESSAGE__MESSAGE_TYPE {
        COMMENT("comment","备注"),
        NOTIFICATION("notification","系统通知");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_COMPOSE_MESSAGE__MESSAGE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_TRANSACTION_TYPE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_TRANSACTION_TYPE {
        CONTAINS("contains","包含"),
        NOT_CONTAINS("not_contains","不包含"),
        MATCH_REGEX("match_regex","正则匹配");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_TRANSACTION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[MAIL_CHANNEL__PUBLIC]
     */
    @Getter
    public enum MAIL_CHANNEL__PUBLIC {
        PUBLIC("public","所有人"),
        PRIVATE("private","仅邀请人员"),
        GROUPS("groups","选择用户组");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_CHANNEL__PUBLIC(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_JOURNAL__POST_AT]
     */
    @Getter
    public enum ACCOUNT_JOURNAL__POST_AT {
        PAY_VAL("pay_val","付款验证"),
        BANK_REC("bank_rec","银行核销");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_JOURNAL__POST_AT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_LANG__WEEK_START]
     */
    @Getter
    public enum RES_LANG__WEEK_START {
        ITEM_1("1","周一"),
        ITEM_2("2","周二"),
        ITEM_3("3","周三"),
        ITEM_4("4","周四"),
        ITEM_5("5","周五"),
        ITEM_6("6","周六"),
        ITEM_7("7","周日");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_LANG__WEEK_START(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[GAMIFICATION_GOAL_DEFINITION__CONDITION]
     */
    @Getter
    public enum GAMIFICATION_GOAL_DEFINITION__CONDITION {
        HIGHER("higher","越高越好"),
        LOWER("lower","越低越好");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        GAMIFICATION_GOAL_DEFINITION__CONDITION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[WEB_EDITOR_CONVERTER_TEST__SELECTION_STR]
     */
    @Getter
    public enum WEB_EDITOR_CONVERTER_TEST__SELECTION_STR {
        A("A","Qu'il n'est pas arrivé à Toronto"),
        B("B","Qu'il était supposé arriver à Toronto"),
        C("C","Qu'est-ce qu'il fout ce maudit pancake, tabernacle ?"),
        D("D","La réponse D");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        WEB_EDITOR_CONVERTER_TEST__SELECTION_STR(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_ALLOCATION__STATE]
     */
    @Getter
    public enum HR_LEAVE_ALLOCATION__STATE {
        DRAFT("draft","待提交"),
        CANCEL("cancel","已取消"),
        CONFIRM("confirm","待批准"),
        REFUSE("refuse","已拒绝"),
        VALIDATE1("validate1","第二次审批"),
        VALIDATE("validate","已批准");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_ALLOCATION__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[REPAIR_ORDER__ACTIVITY_STATE]
     */
    @Getter
    public enum REPAIR_ORDER__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        REPAIR_ORDER__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PAYMENT_TRANSACTION__STATE]
     */
    @Getter
    public enum PAYMENT_TRANSACTION__STATE {
        DRAFT("draft","草稿"),
        PENDING("pending","待定"),
        AUTHORIZED("authorized","授权"),
        DONE("done","完成"),
        CANCEL("cancel","已取消"),
        ERROR("error","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_TRANSACTION__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[月（1～31）]
     */
    @Getter
    public enum CodeList47 {
        ITEM_1("1","1号"),
        ITEM_2("2","2号"),
        ITEM_3("3","3号"),
        ITEM_4("4","4号"),
        ITEM_5("5","5号"),
        ITEM_6("6","6号"),
        ITEM_7("7","7号"),
        ITEM_8("8","8号"),
        ITEM_9("9","9号"),
        ITEM_10("10","10号"),
        ITEM_11("11","11号"),
        ITEM_12("12","12号"),
        ITEM_13("13","13号"),
        ITEM_14("14","14号"),
        ITEM_15("15","15号"),
        ITEM_16("16","16号"),
        ITEM_17("17","17号"),
        ITEM_18("18","18号"),
        ITEM_19("19","19号"),
        ITEM_20("20","20号"),
        ITEM_21("21","21号"),
        ITEM_22("22","22号"),
        ITEM_23("23","23号"),
        ITEM_24("24","24号"),
        ITEM_25("25","25号"),
        ITEM_26("26","26号"),
        ITEM_27("27","27号"),
        ITEM_28("28","28号"),
        ITEM_29("29","29号"),
        ITEM_30("30","30号"),
        ITEM_31("31","31号");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList47(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据通知类型]
     */
    @Getter
    public enum CodeList61 {
        TIME("TIME","定时"),
        NORMAL("NORMAL","值变更"),
        TIMEEX("TIMEEX","定时+值判断");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList61(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_TEMPLATE__TRACKING]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__TRACKING {
        SERIAL("serial","按唯一序列号"),
        LOT("lot","按 批次"),
        NONE("none","无跟踪信息");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__TRACKING(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[实体数据操作步骤]
     */
    @Getter
    public enum CodeList106 {
        GETDEFAULT("GETDEFAULT","获取默认值"),
        BEFORESAVE("BEFORESAVE","保存之前"),
        AFTERSAVE("AFTERSAVE","保存之后"),
        BEFOREREMOVE("BEFOREREMOVE","删除之前"),
        AFTERREMOVE("AFTERREMOVE","删除之后"),
        TESTSAVE("TESTSAVE","测试保存"),
        CUSTOMCALL("CUSTOMCALL","自定义操作"),
        INTERNALCALL("INTERNALCALL","内部调用");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList106(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_WAREHOUSE__MANUFACTURE_STEPS]
     */
    @Getter
    public enum STOCK_WAREHOUSE__MANUFACTURE_STEPS {
        MRP_ONE_STEP("mrp_one_step","制造(1 步)"),
        PBM("pbm","选择组件然后制造（2个步骤）"),
        PBM_SAM("pbm_sam","捡取组件制造，然后储存产品 (3 步)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_WAREHOUSE__MANUFACTURE_STEPS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RESOURCE_CALENDAR_LEAVES__TIME_TYPE]
     */
    @Getter
    public enum RESOURCE_CALENDAR_LEAVES__TIME_TYPE {
        LEAVE("leave","休息时间"),
        OTHER("other","其他");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RESOURCE_CALENDAR_LEAVES__TIME_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_NOTIFICATION__NOTIFICATION_STATUS]
     */
    @Getter
    public enum MAIL_NOTIFICATION__NOTIFICATION_STATUS {
        READY("ready","准备好发送"),
        SENT("sent","已发送"),
        BOUNCE("bounce","被退回"),
        EXCEPTION("exception","异常"),
        CANCELED("canceled","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_NOTIFICATION__NOTIFICATION_STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_COMMON_JOURNAL_REPORT__TARGET_MOVE]
     */
    @Getter
    public enum ACCOUNT_COMMON_JOURNAL_REPORT__TARGET_MOVE {
        POSTED("posted","所有已过账分录"),
        ALL("all","所有分录");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_COMMON_JOURNAL_REPORT__TARGET_MOVE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_JOURNAL__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum ACCOUNT_JOURNAL__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_JOURNAL__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_REPORT__LEAVE_TYPE]
     */
    @Getter
    public enum HR_LEAVE_REPORT__LEAVE_TYPE {
        ALLOCATION("allocation","分配申请"),
        REQUEST("request","休假要求");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_REPORT__LEAVE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_MESSAGE__MODERATION_STATUS]
     */
    @Getter
    public enum MAIL_MESSAGE__MODERATION_STATUS {
        PENDING_MODERATION("pending_moderation","审核中"),
        ACCEPTED("accepted","已同意"),
        REJECTED("rejected","已拒绝");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_MESSAGE__MODERATION_STATUS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[NOTE_NOTE__ACTIVITY_STATE]
     */
    @Getter
    public enum NOTE_NOTE__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        NOTE_NOTE__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__INVENTORY_AVAILABILITY]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__INVENTORY_AVAILABILITY {
        NEVER("never","不考虑库存的销售"),
        ALWAYS("always","在网站上显示库存并且如果没有足够的库存，则阻止销售"),
        THRESHOLD("threshold","Show inventory when below the threshold and prevent sales if not enough stock"),
        CUSTOM("custom","显示具体产品的通知");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__INVENTORY_AVAILABILITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__BASE_ONBOARDING_COMPANY_STATE]
     */
    @Getter
    public enum RES_COMPANY__BASE_ONBOARDING_COMPANY_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__BASE_ONBOARDING_COMPANY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_PICKING__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum STOCK_PICKING__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_PICKING__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_APPLICANT__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum HR_APPLICANT__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_APPLICANT__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据同步方向]
     */
    @Getter
    public enum CodeList97 {
        IN("IN","输入"),
        OUT("OUT","输出");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList97(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_INVOICE_REPORT__INVOICE_PAYMENT_STATE]
     */
    @Getter
    public enum ACCOUNT_INVOICE_REPORT__INVOICE_PAYMENT_STATE {
        NOT_PAID("not_paid","未付款"),
        IN_PAYMENT("in_payment","付款中"),
        PAID("paid","已支付");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_INVOICE_REPORT__INVOICE_PAYMENT_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[表格列对齐]
     */
    @Getter
    public enum CodeList18 {
        LEFT("left","左对齐"),
        CENTER("center","剧中"),
        RIGHT("right","右对齐");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList18(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据通知取值规则]
     */
    @Getter
    public enum CodeList69 {
        AFTER("AFTER","变更后"),
        BEFORE("BEFORE","变更前"),
        CHANGE("CHANGE","值变更");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList69(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[开发数据版本控制状态]
     */
    @Getter
    public enum CodeList115 {
        CHECKIN("CHECKIN","签入"),
        CHECKOUT("CHECKOUT","签出");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList115(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[页面参数值变量]
     */
    @Getter
    public enum CodeList63 {
        PARAM1("PARAM1","参数1(字符)"),
        PARAM2("PARAM2","参数2(字符)");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList63(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL__AMOUNT_TYPE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL__AMOUNT_TYPE {
        FIXED("fixed","固定"),
        PERCENTAGE("percentage","余额百分比"),
        REGEX("regex","来自标签");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL__AMOUNT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PAYMENT_ACQUIRER__SAVE_TOKEN]
     */
    @Getter
    public enum PAYMENT_ACQUIRER__SAVE_TOKEN {
        NONE("none","从不"),
        ASK("ask","让客户决定"),
        ALWAYS("always","总是");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_ACQUIRER__SAVE_TOKEN(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[表格每页记录数]
     */
    @Getter
    public enum CodeList26 {
        ITEM_10("10","10行"),
        ITEM_20("20","20行"),
        ITEM_30("30","30行"),
        ITEM_40("40","40行"),
        ITEM_50("50","50行"),
        ITEM_60("60","60行"),
        ITEM_70("70","70行"),
        ITEM_80("80","80行"),
        ITEM_90("90","90行"),
        ITEM_100("100","100行");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList26(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PAYMENT__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum ACCOUNT_PAYMENT__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_QUESTION__DISPLAY_MODE]
     */
    @Getter
    public enum SURVEY_QUESTION__DISPLAY_MODE {
        COLUMNS("columns","单选按钮"),
        DROPDOWN("dropdown","选择框");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_QUESTION__DISPLAY_MODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }









    /**
     * 代码表[ACCOUNT_JOURNAL__INVOICE_REFERENCE_TYPE]
     */
    @Getter
    public enum ACCOUNT_JOURNAL__INVOICE_REFERENCE_TYPE {
        NONE("none","空闲"),
        PARTNER("partner","基于客户"),
        INVOICE("invoice","开票基于");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_JOURNAL__INVOICE_REFERENCE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SURVEY_SURVEY__QUESTIONS_LAYOUT]
     */
    @Getter
    public enum SURVEY_SURVEY__QUESTIONS_LAYOUT {
        ONE_PAGE("one_page","一页包含所有问题"),
        PAGE_PER_SECTION("page_per_section","每个章节一页"),
        PAGE_PER_QUESTION("page_per_question","每题一页");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_SURVEY__QUESTIONS_LAYOUT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_LEAVE_ALLOCATION__UNIT_PER_INTERVAL]
     */
    @Getter
    public enum HR_LEAVE_ALLOCATION__UNIT_PER_INTERVAL {
        HOURS("hours","小时"),
        DAYS("days","天");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_ALLOCATION__UNIT_PER_INTERVAL(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[EVENT_REGISTRATION__STATE]
     */
    @Getter
    public enum EVENT_REGISTRATION__STATE {
        DRAFT("draft","未确认"),
        CANCEL("cancel","已取消"),
        OPEN("open","已确认"),
        DONE("done","参加");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_REGISTRATION__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_PARTNER__SALE_WARN]
     */
    @Getter
    public enum RES_PARTNER__SALE_WARN {
        NO_SUB_MESSAGE("no-message","没消息"),
        WARNING("warning","警告"),
        BLOCK("block","受阻消息");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_PARTNER__SALE_WARN(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PRODUCT_PRODUCT__ACTIVITY_STATE]
     */
    @Getter
    public enum PRODUCT_PRODUCT__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_PRODUCT__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[界面功能类型]
     */
    @Getter
    public enum CodeList91 {
        INHERIT("INHERIT","继承模板"),
        DEFAULT("DEFAULT","默认功能"),
        CUSTOM("CUSTOM","自定义");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList91(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[日小时（0～23）]
     */
    @Getter
    public enum CodeList85 {
        ITEM_00("00","0点"),
        ITEM_01("01","1点"),
        ITEM_02("02","2点"),
        ITEM_03("03","3点"),
        ITEM_04("04","4点"),
        ITEM_05("05","5点"),
        ITEM_06("06","6点"),
        ITEM_07("07","7点"),
        ITEM_08("08","8点"),
        ITEM_09("09","9点"),
        ITEM_10("10","10点"),
        ITEM_11("11","11点"),
        ITEM_12("12","12点"),
        ITEM_13("13","13点"),
        ITEM_14("14","14点"),
        ITEM_15("15","15点"),
        ITEM_16("16","16点"),
        ITEM_17("17","17点"),
        ITEM_18("18","18点"),
        ITEM_19("19","19点"),
        ITEM_20("20","20点"),
        ITEM_21("21","21点"),
        ITEM_22("22","22点"),
        ITEM_23("23","23点");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList85(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[月份（1～12）]
     */
    @Getter
    public enum CodeList82 {
        ITEM_01("01","1月"),
        ITEM_02("02","2月"),
        ITEM_03("03","3月"),
        ITEM_04("04","4月"),
        ITEM_05("05","5月"),
        ITEM_06("06","6月"),
        ITEM_07("07","7月"),
        ITEM_08("08","8月"),
        ITEM_09("09","9月"),
        ITEM_10("10","10月"),
        ITEM_11("11","11月"),
        ITEM_12("12","12月");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList82(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[BASE_PARTNER_MERGE_AUTOMATIC_WIZARD__STATE]
     */
    @Getter
    public enum BASE_PARTNER_MERGE_AUTOMATIC_WIZARD__STATE {
        OPTION("option","选项"),
        SELECTION("selection","选中内容"),
        FINISHED("finished","已完成");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        BASE_PARTNER_MERGE_AUTOMATIC_WIZARD__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[数据通知监控行为]
     */
    @Getter
    public enum CodeList72 {
        ITEM_1("1","新建"),
        ITEM_2("2","更新"),
        ITEM_3("3","新建或更新"),
        ITEM_4("4","删除");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList72(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PRODUCT_TEMPLATE__INVOICE_POLICY]
     */
    @Getter
    public enum PRODUCT_TEMPLATE__INVOICE_POLICY {
        ORDER("order","订购数量"),
        DELIVERY("delivery","已交货数量");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_TEMPLATE__INVOICE_POLICY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_COMPANY__FONT]
     */
    @Getter
    public enum RES_COMPANY__FONT {
        LATO("Lato","Lato"),
        ROBOTO("Roboto","Roboto"),
        OPEN_SANS("Open_Sans","Open Sans"),
        MONTSERRAT("Montserrat","蒙特塞拉特"),
        OSWALD("Oswald","Oswald"),
        RALEWAY("Raleway","Raleway");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__FONT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[DIGEST_DIGEST__PERIODICITY]
     */
    @Getter
    public enum DIGEST_DIGEST__PERIODICITY {
        WEEKLY("weekly","每周"),
        MONTHLY("monthly","每月"),
        QUARTERLY("quarterly","季度");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        DIGEST_DIGEST__PERIODICITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[FETCHMAIL_SERVER__SERVER_TYPE]
     */
    @Getter
    public enum FETCHMAIL_SERVER__SERVER_TYPE {
        POP("pop","POP 服务器"),
        IMAP("imap","IMAP 服务器"),
        LOCAL("local","本地服务器");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        FETCHMAIL_SERVER__SERVER_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体规则处理_值处理函数]
     */
    @Getter
    public enum CodeList110 {
        DATE_DIFF_D("DATE_DIFF_D","距今天数（日期）"),
        DATE_DIFF_W("DATE_DIFF_W","距今周数（日期）"),
        DATE_DIFF_M("DATE_DIFF_M","距今月份数（日期）"),
        DATE_DIFF_Q("DATE_DIFF_Q","距今季度数（日期）"),
        DATE_DIFF_Y("DATE_DIFF_Y","距今年数（日期）");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList110(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_ORDER_LINE__QTY_RECEIVED_METHOD]
     */
    @Getter
    public enum PURCHASE_ORDER_LINE__QTY_RECEIVED_METHOD {
        MANUAL("manual","手动"),
        STOCK_MOVES("stock_moves","库存移动");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_ORDER_LINE__QTY_RECEIVED_METHOD(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_ACTIVITY_TYPE__DELAY_UNIT]
     */
    @Getter
    public enum MAIL_ACTIVITY_TYPE__DELAY_UNIT {
        DAYS("days","天数"),
        WEEKS("weeks","周"),
        MONTHS("months","月");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_ACTIVITY_TYPE__DELAY_UNIT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[CRM_LEAD__KANBAN_STATE]
     */
    @Getter
    public enum CRM_LEAD__KANBAN_STATE {
        GREY("grey","没有安排下一个活动"),
        RED("red","下一个活动迟到"),
        GREEN("green","下一个活动已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CRM_LEAD__KANBAN_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__PRODUCT_WEIGHT_IN_LBS]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__PRODUCT_WEIGHT_IN_LBS {
        ITEM_0("0","千克"),
        ITEM_1("1","磅");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__PRODUCT_WEIGHT_IN_LBS(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[ACCOUNT_PAYMENT_METHOD__PAYMENT_TYPE]
     */
    @Getter
    public enum ACCOUNT_PAYMENT_METHOD__PAYMENT_TYPE {
        INBOUND("inbound","入"),
        OUTBOUND("outbound","转出");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT_METHOD__PAYMENT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_CURRENCY__POSITION]
     */
    @Getter
    public enum RES_CURRENCY__POSITION {
        AFTER("after","金额后面"),
        BEFORE("before","金额前面");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CURRENCY__POSITION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[HR_EMPLOYEE__GENDER]
     */
    @Getter
    public enum HR_EMPLOYEE__GENDER {
        MALE("male","男性"),
        FEMALE("female","女性"),
        OTHER("other","其他");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_EMPLOYEE__GENDER(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[RES_COMPANY__ACCOUNT_DASHBOARD_ONBOARDING_STATE]
     */
    @Getter
    public enum RES_COMPANY__ACCOUNT_DASHBOARD_ONBOARDING_STATE {
        NOT_DONE("not_done","尚未完成"),
        JUST_DONE("just_done","完成"),
        DONE("done","完成"),
        CLOSED("closed","已关闭");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_COMPANY__ACCOUNT_DASHBOARD_ONBOARDING_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[PURCHASE_REPORT__STATE]
     */
    @Getter
    public enum PURCHASE_REPORT__STATE {
        DRAFT("draft","草稿询价单"),
        SENT("sent","发送询价单"),
        TO_APPROVE("to approve","待批准"),
        PURCHASE("purchase","采购订单"),
        DONE("done","完成"),
        CANCEL("cancel","已取消");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PURCHASE_REPORT__STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_CHANNEL__IBIZPUBLIC]
     */
    @Getter
    public enum MAIL_CHANNEL__IBIZPUBLIC {
        PUBLIC("public","所有人"),
        PRIVATE("private","仅邀请人员"),
        GROUPS("groups","选择用户组");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_CHANNEL__IBIZPUBLIC(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }




    /**
     * 代码表[MAIL_ACTIVITY_MIXIN__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum MAIL_ACTIVITY_MIXIN__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_ACTIVITY_MIXIN__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_PAYMENT__PAYMENT_TYPE]
     */
    @Getter
    public enum ACCOUNT_PAYMENT__PAYMENT_TYPE {
        OUTBOUND("outbound","付款"),
        INBOUND("inbound","收款"),
        TRANSFER("transfer","内部转账");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_PAYMENT__PAYMENT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[SALE_ORDER_TEMPLATE_LINE__DISPLAY_TYPE]
     */
    @Getter
    public enum SALE_ORDER_TEMPLATE_LINE__DISPLAY_TYPE {
        LINE_SECTION("line_section","章节"),
        LINE_NOTE("line_note","备注");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER_TEMPLATE_LINE__DISPLAY_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[EVENT_EVENT__ACTIVITY_STATE]
     */
    @Getter
    public enum EVENT_EVENT__ACTIVITY_STATE {
        OVERDUE("overdue","逾期"),
        TODAY("today","今日"),
        PLANNED("planned","已计划");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_EVENT__ACTIVITY_STATE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_NATURE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_NATURE {
        AMOUNT_RECEIVED("amount_received","收入金额"),
        AMOUNT_PAID("amount_paid","已付金额"),
        BOTH("both","收/付金额");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL_TEMPLATE__MATCH_NATURE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[SNAILMAIL_LETTER__ERROR_CODE]
     */
    @Getter
    public enum SNAILMAIL_LETTER__ERROR_CODE {
        MISSING_REQUIRED_FIELDS("MISSING_REQUIRED_FIELDS","MISSING_REQUIRED_FIELDS"),
        CREDIT_ERROR("CREDIT_ERROR","CREDIT_ERROR"),
        TRIAL_ERROR("TRIAL_ERROR","TRIAL_ERROR"),
        NO_PRICE_AVAILABLE("NO_PRICE_AVAILABLE","NO_PRICE_AVAILABLE"),
        FORMAT_ERROR("FORMAT_ERROR","FORMAT_ERROR"),
        UNKNOWN_ERROR("UNKNOWN_ERROR","UNKNOWN_ERROR");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SNAILMAIL_LETTER__ERROR_CODE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[HR_LEAVE_TYPE__VALIDATION_TYPE]
     */
    @Getter
    public enum HR_LEAVE_TYPE__VALIDATION_TYPE {
        NO_VALIDATION("no_validation","没有验证"),
        HR("hr","休假主管"),
        MANAGER("manager","团队负责人"),
        BOTH("both","团队负责人和休假主管");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        HR_LEAVE_TYPE__VALIDATION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[STOCK_RULE__AUTO]
     */
    @Getter
    public enum STOCK_RULE__AUTO {
        MANUAL("manual","手动作业"),
        TRANSPARENT("transparent","自动，不增加步骤");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        STOCK_RULE__AUTO(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__DEFAULT_INVOICE_POLICY]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__DEFAULT_INVOICE_POLICY {
        ORDER("order","订单发票"),
        DELIVERY("delivery"," 发货发票");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__DEFAULT_INVOICE_POLICY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体数据处理_变量名称]
     */
    @Getter
    public enum CodeList109 {
        _DEFAULT_("%DEFAULT%","默认变量"),
        _ENV_("%ENV%","环境变量"),
        PARAM1("PARAM1","变量1"),
        PARAM2("PARAM2","变量2"),
        PARAM3("PARAM3","变量3"),
        PARAM4("PARAM4","变量4"),
        PARAM5("PARAM5","变量5"),
        _LAST_("%LAST%","历史值"),
        _GLOBAL1_("%GLOBAL1%","全局变量1"),
        _GLOBAL2_("%GLOBAL2%","全局变量2"),
        _GLOBAL3_("%GLOBAL3%","全局变量3"),
        _GLOBAL4_("%GLOBAL4%","全局变量4"),
        _GLOBAL5_("%GLOBAL5%","全局变量5"),
        _BRINST_("%BRINST%","全局规则引擎实例变量");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList109(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[EVENT_TYPE_MAIL__INTERVAL_UNIT]
     */
    @Getter
    public enum EVENT_TYPE_MAIL__INTERVAL_UNIT {
        NOW("now","马上"),
        HOURS("hours","小时"),
        DAYS("days","天"),
        WEEKS("weeks","周"),
        MONTHS("months","月");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        EVENT_TYPE_MAIL__INTERVAL_UNIT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_ACCRUAL_ACCOUNTING_WIZARD__ACCOUNT_TYPE]
     */
    @Getter
    public enum ACCOUNT_ACCRUAL_ACCOUNTING_WIZARD__ACCOUNT_TYPE {
        INCOME("income","收入"),
        EXPENSE("expense","费用");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_ACCRUAL_ACCOUNTING_WIZARD__ACCOUNT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[PRODUCT_PRODUCT__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum PRODUCT_PRODUCT__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PRODUCT_PRODUCT__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MRP_PRODUCTION__PRIORITY]
     */
    @Getter
    public enum MRP_PRODUCTION__PRIORITY {
        ITEM_0("0","不紧急"),
        ITEM_1("1","一般"),
        ITEM_2("2","紧急"),
        ITEM_3("3","非常紧急");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MRP_PRODUCTION__PRIORITY(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }





    /**
     * 代码表[PAYMENT_ACQUIRER__SO_REFERENCE_TYPE]
     */
    @Getter
    public enum PAYMENT_ACQUIRER__SO_REFERENCE_TYPE {
        SO_NAME("so_name","按参考文档"),
        PARTNER("partner","按客户的ID");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        PAYMENT_ACQUIRER__SO_REFERENCE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[MAIL_MESSAGE__MESSAGE_TYPE]
     */
    @Getter
    public enum MAIL_MESSAGE__MESSAGE_TYPE {
        EMAIL("email","EMail"),
        COMMENT("comment","备注"),
        NOTIFICATION("notification","系统通知"),
        USER_NOTIFICATION("user_notification","用户特定通知"),
        SNAILMAIL("snailmail","邮寄信件"),
        SMS("sms","短信");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        MAIL_MESSAGE__MESSAGE_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[CALENDAR_EVENT__WEEK_LIST]
     */
    @Getter
    public enum CALENDAR_EVENT__WEEK_LIST {
        MO("MO","周一"),
        TU("TU","周二"),
        WE("WE","周三"),
        TH("TH","周四"),
        FR("FR","周五"),
        SA("SA","周六"),
        SU("SU","周日");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CALENDAR_EVENT__WEEK_LIST(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[UOM_UOM__UOM_TYPE]
     */
    @Getter
    public enum UOM_UOM__UOM_TYPE {
        BIGGER("bigger","大于参考单位"),
        REFERENCE("reference","这个类别的参考计量单位"),
        SMALLER("smaller","小于参考计量单位");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        UOM_UOM__UOM_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[字段排序方向]
     */
    @Getter
    public enum SortDir {
        ASC("ASC","升序"),
        DESC("DESC","降序");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SortDir(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_TAX_TEMPLATE__AMOUNT_TYPE]
     */
    @Getter
    public enum ACCOUNT_TAX_TEMPLATE__AMOUNT_TYPE {
        GROUP("group","税组"),
        FIXED("fixed","固定"),
        PERCENT("percent","价格百分比"),
        DIVISION("division","含税价格百分比");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_TAX_TEMPLATE__AMOUNT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[日历参与者状态]
     */
    @Getter
    public enum CodeList15 {
        UNDECIDED("UNDECIDED","未确定"),
        ACCEPT("ACCEPT","接受"),
        REJECT("REJECT","拒绝");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList15(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[SURVEY_SURVEY__SCORING_TYPE]
     */
    @Getter
    public enum SURVEY_SURVEY__SCORING_TYPE {
        NO_SCORING("no_scoring","没有得分"),
        SCORING_WITH_ANSWERS("scoring_with_answers","最后得分的答案"),
        SCORING_WITHOUT_ANSWERS("scoring_without_answers","最后没有答案的得分");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_SURVEY__SCORING_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[SURVEY_USER_INPUT__INPUT_TYPE]
     */
    @Getter
    public enum SURVEY_USER_INPUT__INPUT_TYPE {
        MANUALLY("manually","手动"),
        LINK("link","邀请");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SURVEY_USER_INPUT__INPUT_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[RES_CONFIG_SETTINGS__MODULE_PROCUREMENT_JIT]
     */
    @Getter
    public enum RES_CONFIG_SETTINGS__MODULE_PROCUREMENT_JIT {
        ITEM_1("1","在销售订单确认后立即"),
        ITEM_0("0","手动或者基于自动调度器");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        RES_CONFIG_SETTINGS__MODULE_PROCUREMENT_JIT(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[ACCOUNT_RECONCILE_MODEL__MATCH_TRANSACTION_TYPE]
     */
    @Getter
    public enum ACCOUNT_RECONCILE_MODEL__MATCH_TRANSACTION_TYPE {
        CONTAINS("contains","包含"),
        NOT_CONTAINS("not_contains","不包含"),
        MATCH_REGEX("match_regex","正则匹配");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        ACCOUNT_RECONCILE_MODEL__MATCH_TRANSACTION_TYPE(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[实体数据库操作]
     */
    @Getter
    public enum CodeList53 {
        INSERT("INSERT","插入"),
        UPDATE("UPDATE","更新"),
        SELECT("SELECT","查询"),
        DELETE("DELETE","删除"),
        CUSTOM("CUSTOM","自定义");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList53(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }


    /**
     * 代码表[扩展表格单元格边框样式]
     */
    @Getter
    public enum CodeList76 {
        LEFT("LEFT","左边框"),
        TOP("TOP","上边框"),
        RIGHT("RIGHT","右边框"),
        BOTTOM("BOTTOM","下边框");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        CodeList76(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }



    /**
     * 代码表[SALE_ORDER__ACTIVITY_EXCEPTION_DECORATION]
     */
    @Getter
    public enum SALE_ORDER__ACTIVITY_EXCEPTION_DECORATION {
        WARNING("warning","警告"),
        DANGER("danger","错误");

        private String value;
        private String text;
        private String valueSeparator="";
        private String textSeparator="";
        private String emptyText="";

        SALE_ORDER__ACTIVITY_EXCEPTION_DECORATION(String value , String text) {
            this.value=value;
            this.text = text;
        }
    }

}

