package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_bill_unionService;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"采购 & 账单" })
@RestController("Core-purchase_bill_union")
@RequestMapping("")
public class Purchase_bill_unionResource {

    @Autowired
    public IPurchase_bill_unionService purchase_bill_unionService;

    @Autowired
    @Lazy
    public Purchase_bill_unionMapping purchase_bill_unionMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Create-all')")
    @ApiOperation(value = "新建采购 & 账单", tags = {"采购 & 账单" },  notes = "新建采购 & 账单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions")
    public ResponseEntity<Purchase_bill_unionDTO> create(@Validated @RequestBody Purchase_bill_unionDTO purchase_bill_uniondto) {
        Purchase_bill_union domain = purchase_bill_unionMapping.toDomain(purchase_bill_uniondto);
		purchase_bill_unionService.create(domain);
        Purchase_bill_unionDTO dto = purchase_bill_unionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Create-all')")
    @ApiOperation(value = "批量新建采购 & 账单", tags = {"采购 & 账单" },  notes = "批量新建采购 & 账单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_bill_unionDTO> purchase_bill_uniondtos) {
        purchase_bill_unionService.createBatch(purchase_bill_unionMapping.toDomain(purchase_bill_uniondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Update-all')")
    @ApiOperation(value = "更新采购 & 账单", tags = {"采购 & 账单" },  notes = "更新采购 & 账单")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_bill_unions/{purchase_bill_union_id}")
    public ResponseEntity<Purchase_bill_unionDTO> update(@PathVariable("purchase_bill_union_id") Long purchase_bill_union_id, @RequestBody Purchase_bill_unionDTO purchase_bill_uniondto) {
		Purchase_bill_union domain  = purchase_bill_unionMapping.toDomain(purchase_bill_uniondto);
        domain .setId(purchase_bill_union_id);
		purchase_bill_unionService.update(domain );
		Purchase_bill_unionDTO dto = purchase_bill_unionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Update-all')")
    @ApiOperation(value = "批量更新采购 & 账单", tags = {"采购 & 账单" },  notes = "批量更新采购 & 账单")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_bill_unions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_bill_unionDTO> purchase_bill_uniondtos) {
        purchase_bill_unionService.updateBatch(purchase_bill_unionMapping.toDomain(purchase_bill_uniondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Remove-all')")
    @ApiOperation(value = "删除采购 & 账单", tags = {"采购 & 账单" },  notes = "删除采购 & 账单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_bill_unions/{purchase_bill_union_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("purchase_bill_union_id") Long purchase_bill_union_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_bill_unionService.remove(purchase_bill_union_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Remove-all')")
    @ApiOperation(value = "批量删除采购 & 账单", tags = {"采购 & 账单" },  notes = "批量删除采购 & 账单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_bill_unions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        purchase_bill_unionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Get-all')")
    @ApiOperation(value = "获取采购 & 账单", tags = {"采购 & 账单" },  notes = "获取采购 & 账单")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_bill_unions/{purchase_bill_union_id}")
    public ResponseEntity<Purchase_bill_unionDTO> get(@PathVariable("purchase_bill_union_id") Long purchase_bill_union_id) {
        Purchase_bill_union domain = purchase_bill_unionService.get(purchase_bill_union_id);
        Purchase_bill_unionDTO dto = purchase_bill_unionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取采购 & 账单草稿", tags = {"采购 & 账单" },  notes = "获取采购 & 账单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_bill_unions/getdraft")
    public ResponseEntity<Purchase_bill_unionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_bill_unionMapping.toDto(purchase_bill_unionService.getDraft(new Purchase_bill_union())));
    }

    @ApiOperation(value = "检查采购 & 账单", tags = {"采购 & 账单" },  notes = "检查采购 & 账单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Purchase_bill_unionDTO purchase_bill_uniondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_bill_unionService.checkKey(purchase_bill_unionMapping.toDomain(purchase_bill_uniondto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Save-all')")
    @ApiOperation(value = "保存采购 & 账单", tags = {"采购 & 账单" },  notes = "保存采购 & 账单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions/save")
    public ResponseEntity<Boolean> save(@RequestBody Purchase_bill_unionDTO purchase_bill_uniondto) {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_bill_unionService.save(purchase_bill_unionMapping.toDomain(purchase_bill_uniondto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-Save-all')")
    @ApiOperation(value = "批量保存采购 & 账单", tags = {"采购 & 账单" },  notes = "批量保存采购 & 账单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Purchase_bill_unionDTO> purchase_bill_uniondtos) {
        purchase_bill_unionService.saveBatch(purchase_bill_unionMapping.toDomain(purchase_bill_uniondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"采购 & 账单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_bill_unions/fetchdefault")
	public ResponseEntity<List<Purchase_bill_unionDTO>> fetchDefault(Purchase_bill_unionSearchContext context) {
        Page<Purchase_bill_union> domains = purchase_bill_unionService.searchDefault(context) ;
        List<Purchase_bill_unionDTO> list = purchase_bill_unionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_bill_union-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"采购 & 账单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_bill_unions/searchdefault")
	public ResponseEntity<Page<Purchase_bill_unionDTO>> searchDefault(@RequestBody Purchase_bill_unionSearchContext context) {
        Page<Purchase_bill_union> domains = purchase_bill_unionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_bill_unionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

