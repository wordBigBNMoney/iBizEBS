package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template_option;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_template_optionService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_template_optionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"报价模板选项" })
@RestController("Core-sale_order_template_option")
@RequestMapping("")
public class Sale_order_template_optionResource {

    @Autowired
    public ISale_order_template_optionService sale_order_template_optionService;

    @Autowired
    @Lazy
    public Sale_order_template_optionMapping sale_order_template_optionMapping;

    @PreAuthorize("hasPermission(this.sale_order_template_optionMapping.toDomain(#sale_order_template_optiondto),'iBizBusinessCentral-Sale_order_template_option-Create')")
    @ApiOperation(value = "新建报价模板选项", tags = {"报价模板选项" },  notes = "新建报价模板选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_options")
    public ResponseEntity<Sale_order_template_optionDTO> create(@Validated @RequestBody Sale_order_template_optionDTO sale_order_template_optiondto) {
        Sale_order_template_option domain = sale_order_template_optionMapping.toDomain(sale_order_template_optiondto);
		sale_order_template_optionService.create(domain);
        Sale_order_template_optionDTO dto = sale_order_template_optionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_template_optionMapping.toDomain(#sale_order_template_optiondtos),'iBizBusinessCentral-Sale_order_template_option-Create')")
    @ApiOperation(value = "批量新建报价模板选项", tags = {"报价模板选项" },  notes = "批量新建报价模板选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_options/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_order_template_optionDTO> sale_order_template_optiondtos) {
        sale_order_template_optionService.createBatch(sale_order_template_optionMapping.toDomain(sale_order_template_optiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sale_order_template_option" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sale_order_template_optionService.get(#sale_order_template_option_id),'iBizBusinessCentral-Sale_order_template_option-Update')")
    @ApiOperation(value = "更新报价模板选项", tags = {"报价模板选项" },  notes = "更新报价模板选项")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_template_options/{sale_order_template_option_id}")
    public ResponseEntity<Sale_order_template_optionDTO> update(@PathVariable("sale_order_template_option_id") Long sale_order_template_option_id, @RequestBody Sale_order_template_optionDTO sale_order_template_optiondto) {
		Sale_order_template_option domain  = sale_order_template_optionMapping.toDomain(sale_order_template_optiondto);
        domain .setId(sale_order_template_option_id);
		sale_order_template_optionService.update(domain );
		Sale_order_template_optionDTO dto = sale_order_template_optionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_template_optionService.getSaleOrderTemplateOptionByEntities(this.sale_order_template_optionMapping.toDomain(#sale_order_template_optiondtos)),'iBizBusinessCentral-Sale_order_template_option-Update')")
    @ApiOperation(value = "批量更新报价模板选项", tags = {"报价模板选项" },  notes = "批量更新报价模板选项")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_template_options/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_template_optionDTO> sale_order_template_optiondtos) {
        sale_order_template_optionService.updateBatch(sale_order_template_optionMapping.toDomain(sale_order_template_optiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sale_order_template_optionService.get(#sale_order_template_option_id),'iBizBusinessCentral-Sale_order_template_option-Remove')")
    @ApiOperation(value = "删除报价模板选项", tags = {"报价模板选项" },  notes = "删除报价模板选项")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_template_options/{sale_order_template_option_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_template_option_id") Long sale_order_template_option_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_order_template_optionService.remove(sale_order_template_option_id));
    }

    @PreAuthorize("hasPermission(this.sale_order_template_optionService.getSaleOrderTemplateOptionByIds(#ids),'iBizBusinessCentral-Sale_order_template_option-Remove')")
    @ApiOperation(value = "批量删除报价模板选项", tags = {"报价模板选项" },  notes = "批量删除报价模板选项")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_template_options/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_order_template_optionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sale_order_template_optionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sale_order_template_option-Get')")
    @ApiOperation(value = "获取报价模板选项", tags = {"报价模板选项" },  notes = "获取报价模板选项")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_template_options/{sale_order_template_option_id}")
    public ResponseEntity<Sale_order_template_optionDTO> get(@PathVariable("sale_order_template_option_id") Long sale_order_template_option_id) {
        Sale_order_template_option domain = sale_order_template_optionService.get(sale_order_template_option_id);
        Sale_order_template_optionDTO dto = sale_order_template_optionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取报价模板选项草稿", tags = {"报价模板选项" },  notes = "获取报价模板选项草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_template_options/getdraft")
    public ResponseEntity<Sale_order_template_optionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_template_optionMapping.toDto(sale_order_template_optionService.getDraft(new Sale_order_template_option())));
    }

    @ApiOperation(value = "检查报价模板选项", tags = {"报价模板选项" },  notes = "检查报价模板选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_options/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_order_template_optionDTO sale_order_template_optiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_order_template_optionService.checkKey(sale_order_template_optionMapping.toDomain(sale_order_template_optiondto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_template_optionMapping.toDomain(#sale_order_template_optiondto),'iBizBusinessCentral-Sale_order_template_option-Save')")
    @ApiOperation(value = "保存报价模板选项", tags = {"报价模板选项" },  notes = "保存报价模板选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_options/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_order_template_optionDTO sale_order_template_optiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_template_optionService.save(sale_order_template_optionMapping.toDomain(sale_order_template_optiondto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_template_optionMapping.toDomain(#sale_order_template_optiondtos),'iBizBusinessCentral-Sale_order_template_option-Save')")
    @ApiOperation(value = "批量保存报价模板选项", tags = {"报价模板选项" },  notes = "批量保存报价模板选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_options/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_order_template_optionDTO> sale_order_template_optiondtos) {
        sale_order_template_optionService.saveBatch(sale_order_template_optionMapping.toDomain(sale_order_template_optiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_template_option-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_template_option-Get')")
	@ApiOperation(value = "获取数据集", tags = {"报价模板选项" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_order_template_options/fetchdefault")
	public ResponseEntity<List<Sale_order_template_optionDTO>> fetchDefault(Sale_order_template_optionSearchContext context) {
        Page<Sale_order_template_option> domains = sale_order_template_optionService.searchDefault(context) ;
        List<Sale_order_template_optionDTO> list = sale_order_template_optionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_template_option-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_template_option-Get')")
	@ApiOperation(value = "查询数据集", tags = {"报价模板选项" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_order_template_options/searchdefault")
	public ResponseEntity<Page<Sale_order_template_optionDTO>> searchDefault(@RequestBody Sale_order_template_optionSearchContext context) {
        Page<Sale_order_template_option> domains = sale_order_template_optionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_order_template_optionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

