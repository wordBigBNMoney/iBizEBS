package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Hr_recruitment_stageDTO]
 */
@Data
public class Hr_recruitment_stageDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [REQUIREMENTS]
     *
     */
    @JSONField(name = "requirements")
    @JsonProperty("requirements")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String requirements;

    /**
     * 属性 [LEGEND_NORMAL]
     *
     */
    @JSONField(name = "legend_normal")
    @JsonProperty("legend_normal")
    @NotBlank(message = "[灰色看板标签]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String legendNormal;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[阶段名]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [FOLD]
     *
     */
    @JSONField(name = "fold")
    @JsonProperty("fold")
    private Boolean fold;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [LEGEND_DONE]
     *
     */
    @JSONField(name = "legend_done")
    @JsonProperty("legend_done")
    @NotBlank(message = "[绿色看板标签]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String legendDone;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [LEGEND_BLOCKED]
     *
     */
    @JSONField(name = "legend_blocked")
    @JsonProperty("legend_blocked")
    @NotBlank(message = "[红色的看板标签]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String legendBlocked;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String templateIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [JOB_ID_TEXT]
     *
     */
    @JSONField(name = "job_id_text")
    @JsonProperty("job_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jobIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [JOB_ID]
     *
     */
    @JSONField(name = "job_id")
    @JsonProperty("job_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long jobId;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long templateId;


    /**
     * 设置 [REQUIREMENTS]
     */
    public void setRequirements(String  requirements){
        this.requirements = requirements ;
        this.modify("requirements",requirements);
    }

    /**
     * 设置 [LEGEND_NORMAL]
     */
    public void setLegendNormal(String  legendNormal){
        this.legendNormal = legendNormal ;
        this.modify("legend_normal",legendNormal);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [FOLD]
     */
    public void setFold(Boolean  fold){
        this.fold = fold ;
        this.modify("fold",fold);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [LEGEND_DONE]
     */
    public void setLegendDone(String  legendDone){
        this.legendDone = legendDone ;
        this.modify("legend_done",legendDone);
    }

    /**
     * 设置 [LEGEND_BLOCKED]
     */
    public void setLegendBlocked(String  legendBlocked){
        this.legendBlocked = legendBlocked ;
        this.modify("legend_blocked",legendBlocked);
    }

    /**
     * 设置 [JOB_ID]
     */
    public void setJobId(Long  jobId){
        this.jobId = jobId ;
        this.modify("job_id",jobId);
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    public void setTemplateId(Long  templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }


}


