package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivity_lossService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作中心生产力损失" })
@RestController("Core-mrp_workcenter_productivity_loss")
@RequestMapping("")
public class Mrp_workcenter_productivity_lossResource {

    @Autowired
    public IMrp_workcenter_productivity_lossService mrp_workcenter_productivity_lossService;

    @Autowired
    @Lazy
    public Mrp_workcenter_productivity_lossMapping mrp_workcenter_productivity_lossMapping;

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_lossMapping.toDomain(#mrp_workcenter_productivity_lossdto),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Create')")
    @ApiOperation(value = "新建工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "新建工作中心生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses")
    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> create(@Validated @RequestBody Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto) {
        Mrp_workcenter_productivity_loss domain = mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdto);
		mrp_workcenter_productivity_lossService.create(domain);
        Mrp_workcenter_productivity_lossDTO dto = mrp_workcenter_productivity_lossMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_lossMapping.toDomain(#mrp_workcenter_productivity_lossdtos),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Create')")
    @ApiOperation(value = "批量新建工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "批量新建工作中心生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workcenter_productivity_lossDTO> mrp_workcenter_productivity_lossdtos) {
        mrp_workcenter_productivity_lossService.createBatch(mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_workcenter_productivity_loss" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_lossService.get(#mrp_workcenter_productivity_loss_id),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Update')")
    @ApiOperation(value = "更新工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "更新工作中心生产力损失")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")
    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> update(@PathVariable("mrp_workcenter_productivity_loss_id") Long mrp_workcenter_productivity_loss_id, @RequestBody Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto) {
		Mrp_workcenter_productivity_loss domain  = mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdto);
        domain .setId(mrp_workcenter_productivity_loss_id);
		mrp_workcenter_productivity_lossService.update(domain );
		Mrp_workcenter_productivity_lossDTO dto = mrp_workcenter_productivity_lossMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_lossService.getMrpWorkcenterProductivityLossByEntities(this.mrp_workcenter_productivity_lossMapping.toDomain(#mrp_workcenter_productivity_lossdtos)),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Update')")
    @ApiOperation(value = "批量更新工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "批量更新工作中心生产力损失")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_losses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivity_lossDTO> mrp_workcenter_productivity_lossdtos) {
        mrp_workcenter_productivity_lossService.updateBatch(mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_lossService.get(#mrp_workcenter_productivity_loss_id),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Remove')")
    @ApiOperation(value = "删除工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "删除工作中心生产力损失")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_loss_id") Long mrp_workcenter_productivity_loss_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_lossService.remove(mrp_workcenter_productivity_loss_id));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_lossService.getMrpWorkcenterProductivityLossByIds(#ids),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Remove')")
    @ApiOperation(value = "批量删除工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "批量删除工作中心生产力损失")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_losses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_workcenter_productivity_lossService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_workcenter_productivity_lossMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Get')")
    @ApiOperation(value = "获取工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "获取工作中心生产力损失")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")
    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> get(@PathVariable("mrp_workcenter_productivity_loss_id") Long mrp_workcenter_productivity_loss_id) {
        Mrp_workcenter_productivity_loss domain = mrp_workcenter_productivity_lossService.get(mrp_workcenter_productivity_loss_id);
        Mrp_workcenter_productivity_lossDTO dto = mrp_workcenter_productivity_lossMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作中心生产力损失草稿", tags = {"工作中心生产力损失" },  notes = "获取工作中心生产力损失草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_losses/getdraft")
    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_lossMapping.toDto(mrp_workcenter_productivity_lossService.getDraft(new Mrp_workcenter_productivity_loss())));
    }

    @ApiOperation(value = "检查工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "检查工作中心生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_lossService.checkKey(mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_lossMapping.toDomain(#mrp_workcenter_productivity_lossdto),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Save')")
    @ApiOperation(value = "保存工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "保存工作中心生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_lossService.save(mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_lossMapping.toDomain(#mrp_workcenter_productivity_lossdtos),'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Save')")
    @ApiOperation(value = "批量保存工作中心生产力损失", tags = {"工作中心生产力损失" },  notes = "批量保存工作中心生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_workcenter_productivity_lossDTO> mrp_workcenter_productivity_lossdtos) {
        mrp_workcenter_productivity_lossService.saveBatch(mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workcenter_productivity_loss-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作中心生产力损失" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivity_losses/fetchdefault")
	public ResponseEntity<List<Mrp_workcenter_productivity_lossDTO>> fetchDefault(Mrp_workcenter_productivity_lossSearchContext context) {
        Page<Mrp_workcenter_productivity_loss> domains = mrp_workcenter_productivity_lossService.searchDefault(context) ;
        List<Mrp_workcenter_productivity_lossDTO> list = mrp_workcenter_productivity_lossMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workcenter_productivity_loss-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workcenter_productivity_loss-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作中心生产力损失" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_workcenter_productivity_losses/searchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivity_lossDTO>> searchDefault(@RequestBody Mrp_workcenter_productivity_lossSearchContext context) {
        Page<Mrp_workcenter_productivity_loss> domains = mrp_workcenter_productivity_lossService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workcenter_productivity_lossMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

