package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lost_reasonService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lost_reasonSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"丢单原因" })
@RestController("Core-crm_lost_reason")
@RequestMapping("")
public class Crm_lost_reasonResource {

    @Autowired
    public ICrm_lost_reasonService crm_lost_reasonService;

    @Autowired
    @Lazy
    public Crm_lost_reasonMapping crm_lost_reasonMapping;

    @PreAuthorize("hasPermission(this.crm_lost_reasonMapping.toDomain(#crm_lost_reasondto),'iBizBusinessCentral-Crm_lost_reason-Create')")
    @ApiOperation(value = "新建丢单原因", tags = {"丢单原因" },  notes = "新建丢单原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons")
    public ResponseEntity<Crm_lost_reasonDTO> create(@Validated @RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
        Crm_lost_reason domain = crm_lost_reasonMapping.toDomain(crm_lost_reasondto);
		crm_lost_reasonService.create(domain);
        Crm_lost_reasonDTO dto = crm_lost_reasonMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lost_reasonMapping.toDomain(#crm_lost_reasondtos),'iBizBusinessCentral-Crm_lost_reason-Create')")
    @ApiOperation(value = "批量新建丢单原因", tags = {"丢单原因" },  notes = "批量新建丢单原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lost_reasonDTO> crm_lost_reasondtos) {
        crm_lost_reasonService.createBatch(crm_lost_reasonMapping.toDomain(crm_lost_reasondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_lost_reason" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_lost_reasonService.get(#crm_lost_reason_id),'iBizBusinessCentral-Crm_lost_reason-Update')")
    @ApiOperation(value = "更新丢单原因", tags = {"丢单原因" },  notes = "更新丢单原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lost_reasons/{crm_lost_reason_id}")
    public ResponseEntity<Crm_lost_reasonDTO> update(@PathVariable("crm_lost_reason_id") Long crm_lost_reason_id, @RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
		Crm_lost_reason domain  = crm_lost_reasonMapping.toDomain(crm_lost_reasondto);
        domain .setId(crm_lost_reason_id);
		crm_lost_reasonService.update(domain );
		Crm_lost_reasonDTO dto = crm_lost_reasonMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lost_reasonService.getCrmLostReasonByEntities(this.crm_lost_reasonMapping.toDomain(#crm_lost_reasondtos)),'iBizBusinessCentral-Crm_lost_reason-Update')")
    @ApiOperation(value = "批量更新丢单原因", tags = {"丢单原因" },  notes = "批量更新丢单原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lost_reasons/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lost_reasonDTO> crm_lost_reasondtos) {
        crm_lost_reasonService.updateBatch(crm_lost_reasonMapping.toDomain(crm_lost_reasondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_lost_reasonService.get(#crm_lost_reason_id),'iBizBusinessCentral-Crm_lost_reason-Remove')")
    @ApiOperation(value = "删除丢单原因", tags = {"丢单原因" },  notes = "删除丢单原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lost_reasons/{crm_lost_reason_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lost_reason_id") Long crm_lost_reason_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reasonService.remove(crm_lost_reason_id));
    }

    @PreAuthorize("hasPermission(this.crm_lost_reasonService.getCrmLostReasonByIds(#ids),'iBizBusinessCentral-Crm_lost_reason-Remove')")
    @ApiOperation(value = "批量删除丢单原因", tags = {"丢单原因" },  notes = "批量删除丢单原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lost_reasons/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_lost_reasonService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_lost_reasonMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_lost_reason-Get')")
    @ApiOperation(value = "获取丢单原因", tags = {"丢单原因" },  notes = "获取丢单原因")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/{crm_lost_reason_id}")
    public ResponseEntity<Crm_lost_reasonDTO> get(@PathVariable("crm_lost_reason_id") Long crm_lost_reason_id) {
        Crm_lost_reason domain = crm_lost_reasonService.get(crm_lost_reason_id);
        Crm_lost_reasonDTO dto = crm_lost_reasonMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取丢单原因草稿", tags = {"丢单原因" },  notes = "获取丢单原因草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/getdraft")
    public ResponseEntity<Crm_lost_reasonDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reasonMapping.toDto(crm_lost_reasonService.getDraft(new Crm_lost_reason())));
    }

    @ApiOperation(value = "检查丢单原因", tags = {"丢单原因" },  notes = "检查丢单原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_lost_reasonService.checkKey(crm_lost_reasonMapping.toDomain(crm_lost_reasondto)));
    }

    @PreAuthorize("hasPermission(this.crm_lost_reasonMapping.toDomain(#crm_lost_reasondto),'iBizBusinessCentral-Crm_lost_reason-Save')")
    @ApiOperation(value = "保存丢单原因", tags = {"丢单原因" },  notes = "保存丢单原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reasonService.save(crm_lost_reasonMapping.toDomain(crm_lost_reasondto)));
    }

    @PreAuthorize("hasPermission(this.crm_lost_reasonMapping.toDomain(#crm_lost_reasondtos),'iBizBusinessCentral-Crm_lost_reason-Save')")
    @ApiOperation(value = "批量保存丢单原因", tags = {"丢单原因" },  notes = "批量保存丢单原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_lost_reasonDTO> crm_lost_reasondtos) {
        crm_lost_reasonService.saveBatch(crm_lost_reasonMapping.toDomain(crm_lost_reasondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lost_reason-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lost_reason-Get')")
	@ApiOperation(value = "获取数据集", tags = {"丢单原因" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lost_reasons/fetchdefault")
	public ResponseEntity<List<Crm_lost_reasonDTO>> fetchDefault(Crm_lost_reasonSearchContext context) {
        Page<Crm_lost_reason> domains = crm_lost_reasonService.searchDefault(context) ;
        List<Crm_lost_reasonDTO> list = crm_lost_reasonMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lost_reason-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lost_reason-Get')")
	@ApiOperation(value = "查询数据集", tags = {"丢单原因" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lost_reasons/searchdefault")
	public ResponseEntity<Page<Crm_lost_reasonDTO>> searchDefault(@RequestBody Crm_lost_reasonSearchContext context) {
        Page<Crm_lost_reason> domains = crm_lost_reasonService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lost_reasonMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

