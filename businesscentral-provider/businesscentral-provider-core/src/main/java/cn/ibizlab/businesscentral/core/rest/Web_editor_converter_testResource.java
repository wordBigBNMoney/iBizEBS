package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.businesscentral.core.odoo_web_editor.service.IWeb_editor_converter_testService;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Web编辑器转换器测试" })
@RestController("Core-web_editor_converter_test")
@RequestMapping("")
public class Web_editor_converter_testResource {

    @Autowired
    public IWeb_editor_converter_testService web_editor_converter_testService;

    @Autowired
    @Lazy
    public Web_editor_converter_testMapping web_editor_converter_testMapping;

    @PreAuthorize("hasPermission(this.web_editor_converter_testMapping.toDomain(#web_editor_converter_testdto),'iBizBusinessCentral-Web_editor_converter_test-Create')")
    @ApiOperation(value = "新建Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "新建Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests")
    public ResponseEntity<Web_editor_converter_testDTO> create(@Validated @RequestBody Web_editor_converter_testDTO web_editor_converter_testdto) {
        Web_editor_converter_test domain = web_editor_converter_testMapping.toDomain(web_editor_converter_testdto);
		web_editor_converter_testService.create(domain);
        Web_editor_converter_testDTO dto = web_editor_converter_testMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_testMapping.toDomain(#web_editor_converter_testdtos),'iBizBusinessCentral-Web_editor_converter_test-Create')")
    @ApiOperation(value = "批量新建Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "批量新建Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Web_editor_converter_testDTO> web_editor_converter_testdtos) {
        web_editor_converter_testService.createBatch(web_editor_converter_testMapping.toDomain(web_editor_converter_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "web_editor_converter_test" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.web_editor_converter_testService.get(#web_editor_converter_test_id),'iBizBusinessCentral-Web_editor_converter_test-Update')")
    @ApiOperation(value = "更新Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "更新Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_tests/{web_editor_converter_test_id}")
    public ResponseEntity<Web_editor_converter_testDTO> update(@PathVariable("web_editor_converter_test_id") Long web_editor_converter_test_id, @RequestBody Web_editor_converter_testDTO web_editor_converter_testdto) {
		Web_editor_converter_test domain  = web_editor_converter_testMapping.toDomain(web_editor_converter_testdto);
        domain .setId(web_editor_converter_test_id);
		web_editor_converter_testService.update(domain );
		Web_editor_converter_testDTO dto = web_editor_converter_testMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_testService.getWebEditorConverterTestByEntities(this.web_editor_converter_testMapping.toDomain(#web_editor_converter_testdtos)),'iBizBusinessCentral-Web_editor_converter_test-Update')")
    @ApiOperation(value = "批量更新Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "批量更新Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_tests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_editor_converter_testDTO> web_editor_converter_testdtos) {
        web_editor_converter_testService.updateBatch(web_editor_converter_testMapping.toDomain(web_editor_converter_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_testService.get(#web_editor_converter_test_id),'iBizBusinessCentral-Web_editor_converter_test-Remove')")
    @ApiOperation(value = "删除Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "删除Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_tests/{web_editor_converter_test_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("web_editor_converter_test_id") Long web_editor_converter_test_id) {
         return ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_testService.remove(web_editor_converter_test_id));
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_testService.getWebEditorConverterTestByIds(#ids),'iBizBusinessCentral-Web_editor_converter_test-Remove')")
    @ApiOperation(value = "批量删除Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "批量删除Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_tests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        web_editor_converter_testService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.web_editor_converter_testMapping.toDomain(returnObject.body),'iBizBusinessCentral-Web_editor_converter_test-Get')")
    @ApiOperation(value = "获取Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "获取Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_tests/{web_editor_converter_test_id}")
    public ResponseEntity<Web_editor_converter_testDTO> get(@PathVariable("web_editor_converter_test_id") Long web_editor_converter_test_id) {
        Web_editor_converter_test domain = web_editor_converter_testService.get(web_editor_converter_test_id);
        Web_editor_converter_testDTO dto = web_editor_converter_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Web编辑器转换器测试草稿", tags = {"Web编辑器转换器测试" },  notes = "获取Web编辑器转换器测试草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_tests/getdraft")
    public ResponseEntity<Web_editor_converter_testDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_testMapping.toDto(web_editor_converter_testService.getDraft(new Web_editor_converter_test())));
    }

    @ApiOperation(value = "检查Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "检查Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Web_editor_converter_testDTO web_editor_converter_testdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_testService.checkKey(web_editor_converter_testMapping.toDomain(web_editor_converter_testdto)));
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_testMapping.toDomain(#web_editor_converter_testdto),'iBizBusinessCentral-Web_editor_converter_test-Save')")
    @ApiOperation(value = "保存Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "保存Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/save")
    public ResponseEntity<Boolean> save(@RequestBody Web_editor_converter_testDTO web_editor_converter_testdto) {
        return ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_testService.save(web_editor_converter_testMapping.toDomain(web_editor_converter_testdto)));
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_testMapping.toDomain(#web_editor_converter_testdtos),'iBizBusinessCentral-Web_editor_converter_test-Save')")
    @ApiOperation(value = "批量保存Web编辑器转换器测试", tags = {"Web编辑器转换器测试" },  notes = "批量保存Web编辑器转换器测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Web_editor_converter_testDTO> web_editor_converter_testdtos) {
        web_editor_converter_testService.saveBatch(web_editor_converter_testMapping.toDomain(web_editor_converter_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_editor_converter_test-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Web_editor_converter_test-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Web编辑器转换器测试" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/web_editor_converter_tests/fetchdefault")
	public ResponseEntity<List<Web_editor_converter_testDTO>> fetchDefault(Web_editor_converter_testSearchContext context) {
        Page<Web_editor_converter_test> domains = web_editor_converter_testService.searchDefault(context) ;
        List<Web_editor_converter_testDTO> list = web_editor_converter_testMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_editor_converter_test-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Web_editor_converter_test-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Web编辑器转换器测试" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/web_editor_converter_tests/searchdefault")
	public ResponseEntity<Page<Web_editor_converter_testDTO>> searchDefault(@RequestBody Web_editor_converter_testSearchContext context) {
        Page<Web_editor_converter_test> domains = web_editor_converter_testService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(web_editor_converter_testMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

