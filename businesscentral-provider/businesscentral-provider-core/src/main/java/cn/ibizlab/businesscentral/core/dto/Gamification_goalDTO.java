package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Gamification_goalDTO]
 */
@Data
public class Gamification_goalDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 属性 [TO_UPDATE]
     *
     */
    @JSONField(name = "to_update")
    @JsonProperty("to_update")
    private Boolean toUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [REMIND_UPDATE_DELAY]
     *
     */
    @JSONField(name = "remind_update_delay")
    @JsonProperty("remind_update_delay")
    private Integer remindUpdateDelay;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @NotBlank(message = "[状态]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [COMPLETENESS]
     *
     */
    @JSONField(name = "completeness")
    @JsonProperty("completeness")
    private Double completeness;

    /**
     * 属性 [TARGET_GOAL]
     *
     */
    @JSONField(name = "target_goal")
    @JsonProperty("target_goal")
    @NotNull(message = "[达到]不允许为空!")
    private Double targetGoal;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [CLOSED]
     *
     */
    @JSONField(name = "closed")
    @JsonProperty("closed")
    private Boolean closed;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_update" , format="yyyy-MM-dd")
    @JsonProperty("last_update")
    private Timestamp lastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [END_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "end_date" , format="yyyy-MM-dd")
    @JsonProperty("end_date")
    private Timestamp endDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CURRENT]
     *
     */
    @JSONField(name = "current")
    @JsonProperty("current")
    @NotNull(message = "[当前值]不允许为空!")
    private Double current;

    /**
     * 属性 [DEFINITION_DESCRIPTION]
     *
     */
    @JSONField(name = "definition_description")
    @JsonProperty("definition_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String definitionDescription;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [DEFINITION_SUFFIX]
     *
     */
    @JSONField(name = "definition_suffix")
    @JsonProperty("definition_suffix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String definitionSuffix;

    /**
     * 属性 [CHALLENGE_ID_TEXT]
     *
     */
    @JSONField(name = "challenge_id_text")
    @JsonProperty("challenge_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String challengeIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [DEFINITION_ID_TEXT]
     *
     */
    @JSONField(name = "definition_id_text")
    @JsonProperty("definition_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String definitionIdText;

    /**
     * 属性 [DEFINITION_DISPLAY]
     *
     */
    @JSONField(name = "definition_display")
    @JsonProperty("definition_display")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String definitionDisplay;

    /**
     * 属性 [LINE_ID_TEXT]
     *
     */
    @JSONField(name = "line_id_text")
    @JsonProperty("line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lineIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [DEFINITION_CONDITION]
     *
     */
    @JSONField(name = "definition_condition")
    @JsonProperty("definition_condition")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String definitionCondition;

    /**
     * 属性 [COMPUTATION_MODE]
     *
     */
    @JSONField(name = "computation_mode")
    @JsonProperty("computation_mode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String computationMode;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [LINE_ID]
     *
     */
    @JSONField(name = "line_id")
    @JsonProperty("line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lineId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [DEFINITION_ID]
     *
     */
    @JSONField(name = "definition_id")
    @JsonProperty("definition_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[目标定义]不允许为空!")
    private Long definitionId;

    /**
     * 属性 [CHALLENGE_ID]
     *
     */
    @JSONField(name = "challenge_id")
    @JsonProperty("challenge_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long challengeId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[用户]不允许为空!")
    private Long userId;


    /**
     * 设置 [START_DATE]
     */
    public void setStartDate(Timestamp  startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 设置 [TO_UPDATE]
     */
    public void setToUpdate(Boolean  toUpdate){
        this.toUpdate = toUpdate ;
        this.modify("to_update",toUpdate);
    }

    /**
     * 设置 [REMIND_UPDATE_DELAY]
     */
    public void setRemindUpdateDelay(Integer  remindUpdateDelay){
        this.remindUpdateDelay = remindUpdateDelay ;
        this.modify("remind_update_delay",remindUpdateDelay);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [TARGET_GOAL]
     */
    public void setTargetGoal(Double  targetGoal){
        this.targetGoal = targetGoal ;
        this.modify("target_goal",targetGoal);
    }

    /**
     * 设置 [CLOSED]
     */
    public void setClosed(Boolean  closed){
        this.closed = closed ;
        this.modify("closed",closed);
    }

    /**
     * 设置 [LAST_UPDATE]
     */
    public void setLastUpdate(Timestamp  lastUpdate){
        this.lastUpdate = lastUpdate ;
        this.modify("last_update",lastUpdate);
    }

    /**
     * 设置 [END_DATE]
     */
    public void setEndDate(Timestamp  endDate){
        this.endDate = endDate ;
        this.modify("end_date",endDate);
    }

    /**
     * 设置 [CURRENT]
     */
    public void setCurrent(Double  current){
        this.current = current ;
        this.modify("current",current);
    }

    /**
     * 设置 [LINE_ID]
     */
    public void setLineId(Long  lineId){
        this.lineId = lineId ;
        this.modify("line_id",lineId);
    }

    /**
     * 设置 [DEFINITION_ID]
     */
    public void setDefinitionId(Long  definitionId){
        this.definitionId = definitionId ;
        this.modify("definition_id",definitionId);
    }

    /**
     * 设置 [CHALLENGE_ID]
     */
    public void setChallengeId(Long  challengeId){
        this.challengeId = challengeId ;
        this.modify("challenge_id",challengeId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


}


