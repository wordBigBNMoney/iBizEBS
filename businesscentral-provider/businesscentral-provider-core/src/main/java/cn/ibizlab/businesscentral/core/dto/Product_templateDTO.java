package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Product_templateDTO]
 */
@Data
public class Product_templateDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [WEBSITE_STYLE_IDS]
     *
     */
    @JSONField(name = "website_style_ids")
    @JsonProperty("website_style_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteStyleIds;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteUrl;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [PURCHASE_OK]
     *
     */
    @JSONField(name = "purchase_ok")
    @JsonProperty("purchase_ok")
    private Boolean purchaseOk;

    /**
     * 属性 [NBR_REORDERING_RULES]
     *
     */
    @JSONField(name = "nbr_reordering_rules")
    @JsonProperty("nbr_reordering_rules")
    private Integer nbrReorderingRules;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [PACKAGING_IDS]
     *
     */
    @JSONField(name = "packaging_ids")
    @JsonProperty("packaging_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String packagingIds;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [RATING_LAST_FEEDBACK]
     *
     */
    @JSONField(name = "rating_last_feedback")
    @JsonProperty("rating_last_feedback")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String ratingLastFeedback;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_WNVA_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_wnva_ids")
    @JsonProperty("valid_product_attribute_wnva_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validProductAttributeWnvaIds;

    /**
     * 属性 [AVAILABLE_THRESHOLD]
     *
     */
    @JSONField(name = "available_threshold")
    @JsonProperty("available_threshold")
    private Double availableThreshold;

    /**
     * 属性 [PURCHASE_LINE_WARN]
     *
     */
    @JSONField(name = "purchase_line_warn")
    @JsonProperty("purchase_line_warn")
    @NotBlank(message = "[采购订单行]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purchaseLineWarn;

    /**
     * 属性 [SELLER_IDS]
     *
     */
    @JSONField(name = "seller_ids")
    @JsonProperty("seller_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String sellerIds;

    /**
     * 属性 [MRP_PRODUCT_QTY]
     *
     */
    @JSONField(name = "mrp_product_qty")
    @JsonProperty("mrp_product_qty")
    private Double mrpProductQty;

    /**
     * 属性 [PRODUCT_VARIANT_ID]
     *
     */
    @JSONField(name = "product_variant_id")
    @JsonProperty("product_variant_id")
    private Integer productVariantId;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [RATING_COUNT]
     *
     */
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;

    /**
     * 属性 [PROPERTY_COST_METHOD]
     *
     */
    @JSONField(name = "property_cost_method")
    @JsonProperty("property_cost_method")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyCostMethod;

    /**
     * 属性 [SERVICE_TO_PURCHASE]
     *
     */
    @JSONField(name = "service_to_purchase")
    @JsonProperty("service_to_purchase")
    private Boolean serviceToPurchase;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT]
     *
     */
    @JSONField(name = "property_stock_account_input")
    @JsonProperty("property_stock_account_input")
    private Integer propertyStockAccountInput;

    /**
     * 属性 [PURCHASE_LINE_WARN_MSG]
     *
     */
    @JSONField(name = "purchase_line_warn_msg")
    @JsonProperty("purchase_line_warn_msg")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String purchaseLineWarnMsg;

    /**
     * 属性 [WEBSITE_PRICE_DIFFERENCE]
     *
     */
    @JSONField(name = "website_price_difference")
    @JsonProperty("website_price_difference")
    private Boolean websitePriceDifference;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaTitle;

    /**
     * 属性 [DESCRIPTION_PICKINGIN]
     *
     */
    @JSONField(name = "description_pickingin")
    @JsonProperty("description_pickingin")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String descriptionPickingin;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [POS_CATEG_ID]
     *
     */
    @JSONField(name = "pos_categ_id")
    @JsonProperty("pos_categ_id")
    private Integer posCategId;

    /**
     * 属性 [OPTIONAL_PRODUCT_IDS]
     *
     */
    @JSONField(name = "optional_product_ids")
    @JsonProperty("optional_product_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String optionalProductIds;

    /**
     * 属性 [WEIGHT_UOM_ID]
     *
     */
    @JSONField(name = "weight_uom_id")
    @JsonProperty("weight_uom_id")
    private Integer weightUomId;

    /**
     * 属性 [HIDE_EXPENSE_POLICY]
     *
     */
    @JSONField(name = "hide_expense_policy")
    @JsonProperty("hide_expense_policy")
    private Boolean hideExpensePolicy;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [INVOICE_POLICY]
     *
     */
    @JSONField(name = "invoice_policy")
    @JsonProperty("invoice_policy")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String invoicePolicy;

    /**
     * 属性 [WEBSITE_PRICE]
     *
     */
    @JSONField(name = "website_price")
    @JsonProperty("website_price")
    private Double websitePrice;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @NotBlank(message = "[产品类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String type;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_value_ids")
    @JsonProperty("valid_product_attribute_value_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validProductAttributeValueIds;

    /**
     * 属性 [VALID_ARCHIVED_VARIANT_IDS]
     *
     */
    @JSONField(name = "valid_archived_variant_ids")
    @JsonProperty("valid_archived_variant_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validArchivedVariantIds;

    /**
     * 属性 [EXPENSE_POLICY]
     *
     */
    @JSONField(name = "expense_policy")
    @JsonProperty("expense_policy")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String expensePolicy;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private Boolean isSeoOptimized;

    /**
     * 属性 [RATING_LAST_VALUE]
     *
     */
    @JSONField(name = "rating_last_value")
    @JsonProperty("rating_last_value")
    private Double ratingLastValue;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_ID]
     *
     */
    @JSONField(name = "property_account_income_id")
    @JsonProperty("property_account_income_id")
    private Integer propertyAccountIncomeId;

    /**
     * 属性 [ALTERNATIVE_PRODUCT_IDS]
     *
     */
    @JSONField(name = "alternative_product_ids")
    @JsonProperty("alternative_product_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String alternativeProductIds;

    /**
     * 属性 [VALID_EXISTING_VARIANT_IDS]
     *
     */
    @JSONField(name = "valid_existing_variant_ids")
    @JsonProperty("valid_existing_variant_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validExistingVariantIds;

    /**
     * 属性 [PRODUCT_VARIANT_COUNT]
     *
     */
    @JSONField(name = "product_variant_count")
    @JsonProperty("product_variant_count")
    private Integer productVariantCount;

    /**
     * 属性 [PURCHASED_PRODUCT_QTY]
     *
     */
    @JSONField(name = "purchased_product_qty")
    @JsonProperty("purchased_product_qty")
    private Double purchasedProductQty;

    /**
     * 属性 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_IDS]
     *
     */
    @JSONField(name = "valid_product_template_attribute_line_ids")
    @JsonProperty("valid_product_template_attribute_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validProductTemplateAttributeLineIds;

    /**
     * 属性 [DESCRIPTION_PICKING]
     *
     */
    @JSONField(name = "description_picking")
    @JsonProperty("description_picking")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String descriptionPicking;

    /**
     * 属性 [EVENT_OK]
     *
     */
    @JSONField(name = "event_ok")
    @JsonProperty("event_ok")
    private Boolean eventOk;

    /**
     * 属性 [STANDARD_PRICE]
     *
     */
    @JSONField(name = "standard_price")
    @JsonProperty("standard_price")
    private Double standardPrice;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [BOM_LINE_IDS]
     *
     */
    @JSONField(name = "bom_line_ids")
    @JsonProperty("bom_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String bomLineIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [RATING_LAST_IMAGE]
     *
     */
    @JSONField(name = "rating_last_image")
    @JsonProperty("rating_last_image")
    private byte[] ratingLastImage;

    /**
     * 属性 [QTY_AVAILABLE]
     *
     */
    @JSONField(name = "qty_available")
    @JsonProperty("qty_available")
    private Double qtyAvailable;

    /**
     * 属性 [COST_METHOD]
     *
     */
    @JSONField(name = "cost_method")
    @JsonProperty("cost_method")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String costMethod;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaOgImg;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaKeywords;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Integer pricelistId;

    /**
     * 属性 [WEBSITE_SIZE_X]
     *
     */
    @JSONField(name = "website_size_x")
    @JsonProperty("website_size_x")
    private Integer websiteSizeX;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [RENTAL]
     *
     */
    @JSONField(name = "rental")
    @JsonProperty("rental")
    private Boolean rental;

    /**
     * 属性 [OUTGOING_QTY]
     *
     */
    @JSONField(name = "outgoing_qty")
    @JsonProperty("outgoing_qty")
    private Double outgoingQty;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT]
     *
     */
    @JSONField(name = "property_stock_account_output")
    @JsonProperty("property_stock_account_output")
    private Integer propertyStockAccountOutput;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String routeIds;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_ID]
     *
     */
    @JSONField(name = "property_account_expense_id")
    @JsonProperty("property_account_expense_id")
    private Integer propertyAccountExpenseId;

    /**
     * 属性 [SALES_COUNT]
     *
     */
    @JSONField(name = "sales_count")
    @JsonProperty("sales_count")
    private Double salesCount;

    /**
     * 属性 [REORDERING_MIN_QTY]
     *
     */
    @JSONField(name = "reordering_min_qty")
    @JsonProperty("reordering_min_qty")
    private Double reorderingMinQty;

    /**
     * 属性 [TO_WEIGHT]
     *
     */
    @JSONField(name = "to_weight")
    @JsonProperty("to_weight")
    private Boolean toWeight;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_WNVA_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_value_wnva_ids")
    @JsonProperty("valid_product_attribute_value_wnva_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validProductAttributeValueWnvaIds;

    /**
     * 属性 [ITEM_IDS]
     *
     */
    @JSONField(name = "item_ids")
    @JsonProperty("item_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String itemIds;

    /**
     * 属性 [SUPPLIER_TAXES_ID]
     *
     */
    @JSONField(name = "supplier_taxes_id")
    @JsonProperty("supplier_taxes_id")
    private String supplierTaxesId;
    /**
     * 属性 [VOLUME]
     *
     */
    @JSONField(name = "volume")
    @JsonProperty("volume")
    private Double volume;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [PROPERTY_STOCK_PRODUCTION]
     *
     */
    @JSONField(name = "property_stock_production")
    @JsonProperty("property_stock_production")
    private Integer propertyStockProduction;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [PROPERTY_STOCK_INVENTORY]
     *
     */
    @JSONField(name = "property_stock_inventory")
    @JsonProperty("property_stock_inventory")
    private Integer propertyStockInventory;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [SALE_LINE_WARN]
     *
     */
    @JSONField(name = "sale_line_warn")
    @JsonProperty("sale_line_warn")
    @NotBlank(message = "[销售订单行]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleLineWarn;

    /**
     * 属性 [LIST_PRICE]
     *
     */
    @JSONField(name = "list_price")
    @JsonProperty("list_price")
    private Double listPrice;

    /**
     * 属性 [PUBLIC_CATEG_IDS]
     *
     */
    @JSONField(name = "public_categ_ids")
    @JsonProperty("public_categ_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String publicCategIds;

    /**
     * 属性 [VALUATION]
     *
     */
    @JSONField(name = "valuation")
    @JsonProperty("valuation")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String valuation;

    /**
     * 属性 [DESCRIPTION_PICKINGOUT]
     *
     */
    @JSONField(name = "description_pickingout")
    @JsonProperty("description_pickingout")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String descriptionPickingout;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [ACCESSORY_PRODUCT_IDS]
     *
     */
    @JSONField(name = "accessory_product_ids")
    @JsonProperty("accessory_product_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String accessoryProductIds;

    /**
     * 属性 [ROUTE_FROM_CATEG_IDS]
     *
     */
    @JSONField(name = "route_from_categ_ids")
    @JsonProperty("route_from_categ_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String routeFromCategIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WEBSITE_SEQUENCE]
     *
     */
    @JSONField(name = "website_sequence")
    @JsonProperty("website_sequence")
    private Integer websiteSequence;

    /**
     * 属性 [IS_PRODUCT_VARIANT]
     *
     */
    @JSONField(name = "is_product_variant")
    @JsonProperty("is_product_variant")
    private Boolean isProductVariant;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [INCOMING_QTY]
     *
     */
    @JSONField(name = "incoming_qty")
    @JsonProperty("incoming_qty")
    private Double incomingQty;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String ratingIds;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMetaDescription;

    /**
     * 属性 [BOM_IDS]
     *
     */
    @JSONField(name = "bom_ids")
    @JsonProperty("bom_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String bomIds;

    /**
     * 属性 [SALE_LINE_WARN_MSG]
     *
     */
    @JSONField(name = "sale_line_warn_msg")
    @JsonProperty("sale_line_warn_msg")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String saleLineWarnMsg;

    /**
     * 属性 [PURCHASE_METHOD]
     *
     */
    @JSONField(name = "purchase_method")
    @JsonProperty("purchase_method")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purchaseMethod;

    /**
     * 属性 [PRODUCE_DELAY]
     *
     */
    @JSONField(name = "produce_delay")
    @JsonProperty("produce_delay")
    private Double produceDelay;

    /**
     * 属性 [BOM_COUNT]
     *
     */
    @JSONField(name = "bom_count")
    @JsonProperty("bom_count")
    private Integer bomCount;

    /**
     * 属性 [TAXES_ID]
     *
     */
    @JSONField(name = "taxes_id")
    @JsonProperty("taxes_id")
    private String taxesId;
    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [CAN_BE_EXPENSED]
     *
     */
    @JSONField(name = "can_be_expensed")
    @JsonProperty("can_be_expensed")
    private Boolean canBeExpensed;

    /**
     * 属性 [SALE_OK]
     *
     */
    @JSONField(name = "sale_ok")
    @JsonProperty("sale_ok")
    private Boolean saleOk;

    /**
     * 属性 [SERVICE_TYPE]
     *
     */
    @JSONField(name = "service_type")
    @JsonProperty("service_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String serviceType;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [TRACKING]
     *
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    @NotBlank(message = "[追踪]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tracking;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_WNVA_IDS]
     *
     */
    @JSONField(name = "valid_product_template_attribute_line_wnva_ids")
    @JsonProperty("valid_product_template_attribute_line_wnva_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validProductTemplateAttributeLineWnvaIds;

    /**
     * 属性 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE]
     *
     */
    @JSONField(name = "property_account_creditor_price_difference")
    @JsonProperty("property_account_creditor_price_difference")
    private Integer propertyAccountCreditorPriceDifference;

    /**
     * 属性 [INVENTORY_AVAILABILITY]
     *
     */
    @JSONField(name = "inventory_availability")
    @JsonProperty("inventory_availability")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String inventoryAvailability;

    /**
     * 属性 [WEBSITE_SIZE_Y]
     *
     */
    @JSONField(name = "website_size_y")
    @JsonProperty("website_size_y")
    private Integer websiteSizeY;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_ids")
    @JsonProperty("valid_product_attribute_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validProductAttributeIds;

    /**
     * 属性 [LST_PRICE]
     *
     */
    @JSONField(name = "lst_price")
    @JsonProperty("lst_price")
    private Double lstPrice;

    /**
     * 属性 [CUSTOM_MESSAGE]
     *
     */
    @JSONField(name = "custom_message")
    @JsonProperty("custom_message")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String customMessage;

    /**
     * 属性 [AVAILABLE_IN_POS]
     *
     */
    @JSONField(name = "available_in_pos")
    @JsonProperty("available_in_pos")
    private Boolean availableInPos;

    /**
     * 属性 [WEIGHT_UOM_NAME]
     *
     */
    @JSONField(name = "weight_uom_name")
    @JsonProperty("weight_uom_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String weightUomName;

    /**
     * 属性 [COST_CURRENCY_ID]
     *
     */
    @JSONField(name = "cost_currency_id")
    @JsonProperty("cost_currency_id")
    private Integer costCurrencyId;

    /**
     * 属性 [ATTRIBUTE_LINE_IDS]
     *
     */
    @JSONField(name = "attribute_line_ids")
    @JsonProperty("attribute_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String attributeLineIds;

    /**
     * 属性 [WEIGHT]
     *
     */
    @JSONField(name = "weight")
    @JsonProperty("weight")
    private Double weight;

    /**
     * 属性 [VIRTUAL_AVAILABLE]
     *
     */
    @JSONField(name = "virtual_available")
    @JsonProperty("virtual_available")
    private Double virtualAvailable;

    /**
     * 属性 [PRODUCT_IMAGE_IDS]
     *
     */
    @JSONField(name = "product_image_ids")
    @JsonProperty("product_image_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String productImageIds;

    /**
     * 属性 [USED_IN_BOM_COUNT]
     *
     */
    @JSONField(name = "used_in_bom_count")
    @JsonProperty("used_in_bom_count")
    private Integer usedInBomCount;

    /**
     * 属性 [DEFAULT_CODE]
     *
     */
    @JSONField(name = "default_code")
    @JsonProperty("default_code")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String defaultCode;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String barcode;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [PROPERTY_VALUATION]
     *
     */
    @JSONField(name = "property_valuation")
    @JsonProperty("property_valuation")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyValuation;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteDescription;

    /**
     * 属性 [PRODUCT_VARIANT_IDS]
     *
     */
    @JSONField(name = "product_variant_ids")
    @JsonProperty("product_variant_ids")
    @NotBlank(message = "[产品]不允许为空!")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String productVariantIds;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;

    /**
     * 属性 [WEBSITE_PUBLIC_PRICE]
     *
     */
    @JSONField(name = "website_public_price")
    @JsonProperty("website_public_price")
    private Double websitePublicPrice;

    /**
     * 属性 [REORDERING_MAX_QTY]
     *
     */
    @JSONField(name = "reordering_max_qty")
    @JsonProperty("reordering_max_qty")
    private Double reorderingMaxQty;

    /**
     * 属性 [SALE_DELAY]
     *
     */
    @JSONField(name = "sale_delay")
    @JsonProperty("sale_delay")
    private Double saleDelay;

    /**
     * 属性 [VARIANT_SELLER_IDS]
     *
     */
    @JSONField(name = "variant_seller_ids")
    @JsonProperty("variant_seller_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String variantSellerIds;

    /**
     * 属性 [DESCRIPTION_PURCHASE]
     *
     */
    @JSONField(name = "description_purchase")
    @JsonProperty("description_purchase")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String descriptionPurchase;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [DESCRIPTION_SALE]
     *
     */
    @JSONField(name = "description_sale")
    @JsonProperty("description_sale")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String descriptionSale;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [UOM_PO_ID_TEXT]
     *
     */
    @JSONField(name = "uom_po_id_text")
    @JsonProperty("uom_po_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String uomPoIdText;

    /**
     * 属性 [UOM_NAME]
     *
     */
    @JSONField(name = "uom_name")
    @JsonProperty("uom_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String uomName;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String categIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[产品种类]不允许为空!")
    private Long categId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [UOM_ID]
     *
     */
    @JSONField(name = "uom_id")
    @JsonProperty("uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[计量单位]不允许为空!")
    private Long uomId;

    /**
     * 属性 [UOM_PO_ID]
     *
     */
    @JSONField(name = "uom_po_id")
    @JsonProperty("uom_po_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[采购计量单位]不允许为空!")
    private Long uomPoId;


    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [PURCHASE_OK]
     */
    public void setPurchaseOk(Boolean  purchaseOk){
        this.purchaseOk = purchaseOk ;
        this.modify("purchase_ok",purchaseOk);
    }

    /**
     * 设置 [AVAILABLE_THRESHOLD]
     */
    public void setAvailableThreshold(Double  availableThreshold){
        this.availableThreshold = availableThreshold ;
        this.modify("available_threshold",availableThreshold);
    }

    /**
     * 设置 [PURCHASE_LINE_WARN]
     */
    public void setPurchaseLineWarn(String  purchaseLineWarn){
        this.purchaseLineWarn = purchaseLineWarn ;
        this.modify("purchase_line_warn",purchaseLineWarn);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [SERVICE_TO_PURCHASE]
     */
    public void setServiceToPurchase(Boolean  serviceToPurchase){
        this.serviceToPurchase = serviceToPurchase ;
        this.modify("service_to_purchase",serviceToPurchase);
    }

    /**
     * 设置 [PURCHASE_LINE_WARN_MSG]
     */
    public void setPurchaseLineWarnMsg(String  purchaseLineWarnMsg){
        this.purchaseLineWarnMsg = purchaseLineWarnMsg ;
        this.modify("purchase_line_warn_msg",purchaseLineWarnMsg);
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    public void setWebsiteMetaTitle(String  websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }

    /**
     * 设置 [DESCRIPTION_PICKINGIN]
     */
    public void setDescriptionPickingin(String  descriptionPickingin){
        this.descriptionPickingin = descriptionPickingin ;
        this.modify("description_pickingin",descriptionPickingin);
    }

    /**
     * 设置 [POS_CATEG_ID]
     */
    public void setPosCategId(Integer  posCategId){
        this.posCategId = posCategId ;
        this.modify("pos_categ_id",posCategId);
    }

    /**
     * 设置 [INVOICE_POLICY]
     */
    public void setInvoicePolicy(String  invoicePolicy){
        this.invoicePolicy = invoicePolicy ;
        this.modify("invoice_policy",invoicePolicy);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [EXPENSE_POLICY]
     */
    public void setExpensePolicy(String  expensePolicy){
        this.expensePolicy = expensePolicy ;
        this.modify("expense_policy",expensePolicy);
    }

    /**
     * 设置 [RATING_LAST_VALUE]
     */
    public void setRatingLastValue(Double  ratingLastValue){
        this.ratingLastValue = ratingLastValue ;
        this.modify("rating_last_value",ratingLastValue);
    }

    /**
     * 设置 [DESCRIPTION_PICKING]
     */
    public void setDescriptionPicking(String  descriptionPicking){
        this.descriptionPicking = descriptionPicking ;
        this.modify("description_picking",descriptionPicking);
    }

    /**
     * 设置 [EVENT_OK]
     */
    public void setEventOk(Boolean  eventOk){
        this.eventOk = eventOk ;
        this.modify("event_ok",eventOk);
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    public void setWebsiteMetaOgImg(String  websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    public void setWebsiteMetaKeywords(String  websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }

    /**
     * 设置 [WEBSITE_SIZE_X]
     */
    public void setWebsiteSizeX(Integer  websiteSizeX){
        this.websiteSizeX = websiteSizeX ;
        this.modify("website_size_x",websiteSizeX);
    }

    /**
     * 设置 [RENTAL]
     */
    public void setRental(Boolean  rental){
        this.rental = rental ;
        this.modify("rental",rental);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [TO_WEIGHT]
     */
    public void setToWeight(Boolean  toWeight){
        this.toWeight = toWeight ;
        this.modify("to_weight",toWeight);
    }

    /**
     * 设置 [SUPPLIER_TAXES_ID]
     */
    public void setSupplierTaxesId(String  supplierTaxesId){
        this.supplierTaxesId = supplierTaxesId ;
        this.modify("supplier_taxes_id",supplierTaxesId);
    }

    /**
     * 设置 [VOLUME]
     */
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.modify("volume",volume);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [SALE_LINE_WARN]
     */
    public void setSaleLineWarn(String  saleLineWarn){
        this.saleLineWarn = saleLineWarn ;
        this.modify("sale_line_warn",saleLineWarn);
    }

    /**
     * 设置 [LIST_PRICE]
     */
    public void setListPrice(Double  listPrice){
        this.listPrice = listPrice ;
        this.modify("list_price",listPrice);
    }

    /**
     * 设置 [DESCRIPTION_PICKINGOUT]
     */
    public void setDescriptionPickingout(String  descriptionPickingout){
        this.descriptionPickingout = descriptionPickingout ;
        this.modify("description_pickingout",descriptionPickingout);
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    public void setIsPublished(Boolean  isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [WEBSITE_SEQUENCE]
     */
    public void setWebsiteSequence(Integer  websiteSequence){
        this.websiteSequence = websiteSequence ;
        this.modify("website_sequence",websiteSequence);
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    public void setWebsiteMetaDescription(String  websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }

    /**
     * 设置 [SALE_LINE_WARN_MSG]
     */
    public void setSaleLineWarnMsg(String  saleLineWarnMsg){
        this.saleLineWarnMsg = saleLineWarnMsg ;
        this.modify("sale_line_warn_msg",saleLineWarnMsg);
    }

    /**
     * 设置 [PURCHASE_METHOD]
     */
    public void setPurchaseMethod(String  purchaseMethod){
        this.purchaseMethod = purchaseMethod ;
        this.modify("purchase_method",purchaseMethod);
    }

    /**
     * 设置 [PRODUCE_DELAY]
     */
    public void setProduceDelay(Double  produceDelay){
        this.produceDelay = produceDelay ;
        this.modify("produce_delay",produceDelay);
    }

    /**
     * 设置 [TAXES_ID]
     */
    public void setTaxesId(String  taxesId){
        this.taxesId = taxesId ;
        this.modify("taxes_id",taxesId);
    }

    /**
     * 设置 [CAN_BE_EXPENSED]
     */
    public void setCanBeExpensed(Boolean  canBeExpensed){
        this.canBeExpensed = canBeExpensed ;
        this.modify("can_be_expensed",canBeExpensed);
    }

    /**
     * 设置 [SALE_OK]
     */
    public void setSaleOk(Boolean  saleOk){
        this.saleOk = saleOk ;
        this.modify("sale_ok",saleOk);
    }

    /**
     * 设置 [SERVICE_TYPE]
     */
    public void setServiceType(String  serviceType){
        this.serviceType = serviceType ;
        this.modify("service_type",serviceType);
    }

    /**
     * 设置 [TRACKING]
     */
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.modify("tracking",tracking);
    }

    /**
     * 设置 [INVENTORY_AVAILABILITY]
     */
    public void setInventoryAvailability(String  inventoryAvailability){
        this.inventoryAvailability = inventoryAvailability ;
        this.modify("inventory_availability",inventoryAvailability);
    }

    /**
     * 设置 [WEBSITE_SIZE_Y]
     */
    public void setWebsiteSizeY(Integer  websiteSizeY){
        this.websiteSizeY = websiteSizeY ;
        this.modify("website_size_y",websiteSizeY);
    }

    /**
     * 设置 [CUSTOM_MESSAGE]
     */
    public void setCustomMessage(String  customMessage){
        this.customMessage = customMessage ;
        this.modify("custom_message",customMessage);
    }

    /**
     * 设置 [AVAILABLE_IN_POS]
     */
    public void setAvailableInPos(Boolean  availableInPos){
        this.availableInPos = availableInPos ;
        this.modify("available_in_pos",availableInPos);
    }

    /**
     * 设置 [WEIGHT]
     */
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.modify("weight",weight);
    }

    /**
     * 设置 [DEFAULT_CODE]
     */
    public void setDefaultCode(String  defaultCode){
        this.defaultCode = defaultCode ;
        this.modify("default_code",defaultCode);
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    public void setWebsiteDescription(String  websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }

    /**
     * 设置 [SALE_DELAY]
     */
    public void setSaleDelay(Double  saleDelay){
        this.saleDelay = saleDelay ;
        this.modify("sale_delay",saleDelay);
    }

    /**
     * 设置 [DESCRIPTION_PURCHASE]
     */
    public void setDescriptionPurchase(String  descriptionPurchase){
        this.descriptionPurchase = descriptionPurchase ;
        this.modify("description_purchase",descriptionPurchase);
    }

    /**
     * 设置 [DESCRIPTION_SALE]
     */
    public void setDescriptionSale(String  descriptionSale){
        this.descriptionSale = descriptionSale ;
        this.modify("description_sale",descriptionSale);
    }

    /**
     * 设置 [CATEG_ID]
     */
    public void setCategId(Long  categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }

    /**
     * 设置 [UOM_ID]
     */
    public void setUomId(Long  uomId){
        this.uomId = uomId ;
        this.modify("uom_id",uomId);
    }

    /**
     * 设置 [UOM_PO_ID]
     */
    public void setUomPoId(Long  uomPoId){
        this.uomPoId = uomPoId ;
        this.modify("uom_po_id",uomPoId);
    }


}


