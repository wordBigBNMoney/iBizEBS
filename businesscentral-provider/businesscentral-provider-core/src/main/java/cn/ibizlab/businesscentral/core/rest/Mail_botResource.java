package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_botService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_botSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"邮件机器人" })
@RestController("Core-mail_bot")
@RequestMapping("")
public class Mail_botResource {

    @Autowired
    public IMail_botService mail_botService;

    @Autowired
    @Lazy
    public Mail_botMapping mail_botMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Create-all')")
    @ApiOperation(value = "新建邮件机器人", tags = {"邮件机器人" },  notes = "新建邮件机器人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_bots")
    public ResponseEntity<Mail_botDTO> create(@Validated @RequestBody Mail_botDTO mail_botdto) {
        Mail_bot domain = mail_botMapping.toDomain(mail_botdto);
		mail_botService.create(domain);
        Mail_botDTO dto = mail_botMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Create-all')")
    @ApiOperation(value = "批量新建邮件机器人", tags = {"邮件机器人" },  notes = "批量新建邮件机器人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_bots/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_botDTO> mail_botdtos) {
        mail_botService.createBatch(mail_botMapping.toDomain(mail_botdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Update-all')")
    @ApiOperation(value = "更新邮件机器人", tags = {"邮件机器人" },  notes = "更新邮件机器人")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_bots/{mail_bot_id}")
    public ResponseEntity<Mail_botDTO> update(@PathVariable("mail_bot_id") Long mail_bot_id, @RequestBody Mail_botDTO mail_botdto) {
		Mail_bot domain  = mail_botMapping.toDomain(mail_botdto);
        domain .setId(mail_bot_id);
		mail_botService.update(domain );
		Mail_botDTO dto = mail_botMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Update-all')")
    @ApiOperation(value = "批量更新邮件机器人", tags = {"邮件机器人" },  notes = "批量更新邮件机器人")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_bots/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_botDTO> mail_botdtos) {
        mail_botService.updateBatch(mail_botMapping.toDomain(mail_botdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Remove-all')")
    @ApiOperation(value = "删除邮件机器人", tags = {"邮件机器人" },  notes = "删除邮件机器人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_bots/{mail_bot_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_bot_id") Long mail_bot_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_botService.remove(mail_bot_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Remove-all')")
    @ApiOperation(value = "批量删除邮件机器人", tags = {"邮件机器人" },  notes = "批量删除邮件机器人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_bots/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_botService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Get-all')")
    @ApiOperation(value = "获取邮件机器人", tags = {"邮件机器人" },  notes = "获取邮件机器人")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_bots/{mail_bot_id}")
    public ResponseEntity<Mail_botDTO> get(@PathVariable("mail_bot_id") Long mail_bot_id) {
        Mail_bot domain = mail_botService.get(mail_bot_id);
        Mail_botDTO dto = mail_botMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取邮件机器人草稿", tags = {"邮件机器人" },  notes = "获取邮件机器人草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_bots/getdraft")
    public ResponseEntity<Mail_botDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_botMapping.toDto(mail_botService.getDraft(new Mail_bot())));
    }

    @ApiOperation(value = "检查邮件机器人", tags = {"邮件机器人" },  notes = "检查邮件机器人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_bots/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_botDTO mail_botdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_botService.checkKey(mail_botMapping.toDomain(mail_botdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Save-all')")
    @ApiOperation(value = "保存邮件机器人", tags = {"邮件机器人" },  notes = "保存邮件机器人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_bots/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_botDTO mail_botdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_botService.save(mail_botMapping.toDomain(mail_botdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-Save-all')")
    @ApiOperation(value = "批量保存邮件机器人", tags = {"邮件机器人" },  notes = "批量保存邮件机器人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_bots/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_botDTO> mail_botdtos) {
        mail_botService.saveBatch(mail_botMapping.toDomain(mail_botdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"邮件机器人" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_bots/fetchdefault")
	public ResponseEntity<List<Mail_botDTO>> fetchDefault(Mail_botSearchContext context) {
        Page<Mail_bot> domains = mail_botService.searchDefault(context) ;
        List<Mail_botDTO> list = mail_botMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_bot-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"邮件机器人" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_bots/searchdefault")
	public ResponseEntity<Page<Mail_botDTO>> searchDefault(@RequestBody Mail_botSearchContext context) {
        Page<Mail_bot> domains = mail_botService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_botMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

