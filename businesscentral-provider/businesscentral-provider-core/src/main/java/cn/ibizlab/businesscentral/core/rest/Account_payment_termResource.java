package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_payment_termSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"付款条款" })
@RestController("Core-account_payment_term")
@RequestMapping("")
public class Account_payment_termResource {

    @Autowired
    public IAccount_payment_termService account_payment_termService;

    @Autowired
    @Lazy
    public Account_payment_termMapping account_payment_termMapping;

    @PreAuthorize("hasPermission(this.account_payment_termMapping.toDomain(#account_payment_termdto),'iBizBusinessCentral-Account_payment_term-Create')")
    @ApiOperation(value = "新建付款条款", tags = {"付款条款" },  notes = "新建付款条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms")
    public ResponseEntity<Account_payment_termDTO> create(@Validated @RequestBody Account_payment_termDTO account_payment_termdto) {
        Account_payment_term domain = account_payment_termMapping.toDomain(account_payment_termdto);
		account_payment_termService.create(domain);
        Account_payment_termDTO dto = account_payment_termMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_payment_termMapping.toDomain(#account_payment_termdtos),'iBizBusinessCentral-Account_payment_term-Create')")
    @ApiOperation(value = "批量新建付款条款", tags = {"付款条款" },  notes = "批量新建付款条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_payment_termDTO> account_payment_termdtos) {
        account_payment_termService.createBatch(account_payment_termMapping.toDomain(account_payment_termdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_payment_term" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_payment_termService.get(#account_payment_term_id),'iBizBusinessCentral-Account_payment_term-Update')")
    @ApiOperation(value = "更新付款条款", tags = {"付款条款" },  notes = "更新付款条款")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_terms/{account_payment_term_id}")
    public ResponseEntity<Account_payment_termDTO> update(@PathVariable("account_payment_term_id") Long account_payment_term_id, @RequestBody Account_payment_termDTO account_payment_termdto) {
		Account_payment_term domain  = account_payment_termMapping.toDomain(account_payment_termdto);
        domain .setId(account_payment_term_id);
		account_payment_termService.update(domain );
		Account_payment_termDTO dto = account_payment_termMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_payment_termService.getAccountPaymentTermByEntities(this.account_payment_termMapping.toDomain(#account_payment_termdtos)),'iBizBusinessCentral-Account_payment_term-Update')")
    @ApiOperation(value = "批量更新付款条款", tags = {"付款条款" },  notes = "批量更新付款条款")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_terms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_termDTO> account_payment_termdtos) {
        account_payment_termService.updateBatch(account_payment_termMapping.toDomain(account_payment_termdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_payment_termService.get(#account_payment_term_id),'iBizBusinessCentral-Account_payment_term-Remove')")
    @ApiOperation(value = "删除付款条款", tags = {"付款条款" },  notes = "删除付款条款")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_terms/{account_payment_term_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_term_id") Long account_payment_term_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_payment_termService.remove(account_payment_term_id));
    }

    @PreAuthorize("hasPermission(this.account_payment_termService.getAccountPaymentTermByIds(#ids),'iBizBusinessCentral-Account_payment_term-Remove')")
    @ApiOperation(value = "批量删除付款条款", tags = {"付款条款" },  notes = "批量删除付款条款")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_terms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_payment_termService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_payment_termMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_payment_term-Get')")
    @ApiOperation(value = "获取付款条款", tags = {"付款条款" },  notes = "获取付款条款")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_terms/{account_payment_term_id}")
    public ResponseEntity<Account_payment_termDTO> get(@PathVariable("account_payment_term_id") Long account_payment_term_id) {
        Account_payment_term domain = account_payment_termService.get(account_payment_term_id);
        Account_payment_termDTO dto = account_payment_termMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取付款条款草稿", tags = {"付款条款" },  notes = "获取付款条款草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_terms/getdraft")
    public ResponseEntity<Account_payment_termDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_payment_termMapping.toDto(account_payment_termService.getDraft(new Account_payment_term())));
    }

    @ApiOperation(value = "检查付款条款", tags = {"付款条款" },  notes = "检查付款条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_payment_termDTO account_payment_termdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_payment_termService.checkKey(account_payment_termMapping.toDomain(account_payment_termdto)));
    }

    @PreAuthorize("hasPermission(this.account_payment_termMapping.toDomain(#account_payment_termdto),'iBizBusinessCentral-Account_payment_term-Save')")
    @ApiOperation(value = "保存付款条款", tags = {"付款条款" },  notes = "保存付款条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_payment_termDTO account_payment_termdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_payment_termService.save(account_payment_termMapping.toDomain(account_payment_termdto)));
    }

    @PreAuthorize("hasPermission(this.account_payment_termMapping.toDomain(#account_payment_termdtos),'iBizBusinessCentral-Account_payment_term-Save')")
    @ApiOperation(value = "批量保存付款条款", tags = {"付款条款" },  notes = "批量保存付款条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_payment_termDTO> account_payment_termdtos) {
        account_payment_termService.saveBatch(account_payment_termMapping.toDomain(account_payment_termdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_payment_term-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_payment_term-Get')")
	@ApiOperation(value = "获取数据集", tags = {"付款条款" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_payment_terms/fetchdefault")
	public ResponseEntity<List<Account_payment_termDTO>> fetchDefault(Account_payment_termSearchContext context) {
        Page<Account_payment_term> domains = account_payment_termService.searchDefault(context) ;
        List<Account_payment_termDTO> list = account_payment_termMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_payment_term-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_payment_term-Get')")
	@ApiOperation(value = "查询数据集", tags = {"付款条款" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_payment_terms/searchdefault")
	public ResponseEntity<Page<Account_payment_termDTO>> searchDefault(@RequestBody Account_payment_termSearchContext context) {
        Page<Account_payment_term> domains = account_payment_termService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_payment_termMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

