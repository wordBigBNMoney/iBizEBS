package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_o2m_child;
import cn.ibizlab.businesscentral.core.dto.Base_import_tests_models_o2m_childDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreBase_import_tests_models_o2m_childMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_import_tests_models_o2m_childMapping extends MappingBase<Base_import_tests_models_o2m_childDTO, Base_import_tests_models_o2m_child> {


}

