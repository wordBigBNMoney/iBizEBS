package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_language_exportService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_exportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"语言输出" })
@RestController("Core-base_language_export")
@RequestMapping("")
public class Base_language_exportResource {

    @Autowired
    public IBase_language_exportService base_language_exportService;

    @Autowired
    @Lazy
    public Base_language_exportMapping base_language_exportMapping;

    @PreAuthorize("hasPermission(this.base_language_exportMapping.toDomain(#base_language_exportdto),'iBizBusinessCentral-Base_language_export-Create')")
    @ApiOperation(value = "新建语言输出", tags = {"语言输出" },  notes = "新建语言输出")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_exports")
    public ResponseEntity<Base_language_exportDTO> create(@Validated @RequestBody Base_language_exportDTO base_language_exportdto) {
        Base_language_export domain = base_language_exportMapping.toDomain(base_language_exportdto);
		base_language_exportService.create(domain);
        Base_language_exportDTO dto = base_language_exportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_language_exportMapping.toDomain(#base_language_exportdtos),'iBizBusinessCentral-Base_language_export-Create')")
    @ApiOperation(value = "批量新建语言输出", tags = {"语言输出" },  notes = "批量新建语言输出")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_language_exportDTO> base_language_exportdtos) {
        base_language_exportService.createBatch(base_language_exportMapping.toDomain(base_language_exportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_language_export" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_language_exportService.get(#base_language_export_id),'iBizBusinessCentral-Base_language_export-Update')")
    @ApiOperation(value = "更新语言输出", tags = {"语言输出" },  notes = "更新语言输出")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_exports/{base_language_export_id}")
    public ResponseEntity<Base_language_exportDTO> update(@PathVariable("base_language_export_id") Long base_language_export_id, @RequestBody Base_language_exportDTO base_language_exportdto) {
		Base_language_export domain  = base_language_exportMapping.toDomain(base_language_exportdto);
        domain .setId(base_language_export_id);
		base_language_exportService.update(domain );
		Base_language_exportDTO dto = base_language_exportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_language_exportService.getBaseLanguageExportByEntities(this.base_language_exportMapping.toDomain(#base_language_exportdtos)),'iBizBusinessCentral-Base_language_export-Update')")
    @ApiOperation(value = "批量更新语言输出", tags = {"语言输出" },  notes = "批量更新语言输出")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_exports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_exportDTO> base_language_exportdtos) {
        base_language_exportService.updateBatch(base_language_exportMapping.toDomain(base_language_exportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_language_exportService.get(#base_language_export_id),'iBizBusinessCentral-Base_language_export-Remove')")
    @ApiOperation(value = "删除语言输出", tags = {"语言输出" },  notes = "删除语言输出")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_exports/{base_language_export_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_language_export_id") Long base_language_export_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_language_exportService.remove(base_language_export_id));
    }

    @PreAuthorize("hasPermission(this.base_language_exportService.getBaseLanguageExportByIds(#ids),'iBizBusinessCentral-Base_language_export-Remove')")
    @ApiOperation(value = "批量删除语言输出", tags = {"语言输出" },  notes = "批量删除语言输出")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_exports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_language_exportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_language_exportMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_language_export-Get')")
    @ApiOperation(value = "获取语言输出", tags = {"语言输出" },  notes = "获取语言输出")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_exports/{base_language_export_id}")
    public ResponseEntity<Base_language_exportDTO> get(@PathVariable("base_language_export_id") Long base_language_export_id) {
        Base_language_export domain = base_language_exportService.get(base_language_export_id);
        Base_language_exportDTO dto = base_language_exportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取语言输出草稿", tags = {"语言输出" },  notes = "获取语言输出草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_exports/getdraft")
    public ResponseEntity<Base_language_exportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_language_exportMapping.toDto(base_language_exportService.getDraft(new Base_language_export())));
    }

    @ApiOperation(value = "检查语言输出", tags = {"语言输出" },  notes = "检查语言输出")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_language_exportDTO base_language_exportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_language_exportService.checkKey(base_language_exportMapping.toDomain(base_language_exportdto)));
    }

    @PreAuthorize("hasPermission(this.base_language_exportMapping.toDomain(#base_language_exportdto),'iBizBusinessCentral-Base_language_export-Save')")
    @ApiOperation(value = "保存语言输出", tags = {"语言输出" },  notes = "保存语言输出")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_language_exportDTO base_language_exportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_language_exportService.save(base_language_exportMapping.toDomain(base_language_exportdto)));
    }

    @PreAuthorize("hasPermission(this.base_language_exportMapping.toDomain(#base_language_exportdtos),'iBizBusinessCentral-Base_language_export-Save')")
    @ApiOperation(value = "批量保存语言输出", tags = {"语言输出" },  notes = "批量保存语言输出")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_language_exportDTO> base_language_exportdtos) {
        base_language_exportService.saveBatch(base_language_exportMapping.toDomain(base_language_exportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_language_export-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_language_export-Get')")
	@ApiOperation(value = "获取数据集", tags = {"语言输出" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_exports/fetchdefault")
	public ResponseEntity<List<Base_language_exportDTO>> fetchDefault(Base_language_exportSearchContext context) {
        Page<Base_language_export> domains = base_language_exportService.searchDefault(context) ;
        List<Base_language_exportDTO> list = base_language_exportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_language_export-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_language_export-Get')")
	@ApiOperation(value = "查询数据集", tags = {"语言输出" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_language_exports/searchdefault")
	public ResponseEntity<Page<Base_language_exportDTO>> searchDefault(@RequestBody Base_language_exportSearchContext context) {
        Page<Base_language_export> domains = base_language_exportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_language_exportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

