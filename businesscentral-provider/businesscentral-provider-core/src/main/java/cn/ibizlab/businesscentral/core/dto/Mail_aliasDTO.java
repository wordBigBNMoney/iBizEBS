package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mail_aliasDTO]
 */
@Data
public class Mail_aliasDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ALIAS_PARENT_THREAD_ID]
     *
     */
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aliasDomain;

    /**
     * 属性 [ALIAS_MODEL_ID]
     *
     */
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    @NotNull(message = "[模型别名]不允许为空!")
    private Integer aliasModelId;

    /**
     * 属性 [ALIAS_DEFAULTS]
     *
     */
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    @NotBlank(message = "[默认值]不允许为空!")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String aliasDefaults;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aliasName;

    /**
     * 属性 [ALIAS_FORCE_THREAD_ID]
     *
     */
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ALIAS_PARENT_MODEL_ID]
     *
     */
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    @NotBlank(message = "[安全联系人别名]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String aliasContact;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [ALIAS_USER_ID_TEXT]
     *
     */
    @JSONField(name = "alias_user_id_text")
    @JsonProperty("alias_user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String aliasUserIdText;

    /**
     * 属性 [ALIAS_USER_ID]
     *
     */
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long aliasUserId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [ALIAS_PARENT_THREAD_ID]
     */
    public void setAliasParentThreadId(Integer  aliasParentThreadId){
        this.aliasParentThreadId = aliasParentThreadId ;
        this.modify("alias_parent_thread_id",aliasParentThreadId);
    }

    /**
     * 设置 [ALIAS_MODEL_ID]
     */
    public void setAliasModelId(Integer  aliasModelId){
        this.aliasModelId = aliasModelId ;
        this.modify("alias_model_id",aliasModelId);
    }

    /**
     * 设置 [ALIAS_DEFAULTS]
     */
    public void setAliasDefaults(String  aliasDefaults){
        this.aliasDefaults = aliasDefaults ;
        this.modify("alias_defaults",aliasDefaults);
    }

    /**
     * 设置 [ALIAS_NAME]
     */
    public void setAliasName(String  aliasName){
        this.aliasName = aliasName ;
        this.modify("alias_name",aliasName);
    }

    /**
     * 设置 [ALIAS_FORCE_THREAD_ID]
     */
    public void setAliasForceThreadId(Integer  aliasForceThreadId){
        this.aliasForceThreadId = aliasForceThreadId ;
        this.modify("alias_force_thread_id",aliasForceThreadId);
    }

    /**
     * 设置 [ALIAS_PARENT_MODEL_ID]
     */
    public void setAliasParentModelId(Integer  aliasParentModelId){
        this.aliasParentModelId = aliasParentModelId ;
        this.modify("alias_parent_model_id",aliasParentModelId);
    }

    /**
     * 设置 [ALIAS_CONTACT]
     */
    public void setAliasContact(String  aliasContact){
        this.aliasContact = aliasContact ;
        this.modify("alias_contact",aliasContact);
    }

    /**
     * 设置 [ALIAS_USER_ID]
     */
    public void setAliasUserId(Long  aliasUserId){
        this.aliasUserId = aliasUserId ;
        this.modify("alias_user_id",aliasUserId);
    }


}


