package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.businesscentral.core.dto.Mail_wizard_inviteDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMail_wizard_inviteMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_wizard_inviteMapping extends MappingBase<Mail_wizard_inviteDTO, Mail_wizard_invite> {


}

