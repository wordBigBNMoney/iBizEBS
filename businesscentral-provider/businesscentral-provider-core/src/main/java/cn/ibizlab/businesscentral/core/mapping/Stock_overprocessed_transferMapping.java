package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.businesscentral.core.dto.Stock_overprocessed_transferDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreStock_overprocessed_transferMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_overprocessed_transferMapping extends MappingBase<Stock_overprocessed_transferDTO, Stock_overprocessed_transfer> {


}

