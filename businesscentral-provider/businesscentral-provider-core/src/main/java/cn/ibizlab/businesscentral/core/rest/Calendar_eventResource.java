package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_eventService;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_eventSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动" })
@RestController("Core-calendar_event")
@RequestMapping("")
public class Calendar_eventResource {

    @Autowired
    public ICalendar_eventService calendar_eventService;

    @Autowired
    @Lazy
    public Calendar_eventMapping calendar_eventMapping;

    @PreAuthorize("hasPermission(this.calendar_eventMapping.toDomain(#calendar_eventdto),'iBizBusinessCentral-Calendar_event-Create')")
    @ApiOperation(value = "新建活动", tags = {"活动" },  notes = "新建活动")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events")
    public ResponseEntity<Calendar_eventDTO> create(@Validated @RequestBody Calendar_eventDTO calendar_eventdto) {
        Calendar_event domain = calendar_eventMapping.toDomain(calendar_eventdto);
		calendar_eventService.create(domain);
        Calendar_eventDTO dto = calendar_eventMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_eventMapping.toDomain(#calendar_eventdtos),'iBizBusinessCentral-Calendar_event-Create')")
    @ApiOperation(value = "批量新建活动", tags = {"活动" },  notes = "批量新建活动")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_eventDTO> calendar_eventdtos) {
        calendar_eventService.createBatch(calendar_eventMapping.toDomain(calendar_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "calendar_event" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.calendar_eventService.get(#calendar_event_id),'iBizBusinessCentral-Calendar_event-Update')")
    @ApiOperation(value = "更新活动", tags = {"活动" },  notes = "更新活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_events/{calendar_event_id}")
    public ResponseEntity<Calendar_eventDTO> update(@PathVariable("calendar_event_id") Long calendar_event_id, @RequestBody Calendar_eventDTO calendar_eventdto) {
		Calendar_event domain  = calendar_eventMapping.toDomain(calendar_eventdto);
        domain .setId(calendar_event_id);
		calendar_eventService.update(domain );
		Calendar_eventDTO dto = calendar_eventMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_eventService.getCalendarEventByEntities(this.calendar_eventMapping.toDomain(#calendar_eventdtos)),'iBizBusinessCentral-Calendar_event-Update')")
    @ApiOperation(value = "批量更新活动", tags = {"活动" },  notes = "批量更新活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_events/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_eventDTO> calendar_eventdtos) {
        calendar_eventService.updateBatch(calendar_eventMapping.toDomain(calendar_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.calendar_eventService.get(#calendar_event_id),'iBizBusinessCentral-Calendar_event-Remove')")
    @ApiOperation(value = "删除活动", tags = {"活动" },  notes = "删除活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_events/{calendar_event_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("calendar_event_id") Long calendar_event_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_eventService.remove(calendar_event_id));
    }

    @PreAuthorize("hasPermission(this.calendar_eventService.getCalendarEventByIds(#ids),'iBizBusinessCentral-Calendar_event-Remove')")
    @ApiOperation(value = "批量删除活动", tags = {"活动" },  notes = "批量删除活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_events/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        calendar_eventService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.calendar_eventMapping.toDomain(returnObject.body),'iBizBusinessCentral-Calendar_event-Get')")
    @ApiOperation(value = "获取活动", tags = {"活动" },  notes = "获取活动")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_events/{calendar_event_id}")
    public ResponseEntity<Calendar_eventDTO> get(@PathVariable("calendar_event_id") Long calendar_event_id) {
        Calendar_event domain = calendar_eventService.get(calendar_event_id);
        Calendar_eventDTO dto = calendar_eventMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动草稿", tags = {"活动" },  notes = "获取活动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_events/getdraft")
    public ResponseEntity<Calendar_eventDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_eventMapping.toDto(calendar_eventService.getDraft(new Calendar_event())));
    }

    @ApiOperation(value = "检查活动", tags = {"活动" },  notes = "检查活动")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Calendar_eventDTO calendar_eventdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(calendar_eventService.checkKey(calendar_eventMapping.toDomain(calendar_eventdto)));
    }

    @PreAuthorize("hasPermission(this.calendar_eventMapping.toDomain(#calendar_eventdto),'iBizBusinessCentral-Calendar_event-Save')")
    @ApiOperation(value = "保存活动", tags = {"活动" },  notes = "保存活动")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events/save")
    public ResponseEntity<Boolean> save(@RequestBody Calendar_eventDTO calendar_eventdto) {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_eventService.save(calendar_eventMapping.toDomain(calendar_eventdto)));
    }

    @PreAuthorize("hasPermission(this.calendar_eventMapping.toDomain(#calendar_eventdtos),'iBizBusinessCentral-Calendar_event-Save')")
    @ApiOperation(value = "批量保存活动", tags = {"活动" },  notes = "批量保存活动")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Calendar_eventDTO> calendar_eventdtos) {
        calendar_eventService.saveBatch(calendar_eventMapping.toDomain(calendar_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_event-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_event-Get')")
	@ApiOperation(value = "获取数据集", tags = {"活动" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_events/fetchdefault")
	public ResponseEntity<List<Calendar_eventDTO>> fetchDefault(Calendar_eventSearchContext context) {
        Page<Calendar_event> domains = calendar_eventService.searchDefault(context) ;
        List<Calendar_eventDTO> list = calendar_eventMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_event-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_event-Get')")
	@ApiOperation(value = "查询数据集", tags = {"活动" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/calendar_events/searchdefault")
	public ResponseEntity<Page<Calendar_eventDTO>> searchDefault(@RequestBody Calendar_eventSearchContext context) {
        Page<Calendar_event> domains = calendar_eventService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_eventMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

