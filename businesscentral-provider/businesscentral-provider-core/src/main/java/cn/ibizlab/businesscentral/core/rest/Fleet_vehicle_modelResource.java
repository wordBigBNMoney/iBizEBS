package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_modelService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆型号" })
@RestController("Core-fleet_vehicle_model")
@RequestMapping("")
public class Fleet_vehicle_modelResource {

    @Autowired
    public IFleet_vehicle_modelService fleet_vehicle_modelService;

    @Autowired
    @Lazy
    public Fleet_vehicle_modelMapping fleet_vehicle_modelMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_modelMapping.toDomain(#fleet_vehicle_modeldto),'iBizBusinessCentral-Fleet_vehicle_model-Create')")
    @ApiOperation(value = "新建车辆型号", tags = {"车辆型号" },  notes = "新建车辆型号")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models")
    public ResponseEntity<Fleet_vehicle_modelDTO> create(@Validated @RequestBody Fleet_vehicle_modelDTO fleet_vehicle_modeldto) {
        Fleet_vehicle_model domain = fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldto);
		fleet_vehicle_modelService.create(domain);
        Fleet_vehicle_modelDTO dto = fleet_vehicle_modelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_modelMapping.toDomain(#fleet_vehicle_modeldtos),'iBizBusinessCentral-Fleet_vehicle_model-Create')")
    @ApiOperation(value = "批量新建车辆型号", tags = {"车辆型号" },  notes = "批量新建车辆型号")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_modelDTO> fleet_vehicle_modeldtos) {
        fleet_vehicle_modelService.createBatch(fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_model" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_modelService.get(#fleet_vehicle_model_id),'iBizBusinessCentral-Fleet_vehicle_model-Update')")
    @ApiOperation(value = "更新车辆型号", tags = {"车辆型号" },  notes = "更新车辆型号")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_models/{fleet_vehicle_model_id}")
    public ResponseEntity<Fleet_vehicle_modelDTO> update(@PathVariable("fleet_vehicle_model_id") Long fleet_vehicle_model_id, @RequestBody Fleet_vehicle_modelDTO fleet_vehicle_modeldto) {
		Fleet_vehicle_model domain  = fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldto);
        domain .setId(fleet_vehicle_model_id);
		fleet_vehicle_modelService.update(domain );
		Fleet_vehicle_modelDTO dto = fleet_vehicle_modelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_modelService.getFleetVehicleModelByEntities(this.fleet_vehicle_modelMapping.toDomain(#fleet_vehicle_modeldtos)),'iBizBusinessCentral-Fleet_vehicle_model-Update')")
    @ApiOperation(value = "批量更新车辆型号", tags = {"车辆型号" },  notes = "批量更新车辆型号")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_models/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_modelDTO> fleet_vehicle_modeldtos) {
        fleet_vehicle_modelService.updateBatch(fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_modelService.get(#fleet_vehicle_model_id),'iBizBusinessCentral-Fleet_vehicle_model-Remove')")
    @ApiOperation(value = "删除车辆型号", tags = {"车辆型号" },  notes = "删除车辆型号")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_models/{fleet_vehicle_model_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_model_id") Long fleet_vehicle_model_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_modelService.remove(fleet_vehicle_model_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_modelService.getFleetVehicleModelByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_model-Remove')")
    @ApiOperation(value = "批量删除车辆型号", tags = {"车辆型号" },  notes = "批量删除车辆型号")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_models/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_modelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_modelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_model-Get')")
    @ApiOperation(value = "获取车辆型号", tags = {"车辆型号" },  notes = "获取车辆型号")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_models/{fleet_vehicle_model_id}")
    public ResponseEntity<Fleet_vehicle_modelDTO> get(@PathVariable("fleet_vehicle_model_id") Long fleet_vehicle_model_id) {
        Fleet_vehicle_model domain = fleet_vehicle_modelService.get(fleet_vehicle_model_id);
        Fleet_vehicle_modelDTO dto = fleet_vehicle_modelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆型号草稿", tags = {"车辆型号" },  notes = "获取车辆型号草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_models/getdraft")
    public ResponseEntity<Fleet_vehicle_modelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_modelMapping.toDto(fleet_vehicle_modelService.getDraft(new Fleet_vehicle_model())));
    }

    @ApiOperation(value = "检查车辆型号", tags = {"车辆型号" },  notes = "检查车辆型号")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_modelDTO fleet_vehicle_modeldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_modelService.checkKey(fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_modelMapping.toDomain(#fleet_vehicle_modeldto),'iBizBusinessCentral-Fleet_vehicle_model-Save')")
    @ApiOperation(value = "保存车辆型号", tags = {"车辆型号" },  notes = "保存车辆型号")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_modelDTO fleet_vehicle_modeldto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_modelService.save(fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_modelMapping.toDomain(#fleet_vehicle_modeldtos),'iBizBusinessCentral-Fleet_vehicle_model-Save')")
    @ApiOperation(value = "批量保存车辆型号", tags = {"车辆型号" },  notes = "批量保存车辆型号")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_modelDTO> fleet_vehicle_modeldtos) {
        fleet_vehicle_modelService.saveBatch(fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_model-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_model-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆型号" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_models/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_modelDTO>> fetchDefault(Fleet_vehicle_modelSearchContext context) {
        Page<Fleet_vehicle_model> domains = fleet_vehicle_modelService.searchDefault(context) ;
        List<Fleet_vehicle_modelDTO> list = fleet_vehicle_modelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_model-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_model-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆型号" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_models/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_modelDTO>> searchDefault(@RequestBody Fleet_vehicle_modelSearchContext context) {
        Page<Fleet_vehicle_model> domains = fleet_vehicle_modelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_modelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

