package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Stock_location_routeDTO]
 */
@Data
public class Stock_location_routeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SALE_SELECTABLE]
     *
     */
    @JSONField(name = "sale_selectable")
    @JsonProperty("sale_selectable")
    private Boolean saleSelectable;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [PRODUCT_IDS]
     *
     */
    @JSONField(name = "product_ids")
    @JsonProperty("product_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String productIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[路线]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [CATEG_IDS]
     *
     */
    @JSONField(name = "categ_ids")
    @JsonProperty("categ_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String categIds;

    /**
     * 属性 [WAREHOUSE_SELECTABLE]
     *
     */
    @JSONField(name = "warehouse_selectable")
    @JsonProperty("warehouse_selectable")
    private Boolean warehouseSelectable;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [RULE_IDS]
     *
     */
    @JSONField(name = "rule_ids")
    @JsonProperty("rule_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String ruleIds;

    /**
     * 属性 [PRODUCT_SELECTABLE]
     *
     */
    @JSONField(name = "product_selectable")
    @JsonProperty("product_selectable")
    private Boolean productSelectable;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [PRODUCT_CATEG_SELECTABLE]
     *
     */
    @JSONField(name = "product_categ_selectable")
    @JsonProperty("product_categ_selectable")
    private Boolean productCategSelectable;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WAREHOUSE_IDS]
     *
     */
    @JSONField(name = "warehouse_ids")
    @JsonProperty("warehouse_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String warehouseIds;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [SUPPLIED_WH_ID_TEXT]
     *
     */
    @JSONField(name = "supplied_wh_id_text")
    @JsonProperty("supplied_wh_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String suppliedWhIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [SUPPLIER_WH_ID_TEXT]
     *
     */
    @JSONField(name = "supplier_wh_id_text")
    @JsonProperty("supplier_wh_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String supplierWhIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [SUPPLIER_WH_ID]
     *
     */
    @JSONField(name = "supplier_wh_id")
    @JsonProperty("supplier_wh_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long supplierWhId;

    /**
     * 属性 [SUPPLIED_WH_ID]
     *
     */
    @JSONField(name = "supplied_wh_id")
    @JsonProperty("supplied_wh_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long suppliedWhId;


    /**
     * 设置 [SALE_SELECTABLE]
     */
    public void setSaleSelectable(Boolean  saleSelectable){
        this.saleSelectable = saleSelectable ;
        this.modify("sale_selectable",saleSelectable);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [WAREHOUSE_SELECTABLE]
     */
    public void setWarehouseSelectable(Boolean  warehouseSelectable){
        this.warehouseSelectable = warehouseSelectable ;
        this.modify("warehouse_selectable",warehouseSelectable);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [PRODUCT_SELECTABLE]
     */
    public void setProductSelectable(Boolean  productSelectable){
        this.productSelectable = productSelectable ;
        this.modify("product_selectable",productSelectable);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [PRODUCT_CATEG_SELECTABLE]
     */
    public void setProductCategSelectable(Boolean  productCategSelectable){
        this.productCategSelectable = productCategSelectable ;
        this.modify("product_categ_selectable",productCategSelectable);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [SUPPLIER_WH_ID]
     */
    public void setSupplierWhId(Long  supplierWhId){
        this.supplierWhId = supplierWhId ;
        this.modify("supplier_wh_id",supplierWhId);
    }

    /**
     * 设置 [SUPPLIED_WH_ID]
     */
    public void setSuppliedWhId(Long  suppliedWhId){
        this.suppliedWhId = suppliedWhId ;
        this.modify("supplied_wh_id",suppliedWhId);
    }


}


