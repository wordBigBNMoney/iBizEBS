package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelist_itemService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_pricelist_itemSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"价格表明细" })
@RestController("Core-product_pricelist_item")
@RequestMapping("")
public class Product_pricelist_itemResource {

    @Autowired
    public IProduct_pricelist_itemService product_pricelist_itemService;

    @Autowired
    @Lazy
    public Product_pricelist_itemMapping product_pricelist_itemMapping;

    @PreAuthorize("hasPermission(this.product_pricelist_itemMapping.toDomain(#product_pricelist_itemdto),'iBizBusinessCentral-Product_pricelist_item-Create')")
    @ApiOperation(value = "新建价格表明细", tags = {"价格表明细" },  notes = "新建价格表明细")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items")
    public ResponseEntity<Product_pricelist_itemDTO> create(@Validated @RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        Product_pricelist_item domain = product_pricelist_itemMapping.toDomain(product_pricelist_itemdto);
		product_pricelist_itemService.create(domain);
        Product_pricelist_itemDTO dto = product_pricelist_itemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_pricelist_itemMapping.toDomain(#product_pricelist_itemdtos),'iBizBusinessCentral-Product_pricelist_item-Create')")
    @ApiOperation(value = "批量新建价格表明细", tags = {"价格表明细" },  notes = "批量新建价格表明细")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {
        product_pricelist_itemService.createBatch(product_pricelist_itemMapping.toDomain(product_pricelist_itemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_pricelist_item" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_pricelist_itemService.get(#product_pricelist_item_id),'iBizBusinessCentral-Product_pricelist_item-Update')")
    @ApiOperation(value = "更新价格表明细", tags = {"价格表明细" },  notes = "更新价格表明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelist_items/{product_pricelist_item_id}")
    public ResponseEntity<Product_pricelist_itemDTO> update(@PathVariable("product_pricelist_item_id") Long product_pricelist_item_id, @RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
		Product_pricelist_item domain  = product_pricelist_itemMapping.toDomain(product_pricelist_itemdto);
        domain .setId(product_pricelist_item_id);
		product_pricelist_itemService.update(domain );
		Product_pricelist_itemDTO dto = product_pricelist_itemMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_pricelist_itemService.getProductPricelistItemByEntities(this.product_pricelist_itemMapping.toDomain(#product_pricelist_itemdtos)),'iBizBusinessCentral-Product_pricelist_item-Update')")
    @ApiOperation(value = "批量更新价格表明细", tags = {"价格表明细" },  notes = "批量更新价格表明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelist_items/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {
        product_pricelist_itemService.updateBatch(product_pricelist_itemMapping.toDomain(product_pricelist_itemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_pricelist_itemService.get(#product_pricelist_item_id),'iBizBusinessCentral-Product_pricelist_item-Remove')")
    @ApiOperation(value = "删除价格表明细", tags = {"价格表明细" },  notes = "删除价格表明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelist_items/{product_pricelist_item_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_pricelist_item_id") Long product_pricelist_item_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemService.remove(product_pricelist_item_id));
    }

    @PreAuthorize("hasPermission(this.product_pricelist_itemService.getProductPricelistItemByIds(#ids),'iBizBusinessCentral-Product_pricelist_item-Remove')")
    @ApiOperation(value = "批量删除价格表明细", tags = {"价格表明细" },  notes = "批量删除价格表明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelist_items/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_pricelist_itemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_pricelist_itemMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_pricelist_item-Get')")
    @ApiOperation(value = "获取价格表明细", tags = {"价格表明细" },  notes = "获取价格表明细")
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/{product_pricelist_item_id}")
    public ResponseEntity<Product_pricelist_itemDTO> get(@PathVariable("product_pricelist_item_id") Long product_pricelist_item_id) {
        Product_pricelist_item domain = product_pricelist_itemService.get(product_pricelist_item_id);
        Product_pricelist_itemDTO dto = product_pricelist_itemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取价格表明细草稿", tags = {"价格表明细" },  notes = "获取价格表明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/getdraft")
    public ResponseEntity<Product_pricelist_itemDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemMapping.toDto(product_pricelist_itemService.getDraft(new Product_pricelist_item())));
    }

    @ApiOperation(value = "检查价格表明细", tags = {"价格表明细" },  notes = "检查价格表明细")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemService.checkKey(product_pricelist_itemMapping.toDomain(product_pricelist_itemdto)));
    }

    @PreAuthorize("hasPermission(this.product_pricelist_itemMapping.toDomain(#product_pricelist_itemdto),'iBizBusinessCentral-Product_pricelist_item-Save')")
    @ApiOperation(value = "保存价格表明细", tags = {"价格表明细" },  notes = "保存价格表明细")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemService.save(product_pricelist_itemMapping.toDomain(product_pricelist_itemdto)));
    }

    @PreAuthorize("hasPermission(this.product_pricelist_itemMapping.toDomain(#product_pricelist_itemdtos),'iBizBusinessCentral-Product_pricelist_item-Save')")
    @ApiOperation(value = "批量保存价格表明细", tags = {"价格表明细" },  notes = "批量保存价格表明细")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {
        product_pricelist_itemService.saveBatch(product_pricelist_itemMapping.toDomain(product_pricelist_itemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_pricelist_item-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_pricelist_item-Get')")
	@ApiOperation(value = "获取数据集", tags = {"价格表明细" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_pricelist_items/fetchdefault")
	public ResponseEntity<List<Product_pricelist_itemDTO>> fetchDefault(Product_pricelist_itemSearchContext context) {
        Page<Product_pricelist_item> domains = product_pricelist_itemService.searchDefault(context) ;
        List<Product_pricelist_itemDTO> list = product_pricelist_itemMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_pricelist_item-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_pricelist_item-Get')")
	@ApiOperation(value = "查询数据集", tags = {"价格表明细" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_pricelist_items/searchdefault")
	public ResponseEntity<Page<Product_pricelist_itemDTO>> searchDefault(@RequestBody Product_pricelist_itemSearchContext context) {
        Page<Product_pricelist_item> domains = product_pricelist_itemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_pricelist_itemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

