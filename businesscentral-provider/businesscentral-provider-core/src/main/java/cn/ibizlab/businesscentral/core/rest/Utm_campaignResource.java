package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_campaignService;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_campaignSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"UTM 营销活动" })
@RestController("Core-utm_campaign")
@RequestMapping("")
public class Utm_campaignResource {

    @Autowired
    public IUtm_campaignService utm_campaignService;

    @Autowired
    @Lazy
    public Utm_campaignMapping utm_campaignMapping;

    @PreAuthorize("hasPermission(this.utm_campaignMapping.toDomain(#utm_campaigndto),'iBizBusinessCentral-Utm_campaign-Create')")
    @ApiOperation(value = "新建UTM 营销活动", tags = {"UTM 营销活动" },  notes = "新建UTM 营销活动")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns")
    public ResponseEntity<Utm_campaignDTO> create(@Validated @RequestBody Utm_campaignDTO utm_campaigndto) {
        Utm_campaign domain = utm_campaignMapping.toDomain(utm_campaigndto);
		utm_campaignService.create(domain);
        Utm_campaignDTO dto = utm_campaignMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.utm_campaignMapping.toDomain(#utm_campaigndtos),'iBizBusinessCentral-Utm_campaign-Create')")
    @ApiOperation(value = "批量新建UTM 营销活动", tags = {"UTM 营销活动" },  notes = "批量新建UTM 营销活动")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Utm_campaignDTO> utm_campaigndtos) {
        utm_campaignService.createBatch(utm_campaignMapping.toDomain(utm_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "utm_campaign" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.utm_campaignService.get(#utm_campaign_id),'iBizBusinessCentral-Utm_campaign-Update')")
    @ApiOperation(value = "更新UTM 营销活动", tags = {"UTM 营销活动" },  notes = "更新UTM 营销活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_campaigns/{utm_campaign_id}")
    public ResponseEntity<Utm_campaignDTO> update(@PathVariable("utm_campaign_id") Long utm_campaign_id, @RequestBody Utm_campaignDTO utm_campaigndto) {
		Utm_campaign domain  = utm_campaignMapping.toDomain(utm_campaigndto);
        domain .setId(utm_campaign_id);
		utm_campaignService.update(domain );
		Utm_campaignDTO dto = utm_campaignMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.utm_campaignService.getUtmCampaignByEntities(this.utm_campaignMapping.toDomain(#utm_campaigndtos)),'iBizBusinessCentral-Utm_campaign-Update')")
    @ApiOperation(value = "批量更新UTM 营销活动", tags = {"UTM 营销活动" },  notes = "批量更新UTM 营销活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_campaigns/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_campaignDTO> utm_campaigndtos) {
        utm_campaignService.updateBatch(utm_campaignMapping.toDomain(utm_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.utm_campaignService.get(#utm_campaign_id),'iBizBusinessCentral-Utm_campaign-Remove')")
    @ApiOperation(value = "删除UTM 营销活动", tags = {"UTM 营销活动" },  notes = "删除UTM 营销活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_campaigns/{utm_campaign_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("utm_campaign_id") Long utm_campaign_id) {
         return ResponseEntity.status(HttpStatus.OK).body(utm_campaignService.remove(utm_campaign_id));
    }

    @PreAuthorize("hasPermission(this.utm_campaignService.getUtmCampaignByIds(#ids),'iBizBusinessCentral-Utm_campaign-Remove')")
    @ApiOperation(value = "批量删除UTM 营销活动", tags = {"UTM 营销活动" },  notes = "批量删除UTM 营销活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_campaigns/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        utm_campaignService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.utm_campaignMapping.toDomain(returnObject.body),'iBizBusinessCentral-Utm_campaign-Get')")
    @ApiOperation(value = "获取UTM 营销活动", tags = {"UTM 营销活动" },  notes = "获取UTM 营销活动")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_campaigns/{utm_campaign_id}")
    public ResponseEntity<Utm_campaignDTO> get(@PathVariable("utm_campaign_id") Long utm_campaign_id) {
        Utm_campaign domain = utm_campaignService.get(utm_campaign_id);
        Utm_campaignDTO dto = utm_campaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取UTM 营销活动草稿", tags = {"UTM 营销活动" },  notes = "获取UTM 营销活动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_campaigns/getdraft")
    public ResponseEntity<Utm_campaignDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(utm_campaignMapping.toDto(utm_campaignService.getDraft(new Utm_campaign())));
    }

    @ApiOperation(value = "检查UTM 营销活动", tags = {"UTM 营销活动" },  notes = "检查UTM 营销活动")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Utm_campaignDTO utm_campaigndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(utm_campaignService.checkKey(utm_campaignMapping.toDomain(utm_campaigndto)));
    }

    @PreAuthorize("hasPermission(this.utm_campaignMapping.toDomain(#utm_campaigndto),'iBizBusinessCentral-Utm_campaign-Save')")
    @ApiOperation(value = "保存UTM 营销活动", tags = {"UTM 营销活动" },  notes = "保存UTM 营销活动")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns/save")
    public ResponseEntity<Boolean> save(@RequestBody Utm_campaignDTO utm_campaigndto) {
        return ResponseEntity.status(HttpStatus.OK).body(utm_campaignService.save(utm_campaignMapping.toDomain(utm_campaigndto)));
    }

    @PreAuthorize("hasPermission(this.utm_campaignMapping.toDomain(#utm_campaigndtos),'iBizBusinessCentral-Utm_campaign-Save')")
    @ApiOperation(value = "批量保存UTM 营销活动", tags = {"UTM 营销活动" },  notes = "批量保存UTM 营销活动")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Utm_campaignDTO> utm_campaigndtos) {
        utm_campaignService.saveBatch(utm_campaignMapping.toDomain(utm_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_campaign-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Utm_campaign-Get')")
	@ApiOperation(value = "获取数据集", tags = {"UTM 营销活动" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/utm_campaigns/fetchdefault")
	public ResponseEntity<List<Utm_campaignDTO>> fetchDefault(Utm_campaignSearchContext context) {
        Page<Utm_campaign> domains = utm_campaignService.searchDefault(context) ;
        List<Utm_campaignDTO> list = utm_campaignMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_campaign-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Utm_campaign-Get')")
	@ApiOperation(value = "查询数据集", tags = {"UTM 营销活动" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/utm_campaigns/searchdefault")
	public ResponseEntity<Page<Utm_campaignDTO>> searchDefault(@RequestBody Utm_campaignSearchContext context) {
        Page<Utm_campaign> domains = utm_campaignService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(utm_campaignMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

