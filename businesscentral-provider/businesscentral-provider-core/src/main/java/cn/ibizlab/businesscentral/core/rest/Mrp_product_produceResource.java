package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produceService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_product_produceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"记录生产" })
@RestController("Core-mrp_product_produce")
@RequestMapping("")
public class Mrp_product_produceResource {

    @Autowired
    public IMrp_product_produceService mrp_product_produceService;

    @Autowired
    @Lazy
    public Mrp_product_produceMapping mrp_product_produceMapping;

    @PreAuthorize("hasPermission(this.mrp_product_produceMapping.toDomain(#mrp_product_producedto),'iBizBusinessCentral-Mrp_product_produce-Create')")
    @ApiOperation(value = "新建记录生产", tags = {"记录生产" },  notes = "新建记录生产")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces")
    public ResponseEntity<Mrp_product_produceDTO> create(@Validated @RequestBody Mrp_product_produceDTO mrp_product_producedto) {
        Mrp_product_produce domain = mrp_product_produceMapping.toDomain(mrp_product_producedto);
		mrp_product_produceService.create(domain);
        Mrp_product_produceDTO dto = mrp_product_produceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_product_produceMapping.toDomain(#mrp_product_producedtos),'iBizBusinessCentral-Mrp_product_produce-Create')")
    @ApiOperation(value = "批量新建记录生产", tags = {"记录生产" },  notes = "批量新建记录生产")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_product_produceDTO> mrp_product_producedtos) {
        mrp_product_produceService.createBatch(mrp_product_produceMapping.toDomain(mrp_product_producedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_product_produce" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_product_produceService.get(#mrp_product_produce_id),'iBizBusinessCentral-Mrp_product_produce-Update')")
    @ApiOperation(value = "更新记录生产", tags = {"记录生产" },  notes = "更新记录生产")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produces/{mrp_product_produce_id}")
    public ResponseEntity<Mrp_product_produceDTO> update(@PathVariable("mrp_product_produce_id") Long mrp_product_produce_id, @RequestBody Mrp_product_produceDTO mrp_product_producedto) {
		Mrp_product_produce domain  = mrp_product_produceMapping.toDomain(mrp_product_producedto);
        domain .setId(mrp_product_produce_id);
		mrp_product_produceService.update(domain );
		Mrp_product_produceDTO dto = mrp_product_produceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_product_produceService.getMrpProductProduceByEntities(this.mrp_product_produceMapping.toDomain(#mrp_product_producedtos)),'iBizBusinessCentral-Mrp_product_produce-Update')")
    @ApiOperation(value = "批量更新记录生产", tags = {"记录生产" },  notes = "批量更新记录生产")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produces/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_product_produceDTO> mrp_product_producedtos) {
        mrp_product_produceService.updateBatch(mrp_product_produceMapping.toDomain(mrp_product_producedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_product_produceService.get(#mrp_product_produce_id),'iBizBusinessCentral-Mrp_product_produce-Remove')")
    @ApiOperation(value = "删除记录生产", tags = {"记录生产" },  notes = "删除记录生产")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produces/{mrp_product_produce_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_product_produce_id") Long mrp_product_produce_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_product_produceService.remove(mrp_product_produce_id));
    }

    @PreAuthorize("hasPermission(this.mrp_product_produceService.getMrpProductProduceByIds(#ids),'iBizBusinessCentral-Mrp_product_produce-Remove')")
    @ApiOperation(value = "批量删除记录生产", tags = {"记录生产" },  notes = "批量删除记录生产")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produces/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_product_produceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_product_produceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_product_produce-Get')")
    @ApiOperation(value = "获取记录生产", tags = {"记录生产" },  notes = "获取记录生产")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produces/{mrp_product_produce_id}")
    public ResponseEntity<Mrp_product_produceDTO> get(@PathVariable("mrp_product_produce_id") Long mrp_product_produce_id) {
        Mrp_product_produce domain = mrp_product_produceService.get(mrp_product_produce_id);
        Mrp_product_produceDTO dto = mrp_product_produceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取记录生产草稿", tags = {"记录生产" },  notes = "获取记录生产草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produces/getdraft")
    public ResponseEntity<Mrp_product_produceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_product_produceMapping.toDto(mrp_product_produceService.getDraft(new Mrp_product_produce())));
    }

    @ApiOperation(value = "检查记录生产", tags = {"记录生产" },  notes = "检查记录生产")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_product_produceDTO mrp_product_producedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_product_produceService.checkKey(mrp_product_produceMapping.toDomain(mrp_product_producedto)));
    }

    @PreAuthorize("hasPermission(this.mrp_product_produceMapping.toDomain(#mrp_product_producedto),'iBizBusinessCentral-Mrp_product_produce-Save')")
    @ApiOperation(value = "保存记录生产", tags = {"记录生产" },  notes = "保存记录生产")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_product_produceDTO mrp_product_producedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_product_produceService.save(mrp_product_produceMapping.toDomain(mrp_product_producedto)));
    }

    @PreAuthorize("hasPermission(this.mrp_product_produceMapping.toDomain(#mrp_product_producedtos),'iBizBusinessCentral-Mrp_product_produce-Save')")
    @ApiOperation(value = "批量保存记录生产", tags = {"记录生产" },  notes = "批量保存记录生产")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_product_produceDTO> mrp_product_producedtos) {
        mrp_product_produceService.saveBatch(mrp_product_produceMapping.toDomain(mrp_product_producedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_product_produce-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_product_produce-Get')")
	@ApiOperation(value = "获取数据集", tags = {"记录生产" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_product_produces/fetchdefault")
	public ResponseEntity<List<Mrp_product_produceDTO>> fetchDefault(Mrp_product_produceSearchContext context) {
        Page<Mrp_product_produce> domains = mrp_product_produceService.searchDefault(context) ;
        List<Mrp_product_produceDTO> list = mrp_product_produceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_product_produce-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_product_produce-Get')")
	@ApiOperation(value = "查询数据集", tags = {"记录生产" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_product_produces/searchdefault")
	public ResponseEntity<Page<Mrp_product_produceDTO>> searchDefault(@RequestBody Mrp_product_produceSearchContext context) {
        Page<Mrp_product_produce> domains = mrp_product_produceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_product_produceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

