package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_location_routeService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_location_routeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存路线" })
@RestController("Core-stock_location_route")
@RequestMapping("")
public class Stock_location_routeResource {

    @Autowired
    public IStock_location_routeService stock_location_routeService;

    @Autowired
    @Lazy
    public Stock_location_routeMapping stock_location_routeMapping;

    @PreAuthorize("hasPermission(this.stock_location_routeMapping.toDomain(#stock_location_routedto),'iBizBusinessCentral-Stock_location_route-Create')")
    @ApiOperation(value = "新建库存路线", tags = {"库存路线" },  notes = "新建库存路线")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes")
    public ResponseEntity<Stock_location_routeDTO> create(@Validated @RequestBody Stock_location_routeDTO stock_location_routedto) {
        Stock_location_route domain = stock_location_routeMapping.toDomain(stock_location_routedto);
		stock_location_routeService.create(domain);
        Stock_location_routeDTO dto = stock_location_routeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_location_routeMapping.toDomain(#stock_location_routedtos),'iBizBusinessCentral-Stock_location_route-Create')")
    @ApiOperation(value = "批量新建库存路线", tags = {"库存路线" },  notes = "批量新建库存路线")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_location_routeDTO> stock_location_routedtos) {
        stock_location_routeService.createBatch(stock_location_routeMapping.toDomain(stock_location_routedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_location_route" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_location_routeService.get(#stock_location_route_id),'iBizBusinessCentral-Stock_location_route-Update')")
    @ApiOperation(value = "更新库存路线", tags = {"库存路线" },  notes = "更新库存路线")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_location_routes/{stock_location_route_id}")
    public ResponseEntity<Stock_location_routeDTO> update(@PathVariable("stock_location_route_id") Long stock_location_route_id, @RequestBody Stock_location_routeDTO stock_location_routedto) {
		Stock_location_route domain  = stock_location_routeMapping.toDomain(stock_location_routedto);
        domain .setId(stock_location_route_id);
		stock_location_routeService.update(domain );
		Stock_location_routeDTO dto = stock_location_routeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_location_routeService.getStockLocationRouteByEntities(this.stock_location_routeMapping.toDomain(#stock_location_routedtos)),'iBizBusinessCentral-Stock_location_route-Update')")
    @ApiOperation(value = "批量更新库存路线", tags = {"库存路线" },  notes = "批量更新库存路线")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_location_routes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_location_routeDTO> stock_location_routedtos) {
        stock_location_routeService.updateBatch(stock_location_routeMapping.toDomain(stock_location_routedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_location_routeService.get(#stock_location_route_id),'iBizBusinessCentral-Stock_location_route-Remove')")
    @ApiOperation(value = "删除库存路线", tags = {"库存路线" },  notes = "删除库存路线")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_location_routes/{stock_location_route_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_location_route_id") Long stock_location_route_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_location_routeService.remove(stock_location_route_id));
    }

    @PreAuthorize("hasPermission(this.stock_location_routeService.getStockLocationRouteByIds(#ids),'iBizBusinessCentral-Stock_location_route-Remove')")
    @ApiOperation(value = "批量删除库存路线", tags = {"库存路线" },  notes = "批量删除库存路线")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_location_routes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_location_routeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_location_routeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_location_route-Get')")
    @ApiOperation(value = "获取库存路线", tags = {"库存路线" },  notes = "获取库存路线")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_location_routes/{stock_location_route_id}")
    public ResponseEntity<Stock_location_routeDTO> get(@PathVariable("stock_location_route_id") Long stock_location_route_id) {
        Stock_location_route domain = stock_location_routeService.get(stock_location_route_id);
        Stock_location_routeDTO dto = stock_location_routeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存路线草稿", tags = {"库存路线" },  notes = "获取库存路线草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_location_routes/getdraft")
    public ResponseEntity<Stock_location_routeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_location_routeMapping.toDto(stock_location_routeService.getDraft(new Stock_location_route())));
    }

    @ApiOperation(value = "检查库存路线", tags = {"库存路线" },  notes = "检查库存路线")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_location_routeDTO stock_location_routedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_location_routeService.checkKey(stock_location_routeMapping.toDomain(stock_location_routedto)));
    }

    @PreAuthorize("hasPermission(this.stock_location_routeMapping.toDomain(#stock_location_routedto),'iBizBusinessCentral-Stock_location_route-Save')")
    @ApiOperation(value = "保存库存路线", tags = {"库存路线" },  notes = "保存库存路线")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_location_routeDTO stock_location_routedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_location_routeService.save(stock_location_routeMapping.toDomain(stock_location_routedto)));
    }

    @PreAuthorize("hasPermission(this.stock_location_routeMapping.toDomain(#stock_location_routedtos),'iBizBusinessCentral-Stock_location_route-Save')")
    @ApiOperation(value = "批量保存库存路线", tags = {"库存路线" },  notes = "批量保存库存路线")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_location_routeDTO> stock_location_routedtos) {
        stock_location_routeService.saveBatch(stock_location_routeMapping.toDomain(stock_location_routedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_location_route-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_location_route-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存路线" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_location_routes/fetchdefault")
	public ResponseEntity<List<Stock_location_routeDTO>> fetchDefault(Stock_location_routeSearchContext context) {
        Page<Stock_location_route> domains = stock_location_routeService.searchDefault(context) ;
        List<Stock_location_routeDTO> list = stock_location_routeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_location_route-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_location_route-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存路线" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_location_routes/searchdefault")
	public ResponseEntity<Page<Stock_location_routeDTO>> searchDefault(@RequestBody Stock_location_routeSearchContext context) {
        Page<Stock_location_route> domains = stock_location_routeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_location_routeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

