package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.dto.Website_sale_payment_acquirer_onboarding_wizardDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreWebsite_sale_payment_acquirer_onboarding_wizardMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Website_sale_payment_acquirer_onboarding_wizardMapping extends MappingBase<Website_sale_payment_acquirer_onboarding_wizardDTO, Website_sale_payment_acquirer_onboarding_wizard> {


}

