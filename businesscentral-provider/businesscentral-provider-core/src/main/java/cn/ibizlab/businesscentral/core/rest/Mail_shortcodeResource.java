package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_shortcodeService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_shortcodeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"自动回复" })
@RestController("Core-mail_shortcode")
@RequestMapping("")
public class Mail_shortcodeResource {

    @Autowired
    public IMail_shortcodeService mail_shortcodeService;

    @Autowired
    @Lazy
    public Mail_shortcodeMapping mail_shortcodeMapping;

    @PreAuthorize("hasPermission(this.mail_shortcodeMapping.toDomain(#mail_shortcodedto),'iBizBusinessCentral-Mail_shortcode-Create')")
    @ApiOperation(value = "新建自动回复", tags = {"自动回复" },  notes = "新建自动回复")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes")
    public ResponseEntity<Mail_shortcodeDTO> create(@Validated @RequestBody Mail_shortcodeDTO mail_shortcodedto) {
        Mail_shortcode domain = mail_shortcodeMapping.toDomain(mail_shortcodedto);
		mail_shortcodeService.create(domain);
        Mail_shortcodeDTO dto = mail_shortcodeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_shortcodeMapping.toDomain(#mail_shortcodedtos),'iBizBusinessCentral-Mail_shortcode-Create')")
    @ApiOperation(value = "批量新建自动回复", tags = {"自动回复" },  notes = "批量新建自动回复")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_shortcodeDTO> mail_shortcodedtos) {
        mail_shortcodeService.createBatch(mail_shortcodeMapping.toDomain(mail_shortcodedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_shortcode" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_shortcodeService.get(#mail_shortcode_id),'iBizBusinessCentral-Mail_shortcode-Update')")
    @ApiOperation(value = "更新自动回复", tags = {"自动回复" },  notes = "更新自动回复")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_shortcodes/{mail_shortcode_id}")
    public ResponseEntity<Mail_shortcodeDTO> update(@PathVariable("mail_shortcode_id") Long mail_shortcode_id, @RequestBody Mail_shortcodeDTO mail_shortcodedto) {
		Mail_shortcode domain  = mail_shortcodeMapping.toDomain(mail_shortcodedto);
        domain .setId(mail_shortcode_id);
		mail_shortcodeService.update(domain );
		Mail_shortcodeDTO dto = mail_shortcodeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_shortcodeService.getMailShortcodeByEntities(this.mail_shortcodeMapping.toDomain(#mail_shortcodedtos)),'iBizBusinessCentral-Mail_shortcode-Update')")
    @ApiOperation(value = "批量更新自动回复", tags = {"自动回复" },  notes = "批量更新自动回复")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_shortcodes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_shortcodeDTO> mail_shortcodedtos) {
        mail_shortcodeService.updateBatch(mail_shortcodeMapping.toDomain(mail_shortcodedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_shortcodeService.get(#mail_shortcode_id),'iBizBusinessCentral-Mail_shortcode-Remove')")
    @ApiOperation(value = "删除自动回复", tags = {"自动回复" },  notes = "删除自动回复")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_shortcodes/{mail_shortcode_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_shortcode_id") Long mail_shortcode_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_shortcodeService.remove(mail_shortcode_id));
    }

    @PreAuthorize("hasPermission(this.mail_shortcodeService.getMailShortcodeByIds(#ids),'iBizBusinessCentral-Mail_shortcode-Remove')")
    @ApiOperation(value = "批量删除自动回复", tags = {"自动回复" },  notes = "批量删除自动回复")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_shortcodes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_shortcodeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_shortcodeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_shortcode-Get')")
    @ApiOperation(value = "获取自动回复", tags = {"自动回复" },  notes = "获取自动回复")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_shortcodes/{mail_shortcode_id}")
    public ResponseEntity<Mail_shortcodeDTO> get(@PathVariable("mail_shortcode_id") Long mail_shortcode_id) {
        Mail_shortcode domain = mail_shortcodeService.get(mail_shortcode_id);
        Mail_shortcodeDTO dto = mail_shortcodeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取自动回复草稿", tags = {"自动回复" },  notes = "获取自动回复草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_shortcodes/getdraft")
    public ResponseEntity<Mail_shortcodeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_shortcodeMapping.toDto(mail_shortcodeService.getDraft(new Mail_shortcode())));
    }

    @ApiOperation(value = "检查自动回复", tags = {"自动回复" },  notes = "检查自动回复")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_shortcodeDTO mail_shortcodedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_shortcodeService.checkKey(mail_shortcodeMapping.toDomain(mail_shortcodedto)));
    }

    @PreAuthorize("hasPermission(this.mail_shortcodeMapping.toDomain(#mail_shortcodedto),'iBizBusinessCentral-Mail_shortcode-Save')")
    @ApiOperation(value = "保存自动回复", tags = {"自动回复" },  notes = "保存自动回复")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_shortcodeDTO mail_shortcodedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_shortcodeService.save(mail_shortcodeMapping.toDomain(mail_shortcodedto)));
    }

    @PreAuthorize("hasPermission(this.mail_shortcodeMapping.toDomain(#mail_shortcodedtos),'iBizBusinessCentral-Mail_shortcode-Save')")
    @ApiOperation(value = "批量保存自动回复", tags = {"自动回复" },  notes = "批量保存自动回复")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_shortcodeDTO> mail_shortcodedtos) {
        mail_shortcodeService.saveBatch(mail_shortcodeMapping.toDomain(mail_shortcodedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_shortcode-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_shortcode-Get')")
	@ApiOperation(value = "获取数据集", tags = {"自动回复" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_shortcodes/fetchdefault")
	public ResponseEntity<List<Mail_shortcodeDTO>> fetchDefault(Mail_shortcodeSearchContext context) {
        Page<Mail_shortcode> domains = mail_shortcodeService.searchDefault(context) ;
        List<Mail_shortcodeDTO> list = mail_shortcodeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_shortcode-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_shortcode-Get')")
	@ApiOperation(value = "查询数据集", tags = {"自动回复" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_shortcodes/searchdefault")
	public ResponseEntity<Page<Mail_shortcodeDTO>> searchDefault(@RequestBody Mail_shortcodeSearchContext context) {
        Page<Mail_shortcode> domains = mail_shortcodeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_shortcodeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

