package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicleService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆" })
@RestController("Core-fleet_vehicle")
@RequestMapping("")
public class Fleet_vehicleResource {

    @Autowired
    public IFleet_vehicleService fleet_vehicleService;

    @Autowired
    @Lazy
    public Fleet_vehicleMapping fleet_vehicleMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicleMapping.toDomain(#fleet_vehicledto),'iBizBusinessCentral-Fleet_vehicle-Create')")
    @ApiOperation(value = "新建车辆", tags = {"车辆" },  notes = "新建车辆")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles")
    public ResponseEntity<Fleet_vehicleDTO> create(@Validated @RequestBody Fleet_vehicleDTO fleet_vehicledto) {
        Fleet_vehicle domain = fleet_vehicleMapping.toDomain(fleet_vehicledto);
		fleet_vehicleService.create(domain);
        Fleet_vehicleDTO dto = fleet_vehicleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicleMapping.toDomain(#fleet_vehicledtos),'iBizBusinessCentral-Fleet_vehicle-Create')")
    @ApiOperation(value = "批量新建车辆", tags = {"车辆" },  notes = "批量新建车辆")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicleDTO> fleet_vehicledtos) {
        fleet_vehicleService.createBatch(fleet_vehicleMapping.toDomain(fleet_vehicledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicleService.get(#fleet_vehicle_id),'iBizBusinessCentral-Fleet_vehicle-Update')")
    @ApiOperation(value = "更新车辆", tags = {"车辆" },  notes = "更新车辆")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicles/{fleet_vehicle_id}")
    public ResponseEntity<Fleet_vehicleDTO> update(@PathVariable("fleet_vehicle_id") Long fleet_vehicle_id, @RequestBody Fleet_vehicleDTO fleet_vehicledto) {
		Fleet_vehicle domain  = fleet_vehicleMapping.toDomain(fleet_vehicledto);
        domain .setId(fleet_vehicle_id);
		fleet_vehicleService.update(domain );
		Fleet_vehicleDTO dto = fleet_vehicleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicleService.getFleetVehicleByEntities(this.fleet_vehicleMapping.toDomain(#fleet_vehicledtos)),'iBizBusinessCentral-Fleet_vehicle-Update')")
    @ApiOperation(value = "批量更新车辆", tags = {"车辆" },  notes = "批量更新车辆")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicleDTO> fleet_vehicledtos) {
        fleet_vehicleService.updateBatch(fleet_vehicleMapping.toDomain(fleet_vehicledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicleService.get(#fleet_vehicle_id),'iBizBusinessCentral-Fleet_vehicle-Remove')")
    @ApiOperation(value = "删除车辆", tags = {"车辆" },  notes = "删除车辆")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicles/{fleet_vehicle_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_id") Long fleet_vehicle_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicleService.remove(fleet_vehicle_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicleService.getFleetVehicleByIds(#ids),'iBizBusinessCentral-Fleet_vehicle-Remove')")
    @ApiOperation(value = "批量删除车辆", tags = {"车辆" },  notes = "批量删除车辆")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicleMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle-Get')")
    @ApiOperation(value = "获取车辆", tags = {"车辆" },  notes = "获取车辆")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicles/{fleet_vehicle_id}")
    public ResponseEntity<Fleet_vehicleDTO> get(@PathVariable("fleet_vehicle_id") Long fleet_vehicle_id) {
        Fleet_vehicle domain = fleet_vehicleService.get(fleet_vehicle_id);
        Fleet_vehicleDTO dto = fleet_vehicleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆草稿", tags = {"车辆" },  notes = "获取车辆草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicles/getdraft")
    public ResponseEntity<Fleet_vehicleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicleMapping.toDto(fleet_vehicleService.getDraft(new Fleet_vehicle())));
    }

    @ApiOperation(value = "检查车辆", tags = {"车辆" },  notes = "检查车辆")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicleDTO fleet_vehicledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicleService.checkKey(fleet_vehicleMapping.toDomain(fleet_vehicledto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicleMapping.toDomain(#fleet_vehicledto),'iBizBusinessCentral-Fleet_vehicle-Save')")
    @ApiOperation(value = "保存车辆", tags = {"车辆" },  notes = "保存车辆")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicleDTO fleet_vehicledto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicleService.save(fleet_vehicleMapping.toDomain(fleet_vehicledto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicleMapping.toDomain(#fleet_vehicledtos),'iBizBusinessCentral-Fleet_vehicle-Save')")
    @ApiOperation(value = "批量保存车辆", tags = {"车辆" },  notes = "批量保存车辆")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicleDTO> fleet_vehicledtos) {
        fleet_vehicleService.saveBatch(fleet_vehicleMapping.toDomain(fleet_vehicledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicles/fetchdefault")
	public ResponseEntity<List<Fleet_vehicleDTO>> fetchDefault(Fleet_vehicleSearchContext context) {
        Page<Fleet_vehicle> domains = fleet_vehicleService.searchDefault(context) ;
        List<Fleet_vehicleDTO> list = fleet_vehicleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicles/searchdefault")
	public ResponseEntity<Page<Fleet_vehicleDTO>> searchDefault(@RequestBody Fleet_vehicleSearchContext context) {
        Page<Fleet_vehicle> domains = fleet_vehicleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

