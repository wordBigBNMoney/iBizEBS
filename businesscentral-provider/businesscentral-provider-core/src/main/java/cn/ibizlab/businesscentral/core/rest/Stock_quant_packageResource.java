package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quant_packageService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quant_packageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"包裹" })
@RestController("Core-stock_quant_package")
@RequestMapping("")
public class Stock_quant_packageResource {

    @Autowired
    public IStock_quant_packageService stock_quant_packageService;

    @Autowired
    @Lazy
    public Stock_quant_packageMapping stock_quant_packageMapping;

    @PreAuthorize("hasPermission(this.stock_quant_packageMapping.toDomain(#stock_quant_packagedto),'iBizBusinessCentral-Stock_quant_package-Create')")
    @ApiOperation(value = "新建包裹", tags = {"包裹" },  notes = "新建包裹")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages")
    public ResponseEntity<Stock_quant_packageDTO> create(@Validated @RequestBody Stock_quant_packageDTO stock_quant_packagedto) {
        Stock_quant_package domain = stock_quant_packageMapping.toDomain(stock_quant_packagedto);
		stock_quant_packageService.create(domain);
        Stock_quant_packageDTO dto = stock_quant_packageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_quant_packageMapping.toDomain(#stock_quant_packagedtos),'iBizBusinessCentral-Stock_quant_package-Create')")
    @ApiOperation(value = "批量新建包裹", tags = {"包裹" },  notes = "批量新建包裹")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_quant_packageDTO> stock_quant_packagedtos) {
        stock_quant_packageService.createBatch(stock_quant_packageMapping.toDomain(stock_quant_packagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_quant_package" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_quant_packageService.get(#stock_quant_package_id),'iBizBusinessCentral-Stock_quant_package-Update')")
    @ApiOperation(value = "更新包裹", tags = {"包裹" },  notes = "更新包裹")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quant_packages/{stock_quant_package_id}")
    public ResponseEntity<Stock_quant_packageDTO> update(@PathVariable("stock_quant_package_id") Long stock_quant_package_id, @RequestBody Stock_quant_packageDTO stock_quant_packagedto) {
		Stock_quant_package domain  = stock_quant_packageMapping.toDomain(stock_quant_packagedto);
        domain .setId(stock_quant_package_id);
		stock_quant_packageService.update(domain );
		Stock_quant_packageDTO dto = stock_quant_packageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_quant_packageService.getStockQuantPackageByEntities(this.stock_quant_packageMapping.toDomain(#stock_quant_packagedtos)),'iBizBusinessCentral-Stock_quant_package-Update')")
    @ApiOperation(value = "批量更新包裹", tags = {"包裹" },  notes = "批量更新包裹")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quant_packages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quant_packageDTO> stock_quant_packagedtos) {
        stock_quant_packageService.updateBatch(stock_quant_packageMapping.toDomain(stock_quant_packagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_quant_packageService.get(#stock_quant_package_id),'iBizBusinessCentral-Stock_quant_package-Remove')")
    @ApiOperation(value = "删除包裹", tags = {"包裹" },  notes = "删除包裹")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quant_packages/{stock_quant_package_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_quant_package_id") Long stock_quant_package_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_quant_packageService.remove(stock_quant_package_id));
    }

    @PreAuthorize("hasPermission(this.stock_quant_packageService.getStockQuantPackageByIds(#ids),'iBizBusinessCentral-Stock_quant_package-Remove')")
    @ApiOperation(value = "批量删除包裹", tags = {"包裹" },  notes = "批量删除包裹")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quant_packages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_quant_packageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_quant_packageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_quant_package-Get')")
    @ApiOperation(value = "获取包裹", tags = {"包裹" },  notes = "获取包裹")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quant_packages/{stock_quant_package_id}")
    public ResponseEntity<Stock_quant_packageDTO> get(@PathVariable("stock_quant_package_id") Long stock_quant_package_id) {
        Stock_quant_package domain = stock_quant_packageService.get(stock_quant_package_id);
        Stock_quant_packageDTO dto = stock_quant_packageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取包裹草稿", tags = {"包裹" },  notes = "获取包裹草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quant_packages/getdraft")
    public ResponseEntity<Stock_quant_packageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_quant_packageMapping.toDto(stock_quant_packageService.getDraft(new Stock_quant_package())));
    }

    @ApiOperation(value = "检查包裹", tags = {"包裹" },  notes = "检查包裹")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_quant_packageDTO stock_quant_packagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_quant_packageService.checkKey(stock_quant_packageMapping.toDomain(stock_quant_packagedto)));
    }

    @PreAuthorize("hasPermission(this.stock_quant_packageMapping.toDomain(#stock_quant_packagedto),'iBizBusinessCentral-Stock_quant_package-Save')")
    @ApiOperation(value = "保存包裹", tags = {"包裹" },  notes = "保存包裹")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_quant_packageDTO stock_quant_packagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_quant_packageService.save(stock_quant_packageMapping.toDomain(stock_quant_packagedto)));
    }

    @PreAuthorize("hasPermission(this.stock_quant_packageMapping.toDomain(#stock_quant_packagedtos),'iBizBusinessCentral-Stock_quant_package-Save')")
    @ApiOperation(value = "批量保存包裹", tags = {"包裹" },  notes = "批量保存包裹")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_quant_packageDTO> stock_quant_packagedtos) {
        stock_quant_packageService.saveBatch(stock_quant_packageMapping.toDomain(stock_quant_packagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_quant_package-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_quant_package-Get')")
	@ApiOperation(value = "获取数据集", tags = {"包裹" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quant_packages/fetchdefault")
	public ResponseEntity<List<Stock_quant_packageDTO>> fetchDefault(Stock_quant_packageSearchContext context) {
        Page<Stock_quant_package> domains = stock_quant_packageService.searchDefault(context) ;
        List<Stock_quant_packageDTO> list = stock_quant_packageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_quant_package-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_quant_package-Get')")
	@ApiOperation(value = "查询数据集", tags = {"包裹" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_quant_packages/searchdefault")
	public ResponseEntity<Page<Stock_quant_packageDTO>> searchDefault(@RequestBody Stock_quant_packageSearchContext context) {
        Page<Stock_quant_package> domains = stock_quant_packageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_quant_packageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

