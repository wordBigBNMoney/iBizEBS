package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.businesscentral.core.odoo_portal.service.IPortal_shareService;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_shareSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"门户分享" })
@RestController("Core-portal_share")
@RequestMapping("")
public class Portal_shareResource {

    @Autowired
    public IPortal_shareService portal_shareService;

    @Autowired
    @Lazy
    public Portal_shareMapping portal_shareMapping;

    @PreAuthorize("hasPermission(this.portal_shareMapping.toDomain(#portal_sharedto),'iBizBusinessCentral-Portal_share-Create')")
    @ApiOperation(value = "新建门户分享", tags = {"门户分享" },  notes = "新建门户分享")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_shares")
    public ResponseEntity<Portal_shareDTO> create(@Validated @RequestBody Portal_shareDTO portal_sharedto) {
        Portal_share domain = portal_shareMapping.toDomain(portal_sharedto);
		portal_shareService.create(domain);
        Portal_shareDTO dto = portal_shareMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.portal_shareMapping.toDomain(#portal_sharedtos),'iBizBusinessCentral-Portal_share-Create')")
    @ApiOperation(value = "批量新建门户分享", tags = {"门户分享" },  notes = "批量新建门户分享")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_shares/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Portal_shareDTO> portal_sharedtos) {
        portal_shareService.createBatch(portal_shareMapping.toDomain(portal_sharedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "portal_share" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.portal_shareService.get(#portal_share_id),'iBizBusinessCentral-Portal_share-Update')")
    @ApiOperation(value = "更新门户分享", tags = {"门户分享" },  notes = "更新门户分享")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_shares/{portal_share_id}")
    public ResponseEntity<Portal_shareDTO> update(@PathVariable("portal_share_id") Long portal_share_id, @RequestBody Portal_shareDTO portal_sharedto) {
		Portal_share domain  = portal_shareMapping.toDomain(portal_sharedto);
        domain .setId(portal_share_id);
		portal_shareService.update(domain );
		Portal_shareDTO dto = portal_shareMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.portal_shareService.getPortalShareByEntities(this.portal_shareMapping.toDomain(#portal_sharedtos)),'iBizBusinessCentral-Portal_share-Update')")
    @ApiOperation(value = "批量更新门户分享", tags = {"门户分享" },  notes = "批量更新门户分享")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_shares/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_shareDTO> portal_sharedtos) {
        portal_shareService.updateBatch(portal_shareMapping.toDomain(portal_sharedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.portal_shareService.get(#portal_share_id),'iBizBusinessCentral-Portal_share-Remove')")
    @ApiOperation(value = "删除门户分享", tags = {"门户分享" },  notes = "删除门户分享")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_shares/{portal_share_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("portal_share_id") Long portal_share_id) {
         return ResponseEntity.status(HttpStatus.OK).body(portal_shareService.remove(portal_share_id));
    }

    @PreAuthorize("hasPermission(this.portal_shareService.getPortalShareByIds(#ids),'iBizBusinessCentral-Portal_share-Remove')")
    @ApiOperation(value = "批量删除门户分享", tags = {"门户分享" },  notes = "批量删除门户分享")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_shares/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        portal_shareService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.portal_shareMapping.toDomain(returnObject.body),'iBizBusinessCentral-Portal_share-Get')")
    @ApiOperation(value = "获取门户分享", tags = {"门户分享" },  notes = "获取门户分享")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_shares/{portal_share_id}")
    public ResponseEntity<Portal_shareDTO> get(@PathVariable("portal_share_id") Long portal_share_id) {
        Portal_share domain = portal_shareService.get(portal_share_id);
        Portal_shareDTO dto = portal_shareMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取门户分享草稿", tags = {"门户分享" },  notes = "获取门户分享草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_shares/getdraft")
    public ResponseEntity<Portal_shareDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(portal_shareMapping.toDto(portal_shareService.getDraft(new Portal_share())));
    }

    @ApiOperation(value = "检查门户分享", tags = {"门户分享" },  notes = "检查门户分享")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_shares/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Portal_shareDTO portal_sharedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(portal_shareService.checkKey(portal_shareMapping.toDomain(portal_sharedto)));
    }

    @PreAuthorize("hasPermission(this.portal_shareMapping.toDomain(#portal_sharedto),'iBizBusinessCentral-Portal_share-Save')")
    @ApiOperation(value = "保存门户分享", tags = {"门户分享" },  notes = "保存门户分享")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_shares/save")
    public ResponseEntity<Boolean> save(@RequestBody Portal_shareDTO portal_sharedto) {
        return ResponseEntity.status(HttpStatus.OK).body(portal_shareService.save(portal_shareMapping.toDomain(portal_sharedto)));
    }

    @PreAuthorize("hasPermission(this.portal_shareMapping.toDomain(#portal_sharedtos),'iBizBusinessCentral-Portal_share-Save')")
    @ApiOperation(value = "批量保存门户分享", tags = {"门户分享" },  notes = "批量保存门户分享")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_shares/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Portal_shareDTO> portal_sharedtos) {
        portal_shareService.saveBatch(portal_shareMapping.toDomain(portal_sharedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_share-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Portal_share-Get')")
	@ApiOperation(value = "获取数据集", tags = {"门户分享" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/portal_shares/fetchdefault")
	public ResponseEntity<List<Portal_shareDTO>> fetchDefault(Portal_shareSearchContext context) {
        Page<Portal_share> domains = portal_shareService.searchDefault(context) ;
        List<Portal_shareDTO> list = portal_shareMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_share-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Portal_share-Get')")
	@ApiOperation(value = "查询数据集", tags = {"门户分享" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/portal_shares/searchdefault")
	public ResponseEntity<Page<Portal_shareDTO>> searchDefault(@RequestBody Portal_shareSearchContext context) {
        Page<Portal_share> domains = portal_shareService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(portal_shareMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

