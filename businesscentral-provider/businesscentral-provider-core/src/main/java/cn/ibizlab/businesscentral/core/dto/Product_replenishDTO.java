package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Product_replenishDTO]
 */
@Data
public class Product_replenishDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PRODUCT_HAS_VARIANTS]
     *
     */
    @JSONField(name = "product_has_variants")
    @JsonProperty("product_has_variants")
    @NotNull(message = "[有变体]不允许为空!")
    private Boolean productHasVariants;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    @NotNull(message = "[数量]不允许为空!")
    private Double quantity;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PRODUCT_UOM_CATEGORY_ID]
     *
     */
    @JSONField(name = "product_uom_category_id")
    @JsonProperty("product_uom_category_id")
    @NotNull(message = "[类别]不允许为空!")
    private Integer productUomCategoryId;

    /**
     * 属性 [DATE_PLANNED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned")
    private Timestamp datePlanned;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String routeIds;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productUomIdText;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @JSONField(name = "product_tmpl_id_text")
    @JsonProperty("product_tmpl_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productTmplIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String warehouseIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[产品模板]不允许为空!")
    private Long productTmplId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[计量单位]不允许为空!")
    private Long productUomId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[产品]不允许为空!")
    private Long productId;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[仓库]不允许为空!")
    private Long warehouseId;


    /**
     * 设置 [PRODUCT_HAS_VARIANTS]
     */
    public void setProductHasVariants(Boolean  productHasVariants){
        this.productHasVariants = productHasVariants ;
        this.modify("product_has_variants",productHasVariants);
    }

    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [DATE_PLANNED]
     */
    public void setDatePlanned(Timestamp  datePlanned){
        this.datePlanned = datePlanned ;
        this.modify("date_planned",datePlanned);
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    public void setProductTmplId(Long  productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Long  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    public void setWarehouseId(Long  warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }


}


