package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.businesscentral.core.dto.Calendar_attendeeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreCalendar_attendeeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Calendar_attendeeMapping extends MappingBase<Calendar_attendeeDTO, Calendar_attendee> {


}

