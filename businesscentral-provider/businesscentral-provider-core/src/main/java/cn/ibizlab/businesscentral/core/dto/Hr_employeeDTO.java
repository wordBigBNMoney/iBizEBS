package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Hr_employeeDTO]
 */
@Data
public class Hr_employeeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MOBILE_PHONE]
     *
     */
    @JSONField(name = "mobile_phone")
    @JsonProperty("mobile_phone")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mobilePhone;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [LEAVE_DATE_FROM]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "leave_date_from" , format="yyyy-MM-dd")
    @JsonProperty("leave_date_from")
    private Timestamp leaveDateFrom;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [CHILDREN]
     *
     */
    @JSONField(name = "children")
    @JsonProperty("children")
    private Integer children;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [PIN]
     *
     */
    @JSONField(name = "pin")
    @JsonProperty("pin")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String pin;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [STUDY_SCHOOL]
     *
     */
    @JSONField(name = "study_school")
    @JsonProperty("study_school")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String studySchool;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [MARITAL]
     *
     */
    @JSONField(name = "marital")
    @JsonProperty("marital")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String marital;

    /**
     * 属性 [DIRECT_BADGE_IDS]
     *
     */
    @JSONField(name = "direct_badge_ids")
    @JsonProperty("direct_badge_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String directBadgeIds;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [EMERGENCY_PHONE]
     *
     */
    @JSONField(name = "emergency_phone")
    @JsonProperty("emergency_phone")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emergencyPhone;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [KM_HOME_WORK]
     *
     */
    @JSONField(name = "km_home_work")
    @JsonProperty("km_home_work")
    private Integer kmHomeWork;

    /**
     * 属性 [VISA_NO]
     *
     */
    @JSONField(name = "visa_no")
    @JsonProperty("visa_no")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String visaNo;

    /**
     * 属性 [PLACE_OF_BIRTH]
     *
     */
    @JSONField(name = "place_of_birth")
    @JsonProperty("place_of_birth")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String placeOfBirth;

    /**
     * 属性 [SPOUSE_COMPLETE_NAME]
     *
     */
    @JSONField(name = "spouse_complete_name")
    @JsonProperty("spouse_complete_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String spouseCompleteName;

    /**
     * 属性 [SINID]
     *
     */
    @JSONField(name = "sinid")
    @JsonProperty("sinid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sinid;

    /**
     * 属性 [BADGE_IDS]
     *
     */
    @JSONField(name = "badge_ids")
    @JsonProperty("badge_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String badgeIds;

    /**
     * 属性 [WORK_EMAIL]
     *
     */
    @JSONField(name = "work_email")
    @JsonProperty("work_email")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String workEmail;

    /**
     * 属性 [HAS_BADGES]
     *
     */
    @JSONField(name = "has_badges")
    @JsonProperty("has_badges")
    private Boolean hasBadges;

    /**
     * 属性 [CURRENT_LEAVE_ID]
     *
     */
    @JSONField(name = "current_leave_id")
    @JsonProperty("current_leave_id")
    private Integer currentLeaveId;

    /**
     * 属性 [CONTRACT_IDS]
     *
     */
    @JSONField(name = "contract_ids")
    @JsonProperty("contract_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String contractIds;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String barcode;

    /**
     * 属性 [BIRTHDAY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "birthday" , format="yyyy-MM-dd")
    @JsonProperty("birthday")
    private Timestamp birthday;

    /**
     * 属性 [CERTIFICATE]
     *
     */
    @JSONField(name = "certificate")
    @JsonProperty("certificate")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String certificate;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [STUDY_FIELD]
     *
     */
    @JSONField(name = "study_field")
    @JsonProperty("study_field")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String studyField;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [ATTENDANCE_STATE]
     *
     */
    @JSONField(name = "attendance_state")
    @JsonProperty("attendance_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String attendanceState;

    /**
     * 属性 [MANUALLY_SET_PRESENT]
     *
     */
    @JSONField(name = "manually_set_present")
    @JsonProperty("manually_set_present")
    private Boolean manuallySetPresent;

    /**
     * 属性 [CONTRACTS_COUNT]
     *
     */
    @JSONField(name = "contracts_count")
    @JsonProperty("contracts_count")
    private Integer contractsCount;

    /**
     * 属性 [GENDER]
     *
     */
    @JSONField(name = "gender")
    @JsonProperty("gender")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String gender;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [SPOUSE_BIRTHDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "spouse_birthdate" , format="yyyy-MM-dd")
    @JsonProperty("spouse_birthdate")
    private Timestamp spouseBirthdate;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [LEAVES_COUNT]
     *
     */
    @JSONField(name = "leaves_count")
    @JsonProperty("leaves_count")
    private Double leavesCount;

    /**
     * 属性 [NOTES]
     *
     */
    @JSONField(name = "notes")
    @JsonProperty("notes")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String notes;

    /**
     * 属性 [ADDITIONAL_NOTE]
     *
     */
    @JSONField(name = "additional_note")
    @JsonProperty("additional_note")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String additionalNote;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MEDIC_EXAM]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "medic_exam" , format="yyyy-MM-dd")
    @JsonProperty("medic_exam")
    private Timestamp medicExam;

    /**
     * 属性 [VISA_EXPIRE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "visa_expire" , format="yyyy-MM-dd")
    @JsonProperty("visa_expire")
    private Timestamp visaExpire;

    /**
     * 属性 [WORK_PHONE]
     *
     */
    @JSONField(name = "work_phone")
    @JsonProperty("work_phone")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String workPhone;

    /**
     * 属性 [EMERGENCY_CONTACT]
     *
     */
    @JSONField(name = "emergency_contact")
    @JsonProperty("emergency_contact")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emergencyContact;

    /**
     * 属性 [NEWLY_HIRED_EMPLOYEE]
     *
     */
    @JSONField(name = "newly_hired_employee")
    @JsonProperty("newly_hired_employee")
    private Boolean newlyHiredEmployee;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [IS_ADDRESS_HOME_A_COMPANY]
     *
     */
    @JSONField(name = "is_address_home_a_company")
    @JsonProperty("is_address_home_a_company")
    private Boolean isAddressHomeACompany;

    /**
     * 属性 [ATTENDANCE_IDS]
     *
     */
    @JSONField(name = "attendance_ids")
    @JsonProperty("attendance_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String attendanceIds;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [CURRENT_LEAVE_STATE]
     *
     */
    @JSONField(name = "current_leave_state")
    @JsonProperty("current_leave_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currentLeaveState;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String childIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [MANUAL_ATTENDANCE]
     *
     */
    @JSONField(name = "manual_attendance")
    @JsonProperty("manual_attendance")
    private Boolean manualAttendance;

    /**
     * 属性 [REMAINING_LEAVES]
     *
     */
    @JSONField(name = "remaining_leaves")
    @JsonProperty("remaining_leaves")
    private Double remainingLeaves;

    /**
     * 属性 [SSNID]
     *
     */
    @JSONField(name = "ssnid")
    @JsonProperty("ssnid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String ssnid;

    /**
     * 属性 [JOB_TITLE]
     *
     */
    @JSONField(name = "job_title")
    @JsonProperty("job_title")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String jobTitle;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [IS_ABSENT_TOTAY]
     *
     */
    @JSONField(name = "is_absent_totay")
    @JsonProperty("is_absent_totay")
    private Boolean isAbsentTotay;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [PASSPORT_ID]
     *
     */
    @JSONField(name = "passport_id")
    @JsonProperty("passport_id")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String passportId;

    /**
     * 属性 [GOAL_IDS]
     *
     */
    @JSONField(name = "goal_ids")
    @JsonProperty("goal_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String goalIds;

    /**
     * 属性 [LEAVE_DATE_TO]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "leave_date_to" , format="yyyy-MM-dd")
    @JsonProperty("leave_date_to")
    private Timestamp leaveDateTo;

    /**
     * 属性 [WORK_LOCATION]
     *
     */
    @JSONField(name = "work_location")
    @JsonProperty("work_location")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String workLocation;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [CONTRACT_ID]
     *
     */
    @JSONField(name = "contract_id")
    @JsonProperty("contract_id")
    private Integer contractId;

    /**
     * 属性 [PERMIT_NO]
     *
     */
    @JSONField(name = "permit_no")
    @JsonProperty("permit_no")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String permitNo;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [IDENTIFICATION_ID]
     *
     */
    @JSONField(name = "identification_id")
    @JsonProperty("identification_id")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String identificationId;

    /**
     * 属性 [VEHICLE]
     *
     */
    @JSONField(name = "vehicle")
    @JsonProperty("vehicle")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String vehicle;

    /**
     * 属性 [CATEGORY_IDS]
     *
     */
    @JSONField(name = "category_ids")
    @JsonProperty("category_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String categoryIds;

    /**
     * 属性 [SHOW_LEAVES]
     *
     */
    @JSONField(name = "show_leaves")
    @JsonProperty("show_leaves")
    private Boolean showLeaves;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String parentIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ADDRESS_HOME_ID_TEXT]
     *
     */
    @JSONField(name = "address_home_id_text")
    @JsonProperty("address_home_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String addressHomeIdText;

    /**
     * 属性 [TZ]
     *
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tz;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resourceCalendarIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String departmentIdText;

    /**
     * 属性 [COUNTRY_OF_BIRTH_TEXT]
     *
     */
    @JSONField(name = "country_of_birth_text")
    @JsonProperty("country_of_birth_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String countryOfBirthText;

    /**
     * 属性 [EXPENSE_MANAGER_ID_TEXT]
     *
     */
    @JSONField(name = "expense_manager_id_text")
    @JsonProperty("expense_manager_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String expenseManagerIdText;

    /**
     * 属性 [JOB_ID_TEXT]
     *
     */
    @JSONField(name = "job_id_text")
    @JsonProperty("job_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jobIdText;

    /**
     * 属性 [ADDRESS_ID_TEXT]
     *
     */
    @JSONField(name = "address_id_text")
    @JsonProperty("address_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String addressIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [COACH_ID_TEXT]
     *
     */
    @JSONField(name = "coach_id_text")
    @JsonProperty("coach_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String coachIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String countryIdText;

    /**
     * 属性 [ADDRESS_HOME_ID]
     *
     */
    @JSONField(name = "address_home_id")
    @JsonProperty("address_home_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long addressHomeId;

    /**
     * 属性 [EXPENSE_MANAGER_ID]
     *
     */
    @JSONField(name = "expense_manager_id")
    @JsonProperty("expense_manager_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long expenseManagerId;

    /**
     * 属性 [BANK_ACCOUNT_ID]
     *
     */
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long bankAccountId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [JOB_ID]
     *
     */
    @JSONField(name = "job_id")
    @JsonProperty("job_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long jobId;

    /**
     * 属性 [RESOURCE_ID]
     *
     */
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[资源]不允许为空!")
    private Long resourceId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long departmentId;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [LAST_ATTENDANCE_ID]
     *
     */
    @JSONField(name = "last_attendance_id")
    @JsonProperty("last_attendance_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lastAttendanceId;

    /**
     * 属性 [COACH_ID]
     *
     */
    @JSONField(name = "coach_id")
    @JsonProperty("coach_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long coachId;

    /**
     * 属性 [ADDRESS_ID]
     *
     */
    @JSONField(name = "address_id")
    @JsonProperty("address_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long addressId;

    /**
     * 属性 [COUNTRY_OF_BIRTH]
     *
     */
    @JSONField(name = "country_of_birth")
    @JsonProperty("country_of_birth")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryOfBirth;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resourceCalendarId;

    /**
     * 属性 [LEAVE_MANAGER_ID]
     *
     */
    @JSONField(name = "leave_manager_id")
    @JsonProperty("leave_manager_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long leaveManagerId;


    /**
     * 设置 [MOBILE_PHONE]
     */
    public void setMobilePhone(String  mobilePhone){
        this.mobilePhone = mobilePhone ;
        this.modify("mobile_phone",mobilePhone);
    }

    /**
     * 设置 [CHILDREN]
     */
    public void setChildren(Integer  children){
        this.children = children ;
        this.modify("children",children);
    }

    /**
     * 设置 [PIN]
     */
    public void setPin(String  pin){
        this.pin = pin ;
        this.modify("pin",pin);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [STUDY_SCHOOL]
     */
    public void setStudySchool(String  studySchool){
        this.studySchool = studySchool ;
        this.modify("study_school",studySchool);
    }

    /**
     * 设置 [MARITAL]
     */
    public void setMarital(String  marital){
        this.marital = marital ;
        this.modify("marital",marital);
    }

    /**
     * 设置 [EMERGENCY_PHONE]
     */
    public void setEmergencyPhone(String  emergencyPhone){
        this.emergencyPhone = emergencyPhone ;
        this.modify("emergency_phone",emergencyPhone);
    }

    /**
     * 设置 [KM_HOME_WORK]
     */
    public void setKmHomeWork(Integer  kmHomeWork){
        this.kmHomeWork = kmHomeWork ;
        this.modify("km_home_work",kmHomeWork);
    }

    /**
     * 设置 [VISA_NO]
     */
    public void setVisaNo(String  visaNo){
        this.visaNo = visaNo ;
        this.modify("visa_no",visaNo);
    }

    /**
     * 设置 [PLACE_OF_BIRTH]
     */
    public void setPlaceOfBirth(String  placeOfBirth){
        this.placeOfBirth = placeOfBirth ;
        this.modify("place_of_birth",placeOfBirth);
    }

    /**
     * 设置 [SPOUSE_COMPLETE_NAME]
     */
    public void setSpouseCompleteName(String  spouseCompleteName){
        this.spouseCompleteName = spouseCompleteName ;
        this.modify("spouse_complete_name",spouseCompleteName);
    }

    /**
     * 设置 [SINID]
     */
    public void setSinid(String  sinid){
        this.sinid = sinid ;
        this.modify("sinid",sinid);
    }

    /**
     * 设置 [WORK_EMAIL]
     */
    public void setWorkEmail(String  workEmail){
        this.workEmail = workEmail ;
        this.modify("work_email",workEmail);
    }

    /**
     * 设置 [BARCODE]
     */
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [BIRTHDAY]
     */
    public void setBirthday(Timestamp  birthday){
        this.birthday = birthday ;
        this.modify("birthday",birthday);
    }

    /**
     * 设置 [CERTIFICATE]
     */
    public void setCertificate(String  certificate){
        this.certificate = certificate ;
        this.modify("certificate",certificate);
    }

    /**
     * 设置 [STUDY_FIELD]
     */
    public void setStudyField(String  studyField){
        this.studyField = studyField ;
        this.modify("study_field",studyField);
    }

    /**
     * 设置 [MANUALLY_SET_PRESENT]
     */
    public void setManuallySetPresent(Boolean  manuallySetPresent){
        this.manuallySetPresent = manuallySetPresent ;
        this.modify("manually_set_present",manuallySetPresent);
    }

    /**
     * 设置 [GENDER]
     */
    public void setGender(String  gender){
        this.gender = gender ;
        this.modify("gender",gender);
    }

    /**
     * 设置 [SPOUSE_BIRTHDATE]
     */
    public void setSpouseBirthdate(Timestamp  spouseBirthdate){
        this.spouseBirthdate = spouseBirthdate ;
        this.modify("spouse_birthdate",spouseBirthdate);
    }

    /**
     * 设置 [NOTES]
     */
    public void setNotes(String  notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [ADDITIONAL_NOTE]
     */
    public void setAdditionalNote(String  additionalNote){
        this.additionalNote = additionalNote ;
        this.modify("additional_note",additionalNote);
    }

    /**
     * 设置 [MEDIC_EXAM]
     */
    public void setMedicExam(Timestamp  medicExam){
        this.medicExam = medicExam ;
        this.modify("medic_exam",medicExam);
    }

    /**
     * 设置 [VISA_EXPIRE]
     */
    public void setVisaExpire(Timestamp  visaExpire){
        this.visaExpire = visaExpire ;
        this.modify("visa_expire",visaExpire);
    }

    /**
     * 设置 [WORK_PHONE]
     */
    public void setWorkPhone(String  workPhone){
        this.workPhone = workPhone ;
        this.modify("work_phone",workPhone);
    }

    /**
     * 设置 [EMERGENCY_CONTACT]
     */
    public void setEmergencyContact(String  emergencyContact){
        this.emergencyContact = emergencyContact ;
        this.modify("emergency_contact",emergencyContact);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [SSNID]
     */
    public void setSsnid(String  ssnid){
        this.ssnid = ssnid ;
        this.modify("ssnid",ssnid);
    }

    /**
     * 设置 [JOB_TITLE]
     */
    public void setJobTitle(String  jobTitle){
        this.jobTitle = jobTitle ;
        this.modify("job_title",jobTitle);
    }

    /**
     * 设置 [PASSPORT_ID]
     */
    public void setPassportId(String  passportId){
        this.passportId = passportId ;
        this.modify("passport_id",passportId);
    }

    /**
     * 设置 [WORK_LOCATION]
     */
    public void setWorkLocation(String  workLocation){
        this.workLocation = workLocation ;
        this.modify("work_location",workLocation);
    }

    /**
     * 设置 [PERMIT_NO]
     */
    public void setPermitNo(String  permitNo){
        this.permitNo = permitNo ;
        this.modify("permit_no",permitNo);
    }

    /**
     * 设置 [IDENTIFICATION_ID]
     */
    public void setIdentificationId(String  identificationId){
        this.identificationId = identificationId ;
        this.modify("identification_id",identificationId);
    }

    /**
     * 设置 [VEHICLE]
     */
    public void setVehicle(String  vehicle){
        this.vehicle = vehicle ;
        this.modify("vehicle",vehicle);
    }

    /**
     * 设置 [ADDRESS_HOME_ID]
     */
    public void setAddressHomeId(Long  addressHomeId){
        this.addressHomeId = addressHomeId ;
        this.modify("address_home_id",addressHomeId);
    }

    /**
     * 设置 [EXPENSE_MANAGER_ID]
     */
    public void setExpenseManagerId(Long  expenseManagerId){
        this.expenseManagerId = expenseManagerId ;
        this.modify("expense_manager_id",expenseManagerId);
    }

    /**
     * 设置 [BANK_ACCOUNT_ID]
     */
    public void setBankAccountId(Long  bankAccountId){
        this.bankAccountId = bankAccountId ;
        this.modify("bank_account_id",bankAccountId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Long  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [JOB_ID]
     */
    public void setJobId(Long  jobId){
        this.jobId = jobId ;
        this.modify("job_id",jobId);
    }

    /**
     * 设置 [RESOURCE_ID]
     */
    public void setResourceId(Long  resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    public void setDepartmentId(Long  departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Long  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [LAST_ATTENDANCE_ID]
     */
    public void setLastAttendanceId(Long  lastAttendanceId){
        this.lastAttendanceId = lastAttendanceId ;
        this.modify("last_attendance_id",lastAttendanceId);
    }

    /**
     * 设置 [COACH_ID]
     */
    public void setCoachId(Long  coachId){
        this.coachId = coachId ;
        this.modify("coach_id",coachId);
    }

    /**
     * 设置 [ADDRESS_ID]
     */
    public void setAddressId(Long  addressId){
        this.addressId = addressId ;
        this.modify("address_id",addressId);
    }

    /**
     * 设置 [COUNTRY_OF_BIRTH]
     */
    public void setCountryOfBirth(Long  countryOfBirth){
        this.countryOfBirth = countryOfBirth ;
        this.modify("country_of_birth",countryOfBirth);
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    public void setResourceCalendarId(Long  resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [LEAVE_MANAGER_ID]
     */
    public void setLeaveManagerId(Long  leaveManagerId){
        this.leaveManagerId = leaveManagerId ;
        this.modify("leave_manager_id",leaveManagerId);
    }


}


