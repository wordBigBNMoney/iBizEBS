package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char_noreadonly;
import cn.ibizlab.businesscentral.core.dto.Base_import_tests_models_char_noreadonlyDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreBase_import_tests_models_char_noreadonlyMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_import_tests_models_char_noreadonlyMapping extends MappingBase<Base_import_tests_models_char_noreadonlyDTO, Base_import_tests_models_char_noreadonly> {


}

