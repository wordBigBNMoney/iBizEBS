package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Survey_mail_compose_messageDTO]
 */
@Data
public class Survey_mail_compose_messageDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [AUTO_DELETE]
     *
     */
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private Boolean autoDelete;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String ratingIds;

    /**
     * 属性 [NEEDACTION_PARTNER_IDS]
     *
     */
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String needactionPartnerIds;

    /**
     * 属性 [RATING_VALUE]
     *
     */
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String messageId;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 属性 [BODY]
     *
     */
    @JSONField(name = "body")
    @JsonProperty("body")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String body;

    /**
     * 属性 [STARRED_PARTNER_IDS]
     *
     */
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String starredPartnerIds;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String channelIds;

    /**
     * 属性 [STARRED]
     *
     */
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private Boolean starred;

    /**
     * 属性 [NEED_MODERATION]
     *
     */
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private Boolean needModeration;

    /**
     * 属性 [TRACKING_VALUE_IDS]
     *
     */
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String trackingValueIds;

    /**
     * 属性 [DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    private Timestamp dateDeadline;

    /**
     * 属性 [NO_AUTO_THREAD]
     *
     */
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private Boolean noAutoThread;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String subject;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String replyTo;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String description;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String partnerIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [HAS_ERROR]
     *
     */
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private Boolean hasError;

    /**
     * 属性 [MULTI_EMAIL]
     *
     */
    @JSONField(name = "multi_email")
    @JsonProperty("multi_email")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String multiEmail;

    /**
     * 属性 [USE_ACTIVE_DOMAIN]
     *
     */
    @JSONField(name = "use_active_domain")
    @JsonProperty("use_active_domain")
    private Boolean useActiveDomain;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String model;

    /**
     * 属性 [NEEDACTION]
     *
     */
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private Boolean needaction;

    /**
     * 属性 [LAYOUT]
     *
     */
    @JSONField(name = "layout")
    @JsonProperty("layout")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String layout;

    /**
     * 属性 [MASS_MAILING_NAME]
     *
     */
    @JSONField(name = "mass_mailing_name")
    @JsonProperty("mass_mailing_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String massMailingName;

    /**
     * 属性 [AUTO_DELETE_MESSAGE]
     *
     */
    @JSONField(name = "auto_delete_message")
    @JsonProperty("auto_delete_message")
    private Boolean autoDeleteMessage;

    /**
     * 属性 [PUBLIC_URL_HTML]
     *
     */
    @JSONField(name = "public_url_html")
    @JsonProperty("public_url_html")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String publicUrlHtml;

    /**
     * 属性 [RECORD_NAME]
     *
     */
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String recordName;

    /**
     * 属性 [PUBLIC_URL]
     *
     */
    @JSONField(name = "public_url")
    @JsonProperty("public_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String publicUrl;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String childIds;

    /**
     * 属性 [NOTIFICATION_IDS]
     *
     */
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String notificationIds;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emailFrom;

    /**
     * 属性 [IS_LOG]
     *
     */
    @JSONField(name = "is_log")
    @JsonProperty("is_log")
    private Boolean isLog;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String attachmentIds;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [ADD_SIGN]
     *
     */
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private Boolean addSign;

    /**
     * 属性 [MODERATION_STATUS]
     *
     */
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String moderationStatus;

    /**
     * 属性 [MESSAGE_TYPE]
     *
     */
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    @NotBlank(message = "[类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String messageType;

    /**
     * 属性 [IBIZPUBLIC]
     *
     */
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    @NotBlank(message = "[分享选项]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String ibizpublic;

    /**
     * 属性 [ACTIVE_DOMAIN]
     *
     */
    @JSONField(name = "active_domain")
    @JsonProperty("active_domain")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activeDomain;

    /**
     * 属性 [NOTIFY]
     *
     */
    @JSONField(name = "notify")
    @JsonProperty("notify")
    private Boolean notify;

    /**
     * 属性 [COMPOSITION_MODE]
     *
     */
    @JSONField(name = "composition_mode")
    @JsonProperty("composition_mode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String compositionMode;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;

    /**
     * 属性 [MAILING_LIST_IDS]
     *
     */
    @JSONField(name = "mailing_list_ids")
    @JsonProperty("mailing_list_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String mailingListIds;

    /**
     * 属性 [AUTHOR_ID_TEXT]
     *
     */
    @JSONField(name = "author_id_text")
    @JsonProperty("author_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String authorIdText;

    /**
     * 属性 [SUBTYPE_ID_TEXT]
     *
     */
    @JSONField(name = "subtype_id_text")
    @JsonProperty("subtype_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String subtypeIdText;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String templateIdText;

    /**
     * 属性 [AUTHOR_AVATAR]
     *
     */
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "mail_activity_type_id_text")
    @JsonProperty("mail_activity_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mailActivityTypeIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [MODERATOR_ID_TEXT]
     *
     */
    @JSONField(name = "moderator_id_text")
    @JsonProperty("moderator_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String moderatorIdText;

    /**
     * 属性 [MASS_MAILING_ID_TEXT]
     *
     */
    @JSONField(name = "mass_mailing_id_text")
    @JsonProperty("mass_mailing_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String massMailingIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     *
     */
    @JSONField(name = "mass_mailing_campaign_id_text")
    @JsonProperty("mass_mailing_campaign_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String massMailingCampaignIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [SURVEY_ID]
     *
     */
    @JSONField(name = "survey_id")
    @JsonProperty("survey_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[问卷]不允许为空!")
    private Long surveyId;

    /**
     * 属性 [AUTHOR_ID]
     *
     */
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long authorId;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID]
     *
     */
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long massMailingCampaignId;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [SUBTYPE_ID]
     *
     */
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long subtypeId;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long templateId;

    /**
     * 属性 [MASS_MAILING_ID]
     *
     */
    @JSONField(name = "mass_mailing_id")
    @JsonProperty("mass_mailing_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long massMailingId;

    /**
     * 属性 [MODERATOR_ID]
     *
     */
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long moderatorId;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mailActivityTypeId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [AUTO_DELETE]
     */
    public void setAutoDelete(Boolean  autoDelete){
        this.autoDelete = autoDelete ;
        this.modify("auto_delete",autoDelete);
    }

    /**
     * 设置 [MESSAGE_ID]
     */
    public void setMessageId(String  messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }

    /**
     * 设置 [MAIL_SERVER_ID]
     */
    public void setMailServerId(Integer  mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }

    /**
     * 设置 [BODY]
     */
    public void setBody(String  body){
        this.body = body ;
        this.modify("body",body);
    }

    /**
     * 设置 [DATE_DEADLINE]
     */
    public void setDateDeadline(Timestamp  dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }

    /**
     * 设置 [NO_AUTO_THREAD]
     */
    public void setNoAutoThread(Boolean  noAutoThread){
        this.noAutoThread = noAutoThread ;
        this.modify("no_auto_thread",noAutoThread);
    }

    /**
     * 设置 [SUBJECT]
     */
    public void setSubject(String  subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [REPLY_TO]
     */
    public void setReplyTo(String  replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }

    /**
     * 设置 [MULTI_EMAIL]
     */
    public void setMultiEmail(String  multiEmail){
        this.multiEmail = multiEmail ;
        this.modify("multi_email",multiEmail);
    }

    /**
     * 设置 [USE_ACTIVE_DOMAIN]
     */
    public void setUseActiveDomain(Boolean  useActiveDomain){
        this.useActiveDomain = useActiveDomain ;
        this.modify("use_active_domain",useActiveDomain);
    }

    /**
     * 设置 [MODEL]
     */
    public void setModel(String  model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [LAYOUT]
     */
    public void setLayout(String  layout){
        this.layout = layout ;
        this.modify("layout",layout);
    }

    /**
     * 设置 [MASS_MAILING_NAME]
     */
    public void setMassMailingName(String  massMailingName){
        this.massMailingName = massMailingName ;
        this.modify("mass_mailing_name",massMailingName);
    }

    /**
     * 设置 [AUTO_DELETE_MESSAGE]
     */
    public void setAutoDeleteMessage(Boolean  autoDeleteMessage){
        this.autoDeleteMessage = autoDeleteMessage ;
        this.modify("auto_delete_message",autoDeleteMessage);
    }

    /**
     * 设置 [RECORD_NAME]
     */
    public void setRecordName(String  recordName){
        this.recordName = recordName ;
        this.modify("record_name",recordName);
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    public void setEmailFrom(String  emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [IS_LOG]
     */
    public void setIsLog(Boolean  isLog){
        this.isLog = isLog ;
        this.modify("is_log",isLog);
    }

    /**
     * 设置 [RES_ID]
     */
    public void setResId(Integer  resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [ADD_SIGN]
     */
    public void setAddSign(Boolean  addSign){
        this.addSign = addSign ;
        this.modify("add_sign",addSign);
    }

    /**
     * 设置 [MODERATION_STATUS]
     */
    public void setModerationStatus(String  moderationStatus){
        this.moderationStatus = moderationStatus ;
        this.modify("moderation_status",moderationStatus);
    }

    /**
     * 设置 [MESSAGE_TYPE]
     */
    public void setMessageType(String  messageType){
        this.messageType = messageType ;
        this.modify("message_type",messageType);
    }

    /**
     * 设置 [IBIZPUBLIC]
     */
    public void setIbizpublic(String  ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.modify("ibizpublic",ibizpublic);
    }

    /**
     * 设置 [ACTIVE_DOMAIN]
     */
    public void setActiveDomain(String  activeDomain){
        this.activeDomain = activeDomain ;
        this.modify("active_domain",activeDomain);
    }

    /**
     * 设置 [NOTIFY]
     */
    public void setNotify(Boolean  notify){
        this.notify = notify ;
        this.modify("notify",notify);
    }

    /**
     * 设置 [COMPOSITION_MODE]
     */
    public void setCompositionMode(String  compositionMode){
        this.compositionMode = compositionMode ;
        this.modify("composition_mode",compositionMode);
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    public void setWebsitePublished(Boolean  websitePublished){
        this.websitePublished = websitePublished ;
        this.modify("website_published",websitePublished);
    }

    /**
     * 设置 [SURVEY_ID]
     */
    public void setSurveyId(Long  surveyId){
        this.surveyId = surveyId ;
        this.modify("survey_id",surveyId);
    }

    /**
     * 设置 [AUTHOR_ID]
     */
    public void setAuthorId(Long  authorId){
        this.authorId = authorId ;
        this.modify("author_id",authorId);
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID]
     */
    public void setMassMailingCampaignId(Long  massMailingCampaignId){
        this.massMailingCampaignId = massMailingCampaignId ;
        this.modify("mass_mailing_campaign_id",massMailingCampaignId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Long  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [SUBTYPE_ID]
     */
    public void setSubtypeId(Long  subtypeId){
        this.subtypeId = subtypeId ;
        this.modify("subtype_id",subtypeId);
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    public void setTemplateId(Long  templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }

    /**
     * 设置 [MASS_MAILING_ID]
     */
    public void setMassMailingId(Long  massMailingId){
        this.massMailingId = massMailingId ;
        this.modify("mass_mailing_id",massMailingId);
    }

    /**
     * 设置 [MODERATOR_ID]
     */
    public void setModeratorId(Long  moderatorId){
        this.moderatorId = moderatorId ;
        this.modify("moderator_id",moderatorId);
    }

    /**
     * 设置 [MAIL_ACTIVITY_TYPE_ID]
     */
    public void setMailActivityTypeId(Long  mailActivityTypeId){
        this.mailActivityTypeId = mailActivityTypeId ;
        this.modify("mail_activity_type_id",mailActivityTypeId);
    }


}


