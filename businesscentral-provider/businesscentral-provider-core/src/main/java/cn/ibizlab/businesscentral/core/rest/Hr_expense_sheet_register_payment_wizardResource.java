package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheet_register_payment_wizardService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_sheet_register_payment_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"费用登记付款向导" })
@RestController("Core-hr_expense_sheet_register_payment_wizard")
@RequestMapping("")
public class Hr_expense_sheet_register_payment_wizardResource {

    @Autowired
    public IHr_expense_sheet_register_payment_wizardService hr_expense_sheet_register_payment_wizardService;

    @Autowired
    @Lazy
    public Hr_expense_sheet_register_payment_wizardMapping hr_expense_sheet_register_payment_wizardMapping;

    @PreAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardMapping.toDomain(#hr_expense_sheet_register_payment_wizarddto),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Create')")
    @ApiOperation(value = "新建费用登记付款向导", tags = {"费用登记付款向导" },  notes = "新建费用登记付款向导")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheet_register_payment_wizards")
    public ResponseEntity<Hr_expense_sheet_register_payment_wizardDTO> create(@Validated @RequestBody Hr_expense_sheet_register_payment_wizardDTO hr_expense_sheet_register_payment_wizarddto) {
        Hr_expense_sheet_register_payment_wizard domain = hr_expense_sheet_register_payment_wizardMapping.toDomain(hr_expense_sheet_register_payment_wizarddto);
		hr_expense_sheet_register_payment_wizardService.create(domain);
        Hr_expense_sheet_register_payment_wizardDTO dto = hr_expense_sheet_register_payment_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardMapping.toDomain(#hr_expense_sheet_register_payment_wizarddtos),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Create')")
    @ApiOperation(value = "批量新建费用登记付款向导", tags = {"费用登记付款向导" },  notes = "批量新建费用登记付款向导")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheet_register_payment_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_expense_sheet_register_payment_wizardDTO> hr_expense_sheet_register_payment_wizarddtos) {
        hr_expense_sheet_register_payment_wizardService.createBatch(hr_expense_sheet_register_payment_wizardMapping.toDomain(hr_expense_sheet_register_payment_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_expense_sheet_register_payment_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardService.get(#hr_expense_sheet_register_payment_wizard_id),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Update')")
    @ApiOperation(value = "更新费用登记付款向导", tags = {"费用登记付款向导" },  notes = "更新费用登记付款向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheet_register_payment_wizards/{hr_expense_sheet_register_payment_wizard_id}")
    public ResponseEntity<Hr_expense_sheet_register_payment_wizardDTO> update(@PathVariable("hr_expense_sheet_register_payment_wizard_id") Long hr_expense_sheet_register_payment_wizard_id, @RequestBody Hr_expense_sheet_register_payment_wizardDTO hr_expense_sheet_register_payment_wizarddto) {
		Hr_expense_sheet_register_payment_wizard domain  = hr_expense_sheet_register_payment_wizardMapping.toDomain(hr_expense_sheet_register_payment_wizarddto);
        domain .setId(hr_expense_sheet_register_payment_wizard_id);
		hr_expense_sheet_register_payment_wizardService.update(domain );
		Hr_expense_sheet_register_payment_wizardDTO dto = hr_expense_sheet_register_payment_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardService.getHrExpenseSheetRegisterPaymentWizardByEntities(this.hr_expense_sheet_register_payment_wizardMapping.toDomain(#hr_expense_sheet_register_payment_wizarddtos)),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Update')")
    @ApiOperation(value = "批量更新费用登记付款向导", tags = {"费用登记付款向导" },  notes = "批量更新费用登记付款向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheet_register_payment_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expense_sheet_register_payment_wizardDTO> hr_expense_sheet_register_payment_wizarddtos) {
        hr_expense_sheet_register_payment_wizardService.updateBatch(hr_expense_sheet_register_payment_wizardMapping.toDomain(hr_expense_sheet_register_payment_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardService.get(#hr_expense_sheet_register_payment_wizard_id),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Remove')")
    @ApiOperation(value = "删除费用登记付款向导", tags = {"费用登记付款向导" },  notes = "删除费用登记付款向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheet_register_payment_wizards/{hr_expense_sheet_register_payment_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_sheet_register_payment_wizard_id") Long hr_expense_sheet_register_payment_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheet_register_payment_wizardService.remove(hr_expense_sheet_register_payment_wizard_id));
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardService.getHrExpenseSheetRegisterPaymentWizardByIds(#ids),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Remove')")
    @ApiOperation(value = "批量删除费用登记付款向导", tags = {"费用登记付款向导" },  notes = "批量删除费用登记付款向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheet_register_payment_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_expense_sheet_register_payment_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Get')")
    @ApiOperation(value = "获取费用登记付款向导", tags = {"费用登记付款向导" },  notes = "获取费用登记付款向导")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheet_register_payment_wizards/{hr_expense_sheet_register_payment_wizard_id}")
    public ResponseEntity<Hr_expense_sheet_register_payment_wizardDTO> get(@PathVariable("hr_expense_sheet_register_payment_wizard_id") Long hr_expense_sheet_register_payment_wizard_id) {
        Hr_expense_sheet_register_payment_wizard domain = hr_expense_sheet_register_payment_wizardService.get(hr_expense_sheet_register_payment_wizard_id);
        Hr_expense_sheet_register_payment_wizardDTO dto = hr_expense_sheet_register_payment_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取费用登记付款向导草稿", tags = {"费用登记付款向导" },  notes = "获取费用登记付款向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheet_register_payment_wizards/getdraft")
    public ResponseEntity<Hr_expense_sheet_register_payment_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheet_register_payment_wizardMapping.toDto(hr_expense_sheet_register_payment_wizardService.getDraft(new Hr_expense_sheet_register_payment_wizard())));
    }

    @ApiOperation(value = "检查费用登记付款向导", tags = {"费用登记付款向导" },  notes = "检查费用登记付款向导")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheet_register_payment_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_expense_sheet_register_payment_wizardDTO hr_expense_sheet_register_payment_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheet_register_payment_wizardService.checkKey(hr_expense_sheet_register_payment_wizardMapping.toDomain(hr_expense_sheet_register_payment_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardMapping.toDomain(#hr_expense_sheet_register_payment_wizarddto),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Save')")
    @ApiOperation(value = "保存费用登记付款向导", tags = {"费用登记付款向导" },  notes = "保存费用登记付款向导")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheet_register_payment_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_expense_sheet_register_payment_wizardDTO hr_expense_sheet_register_payment_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheet_register_payment_wizardService.save(hr_expense_sheet_register_payment_wizardMapping.toDomain(hr_expense_sheet_register_payment_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheet_register_payment_wizardMapping.toDomain(#hr_expense_sheet_register_payment_wizarddtos),'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Save')")
    @ApiOperation(value = "批量保存费用登记付款向导", tags = {"费用登记付款向导" },  notes = "批量保存费用登记付款向导")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheet_register_payment_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_expense_sheet_register_payment_wizardDTO> hr_expense_sheet_register_payment_wizarddtos) {
        hr_expense_sheet_register_payment_wizardService.saveBatch(hr_expense_sheet_register_payment_wizardMapping.toDomain(hr_expense_sheet_register_payment_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"费用登记付款向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expense_sheet_register_payment_wizards/fetchdefault")
	public ResponseEntity<List<Hr_expense_sheet_register_payment_wizardDTO>> fetchDefault(Hr_expense_sheet_register_payment_wizardSearchContext context) {
        Page<Hr_expense_sheet_register_payment_wizard> domains = hr_expense_sheet_register_payment_wizardService.searchDefault(context) ;
        List<Hr_expense_sheet_register_payment_wizardDTO> list = hr_expense_sheet_register_payment_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_expense_sheet_register_payment_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"费用登记付款向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_expense_sheet_register_payment_wizards/searchdefault")
	public ResponseEntity<Page<Hr_expense_sheet_register_payment_wizardDTO>> searchDefault(@RequestBody Hr_expense_sheet_register_payment_wizardSearchContext context) {
        Page<Hr_expense_sheet_register_payment_wizard> domains = hr_expense_sheet_register_payment_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_expense_sheet_register_payment_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

