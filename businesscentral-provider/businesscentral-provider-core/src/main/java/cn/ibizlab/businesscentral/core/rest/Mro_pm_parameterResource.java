package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_parameter;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_parameterService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_parameterSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Asset Parameters" })
@RestController("Core-mro_pm_parameter")
@RequestMapping("")
public class Mro_pm_parameterResource {

    @Autowired
    public IMro_pm_parameterService mro_pm_parameterService;

    @Autowired
    @Lazy
    public Mro_pm_parameterMapping mro_pm_parameterMapping;

    @PreAuthorize("hasPermission(this.mro_pm_parameterMapping.toDomain(#mro_pm_parameterdto),'iBizBusinessCentral-Mro_pm_parameter-Create')")
    @ApiOperation(value = "新建Asset Parameters", tags = {"Asset Parameters" },  notes = "新建Asset Parameters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters")
    public ResponseEntity<Mro_pm_parameterDTO> create(@Validated @RequestBody Mro_pm_parameterDTO mro_pm_parameterdto) {
        Mro_pm_parameter domain = mro_pm_parameterMapping.toDomain(mro_pm_parameterdto);
		mro_pm_parameterService.create(domain);
        Mro_pm_parameterDTO dto = mro_pm_parameterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_parameterMapping.toDomain(#mro_pm_parameterdtos),'iBizBusinessCentral-Mro_pm_parameter-Create')")
    @ApiOperation(value = "批量新建Asset Parameters", tags = {"Asset Parameters" },  notes = "批量新建Asset Parameters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_parameterDTO> mro_pm_parameterdtos) {
        mro_pm_parameterService.createBatch(mro_pm_parameterMapping.toDomain(mro_pm_parameterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_pm_parameter" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_pm_parameterService.get(#mro_pm_parameter_id),'iBizBusinessCentral-Mro_pm_parameter-Update')")
    @ApiOperation(value = "更新Asset Parameters", tags = {"Asset Parameters" },  notes = "更新Asset Parameters")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_parameters/{mro_pm_parameter_id}")
    public ResponseEntity<Mro_pm_parameterDTO> update(@PathVariable("mro_pm_parameter_id") Long mro_pm_parameter_id, @RequestBody Mro_pm_parameterDTO mro_pm_parameterdto) {
		Mro_pm_parameter domain  = mro_pm_parameterMapping.toDomain(mro_pm_parameterdto);
        domain .setId(mro_pm_parameter_id);
		mro_pm_parameterService.update(domain );
		Mro_pm_parameterDTO dto = mro_pm_parameterMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_parameterService.getMroPmParameterByEntities(this.mro_pm_parameterMapping.toDomain(#mro_pm_parameterdtos)),'iBizBusinessCentral-Mro_pm_parameter-Update')")
    @ApiOperation(value = "批量更新Asset Parameters", tags = {"Asset Parameters" },  notes = "批量更新Asset Parameters")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_parameters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_parameterDTO> mro_pm_parameterdtos) {
        mro_pm_parameterService.updateBatch(mro_pm_parameterMapping.toDomain(mro_pm_parameterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_pm_parameterService.get(#mro_pm_parameter_id),'iBizBusinessCentral-Mro_pm_parameter-Remove')")
    @ApiOperation(value = "删除Asset Parameters", tags = {"Asset Parameters" },  notes = "删除Asset Parameters")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_parameters/{mro_pm_parameter_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_parameter_id") Long mro_pm_parameter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_parameterService.remove(mro_pm_parameter_id));
    }

    @PreAuthorize("hasPermission(this.mro_pm_parameterService.getMroPmParameterByIds(#ids),'iBizBusinessCentral-Mro_pm_parameter-Remove')")
    @ApiOperation(value = "批量删除Asset Parameters", tags = {"Asset Parameters" },  notes = "批量删除Asset Parameters")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_parameters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_pm_parameterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_pm_parameterMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_pm_parameter-Get')")
    @ApiOperation(value = "获取Asset Parameters", tags = {"Asset Parameters" },  notes = "获取Asset Parameters")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_parameters/{mro_pm_parameter_id}")
    public ResponseEntity<Mro_pm_parameterDTO> get(@PathVariable("mro_pm_parameter_id") Long mro_pm_parameter_id) {
        Mro_pm_parameter domain = mro_pm_parameterService.get(mro_pm_parameter_id);
        Mro_pm_parameterDTO dto = mro_pm_parameterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Asset Parameters草稿", tags = {"Asset Parameters" },  notes = "获取Asset Parameters草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_parameters/getdraft")
    public ResponseEntity<Mro_pm_parameterDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_parameterMapping.toDto(mro_pm_parameterService.getDraft(new Mro_pm_parameter())));
    }

    @ApiOperation(value = "检查Asset Parameters", tags = {"Asset Parameters" },  notes = "检查Asset Parameters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_pm_parameterDTO mro_pm_parameterdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_pm_parameterService.checkKey(mro_pm_parameterMapping.toDomain(mro_pm_parameterdto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_parameterMapping.toDomain(#mro_pm_parameterdto),'iBizBusinessCentral-Mro_pm_parameter-Save')")
    @ApiOperation(value = "保存Asset Parameters", tags = {"Asset Parameters" },  notes = "保存Asset Parameters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_pm_parameterDTO mro_pm_parameterdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_parameterService.save(mro_pm_parameterMapping.toDomain(mro_pm_parameterdto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_parameterMapping.toDomain(#mro_pm_parameterdtos),'iBizBusinessCentral-Mro_pm_parameter-Save')")
    @ApiOperation(value = "批量保存Asset Parameters", tags = {"Asset Parameters" },  notes = "批量保存Asset Parameters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_pm_parameterDTO> mro_pm_parameterdtos) {
        mro_pm_parameterService.saveBatch(mro_pm_parameterMapping.toDomain(mro_pm_parameterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_parameter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_parameter-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Asset Parameters" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_parameters/fetchdefault")
	public ResponseEntity<List<Mro_pm_parameterDTO>> fetchDefault(Mro_pm_parameterSearchContext context) {
        Page<Mro_pm_parameter> domains = mro_pm_parameterService.searchDefault(context) ;
        List<Mro_pm_parameterDTO> list = mro_pm_parameterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_parameter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_parameter-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Asset Parameters" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_pm_parameters/searchdefault")
	public ResponseEntity<Page<Mro_pm_parameterDTO>> searchDefault(@RequestBody Mro_pm_parameterSearchContext context) {
        Page<Mro_pm_parameter> domains = mro_pm_parameterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_parameterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

