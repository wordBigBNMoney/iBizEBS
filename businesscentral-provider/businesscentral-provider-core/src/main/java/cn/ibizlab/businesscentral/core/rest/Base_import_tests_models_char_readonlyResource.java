package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char_readonly;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_char_readonlyService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_char_readonlySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型，字符只读" })
@RestController("Core-base_import_tests_models_char_readonly")
@RequestMapping("")
public class Base_import_tests_models_char_readonlyResource {

    @Autowired
    public IBase_import_tests_models_char_readonlyService base_import_tests_models_char_readonlyService;

    @Autowired
    @Lazy
    public Base_import_tests_models_char_readonlyMapping base_import_tests_models_char_readonlyMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_readonlyMapping.toDomain(#base_import_tests_models_char_readonlydto),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Create')")
    @ApiOperation(value = "新建测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "新建测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_readonlies")
    public ResponseEntity<Base_import_tests_models_char_readonlyDTO> create(@Validated @RequestBody Base_import_tests_models_char_readonlyDTO base_import_tests_models_char_readonlydto) {
        Base_import_tests_models_char_readonly domain = base_import_tests_models_char_readonlyMapping.toDomain(base_import_tests_models_char_readonlydto);
		base_import_tests_models_char_readonlyService.create(domain);
        Base_import_tests_models_char_readonlyDTO dto = base_import_tests_models_char_readonlyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_readonlyMapping.toDomain(#base_import_tests_models_char_readonlydtos),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "批量新建测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_readonlies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_char_readonlyDTO> base_import_tests_models_char_readonlydtos) {
        base_import_tests_models_char_readonlyService.createBatch(base_import_tests_models_char_readonlyMapping.toDomain(base_import_tests_models_char_readonlydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_char_readonly" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_char_readonlyService.get(#base_import_tests_models_char_readonly_id),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Update')")
    @ApiOperation(value = "更新测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "更新测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_readonlies/{base_import_tests_models_char_readonly_id}")
    public ResponseEntity<Base_import_tests_models_char_readonlyDTO> update(@PathVariable("base_import_tests_models_char_readonly_id") Long base_import_tests_models_char_readonly_id, @RequestBody Base_import_tests_models_char_readonlyDTO base_import_tests_models_char_readonlydto) {
		Base_import_tests_models_char_readonly domain  = base_import_tests_models_char_readonlyMapping.toDomain(base_import_tests_models_char_readonlydto);
        domain .setId(base_import_tests_models_char_readonly_id);
		base_import_tests_models_char_readonlyService.update(domain );
		Base_import_tests_models_char_readonlyDTO dto = base_import_tests_models_char_readonlyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_readonlyService.getBaseImportTestsModelsCharReadonlyByEntities(this.base_import_tests_models_char_readonlyMapping.toDomain(#base_import_tests_models_char_readonlydtos)),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "批量更新测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_readonlies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_readonlyDTO> base_import_tests_models_char_readonlydtos) {
        base_import_tests_models_char_readonlyService.updateBatch(base_import_tests_models_char_readonlyMapping.toDomain(base_import_tests_models_char_readonlydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_readonlyService.get(#base_import_tests_models_char_readonly_id),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "删除测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_readonlies/{base_import_tests_models_char_readonly_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_readonly_id") Long base_import_tests_models_char_readonly_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_readonlyService.remove(base_import_tests_models_char_readonly_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_readonlyService.getBaseImportTestsModelsCharReadonlyByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "批量删除测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_readonlies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_char_readonlyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_char_readonlyMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Get')")
    @ApiOperation(value = "获取测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "获取测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_readonlies/{base_import_tests_models_char_readonly_id}")
    public ResponseEntity<Base_import_tests_models_char_readonlyDTO> get(@PathVariable("base_import_tests_models_char_readonly_id") Long base_import_tests_models_char_readonly_id) {
        Base_import_tests_models_char_readonly domain = base_import_tests_models_char_readonlyService.get(base_import_tests_models_char_readonly_id);
        Base_import_tests_models_char_readonlyDTO dto = base_import_tests_models_char_readonlyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型，字符只读草稿", tags = {"测试:基本导入模型，字符只读" },  notes = "获取测试:基本导入模型，字符只读草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_readonlies/getdraft")
    public ResponseEntity<Base_import_tests_models_char_readonlyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_readonlyMapping.toDto(base_import_tests_models_char_readonlyService.getDraft(new Base_import_tests_models_char_readonly())));
    }

    @ApiOperation(value = "检查测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "检查测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_readonlies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_char_readonlyDTO base_import_tests_models_char_readonlydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_readonlyService.checkKey(base_import_tests_models_char_readonlyMapping.toDomain(base_import_tests_models_char_readonlydto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_readonlyMapping.toDomain(#base_import_tests_models_char_readonlydto),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Save')")
    @ApiOperation(value = "保存测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "保存测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_readonlies/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_char_readonlyDTO base_import_tests_models_char_readonlydto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_readonlyService.save(base_import_tests_models_char_readonlyMapping.toDomain(base_import_tests_models_char_readonlydto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_readonlyMapping.toDomain(#base_import_tests_models_char_readonlydtos),'iBizBusinessCentral-Base_import_tests_models_char_readonly-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型，字符只读", tags = {"测试:基本导入模型，字符只读" },  notes = "批量保存测试:基本导入模型，字符只读")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_readonlies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_char_readonlyDTO> base_import_tests_models_char_readonlydtos) {
        base_import_tests_models_char_readonlyService.saveBatch(base_import_tests_models_char_readonlyMapping.toDomain(base_import_tests_models_char_readonlydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_char_readonly-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_char_readonly-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型，字符只读" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_char_readonlies/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_char_readonlyDTO>> fetchDefault(Base_import_tests_models_char_readonlySearchContext context) {
        Page<Base_import_tests_models_char_readonly> domains = base_import_tests_models_char_readonlyService.searchDefault(context) ;
        List<Base_import_tests_models_char_readonlyDTO> list = base_import_tests_models_char_readonlyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_char_readonly-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_char_readonly-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型，字符只读" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_char_readonlies/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_readonlyDTO>> searchDefault(@RequestBody Base_import_tests_models_char_readonlySearchContext context) {
        Page<Base_import_tests_models_char_readonly> domains = base_import_tests_models_char_readonlyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_char_readonlyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

