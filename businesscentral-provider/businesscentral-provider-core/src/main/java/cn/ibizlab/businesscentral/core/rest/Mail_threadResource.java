package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_thread;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_threadService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_threadSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"EMail线程" })
@RestController("Core-mail_thread")
@RequestMapping("")
public class Mail_threadResource {

    @Autowired
    public IMail_threadService mail_threadService;

    @Autowired
    @Lazy
    public Mail_threadMapping mail_threadMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Create-all')")
    @ApiOperation(value = "新建EMail线程", tags = {"EMail线程" },  notes = "新建EMail线程")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_threads")
    public ResponseEntity<Mail_threadDTO> create(@Validated @RequestBody Mail_threadDTO mail_threaddto) {
        Mail_thread domain = mail_threadMapping.toDomain(mail_threaddto);
		mail_threadService.create(domain);
        Mail_threadDTO dto = mail_threadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Create-all')")
    @ApiOperation(value = "批量新建EMail线程", tags = {"EMail线程" },  notes = "批量新建EMail线程")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_threads/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_threadDTO> mail_threaddtos) {
        mail_threadService.createBatch(mail_threadMapping.toDomain(mail_threaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Update-all')")
    @ApiOperation(value = "更新EMail线程", tags = {"EMail线程" },  notes = "更新EMail线程")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_threads/{mail_thread_id}")
    public ResponseEntity<Mail_threadDTO> update(@PathVariable("mail_thread_id") Long mail_thread_id, @RequestBody Mail_threadDTO mail_threaddto) {
		Mail_thread domain  = mail_threadMapping.toDomain(mail_threaddto);
        domain .setId(mail_thread_id);
		mail_threadService.update(domain );
		Mail_threadDTO dto = mail_threadMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Update-all')")
    @ApiOperation(value = "批量更新EMail线程", tags = {"EMail线程" },  notes = "批量更新EMail线程")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_threads/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_threadDTO> mail_threaddtos) {
        mail_threadService.updateBatch(mail_threadMapping.toDomain(mail_threaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Remove-all')")
    @ApiOperation(value = "删除EMail线程", tags = {"EMail线程" },  notes = "删除EMail线程")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_threads/{mail_thread_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_thread_id") Long mail_thread_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_threadService.remove(mail_thread_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Remove-all')")
    @ApiOperation(value = "批量删除EMail线程", tags = {"EMail线程" },  notes = "批量删除EMail线程")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_threads/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_threadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Get-all')")
    @ApiOperation(value = "获取EMail线程", tags = {"EMail线程" },  notes = "获取EMail线程")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_threads/{mail_thread_id}")
    public ResponseEntity<Mail_threadDTO> get(@PathVariable("mail_thread_id") Long mail_thread_id) {
        Mail_thread domain = mail_threadService.get(mail_thread_id);
        Mail_threadDTO dto = mail_threadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取EMail线程草稿", tags = {"EMail线程" },  notes = "获取EMail线程草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_threads/getdraft")
    public ResponseEntity<Mail_threadDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_threadMapping.toDto(mail_threadService.getDraft(new Mail_thread())));
    }

    @ApiOperation(value = "检查EMail线程", tags = {"EMail线程" },  notes = "检查EMail线程")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_threads/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_threadDTO mail_threaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_threadService.checkKey(mail_threadMapping.toDomain(mail_threaddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Save-all')")
    @ApiOperation(value = "保存EMail线程", tags = {"EMail线程" },  notes = "保存EMail线程")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_threads/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_threadDTO mail_threaddto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_threadService.save(mail_threadMapping.toDomain(mail_threaddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-Save-all')")
    @ApiOperation(value = "批量保存EMail线程", tags = {"EMail线程" },  notes = "批量保存EMail线程")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_threads/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_threadDTO> mail_threaddtos) {
        mail_threadService.saveBatch(mail_threadMapping.toDomain(mail_threaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"EMail线程" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_threads/fetchdefault")
	public ResponseEntity<List<Mail_threadDTO>> fetchDefault(Mail_threadSearchContext context) {
        Page<Mail_thread> domains = mail_threadService.searchDefault(context) ;
        List<Mail_threadDTO> list = mail_threadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_thread-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"EMail线程" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_threads/searchdefault")
	public ResponseEntity<Page<Mail_threadDTO>> searchDefault(@RequestBody Mail_threadSearchContext context) {
        Page<Mail_thread> domains = mail_threadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_threadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

