package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_resend_cancelService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_resend_cancelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"重发请求被拒绝" })
@RestController("Core-mail_resend_cancel")
@RequestMapping("")
public class Mail_resend_cancelResource {

    @Autowired
    public IMail_resend_cancelService mail_resend_cancelService;

    @Autowired
    @Lazy
    public Mail_resend_cancelMapping mail_resend_cancelMapping;

    @PreAuthorize("hasPermission(this.mail_resend_cancelMapping.toDomain(#mail_resend_canceldto),'iBizBusinessCentral-Mail_resend_cancel-Create')")
    @ApiOperation(value = "新建重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "新建重发请求被拒绝")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels")
    public ResponseEntity<Mail_resend_cancelDTO> create(@Validated @RequestBody Mail_resend_cancelDTO mail_resend_canceldto) {
        Mail_resend_cancel domain = mail_resend_cancelMapping.toDomain(mail_resend_canceldto);
		mail_resend_cancelService.create(domain);
        Mail_resend_cancelDTO dto = mail_resend_cancelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_resend_cancelMapping.toDomain(#mail_resend_canceldtos),'iBizBusinessCentral-Mail_resend_cancel-Create')")
    @ApiOperation(value = "批量新建重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "批量新建重发请求被拒绝")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_resend_cancelDTO> mail_resend_canceldtos) {
        mail_resend_cancelService.createBatch(mail_resend_cancelMapping.toDomain(mail_resend_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_resend_cancel" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_resend_cancelService.get(#mail_resend_cancel_id),'iBizBusinessCentral-Mail_resend_cancel-Update')")
    @ApiOperation(value = "更新重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "更新重发请求被拒绝")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_cancels/{mail_resend_cancel_id}")
    public ResponseEntity<Mail_resend_cancelDTO> update(@PathVariable("mail_resend_cancel_id") Long mail_resend_cancel_id, @RequestBody Mail_resend_cancelDTO mail_resend_canceldto) {
		Mail_resend_cancel domain  = mail_resend_cancelMapping.toDomain(mail_resend_canceldto);
        domain .setId(mail_resend_cancel_id);
		mail_resend_cancelService.update(domain );
		Mail_resend_cancelDTO dto = mail_resend_cancelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_resend_cancelService.getMailResendCancelByEntities(this.mail_resend_cancelMapping.toDomain(#mail_resend_canceldtos)),'iBizBusinessCentral-Mail_resend_cancel-Update')")
    @ApiOperation(value = "批量更新重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "批量更新重发请求被拒绝")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_cancels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_resend_cancelDTO> mail_resend_canceldtos) {
        mail_resend_cancelService.updateBatch(mail_resend_cancelMapping.toDomain(mail_resend_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_resend_cancelService.get(#mail_resend_cancel_id),'iBizBusinessCentral-Mail_resend_cancel-Remove')")
    @ApiOperation(value = "删除重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "删除重发请求被拒绝")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_cancels/{mail_resend_cancel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_resend_cancel_id") Long mail_resend_cancel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_resend_cancelService.remove(mail_resend_cancel_id));
    }

    @PreAuthorize("hasPermission(this.mail_resend_cancelService.getMailResendCancelByIds(#ids),'iBizBusinessCentral-Mail_resend_cancel-Remove')")
    @ApiOperation(value = "批量删除重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "批量删除重发请求被拒绝")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_cancels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_resend_cancelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_resend_cancelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_resend_cancel-Get')")
    @ApiOperation(value = "获取重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "获取重发请求被拒绝")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_resend_cancels/{mail_resend_cancel_id}")
    public ResponseEntity<Mail_resend_cancelDTO> get(@PathVariable("mail_resend_cancel_id") Long mail_resend_cancel_id) {
        Mail_resend_cancel domain = mail_resend_cancelService.get(mail_resend_cancel_id);
        Mail_resend_cancelDTO dto = mail_resend_cancelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取重发请求被拒绝草稿", tags = {"重发请求被拒绝" },  notes = "获取重发请求被拒绝草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_resend_cancels/getdraft")
    public ResponseEntity<Mail_resend_cancelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_resend_cancelMapping.toDto(mail_resend_cancelService.getDraft(new Mail_resend_cancel())));
    }

    @ApiOperation(value = "检查重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "检查重发请求被拒绝")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_resend_cancelDTO mail_resend_canceldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_resend_cancelService.checkKey(mail_resend_cancelMapping.toDomain(mail_resend_canceldto)));
    }

    @PreAuthorize("hasPermission(this.mail_resend_cancelMapping.toDomain(#mail_resend_canceldto),'iBizBusinessCentral-Mail_resend_cancel-Save')")
    @ApiOperation(value = "保存重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "保存重发请求被拒绝")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_resend_cancelDTO mail_resend_canceldto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_resend_cancelService.save(mail_resend_cancelMapping.toDomain(mail_resend_canceldto)));
    }

    @PreAuthorize("hasPermission(this.mail_resend_cancelMapping.toDomain(#mail_resend_canceldtos),'iBizBusinessCentral-Mail_resend_cancel-Save')")
    @ApiOperation(value = "批量保存重发请求被拒绝", tags = {"重发请求被拒绝" },  notes = "批量保存重发请求被拒绝")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_resend_cancelDTO> mail_resend_canceldtos) {
        mail_resend_cancelService.saveBatch(mail_resend_cancelMapping.toDomain(mail_resend_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_resend_cancel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_resend_cancel-Get')")
	@ApiOperation(value = "获取数据集", tags = {"重发请求被拒绝" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_resend_cancels/fetchdefault")
	public ResponseEntity<List<Mail_resend_cancelDTO>> fetchDefault(Mail_resend_cancelSearchContext context) {
        Page<Mail_resend_cancel> domains = mail_resend_cancelService.searchDefault(context) ;
        List<Mail_resend_cancelDTO> list = mail_resend_cancelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_resend_cancel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_resend_cancel-Get')")
	@ApiOperation(value = "查询数据集", tags = {"重发请求被拒绝" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_resend_cancels/searchdefault")
	public ResponseEntity<Page<Mail_resend_cancelDTO>> searchDefault(@RequestBody Mail_resend_cancelSearchContext context) {
        Page<Mail_resend_cancel> domains = mail_resend_cancelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_resend_cancelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

