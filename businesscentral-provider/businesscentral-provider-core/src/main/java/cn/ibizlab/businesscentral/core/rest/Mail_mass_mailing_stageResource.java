package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_stage;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_stageService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_stageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"群发邮件营销阶段" })
@RestController("Core-mail_mass_mailing_stage")
@RequestMapping("")
public class Mail_mass_mailing_stageResource {

    @Autowired
    public IMail_mass_mailing_stageService mail_mass_mailing_stageService;

    @Autowired
    @Lazy
    public Mail_mass_mailing_stageMapping mail_mass_mailing_stageMapping;

    @PreAuthorize("hasPermission(this.mail_mass_mailing_stageMapping.toDomain(#mail_mass_mailing_stagedto),'iBizBusinessCentral-Mail_mass_mailing_stage-Create')")
    @ApiOperation(value = "新建群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "新建群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages")
    public ResponseEntity<Mail_mass_mailing_stageDTO> create(@Validated @RequestBody Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto) {
        Mail_mass_mailing_stage domain = mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedto);
		mail_mass_mailing_stageService.create(domain);
        Mail_mass_mailing_stageDTO dto = mail_mass_mailing_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_stageMapping.toDomain(#mail_mass_mailing_stagedtos),'iBizBusinessCentral-Mail_mass_mailing_stage-Create')")
    @ApiOperation(value = "批量新建群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "批量新建群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_stageDTO> mail_mass_mailing_stagedtos) {
        mail_mass_mailing_stageService.createBatch(mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mass_mailing_stage" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mass_mailing_stageService.get(#mail_mass_mailing_stage_id),'iBizBusinessCentral-Mail_mass_mailing_stage-Update')")
    @ApiOperation(value = "更新群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "更新群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")
    public ResponseEntity<Mail_mass_mailing_stageDTO> update(@PathVariable("mail_mass_mailing_stage_id") Long mail_mass_mailing_stage_id, @RequestBody Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto) {
		Mail_mass_mailing_stage domain  = mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedto);
        domain .setId(mail_mass_mailing_stage_id);
		mail_mass_mailing_stageService.update(domain );
		Mail_mass_mailing_stageDTO dto = mail_mass_mailing_stageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_stageService.getMailMassMailingStageByEntities(this.mail_mass_mailing_stageMapping.toDomain(#mail_mass_mailing_stagedtos)),'iBizBusinessCentral-Mail_mass_mailing_stage-Update')")
    @ApiOperation(value = "批量更新群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "批量更新群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_stageDTO> mail_mass_mailing_stagedtos) {
        mail_mass_mailing_stageService.updateBatch(mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_stageService.get(#mail_mass_mailing_stage_id),'iBizBusinessCentral-Mail_mass_mailing_stage-Remove')")
    @ApiOperation(value = "删除群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "删除群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_stage_id") Long mail_mass_mailing_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_stageService.remove(mail_mass_mailing_stage_id));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_stageService.getMailMassMailingStageByIds(#ids),'iBizBusinessCentral-Mail_mass_mailing_stage-Remove')")
    @ApiOperation(value = "批量删除群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "批量删除群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mass_mailing_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mass_mailing_stageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mass_mailing_stage-Get')")
    @ApiOperation(value = "获取群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "获取群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")
    public ResponseEntity<Mail_mass_mailing_stageDTO> get(@PathVariable("mail_mass_mailing_stage_id") Long mail_mass_mailing_stage_id) {
        Mail_mass_mailing_stage domain = mail_mass_mailing_stageService.get(mail_mass_mailing_stage_id);
        Mail_mass_mailing_stageDTO dto = mail_mass_mailing_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取群发邮件营销阶段草稿", tags = {"群发邮件营销阶段" },  notes = "获取群发邮件营销阶段草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_stages/getdraft")
    public ResponseEntity<Mail_mass_mailing_stageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_stageMapping.toDto(mail_mass_mailing_stageService.getDraft(new Mail_mass_mailing_stage())));
    }

    @ApiOperation(value = "检查群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "检查群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_stageService.checkKey(mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_stageMapping.toDomain(#mail_mass_mailing_stagedto),'iBizBusinessCentral-Mail_mass_mailing_stage-Save')")
    @ApiOperation(value = "保存群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "保存群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_stageService.save(mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_stageMapping.toDomain(#mail_mass_mailing_stagedtos),'iBizBusinessCentral-Mail_mass_mailing_stage-Save')")
    @ApiOperation(value = "批量保存群发邮件营销阶段", tags = {"群发邮件营销阶段" },  notes = "批量保存群发邮件营销阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mass_mailing_stageDTO> mail_mass_mailing_stagedtos) {
        mail_mass_mailing_stageService.saveBatch(mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_stage-Get')")
	@ApiOperation(value = "获取数据集", tags = {"群发邮件营销阶段" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_stages/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_stageDTO>> fetchDefault(Mail_mass_mailing_stageSearchContext context) {
        Page<Mail_mass_mailing_stage> domains = mail_mass_mailing_stageService.searchDefault(context) ;
        List<Mail_mass_mailing_stageDTO> list = mail_mass_mailing_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_stage-Get')")
	@ApiOperation(value = "查询数据集", tags = {"群发邮件营销阶段" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mass_mailing_stages/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_stageDTO>> searchDefault(@RequestBody Mail_mass_mailing_stageSearchContext context) {
        Page<Mail_mass_mailing_stage> domains = mail_mass_mailing_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

