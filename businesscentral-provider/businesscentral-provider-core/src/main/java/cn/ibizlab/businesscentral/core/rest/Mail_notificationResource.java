package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_notificationService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_notificationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"消息通知" })
@RestController("Core-mail_notification")
@RequestMapping("")
public class Mail_notificationResource {

    @Autowired
    public IMail_notificationService mail_notificationService;

    @Autowired
    @Lazy
    public Mail_notificationMapping mail_notificationMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Create-all')")
    @ApiOperation(value = "新建消息通知", tags = {"消息通知" },  notes = "新建消息通知")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_notifications")
    public ResponseEntity<Mail_notificationDTO> create(@Validated @RequestBody Mail_notificationDTO mail_notificationdto) {
        Mail_notification domain = mail_notificationMapping.toDomain(mail_notificationdto);
		mail_notificationService.create(domain);
        Mail_notificationDTO dto = mail_notificationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Create-all')")
    @ApiOperation(value = "批量新建消息通知", tags = {"消息通知" },  notes = "批量新建消息通知")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_notificationDTO> mail_notificationdtos) {
        mail_notificationService.createBatch(mail_notificationMapping.toDomain(mail_notificationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Update-all')")
    @ApiOperation(value = "更新消息通知", tags = {"消息通知" },  notes = "更新消息通知")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_notifications/{mail_notification_id}")
    public ResponseEntity<Mail_notificationDTO> update(@PathVariable("mail_notification_id") Long mail_notification_id, @RequestBody Mail_notificationDTO mail_notificationdto) {
		Mail_notification domain  = mail_notificationMapping.toDomain(mail_notificationdto);
        domain .setId(mail_notification_id);
		mail_notificationService.update(domain );
		Mail_notificationDTO dto = mail_notificationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Update-all')")
    @ApiOperation(value = "批量更新消息通知", tags = {"消息通知" },  notes = "批量更新消息通知")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_notifications/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_notificationDTO> mail_notificationdtos) {
        mail_notificationService.updateBatch(mail_notificationMapping.toDomain(mail_notificationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Remove-all')")
    @ApiOperation(value = "删除消息通知", tags = {"消息通知" },  notes = "删除消息通知")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_notifications/{mail_notification_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_notification_id") Long mail_notification_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_notificationService.remove(mail_notification_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Remove-all')")
    @ApiOperation(value = "批量删除消息通知", tags = {"消息通知" },  notes = "批量删除消息通知")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_notifications/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_notificationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Get-all')")
    @ApiOperation(value = "获取消息通知", tags = {"消息通知" },  notes = "获取消息通知")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_notifications/{mail_notification_id}")
    public ResponseEntity<Mail_notificationDTO> get(@PathVariable("mail_notification_id") Long mail_notification_id) {
        Mail_notification domain = mail_notificationService.get(mail_notification_id);
        Mail_notificationDTO dto = mail_notificationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取消息通知草稿", tags = {"消息通知" },  notes = "获取消息通知草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_notifications/getdraft")
    public ResponseEntity<Mail_notificationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_notificationMapping.toDto(mail_notificationService.getDraft(new Mail_notification())));
    }

    @ApiOperation(value = "检查消息通知", tags = {"消息通知" },  notes = "检查消息通知")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_notificationDTO mail_notificationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_notificationService.checkKey(mail_notificationMapping.toDomain(mail_notificationdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Save-all')")
    @ApiOperation(value = "保存消息通知", tags = {"消息通知" },  notes = "保存消息通知")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_notificationDTO mail_notificationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_notificationService.save(mail_notificationMapping.toDomain(mail_notificationdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-Save-all')")
    @ApiOperation(value = "批量保存消息通知", tags = {"消息通知" },  notes = "批量保存消息通知")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_notificationDTO> mail_notificationdtos) {
        mail_notificationService.saveBatch(mail_notificationMapping.toDomain(mail_notificationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"消息通知" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_notifications/fetchdefault")
	public ResponseEntity<List<Mail_notificationDTO>> fetchDefault(Mail_notificationSearchContext context) {
        Page<Mail_notification> domains = mail_notificationService.searchDefault(context) ;
        List<Mail_notificationDTO> list = mail_notificationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_notification-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"消息通知" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_notifications/searchdefault")
	public ResponseEntity<Page<Mail_notificationDTO>> searchDefault(@RequestBody Mail_notificationSearchContext context) {
        Page<Mail_notification> domains = mail_notificationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_notificationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

