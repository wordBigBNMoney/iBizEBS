package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_groups;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_groupsService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_groupsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"访问群" })
@RestController("Core-res_groups")
@RequestMapping("")
public class Res_groupsResource {

    @Autowired
    public IRes_groupsService res_groupsService;

    @Autowired
    @Lazy
    public Res_groupsMapping res_groupsMapping;

    @PreAuthorize("hasPermission(this.res_groupsMapping.toDomain(#res_groupsdto),'iBizBusinessCentral-Res_groups-Create')")
    @ApiOperation(value = "新建访问群", tags = {"访问群" },  notes = "新建访问群")
	@RequestMapping(method = RequestMethod.POST, value = "/res_groups")
    public ResponseEntity<Res_groupsDTO> create(@Validated @RequestBody Res_groupsDTO res_groupsdto) {
        Res_groups domain = res_groupsMapping.toDomain(res_groupsdto);
		res_groupsService.create(domain);
        Res_groupsDTO dto = res_groupsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_groupsMapping.toDomain(#res_groupsdtos),'iBizBusinessCentral-Res_groups-Create')")
    @ApiOperation(value = "批量新建访问群", tags = {"访问群" },  notes = "批量新建访问群")
	@RequestMapping(method = RequestMethod.POST, value = "/res_groups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_groupsDTO> res_groupsdtos) {
        res_groupsService.createBatch(res_groupsMapping.toDomain(res_groupsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_groups" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_groupsService.get(#res_groups_id),'iBizBusinessCentral-Res_groups-Update')")
    @ApiOperation(value = "更新访问群", tags = {"访问群" },  notes = "更新访问群")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_groups/{res_groups_id}")
    public ResponseEntity<Res_groupsDTO> update(@PathVariable("res_groups_id") Long res_groups_id, @RequestBody Res_groupsDTO res_groupsdto) {
		Res_groups domain  = res_groupsMapping.toDomain(res_groupsdto);
        domain .setId(res_groups_id);
		res_groupsService.update(domain );
		Res_groupsDTO dto = res_groupsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_groupsService.getResGroupsByEntities(this.res_groupsMapping.toDomain(#res_groupsdtos)),'iBizBusinessCentral-Res_groups-Update')")
    @ApiOperation(value = "批量更新访问群", tags = {"访问群" },  notes = "批量更新访问群")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_groups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_groupsDTO> res_groupsdtos) {
        res_groupsService.updateBatch(res_groupsMapping.toDomain(res_groupsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_groupsService.get(#res_groups_id),'iBizBusinessCentral-Res_groups-Remove')")
    @ApiOperation(value = "删除访问群", tags = {"访问群" },  notes = "删除访问群")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_groups/{res_groups_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_groups_id") Long res_groups_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_groupsService.remove(res_groups_id));
    }

    @PreAuthorize("hasPermission(this.res_groupsService.getResGroupsByIds(#ids),'iBizBusinessCentral-Res_groups-Remove')")
    @ApiOperation(value = "批量删除访问群", tags = {"访问群" },  notes = "批量删除访问群")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_groups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_groupsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_groupsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_groups-Get')")
    @ApiOperation(value = "获取访问群", tags = {"访问群" },  notes = "获取访问群")
	@RequestMapping(method = RequestMethod.GET, value = "/res_groups/{res_groups_id}")
    public ResponseEntity<Res_groupsDTO> get(@PathVariable("res_groups_id") Long res_groups_id) {
        Res_groups domain = res_groupsService.get(res_groups_id);
        Res_groupsDTO dto = res_groupsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取访问群草稿", tags = {"访问群" },  notes = "获取访问群草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_groups/getdraft")
    public ResponseEntity<Res_groupsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_groupsMapping.toDto(res_groupsService.getDraft(new Res_groups())));
    }

    @ApiOperation(value = "检查访问群", tags = {"访问群" },  notes = "检查访问群")
	@RequestMapping(method = RequestMethod.POST, value = "/res_groups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_groupsDTO res_groupsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_groupsService.checkKey(res_groupsMapping.toDomain(res_groupsdto)));
    }

    @PreAuthorize("hasPermission(this.res_groupsMapping.toDomain(#res_groupsdto),'iBizBusinessCentral-Res_groups-Save')")
    @ApiOperation(value = "保存访问群", tags = {"访问群" },  notes = "保存访问群")
	@RequestMapping(method = RequestMethod.POST, value = "/res_groups/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_groupsDTO res_groupsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_groupsService.save(res_groupsMapping.toDomain(res_groupsdto)));
    }

    @PreAuthorize("hasPermission(this.res_groupsMapping.toDomain(#res_groupsdtos),'iBizBusinessCentral-Res_groups-Save')")
    @ApiOperation(value = "批量保存访问群", tags = {"访问群" },  notes = "批量保存访问群")
	@RequestMapping(method = RequestMethod.POST, value = "/res_groups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_groupsDTO> res_groupsdtos) {
        res_groupsService.saveBatch(res_groupsMapping.toDomain(res_groupsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_groups-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_groups-Get')")
	@ApiOperation(value = "获取数据集", tags = {"访问群" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_groups/fetchdefault")
	public ResponseEntity<List<Res_groupsDTO>> fetchDefault(Res_groupsSearchContext context) {
        Page<Res_groups> domains = res_groupsService.searchDefault(context) ;
        List<Res_groupsDTO> list = res_groupsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_groups-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_groups-Get')")
	@ApiOperation(value = "查询数据集", tags = {"访问群" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_groups/searchdefault")
	public ResponseEntity<Page<Res_groupsDTO>> searchDefault(@RequestBody Res_groupsSearchContext context) {
        Page<Res_groups> domains = res_groupsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_groupsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

