package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_parameter;
import cn.ibizlab.businesscentral.core.dto.Mro_pm_parameterDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMro_pm_parameterMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mro_pm_parameterMapping extends MappingBase<Mro_pm_parameterDTO, Mro_pm_parameter> {


}

