package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_ruleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存规则" })
@RestController("Core-stock_rule")
@RequestMapping("")
public class Stock_ruleResource {

    @Autowired
    public IStock_ruleService stock_ruleService;

    @Autowired
    @Lazy
    public Stock_ruleMapping stock_ruleMapping;

    @PreAuthorize("hasPermission(this.stock_ruleMapping.toDomain(#stock_ruledto),'iBizBusinessCentral-Stock_rule-Create')")
    @ApiOperation(value = "新建库存规则", tags = {"库存规则" },  notes = "新建库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules")
    public ResponseEntity<Stock_ruleDTO> create(@Validated @RequestBody Stock_ruleDTO stock_ruledto) {
        Stock_rule domain = stock_ruleMapping.toDomain(stock_ruledto);
		stock_ruleService.create(domain);
        Stock_ruleDTO dto = stock_ruleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_ruleMapping.toDomain(#stock_ruledtos),'iBizBusinessCentral-Stock_rule-Create')")
    @ApiOperation(value = "批量新建库存规则", tags = {"库存规则" },  notes = "批量新建库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_ruleDTO> stock_ruledtos) {
        stock_ruleService.createBatch(stock_ruleMapping.toDomain(stock_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_rule" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_ruleService.get(#stock_rule_id),'iBizBusinessCentral-Stock_rule-Update')")
    @ApiOperation(value = "更新库存规则", tags = {"库存规则" },  notes = "更新库存规则")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_rules/{stock_rule_id}")
    public ResponseEntity<Stock_ruleDTO> update(@PathVariable("stock_rule_id") Long stock_rule_id, @RequestBody Stock_ruleDTO stock_ruledto) {
		Stock_rule domain  = stock_ruleMapping.toDomain(stock_ruledto);
        domain .setId(stock_rule_id);
		stock_ruleService.update(domain );
		Stock_ruleDTO dto = stock_ruleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_ruleService.getStockRuleByEntities(this.stock_ruleMapping.toDomain(#stock_ruledtos)),'iBizBusinessCentral-Stock_rule-Update')")
    @ApiOperation(value = "批量更新库存规则", tags = {"库存规则" },  notes = "批量更新库存规则")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_rules/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_ruleDTO> stock_ruledtos) {
        stock_ruleService.updateBatch(stock_ruleMapping.toDomain(stock_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_ruleService.get(#stock_rule_id),'iBizBusinessCentral-Stock_rule-Remove')")
    @ApiOperation(value = "删除库存规则", tags = {"库存规则" },  notes = "删除库存规则")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules/{stock_rule_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_rule_id") Long stock_rule_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_ruleService.remove(stock_rule_id));
    }

    @PreAuthorize("hasPermission(this.stock_ruleService.getStockRuleByIds(#ids),'iBizBusinessCentral-Stock_rule-Remove')")
    @ApiOperation(value = "批量删除库存规则", tags = {"库存规则" },  notes = "批量删除库存规则")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_ruleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_ruleMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_rule-Get')")
    @ApiOperation(value = "获取库存规则", tags = {"库存规则" },  notes = "获取库存规则")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_rules/{stock_rule_id}")
    public ResponseEntity<Stock_ruleDTO> get(@PathVariable("stock_rule_id") Long stock_rule_id) {
        Stock_rule domain = stock_ruleService.get(stock_rule_id);
        Stock_ruleDTO dto = stock_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存规则草稿", tags = {"库存规则" },  notes = "获取库存规则草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_rules/getdraft")
    public ResponseEntity<Stock_ruleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_ruleMapping.toDto(stock_ruleService.getDraft(new Stock_rule())));
    }

    @ApiOperation(value = "检查库存规则", tags = {"库存规则" },  notes = "检查库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_ruleDTO stock_ruledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_ruleService.checkKey(stock_ruleMapping.toDomain(stock_ruledto)));
    }

    @PreAuthorize("hasPermission(this.stock_ruleMapping.toDomain(#stock_ruledto),'iBizBusinessCentral-Stock_rule-Save')")
    @ApiOperation(value = "保存库存规则", tags = {"库存规则" },  notes = "保存库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_ruleDTO stock_ruledto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_ruleService.save(stock_ruleMapping.toDomain(stock_ruledto)));
    }

    @PreAuthorize("hasPermission(this.stock_ruleMapping.toDomain(#stock_ruledtos),'iBizBusinessCentral-Stock_rule-Save')")
    @ApiOperation(value = "批量保存库存规则", tags = {"库存规则" },  notes = "批量保存库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_ruleDTO> stock_ruledtos) {
        stock_ruleService.saveBatch(stock_ruleMapping.toDomain(stock_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_rule-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_rule-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存规则" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_rules/fetchdefault")
	public ResponseEntity<List<Stock_ruleDTO>> fetchDefault(Stock_ruleSearchContext context) {
        Page<Stock_rule> domains = stock_ruleService.searchDefault(context) ;
        List<Stock_ruleDTO> list = stock_ruleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_rule-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_rule-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存规则" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_rules/searchdefault")
	public ResponseEntity<Page<Stock_ruleDTO>> searchDefault(@RequestBody Stock_ruleSearchContext context) {
        Page<Stock_rule> domains = stock_ruleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_ruleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

