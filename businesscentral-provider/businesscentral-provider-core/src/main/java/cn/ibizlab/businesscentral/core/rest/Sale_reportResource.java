package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_reportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售分析报告" })
@RestController("Core-sale_report")
@RequestMapping("")
public class Sale_reportResource {

    @Autowired
    public ISale_reportService sale_reportService;

    @Autowired
    @Lazy
    public Sale_reportMapping sale_reportMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Create-all')")
    @ApiOperation(value = "新建销售分析报告", tags = {"销售分析报告" },  notes = "新建销售分析报告")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_reports")
    public ResponseEntity<Sale_reportDTO> create(@Validated @RequestBody Sale_reportDTO sale_reportdto) {
        Sale_report domain = sale_reportMapping.toDomain(sale_reportdto);
		sale_reportService.create(domain);
        Sale_reportDTO dto = sale_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Create-all')")
    @ApiOperation(value = "批量新建销售分析报告", tags = {"销售分析报告" },  notes = "批量新建销售分析报告")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_reportDTO> sale_reportdtos) {
        sale_reportService.createBatch(sale_reportMapping.toDomain(sale_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Update-all')")
    @ApiOperation(value = "更新销售分析报告", tags = {"销售分析报告" },  notes = "更新销售分析报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_reports/{sale_report_id}")
    public ResponseEntity<Sale_reportDTO> update(@PathVariable("sale_report_id") Long sale_report_id, @RequestBody Sale_reportDTO sale_reportdto) {
		Sale_report domain  = sale_reportMapping.toDomain(sale_reportdto);
        domain .setId(sale_report_id);
		sale_reportService.update(domain );
		Sale_reportDTO dto = sale_reportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Update-all')")
    @ApiOperation(value = "批量更新销售分析报告", tags = {"销售分析报告" },  notes = "批量更新销售分析报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_reportDTO> sale_reportdtos) {
        sale_reportService.updateBatch(sale_reportMapping.toDomain(sale_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Remove-all')")
    @ApiOperation(value = "删除销售分析报告", tags = {"销售分析报告" },  notes = "删除销售分析报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_reports/{sale_report_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_report_id") Long sale_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_reportService.remove(sale_report_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Remove-all')")
    @ApiOperation(value = "批量删除销售分析报告", tags = {"销售分析报告" },  notes = "批量删除销售分析报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Get-all')")
    @ApiOperation(value = "获取销售分析报告", tags = {"销售分析报告" },  notes = "获取销售分析报告")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_reports/{sale_report_id}")
    public ResponseEntity<Sale_reportDTO> get(@PathVariable("sale_report_id") Long sale_report_id) {
        Sale_report domain = sale_reportService.get(sale_report_id);
        Sale_reportDTO dto = sale_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售分析报告草稿", tags = {"销售分析报告" },  notes = "获取销售分析报告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_reports/getdraft")
    public ResponseEntity<Sale_reportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_reportMapping.toDto(sale_reportService.getDraft(new Sale_report())));
    }

    @ApiOperation(value = "检查销售分析报告", tags = {"销售分析报告" },  notes = "检查销售分析报告")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_reports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_reportDTO sale_reportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_reportService.checkKey(sale_reportMapping.toDomain(sale_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Save-all')")
    @ApiOperation(value = "保存销售分析报告", tags = {"销售分析报告" },  notes = "保存销售分析报告")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_reports/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_reportDTO sale_reportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_reportService.save(sale_reportMapping.toDomain(sale_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-Save-all')")
    @ApiOperation(value = "批量保存销售分析报告", tags = {"销售分析报告" },  notes = "批量保存销售分析报告")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_reports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_reportDTO> sale_reportdtos) {
        sale_reportService.saveBatch(sale_reportMapping.toDomain(sale_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"销售分析报告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_reports/fetchdefault")
	public ResponseEntity<List<Sale_reportDTO>> fetchDefault(Sale_reportSearchContext context) {
        Page<Sale_report> domains = sale_reportService.searchDefault(context) ;
        List<Sale_reportDTO> list = sale_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_report-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"销售分析报告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_reports/searchdefault")
	public ResponseEntity<Page<Sale_reportDTO>> searchDefault(@RequestBody Sale_reportSearchContext context) {
        Page<Sale_report> domains = sale_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

