package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant_category;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicant_categoryService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_applicant_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"申请人类别" })
@RestController("Core-hr_applicant_category")
@RequestMapping("")
public class Hr_applicant_categoryResource {

    @Autowired
    public IHr_applicant_categoryService hr_applicant_categoryService;

    @Autowired
    @Lazy
    public Hr_applicant_categoryMapping hr_applicant_categoryMapping;

    @PreAuthorize("hasPermission(this.hr_applicant_categoryMapping.toDomain(#hr_applicant_categorydto),'iBizBusinessCentral-Hr_applicant_category-Create')")
    @ApiOperation(value = "新建申请人类别", tags = {"申请人类别" },  notes = "新建申请人类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories")
    public ResponseEntity<Hr_applicant_categoryDTO> create(@Validated @RequestBody Hr_applicant_categoryDTO hr_applicant_categorydto) {
        Hr_applicant_category domain = hr_applicant_categoryMapping.toDomain(hr_applicant_categorydto);
		hr_applicant_categoryService.create(domain);
        Hr_applicant_categoryDTO dto = hr_applicant_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_applicant_categoryMapping.toDomain(#hr_applicant_categorydtos),'iBizBusinessCentral-Hr_applicant_category-Create')")
    @ApiOperation(value = "批量新建申请人类别", tags = {"申请人类别" },  notes = "批量新建申请人类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_applicant_categoryDTO> hr_applicant_categorydtos) {
        hr_applicant_categoryService.createBatch(hr_applicant_categoryMapping.toDomain(hr_applicant_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_applicant_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_applicant_categoryService.get(#hr_applicant_category_id),'iBizBusinessCentral-Hr_applicant_category-Update')")
    @ApiOperation(value = "更新申请人类别", tags = {"申请人类别" },  notes = "更新申请人类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_applicant_categories/{hr_applicant_category_id}")
    public ResponseEntity<Hr_applicant_categoryDTO> update(@PathVariable("hr_applicant_category_id") Long hr_applicant_category_id, @RequestBody Hr_applicant_categoryDTO hr_applicant_categorydto) {
		Hr_applicant_category domain  = hr_applicant_categoryMapping.toDomain(hr_applicant_categorydto);
        domain .setId(hr_applicant_category_id);
		hr_applicant_categoryService.update(domain );
		Hr_applicant_categoryDTO dto = hr_applicant_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_applicant_categoryService.getHrApplicantCategoryByEntities(this.hr_applicant_categoryMapping.toDomain(#hr_applicant_categorydtos)),'iBizBusinessCentral-Hr_applicant_category-Update')")
    @ApiOperation(value = "批量更新申请人类别", tags = {"申请人类别" },  notes = "批量更新申请人类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_applicant_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_applicant_categoryDTO> hr_applicant_categorydtos) {
        hr_applicant_categoryService.updateBatch(hr_applicant_categoryMapping.toDomain(hr_applicant_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_applicant_categoryService.get(#hr_applicant_category_id),'iBizBusinessCentral-Hr_applicant_category-Remove')")
    @ApiOperation(value = "删除申请人类别", tags = {"申请人类别" },  notes = "删除申请人类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicant_categories/{hr_applicant_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_applicant_category_id") Long hr_applicant_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_applicant_categoryService.remove(hr_applicant_category_id));
    }

    @PreAuthorize("hasPermission(this.hr_applicant_categoryService.getHrApplicantCategoryByIds(#ids),'iBizBusinessCentral-Hr_applicant_category-Remove')")
    @ApiOperation(value = "批量删除申请人类别", tags = {"申请人类别" },  notes = "批量删除申请人类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicant_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_applicant_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_applicant_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_applicant_category-Get')")
    @ApiOperation(value = "获取申请人类别", tags = {"申请人类别" },  notes = "获取申请人类别")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_applicant_categories/{hr_applicant_category_id}")
    public ResponseEntity<Hr_applicant_categoryDTO> get(@PathVariable("hr_applicant_category_id") Long hr_applicant_category_id) {
        Hr_applicant_category domain = hr_applicant_categoryService.get(hr_applicant_category_id);
        Hr_applicant_categoryDTO dto = hr_applicant_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取申请人类别草稿", tags = {"申请人类别" },  notes = "获取申请人类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_applicant_categories/getdraft")
    public ResponseEntity<Hr_applicant_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_applicant_categoryMapping.toDto(hr_applicant_categoryService.getDraft(new Hr_applicant_category())));
    }

    @ApiOperation(value = "检查申请人类别", tags = {"申请人类别" },  notes = "检查申请人类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_applicant_categoryDTO hr_applicant_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_applicant_categoryService.checkKey(hr_applicant_categoryMapping.toDomain(hr_applicant_categorydto)));
    }

    @PreAuthorize("hasPermission(this.hr_applicant_categoryMapping.toDomain(#hr_applicant_categorydto),'iBizBusinessCentral-Hr_applicant_category-Save')")
    @ApiOperation(value = "保存申请人类别", tags = {"申请人类别" },  notes = "保存申请人类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_applicant_categoryDTO hr_applicant_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_applicant_categoryService.save(hr_applicant_categoryMapping.toDomain(hr_applicant_categorydto)));
    }

    @PreAuthorize("hasPermission(this.hr_applicant_categoryMapping.toDomain(#hr_applicant_categorydtos),'iBizBusinessCentral-Hr_applicant_category-Save')")
    @ApiOperation(value = "批量保存申请人类别", tags = {"申请人类别" },  notes = "批量保存申请人类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_applicant_categoryDTO> hr_applicant_categorydtos) {
        hr_applicant_categoryService.saveBatch(hr_applicant_categoryMapping.toDomain(hr_applicant_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_applicant_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_applicant_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"申请人类别" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_applicant_categories/fetchdefault")
	public ResponseEntity<List<Hr_applicant_categoryDTO>> fetchDefault(Hr_applicant_categorySearchContext context) {
        Page<Hr_applicant_category> domains = hr_applicant_categoryService.searchDefault(context) ;
        List<Hr_applicant_categoryDTO> list = hr_applicant_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_applicant_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_applicant_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"申请人类别" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_applicant_categories/searchdefault")
	public ResponseEntity<Page<Hr_applicant_categoryDTO>> searchDefault(@RequestBody Hr_applicant_categorySearchContext context) {
        Page<Hr_applicant_category> domains = hr_applicant_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_applicant_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

