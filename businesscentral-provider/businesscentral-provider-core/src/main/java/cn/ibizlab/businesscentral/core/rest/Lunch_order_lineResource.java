package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_order_lineService;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_order_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作餐订单行" })
@RestController("Core-lunch_order_line")
@RequestMapping("")
public class Lunch_order_lineResource {

    @Autowired
    public ILunch_order_lineService lunch_order_lineService;

    @Autowired
    @Lazy
    public Lunch_order_lineMapping lunch_order_lineMapping;

    @PreAuthorize("hasPermission(this.lunch_order_lineMapping.toDomain(#lunch_order_linedto),'iBizBusinessCentral-Lunch_order_line-Create')")
    @ApiOperation(value = "新建工作餐订单行", tags = {"工作餐订单行" },  notes = "新建工作餐订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines")
    public ResponseEntity<Lunch_order_lineDTO> create(@Validated @RequestBody Lunch_order_lineDTO lunch_order_linedto) {
        Lunch_order_line domain = lunch_order_lineMapping.toDomain(lunch_order_linedto);
		lunch_order_lineService.create(domain);
        Lunch_order_lineDTO dto = lunch_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_order_lineMapping.toDomain(#lunch_order_linedtos),'iBizBusinessCentral-Lunch_order_line-Create')")
    @ApiOperation(value = "批量新建工作餐订单行", tags = {"工作餐订单行" },  notes = "批量新建工作餐订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_order_lineDTO> lunch_order_linedtos) {
        lunch_order_lineService.createBatch(lunch_order_lineMapping.toDomain(lunch_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lunch_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.lunch_order_lineService.get(#lunch_order_line_id),'iBizBusinessCentral-Lunch_order_line-Update')")
    @ApiOperation(value = "更新工作餐订单行", tags = {"工作餐订单行" },  notes = "更新工作餐订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_lines/{lunch_order_line_id}")
    public ResponseEntity<Lunch_order_lineDTO> update(@PathVariable("lunch_order_line_id") Long lunch_order_line_id, @RequestBody Lunch_order_lineDTO lunch_order_linedto) {
		Lunch_order_line domain  = lunch_order_lineMapping.toDomain(lunch_order_linedto);
        domain .setId(lunch_order_line_id);
		lunch_order_lineService.update(domain );
		Lunch_order_lineDTO dto = lunch_order_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_order_lineService.getLunchOrderLineByEntities(this.lunch_order_lineMapping.toDomain(#lunch_order_linedtos)),'iBizBusinessCentral-Lunch_order_line-Update')")
    @ApiOperation(value = "批量更新工作餐订单行", tags = {"工作餐订单行" },  notes = "批量更新工作餐订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_order_lineDTO> lunch_order_linedtos) {
        lunch_order_lineService.updateBatch(lunch_order_lineMapping.toDomain(lunch_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.lunch_order_lineService.get(#lunch_order_line_id),'iBizBusinessCentral-Lunch_order_line-Remove')")
    @ApiOperation(value = "删除工作餐订单行", tags = {"工作餐订单行" },  notes = "删除工作餐订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_lines/{lunch_order_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_line_id") Long lunch_order_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_order_lineService.remove(lunch_order_line_id));
    }

    @PreAuthorize("hasPermission(this.lunch_order_lineService.getLunchOrderLineByIds(#ids),'iBizBusinessCentral-Lunch_order_line-Remove')")
    @ApiOperation(value = "批量删除工作餐订单行", tags = {"工作餐订单行" },  notes = "批量删除工作餐订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        lunch_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.lunch_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lunch_order_line-Get')")
    @ApiOperation(value = "获取工作餐订单行", tags = {"工作餐订单行" },  notes = "获取工作餐订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_order_lines/{lunch_order_line_id}")
    public ResponseEntity<Lunch_order_lineDTO> get(@PathVariable("lunch_order_line_id") Long lunch_order_line_id) {
        Lunch_order_line domain = lunch_order_lineService.get(lunch_order_line_id);
        Lunch_order_lineDTO dto = lunch_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作餐订单行草稿", tags = {"工作餐订单行" },  notes = "获取工作餐订单行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_order_lines/getdraft")
    public ResponseEntity<Lunch_order_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_order_lineMapping.toDto(lunch_order_lineService.getDraft(new Lunch_order_line())));
    }

    @ApiOperation(value = "检查工作餐订单行", tags = {"工作餐订单行" },  notes = "检查工作餐订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Lunch_order_lineDTO lunch_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(lunch_order_lineService.checkKey(lunch_order_lineMapping.toDomain(lunch_order_linedto)));
    }

    @PreAuthorize("hasPermission(this.lunch_order_lineMapping.toDomain(#lunch_order_linedto),'iBizBusinessCentral-Lunch_order_line-Save')")
    @ApiOperation(value = "保存工作餐订单行", tags = {"工作餐订单行" },  notes = "保存工作餐订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Lunch_order_lineDTO lunch_order_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_order_lineService.save(lunch_order_lineMapping.toDomain(lunch_order_linedto)));
    }

    @PreAuthorize("hasPermission(this.lunch_order_lineMapping.toDomain(#lunch_order_linedtos),'iBizBusinessCentral-Lunch_order_line-Save')")
    @ApiOperation(value = "批量保存工作餐订单行", tags = {"工作餐订单行" },  notes = "批量保存工作餐订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Lunch_order_lineDTO> lunch_order_linedtos) {
        lunch_order_lineService.saveBatch(lunch_order_lineMapping.toDomain(lunch_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_order_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作餐订单行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_order_lines/fetchdefault")
	public ResponseEntity<List<Lunch_order_lineDTO>> fetchDefault(Lunch_order_lineSearchContext context) {
        Page<Lunch_order_line> domains = lunch_order_lineService.searchDefault(context) ;
        List<Lunch_order_lineDTO> list = lunch_order_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_order_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作餐订单行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/lunch_order_lines/searchdefault")
	public ResponseEntity<Page<Lunch_order_lineDTO>> searchDefault(@RequestBody Lunch_order_lineSearchContext context) {
        Page<Lunch_order_line> domains = lunch_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

