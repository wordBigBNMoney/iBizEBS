package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.businesscentral.core.dto.Account_invoice_reportDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_invoice_reportMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_invoice_reportMapping extends MappingBase<Account_invoice_reportDTO, Account_invoice_report> {


}

