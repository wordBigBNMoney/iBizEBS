package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_odometerService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_odometerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆的里程表记录" })
@RestController("Core-fleet_vehicle_odometer")
@RequestMapping("")
public class Fleet_vehicle_odometerResource {

    @Autowired
    public IFleet_vehicle_odometerService fleet_vehicle_odometerService;

    @Autowired
    @Lazy
    public Fleet_vehicle_odometerMapping fleet_vehicle_odometerMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_odometerMapping.toDomain(#fleet_vehicle_odometerdto),'iBizBusinessCentral-Fleet_vehicle_odometer-Create')")
    @ApiOperation(value = "新建车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "新建车辆的里程表记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers")
    public ResponseEntity<Fleet_vehicle_odometerDTO> create(@Validated @RequestBody Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto) {
        Fleet_vehicle_odometer domain = fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdto);
		fleet_vehicle_odometerService.create(domain);
        Fleet_vehicle_odometerDTO dto = fleet_vehicle_odometerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_odometerMapping.toDomain(#fleet_vehicle_odometerdtos),'iBizBusinessCentral-Fleet_vehicle_odometer-Create')")
    @ApiOperation(value = "批量新建车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "批量新建车辆的里程表记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_odometerDTO> fleet_vehicle_odometerdtos) {
        fleet_vehicle_odometerService.createBatch(fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_odometer" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_odometerService.get(#fleet_vehicle_odometer_id),'iBizBusinessCentral-Fleet_vehicle_odometer-Update')")
    @ApiOperation(value = "更新车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "更新车辆的里程表记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")
    public ResponseEntity<Fleet_vehicle_odometerDTO> update(@PathVariable("fleet_vehicle_odometer_id") Long fleet_vehicle_odometer_id, @RequestBody Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto) {
		Fleet_vehicle_odometer domain  = fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdto);
        domain .setId(fleet_vehicle_odometer_id);
		fleet_vehicle_odometerService.update(domain );
		Fleet_vehicle_odometerDTO dto = fleet_vehicle_odometerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_odometerService.getFleetVehicleOdometerByEntities(this.fleet_vehicle_odometerMapping.toDomain(#fleet_vehicle_odometerdtos)),'iBizBusinessCentral-Fleet_vehicle_odometer-Update')")
    @ApiOperation(value = "批量更新车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "批量更新车辆的里程表记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_odometers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_odometerDTO> fleet_vehicle_odometerdtos) {
        fleet_vehicle_odometerService.updateBatch(fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_odometerService.get(#fleet_vehicle_odometer_id),'iBizBusinessCentral-Fleet_vehicle_odometer-Remove')")
    @ApiOperation(value = "删除车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "删除车辆的里程表记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_odometer_id") Long fleet_vehicle_odometer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_odometerService.remove(fleet_vehicle_odometer_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_odometerService.getFleetVehicleOdometerByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_odometer-Remove')")
    @ApiOperation(value = "批量删除车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "批量删除车辆的里程表记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_odometers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_odometerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_odometerMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_odometer-Get')")
    @ApiOperation(value = "获取车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "获取车辆的里程表记录")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")
    public ResponseEntity<Fleet_vehicle_odometerDTO> get(@PathVariable("fleet_vehicle_odometer_id") Long fleet_vehicle_odometer_id) {
        Fleet_vehicle_odometer domain = fleet_vehicle_odometerService.get(fleet_vehicle_odometer_id);
        Fleet_vehicle_odometerDTO dto = fleet_vehicle_odometerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆的里程表记录草稿", tags = {"车辆的里程表记录" },  notes = "获取车辆的里程表记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_odometers/getdraft")
    public ResponseEntity<Fleet_vehicle_odometerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_odometerMapping.toDto(fleet_vehicle_odometerService.getDraft(new Fleet_vehicle_odometer())));
    }

    @ApiOperation(value = "检查车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "检查车辆的里程表记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_odometerService.checkKey(fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_odometerMapping.toDomain(#fleet_vehicle_odometerdto),'iBizBusinessCentral-Fleet_vehicle_odometer-Save')")
    @ApiOperation(value = "保存车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "保存车辆的里程表记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_odometerService.save(fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_odometerMapping.toDomain(#fleet_vehicle_odometerdtos),'iBizBusinessCentral-Fleet_vehicle_odometer-Save')")
    @ApiOperation(value = "批量保存车辆的里程表记录", tags = {"车辆的里程表记录" },  notes = "批量保存车辆的里程表记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_odometerDTO> fleet_vehicle_odometerdtos) {
        fleet_vehicle_odometerService.saveBatch(fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_odometer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_odometer-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆的里程表记录" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_odometers/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_odometerDTO>> fetchDefault(Fleet_vehicle_odometerSearchContext context) {
        Page<Fleet_vehicle_odometer> domains = fleet_vehicle_odometerService.searchDefault(context) ;
        List<Fleet_vehicle_odometerDTO> list = fleet_vehicle_odometerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_odometer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_odometer-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆的里程表记录" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_odometers/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_odometerDTO>> searchDefault(@RequestBody Fleet_vehicle_odometerSearchContext context) {
        Page<Fleet_vehicle_odometer> domains = fleet_vehicle_odometerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_odometerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

