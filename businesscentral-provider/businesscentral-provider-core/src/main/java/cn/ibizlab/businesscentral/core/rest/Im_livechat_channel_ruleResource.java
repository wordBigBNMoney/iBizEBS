package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_channel_ruleService;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"实时聊天频道规则" })
@RestController("Core-im_livechat_channel_rule")
@RequestMapping("")
public class Im_livechat_channel_ruleResource {

    @Autowired
    public IIm_livechat_channel_ruleService im_livechat_channel_ruleService;

    @Autowired
    @Lazy
    public Im_livechat_channel_ruleMapping im_livechat_channel_ruleMapping;

    @PreAuthorize("hasPermission(this.im_livechat_channel_ruleMapping.toDomain(#im_livechat_channel_ruledto),'iBizBusinessCentral-Im_livechat_channel_rule-Create')")
    @ApiOperation(value = "新建实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "新建实时聊天频道规则")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules")
    public ResponseEntity<Im_livechat_channel_ruleDTO> create(@Validated @RequestBody Im_livechat_channel_ruleDTO im_livechat_channel_ruledto) {
        Im_livechat_channel_rule domain = im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledto);
		im_livechat_channel_ruleService.create(domain);
        Im_livechat_channel_ruleDTO dto = im_livechat_channel_ruleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.im_livechat_channel_ruleMapping.toDomain(#im_livechat_channel_ruledtos),'iBizBusinessCentral-Im_livechat_channel_rule-Create')")
    @ApiOperation(value = "批量新建实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "批量新建实时聊天频道规则")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Im_livechat_channel_ruleDTO> im_livechat_channel_ruledtos) {
        im_livechat_channel_ruleService.createBatch(im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "im_livechat_channel_rule" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.im_livechat_channel_ruleService.get(#im_livechat_channel_rule_id),'iBizBusinessCentral-Im_livechat_channel_rule-Update')")
    @ApiOperation(value = "更新实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "更新实时聊天频道规则")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channel_rules/{im_livechat_channel_rule_id}")
    public ResponseEntity<Im_livechat_channel_ruleDTO> update(@PathVariable("im_livechat_channel_rule_id") Long im_livechat_channel_rule_id, @RequestBody Im_livechat_channel_ruleDTO im_livechat_channel_ruledto) {
		Im_livechat_channel_rule domain  = im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledto);
        domain .setId(im_livechat_channel_rule_id);
		im_livechat_channel_ruleService.update(domain );
		Im_livechat_channel_ruleDTO dto = im_livechat_channel_ruleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.im_livechat_channel_ruleService.getImLivechatChannelRuleByEntities(this.im_livechat_channel_ruleMapping.toDomain(#im_livechat_channel_ruledtos)),'iBizBusinessCentral-Im_livechat_channel_rule-Update')")
    @ApiOperation(value = "批量更新实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "批量更新实时聊天频道规则")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channel_rules/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_channel_ruleDTO> im_livechat_channel_ruledtos) {
        im_livechat_channel_ruleService.updateBatch(im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.im_livechat_channel_ruleService.get(#im_livechat_channel_rule_id),'iBizBusinessCentral-Im_livechat_channel_rule-Remove')")
    @ApiOperation(value = "删除实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "删除实时聊天频道规则")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channel_rules/{im_livechat_channel_rule_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_channel_rule_id") Long im_livechat_channel_rule_id) {
         return ResponseEntity.status(HttpStatus.OK).body(im_livechat_channel_ruleService.remove(im_livechat_channel_rule_id));
    }

    @PreAuthorize("hasPermission(this.im_livechat_channel_ruleService.getImLivechatChannelRuleByIds(#ids),'iBizBusinessCentral-Im_livechat_channel_rule-Remove')")
    @ApiOperation(value = "批量删除实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "批量删除实时聊天频道规则")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channel_rules/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        im_livechat_channel_ruleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.im_livechat_channel_ruleMapping.toDomain(returnObject.body),'iBizBusinessCentral-Im_livechat_channel_rule-Get')")
    @ApiOperation(value = "获取实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "获取实时聊天频道规则")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channel_rules/{im_livechat_channel_rule_id}")
    public ResponseEntity<Im_livechat_channel_ruleDTO> get(@PathVariable("im_livechat_channel_rule_id") Long im_livechat_channel_rule_id) {
        Im_livechat_channel_rule domain = im_livechat_channel_ruleService.get(im_livechat_channel_rule_id);
        Im_livechat_channel_ruleDTO dto = im_livechat_channel_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取实时聊天频道规则草稿", tags = {"实时聊天频道规则" },  notes = "获取实时聊天频道规则草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channel_rules/getdraft")
    public ResponseEntity<Im_livechat_channel_ruleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(im_livechat_channel_ruleMapping.toDto(im_livechat_channel_ruleService.getDraft(new Im_livechat_channel_rule())));
    }

    @ApiOperation(value = "检查实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "检查实时聊天频道规则")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Im_livechat_channel_ruleDTO im_livechat_channel_ruledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(im_livechat_channel_ruleService.checkKey(im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledto)));
    }

    @PreAuthorize("hasPermission(this.im_livechat_channel_ruleMapping.toDomain(#im_livechat_channel_ruledto),'iBizBusinessCentral-Im_livechat_channel_rule-Save')")
    @ApiOperation(value = "保存实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "保存实时聊天频道规则")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules/save")
    public ResponseEntity<Boolean> save(@RequestBody Im_livechat_channel_ruleDTO im_livechat_channel_ruledto) {
        return ResponseEntity.status(HttpStatus.OK).body(im_livechat_channel_ruleService.save(im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledto)));
    }

    @PreAuthorize("hasPermission(this.im_livechat_channel_ruleMapping.toDomain(#im_livechat_channel_ruledtos),'iBizBusinessCentral-Im_livechat_channel_rule-Save')")
    @ApiOperation(value = "批量保存实时聊天频道规则", tags = {"实时聊天频道规则" },  notes = "批量保存实时聊天频道规则")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Im_livechat_channel_ruleDTO> im_livechat_channel_ruledtos) {
        im_livechat_channel_ruleService.saveBatch(im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_channel_rule-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Im_livechat_channel_rule-Get')")
	@ApiOperation(value = "获取数据集", tags = {"实时聊天频道规则" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_channel_rules/fetchdefault")
	public ResponseEntity<List<Im_livechat_channel_ruleDTO>> fetchDefault(Im_livechat_channel_ruleSearchContext context) {
        Page<Im_livechat_channel_rule> domains = im_livechat_channel_ruleService.searchDefault(context) ;
        List<Im_livechat_channel_ruleDTO> list = im_livechat_channel_ruleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_channel_rule-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Im_livechat_channel_rule-Get')")
	@ApiOperation(value = "查询数据集", tags = {"实时聊天频道规则" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/im_livechat_channel_rules/searchdefault")
	public ResponseEntity<Page<Im_livechat_channel_ruleDTO>> searchDefault(@RequestBody Im_livechat_channel_ruleSearchContext context) {
        Page<Im_livechat_channel_rule> domains = im_livechat_channel_ruleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(im_livechat_channel_ruleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

