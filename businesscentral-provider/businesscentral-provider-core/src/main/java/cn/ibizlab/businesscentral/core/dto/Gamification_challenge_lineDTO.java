package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Gamification_challenge_lineDTO]
 */
@Data
public class Gamification_challenge_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [TARGET_GOAL]
     *
     */
    @JSONField(name = "target_goal")
    @JsonProperty("target_goal")
    @NotNull(message = "[要达到的目标值]不允许为空!")
    private Double targetGoal;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [DEFINITION_MONETARY]
     *
     */
    @JSONField(name = "definition_monetary")
    @JsonProperty("definition_monetary")
    private Boolean definitionMonetary;

    /**
     * 属性 [CONDITION]
     *
     */
    @JSONField(name = "condition")
    @JsonProperty("condition")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String condition;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [DEFINITION_FULL_SUFFIX]
     *
     */
    @JSONField(name = "definition_full_suffix")
    @JsonProperty("definition_full_suffix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String definitionFullSuffix;

    /**
     * 属性 [CHALLENGE_ID_TEXT]
     *
     */
    @JSONField(name = "challenge_id_text")
    @JsonProperty("challenge_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String challengeIdText;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [DEFINITION_SUFFIX]
     *
     */
    @JSONField(name = "definition_suffix")
    @JsonProperty("definition_suffix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String definitionSuffix;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CHALLENGE_ID]
     *
     */
    @JSONField(name = "challenge_id")
    @JsonProperty("challenge_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[挑战]不允许为空!")
    private Long challengeId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [DEFINITION_ID]
     *
     */
    @JSONField(name = "definition_id")
    @JsonProperty("definition_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[目标定义]不允许为空!")
    private Long definitionId;


    /**
     * 设置 [TARGET_GOAL]
     */
    public void setTargetGoal(Double  targetGoal){
        this.targetGoal = targetGoal ;
        this.modify("target_goal",targetGoal);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [CHALLENGE_ID]
     */
    public void setChallengeId(Long  challengeId){
        this.challengeId = challengeId ;
        this.modify("challenge_id",challengeId);
    }

    /**
     * 设置 [DEFINITION_ID]
     */
    public void setDefinitionId(Long  definitionId){
        this.definitionId = definitionId ;
        this.modify("definition_id",definitionId);
    }


}


