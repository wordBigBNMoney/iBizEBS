package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Res_supplierDTO]
 */
@Data
public class Res_supplierDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String barcode;

    /**
     * 属性 [PHONE]
     *
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String phone;

    /**
     * 属性 [COMPANY_TYPE]
     *
     */
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyType;

    /**
     * 属性 [REF]
     *
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String ref;

    /**
     * 属性 [EMAIL]
     *
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String email;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String type;

    /**
     * 属性 [FUNCTION]
     *
     */
    @JSONField(name = "function")
    @JsonProperty("function")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String function;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private String categoryId;
    /**
     * 属性 [MOBILE]
     *
     */
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mobile;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [VAT]
     *
     */
    @JSONField(name = "vat")
    @JsonProperty("vat")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String vat;

    /**
     * 属性 [PROPERTY_PURCHASE_CURRENCY_NAME]
     *
     */
    @JSONField(name = "property_purchase_currency_name")
    @JsonProperty("property_purchase_currency_name")
    @Size(min = 0, max = 3, message = "内容长度必须小于等于[3]")
    private String propertyPurchaseCurrencyName;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String companyIdText;

    /**
     * 属性 [PROPERTY_STOCK_SUBCONTRACTOR_NAME]
     *
     */
    @JSONField(name = "property_stock_subcontractor_name")
    @JsonProperty("property_stock_subcontractor_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String propertyStockSubcontractorName;

    /**
     * 属性 [PROPERTY_STOCK_CUSTOMER_NAME]
     *
     */
    @JSONField(name = "property_stock_customer_name")
    @JsonProperty("property_stock_customer_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String propertyStockCustomerName;

    /**
     * 属性 [PROPERTY_STOCK_SUPPLIER_NAME]
     *
     */
    @JSONField(name = "property_stock_supplier_name")
    @JsonProperty("property_stock_supplier_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String propertyStockSupplierName;

    /**
     * 属性 [PARENT_NAME]
     *
     */
    @JSONField(name = "parent_name")
    @JsonProperty("parent_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String parentName;

    /**
     * 属性 [PROPERTY_PAYMENT_TERM_NAME]
     *
     */
    @JSONField(name = "property_payment_term_name")
    @JsonProperty("property_payment_term_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String propertyPaymentTermName;

    /**
     * 属性 [PROPERTY_DELIVERY_CARRIER_NAME]
     *
     */
    @JSONField(name = "property_delivery_carrier_name")
    @JsonProperty("property_delivery_carrier_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String propertyDeliveryCarrierName;

    /**
     * 属性 [PROPERTY_PRODUCT_PRICELIST_NAME]
     *
     */
    @JSONField(name = "property_product_pricelist_name")
    @JsonProperty("property_product_pricelist_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String propertyProductPricelistName;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String countryIdText;

    /**
     * 属性 [PROPERTY_ACCOUNT_POSITION_NAME]
     *
     */
    @JSONField(name = "property_account_position_name")
    @JsonProperty("property_account_position_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String propertyAccountPositionName;

    /**
     * 属性 [STATE_ID_TEXT]
     *
     */
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String stateIdText;

    /**
     * 属性 [TITLE_TEXT]
     *
     */
    @JSONField(name = "title_text")
    @JsonProperty("title_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String titleText;

    /**
     * 属性 [PROPERTY_SUPPLIER_PAYMENT_TERM_NAME]
     *
     */
    @JSONField(name = "property_supplier_payment_term_name")
    @JsonProperty("property_supplier_payment_term_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String propertySupplierPaymentTermName;

    /**
     * 属性 [USER_NAME]
     *
     */
    @JSONField(name = "user_name")
    @JsonProperty("user_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String userName;

    /**
     * 属性 [STATE_ID]
     *
     */
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stateId;

    /**
     * 属性 [PROPERTY_PURCHASE_CURRENCY_ID]
     *
     */
    @JSONField(name = "property_purchase_currency_id")
    @JsonProperty("property_purchase_currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyPurchaseCurrencyId;

    /**
     * 属性 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "property_supplier_payment_term_id")
    @JsonProperty("property_supplier_payment_term_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertySupplierPaymentTermId;

    /**
     * 属性 [PROPERTY_PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "property_payment_term_id")
    @JsonProperty("property_payment_term_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyPaymentTermId;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryId;

    /**
     * 属性 [PROPERTY_STOCK_SUPPLIER]
     *
     */
    @JSONField(name = "property_stock_supplier")
    @JsonProperty("property_stock_supplier")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyStockSupplier;

    /**
     * 属性 [PROPERTY_DELIVERY_CARRIER_ID]
     *
     */
    @JSONField(name = "property_delivery_carrier_id")
    @JsonProperty("property_delivery_carrier_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyDeliveryCarrierId;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long title;

    /**
     * 属性 [PROPERTY_ACCOUNT_POSITION_ID]
     *
     */
    @JSONField(name = "property_account_position_id")
    @JsonProperty("property_account_position_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyAccountPositionId;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [PROPERTY_STOCK_CUSTOMER]
     *
     */
    @JSONField(name = "property_stock_customer")
    @JsonProperty("property_stock_customer")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyStockCustomer;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [PROPERTY_STOCK_SUBCONTRACTOR]
     *
     */
    @JSONField(name = "property_stock_subcontractor")
    @JsonProperty("property_stock_subcontractor")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyStockSubcontractor;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [PROPERTY_PRODUCT_PRICELIST]
     *
     */
    @JSONField(name = "property_product_pricelist")
    @JsonProperty("property_product_pricelist")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyProductPricelist;

    /**
     * 属性 [STREET]
     *
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String street;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteUrl;

    /**
     * 属性 [CITY]
     *
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String city;

    /**
     * 属性 [STREET2]
     *
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String street2;

    /**
     * 属性 [ZIP]
     *
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String zip;

    /**
     * 属性 [IS_COMPANY]
     *
     */
    @JSONField(name = "is_company")
    @JsonProperty("is_company")
    private Boolean isCompany;


    /**
     * 设置 [BARCODE]
     */
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [PHONE]
     */
    public void setPhone(String  phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }

    /**
     * 设置 [REF]
     */
    public void setRef(String  ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }

    /**
     * 设置 [EMAIL]
     */
    public void setEmail(String  email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [FUNCTION]
     */
    public void setFunction(String  function){
        this.function = function ;
        this.modify("function",function);
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    public void setCategoryId(String  categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

    /**
     * 设置 [MOBILE]
     */
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [VAT]
     */
    public void setVat(String  vat){
        this.vat = vat ;
        this.modify("vat",vat);
    }

    /**
     * 设置 [STATE_ID]
     */
    public void setStateId(Long  stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }

    /**
     * 设置 [PROPERTY_PURCHASE_CURRENCY_ID]
     */
    public void setPropertyPurchaseCurrencyId(Long  propertyPurchaseCurrencyId){
        this.propertyPurchaseCurrencyId = propertyPurchaseCurrencyId ;
        this.modify("property_purchase_currency_id",propertyPurchaseCurrencyId);
    }

    /**
     * 设置 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     */
    public void setPropertySupplierPaymentTermId(Long  propertySupplierPaymentTermId){
        this.propertySupplierPaymentTermId = propertySupplierPaymentTermId ;
        this.modify("property_supplier_payment_term_id",propertySupplierPaymentTermId);
    }

    /**
     * 设置 [PROPERTY_PAYMENT_TERM_ID]
     */
    public void setPropertyPaymentTermId(Long  propertyPaymentTermId){
        this.propertyPaymentTermId = propertyPaymentTermId ;
        this.modify("property_payment_term_id",propertyPaymentTermId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Long  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [PROPERTY_STOCK_SUPPLIER]
     */
    public void setPropertyStockSupplier(Long  propertyStockSupplier){
        this.propertyStockSupplier = propertyStockSupplier ;
        this.modify("property_stock_supplier",propertyStockSupplier);
    }

    /**
     * 设置 [PROPERTY_DELIVERY_CARRIER_ID]
     */
    public void setPropertyDeliveryCarrierId(Long  propertyDeliveryCarrierId){
        this.propertyDeliveryCarrierId = propertyDeliveryCarrierId ;
        this.modify("property_delivery_carrier_id",propertyDeliveryCarrierId);
    }

    /**
     * 设置 [TITLE]
     */
    public void setTitle(Long  title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_POSITION_ID]
     */
    public void setPropertyAccountPositionId(Long  propertyAccountPositionId){
        this.propertyAccountPositionId = propertyAccountPositionId ;
        this.modify("property_account_position_id",propertyAccountPositionId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Long  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [PROPERTY_STOCK_CUSTOMER]
     */
    public void setPropertyStockCustomer(Long  propertyStockCustomer){
        this.propertyStockCustomer = propertyStockCustomer ;
        this.modify("property_stock_customer",propertyStockCustomer);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [PROPERTY_STOCK_SUBCONTRACTOR]
     */
    public void setPropertyStockSubcontractor(Long  propertyStockSubcontractor){
        this.propertyStockSubcontractor = propertyStockSubcontractor ;
        this.modify("property_stock_subcontractor",propertyStockSubcontractor);
    }

    /**
     * 设置 [PROPERTY_PRODUCT_PRICELIST]
     */
    public void setPropertyProductPricelist(Long  propertyProductPricelist){
        this.propertyProductPricelist = propertyProductPricelist ;
        this.modify("property_product_pricelist",propertyProductPricelist);
    }

    /**
     * 设置 [STREET]
     */
    public void setStreet(String  street){
        this.street = street ;
        this.modify("street",street);
    }

    /**
     * 设置 [CITY]
     */
    public void setCity(String  city){
        this.city = city ;
        this.modify("city",city);
    }

    /**
     * 设置 [STREET2]
     */
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.modify("street2",street2);
    }

    /**
     * 设置 [ZIP]
     */
    public void setZip(String  zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }

    /**
     * 设置 [IS_COMPANY]
     */
    public void setIsCompany(Boolean  isCompany){
        this.isCompany = isCompany ;
        this.modify("is_company",isCompany);
    }


}


