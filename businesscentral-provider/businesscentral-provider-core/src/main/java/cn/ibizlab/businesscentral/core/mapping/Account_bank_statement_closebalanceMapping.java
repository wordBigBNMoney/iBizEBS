package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.businesscentral.core.dto.Account_bank_statement_closebalanceDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_bank_statement_closebalanceMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_bank_statement_closebalanceMapping extends MappingBase<Account_bank_statement_closebalanceDTO, Account_bank_statement_closebalance> {


}

