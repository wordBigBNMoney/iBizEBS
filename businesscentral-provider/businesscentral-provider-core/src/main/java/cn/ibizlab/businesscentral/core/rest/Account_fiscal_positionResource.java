package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_positionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"税科目调整" })
@RestController("Core-account_fiscal_position")
@RequestMapping("")
public class Account_fiscal_positionResource {

    @Autowired
    public IAccount_fiscal_positionService account_fiscal_positionService;

    @Autowired
    @Lazy
    public Account_fiscal_positionMapping account_fiscal_positionMapping;

    @PreAuthorize("hasPermission(this.account_fiscal_positionMapping.toDomain(#account_fiscal_positiondto),'iBizBusinessCentral-Account_fiscal_position-Create')")
    @ApiOperation(value = "新建税科目调整", tags = {"税科目调整" },  notes = "新建税科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions")
    public ResponseEntity<Account_fiscal_positionDTO> create(@Validated @RequestBody Account_fiscal_positionDTO account_fiscal_positiondto) {
        Account_fiscal_position domain = account_fiscal_positionMapping.toDomain(account_fiscal_positiondto);
		account_fiscal_positionService.create(domain);
        Account_fiscal_positionDTO dto = account_fiscal_positionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_positionMapping.toDomain(#account_fiscal_positiondtos),'iBizBusinessCentral-Account_fiscal_position-Create')")
    @ApiOperation(value = "批量新建税科目调整", tags = {"税科目调整" },  notes = "批量新建税科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_fiscal_positionDTO> account_fiscal_positiondtos) {
        account_fiscal_positionService.createBatch(account_fiscal_positionMapping.toDomain(account_fiscal_positiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_fiscal_position" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_fiscal_positionService.get(#account_fiscal_position_id),'iBizBusinessCentral-Account_fiscal_position-Update')")
    @ApiOperation(value = "更新税科目调整", tags = {"税科目调整" },  notes = "更新税科目调整")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_positions/{account_fiscal_position_id}")
    public ResponseEntity<Account_fiscal_positionDTO> update(@PathVariable("account_fiscal_position_id") Long account_fiscal_position_id, @RequestBody Account_fiscal_positionDTO account_fiscal_positiondto) {
		Account_fiscal_position domain  = account_fiscal_positionMapping.toDomain(account_fiscal_positiondto);
        domain .setId(account_fiscal_position_id);
		account_fiscal_positionService.update(domain );
		Account_fiscal_positionDTO dto = account_fiscal_positionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_positionService.getAccountFiscalPositionByEntities(this.account_fiscal_positionMapping.toDomain(#account_fiscal_positiondtos)),'iBizBusinessCentral-Account_fiscal_position-Update')")
    @ApiOperation(value = "批量更新税科目调整", tags = {"税科目调整" },  notes = "批量更新税科目调整")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_positions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_positionDTO> account_fiscal_positiondtos) {
        account_fiscal_positionService.updateBatch(account_fiscal_positionMapping.toDomain(account_fiscal_positiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_positionService.get(#account_fiscal_position_id),'iBizBusinessCentral-Account_fiscal_position-Remove')")
    @ApiOperation(value = "删除税科目调整", tags = {"税科目调整" },  notes = "删除税科目调整")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_positions/{account_fiscal_position_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_position_id") Long account_fiscal_position_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_positionService.remove(account_fiscal_position_id));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_positionService.getAccountFiscalPositionByIds(#ids),'iBizBusinessCentral-Account_fiscal_position-Remove')")
    @ApiOperation(value = "批量删除税科目调整", tags = {"税科目调整" },  notes = "批量删除税科目调整")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_positions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_fiscal_positionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_fiscal_positionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_fiscal_position-Get')")
    @ApiOperation(value = "获取税科目调整", tags = {"税科目调整" },  notes = "获取税科目调整")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_positions/{account_fiscal_position_id}")
    public ResponseEntity<Account_fiscal_positionDTO> get(@PathVariable("account_fiscal_position_id") Long account_fiscal_position_id) {
        Account_fiscal_position domain = account_fiscal_positionService.get(account_fiscal_position_id);
        Account_fiscal_positionDTO dto = account_fiscal_positionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取税科目调整草稿", tags = {"税科目调整" },  notes = "获取税科目调整草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_positions/getdraft")
    public ResponseEntity<Account_fiscal_positionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_positionMapping.toDto(account_fiscal_positionService.getDraft(new Account_fiscal_position())));
    }

    @ApiOperation(value = "检查税科目调整", tags = {"税科目调整" },  notes = "检查税科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_fiscal_positionDTO account_fiscal_positiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_fiscal_positionService.checkKey(account_fiscal_positionMapping.toDomain(account_fiscal_positiondto)));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_positionMapping.toDomain(#account_fiscal_positiondto),'iBizBusinessCentral-Account_fiscal_position-Save')")
    @ApiOperation(value = "保存税科目调整", tags = {"税科目调整" },  notes = "保存税科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_fiscal_positionDTO account_fiscal_positiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_positionService.save(account_fiscal_positionMapping.toDomain(account_fiscal_positiondto)));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_positionMapping.toDomain(#account_fiscal_positiondtos),'iBizBusinessCentral-Account_fiscal_position-Save')")
    @ApiOperation(value = "批量保存税科目调整", tags = {"税科目调整" },  notes = "批量保存税科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_fiscal_positionDTO> account_fiscal_positiondtos) {
        account_fiscal_positionService.saveBatch(account_fiscal_positionMapping.toDomain(account_fiscal_positiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_fiscal_position-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_fiscal_position-Get')")
	@ApiOperation(value = "获取数据集", tags = {"税科目调整" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_fiscal_positions/fetchdefault")
	public ResponseEntity<List<Account_fiscal_positionDTO>> fetchDefault(Account_fiscal_positionSearchContext context) {
        Page<Account_fiscal_position> domains = account_fiscal_positionService.searchDefault(context) ;
        List<Account_fiscal_positionDTO> list = account_fiscal_positionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_fiscal_position-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_fiscal_position-Get')")
	@ApiOperation(value = "查询数据集", tags = {"税科目调整" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_fiscal_positions/searchdefault")
	public ResponseEntity<Page<Account_fiscal_positionDTO>> searchDefault(@RequestBody Account_fiscal_positionSearchContext context) {
        Page<Account_fiscal_position> domains = account_fiscal_positionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_fiscal_positionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

