package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Delivery_carrierDTO]
 */
@Data
public class Delivery_carrierDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;

    /**
     * 属性 [FIXED_PRICE]
     *
     */
    @JSONField(name = "fixed_price")
    @JsonProperty("fixed_price")
    private Double fixedPrice;

    /**
     * 属性 [DELIVERY_TYPE]
     *
     */
    @JSONField(name = "delivery_type")
    @JsonProperty("delivery_type")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String deliveryType;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [RETURN_LABEL_ON_DELIVERY]
     *
     */
    @JSONField(name = "return_label_on_delivery")
    @JsonProperty("return_label_on_delivery")
    private Boolean returnLabelOnDelivery;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [INVOICE_POLICY]
     *
     */
    @JSONField(name = "invoice_policy")
    @JsonProperty("invoice_policy")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String invoicePolicy;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [MARGIN]
     *
     */
    @JSONField(name = "margin")
    @JsonProperty("margin")
    private Double margin;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [DEBUG_LOGGING]
     *
     */
    @JSONField(name = "debug_logging")
    @JsonProperty("debug_logging")
    private Boolean debugLogging;

    /**
     * 属性 [ZIP_TO]
     *
     */
    @JSONField(name = "zip_to")
    @JsonProperty("zip_to")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String zipTo;

    /**
     * 属性 [ZIP_FROM]
     *
     */
    @JSONField(name = "zip_from")
    @JsonProperty("zip_from")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String zipFrom;

    /**
     * 属性 [FREE_OVER]
     *
     */
    @JSONField(name = "free_over")
    @JsonProperty("free_over")
    private Boolean freeOver;

    /**
     * 属性 [INTEGRATION_LEVEL]
     *
     */
    @JSONField(name = "integration_level")
    @JsonProperty("integration_level")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String integrationLevel;

    /**
     * 属性 [PROD_ENVIRONMENT]
     *
     */
    @JSONField(name = "prod_environment")
    @JsonProperty("prod_environment")
    private Boolean prodEnvironment;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [GET_RETURN_LABEL_FROM_PORTAL]
     *
     */
    @JSONField(name = "get_return_label_from_portal")
    @JsonProperty("get_return_label_from_portal")
    private Boolean getReturnLabelFromPortal;

    /**
     * 属性 [PRODUCT_NAME]
     *
     */
    @JSONField(name = "product_name")
    @JsonProperty("product_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String productName;

    /**
     * 属性 [COMPANY_NAME]
     *
     */
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String companyName;

    /**
     * 属性 [WRITE_UNAME]
     *
     */
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String writeUname;

    /**
     * 属性 [CREATE_UNAME]
     *
     */
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String createUname;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [IS_PUBLISHED]
     */
    public void setIsPublished(Boolean  isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [FIXED_PRICE]
     */
    public void setFixedPrice(Double  fixedPrice){
        this.fixedPrice = fixedPrice ;
        this.modify("fixed_price",fixedPrice);
    }

    /**
     * 设置 [DELIVERY_TYPE]
     */
    public void setDeliveryType(String  deliveryType){
        this.deliveryType = deliveryType ;
        this.modify("delivery_type",deliveryType);
    }

    /**
     * 设置 [WRITE_DATE]
     */
    public void setWriteDate(Timestamp  writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 设置 [RETURN_LABEL_ON_DELIVERY]
     */
    public void setReturnLabelOnDelivery(Boolean  returnLabelOnDelivery){
        this.returnLabelOnDelivery = returnLabelOnDelivery ;
        this.modify("return_label_on_delivery",returnLabelOnDelivery);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [INVOICE_POLICY]
     */
    public void setInvoicePolicy(String  invoicePolicy){
        this.invoicePolicy = invoicePolicy ;
        this.modify("invoice_policy",invoicePolicy);
    }

    /**
     * 设置 [CREATE_DATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [MARGIN]
     */
    public void setMargin(Double  margin){
        this.margin = margin ;
        this.modify("margin",margin);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [DEBUG_LOGGING]
     */
    public void setDebugLogging(Boolean  debugLogging){
        this.debugLogging = debugLogging ;
        this.modify("debug_logging",debugLogging);
    }

    /**
     * 设置 [ZIP_TO]
     */
    public void setZipTo(String  zipTo){
        this.zipTo = zipTo ;
        this.modify("zip_to",zipTo);
    }

    /**
     * 设置 [ZIP_FROM]
     */
    public void setZipFrom(String  zipFrom){
        this.zipFrom = zipFrom ;
        this.modify("zip_from",zipFrom);
    }

    /**
     * 设置 [FREE_OVER]
     */
    public void setFreeOver(Boolean  freeOver){
        this.freeOver = freeOver ;
        this.modify("free_over",freeOver);
    }

    /**
     * 设置 [INTEGRATION_LEVEL]
     */
    public void setIntegrationLevel(String  integrationLevel){
        this.integrationLevel = integrationLevel ;
        this.modify("integration_level",integrationLevel);
    }

    /**
     * 设置 [PROD_ENVIRONMENT]
     */
    public void setProdEnvironment(Boolean  prodEnvironment){
        this.prodEnvironment = prodEnvironment ;
        this.modify("prod_environment",prodEnvironment);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [GET_RETURN_LABEL_FROM_PORTAL]
     */
    public void setGetReturnLabelFromPortal(Boolean  getReturnLabelFromPortal){
        this.getReturnLabelFromPortal = getReturnLabelFromPortal ;
        this.modify("get_return_label_from_portal",getReturnLabelFromPortal);
    }

    /**
     * 设置 [WRITE_UID]
     */
    public void setWriteUid(Long  writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [CREATE_UID]
     */
    public void setCreateUid(Long  createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }


}


