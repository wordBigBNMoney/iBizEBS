package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_request_rejectService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_request_rejectSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Reject Request" })
@RestController("Core-mro_request_reject")
@RequestMapping("")
public class Mro_request_rejectResource {

    @Autowired
    public IMro_request_rejectService mro_request_rejectService;

    @Autowired
    @Lazy
    public Mro_request_rejectMapping mro_request_rejectMapping;

    @PreAuthorize("hasPermission(this.mro_request_rejectMapping.toDomain(#mro_request_rejectdto),'iBizBusinessCentral-Mro_request_reject-Create')")
    @ApiOperation(value = "新建Reject Request", tags = {"Reject Request" },  notes = "新建Reject Request")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects")
    public ResponseEntity<Mro_request_rejectDTO> create(@Validated @RequestBody Mro_request_rejectDTO mro_request_rejectdto) {
        Mro_request_reject domain = mro_request_rejectMapping.toDomain(mro_request_rejectdto);
		mro_request_rejectService.create(domain);
        Mro_request_rejectDTO dto = mro_request_rejectMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_request_rejectMapping.toDomain(#mro_request_rejectdtos),'iBizBusinessCentral-Mro_request_reject-Create')")
    @ApiOperation(value = "批量新建Reject Request", tags = {"Reject Request" },  notes = "批量新建Reject Request")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_request_rejectDTO> mro_request_rejectdtos) {
        mro_request_rejectService.createBatch(mro_request_rejectMapping.toDomain(mro_request_rejectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_request_reject" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_request_rejectService.get(#mro_request_reject_id),'iBizBusinessCentral-Mro_request_reject-Update')")
    @ApiOperation(value = "更新Reject Request", tags = {"Reject Request" },  notes = "更新Reject Request")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_request_rejects/{mro_request_reject_id}")
    public ResponseEntity<Mro_request_rejectDTO> update(@PathVariable("mro_request_reject_id") Long mro_request_reject_id, @RequestBody Mro_request_rejectDTO mro_request_rejectdto) {
		Mro_request_reject domain  = mro_request_rejectMapping.toDomain(mro_request_rejectdto);
        domain .setId(mro_request_reject_id);
		mro_request_rejectService.update(domain );
		Mro_request_rejectDTO dto = mro_request_rejectMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_request_rejectService.getMroRequestRejectByEntities(this.mro_request_rejectMapping.toDomain(#mro_request_rejectdtos)),'iBizBusinessCentral-Mro_request_reject-Update')")
    @ApiOperation(value = "批量更新Reject Request", tags = {"Reject Request" },  notes = "批量更新Reject Request")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_request_rejects/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_request_rejectDTO> mro_request_rejectdtos) {
        mro_request_rejectService.updateBatch(mro_request_rejectMapping.toDomain(mro_request_rejectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_request_rejectService.get(#mro_request_reject_id),'iBizBusinessCentral-Mro_request_reject-Remove')")
    @ApiOperation(value = "删除Reject Request", tags = {"Reject Request" },  notes = "删除Reject Request")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_request_rejects/{mro_request_reject_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_request_reject_id") Long mro_request_reject_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_request_rejectService.remove(mro_request_reject_id));
    }

    @PreAuthorize("hasPermission(this.mro_request_rejectService.getMroRequestRejectByIds(#ids),'iBizBusinessCentral-Mro_request_reject-Remove')")
    @ApiOperation(value = "批量删除Reject Request", tags = {"Reject Request" },  notes = "批量删除Reject Request")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_request_rejects/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_request_rejectService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_request_rejectMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_request_reject-Get')")
    @ApiOperation(value = "获取Reject Request", tags = {"Reject Request" },  notes = "获取Reject Request")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_request_rejects/{mro_request_reject_id}")
    public ResponseEntity<Mro_request_rejectDTO> get(@PathVariable("mro_request_reject_id") Long mro_request_reject_id) {
        Mro_request_reject domain = mro_request_rejectService.get(mro_request_reject_id);
        Mro_request_rejectDTO dto = mro_request_rejectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Reject Request草稿", tags = {"Reject Request" },  notes = "获取Reject Request草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_request_rejects/getdraft")
    public ResponseEntity<Mro_request_rejectDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_request_rejectMapping.toDto(mro_request_rejectService.getDraft(new Mro_request_reject())));
    }

    @ApiOperation(value = "检查Reject Request", tags = {"Reject Request" },  notes = "检查Reject Request")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_request_rejectDTO mro_request_rejectdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_request_rejectService.checkKey(mro_request_rejectMapping.toDomain(mro_request_rejectdto)));
    }

    @PreAuthorize("hasPermission(this.mro_request_rejectMapping.toDomain(#mro_request_rejectdto),'iBizBusinessCentral-Mro_request_reject-Save')")
    @ApiOperation(value = "保存Reject Request", tags = {"Reject Request" },  notes = "保存Reject Request")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_request_rejectDTO mro_request_rejectdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_request_rejectService.save(mro_request_rejectMapping.toDomain(mro_request_rejectdto)));
    }

    @PreAuthorize("hasPermission(this.mro_request_rejectMapping.toDomain(#mro_request_rejectdtos),'iBizBusinessCentral-Mro_request_reject-Save')")
    @ApiOperation(value = "批量保存Reject Request", tags = {"Reject Request" },  notes = "批量保存Reject Request")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_request_rejectDTO> mro_request_rejectdtos) {
        mro_request_rejectService.saveBatch(mro_request_rejectMapping.toDomain(mro_request_rejectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_request_reject-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_request_reject-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Reject Request" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_request_rejects/fetchdefault")
	public ResponseEntity<List<Mro_request_rejectDTO>> fetchDefault(Mro_request_rejectSearchContext context) {
        Page<Mro_request_reject> domains = mro_request_rejectService.searchDefault(context) ;
        List<Mro_request_rejectDTO> list = mro_request_rejectMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_request_reject-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_request_reject-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Reject Request" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_request_rejects/searchdefault")
	public ResponseEntity<Page<Mro_request_rejectDTO>> searchDefault(@RequestBody Mro_request_rejectSearchContext context) {
        Page<Mro_request_reject> domains = mro_request_rejectService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_request_rejectMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

