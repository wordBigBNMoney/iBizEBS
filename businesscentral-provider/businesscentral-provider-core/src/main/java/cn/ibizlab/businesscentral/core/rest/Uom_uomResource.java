package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_uomSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品计量单位" })
@RestController("Core-uom_uom")
@RequestMapping("")
public class Uom_uomResource {

    @Autowired
    public IUom_uomService uom_uomService;

    @Autowired
    @Lazy
    public Uom_uomMapping uom_uomMapping;

    @PreAuthorize("hasPermission(this.uom_uomMapping.toDomain(#uom_uomdto),'iBizBusinessCentral-Uom_uom-Create')")
    @ApiOperation(value = "新建产品计量单位", tags = {"产品计量单位" },  notes = "新建产品计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms")
    public ResponseEntity<Uom_uomDTO> create(@Validated @RequestBody Uom_uomDTO uom_uomdto) {
        Uom_uom domain = uom_uomMapping.toDomain(uom_uomdto);
		uom_uomService.create(domain);
        Uom_uomDTO dto = uom_uomMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.uom_uomMapping.toDomain(#uom_uomdtos),'iBizBusinessCentral-Uom_uom-Create')")
    @ApiOperation(value = "批量新建产品计量单位", tags = {"产品计量单位" },  notes = "批量新建产品计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Uom_uomDTO> uom_uomdtos) {
        uom_uomService.createBatch(uom_uomMapping.toDomain(uom_uomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "uom_uom" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.uom_uomService.get(#uom_uom_id),'iBizBusinessCentral-Uom_uom-Update')")
    @ApiOperation(value = "更新产品计量单位", tags = {"产品计量单位" },  notes = "更新产品计量单位")
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_uoms/{uom_uom_id}")
    public ResponseEntity<Uom_uomDTO> update(@PathVariable("uom_uom_id") Long uom_uom_id, @RequestBody Uom_uomDTO uom_uomdto) {
		Uom_uom domain  = uom_uomMapping.toDomain(uom_uomdto);
        domain .setId(uom_uom_id);
		uom_uomService.update(domain );
		Uom_uomDTO dto = uom_uomMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.uom_uomService.getUomUomByEntities(this.uom_uomMapping.toDomain(#uom_uomdtos)),'iBizBusinessCentral-Uom_uom-Update')")
    @ApiOperation(value = "批量更新产品计量单位", tags = {"产品计量单位" },  notes = "批量更新产品计量单位")
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_uoms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Uom_uomDTO> uom_uomdtos) {
        uom_uomService.updateBatch(uom_uomMapping.toDomain(uom_uomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.uom_uomService.get(#uom_uom_id),'iBizBusinessCentral-Uom_uom-Remove')")
    @ApiOperation(value = "删除产品计量单位", tags = {"产品计量单位" },  notes = "删除产品计量单位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_uoms/{uom_uom_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("uom_uom_id") Long uom_uom_id) {
         return ResponseEntity.status(HttpStatus.OK).body(uom_uomService.remove(uom_uom_id));
    }

    @PreAuthorize("hasPermission(this.uom_uomService.getUomUomByIds(#ids),'iBizBusinessCentral-Uom_uom-Remove')")
    @ApiOperation(value = "批量删除产品计量单位", tags = {"产品计量单位" },  notes = "批量删除产品计量单位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_uoms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        uom_uomService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.uom_uomMapping.toDomain(returnObject.body),'iBizBusinessCentral-Uom_uom-Get')")
    @ApiOperation(value = "获取产品计量单位", tags = {"产品计量单位" },  notes = "获取产品计量单位")
	@RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/{uom_uom_id}")
    public ResponseEntity<Uom_uomDTO> get(@PathVariable("uom_uom_id") Long uom_uom_id) {
        Uom_uom domain = uom_uomService.get(uom_uom_id);
        Uom_uomDTO dto = uom_uomMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品计量单位草稿", tags = {"产品计量单位" },  notes = "获取产品计量单位草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/getdraft")
    public ResponseEntity<Uom_uomDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(uom_uomMapping.toDto(uom_uomService.getDraft(new Uom_uom())));
    }

    @ApiOperation(value = "检查产品计量单位", tags = {"产品计量单位" },  notes = "检查产品计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Uom_uomDTO uom_uomdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(uom_uomService.checkKey(uom_uomMapping.toDomain(uom_uomdto)));
    }

    @PreAuthorize("hasPermission(this.uom_uomMapping.toDomain(#uom_uomdto),'iBizBusinessCentral-Uom_uom-Save')")
    @ApiOperation(value = "保存产品计量单位", tags = {"产品计量单位" },  notes = "保存产品计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms/save")
    public ResponseEntity<Boolean> save(@RequestBody Uom_uomDTO uom_uomdto) {
        return ResponseEntity.status(HttpStatus.OK).body(uom_uomService.save(uom_uomMapping.toDomain(uom_uomdto)));
    }

    @PreAuthorize("hasPermission(this.uom_uomMapping.toDomain(#uom_uomdtos),'iBizBusinessCentral-Uom_uom-Save')")
    @ApiOperation(value = "批量保存产品计量单位", tags = {"产品计量单位" },  notes = "批量保存产品计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Uom_uomDTO> uom_uomdtos) {
        uom_uomService.saveBatch(uom_uomMapping.toDomain(uom_uomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Uom_uom-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Uom_uom-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品计量单位" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/uom_uoms/fetchdefault")
	public ResponseEntity<List<Uom_uomDTO>> fetchDefault(Uom_uomSearchContext context) {
        Page<Uom_uom> domains = uom_uomService.searchDefault(context) ;
        List<Uom_uomDTO> list = uom_uomMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Uom_uom-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Uom_uom-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品计量单位" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/uom_uoms/searchdefault")
	public ResponseEntity<Page<Uom_uomDTO>> searchDefault(@RequestBody Uom_uomSearchContext context) {
        Page<Uom_uom> domains = uom_uomService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(uom_uomMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

