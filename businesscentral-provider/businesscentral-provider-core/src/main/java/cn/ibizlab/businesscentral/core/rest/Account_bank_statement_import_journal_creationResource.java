package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_import_journal_creationService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_import_journal_creationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"在银行对账单导入创建日记账" })
@RestController("Core-account_bank_statement_import_journal_creation")
@RequestMapping("")
public class Account_bank_statement_import_journal_creationResource {

    @Autowired
    public IAccount_bank_statement_import_journal_creationService account_bank_statement_import_journal_creationService;

    @Autowired
    @Lazy
    public Account_bank_statement_import_journal_creationMapping account_bank_statement_import_journal_creationMapping;

    @PreAuthorize("hasPermission(this.account_bank_statement_import_journal_creationMapping.toDomain(#account_bank_statement_import_journal_creationdto),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Create')")
    @ApiOperation(value = "新建在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "新建在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations")
    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> create(@Validated @RequestBody Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto) {
        Account_bank_statement_import_journal_creation domain = account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdto);
		account_bank_statement_import_journal_creationService.create(domain);
        Account_bank_statement_import_journal_creationDTO dto = account_bank_statement_import_journal_creationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_import_journal_creationMapping.toDomain(#account_bank_statement_import_journal_creationdtos),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Create')")
    @ApiOperation(value = "批量新建在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "批量新建在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_import_journal_creationDTO> account_bank_statement_import_journal_creationdtos) {
        account_bank_statement_import_journal_creationService.createBatch(account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_bank_statement_import_journal_creation" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_bank_statement_import_journal_creationService.get(#account_bank_statement_import_journal_creation_id),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Update')")
    @ApiOperation(value = "更新在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "更新在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")
    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> update(@PathVariable("account_bank_statement_import_journal_creation_id") Long account_bank_statement_import_journal_creation_id, @RequestBody Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto) {
		Account_bank_statement_import_journal_creation domain  = account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdto);
        domain .setId(account_bank_statement_import_journal_creation_id);
		account_bank_statement_import_journal_creationService.update(domain );
		Account_bank_statement_import_journal_creationDTO dto = account_bank_statement_import_journal_creationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_import_journal_creationService.getAccountBankStatementImportJournalCreationByEntities(this.account_bank_statement_import_journal_creationMapping.toDomain(#account_bank_statement_import_journal_creationdtos)),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Update')")
    @ApiOperation(value = "批量更新在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "批量更新在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_import_journal_creations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_import_journal_creationDTO> account_bank_statement_import_journal_creationdtos) {
        account_bank_statement_import_journal_creationService.updateBatch(account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_import_journal_creationService.get(#account_bank_statement_import_journal_creation_id),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Remove')")
    @ApiOperation(value = "删除在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "删除在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_import_journal_creation_id") Long account_bank_statement_import_journal_creation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_import_journal_creationService.remove(account_bank_statement_import_journal_creation_id));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_import_journal_creationService.getAccountBankStatementImportJournalCreationByIds(#ids),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Remove')")
    @ApiOperation(value = "批量删除在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "批量删除在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_import_journal_creations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_bank_statement_import_journal_creationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_bank_statement_import_journal_creationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Get')")
    @ApiOperation(value = "获取在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "获取在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")
    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> get(@PathVariable("account_bank_statement_import_journal_creation_id") Long account_bank_statement_import_journal_creation_id) {
        Account_bank_statement_import_journal_creation domain = account_bank_statement_import_journal_creationService.get(account_bank_statement_import_journal_creation_id);
        Account_bank_statement_import_journal_creationDTO dto = account_bank_statement_import_journal_creationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取在银行对账单导入创建日记账草稿", tags = {"在银行对账单导入创建日记账" },  notes = "获取在银行对账单导入创建日记账草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_import_journal_creations/getdraft")
    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_import_journal_creationMapping.toDto(account_bank_statement_import_journal_creationService.getDraft(new Account_bank_statement_import_journal_creation())));
    }

    @ApiOperation(value = "检查在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "检查在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_import_journal_creationService.checkKey(account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_import_journal_creationMapping.toDomain(#account_bank_statement_import_journal_creationdto),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Save')")
    @ApiOperation(value = "保存在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "保存在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_import_journal_creationService.save(account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_import_journal_creationMapping.toDomain(#account_bank_statement_import_journal_creationdtos),'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Save')")
    @ApiOperation(value = "批量保存在银行对账单导入创建日记账", tags = {"在银行对账单导入创建日记账" },  notes = "批量保存在银行对账单导入创建日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_bank_statement_import_journal_creationDTO> account_bank_statement_import_journal_creationdtos) {
        account_bank_statement_import_journal_creationService.saveBatch(account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_import_journal_creation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Get')")
	@ApiOperation(value = "获取数据集", tags = {"在银行对账单导入创建日记账" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_import_journal_creations/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_import_journal_creationDTO>> fetchDefault(Account_bank_statement_import_journal_creationSearchContext context) {
        Page<Account_bank_statement_import_journal_creation> domains = account_bank_statement_import_journal_creationService.searchDefault(context) ;
        List<Account_bank_statement_import_journal_creationDTO> list = account_bank_statement_import_journal_creationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_import_journal_creation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_import_journal_creation-Get')")
	@ApiOperation(value = "查询数据集", tags = {"在银行对账单导入创建日记账" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_bank_statement_import_journal_creations/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_import_journal_creationDTO>> searchDefault(@RequestBody Account_bank_statement_import_journal_creationSearchContext context) {
        Page<Account_bank_statement_import_journal_creation> domains = account_bank_statement_import_journal_creationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_import_journal_creationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

