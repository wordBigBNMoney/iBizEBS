package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_labelService;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_labelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"调查标签" })
@RestController("Core-survey_label")
@RequestMapping("")
public class Survey_labelResource {

    @Autowired
    public ISurvey_labelService survey_labelService;

    @Autowired
    @Lazy
    public Survey_labelMapping survey_labelMapping;

    @PreAuthorize("hasPermission(this.survey_labelMapping.toDomain(#survey_labeldto),'iBizBusinessCentral-Survey_label-Create')")
    @ApiOperation(value = "新建调查标签", tags = {"调查标签" },  notes = "新建调查标签")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_labels")
    public ResponseEntity<Survey_labelDTO> create(@Validated @RequestBody Survey_labelDTO survey_labeldto) {
        Survey_label domain = survey_labelMapping.toDomain(survey_labeldto);
		survey_labelService.create(domain);
        Survey_labelDTO dto = survey_labelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_labelMapping.toDomain(#survey_labeldtos),'iBizBusinessCentral-Survey_label-Create')")
    @ApiOperation(value = "批量新建调查标签", tags = {"调查标签" },  notes = "批量新建调查标签")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_labels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_labelDTO> survey_labeldtos) {
        survey_labelService.createBatch(survey_labelMapping.toDomain(survey_labeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "survey_label" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.survey_labelService.get(#survey_label_id),'iBizBusinessCentral-Survey_label-Update')")
    @ApiOperation(value = "更新调查标签", tags = {"调查标签" },  notes = "更新调查标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_labels/{survey_label_id}")
    public ResponseEntity<Survey_labelDTO> update(@PathVariable("survey_label_id") Long survey_label_id, @RequestBody Survey_labelDTO survey_labeldto) {
		Survey_label domain  = survey_labelMapping.toDomain(survey_labeldto);
        domain .setId(survey_label_id);
		survey_labelService.update(domain );
		Survey_labelDTO dto = survey_labelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_labelService.getSurveyLabelByEntities(this.survey_labelMapping.toDomain(#survey_labeldtos)),'iBizBusinessCentral-Survey_label-Update')")
    @ApiOperation(value = "批量更新调查标签", tags = {"调查标签" },  notes = "批量更新调查标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_labels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_labelDTO> survey_labeldtos) {
        survey_labelService.updateBatch(survey_labelMapping.toDomain(survey_labeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.survey_labelService.get(#survey_label_id),'iBizBusinessCentral-Survey_label-Remove')")
    @ApiOperation(value = "删除调查标签", tags = {"调查标签" },  notes = "删除调查标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_labels/{survey_label_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("survey_label_id") Long survey_label_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_labelService.remove(survey_label_id));
    }

    @PreAuthorize("hasPermission(this.survey_labelService.getSurveyLabelByIds(#ids),'iBizBusinessCentral-Survey_label-Remove')")
    @ApiOperation(value = "批量删除调查标签", tags = {"调查标签" },  notes = "批量删除调查标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_labels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        survey_labelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.survey_labelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Survey_label-Get')")
    @ApiOperation(value = "获取调查标签", tags = {"调查标签" },  notes = "获取调查标签")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_labels/{survey_label_id}")
    public ResponseEntity<Survey_labelDTO> get(@PathVariable("survey_label_id") Long survey_label_id) {
        Survey_label domain = survey_labelService.get(survey_label_id);
        Survey_labelDTO dto = survey_labelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取调查标签草稿", tags = {"调查标签" },  notes = "获取调查标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_labels/getdraft")
    public ResponseEntity<Survey_labelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(survey_labelMapping.toDto(survey_labelService.getDraft(new Survey_label())));
    }

    @ApiOperation(value = "检查调查标签", tags = {"调查标签" },  notes = "检查调查标签")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_labels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Survey_labelDTO survey_labeldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(survey_labelService.checkKey(survey_labelMapping.toDomain(survey_labeldto)));
    }

    @PreAuthorize("hasPermission(this.survey_labelMapping.toDomain(#survey_labeldto),'iBizBusinessCentral-Survey_label-Save')")
    @ApiOperation(value = "保存调查标签", tags = {"调查标签" },  notes = "保存调查标签")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_labels/save")
    public ResponseEntity<Boolean> save(@RequestBody Survey_labelDTO survey_labeldto) {
        return ResponseEntity.status(HttpStatus.OK).body(survey_labelService.save(survey_labelMapping.toDomain(survey_labeldto)));
    }

    @PreAuthorize("hasPermission(this.survey_labelMapping.toDomain(#survey_labeldtos),'iBizBusinessCentral-Survey_label-Save')")
    @ApiOperation(value = "批量保存调查标签", tags = {"调查标签" },  notes = "批量保存调查标签")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_labels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Survey_labelDTO> survey_labeldtos) {
        survey_labelService.saveBatch(survey_labelMapping.toDomain(survey_labeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_label-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_label-Get')")
	@ApiOperation(value = "获取数据集", tags = {"调查标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/survey_labels/fetchdefault")
	public ResponseEntity<List<Survey_labelDTO>> fetchDefault(Survey_labelSearchContext context) {
        Page<Survey_label> domains = survey_labelService.searchDefault(context) ;
        List<Survey_labelDTO> list = survey_labelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_label-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_label-Get')")
	@ApiOperation(value = "查询数据集", tags = {"调查标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/survey_labels/searchdefault")
	public ResponseEntity<Page<Survey_labelDTO>> searchDefault(@RequestBody Survey_labelSearchContext context) {
        Page<Survey_label> domains = survey_labelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_labelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

