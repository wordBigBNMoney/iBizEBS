package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_invoice_sendDTO]
 */
@Data
public class Account_invoice_sendDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String ratingIds;

    /**
     * 属性 [TRACKING_VALUE_IDS]
     *
     */
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String trackingValueIds;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [MAILING_LIST_IDS]
     *
     */
    @JSONField(name = "mailing_list_ids")
    @JsonProperty("mailing_list_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String mailingListIds;

    /**
     * 属性 [PRINTED]
     *
     */
    @JSONField(name = "printed")
    @JsonProperty("printed")
    private Boolean printed;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String attachmentIds;

    /**
     * 属性 [INVOICE_WITHOUT_EMAIL]
     *
     */
    @JSONField(name = "invoice_without_email")
    @JsonProperty("invoice_without_email")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceWithoutEmail;

    /**
     * 属性 [NEEDACTION_PARTNER_IDS]
     *
     */
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String needactionPartnerIds;

    /**
     * 属性 [SNAILMAIL_IS_LETTER]
     *
     */
    @JSONField(name = "snailmail_is_letter")
    @JsonProperty("snailmail_is_letter")
    private Boolean snailmailIsLetter;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [IS_PRINT]
     *
     */
    @JSONField(name = "is_print")
    @JsonProperty("is_print")
    private Boolean isPrint;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [SNAILMAIL_COST]
     *
     */
    @JSONField(name = "snailmail_cost")
    @JsonProperty("snailmail_cost")
    private Double snailmailCost;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String channelIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [STARRED_PARTNER_IDS]
     *
     */
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String starredPartnerIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String partnerIds;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String childIds;

    /**
     * 属性 [LETTER_IDS]
     *
     */
    @JSONField(name = "letter_ids")
    @JsonProperty("letter_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String letterIds;

    /**
     * 属性 [NOTIFICATION_IDS]
     *
     */
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String notificationIds;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceIds;

    /**
     * 属性 [IS_EMAIL]
     *
     */
    @JSONField(name = "is_email")
    @JsonProperty("is_email")
    private Boolean isEmail;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [USE_ACTIVE_DOMAIN]
     *
     */
    @JSONField(name = "use_active_domain")
    @JsonProperty("use_active_domain")
    private Boolean useActiveDomain;

    /**
     * 属性 [MODERATION_STATUS]
     *
     */
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String moderationStatus;

    /**
     * 属性 [MODERATOR_ID]
     *
     */
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long moderatorId;

    /**
     * 属性 [HAS_ERROR]
     *
     */
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private Boolean hasError;

    /**
     * 属性 [ACTIVE_DOMAIN]
     *
     */
    @JSONField(name = "active_domain")
    @JsonProperty("active_domain")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activeDomain;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String description;

    /**
     * 属性 [NO_AUTO_THREAD]
     *
     */
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private Boolean noAutoThread;

    /**
     * 属性 [COMPOSITION_MODE]
     *
     */
    @JSONField(name = "composition_mode")
    @JsonProperty("composition_mode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String compositionMode;

    /**
     * 属性 [ADD_SIGN]
     *
     */
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private Boolean addSign;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emailFrom;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mailActivityTypeId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [NEEDACTION]
     *
     */
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private Boolean needaction;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String subject;

    /**
     * 属性 [RECORD_NAME]
     *
     */
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String recordName;

    /**
     * 属性 [AUTHOR_AVATAR]
     *
     */
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;

    /**
     * 属性 [RATING_VALUE]
     *
     */
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;

    /**
     * 属性 [AUTO_DELETE]
     *
     */
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private Boolean autoDelete;

    /**
     * 属性 [AUTO_DELETE_MESSAGE]
     *
     */
    @JSONField(name = "auto_delete_message")
    @JsonProperty("auto_delete_message")
    private Boolean autoDeleteMessage;

    /**
     * 属性 [BODY]
     *
     */
    @JSONField(name = "body")
    @JsonProperty("body")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String body;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String messageId;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String model;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String replyTo;

    /**
     * 属性 [MESSAGE_TYPE]
     *
     */
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    @NotBlank(message = "[类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String messageType;

    /**
     * 属性 [STARRED]
     *
     */
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private Boolean starred;

    /**
     * 属性 [IS_LOG]
     *
     */
    @JSONField(name = "is_log")
    @JsonProperty("is_log")
    private Boolean isLog;

    /**
     * 属性 [NOTIFY]
     *
     */
    @JSONField(name = "notify")
    @JsonProperty("notify")
    private Boolean notify;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID]
     *
     */
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long massMailingCampaignId;

    /**
     * 属性 [NEED_MODERATION]
     *
     */
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private Boolean needModeration;

    /**
     * 属性 [SUBTYPE_ID]
     *
     */
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long subtypeId;

    /**
     * 属性 [AUTHOR_ID]
     *
     */
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long authorId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;

    /**
     * 属性 [MASS_MAILING_ID]
     *
     */
    @JSONField(name = "mass_mailing_id")
    @JsonProperty("mass_mailing_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long massMailingId;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String templateIdText;

    /**
     * 属性 [MASS_MAILING_NAME]
     *
     */
    @JSONField(name = "mass_mailing_name")
    @JsonProperty("mass_mailing_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String massMailingName;

    /**
     * 属性 [LAYOUT]
     *
     */
    @JSONField(name = "layout")
    @JsonProperty("layout")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String layout;

    /**
     * 属性 [COMPOSER_ID]
     *
     */
    @JSONField(name = "composer_id")
    @JsonProperty("composer_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[邮件撰写者]不允许为空!")
    private Long composerId;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long templateId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [PRINTED]
     */
    public void setPrinted(Boolean  printed){
        this.printed = printed ;
        this.modify("printed",printed);
    }

    /**
     * 设置 [SNAILMAIL_IS_LETTER]
     */
    public void setSnailmailIsLetter(Boolean  snailmailIsLetter){
        this.snailmailIsLetter = snailmailIsLetter ;
        this.modify("snailmail_is_letter",snailmailIsLetter);
    }

    /**
     * 设置 [IS_PRINT]
     */
    public void setIsPrint(Boolean  isPrint){
        this.isPrint = isPrint ;
        this.modify("is_print",isPrint);
    }

    /**
     * 设置 [SNAILMAIL_COST]
     */
    public void setSnailmailCost(Double  snailmailCost){
        this.snailmailCost = snailmailCost ;
        this.modify("snailmail_cost",snailmailCost);
    }

    /**
     * 设置 [IS_EMAIL]
     */
    public void setIsEmail(Boolean  isEmail){
        this.isEmail = isEmail ;
        this.modify("is_email",isEmail);
    }

    /**
     * 设置 [COMPOSER_ID]
     */
    public void setComposerId(Long  composerId){
        this.composerId = composerId ;
        this.modify("composer_id",composerId);
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    public void setTemplateId(Long  templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }


}


