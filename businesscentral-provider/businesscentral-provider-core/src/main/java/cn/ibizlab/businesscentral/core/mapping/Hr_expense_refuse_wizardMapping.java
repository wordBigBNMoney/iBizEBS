package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import cn.ibizlab.businesscentral.core.dto.Hr_expense_refuse_wizardDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreHr_expense_refuse_wizardMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_expense_refuse_wizardMapping extends MappingBase<Hr_expense_refuse_wizardDTO, Hr_expense_refuse_wizard> {


}

