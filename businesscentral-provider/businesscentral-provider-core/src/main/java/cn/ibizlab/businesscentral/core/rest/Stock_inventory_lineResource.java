package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory_line;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventory_lineService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_inventory_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存明细" })
@RestController("Core-stock_inventory_line")
@RequestMapping("")
public class Stock_inventory_lineResource {

    @Autowired
    public IStock_inventory_lineService stock_inventory_lineService;

    @Autowired
    @Lazy
    public Stock_inventory_lineMapping stock_inventory_lineMapping;

    @PreAuthorize("hasPermission(this.stock_inventory_lineMapping.toDomain(#stock_inventory_linedto),'iBizBusinessCentral-Stock_inventory_line-Create')")
    @ApiOperation(value = "新建库存明细", tags = {"库存明细" },  notes = "新建库存明细")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines")
    public ResponseEntity<Stock_inventory_lineDTO> create(@Validated @RequestBody Stock_inventory_lineDTO stock_inventory_linedto) {
        Stock_inventory_line domain = stock_inventory_lineMapping.toDomain(stock_inventory_linedto);
		stock_inventory_lineService.create(domain);
        Stock_inventory_lineDTO dto = stock_inventory_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_inventory_lineMapping.toDomain(#stock_inventory_linedtos),'iBizBusinessCentral-Stock_inventory_line-Create')")
    @ApiOperation(value = "批量新建库存明细", tags = {"库存明细" },  notes = "批量新建库存明细")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_inventory_lineDTO> stock_inventory_linedtos) {
        stock_inventory_lineService.createBatch(stock_inventory_lineMapping.toDomain(stock_inventory_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_inventory_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_inventory_lineService.get(#stock_inventory_line_id),'iBizBusinessCentral-Stock_inventory_line-Update')")
    @ApiOperation(value = "更新库存明细", tags = {"库存明细" },  notes = "更新库存明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_inventory_lines/{stock_inventory_line_id}")
    public ResponseEntity<Stock_inventory_lineDTO> update(@PathVariable("stock_inventory_line_id") Long stock_inventory_line_id, @RequestBody Stock_inventory_lineDTO stock_inventory_linedto) {
		Stock_inventory_line domain  = stock_inventory_lineMapping.toDomain(stock_inventory_linedto);
        domain .setId(stock_inventory_line_id);
		stock_inventory_lineService.update(domain );
		Stock_inventory_lineDTO dto = stock_inventory_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_inventory_lineService.getStockInventoryLineByEntities(this.stock_inventory_lineMapping.toDomain(#stock_inventory_linedtos)),'iBizBusinessCentral-Stock_inventory_line-Update')")
    @ApiOperation(value = "批量更新库存明细", tags = {"库存明细" },  notes = "批量更新库存明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_inventory_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_inventory_lineDTO> stock_inventory_linedtos) {
        stock_inventory_lineService.updateBatch(stock_inventory_lineMapping.toDomain(stock_inventory_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_inventory_lineService.get(#stock_inventory_line_id),'iBizBusinessCentral-Stock_inventory_line-Remove')")
    @ApiOperation(value = "删除库存明细", tags = {"库存明细" },  notes = "删除库存明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventory_lines/{stock_inventory_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_inventory_line_id") Long stock_inventory_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_inventory_lineService.remove(stock_inventory_line_id));
    }

    @PreAuthorize("hasPermission(this.stock_inventory_lineService.getStockInventoryLineByIds(#ids),'iBizBusinessCentral-Stock_inventory_line-Remove')")
    @ApiOperation(value = "批量删除库存明细", tags = {"库存明细" },  notes = "批量删除库存明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventory_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_inventory_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_inventory_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_inventory_line-Get')")
    @ApiOperation(value = "获取库存明细", tags = {"库存明细" },  notes = "获取库存明细")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_inventory_lines/{stock_inventory_line_id}")
    public ResponseEntity<Stock_inventory_lineDTO> get(@PathVariable("stock_inventory_line_id") Long stock_inventory_line_id) {
        Stock_inventory_line domain = stock_inventory_lineService.get(stock_inventory_line_id);
        Stock_inventory_lineDTO dto = stock_inventory_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存明细草稿", tags = {"库存明细" },  notes = "获取库存明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_inventory_lines/getdraft")
    public ResponseEntity<Stock_inventory_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_inventory_lineMapping.toDto(stock_inventory_lineService.getDraft(new Stock_inventory_line())));
    }

    @ApiOperation(value = "检查库存明细", tags = {"库存明细" },  notes = "检查库存明细")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_inventory_lineDTO stock_inventory_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_inventory_lineService.checkKey(stock_inventory_lineMapping.toDomain(stock_inventory_linedto)));
    }

    @PreAuthorize("hasPermission(this.stock_inventory_lineMapping.toDomain(#stock_inventory_linedto),'iBizBusinessCentral-Stock_inventory_line-Save')")
    @ApiOperation(value = "保存库存明细", tags = {"库存明细" },  notes = "保存库存明细")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_inventory_lineDTO stock_inventory_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_inventory_lineService.save(stock_inventory_lineMapping.toDomain(stock_inventory_linedto)));
    }

    @PreAuthorize("hasPermission(this.stock_inventory_lineMapping.toDomain(#stock_inventory_linedtos),'iBizBusinessCentral-Stock_inventory_line-Save')")
    @ApiOperation(value = "批量保存库存明细", tags = {"库存明细" },  notes = "批量保存库存明细")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_inventory_lineDTO> stock_inventory_linedtos) {
        stock_inventory_lineService.saveBatch(stock_inventory_lineMapping.toDomain(stock_inventory_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_inventory_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_inventory_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存明细" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_inventory_lines/fetchdefault")
	public ResponseEntity<List<Stock_inventory_lineDTO>> fetchDefault(Stock_inventory_lineSearchContext context) {
        Page<Stock_inventory_line> domains = stock_inventory_lineService.searchDefault(context) ;
        List<Stock_inventory_lineDTO> list = stock_inventory_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_inventory_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_inventory_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存明细" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_inventory_lines/searchdefault")
	public ResponseEntity<Page<Stock_inventory_lineDTO>> searchDefault(@RequestBody Stock_inventory_lineSearchContext context) {
        Page<Stock_inventory_line> domains = stock_inventory_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_inventory_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

