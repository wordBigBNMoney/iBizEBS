package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_taxService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_taxSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"税率" })
@RestController("Core-account_tax")
@RequestMapping("")
public class Account_taxResource {

    @Autowired
    public IAccount_taxService account_taxService;

    @Autowired
    @Lazy
    public Account_taxMapping account_taxMapping;

    @PreAuthorize("hasPermission(this.account_taxMapping.toDomain(#account_taxdto),'iBizBusinessCentral-Account_tax-Create')")
    @ApiOperation(value = "新建税率", tags = {"税率" },  notes = "新建税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_taxes")
    public ResponseEntity<Account_taxDTO> create(@Validated @RequestBody Account_taxDTO account_taxdto) {
        Account_tax domain = account_taxMapping.toDomain(account_taxdto);
		account_taxService.create(domain);
        Account_taxDTO dto = account_taxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_taxMapping.toDomain(#account_taxdtos),'iBizBusinessCentral-Account_tax-Create')")
    @ApiOperation(value = "批量新建税率", tags = {"税率" },  notes = "批量新建税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_taxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_taxDTO> account_taxdtos) {
        account_taxService.createBatch(account_taxMapping.toDomain(account_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_tax" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_taxService.get(#account_tax_id),'iBizBusinessCentral-Account_tax-Update')")
    @ApiOperation(value = "更新税率", tags = {"税率" },  notes = "更新税率")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_taxes/{account_tax_id}")
    public ResponseEntity<Account_taxDTO> update(@PathVariable("account_tax_id") Long account_tax_id, @RequestBody Account_taxDTO account_taxdto) {
		Account_tax domain  = account_taxMapping.toDomain(account_taxdto);
        domain .setId(account_tax_id);
		account_taxService.update(domain );
		Account_taxDTO dto = account_taxMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_taxService.getAccountTaxByEntities(this.account_taxMapping.toDomain(#account_taxdtos)),'iBizBusinessCentral-Account_tax-Update')")
    @ApiOperation(value = "批量更新税率", tags = {"税率" },  notes = "批量更新税率")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_taxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_taxDTO> account_taxdtos) {
        account_taxService.updateBatch(account_taxMapping.toDomain(account_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_taxService.get(#account_tax_id),'iBizBusinessCentral-Account_tax-Remove')")
    @ApiOperation(value = "删除税率", tags = {"税率" },  notes = "删除税率")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_taxes/{account_tax_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_tax_id") Long account_tax_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_taxService.remove(account_tax_id));
    }

    @PreAuthorize("hasPermission(this.account_taxService.getAccountTaxByIds(#ids),'iBizBusinessCentral-Account_tax-Remove')")
    @ApiOperation(value = "批量删除税率", tags = {"税率" },  notes = "批量删除税率")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_taxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_taxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_taxMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_tax-Get')")
    @ApiOperation(value = "获取税率", tags = {"税率" },  notes = "获取税率")
	@RequestMapping(method = RequestMethod.GET, value = "/account_taxes/{account_tax_id}")
    public ResponseEntity<Account_taxDTO> get(@PathVariable("account_tax_id") Long account_tax_id) {
        Account_tax domain = account_taxService.get(account_tax_id);
        Account_taxDTO dto = account_taxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取税率草稿", tags = {"税率" },  notes = "获取税率草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_taxes/getdraft")
    public ResponseEntity<Account_taxDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_taxMapping.toDto(account_taxService.getDraft(new Account_tax())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax-Calc_tax-all')")
    @ApiOperation(value = "calc_tax", tags = {"税率" },  notes = "calc_tax")
	@RequestMapping(method = RequestMethod.POST, value = "/account_taxes/{account_tax_id}/calc_tax")
    public ResponseEntity<Account_taxDTO> calc_tax(@PathVariable("account_tax_id") Long account_tax_id, @RequestBody Account_taxDTO account_taxdto) {
        Account_tax domain = account_taxMapping.toDomain(account_taxdto);
        domain.setId(account_tax_id);
        domain = account_taxService.calc_tax(domain);
        account_taxdto = account_taxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(account_taxdto);
    }

    @ApiOperation(value = "检查税率", tags = {"税率" },  notes = "检查税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_taxes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_taxDTO account_taxdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_taxService.checkKey(account_taxMapping.toDomain(account_taxdto)));
    }

    @PreAuthorize("hasPermission(this.account_taxMapping.toDomain(#account_taxdto),'iBizBusinessCentral-Account_tax-Save')")
    @ApiOperation(value = "保存税率", tags = {"税率" },  notes = "保存税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_taxes/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_taxDTO account_taxdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_taxService.save(account_taxMapping.toDomain(account_taxdto)));
    }

    @PreAuthorize("hasPermission(this.account_taxMapping.toDomain(#account_taxdtos),'iBizBusinessCentral-Account_tax-Save')")
    @ApiOperation(value = "批量保存税率", tags = {"税率" },  notes = "批量保存税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_taxes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_taxDTO> account_taxdtos) {
        account_taxService.saveBatch(account_taxMapping.toDomain(account_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_tax-Get')")
	@ApiOperation(value = "获取数据集", tags = {"税率" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_taxes/fetchdefault")
	public ResponseEntity<List<Account_taxDTO>> fetchDefault(Account_taxSearchContext context) {
        Page<Account_tax> domains = account_taxService.searchDefault(context) ;
        List<Account_taxDTO> list = account_taxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_tax-Get')")
	@ApiOperation(value = "查询数据集", tags = {"税率" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_taxes/searchdefault")
	public ResponseEntity<Page<Account_taxDTO>> searchDefault(@RequestBody Account_taxSearchContext context) {
        Page<Account_tax> domains = account_taxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_taxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax-searchPurchase-all') and hasPermission(#context,'iBizBusinessCentral-Account_tax-Get')")
	@ApiOperation(value = "获取采购税率", tags = {"税率" } ,notes = "获取采购税率")
    @RequestMapping(method= RequestMethod.GET , value="/account_taxes/fetchpurchase")
	public ResponseEntity<List<Account_taxDTO>> fetchPurchase(Account_taxSearchContext context) {
        Page<Account_tax> domains = account_taxService.searchPurchase(context) ;
        List<Account_taxDTO> list = account_taxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax-searchPurchase-all') and hasPermission(#context,'iBizBusinessCentral-Account_tax-Get')")
	@ApiOperation(value = "查询采购税率", tags = {"税率" } ,notes = "查询采购税率")
    @RequestMapping(method= RequestMethod.POST , value="/account_taxes/searchpurchase")
	public ResponseEntity<Page<Account_taxDTO>> searchPurchase(@RequestBody Account_taxSearchContext context) {
        Page<Account_tax> domains = account_taxService.searchPurchase(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_taxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax-searchSale-all') and hasPermission(#context,'iBizBusinessCentral-Account_tax-Get')")
	@ApiOperation(value = "获取销售税率", tags = {"税率" } ,notes = "获取销售税率")
    @RequestMapping(method= RequestMethod.GET , value="/account_taxes/fetchsale")
	public ResponseEntity<List<Account_taxDTO>> fetchSale(Account_taxSearchContext context) {
        Page<Account_tax> domains = account_taxService.searchSale(context) ;
        List<Account_taxDTO> list = account_taxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax-searchSale-all') and hasPermission(#context,'iBizBusinessCentral-Account_tax-Get')")
	@ApiOperation(value = "查询销售税率", tags = {"税率" } ,notes = "查询销售税率")
    @RequestMapping(method= RequestMethod.POST , value="/account_taxes/searchsale")
	public ResponseEntity<Page<Account_taxDTO>> searchSale(@RequestBody Account_taxSearchContext context) {
        Page<Account_tax> domains = account_taxService.searchSale(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_taxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

