package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_setup_bank_manual_configService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"银行设置通用配置" })
@RestController("Core-account_setup_bank_manual_config")
@RequestMapping("")
public class Account_setup_bank_manual_configResource {

    @Autowired
    public IAccount_setup_bank_manual_configService account_setup_bank_manual_configService;

    @Autowired
    @Lazy
    public Account_setup_bank_manual_configMapping account_setup_bank_manual_configMapping;

    @PreAuthorize("hasPermission(this.account_setup_bank_manual_configMapping.toDomain(#account_setup_bank_manual_configdto),'iBizBusinessCentral-Account_setup_bank_manual_config-Create')")
    @ApiOperation(value = "新建银行设置通用配置", tags = {"银行设置通用配置" },  notes = "新建银行设置通用配置")
	@RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs")
    public ResponseEntity<Account_setup_bank_manual_configDTO> create(@Validated @RequestBody Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto) {
        Account_setup_bank_manual_config domain = account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdto);
		account_setup_bank_manual_configService.create(domain);
        Account_setup_bank_manual_configDTO dto = account_setup_bank_manual_configMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_setup_bank_manual_configMapping.toDomain(#account_setup_bank_manual_configdtos),'iBizBusinessCentral-Account_setup_bank_manual_config-Create')")
    @ApiOperation(value = "批量新建银行设置通用配置", tags = {"银行设置通用配置" },  notes = "批量新建银行设置通用配置")
	@RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_setup_bank_manual_configDTO> account_setup_bank_manual_configdtos) {
        account_setup_bank_manual_configService.createBatch(account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_setup_bank_manual_config" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_setup_bank_manual_configService.get(#account_setup_bank_manual_config_id),'iBizBusinessCentral-Account_setup_bank_manual_config-Update')")
    @ApiOperation(value = "更新银行设置通用配置", tags = {"银行设置通用配置" },  notes = "更新银行设置通用配置")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")
    public ResponseEntity<Account_setup_bank_manual_configDTO> update(@PathVariable("account_setup_bank_manual_config_id") Long account_setup_bank_manual_config_id, @RequestBody Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto) {
		Account_setup_bank_manual_config domain  = account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdto);
        domain .setId(account_setup_bank_manual_config_id);
		account_setup_bank_manual_configService.update(domain );
		Account_setup_bank_manual_configDTO dto = account_setup_bank_manual_configMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_setup_bank_manual_configService.getAccountSetupBankManualConfigByEntities(this.account_setup_bank_manual_configMapping.toDomain(#account_setup_bank_manual_configdtos)),'iBizBusinessCentral-Account_setup_bank_manual_config-Update')")
    @ApiOperation(value = "批量更新银行设置通用配置", tags = {"银行设置通用配置" },  notes = "批量更新银行设置通用配置")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_setup_bank_manual_configs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_setup_bank_manual_configDTO> account_setup_bank_manual_configdtos) {
        account_setup_bank_manual_configService.updateBatch(account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_setup_bank_manual_configService.get(#account_setup_bank_manual_config_id),'iBizBusinessCentral-Account_setup_bank_manual_config-Remove')")
    @ApiOperation(value = "删除银行设置通用配置", tags = {"银行设置通用配置" },  notes = "删除银行设置通用配置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_setup_bank_manual_config_id") Long account_setup_bank_manual_config_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_setup_bank_manual_configService.remove(account_setup_bank_manual_config_id));
    }

    @PreAuthorize("hasPermission(this.account_setup_bank_manual_configService.getAccountSetupBankManualConfigByIds(#ids),'iBizBusinessCentral-Account_setup_bank_manual_config-Remove')")
    @ApiOperation(value = "批量删除银行设置通用配置", tags = {"银行设置通用配置" },  notes = "批量删除银行设置通用配置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_setup_bank_manual_configs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_setup_bank_manual_configService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_setup_bank_manual_configMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_setup_bank_manual_config-Get')")
    @ApiOperation(value = "获取银行设置通用配置", tags = {"银行设置通用配置" },  notes = "获取银行设置通用配置")
	@RequestMapping(method = RequestMethod.GET, value = "/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")
    public ResponseEntity<Account_setup_bank_manual_configDTO> get(@PathVariable("account_setup_bank_manual_config_id") Long account_setup_bank_manual_config_id) {
        Account_setup_bank_manual_config domain = account_setup_bank_manual_configService.get(account_setup_bank_manual_config_id);
        Account_setup_bank_manual_configDTO dto = account_setup_bank_manual_configMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取银行设置通用配置草稿", tags = {"银行设置通用配置" },  notes = "获取银行设置通用配置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_setup_bank_manual_configs/getdraft")
    public ResponseEntity<Account_setup_bank_manual_configDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_setup_bank_manual_configMapping.toDto(account_setup_bank_manual_configService.getDraft(new Account_setup_bank_manual_config())));
    }

    @ApiOperation(value = "检查银行设置通用配置", tags = {"银行设置通用配置" },  notes = "检查银行设置通用配置")
	@RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_setup_bank_manual_configService.checkKey(account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdto)));
    }

    @PreAuthorize("hasPermission(this.account_setup_bank_manual_configMapping.toDomain(#account_setup_bank_manual_configdto),'iBizBusinessCentral-Account_setup_bank_manual_config-Save')")
    @ApiOperation(value = "保存银行设置通用配置", tags = {"银行设置通用配置" },  notes = "保存银行设置通用配置")
	@RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_setup_bank_manual_configService.save(account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdto)));
    }

    @PreAuthorize("hasPermission(this.account_setup_bank_manual_configMapping.toDomain(#account_setup_bank_manual_configdtos),'iBizBusinessCentral-Account_setup_bank_manual_config-Save')")
    @ApiOperation(value = "批量保存银行设置通用配置", tags = {"银行设置通用配置" },  notes = "批量保存银行设置通用配置")
	@RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_setup_bank_manual_configDTO> account_setup_bank_manual_configdtos) {
        account_setup_bank_manual_configService.saveBatch(account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_setup_bank_manual_config-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_setup_bank_manual_config-Get')")
	@ApiOperation(value = "获取数据集", tags = {"银行设置通用配置" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_setup_bank_manual_configs/fetchdefault")
	public ResponseEntity<List<Account_setup_bank_manual_configDTO>> fetchDefault(Account_setup_bank_manual_configSearchContext context) {
        Page<Account_setup_bank_manual_config> domains = account_setup_bank_manual_configService.searchDefault(context) ;
        List<Account_setup_bank_manual_configDTO> list = account_setup_bank_manual_configMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_setup_bank_manual_config-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_setup_bank_manual_config-Get')")
	@ApiOperation(value = "查询数据集", tags = {"银行设置通用配置" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_setup_bank_manual_configs/searchdefault")
	public ResponseEntity<Page<Account_setup_bank_manual_configDTO>> searchDefault(@RequestBody Account_setup_bank_manual_configSearchContext context) {
        Page<Account_setup_bank_manual_config> domains = account_setup_bank_manual_configService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_setup_bank_manual_configMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

