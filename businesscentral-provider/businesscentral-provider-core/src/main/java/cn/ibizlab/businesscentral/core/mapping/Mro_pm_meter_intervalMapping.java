package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.businesscentral.core.dto.Mro_pm_meter_intervalDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMro_pm_meter_intervalMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mro_pm_meter_intervalMapping extends MappingBase<Mro_pm_meter_intervalDTO, Mro_pm_meter_interval> {


}

