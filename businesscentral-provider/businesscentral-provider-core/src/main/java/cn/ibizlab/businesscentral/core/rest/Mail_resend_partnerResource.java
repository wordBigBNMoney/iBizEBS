package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_resend_partner;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_resend_partnerService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_resend_partnerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"业务伙伴需要额外信息重发EMail" })
@RestController("Core-mail_resend_partner")
@RequestMapping("")
public class Mail_resend_partnerResource {

    @Autowired
    public IMail_resend_partnerService mail_resend_partnerService;

    @Autowired
    @Lazy
    public Mail_resend_partnerMapping mail_resend_partnerMapping;

    @PreAuthorize("hasPermission(this.mail_resend_partnerMapping.toDomain(#mail_resend_partnerdto),'iBizBusinessCentral-Mail_resend_partner-Create')")
    @ApiOperation(value = "新建业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "新建业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_partners")
    public ResponseEntity<Mail_resend_partnerDTO> create(@Validated @RequestBody Mail_resend_partnerDTO mail_resend_partnerdto) {
        Mail_resend_partner domain = mail_resend_partnerMapping.toDomain(mail_resend_partnerdto);
		mail_resend_partnerService.create(domain);
        Mail_resend_partnerDTO dto = mail_resend_partnerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_resend_partnerMapping.toDomain(#mail_resend_partnerdtos),'iBizBusinessCentral-Mail_resend_partner-Create')")
    @ApiOperation(value = "批量新建业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "批量新建业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_partners/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_resend_partnerDTO> mail_resend_partnerdtos) {
        mail_resend_partnerService.createBatch(mail_resend_partnerMapping.toDomain(mail_resend_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_resend_partner" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_resend_partnerService.get(#mail_resend_partner_id),'iBizBusinessCentral-Mail_resend_partner-Update')")
    @ApiOperation(value = "更新业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "更新业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_partners/{mail_resend_partner_id}")
    public ResponseEntity<Mail_resend_partnerDTO> update(@PathVariable("mail_resend_partner_id") Long mail_resend_partner_id, @RequestBody Mail_resend_partnerDTO mail_resend_partnerdto) {
		Mail_resend_partner domain  = mail_resend_partnerMapping.toDomain(mail_resend_partnerdto);
        domain .setId(mail_resend_partner_id);
		mail_resend_partnerService.update(domain );
		Mail_resend_partnerDTO dto = mail_resend_partnerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_resend_partnerService.getMailResendPartnerByEntities(this.mail_resend_partnerMapping.toDomain(#mail_resend_partnerdtos)),'iBizBusinessCentral-Mail_resend_partner-Update')")
    @ApiOperation(value = "批量更新业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "批量更新业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_partners/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_resend_partnerDTO> mail_resend_partnerdtos) {
        mail_resend_partnerService.updateBatch(mail_resend_partnerMapping.toDomain(mail_resend_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_resend_partnerService.get(#mail_resend_partner_id),'iBizBusinessCentral-Mail_resend_partner-Remove')")
    @ApiOperation(value = "删除业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "删除业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_partners/{mail_resend_partner_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_resend_partner_id") Long mail_resend_partner_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_resend_partnerService.remove(mail_resend_partner_id));
    }

    @PreAuthorize("hasPermission(this.mail_resend_partnerService.getMailResendPartnerByIds(#ids),'iBizBusinessCentral-Mail_resend_partner-Remove')")
    @ApiOperation(value = "批量删除业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "批量删除业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_partners/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_resend_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_resend_partnerMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_resend_partner-Get')")
    @ApiOperation(value = "获取业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "获取业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_resend_partners/{mail_resend_partner_id}")
    public ResponseEntity<Mail_resend_partnerDTO> get(@PathVariable("mail_resend_partner_id") Long mail_resend_partner_id) {
        Mail_resend_partner domain = mail_resend_partnerService.get(mail_resend_partner_id);
        Mail_resend_partnerDTO dto = mail_resend_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取业务伙伴需要额外信息重发EMail草稿", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "获取业务伙伴需要额外信息重发EMail草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_resend_partners/getdraft")
    public ResponseEntity<Mail_resend_partnerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_resend_partnerMapping.toDto(mail_resend_partnerService.getDraft(new Mail_resend_partner())));
    }

    @ApiOperation(value = "检查业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "检查业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_partners/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_resend_partnerDTO mail_resend_partnerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_resend_partnerService.checkKey(mail_resend_partnerMapping.toDomain(mail_resend_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.mail_resend_partnerMapping.toDomain(#mail_resend_partnerdto),'iBizBusinessCentral-Mail_resend_partner-Save')")
    @ApiOperation(value = "保存业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "保存业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_partners/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_resend_partnerDTO mail_resend_partnerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_resend_partnerService.save(mail_resend_partnerMapping.toDomain(mail_resend_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.mail_resend_partnerMapping.toDomain(#mail_resend_partnerdtos),'iBizBusinessCentral-Mail_resend_partner-Save')")
    @ApiOperation(value = "批量保存业务伙伴需要额外信息重发EMail", tags = {"业务伙伴需要额外信息重发EMail" },  notes = "批量保存业务伙伴需要额外信息重发EMail")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_partners/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_resend_partnerDTO> mail_resend_partnerdtos) {
        mail_resend_partnerService.saveBatch(mail_resend_partnerMapping.toDomain(mail_resend_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_resend_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_resend_partner-Get')")
	@ApiOperation(value = "获取数据集", tags = {"业务伙伴需要额外信息重发EMail" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_resend_partners/fetchdefault")
	public ResponseEntity<List<Mail_resend_partnerDTO>> fetchDefault(Mail_resend_partnerSearchContext context) {
        Page<Mail_resend_partner> domains = mail_resend_partnerService.searchDefault(context) ;
        List<Mail_resend_partnerDTO> list = mail_resend_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_resend_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_resend_partner-Get')")
	@ApiOperation(value = "查询数据集", tags = {"业务伙伴需要额外信息重发EMail" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_resend_partners/searchdefault")
	public ResponseEntity<Page<Mail_resend_partnerDTO>> searchDefault(@RequestBody Mail_resend_partnerSearchContext context) {
        Page<Mail_resend_partner> domains = mail_resend_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_resend_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

