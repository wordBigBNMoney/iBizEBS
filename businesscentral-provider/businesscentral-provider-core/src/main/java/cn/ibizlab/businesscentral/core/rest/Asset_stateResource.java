package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_state;
import cn.ibizlab.businesscentral.core.odoo_asset.service.IAsset_stateService;
import cn.ibizlab.businesscentral.core.odoo_asset.filter.Asset_stateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"State of Asset" })
@RestController("Core-asset_state")
@RequestMapping("")
public class Asset_stateResource {

    @Autowired
    public IAsset_stateService asset_stateService;

    @Autowired
    @Lazy
    public Asset_stateMapping asset_stateMapping;

    @PreAuthorize("hasPermission(this.asset_stateMapping.toDomain(#asset_statedto),'iBizBusinessCentral-Asset_state-Create')")
    @ApiOperation(value = "新建State of Asset", tags = {"State of Asset" },  notes = "新建State of Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_states")
    public ResponseEntity<Asset_stateDTO> create(@Validated @RequestBody Asset_stateDTO asset_statedto) {
        Asset_state domain = asset_stateMapping.toDomain(asset_statedto);
		asset_stateService.create(domain);
        Asset_stateDTO dto = asset_stateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.asset_stateMapping.toDomain(#asset_statedtos),'iBizBusinessCentral-Asset_state-Create')")
    @ApiOperation(value = "批量新建State of Asset", tags = {"State of Asset" },  notes = "批量新建State of Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_states/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Asset_stateDTO> asset_statedtos) {
        asset_stateService.createBatch(asset_stateMapping.toDomain(asset_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "asset_state" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.asset_stateService.get(#asset_state_id),'iBizBusinessCentral-Asset_state-Update')")
    @ApiOperation(value = "更新State of Asset", tags = {"State of Asset" },  notes = "更新State of Asset")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_states/{asset_state_id}")
    public ResponseEntity<Asset_stateDTO> update(@PathVariable("asset_state_id") Long asset_state_id, @RequestBody Asset_stateDTO asset_statedto) {
		Asset_state domain  = asset_stateMapping.toDomain(asset_statedto);
        domain .setId(asset_state_id);
		asset_stateService.update(domain );
		Asset_stateDTO dto = asset_stateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.asset_stateService.getAssetStateByEntities(this.asset_stateMapping.toDomain(#asset_statedtos)),'iBizBusinessCentral-Asset_state-Update')")
    @ApiOperation(value = "批量更新State of Asset", tags = {"State of Asset" },  notes = "批量更新State of Asset")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_states/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_stateDTO> asset_statedtos) {
        asset_stateService.updateBatch(asset_stateMapping.toDomain(asset_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.asset_stateService.get(#asset_state_id),'iBizBusinessCentral-Asset_state-Remove')")
    @ApiOperation(value = "删除State of Asset", tags = {"State of Asset" },  notes = "删除State of Asset")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_states/{asset_state_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("asset_state_id") Long asset_state_id) {
         return ResponseEntity.status(HttpStatus.OK).body(asset_stateService.remove(asset_state_id));
    }

    @PreAuthorize("hasPermission(this.asset_stateService.getAssetStateByIds(#ids),'iBizBusinessCentral-Asset_state-Remove')")
    @ApiOperation(value = "批量删除State of Asset", tags = {"State of Asset" },  notes = "批量删除State of Asset")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_states/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        asset_stateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.asset_stateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Asset_state-Get')")
    @ApiOperation(value = "获取State of Asset", tags = {"State of Asset" },  notes = "获取State of Asset")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_states/{asset_state_id}")
    public ResponseEntity<Asset_stateDTO> get(@PathVariable("asset_state_id") Long asset_state_id) {
        Asset_state domain = asset_stateService.get(asset_state_id);
        Asset_stateDTO dto = asset_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取State of Asset草稿", tags = {"State of Asset" },  notes = "获取State of Asset草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_states/getdraft")
    public ResponseEntity<Asset_stateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(asset_stateMapping.toDto(asset_stateService.getDraft(new Asset_state())));
    }

    @ApiOperation(value = "检查State of Asset", tags = {"State of Asset" },  notes = "检查State of Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_states/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Asset_stateDTO asset_statedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(asset_stateService.checkKey(asset_stateMapping.toDomain(asset_statedto)));
    }

    @PreAuthorize("hasPermission(this.asset_stateMapping.toDomain(#asset_statedto),'iBizBusinessCentral-Asset_state-Save')")
    @ApiOperation(value = "保存State of Asset", tags = {"State of Asset" },  notes = "保存State of Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_states/save")
    public ResponseEntity<Boolean> save(@RequestBody Asset_stateDTO asset_statedto) {
        return ResponseEntity.status(HttpStatus.OK).body(asset_stateService.save(asset_stateMapping.toDomain(asset_statedto)));
    }

    @PreAuthorize("hasPermission(this.asset_stateMapping.toDomain(#asset_statedtos),'iBizBusinessCentral-Asset_state-Save')")
    @ApiOperation(value = "批量保存State of Asset", tags = {"State of Asset" },  notes = "批量保存State of Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_states/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Asset_stateDTO> asset_statedtos) {
        asset_stateService.saveBatch(asset_stateMapping.toDomain(asset_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Asset_state-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Asset_state-Get')")
	@ApiOperation(value = "获取数据集", tags = {"State of Asset" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/asset_states/fetchdefault")
	public ResponseEntity<List<Asset_stateDTO>> fetchDefault(Asset_stateSearchContext context) {
        Page<Asset_state> domains = asset_stateService.searchDefault(context) ;
        List<Asset_stateDTO> list = asset_stateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Asset_state-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Asset_state-Get')")
	@ApiOperation(value = "查询数据集", tags = {"State of Asset" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/asset_states/searchdefault")
	public ResponseEntity<Page<Asset_stateDTO>> searchDefault(@RequestBody Asset_stateSearchContext context) {
        Page<Asset_state> domains = asset_stateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(asset_stateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

