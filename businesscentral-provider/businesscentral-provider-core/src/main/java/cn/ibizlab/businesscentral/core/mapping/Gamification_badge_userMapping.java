package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.businesscentral.core.dto.Gamification_badge_userDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreGamification_badge_userMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Gamification_badge_userMapping extends MappingBase<Gamification_badge_userDTO, Gamification_badge_user> {


}

