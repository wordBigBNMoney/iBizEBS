package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_accountSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"分析账户" })
@RestController("Core-account_analytic_account")
@RequestMapping("")
public class Account_analytic_accountResource {

    @Autowired
    public IAccount_analytic_accountService account_analytic_accountService;

    @Autowired
    @Lazy
    public Account_analytic_accountMapping account_analytic_accountMapping;

    @PreAuthorize("hasPermission(this.account_analytic_accountMapping.toDomain(#account_analytic_accountdto),'iBizBusinessCentral-Account_analytic_account-Create')")
    @ApiOperation(value = "新建分析账户", tags = {"分析账户" },  notes = "新建分析账户")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts")
    public ResponseEntity<Account_analytic_accountDTO> create(@Validated @RequestBody Account_analytic_accountDTO account_analytic_accountdto) {
        Account_analytic_account domain = account_analytic_accountMapping.toDomain(account_analytic_accountdto);
		account_analytic_accountService.create(domain);
        Account_analytic_accountDTO dto = account_analytic_accountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_accountMapping.toDomain(#account_analytic_accountdtos),'iBizBusinessCentral-Account_analytic_account-Create')")
    @ApiOperation(value = "批量新建分析账户", tags = {"分析账户" },  notes = "批量新建分析账户")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_analytic_accountDTO> account_analytic_accountdtos) {
        account_analytic_accountService.createBatch(account_analytic_accountMapping.toDomain(account_analytic_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_analytic_account" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_analytic_accountService.get(#account_analytic_account_id),'iBizBusinessCentral-Account_analytic_account-Update')")
    @ApiOperation(value = "更新分析账户", tags = {"分析账户" },  notes = "更新分析账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_accounts/{account_analytic_account_id}")
    public ResponseEntity<Account_analytic_accountDTO> update(@PathVariable("account_analytic_account_id") Long account_analytic_account_id, @RequestBody Account_analytic_accountDTO account_analytic_accountdto) {
		Account_analytic_account domain  = account_analytic_accountMapping.toDomain(account_analytic_accountdto);
        domain .setId(account_analytic_account_id);
		account_analytic_accountService.update(domain );
		Account_analytic_accountDTO dto = account_analytic_accountMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_accountService.getAccountAnalyticAccountByEntities(this.account_analytic_accountMapping.toDomain(#account_analytic_accountdtos)),'iBizBusinessCentral-Account_analytic_account-Update')")
    @ApiOperation(value = "批量更新分析账户", tags = {"分析账户" },  notes = "批量更新分析账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_accounts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_accountDTO> account_analytic_accountdtos) {
        account_analytic_accountService.updateBatch(account_analytic_accountMapping.toDomain(account_analytic_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_analytic_accountService.get(#account_analytic_account_id),'iBizBusinessCentral-Account_analytic_account-Remove')")
    @ApiOperation(value = "删除分析账户", tags = {"分析账户" },  notes = "删除分析账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_accounts/{account_analytic_account_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_account_id") Long account_analytic_account_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_analytic_accountService.remove(account_analytic_account_id));
    }

    @PreAuthorize("hasPermission(this.account_analytic_accountService.getAccountAnalyticAccountByIds(#ids),'iBizBusinessCentral-Account_analytic_account-Remove')")
    @ApiOperation(value = "批量删除分析账户", tags = {"分析账户" },  notes = "批量删除分析账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_accounts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_analytic_accountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_analytic_accountMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_analytic_account-Get')")
    @ApiOperation(value = "获取分析账户", tags = {"分析账户" },  notes = "获取分析账户")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_accounts/{account_analytic_account_id}")
    public ResponseEntity<Account_analytic_accountDTO> get(@PathVariable("account_analytic_account_id") Long account_analytic_account_id) {
        Account_analytic_account domain = account_analytic_accountService.get(account_analytic_account_id);
        Account_analytic_accountDTO dto = account_analytic_accountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取分析账户草稿", tags = {"分析账户" },  notes = "获取分析账户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_accounts/getdraft")
    public ResponseEntity<Account_analytic_accountDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_accountMapping.toDto(account_analytic_accountService.getDraft(new Account_analytic_account())));
    }

    @ApiOperation(value = "检查分析账户", tags = {"分析账户" },  notes = "检查分析账户")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_analytic_accountDTO account_analytic_accountdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_analytic_accountService.checkKey(account_analytic_accountMapping.toDomain(account_analytic_accountdto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_accountMapping.toDomain(#account_analytic_accountdto),'iBizBusinessCentral-Account_analytic_account-Save')")
    @ApiOperation(value = "保存分析账户", tags = {"分析账户" },  notes = "保存分析账户")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_analytic_accountDTO account_analytic_accountdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_accountService.save(account_analytic_accountMapping.toDomain(account_analytic_accountdto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_accountMapping.toDomain(#account_analytic_accountdtos),'iBizBusinessCentral-Account_analytic_account-Save')")
    @ApiOperation(value = "批量保存分析账户", tags = {"分析账户" },  notes = "批量保存分析账户")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_analytic_accountDTO> account_analytic_accountdtos) {
        account_analytic_accountService.saveBatch(account_analytic_accountMapping.toDomain(account_analytic_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_account-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_account-Get')")
	@ApiOperation(value = "获取数据集", tags = {"分析账户" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_accounts/fetchdefault")
	public ResponseEntity<List<Account_analytic_accountDTO>> fetchDefault(Account_analytic_accountSearchContext context) {
        Page<Account_analytic_account> domains = account_analytic_accountService.searchDefault(context) ;
        List<Account_analytic_accountDTO> list = account_analytic_accountMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_account-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_account-Get')")
	@ApiOperation(value = "查询数据集", tags = {"分析账户" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_analytic_accounts/searchdefault")
	public ResponseEntity<Page<Account_analytic_accountDTO>> searchDefault(@RequestBody Account_analytic_accountSearchContext context) {
        Page<Account_analytic_account> domains = account_analytic_accountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_analytic_accountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

