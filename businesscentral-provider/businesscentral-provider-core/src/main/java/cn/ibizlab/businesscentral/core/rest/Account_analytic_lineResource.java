package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_lineService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"分析行" })
@RestController("Core-account_analytic_line")
@RequestMapping("")
public class Account_analytic_lineResource {

    @Autowired
    public IAccount_analytic_lineService account_analytic_lineService;

    @Autowired
    @Lazy
    public Account_analytic_lineMapping account_analytic_lineMapping;

    @PreAuthorize("hasPermission(this.account_analytic_lineMapping.toDomain(#account_analytic_linedto),'iBizBusinessCentral-Account_analytic_line-Create')")
    @ApiOperation(value = "新建分析行", tags = {"分析行" },  notes = "新建分析行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines")
    public ResponseEntity<Account_analytic_lineDTO> create(@Validated @RequestBody Account_analytic_lineDTO account_analytic_linedto) {
        Account_analytic_line domain = account_analytic_lineMapping.toDomain(account_analytic_linedto);
		account_analytic_lineService.create(domain);
        Account_analytic_lineDTO dto = account_analytic_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_lineMapping.toDomain(#account_analytic_linedtos),'iBizBusinessCentral-Account_analytic_line-Create')")
    @ApiOperation(value = "批量新建分析行", tags = {"分析行" },  notes = "批量新建分析行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_analytic_lineDTO> account_analytic_linedtos) {
        account_analytic_lineService.createBatch(account_analytic_lineMapping.toDomain(account_analytic_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_analytic_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_analytic_lineService.get(#account_analytic_line_id),'iBizBusinessCentral-Account_analytic_line-Update')")
    @ApiOperation(value = "更新分析行", tags = {"分析行" },  notes = "更新分析行")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_lines/{account_analytic_line_id}")
    public ResponseEntity<Account_analytic_lineDTO> update(@PathVariable("account_analytic_line_id") Long account_analytic_line_id, @RequestBody Account_analytic_lineDTO account_analytic_linedto) {
		Account_analytic_line domain  = account_analytic_lineMapping.toDomain(account_analytic_linedto);
        domain .setId(account_analytic_line_id);
		account_analytic_lineService.update(domain );
		Account_analytic_lineDTO dto = account_analytic_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_lineService.getAccountAnalyticLineByEntities(this.account_analytic_lineMapping.toDomain(#account_analytic_linedtos)),'iBizBusinessCentral-Account_analytic_line-Update')")
    @ApiOperation(value = "批量更新分析行", tags = {"分析行" },  notes = "批量更新分析行")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_lineDTO> account_analytic_linedtos) {
        account_analytic_lineService.updateBatch(account_analytic_lineMapping.toDomain(account_analytic_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_analytic_lineService.get(#account_analytic_line_id),'iBizBusinessCentral-Account_analytic_line-Remove')")
    @ApiOperation(value = "删除分析行", tags = {"分析行" },  notes = "删除分析行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_lines/{account_analytic_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_line_id") Long account_analytic_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_analytic_lineService.remove(account_analytic_line_id));
    }

    @PreAuthorize("hasPermission(this.account_analytic_lineService.getAccountAnalyticLineByIds(#ids),'iBizBusinessCentral-Account_analytic_line-Remove')")
    @ApiOperation(value = "批量删除分析行", tags = {"分析行" },  notes = "批量删除分析行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_analytic_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_analytic_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_analytic_line-Get')")
    @ApiOperation(value = "获取分析行", tags = {"分析行" },  notes = "获取分析行")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_lines/{account_analytic_line_id}")
    public ResponseEntity<Account_analytic_lineDTO> get(@PathVariable("account_analytic_line_id") Long account_analytic_line_id) {
        Account_analytic_line domain = account_analytic_lineService.get(account_analytic_line_id);
        Account_analytic_lineDTO dto = account_analytic_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取分析行草稿", tags = {"分析行" },  notes = "获取分析行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_lines/getdraft")
    public ResponseEntity<Account_analytic_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_lineMapping.toDto(account_analytic_lineService.getDraft(new Account_analytic_line())));
    }

    @ApiOperation(value = "检查分析行", tags = {"分析行" },  notes = "检查分析行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_analytic_lineDTO account_analytic_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_analytic_lineService.checkKey(account_analytic_lineMapping.toDomain(account_analytic_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_lineMapping.toDomain(#account_analytic_linedto),'iBizBusinessCentral-Account_analytic_line-Save')")
    @ApiOperation(value = "保存分析行", tags = {"分析行" },  notes = "保存分析行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_analytic_lineDTO account_analytic_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_lineService.save(account_analytic_lineMapping.toDomain(account_analytic_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_lineMapping.toDomain(#account_analytic_linedtos),'iBizBusinessCentral-Account_analytic_line-Save')")
    @ApiOperation(value = "批量保存分析行", tags = {"分析行" },  notes = "批量保存分析行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_analytic_lineDTO> account_analytic_linedtos) {
        account_analytic_lineService.saveBatch(account_analytic_lineMapping.toDomain(account_analytic_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"分析行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_lines/fetchdefault")
	public ResponseEntity<List<Account_analytic_lineDTO>> fetchDefault(Account_analytic_lineSearchContext context) {
        Page<Account_analytic_line> domains = account_analytic_lineService.searchDefault(context) ;
        List<Account_analytic_lineDTO> list = account_analytic_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"分析行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_analytic_lines/searchdefault")
	public ResponseEntity<Page<Account_analytic_lineDTO>> searchDefault(@RequestBody Account_analytic_lineSearchContext context) {
        Page<Account_analytic_line> domains = account_analytic_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_analytic_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

