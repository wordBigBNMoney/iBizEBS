package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.businesscentral.core.dto.Hr_leave_allocationDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreHr_leave_allocationMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_leave_allocationMapping extends MappingBase<Hr_leave_allocationDTO, Hr_leave_allocation> {


}

