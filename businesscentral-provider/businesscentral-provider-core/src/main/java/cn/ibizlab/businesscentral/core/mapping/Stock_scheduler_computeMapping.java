package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.businesscentral.core.dto.Stock_scheduler_computeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreStock_scheduler_computeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_scheduler_computeMapping extends MappingBase<Stock_scheduler_computeDTO, Stock_scheduler_compute> {


}

