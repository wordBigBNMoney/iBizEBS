package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_traceability_report;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_traceability_reportService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_traceability_reportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"追溯报告" })
@RestController("Core-stock_traceability_report")
@RequestMapping("")
public class Stock_traceability_reportResource {

    @Autowired
    public IStock_traceability_reportService stock_traceability_reportService;

    @Autowired
    @Lazy
    public Stock_traceability_reportMapping stock_traceability_reportMapping;

    @PreAuthorize("hasPermission(this.stock_traceability_reportMapping.toDomain(#stock_traceability_reportdto),'iBizBusinessCentral-Stock_traceability_report-Create')")
    @ApiOperation(value = "新建追溯报告", tags = {"追溯报告" },  notes = "新建追溯报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_traceability_reports")
    public ResponseEntity<Stock_traceability_reportDTO> create(@Validated @RequestBody Stock_traceability_reportDTO stock_traceability_reportdto) {
        Stock_traceability_report domain = stock_traceability_reportMapping.toDomain(stock_traceability_reportdto);
		stock_traceability_reportService.create(domain);
        Stock_traceability_reportDTO dto = stock_traceability_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_traceability_reportMapping.toDomain(#stock_traceability_reportdtos),'iBizBusinessCentral-Stock_traceability_report-Create')")
    @ApiOperation(value = "批量新建追溯报告", tags = {"追溯报告" },  notes = "批量新建追溯报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_traceability_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_traceability_reportDTO> stock_traceability_reportdtos) {
        stock_traceability_reportService.createBatch(stock_traceability_reportMapping.toDomain(stock_traceability_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_traceability_report" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_traceability_reportService.get(#stock_traceability_report_id),'iBizBusinessCentral-Stock_traceability_report-Update')")
    @ApiOperation(value = "更新追溯报告", tags = {"追溯报告" },  notes = "更新追溯报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_traceability_reports/{stock_traceability_report_id}")
    public ResponseEntity<Stock_traceability_reportDTO> update(@PathVariable("stock_traceability_report_id") Long stock_traceability_report_id, @RequestBody Stock_traceability_reportDTO stock_traceability_reportdto) {
		Stock_traceability_report domain  = stock_traceability_reportMapping.toDomain(stock_traceability_reportdto);
        domain .setId(stock_traceability_report_id);
		stock_traceability_reportService.update(domain );
		Stock_traceability_reportDTO dto = stock_traceability_reportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_traceability_reportService.getStockTraceabilityReportByEntities(this.stock_traceability_reportMapping.toDomain(#stock_traceability_reportdtos)),'iBizBusinessCentral-Stock_traceability_report-Update')")
    @ApiOperation(value = "批量更新追溯报告", tags = {"追溯报告" },  notes = "批量更新追溯报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_traceability_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_traceability_reportDTO> stock_traceability_reportdtos) {
        stock_traceability_reportService.updateBatch(stock_traceability_reportMapping.toDomain(stock_traceability_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_traceability_reportService.get(#stock_traceability_report_id),'iBizBusinessCentral-Stock_traceability_report-Remove')")
    @ApiOperation(value = "删除追溯报告", tags = {"追溯报告" },  notes = "删除追溯报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_traceability_reports/{stock_traceability_report_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_traceability_report_id") Long stock_traceability_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_traceability_reportService.remove(stock_traceability_report_id));
    }

    @PreAuthorize("hasPermission(this.stock_traceability_reportService.getStockTraceabilityReportByIds(#ids),'iBizBusinessCentral-Stock_traceability_report-Remove')")
    @ApiOperation(value = "批量删除追溯报告", tags = {"追溯报告" },  notes = "批量删除追溯报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_traceability_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_traceability_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_traceability_reportMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_traceability_report-Get')")
    @ApiOperation(value = "获取追溯报告", tags = {"追溯报告" },  notes = "获取追溯报告")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_traceability_reports/{stock_traceability_report_id}")
    public ResponseEntity<Stock_traceability_reportDTO> get(@PathVariable("stock_traceability_report_id") Long stock_traceability_report_id) {
        Stock_traceability_report domain = stock_traceability_reportService.get(stock_traceability_report_id);
        Stock_traceability_reportDTO dto = stock_traceability_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取追溯报告草稿", tags = {"追溯报告" },  notes = "获取追溯报告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_traceability_reports/getdraft")
    public ResponseEntity<Stock_traceability_reportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_traceability_reportMapping.toDto(stock_traceability_reportService.getDraft(new Stock_traceability_report())));
    }

    @ApiOperation(value = "检查追溯报告", tags = {"追溯报告" },  notes = "检查追溯报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_traceability_reports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_traceability_reportDTO stock_traceability_reportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_traceability_reportService.checkKey(stock_traceability_reportMapping.toDomain(stock_traceability_reportdto)));
    }

    @PreAuthorize("hasPermission(this.stock_traceability_reportMapping.toDomain(#stock_traceability_reportdto),'iBizBusinessCentral-Stock_traceability_report-Save')")
    @ApiOperation(value = "保存追溯报告", tags = {"追溯报告" },  notes = "保存追溯报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_traceability_reports/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_traceability_reportDTO stock_traceability_reportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_traceability_reportService.save(stock_traceability_reportMapping.toDomain(stock_traceability_reportdto)));
    }

    @PreAuthorize("hasPermission(this.stock_traceability_reportMapping.toDomain(#stock_traceability_reportdtos),'iBizBusinessCentral-Stock_traceability_report-Save')")
    @ApiOperation(value = "批量保存追溯报告", tags = {"追溯报告" },  notes = "批量保存追溯报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_traceability_reports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_traceability_reportDTO> stock_traceability_reportdtos) {
        stock_traceability_reportService.saveBatch(stock_traceability_reportMapping.toDomain(stock_traceability_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_traceability_report-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_traceability_report-Get')")
	@ApiOperation(value = "获取数据集", tags = {"追溯报告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_traceability_reports/fetchdefault")
	public ResponseEntity<List<Stock_traceability_reportDTO>> fetchDefault(Stock_traceability_reportSearchContext context) {
        Page<Stock_traceability_report> domains = stock_traceability_reportService.searchDefault(context) ;
        List<Stock_traceability_reportDTO> list = stock_traceability_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_traceability_report-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_traceability_report-Get')")
	@ApiOperation(value = "查询数据集", tags = {"追溯报告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_traceability_reports/searchdefault")
	public ResponseEntity<Page<Stock_traceability_reportDTO>> searchDefault(@RequestBody Stock_traceability_reportSearchContext context) {
        Page<Stock_traceability_report> domains = stock_traceability_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_traceability_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

