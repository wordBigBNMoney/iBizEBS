package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_event_ticketService;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_event_ticketSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动入场券" })
@RestController("Core-event_event_ticket")
@RequestMapping("")
public class Event_event_ticketResource {

    @Autowired
    public IEvent_event_ticketService event_event_ticketService;

    @Autowired
    @Lazy
    public Event_event_ticketMapping event_event_ticketMapping;

    @PreAuthorize("hasPermission(this.event_event_ticketMapping.toDomain(#event_event_ticketdto),'iBizBusinessCentral-Event_event_ticket-Create')")
    @ApiOperation(value = "新建活动入场券", tags = {"活动入场券" },  notes = "新建活动入场券")
	@RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets")
    public ResponseEntity<Event_event_ticketDTO> create(@Validated @RequestBody Event_event_ticketDTO event_event_ticketdto) {
        Event_event_ticket domain = event_event_ticketMapping.toDomain(event_event_ticketdto);
		event_event_ticketService.create(domain);
        Event_event_ticketDTO dto = event_event_ticketMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_event_ticketMapping.toDomain(#event_event_ticketdtos),'iBizBusinessCentral-Event_event_ticket-Create')")
    @ApiOperation(value = "批量新建活动入场券", tags = {"活动入场券" },  notes = "批量新建活动入场券")
	@RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_event_ticketDTO> event_event_ticketdtos) {
        event_event_ticketService.createBatch(event_event_ticketMapping.toDomain(event_event_ticketdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "event_event_ticket" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.event_event_ticketService.get(#event_event_ticket_id),'iBizBusinessCentral-Event_event_ticket-Update')")
    @ApiOperation(value = "更新活动入场券", tags = {"活动入场券" },  notes = "更新活动入场券")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_event_tickets/{event_event_ticket_id}")
    public ResponseEntity<Event_event_ticketDTO> update(@PathVariable("event_event_ticket_id") Long event_event_ticket_id, @RequestBody Event_event_ticketDTO event_event_ticketdto) {
		Event_event_ticket domain  = event_event_ticketMapping.toDomain(event_event_ticketdto);
        domain .setId(event_event_ticket_id);
		event_event_ticketService.update(domain );
		Event_event_ticketDTO dto = event_event_ticketMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_event_ticketService.getEventEventTicketByEntities(this.event_event_ticketMapping.toDomain(#event_event_ticketdtos)),'iBizBusinessCentral-Event_event_ticket-Update')")
    @ApiOperation(value = "批量更新活动入场券", tags = {"活动入场券" },  notes = "批量更新活动入场券")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_event_tickets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_event_ticketDTO> event_event_ticketdtos) {
        event_event_ticketService.updateBatch(event_event_ticketMapping.toDomain(event_event_ticketdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.event_event_ticketService.get(#event_event_ticket_id),'iBizBusinessCentral-Event_event_ticket-Remove')")
    @ApiOperation(value = "删除活动入场券", tags = {"活动入场券" },  notes = "删除活动入场券")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_event_tickets/{event_event_ticket_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("event_event_ticket_id") Long event_event_ticket_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_event_ticketService.remove(event_event_ticket_id));
    }

    @PreAuthorize("hasPermission(this.event_event_ticketService.getEventEventTicketByIds(#ids),'iBizBusinessCentral-Event_event_ticket-Remove')")
    @ApiOperation(value = "批量删除活动入场券", tags = {"活动入场券" },  notes = "批量删除活动入场券")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_event_tickets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        event_event_ticketService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.event_event_ticketMapping.toDomain(returnObject.body),'iBizBusinessCentral-Event_event_ticket-Get')")
    @ApiOperation(value = "获取活动入场券", tags = {"活动入场券" },  notes = "获取活动入场券")
	@RequestMapping(method = RequestMethod.GET, value = "/event_event_tickets/{event_event_ticket_id}")
    public ResponseEntity<Event_event_ticketDTO> get(@PathVariable("event_event_ticket_id") Long event_event_ticket_id) {
        Event_event_ticket domain = event_event_ticketService.get(event_event_ticket_id);
        Event_event_ticketDTO dto = event_event_ticketMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动入场券草稿", tags = {"活动入场券" },  notes = "获取活动入场券草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/event_event_tickets/getdraft")
    public ResponseEntity<Event_event_ticketDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(event_event_ticketMapping.toDto(event_event_ticketService.getDraft(new Event_event_ticket())));
    }

    @ApiOperation(value = "检查活动入场券", tags = {"活动入场券" },  notes = "检查活动入场券")
	@RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Event_event_ticketDTO event_event_ticketdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(event_event_ticketService.checkKey(event_event_ticketMapping.toDomain(event_event_ticketdto)));
    }

    @PreAuthorize("hasPermission(this.event_event_ticketMapping.toDomain(#event_event_ticketdto),'iBizBusinessCentral-Event_event_ticket-Save')")
    @ApiOperation(value = "保存活动入场券", tags = {"活动入场券" },  notes = "保存活动入场券")
	@RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/save")
    public ResponseEntity<Boolean> save(@RequestBody Event_event_ticketDTO event_event_ticketdto) {
        return ResponseEntity.status(HttpStatus.OK).body(event_event_ticketService.save(event_event_ticketMapping.toDomain(event_event_ticketdto)));
    }

    @PreAuthorize("hasPermission(this.event_event_ticketMapping.toDomain(#event_event_ticketdtos),'iBizBusinessCentral-Event_event_ticket-Save')")
    @ApiOperation(value = "批量保存活动入场券", tags = {"活动入场券" },  notes = "批量保存活动入场券")
	@RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Event_event_ticketDTO> event_event_ticketdtos) {
        event_event_ticketService.saveBatch(event_event_ticketMapping.toDomain(event_event_ticketdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_event_ticket-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_event_ticket-Get')")
	@ApiOperation(value = "获取数据集", tags = {"活动入场券" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/event_event_tickets/fetchdefault")
	public ResponseEntity<List<Event_event_ticketDTO>> fetchDefault(Event_event_ticketSearchContext context) {
        Page<Event_event_ticket> domains = event_event_ticketService.searchDefault(context) ;
        List<Event_event_ticketDTO> list = event_event_ticketMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_event_ticket-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_event_ticket-Get')")
	@ApiOperation(value = "查询数据集", tags = {"活动入场券" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/event_event_tickets/searchdefault")
	public ResponseEntity<Page<Event_event_ticketDTO>> searchDefault(@RequestBody Event_event_ticketSearchContext context) {
        Page<Event_event_ticket> domains = event_event_ticketService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_event_ticketMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

