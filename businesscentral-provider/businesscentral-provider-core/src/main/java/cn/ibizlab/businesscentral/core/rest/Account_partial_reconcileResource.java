package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_partial_reconcileService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_partial_reconcileSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"部分核销" })
@RestController("Core-account_partial_reconcile")
@RequestMapping("")
public class Account_partial_reconcileResource {

    @Autowired
    public IAccount_partial_reconcileService account_partial_reconcileService;

    @Autowired
    @Lazy
    public Account_partial_reconcileMapping account_partial_reconcileMapping;

    @PreAuthorize("hasPermission(this.account_partial_reconcileMapping.toDomain(#account_partial_reconciledto),'iBizBusinessCentral-Account_partial_reconcile-Create')")
    @ApiOperation(value = "新建部分核销", tags = {"部分核销" },  notes = "新建部分核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles")
    public ResponseEntity<Account_partial_reconcileDTO> create(@Validated @RequestBody Account_partial_reconcileDTO account_partial_reconciledto) {
        Account_partial_reconcile domain = account_partial_reconcileMapping.toDomain(account_partial_reconciledto);
		account_partial_reconcileService.create(domain);
        Account_partial_reconcileDTO dto = account_partial_reconcileMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_partial_reconcileMapping.toDomain(#account_partial_reconciledtos),'iBizBusinessCentral-Account_partial_reconcile-Create')")
    @ApiOperation(value = "批量新建部分核销", tags = {"部分核销" },  notes = "批量新建部分核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_partial_reconcileDTO> account_partial_reconciledtos) {
        account_partial_reconcileService.createBatch(account_partial_reconcileMapping.toDomain(account_partial_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_partial_reconcile" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_partial_reconcileService.get(#account_partial_reconcile_id),'iBizBusinessCentral-Account_partial_reconcile-Update')")
    @ApiOperation(value = "更新部分核销", tags = {"部分核销" },  notes = "更新部分核销")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_partial_reconciles/{account_partial_reconcile_id}")
    public ResponseEntity<Account_partial_reconcileDTO> update(@PathVariable("account_partial_reconcile_id") Long account_partial_reconcile_id, @RequestBody Account_partial_reconcileDTO account_partial_reconciledto) {
		Account_partial_reconcile domain  = account_partial_reconcileMapping.toDomain(account_partial_reconciledto);
        domain .setId(account_partial_reconcile_id);
		account_partial_reconcileService.update(domain );
		Account_partial_reconcileDTO dto = account_partial_reconcileMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_partial_reconcileService.getAccountPartialReconcileByEntities(this.account_partial_reconcileMapping.toDomain(#account_partial_reconciledtos)),'iBizBusinessCentral-Account_partial_reconcile-Update')")
    @ApiOperation(value = "批量更新部分核销", tags = {"部分核销" },  notes = "批量更新部分核销")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_partial_reconciles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_partial_reconcileDTO> account_partial_reconciledtos) {
        account_partial_reconcileService.updateBatch(account_partial_reconcileMapping.toDomain(account_partial_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_partial_reconcileService.get(#account_partial_reconcile_id),'iBizBusinessCentral-Account_partial_reconcile-Remove')")
    @ApiOperation(value = "删除部分核销", tags = {"部分核销" },  notes = "删除部分核销")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_partial_reconciles/{account_partial_reconcile_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_partial_reconcile_id") Long account_partial_reconcile_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_partial_reconcileService.remove(account_partial_reconcile_id));
    }

    @PreAuthorize("hasPermission(this.account_partial_reconcileService.getAccountPartialReconcileByIds(#ids),'iBizBusinessCentral-Account_partial_reconcile-Remove')")
    @ApiOperation(value = "批量删除部分核销", tags = {"部分核销" },  notes = "批量删除部分核销")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_partial_reconciles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_partial_reconcileService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_partial_reconcileMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_partial_reconcile-Get')")
    @ApiOperation(value = "获取部分核销", tags = {"部分核销" },  notes = "获取部分核销")
	@RequestMapping(method = RequestMethod.GET, value = "/account_partial_reconciles/{account_partial_reconcile_id}")
    public ResponseEntity<Account_partial_reconcileDTO> get(@PathVariable("account_partial_reconcile_id") Long account_partial_reconcile_id) {
        Account_partial_reconcile domain = account_partial_reconcileService.get(account_partial_reconcile_id);
        Account_partial_reconcileDTO dto = account_partial_reconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取部分核销草稿", tags = {"部分核销" },  notes = "获取部分核销草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_partial_reconciles/getdraft")
    public ResponseEntity<Account_partial_reconcileDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_partial_reconcileMapping.toDto(account_partial_reconcileService.getDraft(new Account_partial_reconcile())));
    }

    @ApiOperation(value = "检查部分核销", tags = {"部分核销" },  notes = "检查部分核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_partial_reconcileDTO account_partial_reconciledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_partial_reconcileService.checkKey(account_partial_reconcileMapping.toDomain(account_partial_reconciledto)));
    }

    @PreAuthorize("hasPermission(this.account_partial_reconcileMapping.toDomain(#account_partial_reconciledto),'iBizBusinessCentral-Account_partial_reconcile-Save')")
    @ApiOperation(value = "保存部分核销", tags = {"部分核销" },  notes = "保存部分核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_partial_reconcileDTO account_partial_reconciledto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_partial_reconcileService.save(account_partial_reconcileMapping.toDomain(account_partial_reconciledto)));
    }

    @PreAuthorize("hasPermission(this.account_partial_reconcileMapping.toDomain(#account_partial_reconciledtos),'iBizBusinessCentral-Account_partial_reconcile-Save')")
    @ApiOperation(value = "批量保存部分核销", tags = {"部分核销" },  notes = "批量保存部分核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_partial_reconcileDTO> account_partial_reconciledtos) {
        account_partial_reconcileService.saveBatch(account_partial_reconcileMapping.toDomain(account_partial_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_partial_reconcile-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_partial_reconcile-Get')")
	@ApiOperation(value = "获取数据集", tags = {"部分核销" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_partial_reconciles/fetchdefault")
	public ResponseEntity<List<Account_partial_reconcileDTO>> fetchDefault(Account_partial_reconcileSearchContext context) {
        Page<Account_partial_reconcile> domains = account_partial_reconcileService.searchDefault(context) ;
        List<Account_partial_reconcileDTO> list = account_partial_reconcileMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_partial_reconcile-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_partial_reconcile-Get')")
	@ApiOperation(value = "查询数据集", tags = {"部分核销" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_partial_reconciles/searchdefault")
	public ResponseEntity<Page<Account_partial_reconcileDTO>> searchDefault(@RequestBody Account_partial_reconcileSearchContext context) {
        Page<Account_partial_reconcile> domains = account_partial_reconcileService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_partial_reconcileMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

