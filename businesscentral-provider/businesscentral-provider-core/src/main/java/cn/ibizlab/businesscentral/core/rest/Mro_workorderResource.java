package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_workorder;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_workorderService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_workorderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工单" })
@RestController("Core-mro_workorder")
@RequestMapping("")
public class Mro_workorderResource {

    @Autowired
    public IMro_workorderService mro_workorderService;

    @Autowired
    @Lazy
    public Mro_workorderMapping mro_workorderMapping;

    @PreAuthorize("hasPermission(this.mro_workorderMapping.toDomain(#mro_workorderdto),'iBizBusinessCentral-Mro_workorder-Create')")
    @ApiOperation(value = "新建工单", tags = {"工单" },  notes = "新建工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_workorders")
    public ResponseEntity<Mro_workorderDTO> create(@Validated @RequestBody Mro_workorderDTO mro_workorderdto) {
        Mro_workorder domain = mro_workorderMapping.toDomain(mro_workorderdto);
		mro_workorderService.create(domain);
        Mro_workorderDTO dto = mro_workorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_workorderMapping.toDomain(#mro_workorderdtos),'iBizBusinessCentral-Mro_workorder-Create')")
    @ApiOperation(value = "批量新建工单", tags = {"工单" },  notes = "批量新建工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_workorders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_workorderDTO> mro_workorderdtos) {
        mro_workorderService.createBatch(mro_workorderMapping.toDomain(mro_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_workorder" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_workorderService.get(#mro_workorder_id),'iBizBusinessCentral-Mro_workorder-Update')")
    @ApiOperation(value = "更新工单", tags = {"工单" },  notes = "更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_workorders/{mro_workorder_id}")
    public ResponseEntity<Mro_workorderDTO> update(@PathVariable("mro_workorder_id") Long mro_workorder_id, @RequestBody Mro_workorderDTO mro_workorderdto) {
		Mro_workorder domain  = mro_workorderMapping.toDomain(mro_workorderdto);
        domain .setId(mro_workorder_id);
		mro_workorderService.update(domain );
		Mro_workorderDTO dto = mro_workorderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_workorderService.getMroWorkorderByEntities(this.mro_workorderMapping.toDomain(#mro_workorderdtos)),'iBizBusinessCentral-Mro_workorder-Update')")
    @ApiOperation(value = "批量更新工单", tags = {"工单" },  notes = "批量更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_workorders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_workorderDTO> mro_workorderdtos) {
        mro_workorderService.updateBatch(mro_workorderMapping.toDomain(mro_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_workorderService.get(#mro_workorder_id),'iBizBusinessCentral-Mro_workorder-Remove')")
    @ApiOperation(value = "删除工单", tags = {"工单" },  notes = "删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_workorders/{mro_workorder_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_workorder_id") Long mro_workorder_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_workorderService.remove(mro_workorder_id));
    }

    @PreAuthorize("hasPermission(this.mro_workorderService.getMroWorkorderByIds(#ids),'iBizBusinessCentral-Mro_workorder-Remove')")
    @ApiOperation(value = "批量删除工单", tags = {"工单" },  notes = "批量删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_workorders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_workorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_workorderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_workorder-Get')")
    @ApiOperation(value = "获取工单", tags = {"工单" },  notes = "获取工单")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_workorders/{mro_workorder_id}")
    public ResponseEntity<Mro_workorderDTO> get(@PathVariable("mro_workorder_id") Long mro_workorder_id) {
        Mro_workorder domain = mro_workorderService.get(mro_workorder_id);
        Mro_workorderDTO dto = mro_workorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工单草稿", tags = {"工单" },  notes = "获取工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_workorders/getdraft")
    public ResponseEntity<Mro_workorderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_workorderMapping.toDto(mro_workorderService.getDraft(new Mro_workorder())));
    }

    @ApiOperation(value = "检查工单", tags = {"工单" },  notes = "检查工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_workorders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_workorderDTO mro_workorderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_workorderService.checkKey(mro_workorderMapping.toDomain(mro_workorderdto)));
    }

    @PreAuthorize("hasPermission(this.mro_workorderMapping.toDomain(#mro_workorderdto),'iBizBusinessCentral-Mro_workorder-Save')")
    @ApiOperation(value = "保存工单", tags = {"工单" },  notes = "保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_workorders/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_workorderDTO mro_workorderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_workorderService.save(mro_workorderMapping.toDomain(mro_workorderdto)));
    }

    @PreAuthorize("hasPermission(this.mro_workorderMapping.toDomain(#mro_workorderdtos),'iBizBusinessCentral-Mro_workorder-Save')")
    @ApiOperation(value = "批量保存工单", tags = {"工单" },  notes = "批量保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_workorders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_workorderDTO> mro_workorderdtos) {
        mro_workorderService.saveBatch(mro_workorderMapping.toDomain(mro_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_workorder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_workorder-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_workorders/fetchdefault")
	public ResponseEntity<List<Mro_workorderDTO>> fetchDefault(Mro_workorderSearchContext context) {
        Page<Mro_workorder> domains = mro_workorderService.searchDefault(context) ;
        List<Mro_workorderDTO> list = mro_workorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_workorder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_workorder-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_workorders/searchdefault")
	public ResponseEntity<Page<Mro_workorderDTO>> searchDefault(@RequestBody Mro_workorderSearchContext context) {
        Page<Mro_workorder> domains = mro_workorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_workorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

