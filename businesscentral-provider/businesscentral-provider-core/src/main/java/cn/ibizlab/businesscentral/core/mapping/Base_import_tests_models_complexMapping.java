package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_complex;
import cn.ibizlab.businesscentral.core.dto.Base_import_tests_models_complexDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreBase_import_tests_models_complexMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_import_tests_models_complexMapping extends MappingBase<Base_import_tests_models_complexDTO, Base_import_tests_models_complex> {


}

