package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.businesscentral.core.dto.Resource_calendar_leavesDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreResource_calendar_leavesMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Resource_calendar_leavesMapping extends MappingBase<Resource_calendar_leavesDTO, Resource_calendar_leaves> {


}

