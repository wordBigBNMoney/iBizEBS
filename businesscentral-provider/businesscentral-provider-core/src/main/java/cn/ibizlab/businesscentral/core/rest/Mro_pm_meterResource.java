package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meterService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meterSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Asset Meters" })
@RestController("Core-mro_pm_meter")
@RequestMapping("")
public class Mro_pm_meterResource {

    @Autowired
    public IMro_pm_meterService mro_pm_meterService;

    @Autowired
    @Lazy
    public Mro_pm_meterMapping mro_pm_meterMapping;

    @PreAuthorize("hasPermission(this.mro_pm_meterMapping.toDomain(#mro_pm_meterdto),'iBizBusinessCentral-Mro_pm_meter-Create')")
    @ApiOperation(value = "新建Asset Meters", tags = {"Asset Meters" },  notes = "新建Asset Meters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters")
    public ResponseEntity<Mro_pm_meterDTO> create(@Validated @RequestBody Mro_pm_meterDTO mro_pm_meterdto) {
        Mro_pm_meter domain = mro_pm_meterMapping.toDomain(mro_pm_meterdto);
		mro_pm_meterService.create(domain);
        Mro_pm_meterDTO dto = mro_pm_meterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meterMapping.toDomain(#mro_pm_meterdtos),'iBizBusinessCentral-Mro_pm_meter-Create')")
    @ApiOperation(value = "批量新建Asset Meters", tags = {"Asset Meters" },  notes = "批量新建Asset Meters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_meterDTO> mro_pm_meterdtos) {
        mro_pm_meterService.createBatch(mro_pm_meterMapping.toDomain(mro_pm_meterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_pm_meter" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_pm_meterService.get(#mro_pm_meter_id),'iBizBusinessCentral-Mro_pm_meter-Update')")
    @ApiOperation(value = "更新Asset Meters", tags = {"Asset Meters" },  notes = "更新Asset Meters")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meters/{mro_pm_meter_id}")
    public ResponseEntity<Mro_pm_meterDTO> update(@PathVariable("mro_pm_meter_id") Long mro_pm_meter_id, @RequestBody Mro_pm_meterDTO mro_pm_meterdto) {
		Mro_pm_meter domain  = mro_pm_meterMapping.toDomain(mro_pm_meterdto);
        domain .setId(mro_pm_meter_id);
		mro_pm_meterService.update(domain );
		Mro_pm_meterDTO dto = mro_pm_meterMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meterService.getMroPmMeterByEntities(this.mro_pm_meterMapping.toDomain(#mro_pm_meterdtos)),'iBizBusinessCentral-Mro_pm_meter-Update')")
    @ApiOperation(value = "批量更新Asset Meters", tags = {"Asset Meters" },  notes = "批量更新Asset Meters")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meterDTO> mro_pm_meterdtos) {
        mro_pm_meterService.updateBatch(mro_pm_meterMapping.toDomain(mro_pm_meterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meterService.get(#mro_pm_meter_id),'iBizBusinessCentral-Mro_pm_meter-Remove')")
    @ApiOperation(value = "删除Asset Meters", tags = {"Asset Meters" },  notes = "删除Asset Meters")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meters/{mro_pm_meter_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_id") Long mro_pm_meter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meterService.remove(mro_pm_meter_id));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meterService.getMroPmMeterByIds(#ids),'iBizBusinessCentral-Mro_pm_meter-Remove')")
    @ApiOperation(value = "批量删除Asset Meters", tags = {"Asset Meters" },  notes = "批量删除Asset Meters")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_pm_meterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_pm_meterMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_pm_meter-Get')")
    @ApiOperation(value = "获取Asset Meters", tags = {"Asset Meters" },  notes = "获取Asset Meters")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meters/{mro_pm_meter_id}")
    public ResponseEntity<Mro_pm_meterDTO> get(@PathVariable("mro_pm_meter_id") Long mro_pm_meter_id) {
        Mro_pm_meter domain = mro_pm_meterService.get(mro_pm_meter_id);
        Mro_pm_meterDTO dto = mro_pm_meterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Asset Meters草稿", tags = {"Asset Meters" },  notes = "获取Asset Meters草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meters/getdraft")
    public ResponseEntity<Mro_pm_meterDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meterMapping.toDto(mro_pm_meterService.getDraft(new Mro_pm_meter())));
    }

    @ApiOperation(value = "检查Asset Meters", tags = {"Asset Meters" },  notes = "检查Asset Meters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_pm_meterDTO mro_pm_meterdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_pm_meterService.checkKey(mro_pm_meterMapping.toDomain(mro_pm_meterdto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meterMapping.toDomain(#mro_pm_meterdto),'iBizBusinessCentral-Mro_pm_meter-Save')")
    @ApiOperation(value = "保存Asset Meters", tags = {"Asset Meters" },  notes = "保存Asset Meters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_pm_meterDTO mro_pm_meterdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meterService.save(mro_pm_meterMapping.toDomain(mro_pm_meterdto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meterMapping.toDomain(#mro_pm_meterdtos),'iBizBusinessCentral-Mro_pm_meter-Save')")
    @ApiOperation(value = "批量保存Asset Meters", tags = {"Asset Meters" },  notes = "批量保存Asset Meters")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_pm_meterDTO> mro_pm_meterdtos) {
        mro_pm_meterService.saveBatch(mro_pm_meterMapping.toDomain(mro_pm_meterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_meter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_meter-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Asset Meters" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meters/fetchdefault")
	public ResponseEntity<List<Mro_pm_meterDTO>> fetchDefault(Mro_pm_meterSearchContext context) {
        Page<Mro_pm_meter> domains = mro_pm_meterService.searchDefault(context) ;
        List<Mro_pm_meterDTO> list = mro_pm_meterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_meter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_meter-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Asset Meters" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_pm_meters/searchdefault")
	public ResponseEntity<Page<Mro_pm_meterDTO>> searchDefault(@RequestBody Mro_pm_meterSearchContext context) {
        Page<Mro_pm_meter> domains = mro_pm_meterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_meterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

