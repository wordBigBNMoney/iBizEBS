package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing_workcenter;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routing_workcenterService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_routing_workcenterSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作中心使用情况" })
@RestController("Core-mrp_routing_workcenter")
@RequestMapping("")
public class Mrp_routing_workcenterResource {

    @Autowired
    public IMrp_routing_workcenterService mrp_routing_workcenterService;

    @Autowired
    @Lazy
    public Mrp_routing_workcenterMapping mrp_routing_workcenterMapping;

    @PreAuthorize("hasPermission(this.mrp_routing_workcenterMapping.toDomain(#mrp_routing_workcenterdto),'iBizBusinessCentral-Mrp_routing_workcenter-Create')")
    @ApiOperation(value = "新建工作中心使用情况", tags = {"工作中心使用情况" },  notes = "新建工作中心使用情况")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters")
    public ResponseEntity<Mrp_routing_workcenterDTO> create(@Validated @RequestBody Mrp_routing_workcenterDTO mrp_routing_workcenterdto) {
        Mrp_routing_workcenter domain = mrp_routing_workcenterMapping.toDomain(mrp_routing_workcenterdto);
		mrp_routing_workcenterService.create(domain);
        Mrp_routing_workcenterDTO dto = mrp_routing_workcenterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_routing_workcenterMapping.toDomain(#mrp_routing_workcenterdtos),'iBizBusinessCentral-Mrp_routing_workcenter-Create')")
    @ApiOperation(value = "批量新建工作中心使用情况", tags = {"工作中心使用情况" },  notes = "批量新建工作中心使用情况")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_routing_workcenterDTO> mrp_routing_workcenterdtos) {
        mrp_routing_workcenterService.createBatch(mrp_routing_workcenterMapping.toDomain(mrp_routing_workcenterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_routing_workcenter" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_routing_workcenterService.get(#mrp_routing_workcenter_id),'iBizBusinessCentral-Mrp_routing_workcenter-Update')")
    @ApiOperation(value = "更新工作中心使用情况", tags = {"工作中心使用情况" },  notes = "更新工作中心使用情况")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_routing_workcenters/{mrp_routing_workcenter_id}")
    public ResponseEntity<Mrp_routing_workcenterDTO> update(@PathVariable("mrp_routing_workcenter_id") Long mrp_routing_workcenter_id, @RequestBody Mrp_routing_workcenterDTO mrp_routing_workcenterdto) {
		Mrp_routing_workcenter domain  = mrp_routing_workcenterMapping.toDomain(mrp_routing_workcenterdto);
        domain .setId(mrp_routing_workcenter_id);
		mrp_routing_workcenterService.update(domain );
		Mrp_routing_workcenterDTO dto = mrp_routing_workcenterMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_routing_workcenterService.getMrpRoutingWorkcenterByEntities(this.mrp_routing_workcenterMapping.toDomain(#mrp_routing_workcenterdtos)),'iBizBusinessCentral-Mrp_routing_workcenter-Update')")
    @ApiOperation(value = "批量更新工作中心使用情况", tags = {"工作中心使用情况" },  notes = "批量更新工作中心使用情况")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_routing_workcenters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_routing_workcenterDTO> mrp_routing_workcenterdtos) {
        mrp_routing_workcenterService.updateBatch(mrp_routing_workcenterMapping.toDomain(mrp_routing_workcenterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_routing_workcenterService.get(#mrp_routing_workcenter_id),'iBizBusinessCentral-Mrp_routing_workcenter-Remove')")
    @ApiOperation(value = "删除工作中心使用情况", tags = {"工作中心使用情况" },  notes = "删除工作中心使用情况")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routing_workcenters/{mrp_routing_workcenter_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_routing_workcenter_id") Long mrp_routing_workcenter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_routing_workcenterService.remove(mrp_routing_workcenter_id));
    }

    @PreAuthorize("hasPermission(this.mrp_routing_workcenterService.getMrpRoutingWorkcenterByIds(#ids),'iBizBusinessCentral-Mrp_routing_workcenter-Remove')")
    @ApiOperation(value = "批量删除工作中心使用情况", tags = {"工作中心使用情况" },  notes = "批量删除工作中心使用情况")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routing_workcenters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_routing_workcenterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_routing_workcenterMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_routing_workcenter-Get')")
    @ApiOperation(value = "获取工作中心使用情况", tags = {"工作中心使用情况" },  notes = "获取工作中心使用情况")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_routing_workcenters/{mrp_routing_workcenter_id}")
    public ResponseEntity<Mrp_routing_workcenterDTO> get(@PathVariable("mrp_routing_workcenter_id") Long mrp_routing_workcenter_id) {
        Mrp_routing_workcenter domain = mrp_routing_workcenterService.get(mrp_routing_workcenter_id);
        Mrp_routing_workcenterDTO dto = mrp_routing_workcenterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作中心使用情况草稿", tags = {"工作中心使用情况" },  notes = "获取工作中心使用情况草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_routing_workcenters/getdraft")
    public ResponseEntity<Mrp_routing_workcenterDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_routing_workcenterMapping.toDto(mrp_routing_workcenterService.getDraft(new Mrp_routing_workcenter())));
    }

    @ApiOperation(value = "检查工作中心使用情况", tags = {"工作中心使用情况" },  notes = "检查工作中心使用情况")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_routing_workcenterDTO mrp_routing_workcenterdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_routing_workcenterService.checkKey(mrp_routing_workcenterMapping.toDomain(mrp_routing_workcenterdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_routing_workcenterMapping.toDomain(#mrp_routing_workcenterdto),'iBizBusinessCentral-Mrp_routing_workcenter-Save')")
    @ApiOperation(value = "保存工作中心使用情况", tags = {"工作中心使用情况" },  notes = "保存工作中心使用情况")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_routing_workcenterDTO mrp_routing_workcenterdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_routing_workcenterService.save(mrp_routing_workcenterMapping.toDomain(mrp_routing_workcenterdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_routing_workcenterMapping.toDomain(#mrp_routing_workcenterdtos),'iBizBusinessCentral-Mrp_routing_workcenter-Save')")
    @ApiOperation(value = "批量保存工作中心使用情况", tags = {"工作中心使用情况" },  notes = "批量保存工作中心使用情况")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_routing_workcenterDTO> mrp_routing_workcenterdtos) {
        mrp_routing_workcenterService.saveBatch(mrp_routing_workcenterMapping.toDomain(mrp_routing_workcenterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_routing_workcenter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_routing_workcenter-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作中心使用情况" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_routing_workcenters/fetchdefault")
	public ResponseEntity<List<Mrp_routing_workcenterDTO>> fetchDefault(Mrp_routing_workcenterSearchContext context) {
        Page<Mrp_routing_workcenter> domains = mrp_routing_workcenterService.searchDefault(context) ;
        List<Mrp_routing_workcenterDTO> list = mrp_routing_workcenterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_routing_workcenter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_routing_workcenter-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作中心使用情况" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_routing_workcenters/searchdefault")
	public ResponseEntity<Page<Mrp_routing_workcenterDTO>> searchDefault(@RequestBody Mrp_routing_workcenterSearchContext context) {
        Page<Mrp_routing_workcenter> domains = mrp_routing_workcenterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_routing_workcenterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

