package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Resource_calendar_leavesDTO]
 */
@Data
public class Resource_calendar_leavesDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [TIME_TYPE]
     *
     */
    @JSONField(name = "time_type")
    @JsonProperty("time_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String timeType;

    /**
     * 属性 [DATE_TO]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_to")
    @NotNull(message = "[结束日期]不允许为空!")
    private Timestamp dateTo;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_from")
    @NotNull(message = "[开始日期]不允许为空!")
    private Timestamp dateFrom;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [CALENDAR_ID_TEXT]
     *
     */
    @JSONField(name = "calendar_id_text")
    @JsonProperty("calendar_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String calendarIdText;

    /**
     * 属性 [HOLIDAY_ID_TEXT]
     *
     */
    @JSONField(name = "holiday_id_text")
    @JsonProperty("holiday_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String holidayIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [RESOURCE_ID_TEXT]
     *
     */
    @JSONField(name = "resource_id_text")
    @JsonProperty("resource_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resourceIdText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [HOLIDAY_ID]
     *
     */
    @JSONField(name = "holiday_id")
    @JsonProperty("holiday_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long holidayId;

    /**
     * 属性 [CALENDAR_ID]
     *
     */
    @JSONField(name = "calendar_id")
    @JsonProperty("calendar_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long calendarId;

    /**
     * 属性 [RESOURCE_ID]
     *
     */
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resourceId;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [TIME_TYPE]
     */
    public void setTimeType(String  timeType){
        this.timeType = timeType ;
        this.modify("time_type",timeType);
    }

    /**
     * 设置 [DATE_TO]
     */
    public void setDateTo(Timestamp  dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }

    /**
     * 设置 [DATE_FROM]
     */
    public void setDateFrom(Timestamp  dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [HOLIDAY_ID]
     */
    public void setHolidayId(Long  holidayId){
        this.holidayId = holidayId ;
        this.modify("holiday_id",holidayId);
    }

    /**
     * 设置 [CALENDAR_ID]
     */
    public void setCalendarId(Long  calendarId){
        this.calendarId = calendarId ;
        this.modify("calendar_id",calendarId);
    }

    /**
     * 设置 [RESOURCE_ID]
     */
    public void setResourceId(Long  resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }


}


