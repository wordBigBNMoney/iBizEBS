package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_group;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_country_groupService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_country_groupSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"国家/地区群组" })
@RestController("Core-res_country_group")
@RequestMapping("")
public class Res_country_groupResource {

    @Autowired
    public IRes_country_groupService res_country_groupService;

    @Autowired
    @Lazy
    public Res_country_groupMapping res_country_groupMapping;

    @PreAuthorize("hasPermission(this.res_country_groupMapping.toDomain(#res_country_groupdto),'iBizBusinessCentral-Res_country_group-Create')")
    @ApiOperation(value = "新建国家/地区群组", tags = {"国家/地区群组" },  notes = "新建国家/地区群组")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_groups")
    public ResponseEntity<Res_country_groupDTO> create(@Validated @RequestBody Res_country_groupDTO res_country_groupdto) {
        Res_country_group domain = res_country_groupMapping.toDomain(res_country_groupdto);
		res_country_groupService.create(domain);
        Res_country_groupDTO dto = res_country_groupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_country_groupMapping.toDomain(#res_country_groupdtos),'iBizBusinessCentral-Res_country_group-Create')")
    @ApiOperation(value = "批量新建国家/地区群组", tags = {"国家/地区群组" },  notes = "批量新建国家/地区群组")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_country_groupDTO> res_country_groupdtos) {
        res_country_groupService.createBatch(res_country_groupMapping.toDomain(res_country_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_country_group" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_country_groupService.get(#res_country_group_id),'iBizBusinessCentral-Res_country_group-Update')")
    @ApiOperation(value = "更新国家/地区群组", tags = {"国家/地区群组" },  notes = "更新国家/地区群组")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_country_groups/{res_country_group_id}")
    public ResponseEntity<Res_country_groupDTO> update(@PathVariable("res_country_group_id") Long res_country_group_id, @RequestBody Res_country_groupDTO res_country_groupdto) {
		Res_country_group domain  = res_country_groupMapping.toDomain(res_country_groupdto);
        domain .setId(res_country_group_id);
		res_country_groupService.update(domain );
		Res_country_groupDTO dto = res_country_groupMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_country_groupService.getResCountryGroupByEntities(this.res_country_groupMapping.toDomain(#res_country_groupdtos)),'iBizBusinessCentral-Res_country_group-Update')")
    @ApiOperation(value = "批量更新国家/地区群组", tags = {"国家/地区群组" },  notes = "批量更新国家/地区群组")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_country_groups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_country_groupDTO> res_country_groupdtos) {
        res_country_groupService.updateBatch(res_country_groupMapping.toDomain(res_country_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_country_groupService.get(#res_country_group_id),'iBizBusinessCentral-Res_country_group-Remove')")
    @ApiOperation(value = "删除国家/地区群组", tags = {"国家/地区群组" },  notes = "删除国家/地区群组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_country_groups/{res_country_group_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_country_group_id") Long res_country_group_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_country_groupService.remove(res_country_group_id));
    }

    @PreAuthorize("hasPermission(this.res_country_groupService.getResCountryGroupByIds(#ids),'iBizBusinessCentral-Res_country_group-Remove')")
    @ApiOperation(value = "批量删除国家/地区群组", tags = {"国家/地区群组" },  notes = "批量删除国家/地区群组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_country_groups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_country_groupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_country_groupMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_country_group-Get')")
    @ApiOperation(value = "获取国家/地区群组", tags = {"国家/地区群组" },  notes = "获取国家/地区群组")
	@RequestMapping(method = RequestMethod.GET, value = "/res_country_groups/{res_country_group_id}")
    public ResponseEntity<Res_country_groupDTO> get(@PathVariable("res_country_group_id") Long res_country_group_id) {
        Res_country_group domain = res_country_groupService.get(res_country_group_id);
        Res_country_groupDTO dto = res_country_groupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取国家/地区群组草稿", tags = {"国家/地区群组" },  notes = "获取国家/地区群组草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_country_groups/getdraft")
    public ResponseEntity<Res_country_groupDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_country_groupMapping.toDto(res_country_groupService.getDraft(new Res_country_group())));
    }

    @ApiOperation(value = "检查国家/地区群组", tags = {"国家/地区群组" },  notes = "检查国家/地区群组")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_country_groupDTO res_country_groupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_country_groupService.checkKey(res_country_groupMapping.toDomain(res_country_groupdto)));
    }

    @PreAuthorize("hasPermission(this.res_country_groupMapping.toDomain(#res_country_groupdto),'iBizBusinessCentral-Res_country_group-Save')")
    @ApiOperation(value = "保存国家/地区群组", tags = {"国家/地区群组" },  notes = "保存国家/地区群组")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_country_groupDTO res_country_groupdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_country_groupService.save(res_country_groupMapping.toDomain(res_country_groupdto)));
    }

    @PreAuthorize("hasPermission(this.res_country_groupMapping.toDomain(#res_country_groupdtos),'iBizBusinessCentral-Res_country_group-Save')")
    @ApiOperation(value = "批量保存国家/地区群组", tags = {"国家/地区群组" },  notes = "批量保存国家/地区群组")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_country_groupDTO> res_country_groupdtos) {
        res_country_groupService.saveBatch(res_country_groupMapping.toDomain(res_country_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_country_group-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_country_group-Get')")
	@ApiOperation(value = "获取数据集", tags = {"国家/地区群组" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_country_groups/fetchdefault")
	public ResponseEntity<List<Res_country_groupDTO>> fetchDefault(Res_country_groupSearchContext context) {
        Page<Res_country_group> domains = res_country_groupService.searchDefault(context) ;
        List<Res_country_groupDTO> list = res_country_groupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_country_group-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_country_group-Get')")
	@ApiOperation(value = "查询数据集", tags = {"国家/地区群组" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_country_groups/searchdefault")
	public ResponseEntity<Page<Res_country_groupDTO>> searchDefault(@RequestBody Res_country_groupSearchContext context) {
        Page<Res_country_group> domains = res_country_groupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_country_groupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

