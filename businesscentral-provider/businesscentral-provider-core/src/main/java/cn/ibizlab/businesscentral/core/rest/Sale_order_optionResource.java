package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_option;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_optionService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_optionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售选项" })
@RestController("Core-sale_order_option")
@RequestMapping("")
public class Sale_order_optionResource {

    @Autowired
    public ISale_order_optionService sale_order_optionService;

    @Autowired
    @Lazy
    public Sale_order_optionMapping sale_order_optionMapping;

    @PreAuthorize("hasPermission(this.sale_order_optionMapping.toDomain(#sale_order_optiondto),'iBizBusinessCentral-Sale_order_option-Create')")
    @ApiOperation(value = "新建销售选项", tags = {"销售选项" },  notes = "新建销售选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_options")
    public ResponseEntity<Sale_order_optionDTO> create(@Validated @RequestBody Sale_order_optionDTO sale_order_optiondto) {
        Sale_order_option domain = sale_order_optionMapping.toDomain(sale_order_optiondto);
		sale_order_optionService.create(domain);
        Sale_order_optionDTO dto = sale_order_optionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_optionMapping.toDomain(#sale_order_optiondtos),'iBizBusinessCentral-Sale_order_option-Create')")
    @ApiOperation(value = "批量新建销售选项", tags = {"销售选项" },  notes = "批量新建销售选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_order_optionDTO> sale_order_optiondtos) {
        sale_order_optionService.createBatch(sale_order_optionMapping.toDomain(sale_order_optiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sale_order_option" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sale_order_optionService.get(#sale_order_option_id),'iBizBusinessCentral-Sale_order_option-Update')")
    @ApiOperation(value = "更新销售选项", tags = {"销售选项" },  notes = "更新销售选项")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_options/{sale_order_option_id}")
    public ResponseEntity<Sale_order_optionDTO> update(@PathVariable("sale_order_option_id") Long sale_order_option_id, @RequestBody Sale_order_optionDTO sale_order_optiondto) {
		Sale_order_option domain  = sale_order_optionMapping.toDomain(sale_order_optiondto);
        domain .setId(sale_order_option_id);
		sale_order_optionService.update(domain );
		Sale_order_optionDTO dto = sale_order_optionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_optionService.getSaleOrderOptionByEntities(this.sale_order_optionMapping.toDomain(#sale_order_optiondtos)),'iBizBusinessCentral-Sale_order_option-Update')")
    @ApiOperation(value = "批量更新销售选项", tags = {"销售选项" },  notes = "批量更新销售选项")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_options/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_optionDTO> sale_order_optiondtos) {
        sale_order_optionService.updateBatch(sale_order_optionMapping.toDomain(sale_order_optiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sale_order_optionService.get(#sale_order_option_id),'iBizBusinessCentral-Sale_order_option-Remove')")
    @ApiOperation(value = "删除销售选项", tags = {"销售选项" },  notes = "删除销售选项")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_options/{sale_order_option_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_option_id") Long sale_order_option_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_order_optionService.remove(sale_order_option_id));
    }

    @PreAuthorize("hasPermission(this.sale_order_optionService.getSaleOrderOptionByIds(#ids),'iBizBusinessCentral-Sale_order_option-Remove')")
    @ApiOperation(value = "批量删除销售选项", tags = {"销售选项" },  notes = "批量删除销售选项")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_options/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_order_optionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sale_order_optionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sale_order_option-Get')")
    @ApiOperation(value = "获取销售选项", tags = {"销售选项" },  notes = "获取销售选项")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_options/{sale_order_option_id}")
    public ResponseEntity<Sale_order_optionDTO> get(@PathVariable("sale_order_option_id") Long sale_order_option_id) {
        Sale_order_option domain = sale_order_optionService.get(sale_order_option_id);
        Sale_order_optionDTO dto = sale_order_optionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售选项草稿", tags = {"销售选项" },  notes = "获取销售选项草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_options/getdraft")
    public ResponseEntity<Sale_order_optionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_optionMapping.toDto(sale_order_optionService.getDraft(new Sale_order_option())));
    }

    @ApiOperation(value = "检查销售选项", tags = {"销售选项" },  notes = "检查销售选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_order_optionDTO sale_order_optiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_order_optionService.checkKey(sale_order_optionMapping.toDomain(sale_order_optiondto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_optionMapping.toDomain(#sale_order_optiondto),'iBizBusinessCentral-Sale_order_option-Save')")
    @ApiOperation(value = "保存销售选项", tags = {"销售选项" },  notes = "保存销售选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_order_optionDTO sale_order_optiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_optionService.save(sale_order_optionMapping.toDomain(sale_order_optiondto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_optionMapping.toDomain(#sale_order_optiondtos),'iBizBusinessCentral-Sale_order_option-Save')")
    @ApiOperation(value = "批量保存销售选项", tags = {"销售选项" },  notes = "批量保存销售选项")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_order_optionDTO> sale_order_optiondtos) {
        sale_order_optionService.saveBatch(sale_order_optionMapping.toDomain(sale_order_optiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_option-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_option-Get')")
	@ApiOperation(value = "获取数据集", tags = {"销售选项" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_order_options/fetchdefault")
	public ResponseEntity<List<Sale_order_optionDTO>> fetchDefault(Sale_order_optionSearchContext context) {
        Page<Sale_order_option> domains = sale_order_optionService.searchDefault(context) ;
        List<Sale_order_optionDTO> list = sale_order_optionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_option-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_option-Get')")
	@ApiOperation(value = "查询数据集", tags = {"销售选项" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_order_options/searchdefault")
	public ResponseEntity<Page<Sale_order_optionDTO>> searchDefault(@RequestBody Sale_order_optionSearchContext context) {
        Page<Sale_order_option> domains = sale_order_optionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_order_optionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

