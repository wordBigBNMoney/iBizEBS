package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_accountDTO]
 */
@Data
public class Account_accountDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [OPENING_DEBIT]
     *
     */
    @JSONField(name = "opening_debit")
    @JsonProperty("opening_debit")
    private BigDecimal openingDebit;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String note;

    /**
     * 属性 [OPENING_CREDIT]
     *
     */
    @JSONField(name = "opening_credit")
    @JsonProperty("opening_credit")
    private BigDecimal openingCredit;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    @NotBlank(message = "[代码]不允许为空!")
    @Size(min = 0, max = 64, message = "内容长度必须小于等于[64]")
    private String code;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String taxIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DEPRECATED]
     *
     */
    @JSONField(name = "deprecated")
    @JsonProperty("deprecated")
    private Boolean deprecated;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [RECONCILE]
     *
     */
    @JSONField(name = "reconcile")
    @JsonProperty("reconcile")
    private Boolean reconcile;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String tagIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [INTERNAL_TYPE]
     *
     */
    @JSONField(name = "internal_type")
    @JsonProperty("internal_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String internalType;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [INTERNAL_GROUP]
     *
     */
    @JSONField(name = "internal_group")
    @JsonProperty("internal_group")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String internalGroup;

    /**
     * 属性 [GROUP_ID_TEXT]
     *
     */
    @JSONField(name = "group_id_text")
    @JsonProperty("group_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String groupIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [USER_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "user_type_id_text")
    @JsonProperty("user_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userTypeIdText;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;

    /**
     * 属性 [USER_TYPE_ID]
     *
     */
    @JSONField(name = "user_type_id")
    @JsonProperty("user_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[类型]不允许为空!")
    private Long userTypeId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [DEPRECATED]
     */
    public void setDeprecated(Boolean  deprecated){
        this.deprecated = deprecated ;
        this.modify("deprecated",deprecated);
    }

    /**
     * 设置 [RECONCILE]
     */
    public void setReconcile(Boolean  reconcile){
        this.reconcile = reconcile ;
        this.modify("reconcile",reconcile);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [GROUP_ID]
     */
    public void setGroupId(Long  groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }

    /**
     * 设置 [USER_TYPE_ID]
     */
    public void setUserTypeId(Long  userTypeId){
        this.userTypeId = userTypeId ;
        this.modify("user_type_id",userTypeId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }


}


