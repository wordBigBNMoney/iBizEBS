package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Calendar_eventDTO]
 */
@Data
public class Calendar_eventDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [LOCATION]
     *
     */
    @JSONField(name = "location")
    @JsonProperty("location")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String location;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [RECURRENT_ID_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "recurrent_id_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("recurrent_id_date")
    private Timestamp recurrentIdDate;

    /**
     * 属性 [SA]
     *
     */
    @JSONField(name = "sa")
    @JsonProperty("sa")
    private Boolean sa;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [RECURRENCY]
     *
     */
    @JSONField(name = "recurrency")
    @JsonProperty("recurrency")
    private Boolean recurrency;

    /**
     * 属性 [COUNT]
     *
     */
    @JSONField(name = "count")
    @JsonProperty("count")
    private Integer count;

    /**
     * 属性 [FR]
     *
     */
    @JSONField(name = "fr")
    @JsonProperty("fr")
    private Boolean fr;

    /**
     * 属性 [RRULE]
     *
     */
    @JSONField(name = "rrule")
    @JsonProperty("rrule")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String rrule;

    /**
     * 属性 [MONTH_BY]
     *
     */
    @JSONField(name = "month_by")
    @JsonProperty("month_by")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String monthBy;

    /**
     * 属性 [WEEK_LIST]
     *
     */
    @JSONField(name = "week_list")
    @JsonProperty("week_list")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String weekList;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [STOP]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("stop")
    @NotNull(message = "[停止]不允许为空!")
    private Timestamp stop;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [RECURRENT_ID]
     *
     */
    @JSONField(name = "recurrent_id")
    @JsonProperty("recurrent_id")
    private Integer recurrentId;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [END_TYPE]
     *
     */
    @JSONField(name = "end_type")
    @JsonProperty("end_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String endType;

    /**
     * 属性 [ATTENDEE_IDS]
     *
     */
    @JSONField(name = "attendee_ids")
    @JsonProperty("attendee_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String attendeeIds;

    /**
     * 属性 [ATTENDEE_STATUS]
     *
     */
    @JSONField(name = "attendee_status")
    @JsonProperty("attendee_status")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String attendeeStatus;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [RRULE_TYPE]
     *
     */
    @JSONField(name = "rrule_type")
    @JsonProperty("rrule_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rruleType;

    /**
     * 属性 [INTERVAL]
     *
     */
    @JSONField(name = "interval")
    @JsonProperty("interval")
    private Integer interval;

    /**
     * 属性 [PRIVACY]
     *
     */
    @JSONField(name = "privacy")
    @JsonProperty("privacy")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String privacy;

    /**
     * 属性 [DURATION]
     *
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 属性 [START_DATETIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start_datetime")
    private Timestamp startDatetime;

    /**
     * 属性 [IS_ATTENDEE]
     *
     */
    @JSONField(name = "is_attendee")
    @JsonProperty("is_attendee")
    private Boolean isAttendee;

    /**
     * 属性 [START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 属性 [MO]
     *
     */
    @JSONField(name = "mo")
    @JsonProperty("mo")
    private Boolean mo;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WE]
     *
     */
    @JSONField(name = "we")
    @JsonProperty("we")
    private Boolean we;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [DISPLAY_TIME]
     *
     */
    @JSONField(name = "display_time")
    @JsonProperty("display_time")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayTime;

    /**
     * 属性 [DISPLAY_START]
     *
     */
    @JSONField(name = "display_start")
    @JsonProperty("display_start")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayStart;

    /**
     * 属性 [STOP_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop_date" , format="yyyy-MM-dd")
    @JsonProperty("stop_date")
    private Timestamp stopDate;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [FINAL_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "final_date" , format="yyyy-MM-dd")
    @JsonProperty("final_date")
    private Timestamp finalDate;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;

    /**
     * 属性 [IS_HIGHLIGHTED]
     *
     */
    @JSONField(name = "is_highlighted")
    @JsonProperty("is_highlighted")
    private Boolean isHighlighted;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [STOP_DATETIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("stop_datetime")
    private Timestamp stopDatetime;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resModel;

    /**
     * 属性 [TU]
     *
     */
    @JSONField(name = "tu")
    @JsonProperty("tu")
    private Boolean tu;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SHOW_AS]
     *
     */
    @JSONField(name = "show_as")
    @JsonProperty("show_as")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String showAs;

    /**
     * 属性 [CATEG_IDS]
     *
     */
    @JSONField(name = "categ_ids")
    @JsonProperty("categ_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String categIds;

    /**
     * 属性 [ALARM_IDS]
     *
     */
    @JSONField(name = "alarm_ids")
    @JsonProperty("alarm_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String alarmIds;

    /**
     * 属性 [TH]
     *
     */
    @JSONField(name = "th")
    @JsonProperty("th")
    private Boolean th;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [BYDAY]
     *
     */
    @JSONField(name = "byday")
    @JsonProperty("byday")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String byday;

    /**
     * 属性 [DAY]
     *
     */
    @JSONField(name = "day")
    @JsonProperty("day")
    private Integer day;

    /**
     * 属性 [START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start")
    @NotNull(message = "[开始]不允许为空!")
    private Timestamp start;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String partnerIds;

    /**
     * 属性 [ALLDAY]
     *
     */
    @JSONField(name = "allday")
    @JsonProperty("allday")
    private Boolean allday;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [SU]
     *
     */
    @JSONField(name = "su")
    @JsonProperty("su")
    private Boolean su;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[会议主题]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [OPPORTUNITY_ID_TEXT]
     *
     */
    @JSONField(name = "opportunity_id_text")
    @JsonProperty("opportunity_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String opportunityIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [APPLICANT_ID_TEXT]
     *
     */
    @JSONField(name = "applicant_id_text")
    @JsonProperty("applicant_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String applicantIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [APPLICANT_ID]
     *
     */
    @JSONField(name = "applicant_id")
    @JsonProperty("applicant_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long applicantId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [OPPORTUNITY_ID]
     *
     */
    @JSONField(name = "opportunity_id")
    @JsonProperty("opportunity_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long opportunityId;


    /**
     * 设置 [LOCATION]
     */
    public void setLocation(String  location){
        this.location = location ;
        this.modify("location",location);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [RECURRENT_ID_DATE]
     */
    public void setRecurrentIdDate(Timestamp  recurrentIdDate){
        this.recurrentIdDate = recurrentIdDate ;
        this.modify("recurrent_id_date",recurrentIdDate);
    }

    /**
     * 设置 [SA]
     */
    public void setSa(Boolean  sa){
        this.sa = sa ;
        this.modify("sa",sa);
    }

    /**
     * 设置 [RECURRENCY]
     */
    public void setRecurrency(Boolean  recurrency){
        this.recurrency = recurrency ;
        this.modify("recurrency",recurrency);
    }

    /**
     * 设置 [COUNT]
     */
    public void setCount(Integer  count){
        this.count = count ;
        this.modify("count",count);
    }

    /**
     * 设置 [FR]
     */
    public void setFr(Boolean  fr){
        this.fr = fr ;
        this.modify("fr",fr);
    }

    /**
     * 设置 [RRULE]
     */
    public void setRrule(String  rrule){
        this.rrule = rrule ;
        this.modify("rrule",rrule);
    }

    /**
     * 设置 [MONTH_BY]
     */
    public void setMonthBy(String  monthBy){
        this.monthBy = monthBy ;
        this.modify("month_by",monthBy);
    }

    /**
     * 设置 [WEEK_LIST]
     */
    public void setWeekList(String  weekList){
        this.weekList = weekList ;
        this.modify("week_list",weekList);
    }

    /**
     * 设置 [STOP]
     */
    public void setStop(Timestamp  stop){
        this.stop = stop ;
        this.modify("stop",stop);
    }

    /**
     * 设置 [RES_ID]
     */
    public void setResId(Integer  resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [RECURRENT_ID]
     */
    public void setRecurrentId(Integer  recurrentId){
        this.recurrentId = recurrentId ;
        this.modify("recurrent_id",recurrentId);
    }

    /**
     * 设置 [END_TYPE]
     */
    public void setEndType(String  endType){
        this.endType = endType ;
        this.modify("end_type",endType);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [RRULE_TYPE]
     */
    public void setRruleType(String  rruleType){
        this.rruleType = rruleType ;
        this.modify("rrule_type",rruleType);
    }

    /**
     * 设置 [INTERVAL]
     */
    public void setInterval(Integer  interval){
        this.interval = interval ;
        this.modify("interval",interval);
    }

    /**
     * 设置 [PRIVACY]
     */
    public void setPrivacy(String  privacy){
        this.privacy = privacy ;
        this.modify("privacy",privacy);
    }

    /**
     * 设置 [DURATION]
     */
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [START_DATETIME]
     */
    public void setStartDatetime(Timestamp  startDatetime){
        this.startDatetime = startDatetime ;
        this.modify("start_datetime",startDatetime);
    }

    /**
     * 设置 [START_DATE]
     */
    public void setStartDate(Timestamp  startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 设置 [MO]
     */
    public void setMo(Boolean  mo){
        this.mo = mo ;
        this.modify("mo",mo);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [WE]
     */
    public void setWe(Boolean  we){
        this.we = we ;
        this.modify("we",we);
    }

    /**
     * 设置 [DISPLAY_START]
     */
    public void setDisplayStart(String  displayStart){
        this.displayStart = displayStart ;
        this.modify("display_start",displayStart);
    }

    /**
     * 设置 [STOP_DATE]
     */
    public void setStopDate(Timestamp  stopDate){
        this.stopDate = stopDate ;
        this.modify("stop_date",stopDate);
    }

    /**
     * 设置 [FINAL_DATE]
     */
    public void setFinalDate(Timestamp  finalDate){
        this.finalDate = finalDate ;
        this.modify("final_date",finalDate);
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    public void setResModelId(Integer  resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }

    /**
     * 设置 [STOP_DATETIME]
     */
    public void setStopDatetime(Timestamp  stopDatetime){
        this.stopDatetime = stopDatetime ;
        this.modify("stop_datetime",stopDatetime);
    }

    /**
     * 设置 [RES_MODEL]
     */
    public void setResModel(String  resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [TU]
     */
    public void setTu(Boolean  tu){
        this.tu = tu ;
        this.modify("tu",tu);
    }

    /**
     * 设置 [SHOW_AS]
     */
    public void setShowAs(String  showAs){
        this.showAs = showAs ;
        this.modify("show_as",showAs);
    }

    /**
     * 设置 [TH]
     */
    public void setTh(Boolean  th){
        this.th = th ;
        this.modify("th",th);
    }

    /**
     * 设置 [BYDAY]
     */
    public void setByday(String  byday){
        this.byday = byday ;
        this.modify("byday",byday);
    }

    /**
     * 设置 [DAY]
     */
    public void setDay(Integer  day){
        this.day = day ;
        this.modify("day",day);
    }

    /**
     * 设置 [START]
     */
    public void setStart(Timestamp  start){
        this.start = start ;
        this.modify("start",start);
    }

    /**
     * 设置 [ALLDAY]
     */
    public void setAllday(Boolean  allday){
        this.allday = allday ;
        this.modify("allday",allday);
    }

    /**
     * 设置 [SU]
     */
    public void setSu(Boolean  su){
        this.su = su ;
        this.modify("su",su);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [APPLICANT_ID]
     */
    public void setApplicantId(Long  applicantId){
        this.applicantId = applicantId ;
        this.modify("applicant_id",applicantId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [OPPORTUNITY_ID]
     */
    public void setOpportunityId(Long  opportunityId){
        this.opportunityId = opportunityId ;
        this.modify("opportunity_id",opportunityId);
    }


}


