package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_orderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"采购订单" })
@RestController("Core-purchase_order")
@RequestMapping("")
public class Purchase_orderResource {

    @Autowired
    public IPurchase_orderService purchase_orderService;

    @Autowired
    @Lazy
    public Purchase_orderMapping purchase_orderMapping;

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdto),'iBizBusinessCentral-Purchase_order-Create')")
    @ApiOperation(value = "新建采购订单", tags = {"采购订单" },  notes = "新建采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders")
    public ResponseEntity<Purchase_orderDTO> create(@Validated @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
		purchase_orderService.create(domain);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdtos),'iBizBusinessCentral-Purchase_order-Create')")
    @ApiOperation(value = "批量新建采购订单", tags = {"采购订单" },  notes = "批量新建采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        purchase_orderService.createBatch(purchase_orderMapping.toDomain(purchase_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_orderService.get(#purchase_order_id),'iBizBusinessCentral-Purchase_order-Update')")
    @ApiOperation(value = "更新采购订单", tags = {"采购订单" },  notes = "更新采购订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> update(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
		Purchase_order domain  = purchase_orderMapping.toDomain(purchase_orderdto);
        domain .setId(purchase_order_id);
		purchase_orderService.update(domain );
		Purchase_orderDTO dto = purchase_orderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.getPurchaseOrderByEntities(this.purchase_orderMapping.toDomain(#purchase_orderdtos)),'iBizBusinessCentral-Purchase_order-Update')")
    @ApiOperation(value = "批量更新采购订单", tags = {"采购订单" },  notes = "批量更新采购订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        purchase_orderService.updateBatch(purchase_orderMapping.toDomain(purchase_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.get(#purchase_order_id),'iBizBusinessCentral-Purchase_order-Remove')")
    @ApiOperation(value = "删除采购订单", tags = {"采购订单" },  notes = "删除采购订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("purchase_order_id") Long purchase_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.remove(purchase_order_id));
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.getPurchaseOrderByIds(#ids),'iBizBusinessCentral-Purchase_order-Remove')")
    @ApiOperation(value = "批量删除采购订单", tags = {"采购订单" },  notes = "批量删除采购订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        purchase_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order-Get')")
    @ApiOperation(value = "获取采购订单", tags = {"采购订单" },  notes = "获取采购订单")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> get(@PathVariable("purchase_order_id") Long purchase_order_id) {
        Purchase_order domain = purchase_orderService.get(purchase_order_id);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取采购订单草稿", tags = {"采购订单" },  notes = "获取采购订单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/getdraft")
    public ResponseEntity<Purchase_orderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderMapping.toDto(purchase_orderService.getDraft(new Purchase_order())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_approve-all')")
    @ApiOperation(value = "批准", tags = {"采购订单" },  notes = "批准")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/button_approve")
    public ResponseEntity<Purchase_orderDTO> button_approve(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setId(purchase_order_id);
        domain = purchase_orderService.button_approve(domain);
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_cancel-all')")
    @ApiOperation(value = "取消", tags = {"采购订单" },  notes = "取消")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/button_cancel")
    public ResponseEntity<Purchase_orderDTO> button_cancel(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setId(purchase_order_id);
        domain = purchase_orderService.button_cancel(domain);
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_confirm-all')")
    @ApiOperation(value = "确认订单", tags = {"采购订单" },  notes = "确认订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/button_confirm")
    public ResponseEntity<Purchase_orderDTO> button_confirm(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setId(purchase_order_id);
        domain = purchase_orderService.button_confirm(domain);
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_done-all')")
    @ApiOperation(value = "锁定", tags = {"采购订单" },  notes = "锁定")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/button_done")
    public ResponseEntity<Purchase_orderDTO> button_done(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setId(purchase_order_id);
        domain = purchase_orderService.button_done(domain);
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_unlock-all')")
    @ApiOperation(value = "解锁", tags = {"采购订单" },  notes = "解锁")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/button_unlock")
    public ResponseEntity<Purchase_orderDTO> button_unlock(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setId(purchase_order_id);
        domain = purchase_orderService.button_unlock(domain);
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Calc_amount-all')")
    @ApiOperation(value = "计算订单金额", tags = {"采购订单" },  notes = "计算订单金额")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/calc_amount")
    public ResponseEntity<Purchase_orderDTO> calc_amount(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setId(purchase_order_id);
        domain = purchase_orderService.calc_amount(domain);
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @ApiOperation(value = "检查采购订单", tags = {"采购订单" },  notes = "检查采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Purchase_orderDTO purchase_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.checkKey(purchase_orderMapping.toDomain(purchase_orderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Get_name-all')")
    @ApiOperation(value = "获取订单编号", tags = {"采购订单" },  notes = "获取订单编号")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/{purchase_order_id}/get_name")
    public ResponseEntity<Purchase_orderDTO> get_name(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setId(purchase_order_id);
        domain = purchase_orderService.get_name(domain);
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdto),'iBizBusinessCentral-Purchase_order-Save')")
    @ApiOperation(value = "保存采购订单", tags = {"采购订单" },  notes = "保存采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/save")
    public ResponseEntity<Boolean> save(@RequestBody Purchase_orderDTO purchase_orderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.save(purchase_orderMapping.toDomain(purchase_orderdto)));
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdtos),'iBizBusinessCentral-Purchase_order-Save')")
    @ApiOperation(value = "批量保存采购订单", tags = {"采购订单" },  notes = "批量保存采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        purchase_orderService.saveBatch(purchase_orderMapping.toDomain(purchase_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "获取数据集", tags = {"采购订单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_orders/fetchdefault")
	public ResponseEntity<List<Purchase_orderDTO>> fetchDefault(Purchase_orderSearchContext context) {
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "查询数据集", tags = {"采购订单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_orders/searchdefault")
	public ResponseEntity<Page<Purchase_orderDTO>> searchDefault(@RequestBody Purchase_orderSearchContext context) {
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "获取首选表格", tags = {"采购订单" } ,notes = "获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_orders/fetchmaster")
	public ResponseEntity<List<Purchase_orderDTO>> fetchMaster(Purchase_orderSearchContext context) {
        Page<Purchase_order> domains = purchase_orderService.searchMaster(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "查询首选表格", tags = {"采购订单" } ,notes = "查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_orders/searchmaster")
	public ResponseEntity<Page<Purchase_orderDTO>> searchMaster(@RequestBody Purchase_orderSearchContext context) {
        Page<Purchase_order> domains = purchase_orderService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchOrder-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "获取订单", tags = {"采购订单" } ,notes = "获取订单")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_orders/fetchorder")
	public ResponseEntity<List<Purchase_orderDTO>> fetchOrder(Purchase_orderSearchContext context) {
        Page<Purchase_order> domains = purchase_orderService.searchOrder(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchOrder-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "查询订单", tags = {"采购订单" } ,notes = "查询订单")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_orders/searchorder")
	public ResponseEntity<Page<Purchase_orderDTO>> searchOrder(@RequestBody Purchase_orderSearchContext context) {
        Page<Purchase_order> domains = purchase_orderService.searchOrder(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdto),'iBizBusinessCentral-Purchase_order-Create')")
    @ApiOperation(value = "根据采购申请建立采购订单", tags = {"采购订单" },  notes = "根据采购申请建立采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders")
    public ResponseEntity<Purchase_orderDTO> createByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
		purchase_orderService.create(domain);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdtos),'iBizBusinessCentral-Purchase_order-Create')")
    @ApiOperation(value = "根据采购申请批量建立采购订单", tags = {"采购订单" },  notes = "根据采购申请批量建立采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> createBatchByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
            domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_orderService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_orderService.get(#purchase_order_id),'iBizBusinessCentral-Purchase_order-Update')")
    @ApiOperation(value = "根据采购申请更新采购订单", tags = {"采购订单" },  notes = "根据采购申请更新采购订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> updateByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain.setId(purchase_order_id);
		purchase_orderService.update(domain);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.getPurchaseOrderByEntities(this.purchase_orderMapping.toDomain(#purchase_orderdtos)),'iBizBusinessCentral-Purchase_order-Update')")
    @ApiOperation(value = "根据采购申请批量更新采购订单", tags = {"采购订单" },  notes = "根据采购申请批量更新采购订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> updateBatchByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
            domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_orderService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.get(#purchase_order_id),'iBizBusinessCentral-Purchase_order-Remove')")
    @ApiOperation(value = "根据采购申请删除采购订单", tags = {"采购订单" },  notes = "根据采购申请删除采购订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Boolean> removeByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.remove(purchase_order_id));
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.getPurchaseOrderByIds(#ids),'iBizBusinessCentral-Purchase_order-Remove')")
    @ApiOperation(value = "根据采购申请批量删除采购订单", tags = {"采购订单" },  notes = "根据采购申请批量删除采购订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> removeBatchByPurchase_requisition(@RequestBody List<Long> ids) {
        purchase_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order-Get')")
    @ApiOperation(value = "根据采购申请获取采购订单", tags = {"采购订单" },  notes = "根据采购申请获取采购订单")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> getByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
        Purchase_order domain = purchase_orderService.get(purchase_order_id);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据采购申请获取采购订单草稿", tags = {"采购订单" },  notes = "根据采购申请获取采购订单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/getdraft")
    public ResponseEntity<Purchase_orderDTO> getDraftByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id) {
        Purchase_order domain = new Purchase_order();
        domain.setRequisitionId(purchase_requisition_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderMapping.toDto(purchase_orderService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_approve-all')")
    @ApiOperation(value = "根据采购申请采购订单", tags = {"采购订单" },  notes = "根据采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_approve")
    public ResponseEntity<Purchase_orderDTO> button_approveByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_approve(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_cancel-all')")
    @ApiOperation(value = "根据采购申请采购订单", tags = {"采购订单" },  notes = "根据采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_cancel")
    public ResponseEntity<Purchase_orderDTO> button_cancelByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_cancel(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_confirm-all')")
    @ApiOperation(value = "根据采购申请采购订单", tags = {"采购订单" },  notes = "根据采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_confirm")
    public ResponseEntity<Purchase_orderDTO> button_confirmByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_confirm(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_done-all')")
    @ApiOperation(value = "根据采购申请采购订单", tags = {"采购订单" },  notes = "根据采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_done")
    public ResponseEntity<Purchase_orderDTO> button_doneByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_done(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_unlock-all')")
    @ApiOperation(value = "根据采购申请采购订单", tags = {"采购订单" },  notes = "根据采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_unlock")
    public ResponseEntity<Purchase_orderDTO> button_unlockByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_unlock(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Calc_amount-all')")
    @ApiOperation(value = "根据采购申请采购订单", tags = {"采购订单" },  notes = "根据采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/calc_amount")
    public ResponseEntity<Purchase_orderDTO> calc_amountByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.calc_amount(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @ApiOperation(value = "根据采购申请检查采购订单", tags = {"采购订单" },  notes = "根据采购申请检查采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/checkkey")
    public ResponseEntity<Boolean> checkKeyByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.checkKey(purchase_orderMapping.toDomain(purchase_orderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Get_name-all')")
    @ApiOperation(value = "根据采购申请采购订单", tags = {"采购订单" },  notes = "根据采购申请采购订单")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/get_name")
    public ResponseEntity<Purchase_orderDTO> get_nameByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.get_name(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdto),'iBizBusinessCentral-Purchase_order-Save')")
    @ApiOperation(value = "根据采购申请保存采购订单", tags = {"采购订单" },  notes = "根据采购申请保存采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/save")
    public ResponseEntity<Boolean> saveByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdtos),'iBizBusinessCentral-Purchase_order-Save')")
    @ApiOperation(value = "根据采购申请批量保存采购订单", tags = {"采购订单" },  notes = "根据采购申请批量保存采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/savebatch")
    public ResponseEntity<Boolean> saveBatchByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
             domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_orderService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据采购申请获取数据集", tags = {"采购订单" } ,notes = "根据采购申请获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/fetchdefault")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderDefaultByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id,Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据采购申请查询数据集", tags = {"采购订单" } ,notes = "根据采购申请查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/searchdefault")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderDefaultByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据采购申请获取首选表格", tags = {"采购订单" } ,notes = "根据采购申请获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/fetchmaster")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderMasterByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id,Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchMaster(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据采购申请查询首选表格", tags = {"采购订单" } ,notes = "根据采购申请查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/searchmaster")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderMasterByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchOrder-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据采购申请获取订单", tags = {"采购订单" } ,notes = "根据采购申请获取订单")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/fetchorder")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderOrderByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id,Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchOrder(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchOrder-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据采购申请查询订单", tags = {"采购订单" } ,notes = "根据采购申请查询订单")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/searchorder")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderOrderByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchOrder(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdto),'iBizBusinessCentral-Purchase_order-Create')")
    @ApiOperation(value = "根据供应商建立采购订单", tags = {"采购订单" },  notes = "根据供应商建立采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders")
    public ResponseEntity<Purchase_orderDTO> createByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
		purchase_orderService.create(domain);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdtos),'iBizBusinessCentral-Purchase_order-Create')")
    @ApiOperation(value = "根据供应商批量建立采购订单", tags = {"采购订单" },  notes = "根据供应商批量建立采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
            domain.setPartnerId(res_supplier_id);
        }
        purchase_orderService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_orderService.get(#purchase_order_id),'iBizBusinessCentral-Purchase_order-Update')")
    @ApiOperation(value = "根据供应商更新采购订单", tags = {"采购订单" },  notes = "根据供应商更新采购订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> updateByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        domain.setId(purchase_order_id);
		purchase_orderService.update(domain);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.getPurchaseOrderByEntities(this.purchase_orderMapping.toDomain(#purchase_orderdtos)),'iBizBusinessCentral-Purchase_order-Update')")
    @ApiOperation(value = "根据供应商批量更新采购订单", tags = {"采购订单" },  notes = "根据供应商批量更新采购订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
            domain.setPartnerId(res_supplier_id);
        }
        purchase_orderService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.get(#purchase_order_id),'iBizBusinessCentral-Purchase_order-Remove')")
    @ApiOperation(value = "根据供应商删除采购订单", tags = {"采购订单" },  notes = "根据供应商删除采购订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Boolean> removeByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.remove(purchase_order_id));
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.getPurchaseOrderByIds(#ids),'iBizBusinessCentral-Purchase_order-Remove')")
    @ApiOperation(value = "根据供应商批量删除采购订单", tags = {"采购订单" },  notes = "根据供应商批量删除采购订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplier(@RequestBody List<Long> ids) {
        purchase_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order-Get')")
    @ApiOperation(value = "根据供应商获取采购订单", tags = {"采购订单" },  notes = "根据供应商获取采购订单")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> getByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
        Purchase_order domain = purchase_orderService.get(purchase_order_id);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商获取采购订单草稿", tags = {"采购订单" },  notes = "根据供应商获取采购订单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_orders/getdraft")
    public ResponseEntity<Purchase_orderDTO> getDraftByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id) {
        Purchase_order domain = new Purchase_order();
        domain.setPartnerId(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderMapping.toDto(purchase_orderService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_approve-all')")
    @ApiOperation(value = "根据供应商采购订单", tags = {"采购订单" },  notes = "根据供应商采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/button_approve")
    public ResponseEntity<Purchase_orderDTO> button_approveByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        domain = purchase_orderService.button_approve(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_cancel-all')")
    @ApiOperation(value = "根据供应商采购订单", tags = {"采购订单" },  notes = "根据供应商采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/button_cancel")
    public ResponseEntity<Purchase_orderDTO> button_cancelByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        domain = purchase_orderService.button_cancel(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_confirm-all')")
    @ApiOperation(value = "根据供应商采购订单", tags = {"采购订单" },  notes = "根据供应商采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/button_confirm")
    public ResponseEntity<Purchase_orderDTO> button_confirmByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        domain = purchase_orderService.button_confirm(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_done-all')")
    @ApiOperation(value = "根据供应商采购订单", tags = {"采购订单" },  notes = "根据供应商采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/button_done")
    public ResponseEntity<Purchase_orderDTO> button_doneByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        domain = purchase_orderService.button_done(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_unlock-all')")
    @ApiOperation(value = "根据供应商采购订单", tags = {"采购订单" },  notes = "根据供应商采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/button_unlock")
    public ResponseEntity<Purchase_orderDTO> button_unlockByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        domain = purchase_orderService.button_unlock(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Calc_amount-all')")
    @ApiOperation(value = "根据供应商采购订单", tags = {"采购订单" },  notes = "根据供应商采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/calc_amount")
    public ResponseEntity<Purchase_orderDTO> calc_amountByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        domain = purchase_orderService.calc_amount(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @ApiOperation(value = "根据供应商检查采购订单", tags = {"采购订单" },  notes = "根据供应商检查采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.checkKey(purchase_orderMapping.toDomain(purchase_orderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Get_name-all')")
    @ApiOperation(value = "根据供应商采购订单", tags = {"采购订单" },  notes = "根据供应商采购订单")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/get_name")
    public ResponseEntity<Purchase_orderDTO> get_nameByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        domain = purchase_orderService.get_name(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdto),'iBizBusinessCentral-Purchase_order-Save')")
    @ApiOperation(value = "根据供应商保存采购订单", tags = {"采购订单" },  notes = "根据供应商保存采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/save")
    public ResponseEntity<Boolean> saveByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setPartnerId(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdtos),'iBizBusinessCentral-Purchase_order-Save')")
    @ApiOperation(value = "根据供应商批量保存采购订单", tags = {"采购订单" },  notes = "根据供应商批量保存采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
             domain.setPartnerId(res_supplier_id);
        }
        purchase_orderService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商获取数据集", tags = {"采购订单" } ,notes = "根据供应商获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_orders/fetchdefault")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id,Purchase_orderSearchContext context) {
        context.setN_partner_id_eq(res_supplier_id);
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商查询数据集", tags = {"采购订单" } ,notes = "根据供应商查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_orders/searchdefault")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_partner_id_eq(res_supplier_id);
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商获取首选表格", tags = {"采购订单" } ,notes = "根据供应商获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_orders/fetchmaster")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderMasterByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id,Purchase_orderSearchContext context) {
        context.setN_partner_id_eq(res_supplier_id);
        Page<Purchase_order> domains = purchase_orderService.searchMaster(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商查询首选表格", tags = {"采购订单" } ,notes = "根据供应商查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_orders/searchmaster")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderMasterByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_partner_id_eq(res_supplier_id);
        Page<Purchase_order> domains = purchase_orderService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchOrder-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商获取订单", tags = {"采购订单" } ,notes = "根据供应商获取订单")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_orders/fetchorder")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderOrderByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id,Purchase_orderSearchContext context) {
        context.setN_partner_id_eq(res_supplier_id);
        Page<Purchase_order> domains = purchase_orderService.searchOrder(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchOrder-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商查询订单", tags = {"采购订单" } ,notes = "根据供应商查询订单")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_orders/searchorder")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderOrderByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_partner_id_eq(res_supplier_id);
        Page<Purchase_order> domains = purchase_orderService.searchOrder(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdto),'iBizBusinessCentral-Purchase_order-Create')")
    @ApiOperation(value = "根据供应商采购申请建立采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请建立采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders")
    public ResponseEntity<Purchase_orderDTO> createByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
		purchase_orderService.create(domain);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdtos),'iBizBusinessCentral-Purchase_order-Create')")
    @ApiOperation(value = "根据供应商采购申请批量建立采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请批量建立采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
            domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_orderService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_orderService.get(#purchase_order_id),'iBizBusinessCentral-Purchase_order-Update')")
    @ApiOperation(value = "根据供应商采购申请更新采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请更新采购订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> updateByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain.setId(purchase_order_id);
		purchase_orderService.update(domain);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.getPurchaseOrderByEntities(this.purchase_orderMapping.toDomain(#purchase_orderdtos)),'iBizBusinessCentral-Purchase_order-Update')")
    @ApiOperation(value = "根据供应商采购申请批量更新采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请批量更新采购订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
            domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_orderService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.get(#purchase_order_id),'iBizBusinessCentral-Purchase_order-Remove')")
    @ApiOperation(value = "根据供应商采购申请删除采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请删除采购订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Boolean> removeByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.remove(purchase_order_id));
    }

    @PreAuthorize("hasPermission(this.purchase_orderService.getPurchaseOrderByIds(#ids),'iBizBusinessCentral-Purchase_order-Remove')")
    @ApiOperation(value = "根据供应商采购申请批量删除采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请批量删除采购订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplierPurchase_requisition(@RequestBody List<Long> ids) {
        purchase_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order-Get')")
    @ApiOperation(value = "根据供应商采购申请获取采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请获取采购订单")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> getByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
        Purchase_order domain = purchase_orderService.get(purchase_order_id);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商采购申请获取采购订单草稿", tags = {"采购订单" },  notes = "根据供应商采购申请获取采购订单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/getdraft")
    public ResponseEntity<Purchase_orderDTO> getDraftByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id) {
        Purchase_order domain = new Purchase_order();
        domain.setRequisitionId(purchase_requisition_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderMapping.toDto(purchase_orderService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_approve-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_approve")
    public ResponseEntity<Purchase_orderDTO> button_approveByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_approve(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_cancel-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_cancel")
    public ResponseEntity<Purchase_orderDTO> button_cancelByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_cancel(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_confirm-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_confirm")
    public ResponseEntity<Purchase_orderDTO> button_confirmByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_confirm(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_done-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_done")
    public ResponseEntity<Purchase_orderDTO> button_doneByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_done(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Button_unlock-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/button_unlock")
    public ResponseEntity<Purchase_orderDTO> button_unlockByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.button_unlock(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Calc_amount-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/calc_amount")
    public ResponseEntity<Purchase_orderDTO> calc_amountByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.calc_amount(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @ApiOperation(value = "根据供应商采购申请检查采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请检查采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.checkKey(purchase_orderMapping.toDomain(purchase_orderdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-Get_name-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请采购订单")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/get_name")
    public ResponseEntity<Purchase_orderDTO> get_nameByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_orderService.get_name(domain) ;
        purchase_orderdto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderdto);
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdto),'iBizBusinessCentral-Purchase_order-Save')")
    @ApiOperation(value = "根据供应商采购申请保存采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请保存采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/save")
    public ResponseEntity<Boolean> saveByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setRequisitionId(purchase_requisition_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_orderMapping.toDomain(#purchase_orderdtos),'iBizBusinessCentral-Purchase_order-Save')")
    @ApiOperation(value = "根据供应商采购申请批量保存采购订单", tags = {"采购订单" },  notes = "根据供应商采购申请批量保存采购订单")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        List<Purchase_order> domainlist=purchase_orderMapping.toDomain(purchase_orderdtos);
        for(Purchase_order domain:domainlist){
             domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_orderService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商采购申请获取数据集", tags = {"采购订单" } ,notes = "根据供应商采购申请获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/fetchdefault")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderDefaultByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id,Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商采购申请查询数据集", tags = {"采购订单" } ,notes = "根据供应商采购申请查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/searchdefault")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderDefaultByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商采购申请获取首选表格", tags = {"采购订单" } ,notes = "根据供应商采购申请获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/fetchmaster")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderMasterByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id,Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchMaster(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商采购申请查询首选表格", tags = {"采购订单" } ,notes = "根据供应商采购申请查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/searchmaster")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderMasterByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchOrder-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商采购申请获取订单", tags = {"采购订单" } ,notes = "根据供应商采购申请获取订单")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/fetchorder")
	public ResponseEntity<List<Purchase_orderDTO>> fetchPurchase_orderOrderByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id,Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchOrder(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order-searchOrder-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order-Get')")
	@ApiOperation(value = "根据供应商采购申请查询订单", tags = {"采购订单" } ,notes = "根据供应商采购申请查询订单")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/searchorder")
	public ResponseEntity<Page<Purchase_orderDTO>> searchPurchase_orderOrderByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_orderSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_order> domains = purchase_orderService.searchOrder(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

