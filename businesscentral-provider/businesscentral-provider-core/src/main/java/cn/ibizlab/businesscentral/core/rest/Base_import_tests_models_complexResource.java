package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_complex;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_complexService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_complexSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型复合体" })
@RestController("Core-base_import_tests_models_complex")
@RequestMapping("")
public class Base_import_tests_models_complexResource {

    @Autowired
    public IBase_import_tests_models_complexService base_import_tests_models_complexService;

    @Autowired
    @Lazy
    public Base_import_tests_models_complexMapping base_import_tests_models_complexMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_complexMapping.toDomain(#base_import_tests_models_complexdto),'iBizBusinessCentral-Base_import_tests_models_complex-Create')")
    @ApiOperation(value = "新建测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "新建测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices")
    public ResponseEntity<Base_import_tests_models_complexDTO> create(@Validated @RequestBody Base_import_tests_models_complexDTO base_import_tests_models_complexdto) {
        Base_import_tests_models_complex domain = base_import_tests_models_complexMapping.toDomain(base_import_tests_models_complexdto);
		base_import_tests_models_complexService.create(domain);
        Base_import_tests_models_complexDTO dto = base_import_tests_models_complexMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_complexMapping.toDomain(#base_import_tests_models_complexdtos),'iBizBusinessCentral-Base_import_tests_models_complex-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "批量新建测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_complexDTO> base_import_tests_models_complexdtos) {
        base_import_tests_models_complexService.createBatch(base_import_tests_models_complexMapping.toDomain(base_import_tests_models_complexdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_complex" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_complexService.get(#base_import_tests_models_complex_id),'iBizBusinessCentral-Base_import_tests_models_complex-Update')")
    @ApiOperation(value = "更新测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "更新测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_complices/{base_import_tests_models_complex_id}")
    public ResponseEntity<Base_import_tests_models_complexDTO> update(@PathVariable("base_import_tests_models_complex_id") Long base_import_tests_models_complex_id, @RequestBody Base_import_tests_models_complexDTO base_import_tests_models_complexdto) {
		Base_import_tests_models_complex domain  = base_import_tests_models_complexMapping.toDomain(base_import_tests_models_complexdto);
        domain .setId(base_import_tests_models_complex_id);
		base_import_tests_models_complexService.update(domain );
		Base_import_tests_models_complexDTO dto = base_import_tests_models_complexMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_complexService.getBaseImportTestsModelsComplexByEntities(this.base_import_tests_models_complexMapping.toDomain(#base_import_tests_models_complexdtos)),'iBizBusinessCentral-Base_import_tests_models_complex-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "批量更新测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_complices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_complexDTO> base_import_tests_models_complexdtos) {
        base_import_tests_models_complexService.updateBatch(base_import_tests_models_complexMapping.toDomain(base_import_tests_models_complexdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_complexService.get(#base_import_tests_models_complex_id),'iBizBusinessCentral-Base_import_tests_models_complex-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "删除测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_complices/{base_import_tests_models_complex_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_complex_id") Long base_import_tests_models_complex_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_complexService.remove(base_import_tests_models_complex_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_complexService.getBaseImportTestsModelsComplexByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_complex-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "批量删除测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_complices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_complexService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_complexMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_complex-Get')")
    @ApiOperation(value = "获取测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "获取测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_complices/{base_import_tests_models_complex_id}")
    public ResponseEntity<Base_import_tests_models_complexDTO> get(@PathVariable("base_import_tests_models_complex_id") Long base_import_tests_models_complex_id) {
        Base_import_tests_models_complex domain = base_import_tests_models_complexService.get(base_import_tests_models_complex_id);
        Base_import_tests_models_complexDTO dto = base_import_tests_models_complexMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型复合体草稿", tags = {"测试:基本导入模型复合体" },  notes = "获取测试:基本导入模型复合体草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_complices/getdraft")
    public ResponseEntity<Base_import_tests_models_complexDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_complexMapping.toDto(base_import_tests_models_complexService.getDraft(new Base_import_tests_models_complex())));
    }

    @ApiOperation(value = "检查测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "检查测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_complexDTO base_import_tests_models_complexdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_complexService.checkKey(base_import_tests_models_complexMapping.toDomain(base_import_tests_models_complexdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_complexMapping.toDomain(#base_import_tests_models_complexdto),'iBizBusinessCentral-Base_import_tests_models_complex-Save')")
    @ApiOperation(value = "保存测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "保存测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_complexDTO base_import_tests_models_complexdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_complexService.save(base_import_tests_models_complexMapping.toDomain(base_import_tests_models_complexdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_complexMapping.toDomain(#base_import_tests_models_complexdtos),'iBizBusinessCentral-Base_import_tests_models_complex-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型复合体", tags = {"测试:基本导入模型复合体" },  notes = "批量保存测试:基本导入模型复合体")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_complexDTO> base_import_tests_models_complexdtos) {
        base_import_tests_models_complexService.saveBatch(base_import_tests_models_complexMapping.toDomain(base_import_tests_models_complexdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_complex-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_complex-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型复合体" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_complices/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_complexDTO>> fetchDefault(Base_import_tests_models_complexSearchContext context) {
        Page<Base_import_tests_models_complex> domains = base_import_tests_models_complexService.searchDefault(context) ;
        List<Base_import_tests_models_complexDTO> list = base_import_tests_models_complexMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_complex-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_complex-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型复合体" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_complices/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_complexDTO>> searchDefault(@RequestBody Base_import_tests_models_complexSearchContext context) {
        Page<Base_import_tests_models_complex> domains = base_import_tests_models_complexService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_complexMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

