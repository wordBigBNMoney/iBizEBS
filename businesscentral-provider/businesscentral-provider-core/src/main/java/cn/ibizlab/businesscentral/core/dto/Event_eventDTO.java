package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Event_eventDTO]
 */
@Data
public class Event_eventDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SEATS_USED]
     *
     */
    @JSONField(name = "seats_used")
    @JsonProperty("seats_used")
    private Integer seatsUsed;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private Boolean isSeoOptimized;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [BADGE_FRONT]
     *
     */
    @JSONField(name = "badge_front")
    @JsonProperty("badge_front")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String badgeFront;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @NotBlank(message = "[状态]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [EVENT_MAIL_IDS]
     *
     */
    @JSONField(name = "event_mail_ids")
    @JsonProperty("event_mail_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String eventMailIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SEATS_EXPECTED]
     *
     */
    @JSONField(name = "seats_expected")
    @JsonProperty("seats_expected")
    private Integer seatsExpected;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [SEATS_AVAILABILITY]
     *
     */
    @JSONField(name = "seats_availability")
    @JsonProperty("seats_availability")
    @NotBlank(message = "[与会者最多人数]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String seatsAvailability;

    /**
     * 属性 [EVENT_TICKET_IDS]
     *
     */
    @JSONField(name = "event_ticket_ids")
    @JsonProperty("event_ticket_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String eventTicketIds;

    /**
     * 属性 [SEATS_UNCONFIRMED]
     *
     */
    @JSONField(name = "seats_unconfirmed")
    @JsonProperty("seats_unconfirmed")
    private Integer seatsUnconfirmed;

    /**
     * 属性 [IS_PARTICIPATING]
     *
     */
    @JSONField(name = "is_participating")
    @JsonProperty("is_participating")
    private Boolean isParticipating;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [TWITTER_HASHTAG]
     *
     */
    @JSONField(name = "twitter_hashtag")
    @JsonProperty("twitter_hashtag")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String twitterHashtag;

    /**
     * 属性 [SEATS_MAX]
     *
     */
    @JSONField(name = "seats_max")
    @JsonProperty("seats_max")
    private Integer seatsMax;

    /**
     * 属性 [DATE_BEGIN]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_begin" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_begin")
    @NotNull(message = "[开始日期]不允许为空!")
    private Timestamp dateBegin;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaOgImg;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaTitle;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [REGISTRATION_IDS]
     *
     */
    @JSONField(name = "registration_ids")
    @JsonProperty("registration_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String registrationIds;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [BADGE_INNERRIGHT]
     *
     */
    @JSONField(name = "badge_innerright")
    @JsonProperty("badge_innerright")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String badgeInnerright;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMetaDescription;

    /**
     * 属性 [SEATS_MIN]
     *
     */
    @JSONField(name = "seats_min")
    @JsonProperty("seats_min")
    private Integer seatsMin;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [BADGE_BACK]
     *
     */
    @JSONField(name = "badge_back")
    @JsonProperty("badge_back")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String badgeBack;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaKeywords;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [AUTO_CONFIRM]
     *
     */
    @JSONField(name = "auto_confirm")
    @JsonProperty("auto_confirm")
    private Boolean autoConfirm;

    /**
     * 属性 [DATE_END]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_end")
    @NotNull(message = "[结束日期]不允许为空!")
    private Timestamp dateEnd;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[活动]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [BADGE_INNERLEFT]
     *
     */
    @JSONField(name = "badge_innerleft")
    @JsonProperty("badge_innerleft")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String badgeInnerleft;

    /**
     * 属性 [SEATS_RESERVED]
     *
     */
    @JSONField(name = "seats_reserved")
    @JsonProperty("seats_reserved")
    private Integer seatsReserved;

    /**
     * 属性 [WEBSITE_MENU]
     *
     */
    @JSONField(name = "website_menu")
    @JsonProperty("website_menu")
    private Boolean websiteMenu;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [DATE_END_LOCATED]
     *
     */
    @JSONField(name = "date_end_located")
    @JsonProperty("date_end_located")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dateEndLocated;

    /**
     * 属性 [SEATS_AVAILABLE]
     *
     */
    @JSONField(name = "seats_available")
    @JsonProperty("seats_available")
    private Integer seatsAvailable;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [IS_ONLINE]
     *
     */
    @JSONField(name = "is_online")
    @JsonProperty("is_online")
    private Boolean isOnline;

    /**
     * 属性 [DATE_TZ]
     *
     */
    @JSONField(name = "date_tz")
    @JsonProperty("date_tz")
    @NotBlank(message = "[时区]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String dateTz;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [DATE_BEGIN_LOCATED]
     *
     */
    @JSONField(name = "date_begin_located")
    @JsonProperty("date_begin_located")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String dateBeginLocated;

    /**
     * 属性 [EVENT_LOGO]
     *
     */
    @JSONField(name = "event_logo")
    @JsonProperty("event_logo")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String eventLogo;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteUrl;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [EVENT_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "event_type_id_text")
    @JsonProperty("event_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String eventTypeIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ADDRESS_ID_TEXT]
     *
     */
    @JSONField(name = "address_id_text")
    @JsonProperty("address_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String addressIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String countryIdText;

    /**
     * 属性 [MENU_ID_TEXT]
     *
     */
    @JSONField(name = "menu_id_text")
    @JsonProperty("menu_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String menuIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [ORGANIZER_ID_TEXT]
     *
     */
    @JSONField(name = "organizer_id_text")
    @JsonProperty("organizer_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String organizerIdText;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [ORGANIZER_ID]
     *
     */
    @JSONField(name = "organizer_id")
    @JsonProperty("organizer_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long organizerId;

    /**
     * 属性 [MENU_ID]
     *
     */
    @JSONField(name = "menu_id")
    @JsonProperty("menu_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long menuId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [ADDRESS_ID]
     *
     */
    @JSONField(name = "address_id")
    @JsonProperty("address_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long addressId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [EVENT_TYPE_ID]
     *
     */
    @JSONField(name = "event_type_id")
    @JsonProperty("event_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long eventTypeId;


    /**
     * 设置 [SEATS_USED]
     */
    public void setSeatsUsed(Integer  seatsUsed){
        this.seatsUsed = seatsUsed ;
        this.modify("seats_used",seatsUsed);
    }

    /**
     * 设置 [BADGE_FRONT]
     */
    public void setBadgeFront(String  badgeFront){
        this.badgeFront = badgeFront ;
        this.modify("badge_front",badgeFront);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [SEATS_AVAILABILITY]
     */
    public void setSeatsAvailability(String  seatsAvailability){
        this.seatsAvailability = seatsAvailability ;
        this.modify("seats_availability",seatsAvailability);
    }

    /**
     * 设置 [SEATS_UNCONFIRMED]
     */
    public void setSeatsUnconfirmed(Integer  seatsUnconfirmed){
        this.seatsUnconfirmed = seatsUnconfirmed ;
        this.modify("seats_unconfirmed",seatsUnconfirmed);
    }

    /**
     * 设置 [TWITTER_HASHTAG]
     */
    public void setTwitterHashtag(String  twitterHashtag){
        this.twitterHashtag = twitterHashtag ;
        this.modify("twitter_hashtag",twitterHashtag);
    }

    /**
     * 设置 [SEATS_MAX]
     */
    public void setSeatsMax(Integer  seatsMax){
        this.seatsMax = seatsMax ;
        this.modify("seats_max",seatsMax);
    }

    /**
     * 设置 [DATE_BEGIN]
     */
    public void setDateBegin(Timestamp  dateBegin){
        this.dateBegin = dateBegin ;
        this.modify("date_begin",dateBegin);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    public void setWebsiteMetaOgImg(String  websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    public void setWebsiteMetaTitle(String  websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [BADGE_INNERRIGHT]
     */
    public void setBadgeInnerright(String  badgeInnerright){
        this.badgeInnerright = badgeInnerright ;
        this.modify("badge_innerright",badgeInnerright);
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    public void setWebsiteMetaDescription(String  websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }

    /**
     * 设置 [SEATS_MIN]
     */
    public void setSeatsMin(Integer  seatsMin){
        this.seatsMin = seatsMin ;
        this.modify("seats_min",seatsMin);
    }

    /**
     * 设置 [BADGE_BACK]
     */
    public void setBadgeBack(String  badgeBack){
        this.badgeBack = badgeBack ;
        this.modify("badge_back",badgeBack);
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    public void setWebsiteMetaKeywords(String  websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }

    /**
     * 设置 [AUTO_CONFIRM]
     */
    public void setAutoConfirm(Boolean  autoConfirm){
        this.autoConfirm = autoConfirm ;
        this.modify("auto_confirm",autoConfirm);
    }

    /**
     * 设置 [DATE_END]
     */
    public void setDateEnd(Timestamp  dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [BADGE_INNERLEFT]
     */
    public void setBadgeInnerleft(String  badgeInnerleft){
        this.badgeInnerleft = badgeInnerleft ;
        this.modify("badge_innerleft",badgeInnerleft);
    }

    /**
     * 设置 [SEATS_RESERVED]
     */
    public void setSeatsReserved(Integer  seatsReserved){
        this.seatsReserved = seatsReserved ;
        this.modify("seats_reserved",seatsReserved);
    }

    /**
     * 设置 [WEBSITE_MENU]
     */
    public void setWebsiteMenu(Boolean  websiteMenu){
        this.websiteMenu = websiteMenu ;
        this.modify("website_menu",websiteMenu);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [SEATS_AVAILABLE]
     */
    public void setSeatsAvailable(Integer  seatsAvailable){
        this.seatsAvailable = seatsAvailable ;
        this.modify("seats_available",seatsAvailable);
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    public void setIsPublished(Boolean  isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [IS_ONLINE]
     */
    public void setIsOnline(Boolean  isOnline){
        this.isOnline = isOnline ;
        this.modify("is_online",isOnline);
    }

    /**
     * 设置 [DATE_TZ]
     */
    public void setDateTz(String  dateTz){
        this.dateTz = dateTz ;
        this.modify("date_tz",dateTz);
    }

    /**
     * 设置 [EVENT_LOGO]
     */
    public void setEventLogo(String  eventLogo){
        this.eventLogo = eventLogo ;
        this.modify("event_logo",eventLogo);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Long  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [ORGANIZER_ID]
     */
    public void setOrganizerId(Long  organizerId){
        this.organizerId = organizerId ;
        this.modify("organizer_id",organizerId);
    }

    /**
     * 设置 [MENU_ID]
     */
    public void setMenuId(Long  menuId){
        this.menuId = menuId ;
        this.modify("menu_id",menuId);
    }

    /**
     * 设置 [ADDRESS_ID]
     */
    public void setAddressId(Long  addressId){
        this.addressId = addressId ;
        this.modify("address_id",addressId);
    }

    /**
     * 设置 [EVENT_TYPE_ID]
     */
    public void setEventTypeId(Long  eventTypeId){
        this.eventTypeId = eventTypeId ;
        this.modify("event_type_id",eventTypeId);
    }


}


