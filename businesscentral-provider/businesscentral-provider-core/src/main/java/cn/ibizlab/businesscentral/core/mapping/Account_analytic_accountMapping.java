package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account;
import cn.ibizlab.businesscentral.core.dto.Account_analytic_accountDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_analytic_accountMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_analytic_accountMapping extends MappingBase<Account_analytic_accountDTO, Account_analytic_account> {


}

