package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_tracking_valueService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_tracking_valueSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"邮件跟踪值" })
@RestController("Core-mail_tracking_value")
@RequestMapping("")
public class Mail_tracking_valueResource {

    @Autowired
    public IMail_tracking_valueService mail_tracking_valueService;

    @Autowired
    @Lazy
    public Mail_tracking_valueMapping mail_tracking_valueMapping;

    @PreAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedto),'iBizBusinessCentral-Mail_tracking_value-Create')")
    @ApiOperation(value = "新建邮件跟踪值", tags = {"邮件跟踪值" },  notes = "新建邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values")
    public ResponseEntity<Mail_tracking_valueDTO> create(@Validated @RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        Mail_tracking_value domain = mail_tracking_valueMapping.toDomain(mail_tracking_valuedto);
		mail_tracking_valueService.create(domain);
        Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedtos),'iBizBusinessCentral-Mail_tracking_value-Create')")
    @ApiOperation(value = "批量新建邮件跟踪值", tags = {"邮件跟踪值" },  notes = "批量新建邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        mail_tracking_valueService.createBatch(mail_tracking_valueMapping.toDomain(mail_tracking_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_tracking_value" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_tracking_valueService.get(#mail_tracking_value_id),'iBizBusinessCentral-Mail_tracking_value-Update')")
    @ApiOperation(value = "更新邮件跟踪值", tags = {"邮件跟踪值" },  notes = "更新邮件跟踪值")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_tracking_values/{mail_tracking_value_id}")
    public ResponseEntity<Mail_tracking_valueDTO> update(@PathVariable("mail_tracking_value_id") Long mail_tracking_value_id, @RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
		Mail_tracking_value domain  = mail_tracking_valueMapping.toDomain(mail_tracking_valuedto);
        domain .setId(mail_tracking_value_id);
		mail_tracking_valueService.update(domain );
		Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueService.getMailTrackingValueByEntities(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedtos)),'iBizBusinessCentral-Mail_tracking_value-Update')")
    @ApiOperation(value = "批量更新邮件跟踪值", tags = {"邮件跟踪值" },  notes = "批量更新邮件跟踪值")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_tracking_values/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        mail_tracking_valueService.updateBatch(mail_tracking_valueMapping.toDomain(mail_tracking_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueService.get(#mail_tracking_value_id),'iBizBusinessCentral-Mail_tracking_value-Remove')")
    @ApiOperation(value = "删除邮件跟踪值", tags = {"邮件跟踪值" },  notes = "删除邮件跟踪值")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_tracking_values/{mail_tracking_value_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_tracking_value_id") Long mail_tracking_value_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueService.remove(mail_tracking_value_id));
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueService.getMailTrackingValueByIds(#ids),'iBizBusinessCentral-Mail_tracking_value-Remove')")
    @ApiOperation(value = "批量删除邮件跟踪值", tags = {"邮件跟踪值" },  notes = "批量删除邮件跟踪值")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_tracking_values/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_tracking_valueService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_tracking_value-Get')")
    @ApiOperation(value = "获取邮件跟踪值", tags = {"邮件跟踪值" },  notes = "获取邮件跟踪值")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_tracking_values/{mail_tracking_value_id}")
    public ResponseEntity<Mail_tracking_valueDTO> get(@PathVariable("mail_tracking_value_id") Long mail_tracking_value_id) {
        Mail_tracking_value domain = mail_tracking_valueService.get(mail_tracking_value_id);
        Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取邮件跟踪值草稿", tags = {"邮件跟踪值" },  notes = "获取邮件跟踪值草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_tracking_values/getdraft")
    public ResponseEntity<Mail_tracking_valueDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueMapping.toDto(mail_tracking_valueService.getDraft(new Mail_tracking_value())));
    }

    @ApiOperation(value = "检查邮件跟踪值", tags = {"邮件跟踪值" },  notes = "检查邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueService.checkKey(mail_tracking_valueMapping.toDomain(mail_tracking_valuedto)));
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedto),'iBizBusinessCentral-Mail_tracking_value-Save')")
    @ApiOperation(value = "保存邮件跟踪值", tags = {"邮件跟踪值" },  notes = "保存邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueService.save(mail_tracking_valueMapping.toDomain(mail_tracking_valuedto)));
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedtos),'iBizBusinessCentral-Mail_tracking_value-Save')")
    @ApiOperation(value = "批量保存邮件跟踪值", tags = {"邮件跟踪值" },  notes = "批量保存邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        mail_tracking_valueService.saveBatch(mail_tracking_valueMapping.toDomain(mail_tracking_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_tracking_value-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_tracking_value-Get')")
	@ApiOperation(value = "获取数据集", tags = {"邮件跟踪值" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_tracking_values/fetchdefault")
	public ResponseEntity<List<Mail_tracking_valueDTO>> fetchDefault(Mail_tracking_valueSearchContext context) {
        Page<Mail_tracking_value> domains = mail_tracking_valueService.searchDefault(context) ;
        List<Mail_tracking_valueDTO> list = mail_tracking_valueMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_tracking_value-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_tracking_value-Get')")
	@ApiOperation(value = "查询数据集", tags = {"邮件跟踪值" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_tracking_values/searchdefault")
	public ResponseEntity<Page<Mail_tracking_valueDTO>> searchDefault(@RequestBody Mail_tracking_valueSearchContext context) {
        Page<Mail_tracking_value> domains = mail_tracking_valueService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_tracking_valueMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedto),'iBizBusinessCentral-Mail_tracking_value-Create')")
    @ApiOperation(value = "根据消息建立邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息建立邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/mail_tracking_values")
    public ResponseEntity<Mail_tracking_valueDTO> createByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        Mail_tracking_value domain = mail_tracking_valueMapping.toDomain(mail_tracking_valuedto);
        domain.setMailMessageId(mail_message_id);
		mail_tracking_valueService.create(domain);
        Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedtos),'iBizBusinessCentral-Mail_tracking_value-Create')")
    @ApiOperation(value = "根据消息批量建立邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息批量建立邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/mail_tracking_values/batch")
    public ResponseEntity<Boolean> createBatchByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        List<Mail_tracking_value> domainlist=mail_tracking_valueMapping.toDomain(mail_tracking_valuedtos);
        for(Mail_tracking_value domain:domainlist){
            domain.setMailMessageId(mail_message_id);
        }
        mail_tracking_valueService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_tracking_value" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_tracking_valueService.get(#mail_tracking_value_id),'iBizBusinessCentral-Mail_tracking_value-Update')")
    @ApiOperation(value = "根据消息更新邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息更新邮件跟踪值")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/{mail_message_id}/mail_tracking_values/{mail_tracking_value_id}")
    public ResponseEntity<Mail_tracking_valueDTO> updateByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @PathVariable("mail_tracking_value_id") Long mail_tracking_value_id, @RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        Mail_tracking_value domain = mail_tracking_valueMapping.toDomain(mail_tracking_valuedto);
        domain.setMailMessageId(mail_message_id);
        domain.setId(mail_tracking_value_id);
		mail_tracking_valueService.update(domain);
        Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueService.getMailTrackingValueByEntities(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedtos)),'iBizBusinessCentral-Mail_tracking_value-Update')")
    @ApiOperation(value = "根据消息批量更新邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息批量更新邮件跟踪值")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/{mail_message_id}/mail_tracking_values/batch")
    public ResponseEntity<Boolean> updateBatchByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        List<Mail_tracking_value> domainlist=mail_tracking_valueMapping.toDomain(mail_tracking_valuedtos);
        for(Mail_tracking_value domain:domainlist){
            domain.setMailMessageId(mail_message_id);
        }
        mail_tracking_valueService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueService.get(#mail_tracking_value_id),'iBizBusinessCentral-Mail_tracking_value-Remove')")
    @ApiOperation(value = "根据消息删除邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息删除邮件跟踪值")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/{mail_message_id}/mail_tracking_values/{mail_tracking_value_id}")
    public ResponseEntity<Boolean> removeByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @PathVariable("mail_tracking_value_id") Long mail_tracking_value_id) {
		return ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueService.remove(mail_tracking_value_id));
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueService.getMailTrackingValueByIds(#ids),'iBizBusinessCentral-Mail_tracking_value-Remove')")
    @ApiOperation(value = "根据消息批量删除邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息批量删除邮件跟踪值")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/{mail_message_id}/mail_tracking_values/batch")
    public ResponseEntity<Boolean> removeBatchByMail_message(@RequestBody List<Long> ids) {
        mail_tracking_valueService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_tracking_value-Get')")
    @ApiOperation(value = "根据消息获取邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息获取邮件跟踪值")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_messages/{mail_message_id}/mail_tracking_values/{mail_tracking_value_id}")
    public ResponseEntity<Mail_tracking_valueDTO> getByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @PathVariable("mail_tracking_value_id") Long mail_tracking_value_id) {
        Mail_tracking_value domain = mail_tracking_valueService.get(mail_tracking_value_id);
        Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据消息获取邮件跟踪值草稿", tags = {"邮件跟踪值" },  notes = "根据消息获取邮件跟踪值草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/mail_messages/{mail_message_id}/mail_tracking_values/getdraft")
    public ResponseEntity<Mail_tracking_valueDTO> getDraftByMail_message(@PathVariable("mail_message_id") Long mail_message_id) {
        Mail_tracking_value domain = new Mail_tracking_value();
        domain.setMailMessageId(mail_message_id);
        return ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueMapping.toDto(mail_tracking_valueService.getDraft(domain)));
    }

    @ApiOperation(value = "根据消息检查邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息检查邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/mail_tracking_values/checkkey")
    public ResponseEntity<Boolean> checkKeyByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueService.checkKey(mail_tracking_valueMapping.toDomain(mail_tracking_valuedto)));
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedto),'iBizBusinessCentral-Mail_tracking_value-Save')")
    @ApiOperation(value = "根据消息保存邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息保存邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/mail_tracking_values/save")
    public ResponseEntity<Boolean> saveByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        Mail_tracking_value domain = mail_tracking_valueMapping.toDomain(mail_tracking_valuedto);
        domain.setMailMessageId(mail_message_id);
        return ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueService.save(domain));
    }

    @PreAuthorize("hasPermission(this.mail_tracking_valueMapping.toDomain(#mail_tracking_valuedtos),'iBizBusinessCentral-Mail_tracking_value-Save')")
    @ApiOperation(value = "根据消息批量保存邮件跟踪值", tags = {"邮件跟踪值" },  notes = "根据消息批量保存邮件跟踪值")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/mail_tracking_values/savebatch")
    public ResponseEntity<Boolean> saveBatchByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        List<Mail_tracking_value> domainlist=mail_tracking_valueMapping.toDomain(mail_tracking_valuedtos);
        for(Mail_tracking_value domain:domainlist){
             domain.setMailMessageId(mail_message_id);
        }
        mail_tracking_valueService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_tracking_value-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_tracking_value-Get')")
	@ApiOperation(value = "根据消息获取数据集", tags = {"邮件跟踪值" } ,notes = "根据消息获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_messages/{mail_message_id}/mail_tracking_values/fetchdefault")
	public ResponseEntity<List<Mail_tracking_valueDTO>> fetchMail_tracking_valueDefaultByMail_message(@PathVariable("mail_message_id") Long mail_message_id,Mail_tracking_valueSearchContext context) {
        context.setN_mail_message_id_eq(mail_message_id);
        Page<Mail_tracking_value> domains = mail_tracking_valueService.searchDefault(context) ;
        List<Mail_tracking_valueDTO> list = mail_tracking_valueMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_tracking_value-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_tracking_value-Get')")
	@ApiOperation(value = "根据消息查询数据集", tags = {"邮件跟踪值" } ,notes = "根据消息查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_messages/{mail_message_id}/mail_tracking_values/searchdefault")
	public ResponseEntity<Page<Mail_tracking_valueDTO>> searchMail_tracking_valueDefaultByMail_message(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody Mail_tracking_valueSearchContext context) {
        context.setN_mail_message_id_eq(mail_message_id);
        Page<Mail_tracking_value> domains = mail_tracking_valueService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_tracking_valueMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

