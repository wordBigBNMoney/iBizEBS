package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contract_typeService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_contract_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"合同类型" })
@RestController("Core-hr_contract_type")
@RequestMapping("")
public class Hr_contract_typeResource {

    @Autowired
    public IHr_contract_typeService hr_contract_typeService;

    @Autowired
    @Lazy
    public Hr_contract_typeMapping hr_contract_typeMapping;

    @PreAuthorize("hasPermission(this.hr_contract_typeMapping.toDomain(#hr_contract_typedto),'iBizBusinessCentral-Hr_contract_type-Create')")
    @ApiOperation(value = "新建合同类型", tags = {"合同类型" },  notes = "新建合同类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types")
    public ResponseEntity<Hr_contract_typeDTO> create(@Validated @RequestBody Hr_contract_typeDTO hr_contract_typedto) {
        Hr_contract_type domain = hr_contract_typeMapping.toDomain(hr_contract_typedto);
		hr_contract_typeService.create(domain);
        Hr_contract_typeDTO dto = hr_contract_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_contract_typeMapping.toDomain(#hr_contract_typedtos),'iBizBusinessCentral-Hr_contract_type-Create')")
    @ApiOperation(value = "批量新建合同类型", tags = {"合同类型" },  notes = "批量新建合同类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_contract_typeDTO> hr_contract_typedtos) {
        hr_contract_typeService.createBatch(hr_contract_typeMapping.toDomain(hr_contract_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_contract_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_contract_typeService.get(#hr_contract_type_id),'iBizBusinessCentral-Hr_contract_type-Update')")
    @ApiOperation(value = "更新合同类型", tags = {"合同类型" },  notes = "更新合同类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_contract_types/{hr_contract_type_id}")
    public ResponseEntity<Hr_contract_typeDTO> update(@PathVariable("hr_contract_type_id") Long hr_contract_type_id, @RequestBody Hr_contract_typeDTO hr_contract_typedto) {
		Hr_contract_type domain  = hr_contract_typeMapping.toDomain(hr_contract_typedto);
        domain .setId(hr_contract_type_id);
		hr_contract_typeService.update(domain );
		Hr_contract_typeDTO dto = hr_contract_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_contract_typeService.getHrContractTypeByEntities(this.hr_contract_typeMapping.toDomain(#hr_contract_typedtos)),'iBizBusinessCentral-Hr_contract_type-Update')")
    @ApiOperation(value = "批量更新合同类型", tags = {"合同类型" },  notes = "批量更新合同类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_contract_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_contract_typeDTO> hr_contract_typedtos) {
        hr_contract_typeService.updateBatch(hr_contract_typeMapping.toDomain(hr_contract_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_contract_typeService.get(#hr_contract_type_id),'iBizBusinessCentral-Hr_contract_type-Remove')")
    @ApiOperation(value = "删除合同类型", tags = {"合同类型" },  notes = "删除合同类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_contract_types/{hr_contract_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_contract_type_id") Long hr_contract_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_contract_typeService.remove(hr_contract_type_id));
    }

    @PreAuthorize("hasPermission(this.hr_contract_typeService.getHrContractTypeByIds(#ids),'iBizBusinessCentral-Hr_contract_type-Remove')")
    @ApiOperation(value = "批量删除合同类型", tags = {"合同类型" },  notes = "批量删除合同类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_contract_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_contract_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_contract_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_contract_type-Get')")
    @ApiOperation(value = "获取合同类型", tags = {"合同类型" },  notes = "获取合同类型")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_contract_types/{hr_contract_type_id}")
    public ResponseEntity<Hr_contract_typeDTO> get(@PathVariable("hr_contract_type_id") Long hr_contract_type_id) {
        Hr_contract_type domain = hr_contract_typeService.get(hr_contract_type_id);
        Hr_contract_typeDTO dto = hr_contract_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取合同类型草稿", tags = {"合同类型" },  notes = "获取合同类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_contract_types/getdraft")
    public ResponseEntity<Hr_contract_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_contract_typeMapping.toDto(hr_contract_typeService.getDraft(new Hr_contract_type())));
    }

    @ApiOperation(value = "检查合同类型", tags = {"合同类型" },  notes = "检查合同类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_contract_typeDTO hr_contract_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_contract_typeService.checkKey(hr_contract_typeMapping.toDomain(hr_contract_typedto)));
    }

    @PreAuthorize("hasPermission(this.hr_contract_typeMapping.toDomain(#hr_contract_typedto),'iBizBusinessCentral-Hr_contract_type-Save')")
    @ApiOperation(value = "保存合同类型", tags = {"合同类型" },  notes = "保存合同类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_contract_typeDTO hr_contract_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_contract_typeService.save(hr_contract_typeMapping.toDomain(hr_contract_typedto)));
    }

    @PreAuthorize("hasPermission(this.hr_contract_typeMapping.toDomain(#hr_contract_typedtos),'iBizBusinessCentral-Hr_contract_type-Save')")
    @ApiOperation(value = "批量保存合同类型", tags = {"合同类型" },  notes = "批量保存合同类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_contract_typeDTO> hr_contract_typedtos) {
        hr_contract_typeService.saveBatch(hr_contract_typeMapping.toDomain(hr_contract_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_contract_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_contract_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"合同类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_contract_types/fetchdefault")
	public ResponseEntity<List<Hr_contract_typeDTO>> fetchDefault(Hr_contract_typeSearchContext context) {
        Page<Hr_contract_type> domains = hr_contract_typeService.searchDefault(context) ;
        List<Hr_contract_typeDTO> list = hr_contract_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_contract_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_contract_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"合同类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_contract_types/searchdefault")
	public ResponseEntity<Page<Hr_contract_typeDTO>> searchDefault(@RequestBody Hr_contract_typeSearchContext context) {
        Page<Hr_contract_type> domains = hr_contract_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_contract_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

