package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_move_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品移动(移库明细)" })
@RestController("Core-stock_move_line")
@RequestMapping("")
public class Stock_move_lineResource {

    @Autowired
    public IStock_move_lineService stock_move_lineService;

    @Autowired
    @Lazy
    public Stock_move_lineMapping stock_move_lineMapping;

    @PreAuthorize("hasPermission(this.stock_move_lineMapping.toDomain(#stock_move_linedto),'iBizBusinessCentral-Stock_move_line-Create')")
    @ApiOperation(value = "新建产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "新建产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_move_lines")
    public ResponseEntity<Stock_move_lineDTO> create(@Validated @RequestBody Stock_move_lineDTO stock_move_linedto) {
        Stock_move_line domain = stock_move_lineMapping.toDomain(stock_move_linedto);
		stock_move_lineService.create(domain);
        Stock_move_lineDTO dto = stock_move_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_move_lineMapping.toDomain(#stock_move_linedtos),'iBizBusinessCentral-Stock_move_line-Create')")
    @ApiOperation(value = "批量新建产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "批量新建产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_move_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_move_lineDTO> stock_move_linedtos) {
        stock_move_lineService.createBatch(stock_move_lineMapping.toDomain(stock_move_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_move_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_move_lineService.get(#stock_move_line_id),'iBizBusinessCentral-Stock_move_line-Update')")
    @ApiOperation(value = "更新产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "更新产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_move_lines/{stock_move_line_id}")
    public ResponseEntity<Stock_move_lineDTO> update(@PathVariable("stock_move_line_id") Long stock_move_line_id, @RequestBody Stock_move_lineDTO stock_move_linedto) {
		Stock_move_line domain  = stock_move_lineMapping.toDomain(stock_move_linedto);
        domain .setId(stock_move_line_id);
		stock_move_lineService.update(domain );
		Stock_move_lineDTO dto = stock_move_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_move_lineService.getStockMoveLineByEntities(this.stock_move_lineMapping.toDomain(#stock_move_linedtos)),'iBizBusinessCentral-Stock_move_line-Update')")
    @ApiOperation(value = "批量更新产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "批量更新产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_move_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_move_lineDTO> stock_move_linedtos) {
        stock_move_lineService.updateBatch(stock_move_lineMapping.toDomain(stock_move_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_move_lineService.get(#stock_move_line_id),'iBizBusinessCentral-Stock_move_line-Remove')")
    @ApiOperation(value = "删除产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "删除产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_move_lines/{stock_move_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_move_line_id") Long stock_move_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_move_lineService.remove(stock_move_line_id));
    }

    @PreAuthorize("hasPermission(this.stock_move_lineService.getStockMoveLineByIds(#ids),'iBizBusinessCentral-Stock_move_line-Remove')")
    @ApiOperation(value = "批量删除产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "批量删除产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_move_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_move_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_move_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_move_line-Get')")
    @ApiOperation(value = "获取产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "获取产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_move_lines/{stock_move_line_id}")
    public ResponseEntity<Stock_move_lineDTO> get(@PathVariable("stock_move_line_id") Long stock_move_line_id) {
        Stock_move_line domain = stock_move_lineService.get(stock_move_line_id);
        Stock_move_lineDTO dto = stock_move_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品移动(移库明细)草稿", tags = {"产品移动(移库明细)" },  notes = "获取产品移动(移库明细)草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_move_lines/getdraft")
    public ResponseEntity<Stock_move_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_move_lineMapping.toDto(stock_move_lineService.getDraft(new Stock_move_line())));
    }

    @ApiOperation(value = "检查产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "检查产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_move_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_move_lineDTO stock_move_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_move_lineService.checkKey(stock_move_lineMapping.toDomain(stock_move_linedto)));
    }

    @PreAuthorize("hasPermission(this.stock_move_lineMapping.toDomain(#stock_move_linedto),'iBizBusinessCentral-Stock_move_line-Save')")
    @ApiOperation(value = "保存产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "保存产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_move_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_move_lineDTO stock_move_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_move_lineService.save(stock_move_lineMapping.toDomain(stock_move_linedto)));
    }

    @PreAuthorize("hasPermission(this.stock_move_lineMapping.toDomain(#stock_move_linedtos),'iBizBusinessCentral-Stock_move_line-Save')")
    @ApiOperation(value = "批量保存产品移动(移库明细)", tags = {"产品移动(移库明细)" },  notes = "批量保存产品移动(移库明细)")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_move_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_move_lineDTO> stock_move_linedtos) {
        stock_move_lineService.saveBatch(stock_move_lineMapping.toDomain(stock_move_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_move_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_move_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品移动(移库明细)" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_move_lines/fetchdefault")
	public ResponseEntity<List<Stock_move_lineDTO>> fetchDefault(Stock_move_lineSearchContext context) {
        Page<Stock_move_line> domains = stock_move_lineService.searchDefault(context) ;
        List<Stock_move_lineDTO> list = stock_move_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_move_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_move_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品移动(移库明细)" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_move_lines/searchdefault")
	public ResponseEntity<Page<Stock_move_lineDTO>> searchDefault(@RequestBody Stock_move_lineSearchContext context) {
        Page<Stock_move_line> domains = stock_move_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_move_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

