package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_listService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"邮件列表" })
@RestController("Core-mail_mass_mailing_list")
@RequestMapping("")
public class Mail_mass_mailing_listResource {

    @Autowired
    public IMail_mass_mailing_listService mail_mass_mailing_listService;

    @Autowired
    @Lazy
    public Mail_mass_mailing_listMapping mail_mass_mailing_listMapping;

    @PreAuthorize("hasPermission(this.mail_mass_mailing_listMapping.toDomain(#mail_mass_mailing_listdto),'iBizBusinessCentral-Mail_mass_mailing_list-Create')")
    @ApiOperation(value = "新建邮件列表", tags = {"邮件列表" },  notes = "新建邮件列表")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists")
    public ResponseEntity<Mail_mass_mailing_listDTO> create(@Validated @RequestBody Mail_mass_mailing_listDTO mail_mass_mailing_listdto) {
        Mail_mass_mailing_list domain = mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdto);
		mail_mass_mailing_listService.create(domain);
        Mail_mass_mailing_listDTO dto = mail_mass_mailing_listMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_listMapping.toDomain(#mail_mass_mailing_listdtos),'iBizBusinessCentral-Mail_mass_mailing_list-Create')")
    @ApiOperation(value = "批量新建邮件列表", tags = {"邮件列表" },  notes = "批量新建邮件列表")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_listDTO> mail_mass_mailing_listdtos) {
        mail_mass_mailing_listService.createBatch(mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mass_mailing_list" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mass_mailing_listService.get(#mail_mass_mailing_list_id),'iBizBusinessCentral-Mail_mass_mailing_list-Update')")
    @ApiOperation(value = "更新邮件列表", tags = {"邮件列表" },  notes = "更新邮件列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")
    public ResponseEntity<Mail_mass_mailing_listDTO> update(@PathVariable("mail_mass_mailing_list_id") Long mail_mass_mailing_list_id, @RequestBody Mail_mass_mailing_listDTO mail_mass_mailing_listdto) {
		Mail_mass_mailing_list domain  = mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdto);
        domain .setId(mail_mass_mailing_list_id);
		mail_mass_mailing_listService.update(domain );
		Mail_mass_mailing_listDTO dto = mail_mass_mailing_listMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_listService.getMailMassMailingListByEntities(this.mail_mass_mailing_listMapping.toDomain(#mail_mass_mailing_listdtos)),'iBizBusinessCentral-Mail_mass_mailing_list-Update')")
    @ApiOperation(value = "批量更新邮件列表", tags = {"邮件列表" },  notes = "批量更新邮件列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_lists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_listDTO> mail_mass_mailing_listdtos) {
        mail_mass_mailing_listService.updateBatch(mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_listService.get(#mail_mass_mailing_list_id),'iBizBusinessCentral-Mail_mass_mailing_list-Remove')")
    @ApiOperation(value = "删除邮件列表", tags = {"邮件列表" },  notes = "删除邮件列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_list_id") Long mail_mass_mailing_list_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_listService.remove(mail_mass_mailing_list_id));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_listService.getMailMassMailingListByIds(#ids),'iBizBusinessCentral-Mail_mass_mailing_list-Remove')")
    @ApiOperation(value = "批量删除邮件列表", tags = {"邮件列表" },  notes = "批量删除邮件列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_lists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mass_mailing_listService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mass_mailing_listMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mass_mailing_list-Get')")
    @ApiOperation(value = "获取邮件列表", tags = {"邮件列表" },  notes = "获取邮件列表")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")
    public ResponseEntity<Mail_mass_mailing_listDTO> get(@PathVariable("mail_mass_mailing_list_id") Long mail_mass_mailing_list_id) {
        Mail_mass_mailing_list domain = mail_mass_mailing_listService.get(mail_mass_mailing_list_id);
        Mail_mass_mailing_listDTO dto = mail_mass_mailing_listMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取邮件列表草稿", tags = {"邮件列表" },  notes = "获取邮件列表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_lists/getdraft")
    public ResponseEntity<Mail_mass_mailing_listDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_listMapping.toDto(mail_mass_mailing_listService.getDraft(new Mail_mass_mailing_list())));
    }

    @ApiOperation(value = "检查邮件列表", tags = {"邮件列表" },  notes = "检查邮件列表")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mass_mailing_listDTO mail_mass_mailing_listdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_listService.checkKey(mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_listMapping.toDomain(#mail_mass_mailing_listdto),'iBizBusinessCentral-Mail_mass_mailing_list-Save')")
    @ApiOperation(value = "保存邮件列表", tags = {"邮件列表" },  notes = "保存邮件列表")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mass_mailing_listDTO mail_mass_mailing_listdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_listService.save(mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_listMapping.toDomain(#mail_mass_mailing_listdtos),'iBizBusinessCentral-Mail_mass_mailing_list-Save')")
    @ApiOperation(value = "批量保存邮件列表", tags = {"邮件列表" },  notes = "批量保存邮件列表")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mass_mailing_listDTO> mail_mass_mailing_listdtos) {
        mail_mass_mailing_listService.saveBatch(mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_list-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_list-Get')")
	@ApiOperation(value = "获取数据集", tags = {"邮件列表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_lists/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_listDTO>> fetchDefault(Mail_mass_mailing_listSearchContext context) {
        Page<Mail_mass_mailing_list> domains = mail_mass_mailing_listService.searchDefault(context) ;
        List<Mail_mass_mailing_listDTO> list = mail_mass_mailing_listMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_list-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_list-Get')")
	@ApiOperation(value = "查询数据集", tags = {"邮件列表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mass_mailing_lists/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_listDTO>> searchDefault(@RequestBody Mail_mass_mailing_listSearchContext context) {
        Page<Mail_mass_mailing_list> domains = mail_mass_mailing_listService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_listMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

