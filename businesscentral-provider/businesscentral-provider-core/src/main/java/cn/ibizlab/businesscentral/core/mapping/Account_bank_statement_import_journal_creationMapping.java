package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.businesscentral.core.dto.Account_bank_statement_import_journal_creationDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_bank_statement_import_journal_creationMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_bank_statement_import_journal_creationMapping extends MappingBase<Account_bank_statement_import_journal_creationDTO, Account_bank_statement_import_journal_creation> {


}

