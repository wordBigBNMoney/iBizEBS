package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_categoryService;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品计量单位 类别" })
@RestController("Core-uom_category")
@RequestMapping("")
public class Uom_categoryResource {

    @Autowired
    public IUom_categoryService uom_categoryService;

    @Autowired
    @Lazy
    public Uom_categoryMapping uom_categoryMapping;

    @PreAuthorize("hasPermission(this.uom_categoryMapping.toDomain(#uom_categorydto),'iBizBusinessCentral-Uom_category-Create')")
    @ApiOperation(value = "新建产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "新建产品计量单位 类别")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_categories")
    public ResponseEntity<Uom_categoryDTO> create(@Validated @RequestBody Uom_categoryDTO uom_categorydto) {
        Uom_category domain = uom_categoryMapping.toDomain(uom_categorydto);
		uom_categoryService.create(domain);
        Uom_categoryDTO dto = uom_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.uom_categoryMapping.toDomain(#uom_categorydtos),'iBizBusinessCentral-Uom_category-Create')")
    @ApiOperation(value = "批量新建产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "批量新建产品计量单位 类别")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Uom_categoryDTO> uom_categorydtos) {
        uom_categoryService.createBatch(uom_categoryMapping.toDomain(uom_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "uom_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.uom_categoryService.get(#uom_category_id),'iBizBusinessCentral-Uom_category-Update')")
    @ApiOperation(value = "更新产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "更新产品计量单位 类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_categories/{uom_category_id}")
    public ResponseEntity<Uom_categoryDTO> update(@PathVariable("uom_category_id") Long uom_category_id, @RequestBody Uom_categoryDTO uom_categorydto) {
		Uom_category domain  = uom_categoryMapping.toDomain(uom_categorydto);
        domain .setId(uom_category_id);
		uom_categoryService.update(domain );
		Uom_categoryDTO dto = uom_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.uom_categoryService.getUomCategoryByEntities(this.uom_categoryMapping.toDomain(#uom_categorydtos)),'iBizBusinessCentral-Uom_category-Update')")
    @ApiOperation(value = "批量更新产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "批量更新产品计量单位 类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Uom_categoryDTO> uom_categorydtos) {
        uom_categoryService.updateBatch(uom_categoryMapping.toDomain(uom_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.uom_categoryService.get(#uom_category_id),'iBizBusinessCentral-Uom_category-Remove')")
    @ApiOperation(value = "删除产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "删除产品计量单位 类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_categories/{uom_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("uom_category_id") Long uom_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(uom_categoryService.remove(uom_category_id));
    }

    @PreAuthorize("hasPermission(this.uom_categoryService.getUomCategoryByIds(#ids),'iBizBusinessCentral-Uom_category-Remove')")
    @ApiOperation(value = "批量删除产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "批量删除产品计量单位 类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        uom_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.uom_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Uom_category-Get')")
    @ApiOperation(value = "获取产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "获取产品计量单位 类别")
	@RequestMapping(method = RequestMethod.GET, value = "/uom_categories/{uom_category_id}")
    public ResponseEntity<Uom_categoryDTO> get(@PathVariable("uom_category_id") Long uom_category_id) {
        Uom_category domain = uom_categoryService.get(uom_category_id);
        Uom_categoryDTO dto = uom_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品计量单位 类别草稿", tags = {"产品计量单位 类别" },  notes = "获取产品计量单位 类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/uom_categories/getdraft")
    public ResponseEntity<Uom_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(uom_categoryMapping.toDto(uom_categoryService.getDraft(new Uom_category())));
    }

    @ApiOperation(value = "检查产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "检查产品计量单位 类别")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Uom_categoryDTO uom_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(uom_categoryService.checkKey(uom_categoryMapping.toDomain(uom_categorydto)));
    }

    @PreAuthorize("hasPermission(this.uom_categoryMapping.toDomain(#uom_categorydto),'iBizBusinessCentral-Uom_category-Save')")
    @ApiOperation(value = "保存产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "保存产品计量单位 类别")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Uom_categoryDTO uom_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(uom_categoryService.save(uom_categoryMapping.toDomain(uom_categorydto)));
    }

    @PreAuthorize("hasPermission(this.uom_categoryMapping.toDomain(#uom_categorydtos),'iBizBusinessCentral-Uom_category-Save')")
    @ApiOperation(value = "批量保存产品计量单位 类别", tags = {"产品计量单位 类别" },  notes = "批量保存产品计量单位 类别")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Uom_categoryDTO> uom_categorydtos) {
        uom_categoryService.saveBatch(uom_categoryMapping.toDomain(uom_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Uom_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Uom_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品计量单位 类别" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/uom_categories/fetchdefault")
	public ResponseEntity<List<Uom_categoryDTO>> fetchDefault(Uom_categorySearchContext context) {
        Page<Uom_category> domains = uom_categoryService.searchDefault(context) ;
        List<Uom_categoryDTO> list = uom_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Uom_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Uom_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品计量单位 类别" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/uom_categories/searchdefault")
	public ResponseEntity<Page<Uom_categoryDTO>> searchDefault(@RequestBody Uom_categorySearchContext context) {
        Page<Uom_category> domains = uom_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(uom_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

