package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_tag;
import cn.ibizlab.businesscentral.core.dto.Mail_mass_mailing_tagDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMail_mass_mailing_tagMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_mass_mailing_tagMapping extends MappingBase<Mail_mass_mailing_tagDTO, Mail_mass_mailing_tag> {


}

