package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.businesscentral.core.odoo_bus.service.IBus_presenceService;
import cn.ibizlab.businesscentral.core.odoo_bus.filter.Bus_presenceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"用户上线" })
@RestController("Core-bus_presence")
@RequestMapping("")
public class Bus_presenceResource {

    @Autowired
    public IBus_presenceService bus_presenceService;

    @Autowired
    @Lazy
    public Bus_presenceMapping bus_presenceMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Create-all')")
    @ApiOperation(value = "新建用户上线", tags = {"用户上线" },  notes = "新建用户上线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_presences")
    public ResponseEntity<Bus_presenceDTO> create(@Validated @RequestBody Bus_presenceDTO bus_presencedto) {
        Bus_presence domain = bus_presenceMapping.toDomain(bus_presencedto);
		bus_presenceService.create(domain);
        Bus_presenceDTO dto = bus_presenceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Create-all')")
    @ApiOperation(value = "批量新建用户上线", tags = {"用户上线" },  notes = "批量新建用户上线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_presences/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Bus_presenceDTO> bus_presencedtos) {
        bus_presenceService.createBatch(bus_presenceMapping.toDomain(bus_presencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Update-all')")
    @ApiOperation(value = "更新用户上线", tags = {"用户上线" },  notes = "更新用户上线")
	@RequestMapping(method = RequestMethod.PUT, value = "/bus_presences/{bus_presence_id}")
    public ResponseEntity<Bus_presenceDTO> update(@PathVariable("bus_presence_id") Long bus_presence_id, @RequestBody Bus_presenceDTO bus_presencedto) {
		Bus_presence domain  = bus_presenceMapping.toDomain(bus_presencedto);
        domain .setId(bus_presence_id);
		bus_presenceService.update(domain );
		Bus_presenceDTO dto = bus_presenceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Update-all')")
    @ApiOperation(value = "批量更新用户上线", tags = {"用户上线" },  notes = "批量更新用户上线")
	@RequestMapping(method = RequestMethod.PUT, value = "/bus_presences/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Bus_presenceDTO> bus_presencedtos) {
        bus_presenceService.updateBatch(bus_presenceMapping.toDomain(bus_presencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Remove-all')")
    @ApiOperation(value = "删除用户上线", tags = {"用户上线" },  notes = "删除用户上线")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bus_presences/{bus_presence_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("bus_presence_id") Long bus_presence_id) {
         return ResponseEntity.status(HttpStatus.OK).body(bus_presenceService.remove(bus_presence_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Remove-all')")
    @ApiOperation(value = "批量删除用户上线", tags = {"用户上线" },  notes = "批量删除用户上线")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bus_presences/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        bus_presenceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Get-all')")
    @ApiOperation(value = "获取用户上线", tags = {"用户上线" },  notes = "获取用户上线")
	@RequestMapping(method = RequestMethod.GET, value = "/bus_presences/{bus_presence_id}")
    public ResponseEntity<Bus_presenceDTO> get(@PathVariable("bus_presence_id") Long bus_presence_id) {
        Bus_presence domain = bus_presenceService.get(bus_presence_id);
        Bus_presenceDTO dto = bus_presenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取用户上线草稿", tags = {"用户上线" },  notes = "获取用户上线草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/bus_presences/getdraft")
    public ResponseEntity<Bus_presenceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(bus_presenceMapping.toDto(bus_presenceService.getDraft(new Bus_presence())));
    }

    @ApiOperation(value = "检查用户上线", tags = {"用户上线" },  notes = "检查用户上线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_presences/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Bus_presenceDTO bus_presencedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(bus_presenceService.checkKey(bus_presenceMapping.toDomain(bus_presencedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Save-all')")
    @ApiOperation(value = "保存用户上线", tags = {"用户上线" },  notes = "保存用户上线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_presences/save")
    public ResponseEntity<Boolean> save(@RequestBody Bus_presenceDTO bus_presencedto) {
        return ResponseEntity.status(HttpStatus.OK).body(bus_presenceService.save(bus_presenceMapping.toDomain(bus_presencedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-Save-all')")
    @ApiOperation(value = "批量保存用户上线", tags = {"用户上线" },  notes = "批量保存用户上线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_presences/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Bus_presenceDTO> bus_presencedtos) {
        bus_presenceService.saveBatch(bus_presenceMapping.toDomain(bus_presencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"用户上线" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/bus_presences/fetchdefault")
	public ResponseEntity<List<Bus_presenceDTO>> fetchDefault(Bus_presenceSearchContext context) {
        Page<Bus_presence> domains = bus_presenceService.searchDefault(context) ;
        List<Bus_presenceDTO> list = bus_presenceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_presence-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"用户上线" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/bus_presences/searchdefault")
	public ResponseEntity<Page<Bus_presenceDTO>> searchDefault(@RequestBody Bus_presenceSearchContext context) {
        Page<Bus_presence> domains = bus_presenceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(bus_presenceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

