package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead_lostService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead_lostSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"获取丢失原因" })
@RestController("Core-crm_lead_lost")
@RequestMapping("")
public class Crm_lead_lostResource {

    @Autowired
    public ICrm_lead_lostService crm_lead_lostService;

    @Autowired
    @Lazy
    public Crm_lead_lostMapping crm_lead_lostMapping;

    @PreAuthorize("hasPermission(this.crm_lead_lostMapping.toDomain(#crm_lead_lostdto),'iBizBusinessCentral-Crm_lead_lost-Create')")
    @ApiOperation(value = "新建获取丢失原因", tags = {"获取丢失原因" },  notes = "新建获取丢失原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts")
    public ResponseEntity<Crm_lead_lostDTO> create(@Validated @RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
        Crm_lead_lost domain = crm_lead_lostMapping.toDomain(crm_lead_lostdto);
		crm_lead_lostService.create(domain);
        Crm_lead_lostDTO dto = crm_lead_lostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lead_lostMapping.toDomain(#crm_lead_lostdtos),'iBizBusinessCentral-Crm_lead_lost-Create')")
    @ApiOperation(value = "批量新建获取丢失原因", tags = {"获取丢失原因" },  notes = "批量新建获取丢失原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lead_lostDTO> crm_lead_lostdtos) {
        crm_lead_lostService.createBatch(crm_lead_lostMapping.toDomain(crm_lead_lostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_lead_lost" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_lead_lostService.get(#crm_lead_lost_id),'iBizBusinessCentral-Crm_lead_lost-Update')")
    @ApiOperation(value = "更新获取丢失原因", tags = {"获取丢失原因" },  notes = "更新获取丢失原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_losts/{crm_lead_lost_id}")
    public ResponseEntity<Crm_lead_lostDTO> update(@PathVariable("crm_lead_lost_id") Long crm_lead_lost_id, @RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
		Crm_lead_lost domain  = crm_lead_lostMapping.toDomain(crm_lead_lostdto);
        domain .setId(crm_lead_lost_id);
		crm_lead_lostService.update(domain );
		Crm_lead_lostDTO dto = crm_lead_lostMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lead_lostService.getCrmLeadLostByEntities(this.crm_lead_lostMapping.toDomain(#crm_lead_lostdtos)),'iBizBusinessCentral-Crm_lead_lost-Update')")
    @ApiOperation(value = "批量更新获取丢失原因", tags = {"获取丢失原因" },  notes = "批量更新获取丢失原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_losts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead_lostDTO> crm_lead_lostdtos) {
        crm_lead_lostService.updateBatch(crm_lead_lostMapping.toDomain(crm_lead_lostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_lead_lostService.get(#crm_lead_lost_id),'iBizBusinessCentral-Crm_lead_lost-Remove')")
    @ApiOperation(value = "删除获取丢失原因", tags = {"获取丢失原因" },  notes = "删除获取丢失原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_losts/{crm_lead_lost_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_lost_id") Long crm_lead_lost_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lostService.remove(crm_lead_lost_id));
    }

    @PreAuthorize("hasPermission(this.crm_lead_lostService.getCrmLeadLostByIds(#ids),'iBizBusinessCentral-Crm_lead_lost-Remove')")
    @ApiOperation(value = "批量删除获取丢失原因", tags = {"获取丢失原因" },  notes = "批量删除获取丢失原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_losts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_lead_lostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_lead_lostMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_lead_lost-Get')")
    @ApiOperation(value = "获取获取丢失原因", tags = {"获取丢失原因" },  notes = "获取获取丢失原因")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/{crm_lead_lost_id}")
    public ResponseEntity<Crm_lead_lostDTO> get(@PathVariable("crm_lead_lost_id") Long crm_lead_lost_id) {
        Crm_lead_lost domain = crm_lead_lostService.get(crm_lead_lost_id);
        Crm_lead_lostDTO dto = crm_lead_lostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取获取丢失原因草稿", tags = {"获取丢失原因" },  notes = "获取获取丢失原因草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/getdraft")
    public ResponseEntity<Crm_lead_lostDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lostMapping.toDto(crm_lead_lostService.getDraft(new Crm_lead_lost())));
    }

    @ApiOperation(value = "检查获取丢失原因", tags = {"获取丢失原因" },  notes = "检查获取丢失原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_lead_lostService.checkKey(crm_lead_lostMapping.toDomain(crm_lead_lostdto)));
    }

    @PreAuthorize("hasPermission(this.crm_lead_lostMapping.toDomain(#crm_lead_lostdto),'iBizBusinessCentral-Crm_lead_lost-Save')")
    @ApiOperation(value = "保存获取丢失原因", tags = {"获取丢失原因" },  notes = "保存获取丢失原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lostService.save(crm_lead_lostMapping.toDomain(crm_lead_lostdto)));
    }

    @PreAuthorize("hasPermission(this.crm_lead_lostMapping.toDomain(#crm_lead_lostdtos),'iBizBusinessCentral-Crm_lead_lost-Save')")
    @ApiOperation(value = "批量保存获取丢失原因", tags = {"获取丢失原因" },  notes = "批量保存获取丢失原因")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_lead_lostDTO> crm_lead_lostdtos) {
        crm_lead_lostService.saveBatch(crm_lead_lostMapping.toDomain(crm_lead_lostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead_lost-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead_lost-Get')")
	@ApiOperation(value = "获取数据集", tags = {"获取丢失原因" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead_losts/fetchdefault")
	public ResponseEntity<List<Crm_lead_lostDTO>> fetchDefault(Crm_lead_lostSearchContext context) {
        Page<Crm_lead_lost> domains = crm_lead_lostService.searchDefault(context) ;
        List<Crm_lead_lostDTO> list = crm_lead_lostMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead_lost-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead_lost-Get')")
	@ApiOperation(value = "查询数据集", tags = {"获取丢失原因" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lead_losts/searchdefault")
	public ResponseEntity<Page<Crm_lead_lostDTO>> searchDefault(@RequestBody Crm_lead_lostSearchContext context) {
        Page<Crm_lead_lost> domains = crm_lead_lostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lead_lostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

