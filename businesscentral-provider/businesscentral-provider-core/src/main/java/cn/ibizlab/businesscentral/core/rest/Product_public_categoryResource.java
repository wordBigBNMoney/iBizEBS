package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_public_categoryService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_public_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"网站产品目录" })
@RestController("Core-product_public_category")
@RequestMapping("")
public class Product_public_categoryResource {

    @Autowired
    public IProduct_public_categoryService product_public_categoryService;

    @Autowired
    @Lazy
    public Product_public_categoryMapping product_public_categoryMapping;

    @PreAuthorize("hasPermission(this.product_public_categoryMapping.toDomain(#product_public_categorydto),'iBizBusinessCentral-Product_public_category-Create')")
    @ApiOperation(value = "新建网站产品目录", tags = {"网站产品目录" },  notes = "新建网站产品目录")
	@RequestMapping(method = RequestMethod.POST, value = "/product_public_categories")
    public ResponseEntity<Product_public_categoryDTO> create(@Validated @RequestBody Product_public_categoryDTO product_public_categorydto) {
        Product_public_category domain = product_public_categoryMapping.toDomain(product_public_categorydto);
		product_public_categoryService.create(domain);
        Product_public_categoryDTO dto = product_public_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_public_categoryMapping.toDomain(#product_public_categorydtos),'iBizBusinessCentral-Product_public_category-Create')")
    @ApiOperation(value = "批量新建网站产品目录", tags = {"网站产品目录" },  notes = "批量新建网站产品目录")
	@RequestMapping(method = RequestMethod.POST, value = "/product_public_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_public_categoryDTO> product_public_categorydtos) {
        product_public_categoryService.createBatch(product_public_categoryMapping.toDomain(product_public_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_public_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_public_categoryService.get(#product_public_category_id),'iBizBusinessCentral-Product_public_category-Update')")
    @ApiOperation(value = "更新网站产品目录", tags = {"网站产品目录" },  notes = "更新网站产品目录")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_public_categories/{product_public_category_id}")
    public ResponseEntity<Product_public_categoryDTO> update(@PathVariable("product_public_category_id") Long product_public_category_id, @RequestBody Product_public_categoryDTO product_public_categorydto) {
		Product_public_category domain  = product_public_categoryMapping.toDomain(product_public_categorydto);
        domain .setId(product_public_category_id);
		product_public_categoryService.update(domain );
		Product_public_categoryDTO dto = product_public_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_public_categoryService.getProductPublicCategoryByEntities(this.product_public_categoryMapping.toDomain(#product_public_categorydtos)),'iBizBusinessCentral-Product_public_category-Update')")
    @ApiOperation(value = "批量更新网站产品目录", tags = {"网站产品目录" },  notes = "批量更新网站产品目录")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_public_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_public_categoryDTO> product_public_categorydtos) {
        product_public_categoryService.updateBatch(product_public_categoryMapping.toDomain(product_public_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_public_categoryService.get(#product_public_category_id),'iBizBusinessCentral-Product_public_category-Remove')")
    @ApiOperation(value = "删除网站产品目录", tags = {"网站产品目录" },  notes = "删除网站产品目录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_public_categories/{product_public_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_public_category_id") Long product_public_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_public_categoryService.remove(product_public_category_id));
    }

    @PreAuthorize("hasPermission(this.product_public_categoryService.getProductPublicCategoryByIds(#ids),'iBizBusinessCentral-Product_public_category-Remove')")
    @ApiOperation(value = "批量删除网站产品目录", tags = {"网站产品目录" },  notes = "批量删除网站产品目录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_public_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_public_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_public_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_public_category-Get')")
    @ApiOperation(value = "获取网站产品目录", tags = {"网站产品目录" },  notes = "获取网站产品目录")
	@RequestMapping(method = RequestMethod.GET, value = "/product_public_categories/{product_public_category_id}")
    public ResponseEntity<Product_public_categoryDTO> get(@PathVariable("product_public_category_id") Long product_public_category_id) {
        Product_public_category domain = product_public_categoryService.get(product_public_category_id);
        Product_public_categoryDTO dto = product_public_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取网站产品目录草稿", tags = {"网站产品目录" },  notes = "获取网站产品目录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_public_categories/getdraft")
    public ResponseEntity<Product_public_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_public_categoryMapping.toDto(product_public_categoryService.getDraft(new Product_public_category())));
    }

    @ApiOperation(value = "检查网站产品目录", tags = {"网站产品目录" },  notes = "检查网站产品目录")
	@RequestMapping(method = RequestMethod.POST, value = "/product_public_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_public_categoryDTO product_public_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_public_categoryService.checkKey(product_public_categoryMapping.toDomain(product_public_categorydto)));
    }

    @PreAuthorize("hasPermission(this.product_public_categoryMapping.toDomain(#product_public_categorydto),'iBizBusinessCentral-Product_public_category-Save')")
    @ApiOperation(value = "保存网站产品目录", tags = {"网站产品目录" },  notes = "保存网站产品目录")
	@RequestMapping(method = RequestMethod.POST, value = "/product_public_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_public_categoryDTO product_public_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_public_categoryService.save(product_public_categoryMapping.toDomain(product_public_categorydto)));
    }

    @PreAuthorize("hasPermission(this.product_public_categoryMapping.toDomain(#product_public_categorydtos),'iBizBusinessCentral-Product_public_category-Save')")
    @ApiOperation(value = "批量保存网站产品目录", tags = {"网站产品目录" },  notes = "批量保存网站产品目录")
	@RequestMapping(method = RequestMethod.POST, value = "/product_public_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_public_categoryDTO> product_public_categorydtos) {
        product_public_categoryService.saveBatch(product_public_categoryMapping.toDomain(product_public_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_public_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_public_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"网站产品目录" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_public_categories/fetchdefault")
	public ResponseEntity<List<Product_public_categoryDTO>> fetchDefault(Product_public_categorySearchContext context) {
        Page<Product_public_category> domains = product_public_categoryService.searchDefault(context) ;
        List<Product_public_categoryDTO> list = product_public_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_public_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_public_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"网站产品目录" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_public_categories/searchdefault")
	public ResponseEntity<Page<Product_public_categoryDTO>> searchDefault(@RequestBody Product_public_categorySearchContext context) {
        Page<Product_public_category> domains = product_public_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_public_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

