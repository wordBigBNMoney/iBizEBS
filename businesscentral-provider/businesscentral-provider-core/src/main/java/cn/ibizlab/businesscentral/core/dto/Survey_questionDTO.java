package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Survey_questionDTO]
 */
@Data
public class Survey_questionDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MATRIX_SUBTYPE]
     *
     */
    @JSONField(name = "matrix_subtype")
    @JsonProperty("matrix_subtype")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String matrixSubtype;

    /**
     * 属性 [VALIDATION_ERROR_MSG]
     *
     */
    @JSONField(name = "validation_error_msg")
    @JsonProperty("validation_error_msg")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String validationErrorMsg;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @NotBlank(message = "[问题类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String type;

    /**
     * 属性 [COMMENTS_ALLOWED]
     *
     */
    @JSONField(name = "comments_allowed")
    @JsonProperty("comments_allowed")
    private Boolean commentsAllowed;

    /**
     * 属性 [VALIDATION_MAX_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validation_max_date" , format="yyyy-MM-dd")
    @JsonProperty("validation_max_date")
    private Timestamp validationMaxDate;

    /**
     * 属性 [LABELS_IDS_2]
     *
     */
    @JSONField(name = "labels_ids_2")
    @JsonProperty("labels_ids_2")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String labelsIds2;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [VALIDATION_MAX_FLOAT_VALUE]
     *
     */
    @JSONField(name = "validation_max_float_value")
    @JsonProperty("validation_max_float_value")
    private Double validationMaxFloatValue;

    /**
     * 属性 [QUESTION]
     *
     */
    @JSONField(name = "question")
    @JsonProperty("question")
    @NotBlank(message = "[问题名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String question;

    /**
     * 属性 [LABELS_IDS]
     *
     */
    @JSONField(name = "labels_ids")
    @JsonProperty("labels_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String labelsIds;

    /**
     * 属性 [VALIDATION_MIN_FLOAT_VALUE]
     *
     */
    @JSONField(name = "validation_min_float_value")
    @JsonProperty("validation_min_float_value")
    private Double validationMinFloatValue;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CONSTR_MANDATORY]
     *
     */
    @JSONField(name = "constr_mandatory")
    @JsonProperty("constr_mandatory")
    private Boolean constrMandatory;

    /**
     * 属性 [USER_INPUT_LINE_IDS]
     *
     */
    @JSONField(name = "user_input_line_ids")
    @JsonProperty("user_input_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String userInputLineIds;

    /**
     * 属性 [COMMENTS_MESSAGE]
     *
     */
    @JSONField(name = "comments_message")
    @JsonProperty("comments_message")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String commentsMessage;

    /**
     * 属性 [COMMENT_COUNT_AS_ANSWER]
     *
     */
    @JSONField(name = "comment_count_as_answer")
    @JsonProperty("comment_count_as_answer")
    private Boolean commentCountAsAnswer;

    /**
     * 属性 [VALIDATION_REQUIRED]
     *
     */
    @JSONField(name = "validation_required")
    @JsonProperty("validation_required")
    private Boolean validationRequired;

    /**
     * 属性 [VALIDATION_EMAIL]
     *
     */
    @JSONField(name = "validation_email")
    @JsonProperty("validation_email")
    private Boolean validationEmail;

    /**
     * 属性 [VALIDATION_MIN_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validation_min_date" , format="yyyy-MM-dd")
    @JsonProperty("validation_min_date")
    private Timestamp validationMinDate;

    /**
     * 属性 [CONSTR_ERROR_MSG]
     *
     */
    @JSONField(name = "constr_error_msg")
    @JsonProperty("constr_error_msg")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String constrErrorMsg;

    /**
     * 属性 [VALIDATION_LENGTH_MAX]
     *
     */
    @JSONField(name = "validation_length_max")
    @JsonProperty("validation_length_max")
    private Integer validationLengthMax;

    /**
     * 属性 [COLUMN_NB]
     *
     */
    @JSONField(name = "column_nb")
    @JsonProperty("column_nb")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String columnNb;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [DISPLAY_MODE]
     *
     */
    @JSONField(name = "display_mode")
    @JsonProperty("display_mode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String displayMode;

    /**
     * 属性 [VALIDATION_LENGTH_MIN]
     *
     */
    @JSONField(name = "validation_length_min")
    @JsonProperty("validation_length_min")
    private Integer validationLengthMin;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [SURVEY_ID]
     *
     */
    @JSONField(name = "survey_id")
    @JsonProperty("survey_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long surveyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PAGE_ID]
     *
     */
    @JSONField(name = "page_id")
    @JsonProperty("page_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[调查页面]不允许为空!")
    private Long pageId;


    /**
     * 设置 [MATRIX_SUBTYPE]
     */
    public void setMatrixSubtype(String  matrixSubtype){
        this.matrixSubtype = matrixSubtype ;
        this.modify("matrix_subtype",matrixSubtype);
    }

    /**
     * 设置 [VALIDATION_ERROR_MSG]
     */
    public void setValidationErrorMsg(String  validationErrorMsg){
        this.validationErrorMsg = validationErrorMsg ;
        this.modify("validation_error_msg",validationErrorMsg);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [COMMENTS_ALLOWED]
     */
    public void setCommentsAllowed(Boolean  commentsAllowed){
        this.commentsAllowed = commentsAllowed ;
        this.modify("comments_allowed",commentsAllowed);
    }

    /**
     * 设置 [VALIDATION_MAX_DATE]
     */
    public void setValidationMaxDate(Timestamp  validationMaxDate){
        this.validationMaxDate = validationMaxDate ;
        this.modify("validation_max_date",validationMaxDate);
    }

    /**
     * 设置 [VALIDATION_MAX_FLOAT_VALUE]
     */
    public void setValidationMaxFloatValue(Double  validationMaxFloatValue){
        this.validationMaxFloatValue = validationMaxFloatValue ;
        this.modify("validation_max_float_value",validationMaxFloatValue);
    }

    /**
     * 设置 [QUESTION]
     */
    public void setQuestion(String  question){
        this.question = question ;
        this.modify("question",question);
    }

    /**
     * 设置 [VALIDATION_MIN_FLOAT_VALUE]
     */
    public void setValidationMinFloatValue(Double  validationMinFloatValue){
        this.validationMinFloatValue = validationMinFloatValue ;
        this.modify("validation_min_float_value",validationMinFloatValue);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [CONSTR_MANDATORY]
     */
    public void setConstrMandatory(Boolean  constrMandatory){
        this.constrMandatory = constrMandatory ;
        this.modify("constr_mandatory",constrMandatory);
    }

    /**
     * 设置 [COMMENTS_MESSAGE]
     */
    public void setCommentsMessage(String  commentsMessage){
        this.commentsMessage = commentsMessage ;
        this.modify("comments_message",commentsMessage);
    }

    /**
     * 设置 [COMMENT_COUNT_AS_ANSWER]
     */
    public void setCommentCountAsAnswer(Boolean  commentCountAsAnswer){
        this.commentCountAsAnswer = commentCountAsAnswer ;
        this.modify("comment_count_as_answer",commentCountAsAnswer);
    }

    /**
     * 设置 [VALIDATION_REQUIRED]
     */
    public void setValidationRequired(Boolean  validationRequired){
        this.validationRequired = validationRequired ;
        this.modify("validation_required",validationRequired);
    }

    /**
     * 设置 [VALIDATION_EMAIL]
     */
    public void setValidationEmail(Boolean  validationEmail){
        this.validationEmail = validationEmail ;
        this.modify("validation_email",validationEmail);
    }

    /**
     * 设置 [VALIDATION_MIN_DATE]
     */
    public void setValidationMinDate(Timestamp  validationMinDate){
        this.validationMinDate = validationMinDate ;
        this.modify("validation_min_date",validationMinDate);
    }

    /**
     * 设置 [CONSTR_ERROR_MSG]
     */
    public void setConstrErrorMsg(String  constrErrorMsg){
        this.constrErrorMsg = constrErrorMsg ;
        this.modify("constr_error_msg",constrErrorMsg);
    }

    /**
     * 设置 [VALIDATION_LENGTH_MAX]
     */
    public void setValidationLengthMax(Integer  validationLengthMax){
        this.validationLengthMax = validationLengthMax ;
        this.modify("validation_length_max",validationLengthMax);
    }

    /**
     * 设置 [COLUMN_NB]
     */
    public void setColumnNb(String  columnNb){
        this.columnNb = columnNb ;
        this.modify("column_nb",columnNb);
    }

    /**
     * 设置 [DISPLAY_MODE]
     */
    public void setDisplayMode(String  displayMode){
        this.displayMode = displayMode ;
        this.modify("display_mode",displayMode);
    }

    /**
     * 设置 [VALIDATION_LENGTH_MIN]
     */
    public void setValidationLengthMin(Integer  validationLengthMin){
        this.validationLengthMin = validationLengthMin ;
        this.modify("validation_length_min",validationLengthMin);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [PAGE_ID]
     */
    public void setPageId(Long  pageId){
        this.pageId = pageId ;
        this.modify("page_id",pageId);
    }


}


