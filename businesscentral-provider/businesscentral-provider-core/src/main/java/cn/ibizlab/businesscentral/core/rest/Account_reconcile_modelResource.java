package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_modelService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_modelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"请在发票和付款匹配期间创建日记账分录" })
@RestController("Core-account_reconcile_model")
@RequestMapping("")
public class Account_reconcile_modelResource {

    @Autowired
    public IAccount_reconcile_modelService account_reconcile_modelService;

    @Autowired
    @Lazy
    public Account_reconcile_modelMapping account_reconcile_modelMapping;

    @PreAuthorize("hasPermission(this.account_reconcile_modelMapping.toDomain(#account_reconcile_modeldto),'iBizBusinessCentral-Account_reconcile_model-Create')")
    @ApiOperation(value = "新建请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "新建请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models")
    public ResponseEntity<Account_reconcile_modelDTO> create(@Validated @RequestBody Account_reconcile_modelDTO account_reconcile_modeldto) {
        Account_reconcile_model domain = account_reconcile_modelMapping.toDomain(account_reconcile_modeldto);
		account_reconcile_modelService.create(domain);
        Account_reconcile_modelDTO dto = account_reconcile_modelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_reconcile_modelMapping.toDomain(#account_reconcile_modeldtos),'iBizBusinessCentral-Account_reconcile_model-Create')")
    @ApiOperation(value = "批量新建请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "批量新建请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_reconcile_modelDTO> account_reconcile_modeldtos) {
        account_reconcile_modelService.createBatch(account_reconcile_modelMapping.toDomain(account_reconcile_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_reconcile_model" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_reconcile_modelService.get(#account_reconcile_model_id),'iBizBusinessCentral-Account_reconcile_model-Update')")
    @ApiOperation(value = "更新请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "更新请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_models/{account_reconcile_model_id}")
    public ResponseEntity<Account_reconcile_modelDTO> update(@PathVariable("account_reconcile_model_id") Long account_reconcile_model_id, @RequestBody Account_reconcile_modelDTO account_reconcile_modeldto) {
		Account_reconcile_model domain  = account_reconcile_modelMapping.toDomain(account_reconcile_modeldto);
        domain .setId(account_reconcile_model_id);
		account_reconcile_modelService.update(domain );
		Account_reconcile_modelDTO dto = account_reconcile_modelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_reconcile_modelService.getAccountReconcileModelByEntities(this.account_reconcile_modelMapping.toDomain(#account_reconcile_modeldtos)),'iBizBusinessCentral-Account_reconcile_model-Update')")
    @ApiOperation(value = "批量更新请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "批量更新请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_models/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconcile_modelDTO> account_reconcile_modeldtos) {
        account_reconcile_modelService.updateBatch(account_reconcile_modelMapping.toDomain(account_reconcile_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_reconcile_modelService.get(#account_reconcile_model_id),'iBizBusinessCentral-Account_reconcile_model-Remove')")
    @ApiOperation(value = "删除请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "删除请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_models/{account_reconcile_model_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_reconcile_model_id") Long account_reconcile_model_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_reconcile_modelService.remove(account_reconcile_model_id));
    }

    @PreAuthorize("hasPermission(this.account_reconcile_modelService.getAccountReconcileModelByIds(#ids),'iBizBusinessCentral-Account_reconcile_model-Remove')")
    @ApiOperation(value = "批量删除请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "批量删除请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_models/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_reconcile_modelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_reconcile_modelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_reconcile_model-Get')")
    @ApiOperation(value = "获取请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "获取请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_models/{account_reconcile_model_id}")
    public ResponseEntity<Account_reconcile_modelDTO> get(@PathVariable("account_reconcile_model_id") Long account_reconcile_model_id) {
        Account_reconcile_model domain = account_reconcile_modelService.get(account_reconcile_model_id);
        Account_reconcile_modelDTO dto = account_reconcile_modelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取请在发票和付款匹配期间创建日记账分录草稿", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "获取请在发票和付款匹配期间创建日记账分录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_models/getdraft")
    public ResponseEntity<Account_reconcile_modelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_reconcile_modelMapping.toDto(account_reconcile_modelService.getDraft(new Account_reconcile_model())));
    }

    @ApiOperation(value = "检查请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "检查请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_reconcile_modelDTO account_reconcile_modeldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_reconcile_modelService.checkKey(account_reconcile_modelMapping.toDomain(account_reconcile_modeldto)));
    }

    @PreAuthorize("hasPermission(this.account_reconcile_modelMapping.toDomain(#account_reconcile_modeldto),'iBizBusinessCentral-Account_reconcile_model-Save')")
    @ApiOperation(value = "保存请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "保存请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_reconcile_modelDTO account_reconcile_modeldto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_reconcile_modelService.save(account_reconcile_modelMapping.toDomain(account_reconcile_modeldto)));
    }

    @PreAuthorize("hasPermission(this.account_reconcile_modelMapping.toDomain(#account_reconcile_modeldtos),'iBizBusinessCentral-Account_reconcile_model-Save')")
    @ApiOperation(value = "批量保存请在发票和付款匹配期间创建日记账分录", tags = {"请在发票和付款匹配期间创建日记账分录" },  notes = "批量保存请在发票和付款匹配期间创建日记账分录")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_reconcile_modelDTO> account_reconcile_modeldtos) {
        account_reconcile_modelService.saveBatch(account_reconcile_modelMapping.toDomain(account_reconcile_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconcile_model-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_reconcile_model-Get')")
	@ApiOperation(value = "获取数据集", tags = {"请在发票和付款匹配期间创建日记账分录" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconcile_models/fetchdefault")
	public ResponseEntity<List<Account_reconcile_modelDTO>> fetchDefault(Account_reconcile_modelSearchContext context) {
        Page<Account_reconcile_model> domains = account_reconcile_modelService.searchDefault(context) ;
        List<Account_reconcile_modelDTO> list = account_reconcile_modelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconcile_model-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_reconcile_model-Get')")
	@ApiOperation(value = "查询数据集", tags = {"请在发票和付款匹配期间创建日记账分录" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_reconcile_models/searchdefault")
	public ResponseEntity<Page<Account_reconcile_modelDTO>> searchDefault(@RequestBody Account_reconcile_modelSearchContext context) {
        Page<Account_reconcile_model> domains = account_reconcile_modelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_reconcile_modelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

