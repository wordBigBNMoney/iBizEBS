package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.businesscentral.core.dto.Snailmail_letterDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreSnailmail_letterMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Snailmail_letterMapping extends MappingBase<Snailmail_letterDTO, Snailmail_letter> {


}

