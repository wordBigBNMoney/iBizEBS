package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_ratio;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meter_ratioService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Rules for Meter to Meter Ratio" })
@RestController("Core-mro_pm_meter_ratio")
@RequestMapping("")
public class Mro_pm_meter_ratioResource {

    @Autowired
    public IMro_pm_meter_ratioService mro_pm_meter_ratioService;

    @Autowired
    @Lazy
    public Mro_pm_meter_ratioMapping mro_pm_meter_ratioMapping;

    @PreAuthorize("hasPermission(this.mro_pm_meter_ratioMapping.toDomain(#mro_pm_meter_ratiodto),'iBizBusinessCentral-Mro_pm_meter_ratio-Create')")
    @ApiOperation(value = "新建Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "新建Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios")
    public ResponseEntity<Mro_pm_meter_ratioDTO> create(@Validated @RequestBody Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto) {
        Mro_pm_meter_ratio domain = mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodto);
		mro_pm_meter_ratioService.create(domain);
        Mro_pm_meter_ratioDTO dto = mro_pm_meter_ratioMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_ratioMapping.toDomain(#mro_pm_meter_ratiodtos),'iBizBusinessCentral-Mro_pm_meter_ratio-Create')")
    @ApiOperation(value = "批量新建Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "批量新建Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_meter_ratioDTO> mro_pm_meter_ratiodtos) {
        mro_pm_meter_ratioService.createBatch(mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_pm_meter_ratio" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_pm_meter_ratioService.get(#mro_pm_meter_ratio_id),'iBizBusinessCentral-Mro_pm_meter_ratio-Update')")
    @ApiOperation(value = "更新Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "更新Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")
    public ResponseEntity<Mro_pm_meter_ratioDTO> update(@PathVariable("mro_pm_meter_ratio_id") Long mro_pm_meter_ratio_id, @RequestBody Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto) {
		Mro_pm_meter_ratio domain  = mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodto);
        domain .setId(mro_pm_meter_ratio_id);
		mro_pm_meter_ratioService.update(domain );
		Mro_pm_meter_ratioDTO dto = mro_pm_meter_ratioMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_ratioService.getMroPmMeterRatioByEntities(this.mro_pm_meter_ratioMapping.toDomain(#mro_pm_meter_ratiodtos)),'iBizBusinessCentral-Mro_pm_meter_ratio-Update')")
    @ApiOperation(value = "批量更新Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "批量更新Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_ratios/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meter_ratioDTO> mro_pm_meter_ratiodtos) {
        mro_pm_meter_ratioService.updateBatch(mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_ratioService.get(#mro_pm_meter_ratio_id),'iBizBusinessCentral-Mro_pm_meter_ratio-Remove')")
    @ApiOperation(value = "删除Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "删除Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_ratio_id") Long mro_pm_meter_ratio_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_ratioService.remove(mro_pm_meter_ratio_id));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_ratioService.getMroPmMeterRatioByIds(#ids),'iBizBusinessCentral-Mro_pm_meter_ratio-Remove')")
    @ApiOperation(value = "批量删除Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "批量删除Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_ratios/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_pm_meter_ratioService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_pm_meter_ratioMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_pm_meter_ratio-Get')")
    @ApiOperation(value = "获取Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "获取Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")
    public ResponseEntity<Mro_pm_meter_ratioDTO> get(@PathVariable("mro_pm_meter_ratio_id") Long mro_pm_meter_ratio_id) {
        Mro_pm_meter_ratio domain = mro_pm_meter_ratioService.get(mro_pm_meter_ratio_id);
        Mro_pm_meter_ratioDTO dto = mro_pm_meter_ratioMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Rules for Meter to Meter Ratio草稿", tags = {"Rules for Meter to Meter Ratio" },  notes = "获取Rules for Meter to Meter Ratio草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_ratios/getdraft")
    public ResponseEntity<Mro_pm_meter_ratioDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_ratioMapping.toDto(mro_pm_meter_ratioService.getDraft(new Mro_pm_meter_ratio())));
    }

    @ApiOperation(value = "检查Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "检查Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_ratioService.checkKey(mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_ratioMapping.toDomain(#mro_pm_meter_ratiodto),'iBizBusinessCentral-Mro_pm_meter_ratio-Save')")
    @ApiOperation(value = "保存Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "保存Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_ratioService.save(mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_ratioMapping.toDomain(#mro_pm_meter_ratiodtos),'iBizBusinessCentral-Mro_pm_meter_ratio-Save')")
    @ApiOperation(value = "批量保存Rules for Meter to Meter Ratio", tags = {"Rules for Meter to Meter Ratio" },  notes = "批量保存Rules for Meter to Meter Ratio")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_pm_meter_ratioDTO> mro_pm_meter_ratiodtos) {
        mro_pm_meter_ratioService.saveBatch(mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_meter_ratio-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_meter_ratio-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Rules for Meter to Meter Ratio" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meter_ratios/fetchdefault")
	public ResponseEntity<List<Mro_pm_meter_ratioDTO>> fetchDefault(Mro_pm_meter_ratioSearchContext context) {
        Page<Mro_pm_meter_ratio> domains = mro_pm_meter_ratioService.searchDefault(context) ;
        List<Mro_pm_meter_ratioDTO> list = mro_pm_meter_ratioMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_meter_ratio-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_meter_ratio-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Rules for Meter to Meter Ratio" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_pm_meter_ratios/searchdefault")
	public ResponseEntity<Page<Mro_pm_meter_ratioDTO>> searchDefault(@RequestBody Mro_pm_meter_ratioSearchContext context) {
        Page<Mro_pm_meter_ratio> domains = mro_pm_meter_ratioService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_meter_ratioMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

