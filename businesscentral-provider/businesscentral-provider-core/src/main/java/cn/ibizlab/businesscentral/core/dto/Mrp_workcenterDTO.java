package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mrp_workcenterDTO]
 */
@Data
public class Mrp_workcenterDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [COSTS_HOUR]
     *
     */
    @JSONField(name = "costs_hour")
    @JsonProperty("costs_hour")
    private Double costsHour;

    /**
     * 属性 [WORKCENTER_LOAD]
     *
     */
    @JSONField(name = "workcenter_load")
    @JsonProperty("workcenter_load")
    private Double workcenterLoad;

    /**
     * 属性 [WORKING_STATE]
     *
     */
    @JSONField(name = "working_state")
    @JsonProperty("working_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String workingState;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WORKORDER_LATE_COUNT]
     *
     */
    @JSONField(name = "workorder_late_count")
    @JsonProperty("workorder_late_count")
    private Integer workorderLateCount;

    /**
     * 属性 [PERFORMANCE]
     *
     */
    @JSONField(name = "performance")
    @JsonProperty("performance")
    private Integer performance;

    /**
     * 属性 [OEE_TARGET]
     *
     */
    @JSONField(name = "oee_target")
    @JsonProperty("oee_target")
    private Double oeeTarget;

    /**
     * 属性 [CAPACITY]
     *
     */
    @JSONField(name = "capacity")
    @JsonProperty("capacity")
    private Double capacity;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WORKORDER_COUNT]
     *
     */
    @JSONField(name = "workorder_count")
    @JsonProperty("workorder_count")
    private Integer workorderCount;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [TIME_IDS]
     *
     */
    @JSONField(name = "time_ids")
    @JsonProperty("time_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String timeIds;

    /**
     * 属性 [PRODUCTIVE_TIME]
     *
     */
    @JSONField(name = "productive_time")
    @JsonProperty("productive_time")
    private Double productiveTime;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    @NotNull(message = "[序号]不允许为空!")
    private Integer sequence;

    /**
     * 属性 [ROUTING_LINE_IDS]
     *
     */
    @JSONField(name = "routing_line_ids")
    @JsonProperty("routing_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String routingLineIds;

    /**
     * 属性 [OEE]
     *
     */
    @JSONField(name = "oee")
    @JsonProperty("oee")
    private Double oee;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String note;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String code;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [TIME_STOP]
     *
     */
    @JSONField(name = "time_stop")
    @JsonProperty("time_stop")
    private Double timeStop;

    /**
     * 属性 [TIME_START]
     *
     */
    @JSONField(name = "time_start")
    @JsonProperty("time_start")
    private Double timeStart;

    /**
     * 属性 [WORKORDER_PENDING_COUNT]
     *
     */
    @JSONField(name = "workorder_pending_count")
    @JsonProperty("workorder_pending_count")
    private Integer workorderPendingCount;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [BLOCKED_TIME]
     *
     */
    @JSONField(name = "blocked_time")
    @JsonProperty("blocked_time")
    private Double blockedTime;

    /**
     * 属性 [WORKORDER_READY_COUNT]
     *
     */
    @JSONField(name = "workorder_ready_count")
    @JsonProperty("workorder_ready_count")
    private Integer workorderReadyCount;

    /**
     * 属性 [WORKORDER_PROGRESS_COUNT]
     *
     */
    @JSONField(name = "workorder_progress_count")
    @JsonProperty("workorder_progress_count")
    private Integer workorderProgressCount;

    /**
     * 属性 [ORDER_IDS]
     *
     */
    @JSONField(name = "order_ids")
    @JsonProperty("order_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String orderIds;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [TZ]
     *
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tz;

    /**
     * 属性 [TIME_EFFICIENCY]
     *
     */
    @JSONField(name = "time_efficiency")
    @JsonProperty("time_efficiency")
    private Double timeEfficiency;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resourceCalendarIdText;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resourceCalendarId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [RESOURCE_ID]
     *
     */
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[资源]不允许为空!")
    private Long resourceId;


    /**
     * 设置 [COSTS_HOUR]
     */
    public void setCostsHour(Double  costsHour){
        this.costsHour = costsHour ;
        this.modify("costs_hour",costsHour);
    }

    /**
     * 设置 [WORKING_STATE]
     */
    public void setWorkingState(String  workingState){
        this.workingState = workingState ;
        this.modify("working_state",workingState);
    }

    /**
     * 设置 [OEE_TARGET]
     */
    public void setOeeTarget(Double  oeeTarget){
        this.oeeTarget = oeeTarget ;
        this.modify("oee_target",oeeTarget);
    }

    /**
     * 设置 [CAPACITY]
     */
    public void setCapacity(Double  capacity){
        this.capacity = capacity ;
        this.modify("capacity",capacity);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [TIME_STOP]
     */
    public void setTimeStop(Double  timeStop){
        this.timeStop = timeStop ;
        this.modify("time_stop",timeStop);
    }

    /**
     * 设置 [TIME_START]
     */
    public void setTimeStart(Double  timeStart){
        this.timeStart = timeStart ;
        this.modify("time_start",timeStart);
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    public void setResourceCalendarId(Long  resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [RESOURCE_ID]
     */
    public void setResourceId(Long  resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }


}


