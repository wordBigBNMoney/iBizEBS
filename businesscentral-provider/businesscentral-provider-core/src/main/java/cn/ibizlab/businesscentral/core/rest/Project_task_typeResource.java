package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.businesscentral.core.odoo_project.service.IProject_task_typeService;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_task_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"任务阶段" })
@RestController("Core-project_task_type")
@RequestMapping("")
public class Project_task_typeResource {

    @Autowired
    public IProject_task_typeService project_task_typeService;

    @Autowired
    @Lazy
    public Project_task_typeMapping project_task_typeMapping;

    @PreAuthorize("hasPermission(this.project_task_typeMapping.toDomain(#project_task_typedto),'iBizBusinessCentral-Project_task_type-Create')")
    @ApiOperation(value = "新建任务阶段", tags = {"任务阶段" },  notes = "新建任务阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/project_task_types")
    public ResponseEntity<Project_task_typeDTO> create(@Validated @RequestBody Project_task_typeDTO project_task_typedto) {
        Project_task_type domain = project_task_typeMapping.toDomain(project_task_typedto);
		project_task_typeService.create(domain);
        Project_task_typeDTO dto = project_task_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.project_task_typeMapping.toDomain(#project_task_typedtos),'iBizBusinessCentral-Project_task_type-Create')")
    @ApiOperation(value = "批量新建任务阶段", tags = {"任务阶段" },  notes = "批量新建任务阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/project_task_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Project_task_typeDTO> project_task_typedtos) {
        project_task_typeService.createBatch(project_task_typeMapping.toDomain(project_task_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "project_task_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.project_task_typeService.get(#project_task_type_id),'iBizBusinessCentral-Project_task_type-Update')")
    @ApiOperation(value = "更新任务阶段", tags = {"任务阶段" },  notes = "更新任务阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_task_types/{project_task_type_id}")
    public ResponseEntity<Project_task_typeDTO> update(@PathVariable("project_task_type_id") Long project_task_type_id, @RequestBody Project_task_typeDTO project_task_typedto) {
		Project_task_type domain  = project_task_typeMapping.toDomain(project_task_typedto);
        domain .setId(project_task_type_id);
		project_task_typeService.update(domain );
		Project_task_typeDTO dto = project_task_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.project_task_typeService.getProjectTaskTypeByEntities(this.project_task_typeMapping.toDomain(#project_task_typedtos)),'iBizBusinessCentral-Project_task_type-Update')")
    @ApiOperation(value = "批量更新任务阶段", tags = {"任务阶段" },  notes = "批量更新任务阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_task_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_task_typeDTO> project_task_typedtos) {
        project_task_typeService.updateBatch(project_task_typeMapping.toDomain(project_task_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.project_task_typeService.get(#project_task_type_id),'iBizBusinessCentral-Project_task_type-Remove')")
    @ApiOperation(value = "删除任务阶段", tags = {"任务阶段" },  notes = "删除任务阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_task_types/{project_task_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("project_task_type_id") Long project_task_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(project_task_typeService.remove(project_task_type_id));
    }

    @PreAuthorize("hasPermission(this.project_task_typeService.getProjectTaskTypeByIds(#ids),'iBizBusinessCentral-Project_task_type-Remove')")
    @ApiOperation(value = "批量删除任务阶段", tags = {"任务阶段" },  notes = "批量删除任务阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_task_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        project_task_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.project_task_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Project_task_type-Get')")
    @ApiOperation(value = "获取任务阶段", tags = {"任务阶段" },  notes = "获取任务阶段")
	@RequestMapping(method = RequestMethod.GET, value = "/project_task_types/{project_task_type_id}")
    public ResponseEntity<Project_task_typeDTO> get(@PathVariable("project_task_type_id") Long project_task_type_id) {
        Project_task_type domain = project_task_typeService.get(project_task_type_id);
        Project_task_typeDTO dto = project_task_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取任务阶段草稿", tags = {"任务阶段" },  notes = "获取任务阶段草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/project_task_types/getdraft")
    public ResponseEntity<Project_task_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(project_task_typeMapping.toDto(project_task_typeService.getDraft(new Project_task_type())));
    }

    @ApiOperation(value = "检查任务阶段", tags = {"任务阶段" },  notes = "检查任务阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/project_task_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Project_task_typeDTO project_task_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(project_task_typeService.checkKey(project_task_typeMapping.toDomain(project_task_typedto)));
    }

    @PreAuthorize("hasPermission(this.project_task_typeMapping.toDomain(#project_task_typedto),'iBizBusinessCentral-Project_task_type-Save')")
    @ApiOperation(value = "保存任务阶段", tags = {"任务阶段" },  notes = "保存任务阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/project_task_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Project_task_typeDTO project_task_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(project_task_typeService.save(project_task_typeMapping.toDomain(project_task_typedto)));
    }

    @PreAuthorize("hasPermission(this.project_task_typeMapping.toDomain(#project_task_typedtos),'iBizBusinessCentral-Project_task_type-Save')")
    @ApiOperation(value = "批量保存任务阶段", tags = {"任务阶段" },  notes = "批量保存任务阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/project_task_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Project_task_typeDTO> project_task_typedtos) {
        project_task_typeService.saveBatch(project_task_typeMapping.toDomain(project_task_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Project_task_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Project_task_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"任务阶段" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/project_task_types/fetchdefault")
	public ResponseEntity<List<Project_task_typeDTO>> fetchDefault(Project_task_typeSearchContext context) {
        Page<Project_task_type> domains = project_task_typeService.searchDefault(context) ;
        List<Project_task_typeDTO> list = project_task_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Project_task_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Project_task_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"任务阶段" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/project_task_types/searchdefault")
	public ResponseEntity<Page<Project_task_typeDTO>> searchDefault(@RequestBody Project_task_typeSearchContext context) {
        Page<Project_task_type> domains = project_task_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(project_task_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

