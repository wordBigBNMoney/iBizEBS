package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_model_templateService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"核销模型模板" })
@RestController("Core-account_reconcile_model_template")
@RequestMapping("")
public class Account_reconcile_model_templateResource {

    @Autowired
    public IAccount_reconcile_model_templateService account_reconcile_model_templateService;

    @Autowired
    @Lazy
    public Account_reconcile_model_templateMapping account_reconcile_model_templateMapping;

    @PreAuthorize("hasPermission(this.account_reconcile_model_templateMapping.toDomain(#account_reconcile_model_templatedto),'iBizBusinessCentral-Account_reconcile_model_template-Create')")
    @ApiOperation(value = "新建核销模型模板", tags = {"核销模型模板" },  notes = "新建核销模型模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates")
    public ResponseEntity<Account_reconcile_model_templateDTO> create(@Validated @RequestBody Account_reconcile_model_templateDTO account_reconcile_model_templatedto) {
        Account_reconcile_model_template domain = account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedto);
		account_reconcile_model_templateService.create(domain);
        Account_reconcile_model_templateDTO dto = account_reconcile_model_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_reconcile_model_templateMapping.toDomain(#account_reconcile_model_templatedtos),'iBizBusinessCentral-Account_reconcile_model_template-Create')")
    @ApiOperation(value = "批量新建核销模型模板", tags = {"核销模型模板" },  notes = "批量新建核销模型模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_reconcile_model_templateDTO> account_reconcile_model_templatedtos) {
        account_reconcile_model_templateService.createBatch(account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_reconcile_model_template" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_reconcile_model_templateService.get(#account_reconcile_model_template_id),'iBizBusinessCentral-Account_reconcile_model_template-Update')")
    @ApiOperation(value = "更新核销模型模板", tags = {"核销模型模板" },  notes = "更新核销模型模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_model_templates/{account_reconcile_model_template_id}")
    public ResponseEntity<Account_reconcile_model_templateDTO> update(@PathVariable("account_reconcile_model_template_id") Long account_reconcile_model_template_id, @RequestBody Account_reconcile_model_templateDTO account_reconcile_model_templatedto) {
		Account_reconcile_model_template domain  = account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedto);
        domain .setId(account_reconcile_model_template_id);
		account_reconcile_model_templateService.update(domain );
		Account_reconcile_model_templateDTO dto = account_reconcile_model_templateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_reconcile_model_templateService.getAccountReconcileModelTemplateByEntities(this.account_reconcile_model_templateMapping.toDomain(#account_reconcile_model_templatedtos)),'iBizBusinessCentral-Account_reconcile_model_template-Update')")
    @ApiOperation(value = "批量更新核销模型模板", tags = {"核销模型模板" },  notes = "批量更新核销模型模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_model_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconcile_model_templateDTO> account_reconcile_model_templatedtos) {
        account_reconcile_model_templateService.updateBatch(account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_reconcile_model_templateService.get(#account_reconcile_model_template_id),'iBizBusinessCentral-Account_reconcile_model_template-Remove')")
    @ApiOperation(value = "删除核销模型模板", tags = {"核销模型模板" },  notes = "删除核销模型模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_model_templates/{account_reconcile_model_template_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_reconcile_model_template_id") Long account_reconcile_model_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_reconcile_model_templateService.remove(account_reconcile_model_template_id));
    }

    @PreAuthorize("hasPermission(this.account_reconcile_model_templateService.getAccountReconcileModelTemplateByIds(#ids),'iBizBusinessCentral-Account_reconcile_model_template-Remove')")
    @ApiOperation(value = "批量删除核销模型模板", tags = {"核销模型模板" },  notes = "批量删除核销模型模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_model_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_reconcile_model_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_reconcile_model_templateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_reconcile_model_template-Get')")
    @ApiOperation(value = "获取核销模型模板", tags = {"核销模型模板" },  notes = "获取核销模型模板")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_model_templates/{account_reconcile_model_template_id}")
    public ResponseEntity<Account_reconcile_model_templateDTO> get(@PathVariable("account_reconcile_model_template_id") Long account_reconcile_model_template_id) {
        Account_reconcile_model_template domain = account_reconcile_model_templateService.get(account_reconcile_model_template_id);
        Account_reconcile_model_templateDTO dto = account_reconcile_model_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取核销模型模板草稿", tags = {"核销模型模板" },  notes = "获取核销模型模板草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_model_templates/getdraft")
    public ResponseEntity<Account_reconcile_model_templateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_reconcile_model_templateMapping.toDto(account_reconcile_model_templateService.getDraft(new Account_reconcile_model_template())));
    }

    @ApiOperation(value = "检查核销模型模板", tags = {"核销模型模板" },  notes = "检查核销模型模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_reconcile_model_templateDTO account_reconcile_model_templatedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_reconcile_model_templateService.checkKey(account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedto)));
    }

    @PreAuthorize("hasPermission(this.account_reconcile_model_templateMapping.toDomain(#account_reconcile_model_templatedto),'iBizBusinessCentral-Account_reconcile_model_template-Save')")
    @ApiOperation(value = "保存核销模型模板", tags = {"核销模型模板" },  notes = "保存核销模型模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_reconcile_model_templateDTO account_reconcile_model_templatedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_reconcile_model_templateService.save(account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedto)));
    }

    @PreAuthorize("hasPermission(this.account_reconcile_model_templateMapping.toDomain(#account_reconcile_model_templatedtos),'iBizBusinessCentral-Account_reconcile_model_template-Save')")
    @ApiOperation(value = "批量保存核销模型模板", tags = {"核销模型模板" },  notes = "批量保存核销模型模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_reconcile_model_templateDTO> account_reconcile_model_templatedtos) {
        account_reconcile_model_templateService.saveBatch(account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconcile_model_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_reconcile_model_template-Get')")
	@ApiOperation(value = "获取数据集", tags = {"核销模型模板" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconcile_model_templates/fetchdefault")
	public ResponseEntity<List<Account_reconcile_model_templateDTO>> fetchDefault(Account_reconcile_model_templateSearchContext context) {
        Page<Account_reconcile_model_template> domains = account_reconcile_model_templateService.searchDefault(context) ;
        List<Account_reconcile_model_templateDTO> list = account_reconcile_model_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconcile_model_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_reconcile_model_template-Get')")
	@ApiOperation(value = "查询数据集", tags = {"核销模型模板" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_reconcile_model_templates/searchdefault")
	public ResponseEntity<Page<Account_reconcile_model_templateDTO>> searchDefault(@RequestBody Account_reconcile_model_templateSearchContext context) {
        Page<Account_reconcile_model_template> domains = account_reconcile_model_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_reconcile_model_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

