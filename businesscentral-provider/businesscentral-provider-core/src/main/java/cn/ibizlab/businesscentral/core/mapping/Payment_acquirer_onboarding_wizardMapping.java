package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.dto.Payment_acquirer_onboarding_wizardDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CorePayment_acquirer_onboarding_wizardMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Payment_acquirer_onboarding_wizardMapping extends MappingBase<Payment_acquirer_onboarding_wizardDTO, Payment_acquirer_onboarding_wizard> {


}

