package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead_tagService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead_tagSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"线索标签" })
@RestController("Core-crm_lead_tag")
@RequestMapping("")
public class Crm_lead_tagResource {

    @Autowired
    public ICrm_lead_tagService crm_lead_tagService;

    @Autowired
    @Lazy
    public Crm_lead_tagMapping crm_lead_tagMapping;

    @PreAuthorize("hasPermission(this.crm_lead_tagMapping.toDomain(#crm_lead_tagdto),'iBizBusinessCentral-Crm_lead_tag-Create')")
    @ApiOperation(value = "新建线索标签", tags = {"线索标签" },  notes = "新建线索标签")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags")
    public ResponseEntity<Crm_lead_tagDTO> create(@Validated @RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
        Crm_lead_tag domain = crm_lead_tagMapping.toDomain(crm_lead_tagdto);
		crm_lead_tagService.create(domain);
        Crm_lead_tagDTO dto = crm_lead_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lead_tagMapping.toDomain(#crm_lead_tagdtos),'iBizBusinessCentral-Crm_lead_tag-Create')")
    @ApiOperation(value = "批量新建线索标签", tags = {"线索标签" },  notes = "批量新建线索标签")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lead_tagDTO> crm_lead_tagdtos) {
        crm_lead_tagService.createBatch(crm_lead_tagMapping.toDomain(crm_lead_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_lead_tag" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_lead_tagService.get(#crm_lead_tag_id),'iBizBusinessCentral-Crm_lead_tag-Update')")
    @ApiOperation(value = "更新线索标签", tags = {"线索标签" },  notes = "更新线索标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_tags/{crm_lead_tag_id}")
    public ResponseEntity<Crm_lead_tagDTO> update(@PathVariable("crm_lead_tag_id") Long crm_lead_tag_id, @RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
		Crm_lead_tag domain  = crm_lead_tagMapping.toDomain(crm_lead_tagdto);
        domain .setId(crm_lead_tag_id);
		crm_lead_tagService.update(domain );
		Crm_lead_tagDTO dto = crm_lead_tagMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lead_tagService.getCrmLeadTagByEntities(this.crm_lead_tagMapping.toDomain(#crm_lead_tagdtos)),'iBizBusinessCentral-Crm_lead_tag-Update')")
    @ApiOperation(value = "批量更新线索标签", tags = {"线索标签" },  notes = "批量更新线索标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead_tagDTO> crm_lead_tagdtos) {
        crm_lead_tagService.updateBatch(crm_lead_tagMapping.toDomain(crm_lead_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_lead_tagService.get(#crm_lead_tag_id),'iBizBusinessCentral-Crm_lead_tag-Remove')")
    @ApiOperation(value = "删除线索标签", tags = {"线索标签" },  notes = "删除线索标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_tags/{crm_lead_tag_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_tag_id") Long crm_lead_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lead_tagService.remove(crm_lead_tag_id));
    }

    @PreAuthorize("hasPermission(this.crm_lead_tagService.getCrmLeadTagByIds(#ids),'iBizBusinessCentral-Crm_lead_tag-Remove')")
    @ApiOperation(value = "批量删除线索标签", tags = {"线索标签" },  notes = "批量删除线索标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_lead_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_lead_tagMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_lead_tag-Get')")
    @ApiOperation(value = "获取线索标签", tags = {"线索标签" },  notes = "获取线索标签")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_tags/{crm_lead_tag_id}")
    public ResponseEntity<Crm_lead_tagDTO> get(@PathVariable("crm_lead_tag_id") Long crm_lead_tag_id) {
        Crm_lead_tag domain = crm_lead_tagService.get(crm_lead_tag_id);
        Crm_lead_tagDTO dto = crm_lead_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取线索标签草稿", tags = {"线索标签" },  notes = "获取线索标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_tags/getdraft")
    public ResponseEntity<Crm_lead_tagDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_tagMapping.toDto(crm_lead_tagService.getDraft(new Crm_lead_tag())));
    }

    @ApiOperation(value = "检查线索标签", tags = {"线索标签" },  notes = "检查线索标签")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_lead_tagService.checkKey(crm_lead_tagMapping.toDomain(crm_lead_tagdto)));
    }

    @PreAuthorize("hasPermission(this.crm_lead_tagMapping.toDomain(#crm_lead_tagdto),'iBizBusinessCentral-Crm_lead_tag-Save')")
    @ApiOperation(value = "保存线索标签", tags = {"线索标签" },  notes = "保存线索标签")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_tagService.save(crm_lead_tagMapping.toDomain(crm_lead_tagdto)));
    }

    @PreAuthorize("hasPermission(this.crm_lead_tagMapping.toDomain(#crm_lead_tagdtos),'iBizBusinessCentral-Crm_lead_tag-Save')")
    @ApiOperation(value = "批量保存线索标签", tags = {"线索标签" },  notes = "批量保存线索标签")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_lead_tagDTO> crm_lead_tagdtos) {
        crm_lead_tagService.saveBatch(crm_lead_tagMapping.toDomain(crm_lead_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead_tag-Get')")
	@ApiOperation(value = "获取数据集", tags = {"线索标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead_tags/fetchdefault")
	public ResponseEntity<List<Crm_lead_tagDTO>> fetchDefault(Crm_lead_tagSearchContext context) {
        Page<Crm_lead_tag> domains = crm_lead_tagService.searchDefault(context) ;
        List<Crm_lead_tagDTO> list = crm_lead_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead_tag-Get')")
	@ApiOperation(value = "查询数据集", tags = {"线索标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lead_tags/searchdefault")
	public ResponseEntity<Page<Crm_lead_tagDTO>> searchDefault(@RequestBody Crm_lead_tagSearchContext context) {
        Page<Crm_lead_tag> domains = crm_lead_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lead_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

