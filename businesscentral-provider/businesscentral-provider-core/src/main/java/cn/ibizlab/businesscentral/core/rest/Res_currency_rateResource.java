package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currency_rateService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_currency_rateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"汇率" })
@RestController("Core-res_currency_rate")
@RequestMapping("")
public class Res_currency_rateResource {

    @Autowired
    public IRes_currency_rateService res_currency_rateService;

    @Autowired
    @Lazy
    public Res_currency_rateMapping res_currency_rateMapping;

    @PreAuthorize("hasPermission(this.res_currency_rateMapping.toDomain(#res_currency_ratedto),'iBizBusinessCentral-Res_currency_rate-Create')")
    @ApiOperation(value = "新建汇率", tags = {"汇率" },  notes = "新建汇率")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates")
    public ResponseEntity<Res_currency_rateDTO> create(@Validated @RequestBody Res_currency_rateDTO res_currency_ratedto) {
        Res_currency_rate domain = res_currency_rateMapping.toDomain(res_currency_ratedto);
		res_currency_rateService.create(domain);
        Res_currency_rateDTO dto = res_currency_rateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_currency_rateMapping.toDomain(#res_currency_ratedtos),'iBizBusinessCentral-Res_currency_rate-Create')")
    @ApiOperation(value = "批量新建汇率", tags = {"汇率" },  notes = "批量新建汇率")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_currency_rateDTO> res_currency_ratedtos) {
        res_currency_rateService.createBatch(res_currency_rateMapping.toDomain(res_currency_ratedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_currency_rate" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_currency_rateService.get(#res_currency_rate_id),'iBizBusinessCentral-Res_currency_rate-Update')")
    @ApiOperation(value = "更新汇率", tags = {"汇率" },  notes = "更新汇率")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_currency_rates/{res_currency_rate_id}")
    public ResponseEntity<Res_currency_rateDTO> update(@PathVariable("res_currency_rate_id") Long res_currency_rate_id, @RequestBody Res_currency_rateDTO res_currency_ratedto) {
		Res_currency_rate domain  = res_currency_rateMapping.toDomain(res_currency_ratedto);
        domain .setId(res_currency_rate_id);
		res_currency_rateService.update(domain );
		Res_currency_rateDTO dto = res_currency_rateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_currency_rateService.getResCurrencyRateByEntities(this.res_currency_rateMapping.toDomain(#res_currency_ratedtos)),'iBizBusinessCentral-Res_currency_rate-Update')")
    @ApiOperation(value = "批量更新汇率", tags = {"汇率" },  notes = "批量更新汇率")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_currency_rates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_currency_rateDTO> res_currency_ratedtos) {
        res_currency_rateService.updateBatch(res_currency_rateMapping.toDomain(res_currency_ratedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_currency_rateService.get(#res_currency_rate_id),'iBizBusinessCentral-Res_currency_rate-Remove')")
    @ApiOperation(value = "删除汇率", tags = {"汇率" },  notes = "删除汇率")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_currency_rates/{res_currency_rate_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_currency_rate_id") Long res_currency_rate_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_currency_rateService.remove(res_currency_rate_id));
    }

    @PreAuthorize("hasPermission(this.res_currency_rateService.getResCurrencyRateByIds(#ids),'iBizBusinessCentral-Res_currency_rate-Remove')")
    @ApiOperation(value = "批量删除汇率", tags = {"汇率" },  notes = "批量删除汇率")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_currency_rates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_currency_rateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_currency_rateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_currency_rate-Get')")
    @ApiOperation(value = "获取汇率", tags = {"汇率" },  notes = "获取汇率")
	@RequestMapping(method = RequestMethod.GET, value = "/res_currency_rates/{res_currency_rate_id}")
    public ResponseEntity<Res_currency_rateDTO> get(@PathVariable("res_currency_rate_id") Long res_currency_rate_id) {
        Res_currency_rate domain = res_currency_rateService.get(res_currency_rate_id);
        Res_currency_rateDTO dto = res_currency_rateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取汇率草稿", tags = {"汇率" },  notes = "获取汇率草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_currency_rates/getdraft")
    public ResponseEntity<Res_currency_rateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_currency_rateMapping.toDto(res_currency_rateService.getDraft(new Res_currency_rate())));
    }

    @ApiOperation(value = "检查汇率", tags = {"汇率" },  notes = "检查汇率")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_currency_rateDTO res_currency_ratedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_currency_rateService.checkKey(res_currency_rateMapping.toDomain(res_currency_ratedto)));
    }

    @PreAuthorize("hasPermission(this.res_currency_rateMapping.toDomain(#res_currency_ratedto),'iBizBusinessCentral-Res_currency_rate-Save')")
    @ApiOperation(value = "保存汇率", tags = {"汇率" },  notes = "保存汇率")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_currency_rateDTO res_currency_ratedto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_currency_rateService.save(res_currency_rateMapping.toDomain(res_currency_ratedto)));
    }

    @PreAuthorize("hasPermission(this.res_currency_rateMapping.toDomain(#res_currency_ratedtos),'iBizBusinessCentral-Res_currency_rate-Save')")
    @ApiOperation(value = "批量保存汇率", tags = {"汇率" },  notes = "批量保存汇率")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_currency_rateDTO> res_currency_ratedtos) {
        res_currency_rateService.saveBatch(res_currency_rateMapping.toDomain(res_currency_ratedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_currency_rate-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_currency_rate-Get')")
	@ApiOperation(value = "获取数据集", tags = {"汇率" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_currency_rates/fetchdefault")
	public ResponseEntity<List<Res_currency_rateDTO>> fetchDefault(Res_currency_rateSearchContext context) {
        Page<Res_currency_rate> domains = res_currency_rateService.searchDefault(context) ;
        List<Res_currency_rateDTO> list = res_currency_rateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_currency_rate-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_currency_rate-Get')")
	@ApiOperation(value = "查询数据集", tags = {"汇率" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_currency_rates/searchdefault")
	public ResponseEntity<Page<Res_currency_rateDTO>> searchDefault(@RequestBody Res_currency_rateSearchContext context) {
        Page<Res_currency_rate> domains = res_currency_rateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_currency_rateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

