package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_move_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"日记账项目" })
@RestController("Core-account_move_line")
@RequestMapping("")
public class Account_move_lineResource {

    @Autowired
    public IAccount_move_lineService account_move_lineService;

    @Autowired
    @Lazy
    public Account_move_lineMapping account_move_lineMapping;

    @PreAuthorize("hasPermission(this.account_move_lineMapping.toDomain(#account_move_linedto),'iBizBusinessCentral-Account_move_line-Create')")
    @ApiOperation(value = "新建日记账项目", tags = {"日记账项目" },  notes = "新建日记账项目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_lines")
    public ResponseEntity<Account_move_lineDTO> create(@Validated @RequestBody Account_move_lineDTO account_move_linedto) {
        Account_move_line domain = account_move_lineMapping.toDomain(account_move_linedto);
		account_move_lineService.create(domain);
        Account_move_lineDTO dto = account_move_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_move_lineMapping.toDomain(#account_move_linedtos),'iBizBusinessCentral-Account_move_line-Create')")
    @ApiOperation(value = "批量新建日记账项目", tags = {"日记账项目" },  notes = "批量新建日记账项目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_move_lineDTO> account_move_linedtos) {
        account_move_lineService.createBatch(account_move_lineMapping.toDomain(account_move_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_move_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_move_lineService.get(#account_move_line_id),'iBizBusinessCentral-Account_move_line-Update')")
    @ApiOperation(value = "更新日记账项目", tags = {"日记账项目" },  notes = "更新日记账项目")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_move_lines/{account_move_line_id}")
    public ResponseEntity<Account_move_lineDTO> update(@PathVariable("account_move_line_id") Long account_move_line_id, @RequestBody Account_move_lineDTO account_move_linedto) {
		Account_move_line domain  = account_move_lineMapping.toDomain(account_move_linedto);
        domain .setId(account_move_line_id);
		account_move_lineService.update(domain );
		Account_move_lineDTO dto = account_move_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_move_lineService.getAccountMoveLineByEntities(this.account_move_lineMapping.toDomain(#account_move_linedtos)),'iBizBusinessCentral-Account_move_line-Update')")
    @ApiOperation(value = "批量更新日记账项目", tags = {"日记账项目" },  notes = "批量更新日记账项目")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_move_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_move_lineDTO> account_move_linedtos) {
        account_move_lineService.updateBatch(account_move_lineMapping.toDomain(account_move_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_move_lineService.get(#account_move_line_id),'iBizBusinessCentral-Account_move_line-Remove')")
    @ApiOperation(value = "删除日记账项目", tags = {"日记账项目" },  notes = "删除日记账项目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_move_lines/{account_move_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_move_line_id") Long account_move_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_move_lineService.remove(account_move_line_id));
    }

    @PreAuthorize("hasPermission(this.account_move_lineService.getAccountMoveLineByIds(#ids),'iBizBusinessCentral-Account_move_line-Remove')")
    @ApiOperation(value = "批量删除日记账项目", tags = {"日记账项目" },  notes = "批量删除日记账项目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_move_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_move_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_move_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_move_line-Get')")
    @ApiOperation(value = "获取日记账项目", tags = {"日记账项目" },  notes = "获取日记账项目")
	@RequestMapping(method = RequestMethod.GET, value = "/account_move_lines/{account_move_line_id}")
    public ResponseEntity<Account_move_lineDTO> get(@PathVariable("account_move_line_id") Long account_move_line_id) {
        Account_move_line domain = account_move_lineService.get(account_move_line_id);
        Account_move_lineDTO dto = account_move_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取日记账项目草稿", tags = {"日记账项目" },  notes = "获取日记账项目草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_move_lines/getdraft")
    public ResponseEntity<Account_move_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_move_lineMapping.toDto(account_move_lineService.getDraft(new Account_move_line())));
    }

    @ApiOperation(value = "检查日记账项目", tags = {"日记账项目" },  notes = "检查日记账项目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_move_lineDTO account_move_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_move_lineService.checkKey(account_move_lineMapping.toDomain(account_move_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_move_lineMapping.toDomain(#account_move_linedto),'iBizBusinessCentral-Account_move_line-Save')")
    @ApiOperation(value = "保存日记账项目", tags = {"日记账项目" },  notes = "保存日记账项目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_move_lineDTO account_move_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_move_lineService.save(account_move_lineMapping.toDomain(account_move_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_move_lineMapping.toDomain(#account_move_linedtos),'iBizBusinessCentral-Account_move_line-Save')")
    @ApiOperation(value = "批量保存日记账项目", tags = {"日记账项目" },  notes = "批量保存日记账项目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_move_lineDTO> account_move_linedtos) {
        account_move_lineService.saveBatch(account_move_lineMapping.toDomain(account_move_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_move_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_move_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"日记账项目" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_move_lines/fetchdefault")
	public ResponseEntity<List<Account_move_lineDTO>> fetchDefault(Account_move_lineSearchContext context) {
        Page<Account_move_line> domains = account_move_lineService.searchDefault(context) ;
        List<Account_move_lineDTO> list = account_move_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_move_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_move_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"日记账项目" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_move_lines/searchdefault")
	public ResponseEntity<Page<Account_move_lineDTO>> searchDefault(@RequestBody Account_move_lineSearchContext context) {
        Page<Account_move_line> domains = account_move_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_move_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

