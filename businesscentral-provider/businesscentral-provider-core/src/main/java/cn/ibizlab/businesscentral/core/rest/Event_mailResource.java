package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_mail;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_mailService;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_mailSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"自动发邮件" })
@RestController("Core-event_mail")
@RequestMapping("")
public class Event_mailResource {

    @Autowired
    public IEvent_mailService event_mailService;

    @Autowired
    @Lazy
    public Event_mailMapping event_mailMapping;

    @PreAuthorize("hasPermission(this.event_mailMapping.toDomain(#event_maildto),'iBizBusinessCentral-Event_mail-Create')")
    @ApiOperation(value = "新建自动发邮件", tags = {"自动发邮件" },  notes = "新建自动发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mails")
    public ResponseEntity<Event_mailDTO> create(@Validated @RequestBody Event_mailDTO event_maildto) {
        Event_mail domain = event_mailMapping.toDomain(event_maildto);
		event_mailService.create(domain);
        Event_mailDTO dto = event_mailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_mailMapping.toDomain(#event_maildtos),'iBizBusinessCentral-Event_mail-Create')")
    @ApiOperation(value = "批量新建自动发邮件", tags = {"自动发邮件" },  notes = "批量新建自动发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_mailDTO> event_maildtos) {
        event_mailService.createBatch(event_mailMapping.toDomain(event_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "event_mail" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.event_mailService.get(#event_mail_id),'iBizBusinessCentral-Event_mail-Update')")
    @ApiOperation(value = "更新自动发邮件", tags = {"自动发邮件" },  notes = "更新自动发邮件")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_mails/{event_mail_id}")
    public ResponseEntity<Event_mailDTO> update(@PathVariable("event_mail_id") Long event_mail_id, @RequestBody Event_mailDTO event_maildto) {
		Event_mail domain  = event_mailMapping.toDomain(event_maildto);
        domain .setId(event_mail_id);
		event_mailService.update(domain );
		Event_mailDTO dto = event_mailMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_mailService.getEventMailByEntities(this.event_mailMapping.toDomain(#event_maildtos)),'iBizBusinessCentral-Event_mail-Update')")
    @ApiOperation(value = "批量更新自动发邮件", tags = {"自动发邮件" },  notes = "批量更新自动发邮件")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_mails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_mailDTO> event_maildtos) {
        event_mailService.updateBatch(event_mailMapping.toDomain(event_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.event_mailService.get(#event_mail_id),'iBizBusinessCentral-Event_mail-Remove')")
    @ApiOperation(value = "删除自动发邮件", tags = {"自动发邮件" },  notes = "删除自动发邮件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_mails/{event_mail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("event_mail_id") Long event_mail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_mailService.remove(event_mail_id));
    }

    @PreAuthorize("hasPermission(this.event_mailService.getEventMailByIds(#ids),'iBizBusinessCentral-Event_mail-Remove')")
    @ApiOperation(value = "批量删除自动发邮件", tags = {"自动发邮件" },  notes = "批量删除自动发邮件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_mails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        event_mailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.event_mailMapping.toDomain(returnObject.body),'iBizBusinessCentral-Event_mail-Get')")
    @ApiOperation(value = "获取自动发邮件", tags = {"自动发邮件" },  notes = "获取自动发邮件")
	@RequestMapping(method = RequestMethod.GET, value = "/event_mails/{event_mail_id}")
    public ResponseEntity<Event_mailDTO> get(@PathVariable("event_mail_id") Long event_mail_id) {
        Event_mail domain = event_mailService.get(event_mail_id);
        Event_mailDTO dto = event_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取自动发邮件草稿", tags = {"自动发邮件" },  notes = "获取自动发邮件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/event_mails/getdraft")
    public ResponseEntity<Event_mailDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(event_mailMapping.toDto(event_mailService.getDraft(new Event_mail())));
    }

    @ApiOperation(value = "检查自动发邮件", tags = {"自动发邮件" },  notes = "检查自动发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Event_mailDTO event_maildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(event_mailService.checkKey(event_mailMapping.toDomain(event_maildto)));
    }

    @PreAuthorize("hasPermission(this.event_mailMapping.toDomain(#event_maildto),'iBizBusinessCentral-Event_mail-Save')")
    @ApiOperation(value = "保存自动发邮件", tags = {"自动发邮件" },  notes = "保存自动发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mails/save")
    public ResponseEntity<Boolean> save(@RequestBody Event_mailDTO event_maildto) {
        return ResponseEntity.status(HttpStatus.OK).body(event_mailService.save(event_mailMapping.toDomain(event_maildto)));
    }

    @PreAuthorize("hasPermission(this.event_mailMapping.toDomain(#event_maildtos),'iBizBusinessCentral-Event_mail-Save')")
    @ApiOperation(value = "批量保存自动发邮件", tags = {"自动发邮件" },  notes = "批量保存自动发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Event_mailDTO> event_maildtos) {
        event_mailService.saveBatch(event_mailMapping.toDomain(event_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_mail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_mail-Get')")
	@ApiOperation(value = "获取数据集", tags = {"自动发邮件" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/event_mails/fetchdefault")
	public ResponseEntity<List<Event_mailDTO>> fetchDefault(Event_mailSearchContext context) {
        Page<Event_mail> domains = event_mailService.searchDefault(context) ;
        List<Event_mailDTO> list = event_mailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_mail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_mail-Get')")
	@ApiOperation(value = "查询数据集", tags = {"自动发邮件" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/event_mails/searchdefault")
	public ResponseEntity<Page<Event_mailDTO>> searchDefault(@RequestBody Event_mailSearchContext context) {
        Page<Event_mail> domains = event_mailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_mailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

