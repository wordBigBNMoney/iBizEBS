package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_feeService;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_feeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"修理费" })
@RestController("Core-repair_fee")
@RequestMapping("")
public class Repair_feeResource {

    @Autowired
    public IRepair_feeService repair_feeService;

    @Autowired
    @Lazy
    public Repair_feeMapping repair_feeMapping;

    @PreAuthorize("hasPermission(this.repair_feeMapping.toDomain(#repair_feedto),'iBizBusinessCentral-Repair_fee-Create')")
    @ApiOperation(value = "新建修理费", tags = {"修理费" },  notes = "新建修理费")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_fees")
    public ResponseEntity<Repair_feeDTO> create(@Validated @RequestBody Repair_feeDTO repair_feedto) {
        Repair_fee domain = repair_feeMapping.toDomain(repair_feedto);
		repair_feeService.create(domain);
        Repair_feeDTO dto = repair_feeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_feeMapping.toDomain(#repair_feedtos),'iBizBusinessCentral-Repair_fee-Create')")
    @ApiOperation(value = "批量新建修理费", tags = {"修理费" },  notes = "批量新建修理费")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_fees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_feeDTO> repair_feedtos) {
        repair_feeService.createBatch(repair_feeMapping.toDomain(repair_feedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "repair_fee" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.repair_feeService.get(#repair_fee_id),'iBizBusinessCentral-Repair_fee-Update')")
    @ApiOperation(value = "更新修理费", tags = {"修理费" },  notes = "更新修理费")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_fees/{repair_fee_id}")
    public ResponseEntity<Repair_feeDTO> update(@PathVariable("repair_fee_id") Long repair_fee_id, @RequestBody Repair_feeDTO repair_feedto) {
		Repair_fee domain  = repair_feeMapping.toDomain(repair_feedto);
        domain .setId(repair_fee_id);
		repair_feeService.update(domain );
		Repair_feeDTO dto = repair_feeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_feeService.getRepairFeeByEntities(this.repair_feeMapping.toDomain(#repair_feedtos)),'iBizBusinessCentral-Repair_fee-Update')")
    @ApiOperation(value = "批量更新修理费", tags = {"修理费" },  notes = "批量更新修理费")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_fees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_feeDTO> repair_feedtos) {
        repair_feeService.updateBatch(repair_feeMapping.toDomain(repair_feedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.repair_feeService.get(#repair_fee_id),'iBizBusinessCentral-Repair_fee-Remove')")
    @ApiOperation(value = "删除修理费", tags = {"修理费" },  notes = "删除修理费")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_fees/{repair_fee_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("repair_fee_id") Long repair_fee_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_feeService.remove(repair_fee_id));
    }

    @PreAuthorize("hasPermission(this.repair_feeService.getRepairFeeByIds(#ids),'iBizBusinessCentral-Repair_fee-Remove')")
    @ApiOperation(value = "批量删除修理费", tags = {"修理费" },  notes = "批量删除修理费")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_fees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        repair_feeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.repair_feeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Repair_fee-Get')")
    @ApiOperation(value = "获取修理费", tags = {"修理费" },  notes = "获取修理费")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_fees/{repair_fee_id}")
    public ResponseEntity<Repair_feeDTO> get(@PathVariable("repair_fee_id") Long repair_fee_id) {
        Repair_fee domain = repair_feeService.get(repair_fee_id);
        Repair_feeDTO dto = repair_feeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取修理费草稿", tags = {"修理费" },  notes = "获取修理费草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_fees/getdraft")
    public ResponseEntity<Repair_feeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(repair_feeMapping.toDto(repair_feeService.getDraft(new Repair_fee())));
    }

    @ApiOperation(value = "检查修理费", tags = {"修理费" },  notes = "检查修理费")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_fees/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Repair_feeDTO repair_feedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(repair_feeService.checkKey(repair_feeMapping.toDomain(repair_feedto)));
    }

    @PreAuthorize("hasPermission(this.repair_feeMapping.toDomain(#repair_feedto),'iBizBusinessCentral-Repair_fee-Save')")
    @ApiOperation(value = "保存修理费", tags = {"修理费" },  notes = "保存修理费")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_fees/save")
    public ResponseEntity<Boolean> save(@RequestBody Repair_feeDTO repair_feedto) {
        return ResponseEntity.status(HttpStatus.OK).body(repair_feeService.save(repair_feeMapping.toDomain(repair_feedto)));
    }

    @PreAuthorize("hasPermission(this.repair_feeMapping.toDomain(#repair_feedtos),'iBizBusinessCentral-Repair_fee-Save')")
    @ApiOperation(value = "批量保存修理费", tags = {"修理费" },  notes = "批量保存修理费")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_fees/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Repair_feeDTO> repair_feedtos) {
        repair_feeService.saveBatch(repair_feeMapping.toDomain(repair_feedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_fee-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_fee-Get')")
	@ApiOperation(value = "获取数据集", tags = {"修理费" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/repair_fees/fetchdefault")
	public ResponseEntity<List<Repair_feeDTO>> fetchDefault(Repair_feeSearchContext context) {
        Page<Repair_fee> domains = repair_feeService.searchDefault(context) ;
        List<Repair_feeDTO> list = repair_feeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_fee-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_fee-Get')")
	@ApiOperation(value = "查询数据集", tags = {"修理费" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/repair_fees/searchdefault")
	public ResponseEntity<Page<Repair_feeDTO>> searchDefault(@RequestBody Repair_feeSearchContext context) {
        Page<Repair_fee> domains = repair_feeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_feeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

