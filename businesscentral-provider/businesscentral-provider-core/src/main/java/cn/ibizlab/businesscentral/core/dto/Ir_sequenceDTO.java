package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Ir_sequenceDTO]
 */
@Data
public class Ir_sequenceDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [USE_DATE_RANGE]
     *
     */
    @JSONField(name = "use_date_range")
    @JsonProperty("use_date_range")
    private Boolean useDateRange;

    /**
     * 属性 [NUMBER_INCREMENT]
     *
     */
    @JSONField(name = "number_increment")
    @JsonProperty("number_increment")
    private Integer numberIncrement;

    /**
     * 属性 [SUFFIX]
     *
     */
    @JSONField(name = "suffix")
    @JsonProperty("suffix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String suffix;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [NEXT_CHAR]
     *
     */
    @JSONField(name = "next_char")
    @JsonProperty("next_char")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String nextChar;

    /**
     * 属性 [NEXT_VAL]
     *
     */
    @JSONField(name = "next_val")
    @JsonProperty("next_val")
    private Integer nextVal;

    /**
     * 属性 [PADDING]
     *
     */
    @JSONField(name = "padding")
    @JsonProperty("padding")
    private Integer padding;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [NUMBER_NEXT]
     *
     */
    @JSONField(name = "number_next")
    @JsonProperty("number_next")
    private Integer numberNext;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[序列名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String code;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [IMPLEMENTATION]
     *
     */
    @JSONField(name = "implementation")
    @JsonProperty("implementation")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String implementation;

    /**
     * 属性 [PREFIX]
     *
     */
    @JSONField(name = "prefix")
    @JsonProperty("prefix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String prefix;

    /**
     * 属性 [CREATE_UNAME]
     *
     */
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String createUname;

    /**
     * 属性 [WRITE_UNAME]
     *
     */
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String writeUname;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [USE_DATE_RANGE]
     */
    public void setUseDateRange(Boolean  useDateRange){
        this.useDateRange = useDateRange ;
        this.modify("use_date_range",useDateRange);
    }

    /**
     * 设置 [NUMBER_INCREMENT]
     */
    public void setNumberIncrement(Integer  numberIncrement){
        this.numberIncrement = numberIncrement ;
        this.modify("number_increment",numberIncrement);
    }

    /**
     * 设置 [SUFFIX]
     */
    public void setSuffix(String  suffix){
        this.suffix = suffix ;
        this.modify("suffix",suffix);
    }

    /**
     * 设置 [PADDING]
     */
    public void setPadding(Integer  padding){
        this.padding = padding ;
        this.modify("padding",padding);
    }

    /**
     * 设置 [CREATE_DATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [NUMBER_NEXT]
     */
    public void setNumberNext(Integer  numberNext){
        this.numberNext = numberNext ;
        this.modify("number_next",numberNext);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [WRITE_DATE]
     */
    public void setWriteDate(Timestamp  writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 设置 [IMPLEMENTATION]
     */
    public void setImplementation(String  implementation){
        this.implementation = implementation ;
        this.modify("implementation",implementation);
    }

    /**
     * 设置 [PREFIX]
     */
    public void setPrefix(String  prefix){
        this.prefix = prefix ;
        this.modify("prefix",prefix);
    }

    /**
     * 设置 [WRITE_UID]
     */
    public void setWriteUid(Long  writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [CREATE_UID]
     */
    public void setCreateUid(Long  createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }


}


