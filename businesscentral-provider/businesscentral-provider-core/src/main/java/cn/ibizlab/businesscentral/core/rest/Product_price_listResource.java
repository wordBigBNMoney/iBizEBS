package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_price_listService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_price_listSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"基于价格列表版本的单位产品价格" })
@RestController("Core-product_price_list")
@RequestMapping("")
public class Product_price_listResource {

    @Autowired
    public IProduct_price_listService product_price_listService;

    @Autowired
    @Lazy
    public Product_price_listMapping product_price_listMapping;

    @PreAuthorize("hasPermission(this.product_price_listMapping.toDomain(#product_price_listdto),'iBizBusinessCentral-Product_price_list-Create')")
    @ApiOperation(value = "新建基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "新建基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists")
    public ResponseEntity<Product_price_listDTO> create(@Validated @RequestBody Product_price_listDTO product_price_listdto) {
        Product_price_list domain = product_price_listMapping.toDomain(product_price_listdto);
		product_price_listService.create(domain);
        Product_price_listDTO dto = product_price_listMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_price_listMapping.toDomain(#product_price_listdtos),'iBizBusinessCentral-Product_price_list-Create')")
    @ApiOperation(value = "批量新建基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "批量新建基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_price_listDTO> product_price_listdtos) {
        product_price_listService.createBatch(product_price_listMapping.toDomain(product_price_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_price_list" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_price_listService.get(#product_price_list_id),'iBizBusinessCentral-Product_price_list-Update')")
    @ApiOperation(value = "更新基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "更新基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_lists/{product_price_list_id}")
    public ResponseEntity<Product_price_listDTO> update(@PathVariable("product_price_list_id") Long product_price_list_id, @RequestBody Product_price_listDTO product_price_listdto) {
		Product_price_list domain  = product_price_listMapping.toDomain(product_price_listdto);
        domain .setId(product_price_list_id);
		product_price_listService.update(domain );
		Product_price_listDTO dto = product_price_listMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_price_listService.getProductPriceListByEntities(this.product_price_listMapping.toDomain(#product_price_listdtos)),'iBizBusinessCentral-Product_price_list-Update')")
    @ApiOperation(value = "批量更新基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "批量更新基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_lists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_price_listDTO> product_price_listdtos) {
        product_price_listService.updateBatch(product_price_listMapping.toDomain(product_price_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_price_listService.get(#product_price_list_id),'iBizBusinessCentral-Product_price_list-Remove')")
    @ApiOperation(value = "删除基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "删除基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_lists/{product_price_list_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_price_list_id") Long product_price_list_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_price_listService.remove(product_price_list_id));
    }

    @PreAuthorize("hasPermission(this.product_price_listService.getProductPriceListByIds(#ids),'iBizBusinessCentral-Product_price_list-Remove')")
    @ApiOperation(value = "批量删除基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "批量删除基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_lists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_price_listService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_price_listMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_price_list-Get')")
    @ApiOperation(value = "获取基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "获取基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/{product_price_list_id}")
    public ResponseEntity<Product_price_listDTO> get(@PathVariable("product_price_list_id") Long product_price_list_id) {
        Product_price_list domain = product_price_listService.get(product_price_list_id);
        Product_price_listDTO dto = product_price_listMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取基于价格列表版本的单位产品价格草稿", tags = {"基于价格列表版本的单位产品价格" },  notes = "获取基于价格列表版本的单位产品价格草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/getdraft")
    public ResponseEntity<Product_price_listDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_price_listMapping.toDto(product_price_listService.getDraft(new Product_price_list())));
    }

    @ApiOperation(value = "检查基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "检查基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_price_listDTO product_price_listdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_price_listService.checkKey(product_price_listMapping.toDomain(product_price_listdto)));
    }

    @PreAuthorize("hasPermission(this.product_price_listMapping.toDomain(#product_price_listdto),'iBizBusinessCentral-Product_price_list-Save')")
    @ApiOperation(value = "保存基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "保存基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_price_listDTO product_price_listdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_price_listService.save(product_price_listMapping.toDomain(product_price_listdto)));
    }

    @PreAuthorize("hasPermission(this.product_price_listMapping.toDomain(#product_price_listdtos),'iBizBusinessCentral-Product_price_list-Save')")
    @ApiOperation(value = "批量保存基于价格列表版本的单位产品价格", tags = {"基于价格列表版本的单位产品价格" },  notes = "批量保存基于价格列表版本的单位产品价格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_price_listDTO> product_price_listdtos) {
        product_price_listService.saveBatch(product_price_listMapping.toDomain(product_price_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_price_list-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_price_list-Get')")
	@ApiOperation(value = "获取数据集", tags = {"基于价格列表版本的单位产品价格" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_price_lists/fetchdefault")
	public ResponseEntity<List<Product_price_listDTO>> fetchDefault(Product_price_listSearchContext context) {
        Page<Product_price_list> domains = product_price_listService.searchDefault(context) ;
        List<Product_price_listDTO> list = product_price_listMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_price_list-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_price_list-Get')")
	@ApiOperation(value = "查询数据集", tags = {"基于价格列表版本的单位产品价格" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_price_lists/searchdefault")
	public ResponseEntity<Page<Product_price_listDTO>> searchDefault(@RequestBody Product_price_listSearchContext context) {
        Page<Product_price_list> domains = product_price_listService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_price_listMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

