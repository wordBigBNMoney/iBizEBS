package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.businesscentral.core.odoo_asset.service.IAsset_assetService;
import cn.ibizlab.businesscentral.core.odoo_asset.filter.Asset_assetSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Asset" })
@RestController("Core-asset_asset")
@RequestMapping("")
public class Asset_assetResource {

    @Autowired
    public IAsset_assetService asset_assetService;

    @Autowired
    @Lazy
    public Asset_assetMapping asset_assetMapping;

    @PreAuthorize("hasPermission(this.asset_assetMapping.toDomain(#asset_assetdto),'iBizBusinessCentral-Asset_asset-Create')")
    @ApiOperation(value = "新建Asset", tags = {"Asset" },  notes = "新建Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_assets")
    public ResponseEntity<Asset_assetDTO> create(@Validated @RequestBody Asset_assetDTO asset_assetdto) {
        Asset_asset domain = asset_assetMapping.toDomain(asset_assetdto);
		asset_assetService.create(domain);
        Asset_assetDTO dto = asset_assetMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.asset_assetMapping.toDomain(#asset_assetdtos),'iBizBusinessCentral-Asset_asset-Create')")
    @ApiOperation(value = "批量新建Asset", tags = {"Asset" },  notes = "批量新建Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_assets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Asset_assetDTO> asset_assetdtos) {
        asset_assetService.createBatch(asset_assetMapping.toDomain(asset_assetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "asset_asset" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.asset_assetService.get(#asset_asset_id),'iBizBusinessCentral-Asset_asset-Update')")
    @ApiOperation(value = "更新Asset", tags = {"Asset" },  notes = "更新Asset")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_assets/{asset_asset_id}")
    public ResponseEntity<Asset_assetDTO> update(@PathVariable("asset_asset_id") Long asset_asset_id, @RequestBody Asset_assetDTO asset_assetdto) {
		Asset_asset domain  = asset_assetMapping.toDomain(asset_assetdto);
        domain .setId(asset_asset_id);
		asset_assetService.update(domain );
		Asset_assetDTO dto = asset_assetMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.asset_assetService.getAssetAssetByEntities(this.asset_assetMapping.toDomain(#asset_assetdtos)),'iBizBusinessCentral-Asset_asset-Update')")
    @ApiOperation(value = "批量更新Asset", tags = {"Asset" },  notes = "批量更新Asset")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_assets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_assetDTO> asset_assetdtos) {
        asset_assetService.updateBatch(asset_assetMapping.toDomain(asset_assetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.asset_assetService.get(#asset_asset_id),'iBizBusinessCentral-Asset_asset-Remove')")
    @ApiOperation(value = "删除Asset", tags = {"Asset" },  notes = "删除Asset")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_assets/{asset_asset_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("asset_asset_id") Long asset_asset_id) {
         return ResponseEntity.status(HttpStatus.OK).body(asset_assetService.remove(asset_asset_id));
    }

    @PreAuthorize("hasPermission(this.asset_assetService.getAssetAssetByIds(#ids),'iBizBusinessCentral-Asset_asset-Remove')")
    @ApiOperation(value = "批量删除Asset", tags = {"Asset" },  notes = "批量删除Asset")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_assets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        asset_assetService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.asset_assetMapping.toDomain(returnObject.body),'iBizBusinessCentral-Asset_asset-Get')")
    @ApiOperation(value = "获取Asset", tags = {"Asset" },  notes = "获取Asset")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_assets/{asset_asset_id}")
    public ResponseEntity<Asset_assetDTO> get(@PathVariable("asset_asset_id") Long asset_asset_id) {
        Asset_asset domain = asset_assetService.get(asset_asset_id);
        Asset_assetDTO dto = asset_assetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Asset草稿", tags = {"Asset" },  notes = "获取Asset草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_assets/getdraft")
    public ResponseEntity<Asset_assetDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(asset_assetMapping.toDto(asset_assetService.getDraft(new Asset_asset())));
    }

    @ApiOperation(value = "检查Asset", tags = {"Asset" },  notes = "检查Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_assets/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Asset_assetDTO asset_assetdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(asset_assetService.checkKey(asset_assetMapping.toDomain(asset_assetdto)));
    }

    @PreAuthorize("hasPermission(this.asset_assetMapping.toDomain(#asset_assetdto),'iBizBusinessCentral-Asset_asset-Save')")
    @ApiOperation(value = "保存Asset", tags = {"Asset" },  notes = "保存Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_assets/save")
    public ResponseEntity<Boolean> save(@RequestBody Asset_assetDTO asset_assetdto) {
        return ResponseEntity.status(HttpStatus.OK).body(asset_assetService.save(asset_assetMapping.toDomain(asset_assetdto)));
    }

    @PreAuthorize("hasPermission(this.asset_assetMapping.toDomain(#asset_assetdtos),'iBizBusinessCentral-Asset_asset-Save')")
    @ApiOperation(value = "批量保存Asset", tags = {"Asset" },  notes = "批量保存Asset")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_assets/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Asset_assetDTO> asset_assetdtos) {
        asset_assetService.saveBatch(asset_assetMapping.toDomain(asset_assetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Asset_asset-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Asset_asset-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Asset" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/asset_assets/fetchdefault")
	public ResponseEntity<List<Asset_assetDTO>> fetchDefault(Asset_assetSearchContext context) {
        Page<Asset_asset> domains = asset_assetService.searchDefault(context) ;
        List<Asset_assetDTO> list = asset_assetMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Asset_asset-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Asset_asset-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Asset" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/asset_assets/searchdefault")
	public ResponseEntity<Page<Asset_assetDTO>> searchDefault(@RequestBody Asset_assetSearchContext context) {
        Page<Asset_asset> domains = asset_assetService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(asset_assetMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

