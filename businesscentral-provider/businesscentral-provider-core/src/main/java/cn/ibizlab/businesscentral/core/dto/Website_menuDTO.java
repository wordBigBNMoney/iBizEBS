package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Website_menuDTO]
 */
@Data
public class Website_menuDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [THEME_TEMPLATE_ID]
     *
     */
    @JSONField(name = "theme_template_id")
    @JsonProperty("theme_template_id")
    private Integer themeTemplateId;

    /**
     * 属性 [IS_VISIBLE]
     *
     */
    @JSONField(name = "is_visible")
    @JsonProperty("is_visible")
    private Boolean isVisible;

    /**
     * 属性 [URL]
     *
     */
    @JSONField(name = "url")
    @JsonProperty("url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String url;

    /**
     * 属性 [NEW_WINDOW]
     *
     */
    @JSONField(name = "new_window")
    @JsonProperty("new_window")
    private Boolean newWindow;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PARENT_PATH]
     *
     */
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String parentPath;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CHILD_ID]
     *
     */
    @JSONField(name = "child_id")
    @JsonProperty("child_id")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String childId;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[菜单]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PAGE_ID_TEXT]
     *
     */
    @JSONField(name = "page_id_text")
    @JsonProperty("page_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pageIdText;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String parentIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [PAGE_ID]
     *
     */
    @JSONField(name = "page_id")
    @JsonProperty("page_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pageId;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [THEME_TEMPLATE_ID]
     */
    public void setThemeTemplateId(Integer  themeTemplateId){
        this.themeTemplateId = themeTemplateId ;
        this.modify("theme_template_id",themeTemplateId);
    }

    /**
     * 设置 [URL]
     */
    public void setUrl(String  url){
        this.url = url ;
        this.modify("url",url);
    }

    /**
     * 设置 [NEW_WINDOW]
     */
    public void setNewWindow(Boolean  newWindow){
        this.newWindow = newWindow ;
        this.modify("new_window",newWindow);
    }

    /**
     * 设置 [PARENT_PATH]
     */
    public void setParentPath(String  parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PAGE_ID]
     */
    public void setPageId(Long  pageId){
        this.pageId = pageId ;
        this.modify("page_id",pageId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Long  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }


}


