package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_tokenService;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_tokenSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"付款令牌" })
@RestController("Core-payment_token")
@RequestMapping("")
public class Payment_tokenResource {

    @Autowired
    public IPayment_tokenService payment_tokenService;

    @Autowired
    @Lazy
    public Payment_tokenMapping payment_tokenMapping;

    @PreAuthorize("hasPermission(this.payment_tokenMapping.toDomain(#payment_tokendto),'iBizBusinessCentral-Payment_token-Create')")
    @ApiOperation(value = "新建付款令牌", tags = {"付款令牌" },  notes = "新建付款令牌")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_tokens")
    public ResponseEntity<Payment_tokenDTO> create(@Validated @RequestBody Payment_tokenDTO payment_tokendto) {
        Payment_token domain = payment_tokenMapping.toDomain(payment_tokendto);
		payment_tokenService.create(domain);
        Payment_tokenDTO dto = payment_tokenMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_tokenMapping.toDomain(#payment_tokendtos),'iBizBusinessCentral-Payment_token-Create')")
    @ApiOperation(value = "批量新建付款令牌", tags = {"付款令牌" },  notes = "批量新建付款令牌")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_tokens/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_tokenDTO> payment_tokendtos) {
        payment_tokenService.createBatch(payment_tokenMapping.toDomain(payment_tokendtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "payment_token" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.payment_tokenService.get(#payment_token_id),'iBizBusinessCentral-Payment_token-Update')")
    @ApiOperation(value = "更新付款令牌", tags = {"付款令牌" },  notes = "更新付款令牌")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_tokens/{payment_token_id}")
    public ResponseEntity<Payment_tokenDTO> update(@PathVariable("payment_token_id") Long payment_token_id, @RequestBody Payment_tokenDTO payment_tokendto) {
		Payment_token domain  = payment_tokenMapping.toDomain(payment_tokendto);
        domain .setId(payment_token_id);
		payment_tokenService.update(domain );
		Payment_tokenDTO dto = payment_tokenMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_tokenService.getPaymentTokenByEntities(this.payment_tokenMapping.toDomain(#payment_tokendtos)),'iBizBusinessCentral-Payment_token-Update')")
    @ApiOperation(value = "批量更新付款令牌", tags = {"付款令牌" },  notes = "批量更新付款令牌")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_tokens/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_tokenDTO> payment_tokendtos) {
        payment_tokenService.updateBatch(payment_tokenMapping.toDomain(payment_tokendtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.payment_tokenService.get(#payment_token_id),'iBizBusinessCentral-Payment_token-Remove')")
    @ApiOperation(value = "删除付款令牌", tags = {"付款令牌" },  notes = "删除付款令牌")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_tokens/{payment_token_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("payment_token_id") Long payment_token_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_tokenService.remove(payment_token_id));
    }

    @PreAuthorize("hasPermission(this.payment_tokenService.getPaymentTokenByIds(#ids),'iBizBusinessCentral-Payment_token-Remove')")
    @ApiOperation(value = "批量删除付款令牌", tags = {"付款令牌" },  notes = "批量删除付款令牌")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_tokens/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        payment_tokenService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.payment_tokenMapping.toDomain(returnObject.body),'iBizBusinessCentral-Payment_token-Get')")
    @ApiOperation(value = "获取付款令牌", tags = {"付款令牌" },  notes = "获取付款令牌")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_tokens/{payment_token_id}")
    public ResponseEntity<Payment_tokenDTO> get(@PathVariable("payment_token_id") Long payment_token_id) {
        Payment_token domain = payment_tokenService.get(payment_token_id);
        Payment_tokenDTO dto = payment_tokenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取付款令牌草稿", tags = {"付款令牌" },  notes = "获取付款令牌草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_tokens/getdraft")
    public ResponseEntity<Payment_tokenDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(payment_tokenMapping.toDto(payment_tokenService.getDraft(new Payment_token())));
    }

    @ApiOperation(value = "检查付款令牌", tags = {"付款令牌" },  notes = "检查付款令牌")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_tokens/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Payment_tokenDTO payment_tokendto) {
        return  ResponseEntity.status(HttpStatus.OK).body(payment_tokenService.checkKey(payment_tokenMapping.toDomain(payment_tokendto)));
    }

    @PreAuthorize("hasPermission(this.payment_tokenMapping.toDomain(#payment_tokendto),'iBizBusinessCentral-Payment_token-Save')")
    @ApiOperation(value = "保存付款令牌", tags = {"付款令牌" },  notes = "保存付款令牌")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_tokens/save")
    public ResponseEntity<Boolean> save(@RequestBody Payment_tokenDTO payment_tokendto) {
        return ResponseEntity.status(HttpStatus.OK).body(payment_tokenService.save(payment_tokenMapping.toDomain(payment_tokendto)));
    }

    @PreAuthorize("hasPermission(this.payment_tokenMapping.toDomain(#payment_tokendtos),'iBizBusinessCentral-Payment_token-Save')")
    @ApiOperation(value = "批量保存付款令牌", tags = {"付款令牌" },  notes = "批量保存付款令牌")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_tokens/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Payment_tokenDTO> payment_tokendtos) {
        payment_tokenService.saveBatch(payment_tokenMapping.toDomain(payment_tokendtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_token-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_token-Get')")
	@ApiOperation(value = "获取数据集", tags = {"付款令牌" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/payment_tokens/fetchdefault")
	public ResponseEntity<List<Payment_tokenDTO>> fetchDefault(Payment_tokenSearchContext context) {
        Page<Payment_token> domains = payment_tokenService.searchDefault(context) ;
        List<Payment_tokenDTO> list = payment_tokenMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_token-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_token-Get')")
	@ApiOperation(value = "查询数据集", tags = {"付款令牌" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/payment_tokens/searchdefault")
	public ResponseEntity<Page<Payment_tokenDTO>> searchDefault(@RequestBody Payment_tokenSearchContext context) {
        Page<Payment_token> domains = payment_tokenService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_tokenMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

