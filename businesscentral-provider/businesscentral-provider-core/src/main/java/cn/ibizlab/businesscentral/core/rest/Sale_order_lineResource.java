package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售订单行" })
@RestController("Core-sale_order_line")
@RequestMapping("")
public class Sale_order_lineResource {

    @Autowired
    public ISale_order_lineService sale_order_lineService;

    @Autowired
    @Lazy
    public Sale_order_lineMapping sale_order_lineMapping;

    @PreAuthorize("hasPermission(this.sale_order_lineMapping.toDomain(#sale_order_linedto),'iBizBusinessCentral-Sale_order_line-Create')")
    @ApiOperation(value = "新建销售订单行", tags = {"销售订单行" },  notes = "新建销售订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines")
    public ResponseEntity<Sale_order_lineDTO> create(@Validated @RequestBody Sale_order_lineDTO sale_order_linedto) {
        Sale_order_line domain = sale_order_lineMapping.toDomain(sale_order_linedto);
		sale_order_lineService.create(domain);
        Sale_order_lineDTO dto = sale_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_lineMapping.toDomain(#sale_order_linedtos),'iBizBusinessCentral-Sale_order_line-Create')")
    @ApiOperation(value = "批量新建销售订单行", tags = {"销售订单行" },  notes = "批量新建销售订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_order_lineDTO> sale_order_linedtos) {
        sale_order_lineService.createBatch(sale_order_lineMapping.toDomain(sale_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sale_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sale_order_lineService.get(#sale_order_line_id),'iBizBusinessCentral-Sale_order_line-Update')")
    @ApiOperation(value = "更新销售订单行", tags = {"销售订单行" },  notes = "更新销售订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_lines/{sale_order_line_id}")
    public ResponseEntity<Sale_order_lineDTO> update(@PathVariable("sale_order_line_id") Long sale_order_line_id, @RequestBody Sale_order_lineDTO sale_order_linedto) {
		Sale_order_line domain  = sale_order_lineMapping.toDomain(sale_order_linedto);
        domain .setId(sale_order_line_id);
		sale_order_lineService.update(domain );
		Sale_order_lineDTO dto = sale_order_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_lineService.getSaleOrderLineByEntities(this.sale_order_lineMapping.toDomain(#sale_order_linedtos)),'iBizBusinessCentral-Sale_order_line-Update')")
    @ApiOperation(value = "批量更新销售订单行", tags = {"销售订单行" },  notes = "批量更新销售订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_lineDTO> sale_order_linedtos) {
        sale_order_lineService.updateBatch(sale_order_lineMapping.toDomain(sale_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sale_order_lineService.get(#sale_order_line_id),'iBizBusinessCentral-Sale_order_line-Remove')")
    @ApiOperation(value = "删除销售订单行", tags = {"销售订单行" },  notes = "删除销售订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_lines/{sale_order_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_line_id") Long sale_order_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_order_lineService.remove(sale_order_line_id));
    }

    @PreAuthorize("hasPermission(this.sale_order_lineService.getSaleOrderLineByIds(#ids),'iBizBusinessCentral-Sale_order_line-Remove')")
    @ApiOperation(value = "批量删除销售订单行", tags = {"销售订单行" },  notes = "批量删除销售订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sale_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sale_order_line-Get')")
    @ApiOperation(value = "获取销售订单行", tags = {"销售订单行" },  notes = "获取销售订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_lines/{sale_order_line_id}")
    public ResponseEntity<Sale_order_lineDTO> get(@PathVariable("sale_order_line_id") Long sale_order_line_id) {
        Sale_order_line domain = sale_order_lineService.get(sale_order_line_id);
        Sale_order_lineDTO dto = sale_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售订单行草稿", tags = {"销售订单行" },  notes = "获取销售订单行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_lines/getdraft")
    public ResponseEntity<Sale_order_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_lineMapping.toDto(sale_order_lineService.getDraft(new Sale_order_line())));
    }

    @ApiOperation(value = "检查销售订单行", tags = {"销售订单行" },  notes = "检查销售订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_order_lineDTO sale_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_order_lineService.checkKey(sale_order_lineMapping.toDomain(sale_order_linedto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_lineMapping.toDomain(#sale_order_linedto),'iBizBusinessCentral-Sale_order_line-Save')")
    @ApiOperation(value = "保存销售订单行", tags = {"销售订单行" },  notes = "保存销售订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_order_lineDTO sale_order_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_lineService.save(sale_order_lineMapping.toDomain(sale_order_linedto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_lineMapping.toDomain(#sale_order_linedtos),'iBizBusinessCentral-Sale_order_line-Save')")
    @ApiOperation(value = "批量保存销售订单行", tags = {"销售订单行" },  notes = "批量保存销售订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_order_lineDTO> sale_order_linedtos) {
        sale_order_lineService.saveBatch(sale_order_lineMapping.toDomain(sale_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"销售订单行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_order_lines/fetchdefault")
	public ResponseEntity<List<Sale_order_lineDTO>> fetchDefault(Sale_order_lineSearchContext context) {
        Page<Sale_order_line> domains = sale_order_lineService.searchDefault(context) ;
        List<Sale_order_lineDTO> list = sale_order_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"销售订单行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_order_lines/searchdefault")
	public ResponseEntity<Page<Sale_order_lineDTO>> searchDefault(@RequestBody Sale_order_lineSearchContext context) {
        Page<Sale_order_line> domains = sale_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

