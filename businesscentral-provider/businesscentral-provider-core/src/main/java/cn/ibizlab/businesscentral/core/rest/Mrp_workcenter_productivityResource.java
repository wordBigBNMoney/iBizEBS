package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivityService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作中心生产力日志" })
@RestController("Core-mrp_workcenter_productivity")
@RequestMapping("")
public class Mrp_workcenter_productivityResource {

    @Autowired
    public IMrp_workcenter_productivityService mrp_workcenter_productivityService;

    @Autowired
    @Lazy
    public Mrp_workcenter_productivityMapping mrp_workcenter_productivityMapping;

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivityMapping.toDomain(#mrp_workcenter_productivitydto),'iBizBusinessCentral-Mrp_workcenter_productivity-Create')")
    @ApiOperation(value = "新建工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "新建工作中心生产力日志")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities")
    public ResponseEntity<Mrp_workcenter_productivityDTO> create(@Validated @RequestBody Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto) {
        Mrp_workcenter_productivity domain = mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydto);
		mrp_workcenter_productivityService.create(domain);
        Mrp_workcenter_productivityDTO dto = mrp_workcenter_productivityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivityMapping.toDomain(#mrp_workcenter_productivitydtos),'iBizBusinessCentral-Mrp_workcenter_productivity-Create')")
    @ApiOperation(value = "批量新建工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "批量新建工作中心生产力日志")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workcenter_productivityDTO> mrp_workcenter_productivitydtos) {
        mrp_workcenter_productivityService.createBatch(mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_workcenter_productivity" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_workcenter_productivityService.get(#mrp_workcenter_productivity_id),'iBizBusinessCentral-Mrp_workcenter_productivity-Update')")
    @ApiOperation(value = "更新工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "更新工作中心生产力日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")
    public ResponseEntity<Mrp_workcenter_productivityDTO> update(@PathVariable("mrp_workcenter_productivity_id") Long mrp_workcenter_productivity_id, @RequestBody Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto) {
		Mrp_workcenter_productivity domain  = mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydto);
        domain .setId(mrp_workcenter_productivity_id);
		mrp_workcenter_productivityService.update(domain );
		Mrp_workcenter_productivityDTO dto = mrp_workcenter_productivityMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivityService.getMrpWorkcenterProductivityByEntities(this.mrp_workcenter_productivityMapping.toDomain(#mrp_workcenter_productivitydtos)),'iBizBusinessCentral-Mrp_workcenter_productivity-Update')")
    @ApiOperation(value = "批量更新工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "批量更新工作中心生产力日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivities/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivityDTO> mrp_workcenter_productivitydtos) {
        mrp_workcenter_productivityService.updateBatch(mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivityService.get(#mrp_workcenter_productivity_id),'iBizBusinessCentral-Mrp_workcenter_productivity-Remove')")
    @ApiOperation(value = "删除工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "删除工作中心生产力日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_id") Long mrp_workcenter_productivity_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivityService.remove(mrp_workcenter_productivity_id));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivityService.getMrpWorkcenterProductivityByIds(#ids),'iBizBusinessCentral-Mrp_workcenter_productivity-Remove')")
    @ApiOperation(value = "批量删除工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "批量删除工作中心生产力日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivities/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_workcenter_productivityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_workcenter_productivityMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_workcenter_productivity-Get')")
    @ApiOperation(value = "获取工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "获取工作中心生产力日志")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")
    public ResponseEntity<Mrp_workcenter_productivityDTO> get(@PathVariable("mrp_workcenter_productivity_id") Long mrp_workcenter_productivity_id) {
        Mrp_workcenter_productivity domain = mrp_workcenter_productivityService.get(mrp_workcenter_productivity_id);
        Mrp_workcenter_productivityDTO dto = mrp_workcenter_productivityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作中心生产力日志草稿", tags = {"工作中心生产力日志" },  notes = "获取工作中心生产力日志草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivities/getdraft")
    public ResponseEntity<Mrp_workcenter_productivityDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivityMapping.toDto(mrp_workcenter_productivityService.getDraft(new Mrp_workcenter_productivity())));
    }

    @ApiOperation(value = "检查工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "检查工作中心生产力日志")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivityService.checkKey(mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivityMapping.toDomain(#mrp_workcenter_productivitydto),'iBizBusinessCentral-Mrp_workcenter_productivity-Save')")
    @ApiOperation(value = "保存工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "保存工作中心生产力日志")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivityService.save(mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivityMapping.toDomain(#mrp_workcenter_productivitydtos),'iBizBusinessCentral-Mrp_workcenter_productivity-Save')")
    @ApiOperation(value = "批量保存工作中心生产力日志", tags = {"工作中心生产力日志" },  notes = "批量保存工作中心生产力日志")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_workcenter_productivityDTO> mrp_workcenter_productivitydtos) {
        mrp_workcenter_productivityService.saveBatch(mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workcenter_productivity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workcenter_productivity-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作中心生产力日志" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivities/fetchdefault")
	public ResponseEntity<List<Mrp_workcenter_productivityDTO>> fetchDefault(Mrp_workcenter_productivitySearchContext context) {
        Page<Mrp_workcenter_productivity> domains = mrp_workcenter_productivityService.searchDefault(context) ;
        List<Mrp_workcenter_productivityDTO> list = mrp_workcenter_productivityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workcenter_productivity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workcenter_productivity-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作中心生产力日志" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_workcenter_productivities/searchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivityDTO>> searchDefault(@RequestBody Mrp_workcenter_productivitySearchContext context) {
        Page<Mrp_workcenter_productivity> domains = mrp_workcenter_productivityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workcenter_productivityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

