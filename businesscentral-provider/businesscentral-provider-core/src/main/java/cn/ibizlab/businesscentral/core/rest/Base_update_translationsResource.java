package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_update_translationsService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_update_translationsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"更新翻译" })
@RestController("Core-base_update_translations")
@RequestMapping("")
public class Base_update_translationsResource {

    @Autowired
    public IBase_update_translationsService base_update_translationsService;

    @Autowired
    @Lazy
    public Base_update_translationsMapping base_update_translationsMapping;

    @PreAuthorize("hasPermission(this.base_update_translationsMapping.toDomain(#base_update_translationsdto),'iBizBusinessCentral-Base_update_translations-Create')")
    @ApiOperation(value = "新建更新翻译", tags = {"更新翻译" },  notes = "新建更新翻译")
	@RequestMapping(method = RequestMethod.POST, value = "/base_update_translations")
    public ResponseEntity<Base_update_translationsDTO> create(@Validated @RequestBody Base_update_translationsDTO base_update_translationsdto) {
        Base_update_translations domain = base_update_translationsMapping.toDomain(base_update_translationsdto);
		base_update_translationsService.create(domain);
        Base_update_translationsDTO dto = base_update_translationsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_update_translationsMapping.toDomain(#base_update_translationsdtos),'iBizBusinessCentral-Base_update_translations-Create')")
    @ApiOperation(value = "批量新建更新翻译", tags = {"更新翻译" },  notes = "批量新建更新翻译")
	@RequestMapping(method = RequestMethod.POST, value = "/base_update_translations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_update_translationsDTO> base_update_translationsdtos) {
        base_update_translationsService.createBatch(base_update_translationsMapping.toDomain(base_update_translationsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_update_translations" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_update_translationsService.get(#base_update_translations_id),'iBizBusinessCentral-Base_update_translations-Update')")
    @ApiOperation(value = "更新更新翻译", tags = {"更新翻译" },  notes = "更新更新翻译")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_update_translations/{base_update_translations_id}")
    public ResponseEntity<Base_update_translationsDTO> update(@PathVariable("base_update_translations_id") Long base_update_translations_id, @RequestBody Base_update_translationsDTO base_update_translationsdto) {
		Base_update_translations domain  = base_update_translationsMapping.toDomain(base_update_translationsdto);
        domain .setId(base_update_translations_id);
		base_update_translationsService.update(domain );
		Base_update_translationsDTO dto = base_update_translationsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_update_translationsService.getBaseUpdateTranslationsByEntities(this.base_update_translationsMapping.toDomain(#base_update_translationsdtos)),'iBizBusinessCentral-Base_update_translations-Update')")
    @ApiOperation(value = "批量更新更新翻译", tags = {"更新翻译" },  notes = "批量更新更新翻译")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_update_translations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_update_translationsDTO> base_update_translationsdtos) {
        base_update_translationsService.updateBatch(base_update_translationsMapping.toDomain(base_update_translationsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_update_translationsService.get(#base_update_translations_id),'iBizBusinessCentral-Base_update_translations-Remove')")
    @ApiOperation(value = "删除更新翻译", tags = {"更新翻译" },  notes = "删除更新翻译")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_update_translations/{base_update_translations_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_update_translations_id") Long base_update_translations_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_update_translationsService.remove(base_update_translations_id));
    }

    @PreAuthorize("hasPermission(this.base_update_translationsService.getBaseUpdateTranslationsByIds(#ids),'iBizBusinessCentral-Base_update_translations-Remove')")
    @ApiOperation(value = "批量删除更新翻译", tags = {"更新翻译" },  notes = "批量删除更新翻译")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_update_translations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_update_translationsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_update_translationsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_update_translations-Get')")
    @ApiOperation(value = "获取更新翻译", tags = {"更新翻译" },  notes = "获取更新翻译")
	@RequestMapping(method = RequestMethod.GET, value = "/base_update_translations/{base_update_translations_id}")
    public ResponseEntity<Base_update_translationsDTO> get(@PathVariable("base_update_translations_id") Long base_update_translations_id) {
        Base_update_translations domain = base_update_translationsService.get(base_update_translations_id);
        Base_update_translationsDTO dto = base_update_translationsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取更新翻译草稿", tags = {"更新翻译" },  notes = "获取更新翻译草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_update_translations/getdraft")
    public ResponseEntity<Base_update_translationsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_update_translationsMapping.toDto(base_update_translationsService.getDraft(new Base_update_translations())));
    }

    @ApiOperation(value = "检查更新翻译", tags = {"更新翻译" },  notes = "检查更新翻译")
	@RequestMapping(method = RequestMethod.POST, value = "/base_update_translations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_update_translationsDTO base_update_translationsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_update_translationsService.checkKey(base_update_translationsMapping.toDomain(base_update_translationsdto)));
    }

    @PreAuthorize("hasPermission(this.base_update_translationsMapping.toDomain(#base_update_translationsdto),'iBizBusinessCentral-Base_update_translations-Save')")
    @ApiOperation(value = "保存更新翻译", tags = {"更新翻译" },  notes = "保存更新翻译")
	@RequestMapping(method = RequestMethod.POST, value = "/base_update_translations/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_update_translationsDTO base_update_translationsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_update_translationsService.save(base_update_translationsMapping.toDomain(base_update_translationsdto)));
    }

    @PreAuthorize("hasPermission(this.base_update_translationsMapping.toDomain(#base_update_translationsdtos),'iBizBusinessCentral-Base_update_translations-Save')")
    @ApiOperation(value = "批量保存更新翻译", tags = {"更新翻译" },  notes = "批量保存更新翻译")
	@RequestMapping(method = RequestMethod.POST, value = "/base_update_translations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_update_translationsDTO> base_update_translationsdtos) {
        base_update_translationsService.saveBatch(base_update_translationsMapping.toDomain(base_update_translationsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_update_translations-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_update_translations-Get')")
	@ApiOperation(value = "获取数据集", tags = {"更新翻译" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_update_translations/fetchdefault")
	public ResponseEntity<List<Base_update_translationsDTO>> fetchDefault(Base_update_translationsSearchContext context) {
        Page<Base_update_translations> domains = base_update_translationsService.searchDefault(context) ;
        List<Base_update_translationsDTO> list = base_update_translationsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_update_translations-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_update_translations-Get')")
	@ApiOperation(value = "查询数据集", tags = {"更新翻译" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_update_translations/searchdefault")
	public ResponseEntity<Page<Base_update_translationsDTO>> searchDefault(@RequestBody Base_update_translationsSearchContext context) {
        Page<Base_update_translations> domains = base_update_translationsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_update_translationsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

