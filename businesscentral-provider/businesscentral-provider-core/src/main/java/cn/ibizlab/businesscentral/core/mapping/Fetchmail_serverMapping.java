package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.businesscentral.core.dto.Fetchmail_serverDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreFetchmail_serverMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fetchmail_serverMapping extends MappingBase<Fetchmail_serverDTO, Fetchmail_server> {


}

