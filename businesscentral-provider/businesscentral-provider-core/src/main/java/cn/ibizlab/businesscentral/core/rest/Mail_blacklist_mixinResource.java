package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_blacklist_mixin;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_blacklist_mixinService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_blacklist_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"mixin邮件黑名单" })
@RestController("Core-mail_blacklist_mixin")
@RequestMapping("")
public class Mail_blacklist_mixinResource {

    @Autowired
    public IMail_blacklist_mixinService mail_blacklist_mixinService;

    @Autowired
    @Lazy
    public Mail_blacklist_mixinMapping mail_blacklist_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Create-all')")
    @ApiOperation(value = "新建mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "新建mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklist_mixins")
    public ResponseEntity<Mail_blacklist_mixinDTO> create(@Validated @RequestBody Mail_blacklist_mixinDTO mail_blacklist_mixindto) {
        Mail_blacklist_mixin domain = mail_blacklist_mixinMapping.toDomain(mail_blacklist_mixindto);
		mail_blacklist_mixinService.create(domain);
        Mail_blacklist_mixinDTO dto = mail_blacklist_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Create-all')")
    @ApiOperation(value = "批量新建mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "批量新建mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklist_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_blacklist_mixinDTO> mail_blacklist_mixindtos) {
        mail_blacklist_mixinService.createBatch(mail_blacklist_mixinMapping.toDomain(mail_blacklist_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Update-all')")
    @ApiOperation(value = "更新mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "更新mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklist_mixins/{mail_blacklist_mixin_id}")
    public ResponseEntity<Mail_blacklist_mixinDTO> update(@PathVariable("mail_blacklist_mixin_id") Long mail_blacklist_mixin_id, @RequestBody Mail_blacklist_mixinDTO mail_blacklist_mixindto) {
		Mail_blacklist_mixin domain  = mail_blacklist_mixinMapping.toDomain(mail_blacklist_mixindto);
        domain .setId(mail_blacklist_mixin_id);
		mail_blacklist_mixinService.update(domain );
		Mail_blacklist_mixinDTO dto = mail_blacklist_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Update-all')")
    @ApiOperation(value = "批量更新mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "批量更新mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklist_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_blacklist_mixinDTO> mail_blacklist_mixindtos) {
        mail_blacklist_mixinService.updateBatch(mail_blacklist_mixinMapping.toDomain(mail_blacklist_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Remove-all')")
    @ApiOperation(value = "删除mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "删除mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklist_mixins/{mail_blacklist_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_blacklist_mixin_id") Long mail_blacklist_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_blacklist_mixinService.remove(mail_blacklist_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Remove-all')")
    @ApiOperation(value = "批量删除mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "批量删除mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklist_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_blacklist_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Get-all')")
    @ApiOperation(value = "获取mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "获取mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_blacklist_mixins/{mail_blacklist_mixin_id}")
    public ResponseEntity<Mail_blacklist_mixinDTO> get(@PathVariable("mail_blacklist_mixin_id") Long mail_blacklist_mixin_id) {
        Mail_blacklist_mixin domain = mail_blacklist_mixinService.get(mail_blacklist_mixin_id);
        Mail_blacklist_mixinDTO dto = mail_blacklist_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取mixin邮件黑名单草稿", tags = {"mixin邮件黑名单" },  notes = "获取mixin邮件黑名单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_blacklist_mixins/getdraft")
    public ResponseEntity<Mail_blacklist_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_blacklist_mixinMapping.toDto(mail_blacklist_mixinService.getDraft(new Mail_blacklist_mixin())));
    }

    @ApiOperation(value = "检查mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "检查mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklist_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_blacklist_mixinDTO mail_blacklist_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_blacklist_mixinService.checkKey(mail_blacklist_mixinMapping.toDomain(mail_blacklist_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Save-all')")
    @ApiOperation(value = "保存mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "保存mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklist_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_blacklist_mixinDTO mail_blacklist_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_blacklist_mixinService.save(mail_blacklist_mixinMapping.toDomain(mail_blacklist_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-Save-all')")
    @ApiOperation(value = "批量保存mixin邮件黑名单", tags = {"mixin邮件黑名单" },  notes = "批量保存mixin邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklist_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_blacklist_mixinDTO> mail_blacklist_mixindtos) {
        mail_blacklist_mixinService.saveBatch(mail_blacklist_mixinMapping.toDomain(mail_blacklist_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"mixin邮件黑名单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_blacklist_mixins/fetchdefault")
	public ResponseEntity<List<Mail_blacklist_mixinDTO>> fetchDefault(Mail_blacklist_mixinSearchContext context) {
        Page<Mail_blacklist_mixin> domains = mail_blacklist_mixinService.searchDefault(context) ;
        List<Mail_blacklist_mixinDTO> list = mail_blacklist_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"mixin邮件黑名单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_blacklist_mixins/searchdefault")
	public ResponseEntity<Page<Mail_blacklist_mixinDTO>> searchDefault(@RequestBody Mail_blacklist_mixinSearchContext context) {
        Page<Mail_blacklist_mixin> domains = mail_blacklist_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_blacklist_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

