package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_rule_line;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_rule_lineService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_rule_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Rule for Task" })
@RestController("Core-mro_pm_rule_line")
@RequestMapping("")
public class Mro_pm_rule_lineResource {

    @Autowired
    public IMro_pm_rule_lineService mro_pm_rule_lineService;

    @Autowired
    @Lazy
    public Mro_pm_rule_lineMapping mro_pm_rule_lineMapping;

    @PreAuthorize("hasPermission(this.mro_pm_rule_lineMapping.toDomain(#mro_pm_rule_linedto),'iBizBusinessCentral-Mro_pm_rule_line-Create')")
    @ApiOperation(value = "新建Rule for Task", tags = {"Rule for Task" },  notes = "新建Rule for Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines")
    public ResponseEntity<Mro_pm_rule_lineDTO> create(@Validated @RequestBody Mro_pm_rule_lineDTO mro_pm_rule_linedto) {
        Mro_pm_rule_line domain = mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedto);
		mro_pm_rule_lineService.create(domain);
        Mro_pm_rule_lineDTO dto = mro_pm_rule_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_rule_lineMapping.toDomain(#mro_pm_rule_linedtos),'iBizBusinessCentral-Mro_pm_rule_line-Create')")
    @ApiOperation(value = "批量新建Rule for Task", tags = {"Rule for Task" },  notes = "批量新建Rule for Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_rule_lineDTO> mro_pm_rule_linedtos) {
        mro_pm_rule_lineService.createBatch(mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_pm_rule_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_pm_rule_lineService.get(#mro_pm_rule_line_id),'iBizBusinessCentral-Mro_pm_rule_line-Update')")
    @ApiOperation(value = "更新Rule for Task", tags = {"Rule for Task" },  notes = "更新Rule for Task")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rule_lines/{mro_pm_rule_line_id}")
    public ResponseEntity<Mro_pm_rule_lineDTO> update(@PathVariable("mro_pm_rule_line_id") Long mro_pm_rule_line_id, @RequestBody Mro_pm_rule_lineDTO mro_pm_rule_linedto) {
		Mro_pm_rule_line domain  = mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedto);
        domain .setId(mro_pm_rule_line_id);
		mro_pm_rule_lineService.update(domain );
		Mro_pm_rule_lineDTO dto = mro_pm_rule_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_rule_lineService.getMroPmRuleLineByEntities(this.mro_pm_rule_lineMapping.toDomain(#mro_pm_rule_linedtos)),'iBizBusinessCentral-Mro_pm_rule_line-Update')")
    @ApiOperation(value = "批量更新Rule for Task", tags = {"Rule for Task" },  notes = "批量更新Rule for Task")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rule_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_rule_lineDTO> mro_pm_rule_linedtos) {
        mro_pm_rule_lineService.updateBatch(mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_pm_rule_lineService.get(#mro_pm_rule_line_id),'iBizBusinessCentral-Mro_pm_rule_line-Remove')")
    @ApiOperation(value = "删除Rule for Task", tags = {"Rule for Task" },  notes = "删除Rule for Task")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rule_lines/{mro_pm_rule_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_rule_line_id") Long mro_pm_rule_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_rule_lineService.remove(mro_pm_rule_line_id));
    }

    @PreAuthorize("hasPermission(this.mro_pm_rule_lineService.getMroPmRuleLineByIds(#ids),'iBizBusinessCentral-Mro_pm_rule_line-Remove')")
    @ApiOperation(value = "批量删除Rule for Task", tags = {"Rule for Task" },  notes = "批量删除Rule for Task")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rule_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_pm_rule_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_pm_rule_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_pm_rule_line-Get')")
    @ApiOperation(value = "获取Rule for Task", tags = {"Rule for Task" },  notes = "获取Rule for Task")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rule_lines/{mro_pm_rule_line_id}")
    public ResponseEntity<Mro_pm_rule_lineDTO> get(@PathVariable("mro_pm_rule_line_id") Long mro_pm_rule_line_id) {
        Mro_pm_rule_line domain = mro_pm_rule_lineService.get(mro_pm_rule_line_id);
        Mro_pm_rule_lineDTO dto = mro_pm_rule_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Rule for Task草稿", tags = {"Rule for Task" },  notes = "获取Rule for Task草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rule_lines/getdraft")
    public ResponseEntity<Mro_pm_rule_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_rule_lineMapping.toDto(mro_pm_rule_lineService.getDraft(new Mro_pm_rule_line())));
    }

    @ApiOperation(value = "检查Rule for Task", tags = {"Rule for Task" },  notes = "检查Rule for Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_pm_rule_lineDTO mro_pm_rule_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_pm_rule_lineService.checkKey(mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_rule_lineMapping.toDomain(#mro_pm_rule_linedto),'iBizBusinessCentral-Mro_pm_rule_line-Save')")
    @ApiOperation(value = "保存Rule for Task", tags = {"Rule for Task" },  notes = "保存Rule for Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_pm_rule_lineDTO mro_pm_rule_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_rule_lineService.save(mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_rule_lineMapping.toDomain(#mro_pm_rule_linedtos),'iBizBusinessCentral-Mro_pm_rule_line-Save')")
    @ApiOperation(value = "批量保存Rule for Task", tags = {"Rule for Task" },  notes = "批量保存Rule for Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_pm_rule_lineDTO> mro_pm_rule_linedtos) {
        mro_pm_rule_lineService.saveBatch(mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_rule_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_rule_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Rule for Task" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_rule_lines/fetchdefault")
	public ResponseEntity<List<Mro_pm_rule_lineDTO>> fetchDefault(Mro_pm_rule_lineSearchContext context) {
        Page<Mro_pm_rule_line> domains = mro_pm_rule_lineService.searchDefault(context) ;
        List<Mro_pm_rule_lineDTO> list = mro_pm_rule_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_rule_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_rule_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Rule for Task" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_pm_rule_lines/searchdefault")
	public ResponseEntity<Page<Mro_pm_rule_lineDTO>> searchDefault(@RequestBody Mro_pm_rule_lineSearchContext context) {
        Page<Mro_pm_rule_line> domains = mro_pm_rule_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_rule_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

