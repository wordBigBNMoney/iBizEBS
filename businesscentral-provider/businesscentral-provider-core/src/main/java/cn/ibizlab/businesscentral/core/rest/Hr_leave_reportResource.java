package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_report;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_reportService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leave_reportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"请假摘要/报告" })
@RestController("Core-hr_leave_report")
@RequestMapping("")
public class Hr_leave_reportResource {

    @Autowired
    public IHr_leave_reportService hr_leave_reportService;

    @Autowired
    @Lazy
    public Hr_leave_reportMapping hr_leave_reportMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Create-all')")
    @ApiOperation(value = "新建请假摘要/报告", tags = {"请假摘要/报告" },  notes = "新建请假摘要/报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports")
    public ResponseEntity<Hr_leave_reportDTO> create(@Validated @RequestBody Hr_leave_reportDTO hr_leave_reportdto) {
        Hr_leave_report domain = hr_leave_reportMapping.toDomain(hr_leave_reportdto);
		hr_leave_reportService.create(domain);
        Hr_leave_reportDTO dto = hr_leave_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Create-all')")
    @ApiOperation(value = "批量新建请假摘要/报告", tags = {"请假摘要/报告" },  notes = "批量新建请假摘要/报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_leave_reportDTO> hr_leave_reportdtos) {
        hr_leave_reportService.createBatch(hr_leave_reportMapping.toDomain(hr_leave_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Update-all')")
    @ApiOperation(value = "更新请假摘要/报告", tags = {"请假摘要/报告" },  notes = "更新请假摘要/报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_reports/{hr_leave_report_id}")
    public ResponseEntity<Hr_leave_reportDTO> update(@PathVariable("hr_leave_report_id") Long hr_leave_report_id, @RequestBody Hr_leave_reportDTO hr_leave_reportdto) {
		Hr_leave_report domain  = hr_leave_reportMapping.toDomain(hr_leave_reportdto);
        domain .setId(hr_leave_report_id);
		hr_leave_reportService.update(domain );
		Hr_leave_reportDTO dto = hr_leave_reportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Update-all')")
    @ApiOperation(value = "批量更新请假摘要/报告", tags = {"请假摘要/报告" },  notes = "批量更新请假摘要/报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_reportDTO> hr_leave_reportdtos) {
        hr_leave_reportService.updateBatch(hr_leave_reportMapping.toDomain(hr_leave_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Remove-all')")
    @ApiOperation(value = "删除请假摘要/报告", tags = {"请假摘要/报告" },  notes = "删除请假摘要/报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_reports/{hr_leave_report_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_report_id") Long hr_leave_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_leave_reportService.remove(hr_leave_report_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Remove-all')")
    @ApiOperation(value = "批量删除请假摘要/报告", tags = {"请假摘要/报告" },  notes = "批量删除请假摘要/报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_leave_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Get-all')")
    @ApiOperation(value = "获取请假摘要/报告", tags = {"请假摘要/报告" },  notes = "获取请假摘要/报告")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_reports/{hr_leave_report_id}")
    public ResponseEntity<Hr_leave_reportDTO> get(@PathVariable("hr_leave_report_id") Long hr_leave_report_id) {
        Hr_leave_report domain = hr_leave_reportService.get(hr_leave_report_id);
        Hr_leave_reportDTO dto = hr_leave_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取请假摘要/报告草稿", tags = {"请假摘要/报告" },  notes = "获取请假摘要/报告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_reports/getdraft")
    public ResponseEntity<Hr_leave_reportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_leave_reportMapping.toDto(hr_leave_reportService.getDraft(new Hr_leave_report())));
    }

    @ApiOperation(value = "检查请假摘要/报告", tags = {"请假摘要/报告" },  notes = "检查请假摘要/报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_leave_reportDTO hr_leave_reportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_leave_reportService.checkKey(hr_leave_reportMapping.toDomain(hr_leave_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Save-all')")
    @ApiOperation(value = "保存请假摘要/报告", tags = {"请假摘要/报告" },  notes = "保存请假摘要/报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_leave_reportDTO hr_leave_reportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_leave_reportService.save(hr_leave_reportMapping.toDomain(hr_leave_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-Save-all')")
    @ApiOperation(value = "批量保存请假摘要/报告", tags = {"请假摘要/报告" },  notes = "批量保存请假摘要/报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_leave_reportDTO> hr_leave_reportdtos) {
        hr_leave_reportService.saveBatch(hr_leave_reportMapping.toDomain(hr_leave_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"请假摘要/报告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_reports/fetchdefault")
	public ResponseEntity<List<Hr_leave_reportDTO>> fetchDefault(Hr_leave_reportSearchContext context) {
        Page<Hr_leave_report> domains = hr_leave_reportService.searchDefault(context) ;
        List<Hr_leave_reportDTO> list = hr_leave_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_report-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"请假摘要/报告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_leave_reports/searchdefault")
	public ResponseEntity<Page<Hr_leave_reportDTO>> searchDefault(@RequestBody Hr_leave_reportSearchContext context) {
        Page<Hr_leave_report> domains = hr_leave_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leave_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

