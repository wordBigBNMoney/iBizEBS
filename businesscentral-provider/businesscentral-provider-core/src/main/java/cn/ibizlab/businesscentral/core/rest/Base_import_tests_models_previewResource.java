package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_preview;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_previewService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_previewSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型预览" })
@RestController("Core-base_import_tests_models_preview")
@RequestMapping("")
public class Base_import_tests_models_previewResource {

    @Autowired
    public IBase_import_tests_models_previewService base_import_tests_models_previewService;

    @Autowired
    @Lazy
    public Base_import_tests_models_previewMapping base_import_tests_models_previewMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_previewMapping.toDomain(#base_import_tests_models_previewdto),'iBizBusinessCentral-Base_import_tests_models_preview-Create')")
    @ApiOperation(value = "新建测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "新建测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_previews")
    public ResponseEntity<Base_import_tests_models_previewDTO> create(@Validated @RequestBody Base_import_tests_models_previewDTO base_import_tests_models_previewdto) {
        Base_import_tests_models_preview domain = base_import_tests_models_previewMapping.toDomain(base_import_tests_models_previewdto);
		base_import_tests_models_previewService.create(domain);
        Base_import_tests_models_previewDTO dto = base_import_tests_models_previewMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_previewMapping.toDomain(#base_import_tests_models_previewdtos),'iBizBusinessCentral-Base_import_tests_models_preview-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "批量新建测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_previews/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_previewDTO> base_import_tests_models_previewdtos) {
        base_import_tests_models_previewService.createBatch(base_import_tests_models_previewMapping.toDomain(base_import_tests_models_previewdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_preview" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_previewService.get(#base_import_tests_models_preview_id),'iBizBusinessCentral-Base_import_tests_models_preview-Update')")
    @ApiOperation(value = "更新测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "更新测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_previews/{base_import_tests_models_preview_id}")
    public ResponseEntity<Base_import_tests_models_previewDTO> update(@PathVariable("base_import_tests_models_preview_id") Long base_import_tests_models_preview_id, @RequestBody Base_import_tests_models_previewDTO base_import_tests_models_previewdto) {
		Base_import_tests_models_preview domain  = base_import_tests_models_previewMapping.toDomain(base_import_tests_models_previewdto);
        domain .setId(base_import_tests_models_preview_id);
		base_import_tests_models_previewService.update(domain );
		Base_import_tests_models_previewDTO dto = base_import_tests_models_previewMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_previewService.getBaseImportTestsModelsPreviewByEntities(this.base_import_tests_models_previewMapping.toDomain(#base_import_tests_models_previewdtos)),'iBizBusinessCentral-Base_import_tests_models_preview-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "批量更新测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_previews/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_previewDTO> base_import_tests_models_previewdtos) {
        base_import_tests_models_previewService.updateBatch(base_import_tests_models_previewMapping.toDomain(base_import_tests_models_previewdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_previewService.get(#base_import_tests_models_preview_id),'iBizBusinessCentral-Base_import_tests_models_preview-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "删除测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_previews/{base_import_tests_models_preview_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_preview_id") Long base_import_tests_models_preview_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_previewService.remove(base_import_tests_models_preview_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_previewService.getBaseImportTestsModelsPreviewByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_preview-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "批量删除测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_previews/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_previewService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_previewMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_preview-Get')")
    @ApiOperation(value = "获取测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "获取测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_previews/{base_import_tests_models_preview_id}")
    public ResponseEntity<Base_import_tests_models_previewDTO> get(@PathVariable("base_import_tests_models_preview_id") Long base_import_tests_models_preview_id) {
        Base_import_tests_models_preview domain = base_import_tests_models_previewService.get(base_import_tests_models_preview_id);
        Base_import_tests_models_previewDTO dto = base_import_tests_models_previewMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型预览草稿", tags = {"测试:基本导入模型预览" },  notes = "获取测试:基本导入模型预览草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_previews/getdraft")
    public ResponseEntity<Base_import_tests_models_previewDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_previewMapping.toDto(base_import_tests_models_previewService.getDraft(new Base_import_tests_models_preview())));
    }

    @ApiOperation(value = "检查测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "检查测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_previews/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_previewDTO base_import_tests_models_previewdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_previewService.checkKey(base_import_tests_models_previewMapping.toDomain(base_import_tests_models_previewdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_previewMapping.toDomain(#base_import_tests_models_previewdto),'iBizBusinessCentral-Base_import_tests_models_preview-Save')")
    @ApiOperation(value = "保存测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "保存测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_previews/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_previewDTO base_import_tests_models_previewdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_previewService.save(base_import_tests_models_previewMapping.toDomain(base_import_tests_models_previewdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_previewMapping.toDomain(#base_import_tests_models_previewdtos),'iBizBusinessCentral-Base_import_tests_models_preview-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型预览", tags = {"测试:基本导入模型预览" },  notes = "批量保存测试:基本导入模型预览")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_previews/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_previewDTO> base_import_tests_models_previewdtos) {
        base_import_tests_models_previewService.saveBatch(base_import_tests_models_previewMapping.toDomain(base_import_tests_models_previewdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_preview-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_preview-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型预览" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_previews/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_previewDTO>> fetchDefault(Base_import_tests_models_previewSearchContext context) {
        Page<Base_import_tests_models_preview> domains = base_import_tests_models_previewService.searchDefault(context) ;
        List<Base_import_tests_models_previewDTO> list = base_import_tests_models_previewMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_preview-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_preview-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型预览" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_previews/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_previewDTO>> searchDefault(@RequestBody Base_import_tests_models_previewSearchContext context) {
        Page<Base_import_tests_models_preview> domains = base_import_tests_models_previewService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_previewMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

