package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_moveDTO]
 */
@Data
public class Account_moveDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [NARRATION]
     *
     */
    @JSONField(name = "narration")
    @JsonProperty("narration")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String narration;

    /**
     * 属性 [MATCHED_PERCENTAGE]
     *
     */
    @JSONField(name = "matched_percentage")
    @JsonProperty("matched_percentage")
    private Double matchedPercentage;

    /**
     * 属性 [TAX_TYPE_DOMAIN]
     *
     */
    @JSONField(name = "tax_type_domain")
    @JsonProperty("tax_type_domain")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String taxTypeDomain;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[号码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @NotBlank(message = "[状态]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [REF]
     *
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String ref;

    /**
     * 属性 [DUMMY_ACCOUNT_ID]
     *
     */
    @JSONField(name = "dummy_account_id")
    @JsonProperty("dummy_account_id")
    private Integer dummyAccountId;

    /**
     * 属性 [AUTO_REVERSE]
     *
     */
    @JSONField(name = "auto_reverse")
    @JsonProperty("auto_reverse")
    private Boolean autoReverse;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    @NotNull(message = "[日期]不允许为空!")
    private Timestamp date;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private BigDecimal amount;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [REVERSE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "reverse_date" , format="yyyy-MM-dd")
    @JsonProperty("reverse_date")
    private Timestamp reverseDate;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String lineIds;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [REVERSE_ENTRY_ID_TEXT]
     *
     */
    @JSONField(name = "reverse_entry_id_text")
    @JsonProperty("reverse_entry_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String reverseEntryIdText;

    /**
     * 属性 [STOCK_MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "stock_move_id_text")
    @JsonProperty("stock_move_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stockMoveIdText;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String journalIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [STOCK_MOVE_ID]
     *
     */
    @JSONField(name = "stock_move_id")
    @JsonProperty("stock_move_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stockMoveId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[日记账]不允许为空!")
    private Long journalId;

    /**
     * 属性 [TAX_CASH_BASIS_REC_ID]
     *
     */
    @JSONField(name = "tax_cash_basis_rec_id")
    @JsonProperty("tax_cash_basis_rec_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long taxCashBasisRecId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [REVERSE_ENTRY_ID]
     *
     */
    @JSONField(name = "reverse_entry_id")
    @JsonProperty("reverse_entry_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long reverseEntryId;


    /**
     * 设置 [NARRATION]
     */
    public void setNarration(String  narration){
        this.narration = narration ;
        this.modify("narration",narration);
    }

    /**
     * 设置 [MATCHED_PERCENTAGE]
     */
    public void setMatchedPercentage(Double  matchedPercentage){
        this.matchedPercentage = matchedPercentage ;
        this.modify("matched_percentage",matchedPercentage);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [REF]
     */
    public void setRef(String  ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }

    /**
     * 设置 [AUTO_REVERSE]
     */
    public void setAutoReverse(Boolean  autoReverse){
        this.autoReverse = autoReverse ;
        this.modify("auto_reverse",autoReverse);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(BigDecimal  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [REVERSE_DATE]
     */
    public void setReverseDate(Timestamp  reverseDate){
        this.reverseDate = reverseDate ;
        this.modify("reverse_date",reverseDate);
    }

    /**
     * 设置 [STOCK_MOVE_ID]
     */
    public void setStockMoveId(Long  stockMoveId){
        this.stockMoveId = stockMoveId ;
        this.modify("stock_move_id",stockMoveId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Long  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [TAX_CASH_BASIS_REC_ID]
     */
    public void setTaxCashBasisRecId(Long  taxCashBasisRecId){
        this.taxCashBasisRecId = taxCashBasisRecId ;
        this.modify("tax_cash_basis_rec_id",taxCashBasisRecId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [REVERSE_ENTRY_ID]
     */
    public void setReverseEntryId(Long  reverseEntryId){
        this.reverseEntryId = reverseEntryId ;
        this.modify("reverse_entry_id",reverseEntryId);
    }


}


