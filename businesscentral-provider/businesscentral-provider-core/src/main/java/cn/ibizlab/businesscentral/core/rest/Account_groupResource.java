package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_group;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_groupService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_groupSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"科目组" })
@RestController("Core-account_group")
@RequestMapping("")
public class Account_groupResource {

    @Autowired
    public IAccount_groupService account_groupService;

    @Autowired
    @Lazy
    public Account_groupMapping account_groupMapping;

    @PreAuthorize("hasPermission(this.account_groupMapping.toDomain(#account_groupdto),'iBizBusinessCentral-Account_group-Create')")
    @ApiOperation(value = "新建科目组", tags = {"科目组" },  notes = "新建科目组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_groups")
    public ResponseEntity<Account_groupDTO> create(@Validated @RequestBody Account_groupDTO account_groupdto) {
        Account_group domain = account_groupMapping.toDomain(account_groupdto);
		account_groupService.create(domain);
        Account_groupDTO dto = account_groupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_groupMapping.toDomain(#account_groupdtos),'iBizBusinessCentral-Account_group-Create')")
    @ApiOperation(value = "批量新建科目组", tags = {"科目组" },  notes = "批量新建科目组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_groups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_groupDTO> account_groupdtos) {
        account_groupService.createBatch(account_groupMapping.toDomain(account_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_group" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_groupService.get(#account_group_id),'iBizBusinessCentral-Account_group-Update')")
    @ApiOperation(value = "更新科目组", tags = {"科目组" },  notes = "更新科目组")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_groups/{account_group_id}")
    public ResponseEntity<Account_groupDTO> update(@PathVariable("account_group_id") Long account_group_id, @RequestBody Account_groupDTO account_groupdto) {
		Account_group domain  = account_groupMapping.toDomain(account_groupdto);
        domain .setId(account_group_id);
		account_groupService.update(domain );
		Account_groupDTO dto = account_groupMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_groupService.getAccountGroupByEntities(this.account_groupMapping.toDomain(#account_groupdtos)),'iBizBusinessCentral-Account_group-Update')")
    @ApiOperation(value = "批量更新科目组", tags = {"科目组" },  notes = "批量更新科目组")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_groups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_groupDTO> account_groupdtos) {
        account_groupService.updateBatch(account_groupMapping.toDomain(account_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_groupService.get(#account_group_id),'iBizBusinessCentral-Account_group-Remove')")
    @ApiOperation(value = "删除科目组", tags = {"科目组" },  notes = "删除科目组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_groups/{account_group_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_group_id") Long account_group_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_groupService.remove(account_group_id));
    }

    @PreAuthorize("hasPermission(this.account_groupService.getAccountGroupByIds(#ids),'iBizBusinessCentral-Account_group-Remove')")
    @ApiOperation(value = "批量删除科目组", tags = {"科目组" },  notes = "批量删除科目组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_groups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_groupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_groupMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_group-Get')")
    @ApiOperation(value = "获取科目组", tags = {"科目组" },  notes = "获取科目组")
	@RequestMapping(method = RequestMethod.GET, value = "/account_groups/{account_group_id}")
    public ResponseEntity<Account_groupDTO> get(@PathVariable("account_group_id") Long account_group_id) {
        Account_group domain = account_groupService.get(account_group_id);
        Account_groupDTO dto = account_groupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取科目组草稿", tags = {"科目组" },  notes = "获取科目组草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_groups/getdraft")
    public ResponseEntity<Account_groupDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_groupMapping.toDto(account_groupService.getDraft(new Account_group())));
    }

    @ApiOperation(value = "检查科目组", tags = {"科目组" },  notes = "检查科目组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_groups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_groupDTO account_groupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_groupService.checkKey(account_groupMapping.toDomain(account_groupdto)));
    }

    @PreAuthorize("hasPermission(this.account_groupMapping.toDomain(#account_groupdto),'iBizBusinessCentral-Account_group-Save')")
    @ApiOperation(value = "保存科目组", tags = {"科目组" },  notes = "保存科目组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_groups/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_groupDTO account_groupdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_groupService.save(account_groupMapping.toDomain(account_groupdto)));
    }

    @PreAuthorize("hasPermission(this.account_groupMapping.toDomain(#account_groupdtos),'iBizBusinessCentral-Account_group-Save')")
    @ApiOperation(value = "批量保存科目组", tags = {"科目组" },  notes = "批量保存科目组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_groups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_groupDTO> account_groupdtos) {
        account_groupService.saveBatch(account_groupMapping.toDomain(account_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_group-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_group-Get')")
	@ApiOperation(value = "获取数据集", tags = {"科目组" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_groups/fetchdefault")
	public ResponseEntity<List<Account_groupDTO>> fetchDefault(Account_groupSearchContext context) {
        Page<Account_group> domains = account_groupService.searchDefault(context) ;
        List<Account_groupDTO> list = account_groupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_group-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_group-Get')")
	@ApiOperation(value = "查询数据集", tags = {"科目组" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_groups/searchdefault")
	public ResponseEntity<Page<Account_groupDTO>> searchDefault(@RequestBody Account_groupSearchContext context) {
        Page<Account_group> domains = account_groupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_groupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

