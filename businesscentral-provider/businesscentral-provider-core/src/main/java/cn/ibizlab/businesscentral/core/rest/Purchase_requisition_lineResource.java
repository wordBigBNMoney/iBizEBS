package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisition_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"采购申请行" })
@RestController("Core-purchase_requisition_line")
@RequestMapping("")
public class Purchase_requisition_lineResource {

    @Autowired
    public IPurchase_requisition_lineService purchase_requisition_lineService;

    @Autowired
    @Lazy
    public Purchase_requisition_lineMapping purchase_requisition_lineMapping;

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "新建采购申请行", tags = {"采购申请行" },  notes = "新建采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_lines")
    public ResponseEntity<Purchase_requisition_lineDTO> create(@Validated @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
		purchase_requisition_lineService.create(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "批量新建采购申请行", tags = {"采购申请行" },  notes = "批量新建采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        purchase_requisition_lineService.createBatch(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "更新采购申请行", tags = {"采购申请行" },  notes = "更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> update(@PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
		Purchase_requisition_line domain  = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain .setId(purchase_requisition_line_id);
		purchase_requisition_lineService.update(domain );
		Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByEntities(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos)),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "批量更新采购申请行", tags = {"采购申请行" },  notes = "批量更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        purchase_requisition_lineService.updateBatch(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "删除采购申请行", tags = {"采购申请行" },  notes = "删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.remove(purchase_requisition_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByIds(#ids),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "批量删除采购申请行", tags = {"采购申请行" },  notes = "批量删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        purchase_requisition_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_requisition_line-Get')")
    @ApiOperation(value = "获取采购申请行", tags = {"采购申请行" },  notes = "获取采购申请行")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> get(@PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
        Purchase_requisition_line domain = purchase_requisition_lineService.get(purchase_requisition_line_id);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取采购申请行草稿", tags = {"采购申请行" },  notes = "获取采购申请行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisition_lines/getdraft")
    public ResponseEntity<Purchase_requisition_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineMapping.toDto(purchase_requisition_lineService.getDraft(new Purchase_requisition_line())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Calc_price-all')")
    @ApiOperation(value = "calc_price", tags = {"采购申请行" },  notes = "calc_price")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_lines/{purchase_requisition_line_id}/calc_price")
    public ResponseEntity<Purchase_requisition_lineDTO> calc_price(@PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setId(purchase_requisition_line_id);
        domain = purchase_requisition_lineService.calc_price(domain);
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @ApiOperation(value = "检查采购申请行", tags = {"采购申请行" },  notes = "检查采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.checkKey(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Product_change-all')")
    @ApiOperation(value = "product_change", tags = {"采购申请行" },  notes = "product_change")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_lines/{purchase_requisition_line_id}/product_change")
    public ResponseEntity<Purchase_requisition_lineDTO> product_change(@PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setId(purchase_requisition_line_id);
        domain = purchase_requisition_lineService.product_change(domain);
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "保存采购申请行", tags = {"采购申请行" },  notes = "保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.save(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto)));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "批量保存采购申请行", tags = {"采购申请行" },  notes = "批量保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        purchase_requisition_lineService.saveBatch(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"采购申请行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisition_lines/fetchdefault")
	public ResponseEntity<List<Purchase_requisition_lineDTO>> fetchDefault(Purchase_requisition_lineSearchContext context) {
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
        List<Purchase_requisition_lineDTO> list = purchase_requisition_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"采购申请行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisition_lines/searchdefault")
	public ResponseEntity<Page<Purchase_requisition_lineDTO>> searchDefault(@RequestBody Purchase_requisition_lineSearchContext context) {
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisition_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "根据产品建立采购申请行", tags = {"采购申请行" },  notes = "根据产品建立采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_requisition_lines")
    public ResponseEntity<Purchase_requisition_lineDTO> createByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
		purchase_requisition_lineService.create(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "根据产品批量建立采购申请行", tags = {"采购申请行" },  notes = "根据产品批量建立采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> createBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
            domain.setProductId(product_product_id);
        }
        purchase_requisition_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "根据产品更新采购申请行", tags = {"采购申请行" },  notes = "根据产品更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> updateByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
        domain.setId(purchase_requisition_line_id);
		purchase_requisition_lineService.update(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByEntities(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos)),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "根据产品批量更新采购申请行", tags = {"采购申请行" },  notes = "根据产品批量更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
            domain.setProductId(product_product_id);
        }
        purchase_requisition_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "根据产品删除采购申请行", tags = {"采购申请行" },  notes = "根据产品删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Boolean> removeByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.remove(purchase_requisition_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByIds(#ids),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "根据产品批量删除采购申请行", tags = {"采购申请行" },  notes = "根据产品批量删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_product(@RequestBody List<Long> ids) {
        purchase_requisition_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_requisition_line-Get')")
    @ApiOperation(value = "根据产品获取采购申请行", tags = {"采购申请行" },  notes = "根据产品获取采购申请行")
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> getByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
        Purchase_requisition_line domain = purchase_requisition_lineService.get(purchase_requisition_line_id);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品获取采购申请行草稿", tags = {"采购申请行" },  notes = "根据产品获取采购申请行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}/purchase_requisition_lines/getdraft")
    public ResponseEntity<Purchase_requisition_lineDTO> getDraftByProduct_product(@PathVariable("product_product_id") Long product_product_id) {
        Purchase_requisition_line domain = new Purchase_requisition_line();
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineMapping.toDto(purchase_requisition_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Calc_price-all')")
    @ApiOperation(value = "根据产品采购申请行", tags = {"采购申请行" },  notes = "根据产品采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}/calc_price")
    public ResponseEntity<Purchase_requisition_lineDTO> calc_priceByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_requisition_lineService.calc_price(domain) ;
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @ApiOperation(value = "根据产品检查采购申请行", tags = {"采购申请行" },  notes = "根据产品检查采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_requisition_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.checkKey(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Product_change-all')")
    @ApiOperation(value = "根据产品采购申请行", tags = {"采购申请行" },  notes = "根据产品采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}/product_change")
    public ResponseEntity<Purchase_requisition_lineDTO> product_changeByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_requisition_lineService.product_change(domain) ;
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "根据产品保存采购申请行", tags = {"采购申请行" },  notes = "根据产品保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_requisition_lines/save")
    public ResponseEntity<Boolean> saveByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "根据产品批量保存采购申请行", tags = {"采购申请行" },  notes = "根据产品批量保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_requisition_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
             domain.setProductId(product_product_id);
        }
        purchase_requisition_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "根据产品获取数据集", tags = {"采购申请行" } ,notes = "根据产品获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_products/{product_product_id}/purchase_requisition_lines/fetchdefault")
	public ResponseEntity<List<Purchase_requisition_lineDTO>> fetchPurchase_requisition_lineDefaultByProduct_product(@PathVariable("product_product_id") Long product_product_id,Purchase_requisition_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
        List<Purchase_requisition_lineDTO> list = purchase_requisition_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "根据产品查询数据集", tags = {"采购申请行" } ,notes = "根据产品查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_products/{product_product_id}/purchase_requisition_lines/searchdefault")
	public ResponseEntity<Page<Purchase_requisition_lineDTO>> searchPurchase_requisition_lineDefaultByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_requisition_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisition_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "根据采购申请建立采购申请行", tags = {"采购申请行" },  notes = "根据采购申请建立采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines")
    public ResponseEntity<Purchase_requisition_lineDTO> createByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
		purchase_requisition_lineService.create(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "根据采购申请批量建立采购申请行", tags = {"采购申请行" },  notes = "根据采购申请批量建立采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> createBatchByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
            domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_requisition_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "根据采购申请更新采购申请行", tags = {"采购申请行" },  notes = "根据采购申请更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> updateByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
        domain.setId(purchase_requisition_line_id);
		purchase_requisition_lineService.update(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByEntities(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos)),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "根据采购申请批量更新采购申请行", tags = {"采购申请行" },  notes = "根据采购申请批量更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> updateBatchByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
            domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_requisition_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "根据采购申请删除采购申请行", tags = {"采购申请行" },  notes = "根据采购申请删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Boolean> removeByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.remove(purchase_requisition_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByIds(#ids),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "根据采购申请批量删除采购申请行", tags = {"采购申请行" },  notes = "根据采购申请批量删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> removeBatchByPurchase_requisition(@RequestBody List<Long> ids) {
        purchase_requisition_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_requisition_line-Get')")
    @ApiOperation(value = "根据采购申请获取采购申请行", tags = {"采购申请行" },  notes = "根据采购申请获取采购申请行")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> getByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
        Purchase_requisition_line domain = purchase_requisition_lineService.get(purchase_requisition_line_id);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据采购申请获取采购申请行草稿", tags = {"采购申请行" },  notes = "根据采购申请获取采购申请行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/getdraft")
    public ResponseEntity<Purchase_requisition_lineDTO> getDraftByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id) {
        Purchase_requisition_line domain = new Purchase_requisition_line();
        domain.setRequisitionId(purchase_requisition_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineMapping.toDto(purchase_requisition_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Calc_price-all')")
    @ApiOperation(value = "根据采购申请采购申请行", tags = {"采购申请行" },  notes = "根据采购申请采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}/calc_price")
    public ResponseEntity<Purchase_requisition_lineDTO> calc_priceByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_requisition_lineService.calc_price(domain) ;
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @ApiOperation(value = "根据采购申请检查采购申请行", tags = {"采购申请行" },  notes = "根据采购申请检查采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.checkKey(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Product_change-all')")
    @ApiOperation(value = "根据采购申请采购申请行", tags = {"采购申请行" },  notes = "根据采购申请采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}/product_change")
    public ResponseEntity<Purchase_requisition_lineDTO> product_changeByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_requisition_lineService.product_change(domain) ;
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "根据采购申请保存采购申请行", tags = {"采购申请行" },  notes = "根据采购申请保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/save")
    public ResponseEntity<Boolean> saveByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "根据采购申请批量保存采购申请行", tags = {"采购申请行" },  notes = "根据采购申请批量保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
             domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_requisition_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "根据采购申请获取数据集", tags = {"采购申请行" } ,notes = "根据采购申请获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/fetchdefault")
	public ResponseEntity<List<Purchase_requisition_lineDTO>> fetchPurchase_requisition_lineDefaultByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id,Purchase_requisition_lineSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
        List<Purchase_requisition_lineDTO> list = purchase_requisition_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "根据采购申请查询数据集", tags = {"采购申请行" } ,notes = "根据采购申请查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/searchdefault")
	public ResponseEntity<Page<Purchase_requisition_lineDTO>> searchPurchase_requisition_lineDefaultByPurchase_requisition(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisition_lineSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisition_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "根据产品模板产品建立采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品建立采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines")
    public ResponseEntity<Purchase_requisition_lineDTO> createByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
		purchase_requisition_lineService.create(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "根据产品模板产品批量建立采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品批量建立采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> createBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
            domain.setProductId(product_product_id);
        }
        purchase_requisition_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "根据产品模板产品更新采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> updateByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
        domain.setId(purchase_requisition_line_id);
		purchase_requisition_lineService.update(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByEntities(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos)),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "根据产品模板产品批量更新采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品批量更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
            domain.setProductId(product_product_id);
        }
        purchase_requisition_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "根据产品模板产品删除采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Boolean> removeByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.remove(purchase_requisition_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByIds(#ids),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "根据产品模板产品批量删除采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品批量删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_templateProduct_product(@RequestBody List<Long> ids) {
        purchase_requisition_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_requisition_line-Get')")
    @ApiOperation(value = "根据产品模板产品获取采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品获取采购申请行")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> getByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
        Purchase_requisition_line domain = purchase_requisition_lineService.get(purchase_requisition_line_id);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品模板产品获取采购申请行草稿", tags = {"采购申请行" },  notes = "根据产品模板产品获取采购申请行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/getdraft")
    public ResponseEntity<Purchase_requisition_lineDTO> getDraftByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id) {
        Purchase_requisition_line domain = new Purchase_requisition_line();
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineMapping.toDto(purchase_requisition_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Calc_price-all')")
    @ApiOperation(value = "根据产品模板产品采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}/calc_price")
    public ResponseEntity<Purchase_requisition_lineDTO> calc_priceByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_requisition_lineService.calc_price(domain) ;
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @ApiOperation(value = "根据产品模板产品检查采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品检查采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.checkKey(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Product_change-all')")
    @ApiOperation(value = "根据产品模板产品采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/{purchase_requisition_line_id}/product_change")
    public ResponseEntity<Purchase_requisition_lineDTO> product_changeByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_requisition_lineService.product_change(domain) ;
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "根据产品模板产品保存采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/save")
    public ResponseEntity<Boolean> saveByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "根据产品模板产品批量保存采购申请行", tags = {"采购申请行" },  notes = "根据产品模板产品批量保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
             domain.setProductId(product_product_id);
        }
        purchase_requisition_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "根据产品模板产品获取数据集", tags = {"采购申请行" } ,notes = "根据产品模板产品获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/fetchdefault")
	public ResponseEntity<List<Purchase_requisition_lineDTO>> fetchPurchase_requisition_lineDefaultByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id,Purchase_requisition_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
        List<Purchase_requisition_lineDTO> list = purchase_requisition_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "根据产品模板产品查询数据集", tags = {"采购申请行" } ,notes = "根据产品模板产品查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_requisition_lines/searchdefault")
	public ResponseEntity<Page<Purchase_requisition_lineDTO>> searchPurchase_requisition_lineDefaultByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_requisition_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisition_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "根据供应商采购申请建立采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请建立采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines")
    public ResponseEntity<Purchase_requisition_lineDTO> createByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
		purchase_requisition_lineService.create(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Create')")
    @ApiOperation(value = "根据供应商采购申请批量建立采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请批量建立采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
            domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_requisition_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "根据供应商采购申请更新采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> updateByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
        domain.setId(purchase_requisition_line_id);
		purchase_requisition_lineService.update(domain);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByEntities(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos)),'iBizBusinessCentral-Purchase_requisition_line-Update')")
    @ApiOperation(value = "根据供应商采购申请批量更新采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请批量更新采购申请行")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
            domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_requisition_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.get(#purchase_requisition_line_id),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "根据供应商采购申请删除采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Boolean> removeByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.remove(purchase_requisition_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineService.getPurchaseRequisitionLineByIds(#ids),'iBizBusinessCentral-Purchase_requisition_line-Remove')")
    @ApiOperation(value = "根据供应商采购申请批量删除采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请批量删除采购申请行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplierPurchase_requisition(@RequestBody List<Long> ids) {
        purchase_requisition_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_requisition_line-Get')")
    @ApiOperation(value = "根据供应商采购申请获取采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请获取采购申请行")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}")
    public ResponseEntity<Purchase_requisition_lineDTO> getByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id) {
        Purchase_requisition_line domain = purchase_requisition_lineService.get(purchase_requisition_line_id);
        Purchase_requisition_lineDTO dto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商采购申请获取采购申请行草稿", tags = {"采购申请行" },  notes = "根据供应商采购申请获取采购申请行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/getdraft")
    public ResponseEntity<Purchase_requisition_lineDTO> getDraftByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id) {
        Purchase_requisition_line domain = new Purchase_requisition_line();
        domain.setRequisitionId(purchase_requisition_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineMapping.toDto(purchase_requisition_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Calc_price-all')")
    @ApiOperation(value = "根据供应商采购申请采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}/calc_price")
    public ResponseEntity<Purchase_requisition_lineDTO> calc_priceByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_requisition_lineService.calc_price(domain) ;
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @ApiOperation(value = "根据供应商采购申请检查采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请检查采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.checkKey(purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-Product_change-all')")
    @ApiOperation(value = "根据供应商采购申请采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/{purchase_requisition_line_id}/product_change")
    public ResponseEntity<Purchase_requisition_lineDTO> product_changeByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_requisition_line_id") Long purchase_requisition_line_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
        domain = purchase_requisition_lineService.product_change(domain) ;
        purchase_requisition_linedto = purchase_requisition_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedto),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "根据供应商采购申请保存采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/save")
    public ResponseEntity<Boolean> saveByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisition_lineDTO purchase_requisition_linedto) {
        Purchase_requisition_line domain = purchase_requisition_lineMapping.toDomain(purchase_requisition_linedto);
        domain.setRequisitionId(purchase_requisition_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_requisition_lineMapping.toDomain(#purchase_requisition_linedtos),'iBizBusinessCentral-Purchase_requisition_line-Save')")
    @ApiOperation(value = "根据供应商采购申请批量保存采购申请行", tags = {"采购申请行" },  notes = "根据供应商采购申请批量保存采购申请行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody List<Purchase_requisition_lineDTO> purchase_requisition_linedtos) {
        List<Purchase_requisition_line> domainlist=purchase_requisition_lineMapping.toDomain(purchase_requisition_linedtos);
        for(Purchase_requisition_line domain:domainlist){
             domain.setRequisitionId(purchase_requisition_id);
        }
        purchase_requisition_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "根据供应商采购申请获取数据集", tags = {"采购申请行" } ,notes = "根据供应商采购申请获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/fetchdefault")
	public ResponseEntity<List<Purchase_requisition_lineDTO>> fetchPurchase_requisition_lineDefaultByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id,Purchase_requisition_lineSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
        List<Purchase_requisition_lineDTO> list = purchase_requisition_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition_line-Get')")
	@ApiOperation(value = "根据供应商采购申请查询数据集", tags = {"采购申请行" } ,notes = "根据供应商采购申请查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_requisition_lines/searchdefault")
	public ResponseEntity<Page<Purchase_requisition_lineDTO>> searchPurchase_requisition_lineDefaultByRes_supplierPurchase_requisition(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisition_lineSearchContext context) {
        context.setN_requisition_id_eq(purchase_requisition_id);
        Page<Purchase_requisition_line> domains = purchase_requisition_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisition_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

