package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_incotermsService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_incotermsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"贸易条款" })
@RestController("Core-account_incoterms")
@RequestMapping("")
public class Account_incotermsResource {

    @Autowired
    public IAccount_incotermsService account_incotermsService;

    @Autowired
    @Lazy
    public Account_incotermsMapping account_incotermsMapping;

    @PreAuthorize("hasPermission(this.account_incotermsMapping.toDomain(#account_incotermsdto),'iBizBusinessCentral-Account_incoterms-Create')")
    @ApiOperation(value = "新建贸易条款", tags = {"贸易条款" },  notes = "新建贸易条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_incoterms")
    public ResponseEntity<Account_incotermsDTO> create(@Validated @RequestBody Account_incotermsDTO account_incotermsdto) {
        Account_incoterms domain = account_incotermsMapping.toDomain(account_incotermsdto);
		account_incotermsService.create(domain);
        Account_incotermsDTO dto = account_incotermsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_incotermsMapping.toDomain(#account_incotermsdtos),'iBizBusinessCentral-Account_incoterms-Create')")
    @ApiOperation(value = "批量新建贸易条款", tags = {"贸易条款" },  notes = "批量新建贸易条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_incotermsDTO> account_incotermsdtos) {
        account_incotermsService.createBatch(account_incotermsMapping.toDomain(account_incotermsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_incoterms" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_incotermsService.get(#account_incoterms_id),'iBizBusinessCentral-Account_incoterms-Update')")
    @ApiOperation(value = "更新贸易条款", tags = {"贸易条款" },  notes = "更新贸易条款")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_incoterms/{account_incoterms_id}")
    public ResponseEntity<Account_incotermsDTO> update(@PathVariable("account_incoterms_id") Long account_incoterms_id, @RequestBody Account_incotermsDTO account_incotermsdto) {
		Account_incoterms domain  = account_incotermsMapping.toDomain(account_incotermsdto);
        domain .setId(account_incoterms_id);
		account_incotermsService.update(domain );
		Account_incotermsDTO dto = account_incotermsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_incotermsService.getAccountIncotermsByEntities(this.account_incotermsMapping.toDomain(#account_incotermsdtos)),'iBizBusinessCentral-Account_incoterms-Update')")
    @ApiOperation(value = "批量更新贸易条款", tags = {"贸易条款" },  notes = "批量更新贸易条款")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_incoterms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_incotermsDTO> account_incotermsdtos) {
        account_incotermsService.updateBatch(account_incotermsMapping.toDomain(account_incotermsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_incotermsService.get(#account_incoterms_id),'iBizBusinessCentral-Account_incoterms-Remove')")
    @ApiOperation(value = "删除贸易条款", tags = {"贸易条款" },  notes = "删除贸易条款")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_incoterms/{account_incoterms_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_incoterms_id") Long account_incoterms_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_incotermsService.remove(account_incoterms_id));
    }

    @PreAuthorize("hasPermission(this.account_incotermsService.getAccountIncotermsByIds(#ids),'iBizBusinessCentral-Account_incoterms-Remove')")
    @ApiOperation(value = "批量删除贸易条款", tags = {"贸易条款" },  notes = "批量删除贸易条款")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_incoterms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_incotermsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_incotermsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_incoterms-Get')")
    @ApiOperation(value = "获取贸易条款", tags = {"贸易条款" },  notes = "获取贸易条款")
	@RequestMapping(method = RequestMethod.GET, value = "/account_incoterms/{account_incoterms_id}")
    public ResponseEntity<Account_incotermsDTO> get(@PathVariable("account_incoterms_id") Long account_incoterms_id) {
        Account_incoterms domain = account_incotermsService.get(account_incoterms_id);
        Account_incotermsDTO dto = account_incotermsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取贸易条款草稿", tags = {"贸易条款" },  notes = "获取贸易条款草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_incoterms/getdraft")
    public ResponseEntity<Account_incotermsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_incotermsMapping.toDto(account_incotermsService.getDraft(new Account_incoterms())));
    }

    @ApiOperation(value = "检查贸易条款", tags = {"贸易条款" },  notes = "检查贸易条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_incotermsDTO account_incotermsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_incotermsService.checkKey(account_incotermsMapping.toDomain(account_incotermsdto)));
    }

    @PreAuthorize("hasPermission(this.account_incotermsMapping.toDomain(#account_incotermsdto),'iBizBusinessCentral-Account_incoterms-Save')")
    @ApiOperation(value = "保存贸易条款", tags = {"贸易条款" },  notes = "保存贸易条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_incotermsDTO account_incotermsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_incotermsService.save(account_incotermsMapping.toDomain(account_incotermsdto)));
    }

    @PreAuthorize("hasPermission(this.account_incotermsMapping.toDomain(#account_incotermsdtos),'iBizBusinessCentral-Account_incoterms-Save')")
    @ApiOperation(value = "批量保存贸易条款", tags = {"贸易条款" },  notes = "批量保存贸易条款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_incotermsDTO> account_incotermsdtos) {
        account_incotermsService.saveBatch(account_incotermsMapping.toDomain(account_incotermsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_incoterms-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_incoterms-Get')")
	@ApiOperation(value = "获取数据集", tags = {"贸易条款" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_incoterms/fetchdefault")
	public ResponseEntity<List<Account_incotermsDTO>> fetchDefault(Account_incotermsSearchContext context) {
        Page<Account_incoterms> domains = account_incotermsService.searchDefault(context) ;
        List<Account_incotermsDTO> list = account_incotermsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_incoterms-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_incoterms-Get')")
	@ApiOperation(value = "查询数据集", tags = {"贸易条款" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_incoterms/searchdefault")
	public ResponseEntity<Page<Account_incotermsDTO>> searchDefault(@RequestBody Account_incotermsSearchContext context) {
        Page<Account_incoterms> domains = account_incotermsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_incotermsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

