package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.businesscentral.core.dto.Stock_warn_insufficient_qtyDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreStock_warn_insufficient_qtyMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_warn_insufficient_qtyMapping extends MappingBase<Stock_warn_insufficient_qtyDTO, Stock_warn_insufficient_qty> {


}

