package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_template;
import cn.ibizlab.businesscentral.core.dto.Account_fiscal_position_templateDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_fiscal_position_templateMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_fiscal_position_templateMapping extends MappingBase<Account_fiscal_position_templateDTO, Account_fiscal_position_template> {


}

