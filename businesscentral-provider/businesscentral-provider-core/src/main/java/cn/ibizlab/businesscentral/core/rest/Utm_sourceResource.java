package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_sourceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"UTM来源" })
@RestController("Core-utm_source")
@RequestMapping("")
public class Utm_sourceResource {

    @Autowired
    public IUtm_sourceService utm_sourceService;

    @Autowired
    @Lazy
    public Utm_sourceMapping utm_sourceMapping;

    @PreAuthorize("hasPermission(this.utm_sourceMapping.toDomain(#utm_sourcedto),'iBizBusinessCentral-Utm_source-Create')")
    @ApiOperation(value = "新建UTM来源", tags = {"UTM来源" },  notes = "新建UTM来源")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_sources")
    public ResponseEntity<Utm_sourceDTO> create(@Validated @RequestBody Utm_sourceDTO utm_sourcedto) {
        Utm_source domain = utm_sourceMapping.toDomain(utm_sourcedto);
		utm_sourceService.create(domain);
        Utm_sourceDTO dto = utm_sourceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.utm_sourceMapping.toDomain(#utm_sourcedtos),'iBizBusinessCentral-Utm_source-Create')")
    @ApiOperation(value = "批量新建UTM来源", tags = {"UTM来源" },  notes = "批量新建UTM来源")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_sources/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Utm_sourceDTO> utm_sourcedtos) {
        utm_sourceService.createBatch(utm_sourceMapping.toDomain(utm_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "utm_source" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.utm_sourceService.get(#utm_source_id),'iBizBusinessCentral-Utm_source-Update')")
    @ApiOperation(value = "更新UTM来源", tags = {"UTM来源" },  notes = "更新UTM来源")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_sources/{utm_source_id}")
    public ResponseEntity<Utm_sourceDTO> update(@PathVariable("utm_source_id") Long utm_source_id, @RequestBody Utm_sourceDTO utm_sourcedto) {
		Utm_source domain  = utm_sourceMapping.toDomain(utm_sourcedto);
        domain .setId(utm_source_id);
		utm_sourceService.update(domain );
		Utm_sourceDTO dto = utm_sourceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.utm_sourceService.getUtmSourceByEntities(this.utm_sourceMapping.toDomain(#utm_sourcedtos)),'iBizBusinessCentral-Utm_source-Update')")
    @ApiOperation(value = "批量更新UTM来源", tags = {"UTM来源" },  notes = "批量更新UTM来源")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_sources/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_sourceDTO> utm_sourcedtos) {
        utm_sourceService.updateBatch(utm_sourceMapping.toDomain(utm_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.utm_sourceService.get(#utm_source_id),'iBizBusinessCentral-Utm_source-Remove')")
    @ApiOperation(value = "删除UTM来源", tags = {"UTM来源" },  notes = "删除UTM来源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_sources/{utm_source_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("utm_source_id") Long utm_source_id) {
         return ResponseEntity.status(HttpStatus.OK).body(utm_sourceService.remove(utm_source_id));
    }

    @PreAuthorize("hasPermission(this.utm_sourceService.getUtmSourceByIds(#ids),'iBizBusinessCentral-Utm_source-Remove')")
    @ApiOperation(value = "批量删除UTM来源", tags = {"UTM来源" },  notes = "批量删除UTM来源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_sources/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        utm_sourceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.utm_sourceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Utm_source-Get')")
    @ApiOperation(value = "获取UTM来源", tags = {"UTM来源" },  notes = "获取UTM来源")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_sources/{utm_source_id}")
    public ResponseEntity<Utm_sourceDTO> get(@PathVariable("utm_source_id") Long utm_source_id) {
        Utm_source domain = utm_sourceService.get(utm_source_id);
        Utm_sourceDTO dto = utm_sourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取UTM来源草稿", tags = {"UTM来源" },  notes = "获取UTM来源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_sources/getdraft")
    public ResponseEntity<Utm_sourceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(utm_sourceMapping.toDto(utm_sourceService.getDraft(new Utm_source())));
    }

    @ApiOperation(value = "检查UTM来源", tags = {"UTM来源" },  notes = "检查UTM来源")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_sources/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Utm_sourceDTO utm_sourcedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(utm_sourceService.checkKey(utm_sourceMapping.toDomain(utm_sourcedto)));
    }

    @PreAuthorize("hasPermission(this.utm_sourceMapping.toDomain(#utm_sourcedto),'iBizBusinessCentral-Utm_source-Save')")
    @ApiOperation(value = "保存UTM来源", tags = {"UTM来源" },  notes = "保存UTM来源")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_sources/save")
    public ResponseEntity<Boolean> save(@RequestBody Utm_sourceDTO utm_sourcedto) {
        return ResponseEntity.status(HttpStatus.OK).body(utm_sourceService.save(utm_sourceMapping.toDomain(utm_sourcedto)));
    }

    @PreAuthorize("hasPermission(this.utm_sourceMapping.toDomain(#utm_sourcedtos),'iBizBusinessCentral-Utm_source-Save')")
    @ApiOperation(value = "批量保存UTM来源", tags = {"UTM来源" },  notes = "批量保存UTM来源")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_sources/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Utm_sourceDTO> utm_sourcedtos) {
        utm_sourceService.saveBatch(utm_sourceMapping.toDomain(utm_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_source-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Utm_source-Get')")
	@ApiOperation(value = "获取数据集", tags = {"UTM来源" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/utm_sources/fetchdefault")
	public ResponseEntity<List<Utm_sourceDTO>> fetchDefault(Utm_sourceSearchContext context) {
        Page<Utm_source> domains = utm_sourceService.searchDefault(context) ;
        List<Utm_sourceDTO> list = utm_sourceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_source-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Utm_source-Get')")
	@ApiOperation(value = "查询数据集", tags = {"UTM来源" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/utm_sources/searchdefault")
	public ResponseEntity<Page<Utm_sourceDTO>> searchDefault(@RequestBody Utm_sourceSearchContext context) {
        Page<Utm_source> domains = utm_sourceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(utm_sourceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

