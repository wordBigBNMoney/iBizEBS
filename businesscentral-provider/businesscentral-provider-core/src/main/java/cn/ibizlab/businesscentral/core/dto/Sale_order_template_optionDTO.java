package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Sale_order_template_optionDTO]
 */
@Data
public class Sale_order_template_optionDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    @NotNull(message = "[数量]不允许为空!")
    private Double quantity;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[说明]不允许为空!")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String name;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    @NotNull(message = "[单价]不允许为空!")
    private Double priceUnit;

    /**
     * 属性 [DISCOUNT]
     *
     */
    @JSONField(name = "discount")
    @JsonProperty("discount")
    private Double discount;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "sale_order_template_id_text")
    @JsonProperty("sale_order_template_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleOrderTemplateIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [UOM_ID_TEXT]
     *
     */
    @JSONField(name = "uom_id_text")
    @JsonProperty("uom_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String uomIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[产品]不允许为空!")
    private Long productId;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_ID]
     *
     */
    @JSONField(name = "sale_order_template_id")
    @JsonProperty("sale_order_template_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[报价单模板参考]不允许为空!")
    private Long saleOrderTemplateId;

    /**
     * 属性 [UOM_ID]
     *
     */
    @JSONField(name = "uom_id")
    @JsonProperty("uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[计量单位]不允许为空!")
    private Long uomId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    public void setPriceUnit(Double  priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [DISCOUNT]
     */
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.modify("discount",discount);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [SALE_ORDER_TEMPLATE_ID]
     */
    public void setSaleOrderTemplateId(Long  saleOrderTemplateId){
        this.saleOrderTemplateId = saleOrderTemplateId ;
        this.modify("sale_order_template_id",saleOrderTemplateId);
    }

    /**
     * 设置 [UOM_ID]
     */
    public void setUomId(Long  uomId){
        this.uomId = uomId ;
        this.modify("uom_id",uomId);
    }


}


