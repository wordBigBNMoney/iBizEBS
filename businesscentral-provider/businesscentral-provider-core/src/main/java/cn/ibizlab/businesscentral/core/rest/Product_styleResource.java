package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_style;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_styleService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_styleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品风格" })
@RestController("Core-product_style")
@RequestMapping("")
public class Product_styleResource {

    @Autowired
    public IProduct_styleService product_styleService;

    @Autowired
    @Lazy
    public Product_styleMapping product_styleMapping;

    @PreAuthorize("hasPermission(this.product_styleMapping.toDomain(#product_styledto),'iBizBusinessCentral-Product_style-Create')")
    @ApiOperation(value = "新建产品风格", tags = {"产品风格" },  notes = "新建产品风格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_styles")
    public ResponseEntity<Product_styleDTO> create(@Validated @RequestBody Product_styleDTO product_styledto) {
        Product_style domain = product_styleMapping.toDomain(product_styledto);
		product_styleService.create(domain);
        Product_styleDTO dto = product_styleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_styleMapping.toDomain(#product_styledtos),'iBizBusinessCentral-Product_style-Create')")
    @ApiOperation(value = "批量新建产品风格", tags = {"产品风格" },  notes = "批量新建产品风格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_styles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_styleDTO> product_styledtos) {
        product_styleService.createBatch(product_styleMapping.toDomain(product_styledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_style" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_styleService.get(#product_style_id),'iBizBusinessCentral-Product_style-Update')")
    @ApiOperation(value = "更新产品风格", tags = {"产品风格" },  notes = "更新产品风格")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_styles/{product_style_id}")
    public ResponseEntity<Product_styleDTO> update(@PathVariable("product_style_id") Long product_style_id, @RequestBody Product_styleDTO product_styledto) {
		Product_style domain  = product_styleMapping.toDomain(product_styledto);
        domain .setId(product_style_id);
		product_styleService.update(domain );
		Product_styleDTO dto = product_styleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_styleService.getProductStyleByEntities(this.product_styleMapping.toDomain(#product_styledtos)),'iBizBusinessCentral-Product_style-Update')")
    @ApiOperation(value = "批量更新产品风格", tags = {"产品风格" },  notes = "批量更新产品风格")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_styles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_styleDTO> product_styledtos) {
        product_styleService.updateBatch(product_styleMapping.toDomain(product_styledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_styleService.get(#product_style_id),'iBizBusinessCentral-Product_style-Remove')")
    @ApiOperation(value = "删除产品风格", tags = {"产品风格" },  notes = "删除产品风格")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_styles/{product_style_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_style_id") Long product_style_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_styleService.remove(product_style_id));
    }

    @PreAuthorize("hasPermission(this.product_styleService.getProductStyleByIds(#ids),'iBizBusinessCentral-Product_style-Remove')")
    @ApiOperation(value = "批量删除产品风格", tags = {"产品风格" },  notes = "批量删除产品风格")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_styles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_styleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_styleMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_style-Get')")
    @ApiOperation(value = "获取产品风格", tags = {"产品风格" },  notes = "获取产品风格")
	@RequestMapping(method = RequestMethod.GET, value = "/product_styles/{product_style_id}")
    public ResponseEntity<Product_styleDTO> get(@PathVariable("product_style_id") Long product_style_id) {
        Product_style domain = product_styleService.get(product_style_id);
        Product_styleDTO dto = product_styleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品风格草稿", tags = {"产品风格" },  notes = "获取产品风格草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_styles/getdraft")
    public ResponseEntity<Product_styleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_styleMapping.toDto(product_styleService.getDraft(new Product_style())));
    }

    @ApiOperation(value = "检查产品风格", tags = {"产品风格" },  notes = "检查产品风格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_styles/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_styleDTO product_styledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_styleService.checkKey(product_styleMapping.toDomain(product_styledto)));
    }

    @PreAuthorize("hasPermission(this.product_styleMapping.toDomain(#product_styledto),'iBizBusinessCentral-Product_style-Save')")
    @ApiOperation(value = "保存产品风格", tags = {"产品风格" },  notes = "保存产品风格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_styles/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_styleDTO product_styledto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_styleService.save(product_styleMapping.toDomain(product_styledto)));
    }

    @PreAuthorize("hasPermission(this.product_styleMapping.toDomain(#product_styledtos),'iBizBusinessCentral-Product_style-Save')")
    @ApiOperation(value = "批量保存产品风格", tags = {"产品风格" },  notes = "批量保存产品风格")
	@RequestMapping(method = RequestMethod.POST, value = "/product_styles/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_styleDTO> product_styledtos) {
        product_styleService.saveBatch(product_styleMapping.toDomain(product_styledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_style-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_style-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品风格" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_styles/fetchdefault")
	public ResponseEntity<List<Product_styleDTO>> fetchDefault(Product_styleSearchContext context) {
        Page<Product_style> domains = product_styleService.searchDefault(context) ;
        List<Product_styleDTO> list = product_styleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_style-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_style-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品风格" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_styles/searchdefault")
	public ResponseEntity<Page<Product_styleDTO>> searchDefault(@RequestBody Product_styleSearchContext context) {
        Page<Product_style> domains = product_styleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_styleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

