package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_track_lineService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_track_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存追溯行" })
@RestController("Core-stock_track_line")
@RequestMapping("")
public class Stock_track_lineResource {

    @Autowired
    public IStock_track_lineService stock_track_lineService;

    @Autowired
    @Lazy
    public Stock_track_lineMapping stock_track_lineMapping;

    @PreAuthorize("hasPermission(this.stock_track_lineMapping.toDomain(#stock_track_linedto),'iBizBusinessCentral-Stock_track_line-Create')")
    @ApiOperation(value = "新建库存追溯行", tags = {"库存追溯行" },  notes = "新建库存追溯行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines")
    public ResponseEntity<Stock_track_lineDTO> create(@Validated @RequestBody Stock_track_lineDTO stock_track_linedto) {
        Stock_track_line domain = stock_track_lineMapping.toDomain(stock_track_linedto);
		stock_track_lineService.create(domain);
        Stock_track_lineDTO dto = stock_track_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_track_lineMapping.toDomain(#stock_track_linedtos),'iBizBusinessCentral-Stock_track_line-Create')")
    @ApiOperation(value = "批量新建库存追溯行", tags = {"库存追溯行" },  notes = "批量新建库存追溯行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_track_lineDTO> stock_track_linedtos) {
        stock_track_lineService.createBatch(stock_track_lineMapping.toDomain(stock_track_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_track_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_track_lineService.get(#stock_track_line_id),'iBizBusinessCentral-Stock_track_line-Update')")
    @ApiOperation(value = "更新库存追溯行", tags = {"库存追溯行" },  notes = "更新库存追溯行")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_track_lines/{stock_track_line_id}")
    public ResponseEntity<Stock_track_lineDTO> update(@PathVariable("stock_track_line_id") Long stock_track_line_id, @RequestBody Stock_track_lineDTO stock_track_linedto) {
		Stock_track_line domain  = stock_track_lineMapping.toDomain(stock_track_linedto);
        domain .setId(stock_track_line_id);
		stock_track_lineService.update(domain );
		Stock_track_lineDTO dto = stock_track_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_track_lineService.getStockTrackLineByEntities(this.stock_track_lineMapping.toDomain(#stock_track_linedtos)),'iBizBusinessCentral-Stock_track_line-Update')")
    @ApiOperation(value = "批量更新库存追溯行", tags = {"库存追溯行" },  notes = "批量更新库存追溯行")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_track_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_track_lineDTO> stock_track_linedtos) {
        stock_track_lineService.updateBatch(stock_track_lineMapping.toDomain(stock_track_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_track_lineService.get(#stock_track_line_id),'iBizBusinessCentral-Stock_track_line-Remove')")
    @ApiOperation(value = "删除库存追溯行", tags = {"库存追溯行" },  notes = "删除库存追溯行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_lines/{stock_track_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_track_line_id") Long stock_track_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_track_lineService.remove(stock_track_line_id));
    }

    @PreAuthorize("hasPermission(this.stock_track_lineService.getStockTrackLineByIds(#ids),'iBizBusinessCentral-Stock_track_line-Remove')")
    @ApiOperation(value = "批量删除库存追溯行", tags = {"库存追溯行" },  notes = "批量删除库存追溯行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_track_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_track_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_track_line-Get')")
    @ApiOperation(value = "获取库存追溯行", tags = {"库存追溯行" },  notes = "获取库存追溯行")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_track_lines/{stock_track_line_id}")
    public ResponseEntity<Stock_track_lineDTO> get(@PathVariable("stock_track_line_id") Long stock_track_line_id) {
        Stock_track_line domain = stock_track_lineService.get(stock_track_line_id);
        Stock_track_lineDTO dto = stock_track_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存追溯行草稿", tags = {"库存追溯行" },  notes = "获取库存追溯行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_track_lines/getdraft")
    public ResponseEntity<Stock_track_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_track_lineMapping.toDto(stock_track_lineService.getDraft(new Stock_track_line())));
    }

    @ApiOperation(value = "检查库存追溯行", tags = {"库存追溯行" },  notes = "检查库存追溯行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_track_lineDTO stock_track_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_track_lineService.checkKey(stock_track_lineMapping.toDomain(stock_track_linedto)));
    }

    @PreAuthorize("hasPermission(this.stock_track_lineMapping.toDomain(#stock_track_linedto),'iBizBusinessCentral-Stock_track_line-Save')")
    @ApiOperation(value = "保存库存追溯行", tags = {"库存追溯行" },  notes = "保存库存追溯行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_track_lineDTO stock_track_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_track_lineService.save(stock_track_lineMapping.toDomain(stock_track_linedto)));
    }

    @PreAuthorize("hasPermission(this.stock_track_lineMapping.toDomain(#stock_track_linedtos),'iBizBusinessCentral-Stock_track_line-Save')")
    @ApiOperation(value = "批量保存库存追溯行", tags = {"库存追溯行" },  notes = "批量保存库存追溯行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_track_lineDTO> stock_track_linedtos) {
        stock_track_lineService.saveBatch(stock_track_lineMapping.toDomain(stock_track_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_track_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_track_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存追溯行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_track_lines/fetchdefault")
	public ResponseEntity<List<Stock_track_lineDTO>> fetchDefault(Stock_track_lineSearchContext context) {
        Page<Stock_track_line> domains = stock_track_lineService.searchDefault(context) ;
        List<Stock_track_lineDTO> list = stock_track_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_track_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_track_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存追溯行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_track_lines/searchdefault")
	public ResponseEntity<Page<Stock_track_lineDTO>> searchDefault(@RequestBody Stock_track_lineSearchContext context) {
        Page<Stock_track_line> domains = stock_track_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_track_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

