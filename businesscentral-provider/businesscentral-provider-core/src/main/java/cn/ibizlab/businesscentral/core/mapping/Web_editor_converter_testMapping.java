package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.businesscentral.core.dto.Web_editor_converter_testDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreWeb_editor_converter_testMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Web_editor_converter_testMapping extends MappingBase<Web_editor_converter_testDTO, Web_editor_converter_test> {


}

