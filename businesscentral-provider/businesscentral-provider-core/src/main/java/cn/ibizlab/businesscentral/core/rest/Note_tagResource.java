package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_note.domain.Note_tag;
import cn.ibizlab.businesscentral.core.odoo_note.service.INote_tagService;
import cn.ibizlab.businesscentral.core.odoo_note.filter.Note_tagSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"便签标签" })
@RestController("Core-note_tag")
@RequestMapping("")
public class Note_tagResource {

    @Autowired
    public INote_tagService note_tagService;

    @Autowired
    @Lazy
    public Note_tagMapping note_tagMapping;

    @PreAuthorize("hasPermission(this.note_tagMapping.toDomain(#note_tagdto),'iBizBusinessCentral-Note_tag-Create')")
    @ApiOperation(value = "新建便签标签", tags = {"便签标签" },  notes = "新建便签标签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_tags")
    public ResponseEntity<Note_tagDTO> create(@Validated @RequestBody Note_tagDTO note_tagdto) {
        Note_tag domain = note_tagMapping.toDomain(note_tagdto);
		note_tagService.create(domain);
        Note_tagDTO dto = note_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.note_tagMapping.toDomain(#note_tagdtos),'iBizBusinessCentral-Note_tag-Create')")
    @ApiOperation(value = "批量新建便签标签", tags = {"便签标签" },  notes = "批量新建便签标签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Note_tagDTO> note_tagdtos) {
        note_tagService.createBatch(note_tagMapping.toDomain(note_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "note_tag" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.note_tagService.get(#note_tag_id),'iBizBusinessCentral-Note_tag-Update')")
    @ApiOperation(value = "更新便签标签", tags = {"便签标签" },  notes = "更新便签标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_tags/{note_tag_id}")
    public ResponseEntity<Note_tagDTO> update(@PathVariable("note_tag_id") Long note_tag_id, @RequestBody Note_tagDTO note_tagdto) {
		Note_tag domain  = note_tagMapping.toDomain(note_tagdto);
        domain .setId(note_tag_id);
		note_tagService.update(domain );
		Note_tagDTO dto = note_tagMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.note_tagService.getNoteTagByEntities(this.note_tagMapping.toDomain(#note_tagdtos)),'iBizBusinessCentral-Note_tag-Update')")
    @ApiOperation(value = "批量更新便签标签", tags = {"便签标签" },  notes = "批量更新便签标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_tagDTO> note_tagdtos) {
        note_tagService.updateBatch(note_tagMapping.toDomain(note_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.note_tagService.get(#note_tag_id),'iBizBusinessCentral-Note_tag-Remove')")
    @ApiOperation(value = "删除便签标签", tags = {"便签标签" },  notes = "删除便签标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_tags/{note_tag_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("note_tag_id") Long note_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(note_tagService.remove(note_tag_id));
    }

    @PreAuthorize("hasPermission(this.note_tagService.getNoteTagByIds(#ids),'iBizBusinessCentral-Note_tag-Remove')")
    @ApiOperation(value = "批量删除便签标签", tags = {"便签标签" },  notes = "批量删除便签标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        note_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.note_tagMapping.toDomain(returnObject.body),'iBizBusinessCentral-Note_tag-Get')")
    @ApiOperation(value = "获取便签标签", tags = {"便签标签" },  notes = "获取便签标签")
	@RequestMapping(method = RequestMethod.GET, value = "/note_tags/{note_tag_id}")
    public ResponseEntity<Note_tagDTO> get(@PathVariable("note_tag_id") Long note_tag_id) {
        Note_tag domain = note_tagService.get(note_tag_id);
        Note_tagDTO dto = note_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取便签标签草稿", tags = {"便签标签" },  notes = "获取便签标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/note_tags/getdraft")
    public ResponseEntity<Note_tagDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(note_tagMapping.toDto(note_tagService.getDraft(new Note_tag())));
    }

    @ApiOperation(value = "检查便签标签", tags = {"便签标签" },  notes = "检查便签标签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_tags/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Note_tagDTO note_tagdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(note_tagService.checkKey(note_tagMapping.toDomain(note_tagdto)));
    }

    @PreAuthorize("hasPermission(this.note_tagMapping.toDomain(#note_tagdto),'iBizBusinessCentral-Note_tag-Save')")
    @ApiOperation(value = "保存便签标签", tags = {"便签标签" },  notes = "保存便签标签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_tags/save")
    public ResponseEntity<Boolean> save(@RequestBody Note_tagDTO note_tagdto) {
        return ResponseEntity.status(HttpStatus.OK).body(note_tagService.save(note_tagMapping.toDomain(note_tagdto)));
    }

    @PreAuthorize("hasPermission(this.note_tagMapping.toDomain(#note_tagdtos),'iBizBusinessCentral-Note_tag-Save')")
    @ApiOperation(value = "批量保存便签标签", tags = {"便签标签" },  notes = "批量保存便签标签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_tags/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Note_tagDTO> note_tagdtos) {
        note_tagService.saveBatch(note_tagMapping.toDomain(note_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Note_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Note_tag-Get')")
	@ApiOperation(value = "获取数据集", tags = {"便签标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/note_tags/fetchdefault")
	public ResponseEntity<List<Note_tagDTO>> fetchDefault(Note_tagSearchContext context) {
        Page<Note_tag> domains = note_tagService.searchDefault(context) ;
        List<Note_tagDTO> list = note_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Note_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Note_tag-Get')")
	@ApiOperation(value = "查询数据集", tags = {"便签标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/note_tags/searchdefault")
	public ResponseEntity<Page<Note_tagDTO>> searchDefault(@RequestBody Note_tagSearchContext context) {
        Page<Note_tag> domains = note_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(note_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

