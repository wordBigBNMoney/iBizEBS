package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_holidays_summary_deptService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"按部门的休假摘要报表" })
@RestController("Core-hr_holidays_summary_dept")
@RequestMapping("")
public class Hr_holidays_summary_deptResource {

    @Autowired
    public IHr_holidays_summary_deptService hr_holidays_summary_deptService;

    @Autowired
    @Lazy
    public Hr_holidays_summary_deptMapping hr_holidays_summary_deptMapping;

    @PreAuthorize("hasPermission(this.hr_holidays_summary_deptMapping.toDomain(#hr_holidays_summary_deptdto),'iBizBusinessCentral-Hr_holidays_summary_dept-Create')")
    @ApiOperation(value = "新建按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "新建按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts")
    public ResponseEntity<Hr_holidays_summary_deptDTO> create(@Validated @RequestBody Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto) {
        Hr_holidays_summary_dept domain = hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdto);
		hr_holidays_summary_deptService.create(domain);
        Hr_holidays_summary_deptDTO dto = hr_holidays_summary_deptMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_deptMapping.toDomain(#hr_holidays_summary_deptdtos),'iBizBusinessCentral-Hr_holidays_summary_dept-Create')")
    @ApiOperation(value = "批量新建按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "批量新建按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_holidays_summary_deptDTO> hr_holidays_summary_deptdtos) {
        hr_holidays_summary_deptService.createBatch(hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_holidays_summary_dept" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_holidays_summary_deptService.get(#hr_holidays_summary_dept_id),'iBizBusinessCentral-Hr_holidays_summary_dept-Update')")
    @ApiOperation(value = "更新按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "更新按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")
    public ResponseEntity<Hr_holidays_summary_deptDTO> update(@PathVariable("hr_holidays_summary_dept_id") Long hr_holidays_summary_dept_id, @RequestBody Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto) {
		Hr_holidays_summary_dept domain  = hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdto);
        domain .setId(hr_holidays_summary_dept_id);
		hr_holidays_summary_deptService.update(domain );
		Hr_holidays_summary_deptDTO dto = hr_holidays_summary_deptMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_deptService.getHrHolidaysSummaryDeptByEntities(this.hr_holidays_summary_deptMapping.toDomain(#hr_holidays_summary_deptdtos)),'iBizBusinessCentral-Hr_holidays_summary_dept-Update')")
    @ApiOperation(value = "批量更新按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "批量更新按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_depts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_holidays_summary_deptDTO> hr_holidays_summary_deptdtos) {
        hr_holidays_summary_deptService.updateBatch(hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_deptService.get(#hr_holidays_summary_dept_id),'iBizBusinessCentral-Hr_holidays_summary_dept-Remove')")
    @ApiOperation(value = "删除按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "删除按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_holidays_summary_dept_id") Long hr_holidays_summary_dept_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_deptService.remove(hr_holidays_summary_dept_id));
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_deptService.getHrHolidaysSummaryDeptByIds(#ids),'iBizBusinessCentral-Hr_holidays_summary_dept-Remove')")
    @ApiOperation(value = "批量删除按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "批量删除按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_depts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_holidays_summary_deptService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_holidays_summary_deptMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_holidays_summary_dept-Get')")
    @ApiOperation(value = "获取按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "获取按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")
    public ResponseEntity<Hr_holidays_summary_deptDTO> get(@PathVariable("hr_holidays_summary_dept_id") Long hr_holidays_summary_dept_id) {
        Hr_holidays_summary_dept domain = hr_holidays_summary_deptService.get(hr_holidays_summary_dept_id);
        Hr_holidays_summary_deptDTO dto = hr_holidays_summary_deptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取按部门的休假摘要报表草稿", tags = {"按部门的休假摘要报表" },  notes = "获取按部门的休假摘要报表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_depts/getdraft")
    public ResponseEntity<Hr_holidays_summary_deptDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_deptMapping.toDto(hr_holidays_summary_deptService.getDraft(new Hr_holidays_summary_dept())));
    }

    @ApiOperation(value = "检查按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "检查按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_deptService.checkKey(hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdto)));
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_deptMapping.toDomain(#hr_holidays_summary_deptdto),'iBizBusinessCentral-Hr_holidays_summary_dept-Save')")
    @ApiOperation(value = "保存按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "保存按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_deptService.save(hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdto)));
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_deptMapping.toDomain(#hr_holidays_summary_deptdtos),'iBizBusinessCentral-Hr_holidays_summary_dept-Save')")
    @ApiOperation(value = "批量保存按部门的休假摘要报表", tags = {"按部门的休假摘要报表" },  notes = "批量保存按部门的休假摘要报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_holidays_summary_deptDTO> hr_holidays_summary_deptdtos) {
        hr_holidays_summary_deptService.saveBatch(hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_holidays_summary_dept-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_holidays_summary_dept-Get')")
	@ApiOperation(value = "获取数据集", tags = {"按部门的休假摘要报表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_holidays_summary_depts/fetchdefault")
	public ResponseEntity<List<Hr_holidays_summary_deptDTO>> fetchDefault(Hr_holidays_summary_deptSearchContext context) {
        Page<Hr_holidays_summary_dept> domains = hr_holidays_summary_deptService.searchDefault(context) ;
        List<Hr_holidays_summary_deptDTO> list = hr_holidays_summary_deptMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_holidays_summary_dept-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_holidays_summary_dept-Get')")
	@ApiOperation(value = "查询数据集", tags = {"按部门的休假摘要报表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_holidays_summary_depts/searchdefault")
	public ResponseEntity<Page<Hr_holidays_summary_deptDTO>> searchDefault(@RequestBody Hr_holidays_summary_deptSearchContext context) {
        Page<Hr_holidays_summary_dept> domains = hr_holidays_summary_deptService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_holidays_summary_deptMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

