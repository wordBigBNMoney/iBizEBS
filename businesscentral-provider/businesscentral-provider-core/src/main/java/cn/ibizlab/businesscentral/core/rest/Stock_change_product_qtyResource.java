package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_change_product_qty;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_change_product_qtyService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_change_product_qtySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"更改产品数量" })
@RestController("Core-stock_change_product_qty")
@RequestMapping("")
public class Stock_change_product_qtyResource {

    @Autowired
    public IStock_change_product_qtyService stock_change_product_qtyService;

    @Autowired
    @Lazy
    public Stock_change_product_qtyMapping stock_change_product_qtyMapping;

    @PreAuthorize("hasPermission(this.stock_change_product_qtyMapping.toDomain(#stock_change_product_qtydto),'iBizBusinessCentral-Stock_change_product_qty-Create')")
    @ApiOperation(value = "新建更改产品数量", tags = {"更改产品数量" },  notes = "新建更改产品数量")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties")
    public ResponseEntity<Stock_change_product_qtyDTO> create(@Validated @RequestBody Stock_change_product_qtyDTO stock_change_product_qtydto) {
        Stock_change_product_qty domain = stock_change_product_qtyMapping.toDomain(stock_change_product_qtydto);
		stock_change_product_qtyService.create(domain);
        Stock_change_product_qtyDTO dto = stock_change_product_qtyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_change_product_qtyMapping.toDomain(#stock_change_product_qtydtos),'iBizBusinessCentral-Stock_change_product_qty-Create')")
    @ApiOperation(value = "批量新建更改产品数量", tags = {"更改产品数量" },  notes = "批量新建更改产品数量")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_change_product_qtyDTO> stock_change_product_qtydtos) {
        stock_change_product_qtyService.createBatch(stock_change_product_qtyMapping.toDomain(stock_change_product_qtydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_change_product_qty" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_change_product_qtyService.get(#stock_change_product_qty_id),'iBizBusinessCentral-Stock_change_product_qty-Update')")
    @ApiOperation(value = "更新更改产品数量", tags = {"更改产品数量" },  notes = "更新更改产品数量")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_change_product_qties/{stock_change_product_qty_id}")
    public ResponseEntity<Stock_change_product_qtyDTO> update(@PathVariable("stock_change_product_qty_id") Long stock_change_product_qty_id, @RequestBody Stock_change_product_qtyDTO stock_change_product_qtydto) {
		Stock_change_product_qty domain  = stock_change_product_qtyMapping.toDomain(stock_change_product_qtydto);
        domain .setId(stock_change_product_qty_id);
		stock_change_product_qtyService.update(domain );
		Stock_change_product_qtyDTO dto = stock_change_product_qtyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_change_product_qtyService.getStockChangeProductQtyByEntities(this.stock_change_product_qtyMapping.toDomain(#stock_change_product_qtydtos)),'iBizBusinessCentral-Stock_change_product_qty-Update')")
    @ApiOperation(value = "批量更新更改产品数量", tags = {"更改产品数量" },  notes = "批量更新更改产品数量")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_change_product_qties/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_change_product_qtyDTO> stock_change_product_qtydtos) {
        stock_change_product_qtyService.updateBatch(stock_change_product_qtyMapping.toDomain(stock_change_product_qtydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_change_product_qtyService.get(#stock_change_product_qty_id),'iBizBusinessCentral-Stock_change_product_qty-Remove')")
    @ApiOperation(value = "删除更改产品数量", tags = {"更改产品数量" },  notes = "删除更改产品数量")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_product_qties/{stock_change_product_qty_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_change_product_qty_id") Long stock_change_product_qty_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_change_product_qtyService.remove(stock_change_product_qty_id));
    }

    @PreAuthorize("hasPermission(this.stock_change_product_qtyService.getStockChangeProductQtyByIds(#ids),'iBizBusinessCentral-Stock_change_product_qty-Remove')")
    @ApiOperation(value = "批量删除更改产品数量", tags = {"更改产品数量" },  notes = "批量删除更改产品数量")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_product_qties/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_change_product_qtyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_change_product_qtyMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_change_product_qty-Get')")
    @ApiOperation(value = "获取更改产品数量", tags = {"更改产品数量" },  notes = "获取更改产品数量")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_change_product_qties/{stock_change_product_qty_id}")
    public ResponseEntity<Stock_change_product_qtyDTO> get(@PathVariable("stock_change_product_qty_id") Long stock_change_product_qty_id) {
        Stock_change_product_qty domain = stock_change_product_qtyService.get(stock_change_product_qty_id);
        Stock_change_product_qtyDTO dto = stock_change_product_qtyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取更改产品数量草稿", tags = {"更改产品数量" },  notes = "获取更改产品数量草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_change_product_qties/getdraft")
    public ResponseEntity<Stock_change_product_qtyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_change_product_qtyMapping.toDto(stock_change_product_qtyService.getDraft(new Stock_change_product_qty())));
    }

    @ApiOperation(value = "检查更改产品数量", tags = {"更改产品数量" },  notes = "检查更改产品数量")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_change_product_qtyDTO stock_change_product_qtydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_change_product_qtyService.checkKey(stock_change_product_qtyMapping.toDomain(stock_change_product_qtydto)));
    }

    @PreAuthorize("hasPermission(this.stock_change_product_qtyMapping.toDomain(#stock_change_product_qtydto),'iBizBusinessCentral-Stock_change_product_qty-Save')")
    @ApiOperation(value = "保存更改产品数量", tags = {"更改产品数量" },  notes = "保存更改产品数量")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_change_product_qtyDTO stock_change_product_qtydto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_change_product_qtyService.save(stock_change_product_qtyMapping.toDomain(stock_change_product_qtydto)));
    }

    @PreAuthorize("hasPermission(this.stock_change_product_qtyMapping.toDomain(#stock_change_product_qtydtos),'iBizBusinessCentral-Stock_change_product_qty-Save')")
    @ApiOperation(value = "批量保存更改产品数量", tags = {"更改产品数量" },  notes = "批量保存更改产品数量")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_change_product_qtyDTO> stock_change_product_qtydtos) {
        stock_change_product_qtyService.saveBatch(stock_change_product_qtyMapping.toDomain(stock_change_product_qtydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_change_product_qty-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_change_product_qty-Get')")
	@ApiOperation(value = "获取数据集", tags = {"更改产品数量" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_change_product_qties/fetchdefault")
	public ResponseEntity<List<Stock_change_product_qtyDTO>> fetchDefault(Stock_change_product_qtySearchContext context) {
        Page<Stock_change_product_qty> domains = stock_change_product_qtyService.searchDefault(context) ;
        List<Stock_change_product_qtyDTO> list = stock_change_product_qtyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_change_product_qty-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_change_product_qty-Get')")
	@ApiOperation(value = "查询数据集", tags = {"更改产品数量" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_change_product_qties/searchdefault")
	public ResponseEntity<Page<Stock_change_product_qtyDTO>> searchDefault(@RequestBody Stock_change_product_qtySearchContext context) {
        Page<Stock_change_product_qty> domains = stock_change_product_qtyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_change_product_qtyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

