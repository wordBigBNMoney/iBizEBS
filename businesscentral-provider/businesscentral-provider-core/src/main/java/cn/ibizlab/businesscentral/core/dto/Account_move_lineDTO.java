package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_move_lineDTO]
 */
@Data
public class Account_move_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String analyticTagIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [BALANCE]
     *
     */
    @JSONField(name = "balance")
    @JsonProperty("balance")
    private BigDecimal balance;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String taxIds;

    /**
     * 属性 [AMOUNT_CURRENCY]
     *
     */
    @JSONField(name = "amount_currency")
    @JsonProperty("amount_currency")
    private BigDecimal amountCurrency;

    /**
     * 属性 [TAX_LINE_GROUPING_KEY]
     *
     */
    @JSONField(name = "tax_line_grouping_key")
    @JsonProperty("tax_line_grouping_key")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String taxLineGroupingKey;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [RECONCILED]
     *
     */
    @JSONField(name = "reconciled")
    @JsonProperty("reconciled")
    private Boolean reconciled;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [BALANCE_CASH_BASIS]
     *
     */
    @JSONField(name = "balance_cash_basis")
    @JsonProperty("balance_cash_basis")
    private BigDecimal balanceCashBasis;

    /**
     * 属性 [CREDIT_CASH_BASIS]
     *
     */
    @JSONField(name = "credit_cash_basis")
    @JsonProperty("credit_cash_basis")
    private BigDecimal creditCashBasis;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private Double quantity;

    /**
     * 属性 [AMOUNT_RESIDUAL]
     *
     */
    @JSONField(name = "amount_residual")
    @JsonProperty("amount_residual")
    private BigDecimal amountResidual;

    /**
     * 属性 [RECOMPUTE_TAX_LINE]
     *
     */
    @JSONField(name = "recompute_tax_line")
    @JsonProperty("recompute_tax_line")
    private Boolean recomputeTaxLine;

    /**
     * 属性 [TAX_EXIGIBLE]
     *
     */
    @JSONField(name = "tax_exigible")
    @JsonProperty("tax_exigible")
    private Boolean taxExigible;

    /**
     * 属性 [AMOUNT_RESIDUAL_CURRENCY]
     *
     */
    @JSONField(name = "amount_residual_currency")
    @JsonProperty("amount_residual_currency")
    private BigDecimal amountResidualCurrency;

    /**
     * 属性 [TAX_BASE_AMOUNT]
     *
     */
    @JSONField(name = "tax_base_amount")
    @JsonProperty("tax_base_amount")
    private BigDecimal taxBaseAmount;

    /**
     * 属性 [PARENT_STATE]
     *
     */
    @JSONField(name = "parent_state")
    @JsonProperty("parent_state")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String parentState;

    /**
     * 属性 [BLOCKED]
     *
     */
    @JSONField(name = "blocked")
    @JsonProperty("blocked")
    private Boolean blocked;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MATCHED_CREDIT_IDS]
     *
     */
    @JSONField(name = "matched_credit_ids")
    @JsonProperty("matched_credit_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String matchedCreditIds;

    /**
     * 属性 [ANALYTIC_LINE_IDS]
     *
     */
    @JSONField(name = "analytic_line_ids")
    @JsonProperty("analytic_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String analyticLineIds;

    /**
     * 属性 [DATE_MATURITY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_maturity" , format="yyyy-MM-dd")
    @JsonProperty("date_maturity")
    @NotNull(message = "[到期日期]不允许为空!")
    private Timestamp dateMaturity;

    /**
     * 属性 [DEBIT]
     *
     */
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private BigDecimal debit;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [COUNTERPART]
     *
     */
    @JSONField(name = "counterpart")
    @JsonProperty("counterpart")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String counterpart;

    /**
     * 属性 [MATCHED_DEBIT_IDS]
     *
     */
    @JSONField(name = "matched_debit_ids")
    @JsonProperty("matched_debit_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String matchedDebitIds;

    /**
     * 属性 [DEBIT_CASH_BASIS]
     *
     */
    @JSONField(name = "debit_cash_basis")
    @JsonProperty("debit_cash_basis")
    private BigDecimal debitCashBasis;

    /**
     * 属性 [CREDIT]
     *
     */
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private BigDecimal credit;

    /**
     * 属性 [NARRATION]
     *
     */
    @JSONField(name = "narration")
    @JsonProperty("narration")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String narration;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [USER_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "user_type_id_text")
    @JsonProperty("user_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userTypeIdText;

    /**
     * 属性 [PAYMENT_ID_TEXT]
     *
     */
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paymentIdText;

    /**
     * 属性 [TAX_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "tax_line_id_text")
    @JsonProperty("tax_line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String taxLineIdText;

    /**
     * 属性 [FULL_RECONCILE_ID_TEXT]
     *
     */
    @JSONField(name = "full_reconcile_id_text")
    @JsonProperty("full_reconcile_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fullReconcileIdText;

    /**
     * 属性 [EXPENSE_ID_TEXT]
     *
     */
    @JSONField(name = "expense_id_text")
    @JsonProperty("expense_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String expenseIdText;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String moveIdText;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String journalIdText;

    /**
     * 属性 [COMPANY_CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "company_currency_id_text")
    @JsonProperty("company_currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyCurrencyIdText;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String invoiceIdText;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String analyticAccountIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [REF]
     *
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String ref;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [STATEMENT_ID_TEXT]
     *
     */
    @JSONField(name = "statement_id_text")
    @JsonProperty("statement_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String statementIdText;

    /**
     * 属性 [STATEMENT_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "statement_line_id_text")
    @JsonProperty("statement_line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String statementLineIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountIdText;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productUomIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[日记账分录]不允许为空!")
    private Long moveId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [PAYMENT_ID]
     *
     */
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long paymentId;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyCurrencyId;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long journalId;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long analyticAccountId;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long invoiceId;

    /**
     * 属性 [TAX_LINE_ID]
     *
     */
    @JSONField(name = "tax_line_id")
    @JsonProperty("tax_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long taxLineId;

    /**
     * 属性 [FULL_RECONCILE_ID]
     *
     */
    @JSONField(name = "full_reconcile_id")
    @JsonProperty("full_reconcile_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fullReconcileId;

    /**
     * 属性 [STATEMENT_LINE_ID]
     *
     */
    @JSONField(name = "statement_line_id")
    @JsonProperty("statement_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long statementLineId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productUomId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[科目]不允许为空!")
    private Long accountId;

    /**
     * 属性 [STATEMENT_ID]
     *
     */
    @JSONField(name = "statement_id")
    @JsonProperty("statement_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long statementId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [USER_TYPE_ID]
     *
     */
    @JSONField(name = "user_type_id")
    @JsonProperty("user_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userTypeId;

    /**
     * 属性 [EXPENSE_ID]
     *
     */
    @JSONField(name = "expense_id")
    @JsonProperty("expense_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long expenseId;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [BALANCE]
     */
    public void setBalance(BigDecimal  balance){
        this.balance = balance ;
        this.modify("balance",balance);
    }

    /**
     * 设置 [AMOUNT_CURRENCY]
     */
    public void setAmountCurrency(BigDecimal  amountCurrency){
        this.amountCurrency = amountCurrency ;
        this.modify("amount_currency",amountCurrency);
    }

    /**
     * 设置 [RECONCILED]
     */
    public void setReconciled(Boolean  reconciled){
        this.reconciled = reconciled ;
        this.modify("reconciled",reconciled);
    }

    /**
     * 设置 [BALANCE_CASH_BASIS]
     */
    public void setBalanceCashBasis(BigDecimal  balanceCashBasis){
        this.balanceCashBasis = balanceCashBasis ;
        this.modify("balance_cash_basis",balanceCashBasis);
    }

    /**
     * 设置 [CREDIT_CASH_BASIS]
     */
    public void setCreditCashBasis(BigDecimal  creditCashBasis){
        this.creditCashBasis = creditCashBasis ;
        this.modify("credit_cash_basis",creditCashBasis);
    }

    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [AMOUNT_RESIDUAL]
     */
    public void setAmountResidual(BigDecimal  amountResidual){
        this.amountResidual = amountResidual ;
        this.modify("amount_residual",amountResidual);
    }

    /**
     * 设置 [TAX_EXIGIBLE]
     */
    public void setTaxExigible(Boolean  taxExigible){
        this.taxExigible = taxExigible ;
        this.modify("tax_exigible",taxExigible);
    }

    /**
     * 设置 [AMOUNT_RESIDUAL_CURRENCY]
     */
    public void setAmountResidualCurrency(BigDecimal  amountResidualCurrency){
        this.amountResidualCurrency = amountResidualCurrency ;
        this.modify("amount_residual_currency",amountResidualCurrency);
    }

    /**
     * 设置 [TAX_BASE_AMOUNT]
     */
    public void setTaxBaseAmount(BigDecimal  taxBaseAmount){
        this.taxBaseAmount = taxBaseAmount ;
        this.modify("tax_base_amount",taxBaseAmount);
    }

    /**
     * 设置 [BLOCKED]
     */
    public void setBlocked(Boolean  blocked){
        this.blocked = blocked ;
        this.modify("blocked",blocked);
    }

    /**
     * 设置 [DATE_MATURITY]
     */
    public void setDateMaturity(Timestamp  dateMaturity){
        this.dateMaturity = dateMaturity ;
        this.modify("date_maturity",dateMaturity);
    }

    /**
     * 设置 [DEBIT]
     */
    public void setDebit(BigDecimal  debit){
        this.debit = debit ;
        this.modify("debit",debit);
    }

    /**
     * 设置 [DEBIT_CASH_BASIS]
     */
    public void setDebitCashBasis(BigDecimal  debitCashBasis){
        this.debitCashBasis = debitCashBasis ;
        this.modify("debit_cash_basis",debitCashBasis);
    }

    /**
     * 设置 [CREDIT]
     */
    public void setCredit(BigDecimal  credit){
        this.credit = credit ;
        this.modify("credit",credit);
    }

    /**
     * 设置 [MOVE_ID]
     */
    public void setMoveId(Long  moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [PAYMENT_ID]
     */
    public void setPaymentId(Long  paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    public void setCompanyCurrencyId(Long  companyCurrencyId){
        this.companyCurrencyId = companyCurrencyId ;
        this.modify("company_currency_id",companyCurrencyId);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Long  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    public void setAnalyticAccountId(Long  analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }

    /**
     * 设置 [INVOICE_ID]
     */
    public void setInvoiceId(Long  invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }

    /**
     * 设置 [TAX_LINE_ID]
     */
    public void setTaxLineId(Long  taxLineId){
        this.taxLineId = taxLineId ;
        this.modify("tax_line_id",taxLineId);
    }

    /**
     * 设置 [FULL_RECONCILE_ID]
     */
    public void setFullReconcileId(Long  fullReconcileId){
        this.fullReconcileId = fullReconcileId ;
        this.modify("full_reconcile_id",fullReconcileId);
    }

    /**
     * 设置 [STATEMENT_LINE_ID]
     */
    public void setStatementLineId(Long  statementLineId){
        this.statementLineId = statementLineId ;
        this.modify("statement_line_id",statementLineId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Long  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Long  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [STATEMENT_ID]
     */
    public void setStatementId(Long  statementId){
        this.statementId = statementId ;
        this.modify("statement_id",statementId);
    }

    /**
     * 设置 [USER_TYPE_ID]
     */
    public void setUserTypeId(Long  userTypeId){
        this.userTypeId = userTypeId ;
        this.modify("user_type_id",userTypeId);
    }

    /**
     * 设置 [EXPENSE_ID]
     */
    public void setExpenseId(Long  expenseId){
        this.expenseId = expenseId ;
        this.modify("expense_id",expenseId);
    }


}


