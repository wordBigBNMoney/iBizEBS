package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_reversalService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_move_reversalSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"会计凭证逆转" })
@RestController("Core-account_move_reversal")
@RequestMapping("")
public class Account_move_reversalResource {

    @Autowired
    public IAccount_move_reversalService account_move_reversalService;

    @Autowired
    @Lazy
    public Account_move_reversalMapping account_move_reversalMapping;

    @PreAuthorize("hasPermission(this.account_move_reversalMapping.toDomain(#account_move_reversaldto),'iBizBusinessCentral-Account_move_reversal-Create')")
    @ApiOperation(value = "新建会计凭证逆转", tags = {"会计凭证逆转" },  notes = "新建会计凭证逆转")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals")
    public ResponseEntity<Account_move_reversalDTO> create(@Validated @RequestBody Account_move_reversalDTO account_move_reversaldto) {
        Account_move_reversal domain = account_move_reversalMapping.toDomain(account_move_reversaldto);
		account_move_reversalService.create(domain);
        Account_move_reversalDTO dto = account_move_reversalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_move_reversalMapping.toDomain(#account_move_reversaldtos),'iBizBusinessCentral-Account_move_reversal-Create')")
    @ApiOperation(value = "批量新建会计凭证逆转", tags = {"会计凭证逆转" },  notes = "批量新建会计凭证逆转")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_move_reversalDTO> account_move_reversaldtos) {
        account_move_reversalService.createBatch(account_move_reversalMapping.toDomain(account_move_reversaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_move_reversal" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_move_reversalService.get(#account_move_reversal_id),'iBizBusinessCentral-Account_move_reversal-Update')")
    @ApiOperation(value = "更新会计凭证逆转", tags = {"会计凭证逆转" },  notes = "更新会计凭证逆转")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_move_reversals/{account_move_reversal_id}")
    public ResponseEntity<Account_move_reversalDTO> update(@PathVariable("account_move_reversal_id") Long account_move_reversal_id, @RequestBody Account_move_reversalDTO account_move_reversaldto) {
		Account_move_reversal domain  = account_move_reversalMapping.toDomain(account_move_reversaldto);
        domain .setId(account_move_reversal_id);
		account_move_reversalService.update(domain );
		Account_move_reversalDTO dto = account_move_reversalMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_move_reversalService.getAccountMoveReversalByEntities(this.account_move_reversalMapping.toDomain(#account_move_reversaldtos)),'iBizBusinessCentral-Account_move_reversal-Update')")
    @ApiOperation(value = "批量更新会计凭证逆转", tags = {"会计凭证逆转" },  notes = "批量更新会计凭证逆转")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_move_reversals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_move_reversalDTO> account_move_reversaldtos) {
        account_move_reversalService.updateBatch(account_move_reversalMapping.toDomain(account_move_reversaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_move_reversalService.get(#account_move_reversal_id),'iBizBusinessCentral-Account_move_reversal-Remove')")
    @ApiOperation(value = "删除会计凭证逆转", tags = {"会计凭证逆转" },  notes = "删除会计凭证逆转")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_move_reversals/{account_move_reversal_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_move_reversal_id") Long account_move_reversal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_move_reversalService.remove(account_move_reversal_id));
    }

    @PreAuthorize("hasPermission(this.account_move_reversalService.getAccountMoveReversalByIds(#ids),'iBizBusinessCentral-Account_move_reversal-Remove')")
    @ApiOperation(value = "批量删除会计凭证逆转", tags = {"会计凭证逆转" },  notes = "批量删除会计凭证逆转")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_move_reversals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_move_reversalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_move_reversalMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_move_reversal-Get')")
    @ApiOperation(value = "获取会计凭证逆转", tags = {"会计凭证逆转" },  notes = "获取会计凭证逆转")
	@RequestMapping(method = RequestMethod.GET, value = "/account_move_reversals/{account_move_reversal_id}")
    public ResponseEntity<Account_move_reversalDTO> get(@PathVariable("account_move_reversal_id") Long account_move_reversal_id) {
        Account_move_reversal domain = account_move_reversalService.get(account_move_reversal_id);
        Account_move_reversalDTO dto = account_move_reversalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取会计凭证逆转草稿", tags = {"会计凭证逆转" },  notes = "获取会计凭证逆转草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_move_reversals/getdraft")
    public ResponseEntity<Account_move_reversalDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_move_reversalMapping.toDto(account_move_reversalService.getDraft(new Account_move_reversal())));
    }

    @ApiOperation(value = "检查会计凭证逆转", tags = {"会计凭证逆转" },  notes = "检查会计凭证逆转")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_move_reversalDTO account_move_reversaldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_move_reversalService.checkKey(account_move_reversalMapping.toDomain(account_move_reversaldto)));
    }

    @PreAuthorize("hasPermission(this.account_move_reversalMapping.toDomain(#account_move_reversaldto),'iBizBusinessCentral-Account_move_reversal-Save')")
    @ApiOperation(value = "保存会计凭证逆转", tags = {"会计凭证逆转" },  notes = "保存会计凭证逆转")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_move_reversalDTO account_move_reversaldto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_move_reversalService.save(account_move_reversalMapping.toDomain(account_move_reversaldto)));
    }

    @PreAuthorize("hasPermission(this.account_move_reversalMapping.toDomain(#account_move_reversaldtos),'iBizBusinessCentral-Account_move_reversal-Save')")
    @ApiOperation(value = "批量保存会计凭证逆转", tags = {"会计凭证逆转" },  notes = "批量保存会计凭证逆转")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_move_reversalDTO> account_move_reversaldtos) {
        account_move_reversalService.saveBatch(account_move_reversalMapping.toDomain(account_move_reversaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_move_reversal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_move_reversal-Get')")
	@ApiOperation(value = "获取数据集", tags = {"会计凭证逆转" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_move_reversals/fetchdefault")
	public ResponseEntity<List<Account_move_reversalDTO>> fetchDefault(Account_move_reversalSearchContext context) {
        Page<Account_move_reversal> domains = account_move_reversalService.searchDefault(context) ;
        List<Account_move_reversalDTO> list = account_move_reversalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_move_reversal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_move_reversal-Get')")
	@ApiOperation(value = "查询数据集", tags = {"会计凭证逆转" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_move_reversals/searchdefault")
	public ResponseEntity<Page<Account_move_reversalDTO>> searchDefault(@RequestBody Account_move_reversalSearchContext context) {
        Page<Account_move_reversal> domains = account_move_reversalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_move_reversalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

