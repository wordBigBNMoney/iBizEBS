package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Im_livechat_report_operatorDTO]
 */
@Data
public class Im_livechat_report_operatorDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TIME_TO_ANSWER]
     *
     */
    @JSONField(name = "time_to_answer")
    @JsonProperty("time_to_answer")
    private Double timeToAnswer;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 属性 [NBR_CHANNEL]
     *
     */
    @JSONField(name = "nbr_channel")
    @JsonProperty("nbr_channel")
    private Integer nbrChannel;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [DURATION]
     *
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID_TEXT]
     *
     */
    @JSONField(name = "livechat_channel_id_text")
    @JsonProperty("livechat_channel_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String livechatChannelIdText;

    /**
     * 属性 [CHANNEL_ID_TEXT]
     *
     */
    @JSONField(name = "channel_id_text")
    @JsonProperty("channel_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String channelIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long channelId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID]
     *
     */
    @JSONField(name = "livechat_channel_id")
    @JsonProperty("livechat_channel_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long livechatChannelId;


    /**
     * 设置 [TIME_TO_ANSWER]
     */
    public void setTimeToAnswer(Double  timeToAnswer){
        this.timeToAnswer = timeToAnswer ;
        this.modify("time_to_answer",timeToAnswer);
    }

    /**
     * 设置 [START_DATE]
     */
    public void setStartDate(Timestamp  startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 设置 [NBR_CHANNEL]
     */
    public void setNbrChannel(Integer  nbrChannel){
        this.nbrChannel = nbrChannel ;
        this.modify("nbr_channel",nbrChannel);
    }

    /**
     * 设置 [DURATION]
     */
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [CHANNEL_ID]
     */
    public void setChannelId(Long  channelId){
        this.channelId = channelId ;
        this.modify("channel_id",channelId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [LIVECHAT_CHANNEL_ID]
     */
    public void setLivechatChannelId(Long  livechatChannelId){
        this.livechatChannelId = livechatChannelId ;
        this.modify("livechat_channel_id",livechatChannelId);
    }


}


