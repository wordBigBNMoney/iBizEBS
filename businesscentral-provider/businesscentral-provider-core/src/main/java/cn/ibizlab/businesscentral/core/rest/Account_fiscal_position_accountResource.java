package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_accountService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_accountSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"会计税科目映射" })
@RestController("Core-account_fiscal_position_account")
@RequestMapping("")
public class Account_fiscal_position_accountResource {

    @Autowired
    public IAccount_fiscal_position_accountService account_fiscal_position_accountService;

    @Autowired
    @Lazy
    public Account_fiscal_position_accountMapping account_fiscal_position_accountMapping;

    @PreAuthorize("hasPermission(this.account_fiscal_position_accountMapping.toDomain(#account_fiscal_position_accountdto),'iBizBusinessCentral-Account_fiscal_position_account-Create')")
    @ApiOperation(value = "新建会计税科目映射", tags = {"会计税科目映射" },  notes = "新建会计税科目映射")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_accounts")
    public ResponseEntity<Account_fiscal_position_accountDTO> create(@Validated @RequestBody Account_fiscal_position_accountDTO account_fiscal_position_accountdto) {
        Account_fiscal_position_account domain = account_fiscal_position_accountMapping.toDomain(account_fiscal_position_accountdto);
		account_fiscal_position_accountService.create(domain);
        Account_fiscal_position_accountDTO dto = account_fiscal_position_accountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_accountMapping.toDomain(#account_fiscal_position_accountdtos),'iBizBusinessCentral-Account_fiscal_position_account-Create')")
    @ApiOperation(value = "批量新建会计税科目映射", tags = {"会计税科目映射" },  notes = "批量新建会计税科目映射")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_accounts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_fiscal_position_accountDTO> account_fiscal_position_accountdtos) {
        account_fiscal_position_accountService.createBatch(account_fiscal_position_accountMapping.toDomain(account_fiscal_position_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_fiscal_position_account" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_fiscal_position_accountService.get(#account_fiscal_position_account_id),'iBizBusinessCentral-Account_fiscal_position_account-Update')")
    @ApiOperation(value = "更新会计税科目映射", tags = {"会计税科目映射" },  notes = "更新会计税科目映射")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_accounts/{account_fiscal_position_account_id}")
    public ResponseEntity<Account_fiscal_position_accountDTO> update(@PathVariable("account_fiscal_position_account_id") Long account_fiscal_position_account_id, @RequestBody Account_fiscal_position_accountDTO account_fiscal_position_accountdto) {
		Account_fiscal_position_account domain  = account_fiscal_position_accountMapping.toDomain(account_fiscal_position_accountdto);
        domain .setId(account_fiscal_position_account_id);
		account_fiscal_position_accountService.update(domain );
		Account_fiscal_position_accountDTO dto = account_fiscal_position_accountMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_accountService.getAccountFiscalPositionAccountByEntities(this.account_fiscal_position_accountMapping.toDomain(#account_fiscal_position_accountdtos)),'iBizBusinessCentral-Account_fiscal_position_account-Update')")
    @ApiOperation(value = "批量更新会计税科目映射", tags = {"会计税科目映射" },  notes = "批量更新会计税科目映射")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_accounts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_position_accountDTO> account_fiscal_position_accountdtos) {
        account_fiscal_position_accountService.updateBatch(account_fiscal_position_accountMapping.toDomain(account_fiscal_position_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_accountService.get(#account_fiscal_position_account_id),'iBizBusinessCentral-Account_fiscal_position_account-Remove')")
    @ApiOperation(value = "删除会计税科目映射", tags = {"会计税科目映射" },  notes = "删除会计税科目映射")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_accounts/{account_fiscal_position_account_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_position_account_id") Long account_fiscal_position_account_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_position_accountService.remove(account_fiscal_position_account_id));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_accountService.getAccountFiscalPositionAccountByIds(#ids),'iBizBusinessCentral-Account_fiscal_position_account-Remove')")
    @ApiOperation(value = "批量删除会计税科目映射", tags = {"会计税科目映射" },  notes = "批量删除会计税科目映射")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_accounts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_fiscal_position_accountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_fiscal_position_accountMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_fiscal_position_account-Get')")
    @ApiOperation(value = "获取会计税科目映射", tags = {"会计税科目映射" },  notes = "获取会计税科目映射")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_accounts/{account_fiscal_position_account_id}")
    public ResponseEntity<Account_fiscal_position_accountDTO> get(@PathVariable("account_fiscal_position_account_id") Long account_fiscal_position_account_id) {
        Account_fiscal_position_account domain = account_fiscal_position_accountService.get(account_fiscal_position_account_id);
        Account_fiscal_position_accountDTO dto = account_fiscal_position_accountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取会计税科目映射草稿", tags = {"会计税科目映射" },  notes = "获取会计税科目映射草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_accounts/getdraft")
    public ResponseEntity<Account_fiscal_position_accountDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_position_accountMapping.toDto(account_fiscal_position_accountService.getDraft(new Account_fiscal_position_account())));
    }

    @ApiOperation(value = "检查会计税科目映射", tags = {"会计税科目映射" },  notes = "检查会计税科目映射")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_accounts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_fiscal_position_accountDTO account_fiscal_position_accountdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_fiscal_position_accountService.checkKey(account_fiscal_position_accountMapping.toDomain(account_fiscal_position_accountdto)));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_accountMapping.toDomain(#account_fiscal_position_accountdto),'iBizBusinessCentral-Account_fiscal_position_account-Save')")
    @ApiOperation(value = "保存会计税科目映射", tags = {"会计税科目映射" },  notes = "保存会计税科目映射")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_accounts/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_fiscal_position_accountDTO account_fiscal_position_accountdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_position_accountService.save(account_fiscal_position_accountMapping.toDomain(account_fiscal_position_accountdto)));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_accountMapping.toDomain(#account_fiscal_position_accountdtos),'iBizBusinessCentral-Account_fiscal_position_account-Save')")
    @ApiOperation(value = "批量保存会计税科目映射", tags = {"会计税科目映射" },  notes = "批量保存会计税科目映射")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_accounts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_fiscal_position_accountDTO> account_fiscal_position_accountdtos) {
        account_fiscal_position_accountService.saveBatch(account_fiscal_position_accountMapping.toDomain(account_fiscal_position_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_fiscal_position_account-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_fiscal_position_account-Get')")
	@ApiOperation(value = "获取数据集", tags = {"会计税科目映射" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_fiscal_position_accounts/fetchdefault")
	public ResponseEntity<List<Account_fiscal_position_accountDTO>> fetchDefault(Account_fiscal_position_accountSearchContext context) {
        Page<Account_fiscal_position_account> domains = account_fiscal_position_accountService.searchDefault(context) ;
        List<Account_fiscal_position_accountDTO> list = account_fiscal_position_accountMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_fiscal_position_account-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_fiscal_position_account-Get')")
	@ApiOperation(value = "查询数据集", tags = {"会计税科目映射" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_fiscal_position_accounts/searchdefault")
	public ResponseEntity<Page<Account_fiscal_position_accountDTO>> searchDefault(@RequestBody Account_fiscal_position_accountSearchContext context) {
        Page<Account_fiscal_position_account> domains = account_fiscal_position_accountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_fiscal_position_accountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

