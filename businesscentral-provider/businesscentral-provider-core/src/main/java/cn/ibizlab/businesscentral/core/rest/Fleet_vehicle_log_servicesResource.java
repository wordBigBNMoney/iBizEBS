package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_services;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_servicesService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆服务" })
@RestController("Core-fleet_vehicle_log_services")
@RequestMapping("")
public class Fleet_vehicle_log_servicesResource {

    @Autowired
    public IFleet_vehicle_log_servicesService fleet_vehicle_log_servicesService;

    @Autowired
    @Lazy
    public Fleet_vehicle_log_servicesMapping fleet_vehicle_log_servicesMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_servicesMapping.toDomain(#fleet_vehicle_log_servicesdto),'iBizBusinessCentral-Fleet_vehicle_log_services-Create')")
    @ApiOperation(value = "新建车辆服务", tags = {"车辆服务" },  notes = "新建车辆服务")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services")
    public ResponseEntity<Fleet_vehicle_log_servicesDTO> create(@Validated @RequestBody Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto) {
        Fleet_vehicle_log_services domain = fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdto);
		fleet_vehicle_log_servicesService.create(domain);
        Fleet_vehicle_log_servicesDTO dto = fleet_vehicle_log_servicesMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_servicesMapping.toDomain(#fleet_vehicle_log_servicesdtos),'iBizBusinessCentral-Fleet_vehicle_log_services-Create')")
    @ApiOperation(value = "批量新建车辆服务", tags = {"车辆服务" },  notes = "批量新建车辆服务")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_log_servicesDTO> fleet_vehicle_log_servicesdtos) {
        fleet_vehicle_log_servicesService.createBatch(fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_log_services" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_log_servicesService.get(#fleet_vehicle_log_services_id),'iBizBusinessCentral-Fleet_vehicle_log_services-Update')")
    @ApiOperation(value = "更新车辆服务", tags = {"车辆服务" },  notes = "更新车辆服务")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")
    public ResponseEntity<Fleet_vehicle_log_servicesDTO> update(@PathVariable("fleet_vehicle_log_services_id") Long fleet_vehicle_log_services_id, @RequestBody Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto) {
		Fleet_vehicle_log_services domain  = fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdto);
        domain .setId(fleet_vehicle_log_services_id);
		fleet_vehicle_log_servicesService.update(domain );
		Fleet_vehicle_log_servicesDTO dto = fleet_vehicle_log_servicesMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_servicesService.getFleetVehicleLogServicesByEntities(this.fleet_vehicle_log_servicesMapping.toDomain(#fleet_vehicle_log_servicesdtos)),'iBizBusinessCentral-Fleet_vehicle_log_services-Update')")
    @ApiOperation(value = "批量更新车辆服务", tags = {"车辆服务" },  notes = "批量更新车辆服务")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_services/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_servicesDTO> fleet_vehicle_log_servicesdtos) {
        fleet_vehicle_log_servicesService.updateBatch(fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_servicesService.get(#fleet_vehicle_log_services_id),'iBizBusinessCentral-Fleet_vehicle_log_services-Remove')")
    @ApiOperation(value = "删除车辆服务", tags = {"车辆服务" },  notes = "删除车辆服务")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_services_id") Long fleet_vehicle_log_services_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_servicesService.remove(fleet_vehicle_log_services_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_servicesService.getFleetVehicleLogServicesByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_log_services-Remove')")
    @ApiOperation(value = "批量删除车辆服务", tags = {"车辆服务" },  notes = "批量删除车辆服务")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_services/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_log_servicesService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_log_servicesMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_log_services-Get')")
    @ApiOperation(value = "获取车辆服务", tags = {"车辆服务" },  notes = "获取车辆服务")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")
    public ResponseEntity<Fleet_vehicle_log_servicesDTO> get(@PathVariable("fleet_vehicle_log_services_id") Long fleet_vehicle_log_services_id) {
        Fleet_vehicle_log_services domain = fleet_vehicle_log_servicesService.get(fleet_vehicle_log_services_id);
        Fleet_vehicle_log_servicesDTO dto = fleet_vehicle_log_servicesMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆服务草稿", tags = {"车辆服务" },  notes = "获取车辆服务草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_services/getdraft")
    public ResponseEntity<Fleet_vehicle_log_servicesDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_servicesMapping.toDto(fleet_vehicle_log_servicesService.getDraft(new Fleet_vehicle_log_services())));
    }

    @ApiOperation(value = "检查车辆服务", tags = {"车辆服务" },  notes = "检查车辆服务")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_servicesService.checkKey(fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_servicesMapping.toDomain(#fleet_vehicle_log_servicesdto),'iBizBusinessCentral-Fleet_vehicle_log_services-Save')")
    @ApiOperation(value = "保存车辆服务", tags = {"车辆服务" },  notes = "保存车辆服务")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_servicesService.save(fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_servicesMapping.toDomain(#fleet_vehicle_log_servicesdtos),'iBizBusinessCentral-Fleet_vehicle_log_services-Save')")
    @ApiOperation(value = "批量保存车辆服务", tags = {"车辆服务" },  notes = "批量保存车辆服务")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_log_servicesDTO> fleet_vehicle_log_servicesdtos) {
        fleet_vehicle_log_servicesService.saveBatch(fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_log_services-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_log_services-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆服务" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_services/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_log_servicesDTO>> fetchDefault(Fleet_vehicle_log_servicesSearchContext context) {
        Page<Fleet_vehicle_log_services> domains = fleet_vehicle_log_servicesService.searchDefault(context) ;
        List<Fleet_vehicle_log_servicesDTO> list = fleet_vehicle_log_servicesMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_log_services-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_log_services-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆服务" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_log_services/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_servicesDTO>> searchDefault(@RequestBody Fleet_vehicle_log_servicesSearchContext context) {
        Page<Fleet_vehicle_log_services> domains = fleet_vehicle_log_servicesService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_log_servicesMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

