package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Fleet_vehicle_log_fuelDTO]
 */
@Data
public class Fleet_vehicle_log_fuelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PRICE_PER_LITER]
     *
     */
    @JSONField(name = "price_per_liter")
    @JsonProperty("price_per_liter")
    private Double pricePerLiter;

    /**
     * 属性 [COST_IDS]
     *
     */
    @JSONField(name = "cost_ids")
    @JsonProperty("cost_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String costIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [LITER]
     *
     */
    @JSONField(name = "liter")
    @JsonProperty("liter")
    private Double liter;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [NOTES]
     *
     */
    @JSONField(name = "notes")
    @JsonProperty("notes")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String notes;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [INV_REF]
     *
     */
    @JSONField(name = "inv_ref")
    @JsonProperty("inv_ref")
    @Size(min = 0, max = 64, message = "内容长度必须小于等于[64]")
    private String invRef;

    /**
     * 属性 [COST_SUBTYPE_ID]
     *
     */
    @JSONField(name = "cost_subtype_id")
    @JsonProperty("cost_subtype_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long costSubtypeId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ODOMETER_ID]
     *
     */
    @JSONField(name = "odometer_id")
    @JsonProperty("odometer_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long odometerId;

    /**
     * 属性 [COST_TYPE]
     *
     */
    @JSONField(name = "cost_type")
    @JsonProperty("cost_type")
    @NotBlank(message = "[费用所属类别]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costType;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [PURCHASER_ID_TEXT]
     *
     */
    @JSONField(name = "purchaser_id_text")
    @JsonProperty("purchaser_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purchaserIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [COST_AMOUNT]
     *
     */
    @JSONField(name = "cost_amount")
    @JsonProperty("cost_amount")
    private Double costAmount;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [VEHICLE_ID]
     *
     */
    @JSONField(name = "vehicle_id")
    @JsonProperty("vehicle_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[车辆]不允许为空!")
    private Long vehicleId;

    /**
     * 属性 [AUTO_GENERATED]
     *
     */
    @JSONField(name = "auto_generated")
    @JsonProperty("auto_generated")
    private Boolean autoGenerated;

    /**
     * 属性 [CONTRACT_ID]
     *
     */
    @JSONField(name = "contract_id")
    @JsonProperty("contract_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long contractId;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String description;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [VENDOR_ID_TEXT]
     *
     */
    @JSONField(name = "vendor_id_text")
    @JsonProperty("vendor_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String vendorIdText;

    /**
     * 属性 [ODOMETER]
     *
     */
    @JSONField(name = "odometer")
    @JsonProperty("odometer")
    private Double odometer;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [ODOMETER_UNIT]
     *
     */
    @JSONField(name = "odometer_unit")
    @JsonProperty("odometer_unit")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String odometerUnit;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [COST_ID]
     *
     */
    @JSONField(name = "cost_id")
    @JsonProperty("cost_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[成本]不允许为空!")
    private Long costId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [VENDOR_ID]
     *
     */
    @JSONField(name = "vendor_id")
    @JsonProperty("vendor_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long vendorId;

    /**
     * 属性 [PURCHASER_ID]
     *
     */
    @JSONField(name = "purchaser_id")
    @JsonProperty("purchaser_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaserId;


    /**
     * 设置 [PRICE_PER_LITER]
     */
    public void setPricePerLiter(Double  pricePerLiter){
        this.pricePerLiter = pricePerLiter ;
        this.modify("price_per_liter",pricePerLiter);
    }

    /**
     * 设置 [LITER]
     */
    public void setLiter(Double  liter){
        this.liter = liter ;
        this.modify("liter",liter);
    }

    /**
     * 设置 [NOTES]
     */
    public void setNotes(String  notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [INV_REF]
     */
    public void setInvRef(String  invRef){
        this.invRef = invRef ;
        this.modify("inv_ref",invRef);
    }

    /**
     * 设置 [COST_ID]
     */
    public void setCostId(Long  costId){
        this.costId = costId ;
        this.modify("cost_id",costId);
    }

    /**
     * 设置 [VENDOR_ID]
     */
    public void setVendorId(Long  vendorId){
        this.vendorId = vendorId ;
        this.modify("vendor_id",vendorId);
    }

    /**
     * 设置 [PURCHASER_ID]
     */
    public void setPurchaserId(Long  purchaserId){
        this.purchaserId = purchaserId ;
        this.modify("purchaser_id",purchaserId);
    }


}


