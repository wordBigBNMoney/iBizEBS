package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_tag;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_tagService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_tagSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"群发邮件标签" })
@RestController("Core-mail_mass_mailing_tag")
@RequestMapping("")
public class Mail_mass_mailing_tagResource {

    @Autowired
    public IMail_mass_mailing_tagService mail_mass_mailing_tagService;

    @Autowired
    @Lazy
    public Mail_mass_mailing_tagMapping mail_mass_mailing_tagMapping;

    @PreAuthorize("hasPermission(this.mail_mass_mailing_tagMapping.toDomain(#mail_mass_mailing_tagdto),'iBizBusinessCentral-Mail_mass_mailing_tag-Create')")
    @ApiOperation(value = "新建群发邮件标签", tags = {"群发邮件标签" },  notes = "新建群发邮件标签")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags")
    public ResponseEntity<Mail_mass_mailing_tagDTO> create(@Validated @RequestBody Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto) {
        Mail_mass_mailing_tag domain = mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdto);
		mail_mass_mailing_tagService.create(domain);
        Mail_mass_mailing_tagDTO dto = mail_mass_mailing_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_tagMapping.toDomain(#mail_mass_mailing_tagdtos),'iBizBusinessCentral-Mail_mass_mailing_tag-Create')")
    @ApiOperation(value = "批量新建群发邮件标签", tags = {"群发邮件标签" },  notes = "批量新建群发邮件标签")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_tagDTO> mail_mass_mailing_tagdtos) {
        mail_mass_mailing_tagService.createBatch(mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mass_mailing_tag" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mass_mailing_tagService.get(#mail_mass_mailing_tag_id),'iBizBusinessCentral-Mail_mass_mailing_tag-Update')")
    @ApiOperation(value = "更新群发邮件标签", tags = {"群发邮件标签" },  notes = "更新群发邮件标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")
    public ResponseEntity<Mail_mass_mailing_tagDTO> update(@PathVariable("mail_mass_mailing_tag_id") Long mail_mass_mailing_tag_id, @RequestBody Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto) {
		Mail_mass_mailing_tag domain  = mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdto);
        domain .setId(mail_mass_mailing_tag_id);
		mail_mass_mailing_tagService.update(domain );
		Mail_mass_mailing_tagDTO dto = mail_mass_mailing_tagMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_tagService.getMailMassMailingTagByEntities(this.mail_mass_mailing_tagMapping.toDomain(#mail_mass_mailing_tagdtos)),'iBizBusinessCentral-Mail_mass_mailing_tag-Update')")
    @ApiOperation(value = "批量更新群发邮件标签", tags = {"群发邮件标签" },  notes = "批量更新群发邮件标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_tagDTO> mail_mass_mailing_tagdtos) {
        mail_mass_mailing_tagService.updateBatch(mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_tagService.get(#mail_mass_mailing_tag_id),'iBizBusinessCentral-Mail_mass_mailing_tag-Remove')")
    @ApiOperation(value = "删除群发邮件标签", tags = {"群发邮件标签" },  notes = "删除群发邮件标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_tag_id") Long mail_mass_mailing_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_tagService.remove(mail_mass_mailing_tag_id));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_tagService.getMailMassMailingTagByIds(#ids),'iBizBusinessCentral-Mail_mass_mailing_tag-Remove')")
    @ApiOperation(value = "批量删除群发邮件标签", tags = {"群发邮件标签" },  notes = "批量删除群发邮件标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mass_mailing_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mass_mailing_tagMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mass_mailing_tag-Get')")
    @ApiOperation(value = "获取群发邮件标签", tags = {"群发邮件标签" },  notes = "获取群发邮件标签")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")
    public ResponseEntity<Mail_mass_mailing_tagDTO> get(@PathVariable("mail_mass_mailing_tag_id") Long mail_mass_mailing_tag_id) {
        Mail_mass_mailing_tag domain = mail_mass_mailing_tagService.get(mail_mass_mailing_tag_id);
        Mail_mass_mailing_tagDTO dto = mail_mass_mailing_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取群发邮件标签草稿", tags = {"群发邮件标签" },  notes = "获取群发邮件标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tags/getdraft")
    public ResponseEntity<Mail_mass_mailing_tagDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_tagMapping.toDto(mail_mass_mailing_tagService.getDraft(new Mail_mass_mailing_tag())));
    }

    @ApiOperation(value = "检查群发邮件标签", tags = {"群发邮件标签" },  notes = "检查群发邮件标签")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_tagService.checkKey(mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_tagMapping.toDomain(#mail_mass_mailing_tagdto),'iBizBusinessCentral-Mail_mass_mailing_tag-Save')")
    @ApiOperation(value = "保存群发邮件标签", tags = {"群发邮件标签" },  notes = "保存群发邮件标签")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_tagService.save(mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_tagMapping.toDomain(#mail_mass_mailing_tagdtos),'iBizBusinessCentral-Mail_mass_mailing_tag-Save')")
    @ApiOperation(value = "批量保存群发邮件标签", tags = {"群发邮件标签" },  notes = "批量保存群发邮件标签")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mass_mailing_tagDTO> mail_mass_mailing_tagdtos) {
        mail_mass_mailing_tagService.saveBatch(mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_tag-Get')")
	@ApiOperation(value = "获取数据集", tags = {"群发邮件标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_tags/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_tagDTO>> fetchDefault(Mail_mass_mailing_tagSearchContext context) {
        Page<Mail_mass_mailing_tag> domains = mail_mass_mailing_tagService.searchDefault(context) ;
        List<Mail_mass_mailing_tagDTO> list = mail_mass_mailing_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_tag-Get')")
	@ApiOperation(value = "查询数据集", tags = {"群发邮件标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mass_mailing_tags/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_tagDTO>> searchDefault(@RequestBody Mail_mass_mailing_tagSearchContext context) {
        Page<Mail_mass_mailing_tag> domains = mail_mass_mailing_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

