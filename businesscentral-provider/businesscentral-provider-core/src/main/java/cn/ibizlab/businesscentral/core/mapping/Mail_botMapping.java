package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.businesscentral.core.dto.Mail_botDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMail_botMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_botMapping extends MappingBase<Mail_botDTO, Mail_bot> {


}

