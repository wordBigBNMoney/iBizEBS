package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.businesscentral.core.dto.Hr_contract_typeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreHr_contract_typeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_contract_typeMapping extends MappingBase<Hr_contract_typeDTO, Hr_contract_type> {


}

