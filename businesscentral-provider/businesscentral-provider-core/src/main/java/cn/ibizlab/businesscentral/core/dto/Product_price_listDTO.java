package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Product_price_listDTO]
 */
@Data
public class Product_price_listDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [QTY5]
     *
     */
    @JSONField(name = "qty5")
    @JsonProperty("qty5")
    private Integer qty5;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [QTY3]
     *
     */
    @JSONField(name = "qty3")
    @JsonProperty("qty3")
    private Integer qty3;

    /**
     * 属性 [QTY4]
     *
     */
    @JSONField(name = "qty4")
    @JsonProperty("qty4")
    private Integer qty4;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [QTY1]
     *
     */
    @JSONField(name = "qty1")
    @JsonProperty("qty1")
    private Integer qty1;

    /**
     * 属性 [QTY2]
     *
     */
    @JSONField(name = "qty2")
    @JsonProperty("qty2")
    private Integer qty2;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PRICE_LIST_TEXT]
     *
     */
    @JSONField(name = "price_list_text")
    @JsonProperty("price_list_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String priceListText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PRICE_LIST]
     *
     */
    @JSONField(name = "price_list")
    @JsonProperty("price_list")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[价格表]不允许为空!")
    private Long priceList;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [QTY5]
     */
    public void setQty5(Integer  qty5){
        this.qty5 = qty5 ;
        this.modify("qty5",qty5);
    }

    /**
     * 设置 [QTY3]
     */
    public void setQty3(Integer  qty3){
        this.qty3 = qty3 ;
        this.modify("qty3",qty3);
    }

    /**
     * 设置 [QTY4]
     */
    public void setQty4(Integer  qty4){
        this.qty4 = qty4 ;
        this.modify("qty4",qty4);
    }

    /**
     * 设置 [QTY1]
     */
    public void setQty1(Integer  qty1){
        this.qty1 = qty1 ;
        this.modify("qty1",qty1);
    }

    /**
     * 设置 [QTY2]
     */
    public void setQty2(Integer  qty2){
        this.qty2 = qty2 ;
        this.modify("qty2",qty2);
    }

    /**
     * 设置 [PRICE_LIST]
     */
    public void setPriceList(Long  priceList){
        this.priceList = priceList ;
        this.modify("price_list",priceList);
    }


}


