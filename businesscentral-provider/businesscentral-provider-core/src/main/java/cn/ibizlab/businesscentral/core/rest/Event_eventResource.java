package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_eventService;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_eventSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动" })
@RestController("Core-event_event")
@RequestMapping("")
public class Event_eventResource {

    @Autowired
    public IEvent_eventService event_eventService;

    @Autowired
    @Lazy
    public Event_eventMapping event_eventMapping;

    @PreAuthorize("hasPermission(this.event_eventMapping.toDomain(#event_eventdto),'iBizBusinessCentral-Event_event-Create')")
    @ApiOperation(value = "新建活动", tags = {"活动" },  notes = "新建活动")
	@RequestMapping(method = RequestMethod.POST, value = "/event_events")
    public ResponseEntity<Event_eventDTO> create(@Validated @RequestBody Event_eventDTO event_eventdto) {
        Event_event domain = event_eventMapping.toDomain(event_eventdto);
		event_eventService.create(domain);
        Event_eventDTO dto = event_eventMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_eventMapping.toDomain(#event_eventdtos),'iBizBusinessCentral-Event_event-Create')")
    @ApiOperation(value = "批量新建活动", tags = {"活动" },  notes = "批量新建活动")
	@RequestMapping(method = RequestMethod.POST, value = "/event_events/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_eventDTO> event_eventdtos) {
        event_eventService.createBatch(event_eventMapping.toDomain(event_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "event_event" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.event_eventService.get(#event_event_id),'iBizBusinessCentral-Event_event-Update')")
    @ApiOperation(value = "更新活动", tags = {"活动" },  notes = "更新活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_events/{event_event_id}")
    public ResponseEntity<Event_eventDTO> update(@PathVariable("event_event_id") Long event_event_id, @RequestBody Event_eventDTO event_eventdto) {
		Event_event domain  = event_eventMapping.toDomain(event_eventdto);
        domain .setId(event_event_id);
		event_eventService.update(domain );
		Event_eventDTO dto = event_eventMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_eventService.getEventEventByEntities(this.event_eventMapping.toDomain(#event_eventdtos)),'iBizBusinessCentral-Event_event-Update')")
    @ApiOperation(value = "批量更新活动", tags = {"活动" },  notes = "批量更新活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_events/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_eventDTO> event_eventdtos) {
        event_eventService.updateBatch(event_eventMapping.toDomain(event_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.event_eventService.get(#event_event_id),'iBizBusinessCentral-Event_event-Remove')")
    @ApiOperation(value = "删除活动", tags = {"活动" },  notes = "删除活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_events/{event_event_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("event_event_id") Long event_event_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_eventService.remove(event_event_id));
    }

    @PreAuthorize("hasPermission(this.event_eventService.getEventEventByIds(#ids),'iBizBusinessCentral-Event_event-Remove')")
    @ApiOperation(value = "批量删除活动", tags = {"活动" },  notes = "批量删除活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_events/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        event_eventService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.event_eventMapping.toDomain(returnObject.body),'iBizBusinessCentral-Event_event-Get')")
    @ApiOperation(value = "获取活动", tags = {"活动" },  notes = "获取活动")
	@RequestMapping(method = RequestMethod.GET, value = "/event_events/{event_event_id}")
    public ResponseEntity<Event_eventDTO> get(@PathVariable("event_event_id") Long event_event_id) {
        Event_event domain = event_eventService.get(event_event_id);
        Event_eventDTO dto = event_eventMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动草稿", tags = {"活动" },  notes = "获取活动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/event_events/getdraft")
    public ResponseEntity<Event_eventDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(event_eventMapping.toDto(event_eventService.getDraft(new Event_event())));
    }

    @ApiOperation(value = "检查活动", tags = {"活动" },  notes = "检查活动")
	@RequestMapping(method = RequestMethod.POST, value = "/event_events/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Event_eventDTO event_eventdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(event_eventService.checkKey(event_eventMapping.toDomain(event_eventdto)));
    }

    @PreAuthorize("hasPermission(this.event_eventMapping.toDomain(#event_eventdto),'iBizBusinessCentral-Event_event-Save')")
    @ApiOperation(value = "保存活动", tags = {"活动" },  notes = "保存活动")
	@RequestMapping(method = RequestMethod.POST, value = "/event_events/save")
    public ResponseEntity<Boolean> save(@RequestBody Event_eventDTO event_eventdto) {
        return ResponseEntity.status(HttpStatus.OK).body(event_eventService.save(event_eventMapping.toDomain(event_eventdto)));
    }

    @PreAuthorize("hasPermission(this.event_eventMapping.toDomain(#event_eventdtos),'iBizBusinessCentral-Event_event-Save')")
    @ApiOperation(value = "批量保存活动", tags = {"活动" },  notes = "批量保存活动")
	@RequestMapping(method = RequestMethod.POST, value = "/event_events/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Event_eventDTO> event_eventdtos) {
        event_eventService.saveBatch(event_eventMapping.toDomain(event_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_event-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_event-Get')")
	@ApiOperation(value = "获取数据集", tags = {"活动" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/event_events/fetchdefault")
	public ResponseEntity<List<Event_eventDTO>> fetchDefault(Event_eventSearchContext context) {
        Page<Event_event> domains = event_eventService.searchDefault(context) ;
        List<Event_eventDTO> list = event_eventMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_event-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_event-Get')")
	@ApiOperation(value = "查询数据集", tags = {"活动" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/event_events/searchdefault")
	public ResponseEntity<Page<Event_eventDTO>> searchDefault(@RequestBody Event_eventSearchContext context) {
        Page<Event_event> domains = event_eventService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_eventMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

