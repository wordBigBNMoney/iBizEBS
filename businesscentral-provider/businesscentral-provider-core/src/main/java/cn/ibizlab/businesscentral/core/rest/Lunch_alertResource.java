package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_alertService;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_alertSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"午餐提醒" })
@RestController("Core-lunch_alert")
@RequestMapping("")
public class Lunch_alertResource {

    @Autowired
    public ILunch_alertService lunch_alertService;

    @Autowired
    @Lazy
    public Lunch_alertMapping lunch_alertMapping;

    @PreAuthorize("hasPermission(this.lunch_alertMapping.toDomain(#lunch_alertdto),'iBizBusinessCentral-Lunch_alert-Create')")
    @ApiOperation(value = "新建午餐提醒", tags = {"午餐提醒" },  notes = "新建午餐提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts")
    public ResponseEntity<Lunch_alertDTO> create(@Validated @RequestBody Lunch_alertDTO lunch_alertdto) {
        Lunch_alert domain = lunch_alertMapping.toDomain(lunch_alertdto);
		lunch_alertService.create(domain);
        Lunch_alertDTO dto = lunch_alertMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_alertMapping.toDomain(#lunch_alertdtos),'iBizBusinessCentral-Lunch_alert-Create')")
    @ApiOperation(value = "批量新建午餐提醒", tags = {"午餐提醒" },  notes = "批量新建午餐提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_alertDTO> lunch_alertdtos) {
        lunch_alertService.createBatch(lunch_alertMapping.toDomain(lunch_alertdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lunch_alert" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.lunch_alertService.get(#lunch_alert_id),'iBizBusinessCentral-Lunch_alert-Update')")
    @ApiOperation(value = "更新午餐提醒", tags = {"午餐提醒" },  notes = "更新午餐提醒")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_alerts/{lunch_alert_id}")
    public ResponseEntity<Lunch_alertDTO> update(@PathVariable("lunch_alert_id") Long lunch_alert_id, @RequestBody Lunch_alertDTO lunch_alertdto) {
		Lunch_alert domain  = lunch_alertMapping.toDomain(lunch_alertdto);
        domain .setId(lunch_alert_id);
		lunch_alertService.update(domain );
		Lunch_alertDTO dto = lunch_alertMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_alertService.getLunchAlertByEntities(this.lunch_alertMapping.toDomain(#lunch_alertdtos)),'iBizBusinessCentral-Lunch_alert-Update')")
    @ApiOperation(value = "批量更新午餐提醒", tags = {"午餐提醒" },  notes = "批量更新午餐提醒")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_alerts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_alertDTO> lunch_alertdtos) {
        lunch_alertService.updateBatch(lunch_alertMapping.toDomain(lunch_alertdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.lunch_alertService.get(#lunch_alert_id),'iBizBusinessCentral-Lunch_alert-Remove')")
    @ApiOperation(value = "删除午餐提醒", tags = {"午餐提醒" },  notes = "删除午餐提醒")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_alerts/{lunch_alert_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("lunch_alert_id") Long lunch_alert_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_alertService.remove(lunch_alert_id));
    }

    @PreAuthorize("hasPermission(this.lunch_alertService.getLunchAlertByIds(#ids),'iBizBusinessCentral-Lunch_alert-Remove')")
    @ApiOperation(value = "批量删除午餐提醒", tags = {"午餐提醒" },  notes = "批量删除午餐提醒")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_alerts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        lunch_alertService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.lunch_alertMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lunch_alert-Get')")
    @ApiOperation(value = "获取午餐提醒", tags = {"午餐提醒" },  notes = "获取午餐提醒")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_alerts/{lunch_alert_id}")
    public ResponseEntity<Lunch_alertDTO> get(@PathVariable("lunch_alert_id") Long lunch_alert_id) {
        Lunch_alert domain = lunch_alertService.get(lunch_alert_id);
        Lunch_alertDTO dto = lunch_alertMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取午餐提醒草稿", tags = {"午餐提醒" },  notes = "获取午餐提醒草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_alerts/getdraft")
    public ResponseEntity<Lunch_alertDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_alertMapping.toDto(lunch_alertService.getDraft(new Lunch_alert())));
    }

    @ApiOperation(value = "检查午餐提醒", tags = {"午餐提醒" },  notes = "检查午餐提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Lunch_alertDTO lunch_alertdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(lunch_alertService.checkKey(lunch_alertMapping.toDomain(lunch_alertdto)));
    }

    @PreAuthorize("hasPermission(this.lunch_alertMapping.toDomain(#lunch_alertdto),'iBizBusinessCentral-Lunch_alert-Save')")
    @ApiOperation(value = "保存午餐提醒", tags = {"午餐提醒" },  notes = "保存午餐提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts/save")
    public ResponseEntity<Boolean> save(@RequestBody Lunch_alertDTO lunch_alertdto) {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_alertService.save(lunch_alertMapping.toDomain(lunch_alertdto)));
    }

    @PreAuthorize("hasPermission(this.lunch_alertMapping.toDomain(#lunch_alertdtos),'iBizBusinessCentral-Lunch_alert-Save')")
    @ApiOperation(value = "批量保存午餐提醒", tags = {"午餐提醒" },  notes = "批量保存午餐提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Lunch_alertDTO> lunch_alertdtos) {
        lunch_alertService.saveBatch(lunch_alertMapping.toDomain(lunch_alertdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_alert-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_alert-Get')")
	@ApiOperation(value = "获取数据集", tags = {"午餐提醒" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_alerts/fetchdefault")
	public ResponseEntity<List<Lunch_alertDTO>> fetchDefault(Lunch_alertSearchContext context) {
        Page<Lunch_alert> domains = lunch_alertService.searchDefault(context) ;
        List<Lunch_alertDTO> list = lunch_alertMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_alert-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_alert-Get')")
	@ApiOperation(value = "查询数据集", tags = {"午餐提醒" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/lunch_alerts/searchdefault")
	public ResponseEntity<Page<Lunch_alertDTO>> searchDefault(@RequestBody Lunch_alertSearchContext context) {
        Page<Lunch_alert> domains = lunch_alertService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_alertMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

