package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_channel;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_report_channelService;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_report_channelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"实时聊天支持频道报告" })
@RestController("Core-im_livechat_report_channel")
@RequestMapping("")
public class Im_livechat_report_channelResource {

    @Autowired
    public IIm_livechat_report_channelService im_livechat_report_channelService;

    @Autowired
    @Lazy
    public Im_livechat_report_channelMapping im_livechat_report_channelMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Create-all')")
    @ApiOperation(value = "新建实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "新建实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels")
    public ResponseEntity<Im_livechat_report_channelDTO> create(@Validated @RequestBody Im_livechat_report_channelDTO im_livechat_report_channeldto) {
        Im_livechat_report_channel domain = im_livechat_report_channelMapping.toDomain(im_livechat_report_channeldto);
		im_livechat_report_channelService.create(domain);
        Im_livechat_report_channelDTO dto = im_livechat_report_channelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Create-all')")
    @ApiOperation(value = "批量新建实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "批量新建实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Im_livechat_report_channelDTO> im_livechat_report_channeldtos) {
        im_livechat_report_channelService.createBatch(im_livechat_report_channelMapping.toDomain(im_livechat_report_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Update-all')")
    @ApiOperation(value = "更新实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "更新实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_channels/{im_livechat_report_channel_id}")
    public ResponseEntity<Im_livechat_report_channelDTO> update(@PathVariable("im_livechat_report_channel_id") Long im_livechat_report_channel_id, @RequestBody Im_livechat_report_channelDTO im_livechat_report_channeldto) {
		Im_livechat_report_channel domain  = im_livechat_report_channelMapping.toDomain(im_livechat_report_channeldto);
        domain .setId(im_livechat_report_channel_id);
		im_livechat_report_channelService.update(domain );
		Im_livechat_report_channelDTO dto = im_livechat_report_channelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Update-all')")
    @ApiOperation(value = "批量更新实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "批量更新实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_channels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_report_channelDTO> im_livechat_report_channeldtos) {
        im_livechat_report_channelService.updateBatch(im_livechat_report_channelMapping.toDomain(im_livechat_report_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Remove-all')")
    @ApiOperation(value = "删除实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "删除实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_channels/{im_livechat_report_channel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_report_channel_id") Long im_livechat_report_channel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_channelService.remove(im_livechat_report_channel_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Remove-all')")
    @ApiOperation(value = "批量删除实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "批量删除实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_channels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        im_livechat_report_channelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Get-all')")
    @ApiOperation(value = "获取实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "获取实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_channels/{im_livechat_report_channel_id}")
    public ResponseEntity<Im_livechat_report_channelDTO> get(@PathVariable("im_livechat_report_channel_id") Long im_livechat_report_channel_id) {
        Im_livechat_report_channel domain = im_livechat_report_channelService.get(im_livechat_report_channel_id);
        Im_livechat_report_channelDTO dto = im_livechat_report_channelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取实时聊天支持频道报告草稿", tags = {"实时聊天支持频道报告" },  notes = "获取实时聊天支持频道报告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_channels/getdraft")
    public ResponseEntity<Im_livechat_report_channelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_channelMapping.toDto(im_livechat_report_channelService.getDraft(new Im_livechat_report_channel())));
    }

    @ApiOperation(value = "检查实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "检查实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Im_livechat_report_channelDTO im_livechat_report_channeldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_channelService.checkKey(im_livechat_report_channelMapping.toDomain(im_livechat_report_channeldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Save-all')")
    @ApiOperation(value = "保存实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "保存实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/save")
    public ResponseEntity<Boolean> save(@RequestBody Im_livechat_report_channelDTO im_livechat_report_channeldto) {
        return ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_channelService.save(im_livechat_report_channelMapping.toDomain(im_livechat_report_channeldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-Save-all')")
    @ApiOperation(value = "批量保存实时聊天支持频道报告", tags = {"实时聊天支持频道报告" },  notes = "批量保存实时聊天支持频道报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Im_livechat_report_channelDTO> im_livechat_report_channeldtos) {
        im_livechat_report_channelService.saveBatch(im_livechat_report_channelMapping.toDomain(im_livechat_report_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"实时聊天支持频道报告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_report_channels/fetchdefault")
	public ResponseEntity<List<Im_livechat_report_channelDTO>> fetchDefault(Im_livechat_report_channelSearchContext context) {
        Page<Im_livechat_report_channel> domains = im_livechat_report_channelService.searchDefault(context) ;
        List<Im_livechat_report_channelDTO> list = im_livechat_report_channelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_channel-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"实时聊天支持频道报告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/im_livechat_report_channels/searchdefault")
	public ResponseEntity<Page<Im_livechat_report_channelDTO>> searchDefault(@RequestBody Im_livechat_report_channelSearchContext context) {
        Page<Im_livechat_report_channel> domains = im_livechat_report_channelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(im_livechat_report_channelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

