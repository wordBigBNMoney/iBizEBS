package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Gamification_badge_userDTO]
 */
@Data
public class Gamification_badge_userDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [COMMENT]
     *
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String comment;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [SENDER_ID_TEXT]
     *
     */
    @JSONField(name = "sender_id_text")
    @JsonProperty("sender_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String senderIdText;

    /**
     * 属性 [LEVEL]
     *
     */
    @JSONField(name = "level")
    @JsonProperty("level")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String level;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [BADGE_NAME]
     *
     */
    @JSONField(name = "badge_name")
    @JsonProperty("badge_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String badgeName;

    /**
     * 属性 [CHALLENGE_ID_TEXT]
     *
     */
    @JSONField(name = "challenge_id_text")
    @JsonProperty("challenge_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String challengeIdText;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String employeeIdText;

    /**
     * 属性 [SENDER_ID]
     *
     */
    @JSONField(name = "sender_id")
    @JsonProperty("sender_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long senderId;

    /**
     * 属性 [BADGE_ID]
     *
     */
    @JSONField(name = "badge_id")
    @JsonProperty("badge_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[徽章]不允许为空!")
    private Long badgeId;

    /**
     * 属性 [CHALLENGE_ID]
     *
     */
    @JSONField(name = "challenge_id")
    @JsonProperty("challenge_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long challengeId;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long employeeId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[用户]不允许为空!")
    private Long userId;


    /**
     * 设置 [COMMENT]
     */
    public void setComment(String  comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }

    /**
     * 设置 [SENDER_ID]
     */
    public void setSenderId(Long  senderId){
        this.senderId = senderId ;
        this.modify("sender_id",senderId);
    }

    /**
     * 设置 [BADGE_ID]
     */
    public void setBadgeId(Long  badgeId){
        this.badgeId = badgeId ;
        this.modify("badge_id",badgeId);
    }

    /**
     * 设置 [CHALLENGE_ID]
     */
    public void setChallengeId(Long  challengeId){
        this.challengeId = challengeId ;
        this.modify("challenge_id",challengeId);
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    public void setEmployeeId(Long  employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


}


