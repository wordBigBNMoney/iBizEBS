package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Stock_moveDTO]
 */
@Data
public class Stock_moveDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RETURNED_MOVE_IDS]
     *
     */
    @JSONField(name = "returned_move_ids")
    @JsonProperty("returned_move_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String returnedMoveIds;

    /**
     * 属性 [ADDITIONAL]
     *
     */
    @JSONField(name = "additional")
    @JsonProperty("additional")
    private Boolean additional;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;

    /**
     * 属性 [SCRAP_IDS]
     *
     */
    @JSONField(name = "scrap_ids")
    @JsonProperty("scrap_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String scrapIds;

    /**
     * 属性 [ACCOUNT_MOVE_IDS]
     *
     */
    @JSONField(name = "account_move_ids")
    @JsonProperty("account_move_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String accountMoveIds;

    /**
     * 属性 [MOVE_DEST_IDS]
     *
     */
    @JSONField(name = "move_dest_ids")
    @JsonProperty("move_dest_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String moveDestIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [TO_REFUND]
     *
     */
    @JSONField(name = "to_refund")
    @JsonProperty("to_refund")
    private Boolean toRefund;

    /**
     * 属性 [UNIT_FACTOR]
     *
     */
    @JSONField(name = "unit_factor")
    @JsonProperty("unit_factor")
    private Double unitFactor;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [AVAILABILITY]
     *
     */
    @JSONField(name = "availability")
    @JsonProperty("availability")
    private Double availability;

    /**
     * 属性 [REMAINING_QTY]
     *
     */
    @JSONField(name = "remaining_qty")
    @JsonProperty("remaining_qty")
    private Double remainingQty;

    /**
     * 属性 [PROPAGATE]
     *
     */
    @JSONField(name = "propagate")
    @JsonProperty("propagate")
    private Boolean propagate;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String reference;

    /**
     * 属性 [ACTIVE_MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "active_move_line_ids")
    @JsonProperty("active_move_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activeMoveLineIds;

    /**
     * 属性 [MOVE_LINE_NOSUGGEST_IDS]
     *
     */
    @JSONField(name = "move_line_nosuggest_ids")
    @JsonProperty("move_line_nosuggest_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String moveLineNosuggestIds;

    /**
     * 属性 [SHOW_DETAILS_VISIBLE]
     *
     */
    @JSONField(name = "show_details_visible")
    @JsonProperty("show_details_visible")
    private Boolean showDetailsVisible;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String origin;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    @NotNull(message = "[初始需求]不允许为空!")
    private Double productUomQty;

    /**
     * 属性 [NEEDS_LOTS]
     *
     */
    @JSONField(name = "needs_lots")
    @JsonProperty("needs_lots")
    private Boolean needsLots;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String priority;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    @NotNull(message = "[日期]不允许为空!")
    private Timestamp date;

    /**
     * 属性 [REMAINING_VALUE]
     *
     */
    @JSONField(name = "remaining_value")
    @JsonProperty("remaining_value")
    private Double remainingValue;

    /**
     * 属性 [ORDER_FINISHED_LOT_IDS]
     *
     */
    @JSONField(name = "order_finished_lot_ids")
    @JsonProperty("order_finished_lot_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String orderFinishedLotIds;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MOVE_ORIG_IDS]
     *
     */
    @JSONField(name = "move_orig_ids")
    @JsonProperty("move_orig_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String moveOrigIds;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String moveLineIds;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String note;

    /**
     * 属性 [RESERVED_AVAILABILITY]
     *
     */
    @JSONField(name = "reserved_availability")
    @JsonProperty("reserved_availability")
    private Double reservedAvailability;

    /**
     * 属性 [IS_INITIAL_DEMAND_EDITABLE]
     *
     */
    @JSONField(name = "is_initial_demand_editable")
    @JsonProperty("is_initial_demand_editable")
    private Boolean isInitialDemandEditable;

    /**
     * 属性 [QUANTITY_DONE]
     *
     */
    @JSONField(name = "quantity_done")
    @JsonProperty("quantity_done")
    private Double quantityDone;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private Boolean isLocked;

    /**
     * 属性 [STRING_AVAILABILITY_INFO]
     *
     */
    @JSONField(name = "string_availability_info")
    @JsonProperty("string_availability_info")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String stringAvailabilityInfo;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [SHOW_RESERVED_AVAILABILITY]
     *
     */
    @JSONField(name = "show_reserved_availability")
    @JsonProperty("show_reserved_availability")
    private Boolean showReservedAvailability;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String routeIds;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[说明]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [VALUE]
     *
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private Double value;

    /**
     * 属性 [FINISHED_LOTS_EXIST]
     *
     */
    @JSONField(name = "finished_lots_exist")
    @JsonProperty("finished_lots_exist")
    private Boolean finishedLotsExist;

    /**
     * 属性 [IS_DONE]
     *
     */
    @JSONField(name = "is_done")
    @JsonProperty("is_done")
    private Boolean isDone;

    /**
     * 属性 [IS_QUANTITY_DONE_EDITABLE]
     *
     */
    @JSONField(name = "is_quantity_done_editable")
    @JsonProperty("is_quantity_done_editable")
    private Boolean isQuantityDoneEditable;

    /**
     * 属性 [HAS_MOVE_LINES]
     *
     */
    @JSONField(name = "has_move_lines")
    @JsonProperty("has_move_lines")
    private Boolean hasMoveLines;

    /**
     * 属性 [DATE_EXPECTED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_expected" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_expected")
    @NotNull(message = "[预计日期]不允许为空!")
    private Timestamp dateExpected;

    /**
     * 属性 [PROCURE_METHOD]
     *
     */
    @JSONField(name = "procure_method")
    @JsonProperty("procure_method")
    @NotBlank(message = "[供应方法]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String procureMethod;

    /**
     * 属性 [PRODUCT_TYPE]
     *
     */
    @JSONField(name = "product_type")
    @JsonProperty("product_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productType;

    /**
     * 属性 [UNBUILD_ID_TEXT]
     *
     */
    @JSONField(name = "unbuild_id_text")
    @JsonProperty("unbuild_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String unbuildIdText;

    /**
     * 属性 [SCRAPPED]
     *
     */
    @JSONField(name = "scrapped")
    @JsonProperty("scrapped")
    private Boolean scrapped;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productionIdText;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String warehouseIdText;

    /**
     * 属性 [SHOW_OPERATIONS]
     *
     */
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private Boolean showOperations;

    /**
     * 属性 [CREATED_PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "created_production_id_text")
    @JsonProperty("created_production_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createdProductionIdText;

    /**
     * 属性 [CREATED_PURCHASE_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "created_purchase_line_id_text")
    @JsonProperty("created_purchase_line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createdPurchaseLineIdText;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String locationDestIdText;

    /**
     * 属性 [HAS_TRACKING]
     *
     */
    @JSONField(name = "has_tracking")
    @JsonProperty("has_tracking")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String hasTracking;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingTypeIdText;

    /**
     * 属性 [SALE_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "sale_line_id_text")
    @JsonProperty("sale_line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleLineIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [PICKING_PARTNER_ID]
     *
     */
    @JSONField(name = "picking_partner_id")
    @JsonProperty("picking_partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pickingPartnerId;

    /**
     * 属性 [RAW_MATERIAL_PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "raw_material_production_id_text")
    @JsonProperty("raw_material_production_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rawMaterialProductionIdText;

    /**
     * 属性 [ORIGIN_RETURNED_MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "origin_returned_move_id_text")
    @JsonProperty("origin_returned_move_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String originReturnedMoveIdText;

    /**
     * 属性 [REPAIR_ID_TEXT]
     *
     */
    @JSONField(name = "repair_id_text")
    @JsonProperty("repair_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String repairIdText;

    /**
     * 属性 [BACKORDER_ID]
     *
     */
    @JSONField(name = "backorder_id")
    @JsonProperty("backorder_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long backorderId;

    /**
     * 属性 [PURCHASE_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "purchase_line_id_text")
    @JsonProperty("purchase_line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purchaseLineIdText;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String locationIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productUomText;

    /**
     * 属性 [OPERATION_ID_TEXT]
     *
     */
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String operationIdText;

    /**
     * 属性 [WORKORDER_ID_TEXT]
     *
     */
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String workorderIdText;

    /**
     * 属性 [CONSUME_UNBUILD_ID_TEXT]
     *
     */
    @JSONField(name = "consume_unbuild_id_text")
    @JsonProperty("consume_unbuild_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String consumeUnbuildIdText;

    /**
     * 属性 [PRODUCT_PACKAGING_TEXT]
     *
     */
    @JSONField(name = "product_packaging_text")
    @JsonProperty("product_packaging_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productPackagingText;

    /**
     * 属性 [RESTRICT_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "restrict_partner_id_text")
    @JsonProperty("restrict_partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String restrictPartnerIdText;

    /**
     * 属性 [INVENTORY_ID_TEXT]
     *
     */
    @JSONField(name = "inventory_id_text")
    @JsonProperty("inventory_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String inventoryIdText;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productTmplId;

    /**
     * 属性 [RULE_ID_TEXT]
     *
     */
    @JSONField(name = "rule_id_text")
    @JsonProperty("rule_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String ruleIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PICKING_CODE]
     *
     */
    @JSONField(name = "picking_code")
    @JsonProperty("picking_code")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingCode;

    /**
     * 属性 [PICKING_TYPE_ENTIRE_PACKS]
     *
     */
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private Boolean pickingTypeEntirePacks;

    /**
     * 属性 [CREATED_PRODUCTION_ID]
     *
     */
    @JSONField(name = "created_production_id")
    @JsonProperty("created_production_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createdProductionId;

    /**
     * 属性 [CONSUME_UNBUILD_ID]
     *
     */
    @JSONField(name = "consume_unbuild_id")
    @JsonProperty("consume_unbuild_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long consumeUnbuildId;

    /**
     * 属性 [SALE_LINE_ID]
     *
     */
    @JSONField(name = "sale_line_id")
    @JsonProperty("sale_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long saleLineId;

    /**
     * 属性 [ORIGIN_RETURNED_MOVE_ID]
     *
     */
    @JSONField(name = "origin_returned_move_id")
    @JsonProperty("origin_returned_move_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long originReturnedMoveId;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pickingTypeId;

    /**
     * 属性 [OPERATION_ID]
     *
     */
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long operationId;

    /**
     * 属性 [BOM_LINE_ID]
     *
     */
    @JSONField(name = "bom_line_id")
    @JsonProperty("bom_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long bomLineId;

    /**
     * 属性 [REPAIR_ID]
     *
     */
    @JSONField(name = "repair_id")
    @JsonProperty("repair_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long repairId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[产品]不允许为空!")
    private Long productId;

    /**
     * 属性 [UNBUILD_ID]
     *
     */
    @JSONField(name = "unbuild_id")
    @JsonProperty("unbuild_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long unbuildId;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productionId;

    /**
     * 属性 [CREATED_PURCHASE_LINE_ID]
     *
     */
    @JSONField(name = "created_purchase_line_id")
    @JsonProperty("created_purchase_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createdPurchaseLineId;

    /**
     * 属性 [WORKORDER_ID]
     *
     */
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long workorderId;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[单位]不允许为空!")
    private Long productUom;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pickingId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PACKAGE_LEVEL_ID]
     *
     */
    @JSONField(name = "package_level_id")
    @JsonProperty("package_level_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long packageLevelId;

    /**
     * 属性 [PRODUCT_PACKAGING]
     *
     */
    @JSONField(name = "product_packaging")
    @JsonProperty("product_packaging")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productPackaging;

    /**
     * 属性 [RESTRICT_PARTNER_ID]
     *
     */
    @JSONField(name = "restrict_partner_id")
    @JsonProperty("restrict_partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long restrictPartnerId;

    /**
     * 属性 [RULE_ID]
     *
     */
    @JSONField(name = "rule_id")
    @JsonProperty("rule_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long ruleId;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long warehouseId;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[目的位置]不允许为空!")
    private Long locationDestId;

    /**
     * 属性 [RAW_MATERIAL_PRODUCTION_ID]
     *
     */
    @JSONField(name = "raw_material_production_id")
    @JsonProperty("raw_material_production_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long rawMaterialProductionId;

    /**
     * 属性 [INVENTORY_ID]
     *
     */
    @JSONField(name = "inventory_id")
    @JsonProperty("inventory_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inventoryId;

    /**
     * 属性 [PURCHASE_LINE_ID]
     *
     */
    @JSONField(name = "purchase_line_id")
    @JsonProperty("purchase_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaseLineId;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[源位置]不允许为空!")
    private Long locationId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [ADDITIONAL]
     */
    public void setAdditional(Boolean  additional){
        this.additional = additional ;
        this.modify("additional",additional);
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    public void setPriceUnit(Double  priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [GROUP_ID]
     */
    public void setGroupId(Integer  groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }

    /**
     * 设置 [TO_REFUND]
     */
    public void setToRefund(Boolean  toRefund){
        this.toRefund = toRefund ;
        this.modify("to_refund",toRefund);
    }

    /**
     * 设置 [UNIT_FACTOR]
     */
    public void setUnitFactor(Double  unitFactor){
        this.unitFactor = unitFactor ;
        this.modify("unit_factor",unitFactor);
    }

    /**
     * 设置 [REMAINING_QTY]
     */
    public void setRemainingQty(Double  remainingQty){
        this.remainingQty = remainingQty ;
        this.modify("remaining_qty",remainingQty);
    }

    /**
     * 设置 [PROPAGATE]
     */
    public void setPropagate(Boolean  propagate){
        this.propagate = propagate ;
        this.modify("propagate",propagate);
    }

    /**
     * 设置 [REFERENCE]
     */
    public void setReference(String  reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    public void setProductUomQty(Double  productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [REMAINING_VALUE]
     */
    public void setRemainingValue(Double  remainingValue){
        this.remainingValue = remainingValue ;
        this.modify("remaining_value",remainingValue);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [VALUE]
     */
    public void setValue(Double  value){
        this.value = value ;
        this.modify("value",value);
    }

    /**
     * 设置 [IS_DONE]
     */
    public void setIsDone(Boolean  isDone){
        this.isDone = isDone ;
        this.modify("is_done",isDone);
    }

    /**
     * 设置 [DATE_EXPECTED]
     */
    public void setDateExpected(Timestamp  dateExpected){
        this.dateExpected = dateExpected ;
        this.modify("date_expected",dateExpected);
    }

    /**
     * 设置 [PROCURE_METHOD]
     */
    public void setProcureMethod(String  procureMethod){
        this.procureMethod = procureMethod ;
        this.modify("procure_method",procureMethod);
    }

    /**
     * 设置 [CREATED_PRODUCTION_ID]
     */
    public void setCreatedProductionId(Long  createdProductionId){
        this.createdProductionId = createdProductionId ;
        this.modify("created_production_id",createdProductionId);
    }

    /**
     * 设置 [CONSUME_UNBUILD_ID]
     */
    public void setConsumeUnbuildId(Long  consumeUnbuildId){
        this.consumeUnbuildId = consumeUnbuildId ;
        this.modify("consume_unbuild_id",consumeUnbuildId);
    }

    /**
     * 设置 [SALE_LINE_ID]
     */
    public void setSaleLineId(Long  saleLineId){
        this.saleLineId = saleLineId ;
        this.modify("sale_line_id",saleLineId);
    }

    /**
     * 设置 [ORIGIN_RETURNED_MOVE_ID]
     */
    public void setOriginReturnedMoveId(Long  originReturnedMoveId){
        this.originReturnedMoveId = originReturnedMoveId ;
        this.modify("origin_returned_move_id",originReturnedMoveId);
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    public void setPickingTypeId(Long  pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [OPERATION_ID]
     */
    public void setOperationId(Long  operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }

    /**
     * 设置 [BOM_LINE_ID]
     */
    public void setBomLineId(Long  bomLineId){
        this.bomLineId = bomLineId ;
        this.modify("bom_line_id",bomLineId);
    }

    /**
     * 设置 [REPAIR_ID]
     */
    public void setRepairId(Long  repairId){
        this.repairId = repairId ;
        this.modify("repair_id",repairId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [UNBUILD_ID]
     */
    public void setUnbuildId(Long  unbuildId){
        this.unbuildId = unbuildId ;
        this.modify("unbuild_id",unbuildId);
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    public void setProductionId(Long  productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }

    /**
     * 设置 [CREATED_PURCHASE_LINE_ID]
     */
    public void setCreatedPurchaseLineId(Long  createdPurchaseLineId){
        this.createdPurchaseLineId = createdPurchaseLineId ;
        this.modify("created_purchase_line_id",createdPurchaseLineId);
    }

    /**
     * 设置 [WORKORDER_ID]
     */
    public void setWorkorderId(Long  workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    public void setProductUom(Long  productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [PICKING_ID]
     */
    public void setPickingId(Long  pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [PACKAGE_LEVEL_ID]
     */
    public void setPackageLevelId(Long  packageLevelId){
        this.packageLevelId = packageLevelId ;
        this.modify("package_level_id",packageLevelId);
    }

    /**
     * 设置 [PRODUCT_PACKAGING]
     */
    public void setProductPackaging(Long  productPackaging){
        this.productPackaging = productPackaging ;
        this.modify("product_packaging",productPackaging);
    }

    /**
     * 设置 [RESTRICT_PARTNER_ID]
     */
    public void setRestrictPartnerId(Long  restrictPartnerId){
        this.restrictPartnerId = restrictPartnerId ;
        this.modify("restrict_partner_id",restrictPartnerId);
    }

    /**
     * 设置 [RULE_ID]
     */
    public void setRuleId(Long  ruleId){
        this.ruleId = ruleId ;
        this.modify("rule_id",ruleId);
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    public void setWarehouseId(Long  warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    public void setLocationDestId(Long  locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }

    /**
     * 设置 [RAW_MATERIAL_PRODUCTION_ID]
     */
    public void setRawMaterialProductionId(Long  rawMaterialProductionId){
        this.rawMaterialProductionId = rawMaterialProductionId ;
        this.modify("raw_material_production_id",rawMaterialProductionId);
    }

    /**
     * 设置 [INVENTORY_ID]
     */
    public void setInventoryId(Long  inventoryId){
        this.inventoryId = inventoryId ;
        this.modify("inventory_id",inventoryId);
    }

    /**
     * 设置 [PURCHASE_LINE_ID]
     */
    public void setPurchaseLineId(Long  purchaseLineId){
        this.purchaseLineId = purchaseLineId ;
        this.modify("purchase_line_id",purchaseLineId);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Long  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }


}


