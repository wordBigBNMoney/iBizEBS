package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_skill_type;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_skill_typeService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_skill_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"技能类型" })
@RestController("Core-hr_skill_type")
@RequestMapping("")
public class Hr_skill_typeResource {

    @Autowired
    public IHr_skill_typeService hr_skill_typeService;

    @Autowired
    @Lazy
    public Hr_skill_typeMapping hr_skill_typeMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Create-all')")
    @ApiOperation(value = "新建技能类型", tags = {"技能类型" },  notes = "新建技能类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types")
    public ResponseEntity<Hr_skill_typeDTO> create(@Validated @RequestBody Hr_skill_typeDTO hr_skill_typedto) {
        Hr_skill_type domain = hr_skill_typeMapping.toDomain(hr_skill_typedto);
		hr_skill_typeService.create(domain);
        Hr_skill_typeDTO dto = hr_skill_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Create-all')")
    @ApiOperation(value = "批量新建技能类型", tags = {"技能类型" },  notes = "批量新建技能类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_skill_typeDTO> hr_skill_typedtos) {
        hr_skill_typeService.createBatch(hr_skill_typeMapping.toDomain(hr_skill_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Update-all')")
    @ApiOperation(value = "更新技能类型", tags = {"技能类型" },  notes = "更新技能类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skill_types/{hr_skill_type_id}")
    public ResponseEntity<Hr_skill_typeDTO> update(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skill_typeDTO hr_skill_typedto) {
		Hr_skill_type domain  = hr_skill_typeMapping.toDomain(hr_skill_typedto);
        domain .setId(hr_skill_type_id);
		hr_skill_typeService.update(domain );
		Hr_skill_typeDTO dto = hr_skill_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Update-all')")
    @ApiOperation(value = "批量更新技能类型", tags = {"技能类型" },  notes = "批量更新技能类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skill_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_skill_typeDTO> hr_skill_typedtos) {
        hr_skill_typeService.updateBatch(hr_skill_typeMapping.toDomain(hr_skill_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Remove-all')")
    @ApiOperation(value = "删除技能类型", tags = {"技能类型" },  notes = "删除技能类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skill_types/{hr_skill_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_skill_type_id") Long hr_skill_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_skill_typeService.remove(hr_skill_type_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Remove-all')")
    @ApiOperation(value = "批量删除技能类型", tags = {"技能类型" },  notes = "批量删除技能类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skill_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_skill_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Get-all')")
    @ApiOperation(value = "获取技能类型", tags = {"技能类型" },  notes = "获取技能类型")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_skill_types/{hr_skill_type_id}")
    public ResponseEntity<Hr_skill_typeDTO> get(@PathVariable("hr_skill_type_id") Long hr_skill_type_id) {
        Hr_skill_type domain = hr_skill_typeService.get(hr_skill_type_id);
        Hr_skill_typeDTO dto = hr_skill_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取技能类型草稿", tags = {"技能类型" },  notes = "获取技能类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_skill_types/getdraft")
    public ResponseEntity<Hr_skill_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_skill_typeMapping.toDto(hr_skill_typeService.getDraft(new Hr_skill_type())));
    }

    @ApiOperation(value = "检查技能类型", tags = {"技能类型" },  notes = "检查技能类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_skill_typeDTO hr_skill_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_skill_typeService.checkKey(hr_skill_typeMapping.toDomain(hr_skill_typedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Save-all')")
    @ApiOperation(value = "保存技能类型", tags = {"技能类型" },  notes = "保存技能类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_skill_typeDTO hr_skill_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_skill_typeService.save(hr_skill_typeMapping.toDomain(hr_skill_typedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-Save-all')")
    @ApiOperation(value = "批量保存技能类型", tags = {"技能类型" },  notes = "批量保存技能类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_skill_typeDTO> hr_skill_typedtos) {
        hr_skill_typeService.saveBatch(hr_skill_typeMapping.toDomain(hr_skill_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"技能类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_skill_types/fetchdefault")
	public ResponseEntity<List<Hr_skill_typeDTO>> fetchDefault(Hr_skill_typeSearchContext context) {
        Page<Hr_skill_type> domains = hr_skill_typeService.searchDefault(context) ;
        List<Hr_skill_typeDTO> list = hr_skill_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_type-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"技能类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_skill_types/searchdefault")
	public ResponseEntity<Page<Hr_skill_typeDTO>> searchDefault(@RequestBody Hr_skill_typeSearchContext context) {
        Page<Hr_skill_type> domains = hr_skill_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_skill_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

