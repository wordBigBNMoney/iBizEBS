package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_note.domain.Note_stage;
import cn.ibizlab.businesscentral.core.odoo_note.service.INote_stageService;
import cn.ibizlab.businesscentral.core.odoo_note.filter.Note_stageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"便签阶段" })
@RestController("Core-note_stage")
@RequestMapping("")
public class Note_stageResource {

    @Autowired
    public INote_stageService note_stageService;

    @Autowired
    @Lazy
    public Note_stageMapping note_stageMapping;

    @PreAuthorize("hasPermission(this.note_stageMapping.toDomain(#note_stagedto),'iBizBusinessCentral-Note_stage-Create')")
    @ApiOperation(value = "新建便签阶段", tags = {"便签阶段" },  notes = "新建便签阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/note_stages")
    public ResponseEntity<Note_stageDTO> create(@Validated @RequestBody Note_stageDTO note_stagedto) {
        Note_stage domain = note_stageMapping.toDomain(note_stagedto);
		note_stageService.create(domain);
        Note_stageDTO dto = note_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.note_stageMapping.toDomain(#note_stagedtos),'iBizBusinessCentral-Note_stage-Create')")
    @ApiOperation(value = "批量新建便签阶段", tags = {"便签阶段" },  notes = "批量新建便签阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/note_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Note_stageDTO> note_stagedtos) {
        note_stageService.createBatch(note_stageMapping.toDomain(note_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "note_stage" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.note_stageService.get(#note_stage_id),'iBizBusinessCentral-Note_stage-Update')")
    @ApiOperation(value = "更新便签阶段", tags = {"便签阶段" },  notes = "更新便签阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_stages/{note_stage_id}")
    public ResponseEntity<Note_stageDTO> update(@PathVariable("note_stage_id") Long note_stage_id, @RequestBody Note_stageDTO note_stagedto) {
		Note_stage domain  = note_stageMapping.toDomain(note_stagedto);
        domain .setId(note_stage_id);
		note_stageService.update(domain );
		Note_stageDTO dto = note_stageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.note_stageService.getNoteStageByEntities(this.note_stageMapping.toDomain(#note_stagedtos)),'iBizBusinessCentral-Note_stage-Update')")
    @ApiOperation(value = "批量更新便签阶段", tags = {"便签阶段" },  notes = "批量更新便签阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_stageDTO> note_stagedtos) {
        note_stageService.updateBatch(note_stageMapping.toDomain(note_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.note_stageService.get(#note_stage_id),'iBizBusinessCentral-Note_stage-Remove')")
    @ApiOperation(value = "删除便签阶段", tags = {"便签阶段" },  notes = "删除便签阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_stages/{note_stage_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("note_stage_id") Long note_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(note_stageService.remove(note_stage_id));
    }

    @PreAuthorize("hasPermission(this.note_stageService.getNoteStageByIds(#ids),'iBizBusinessCentral-Note_stage-Remove')")
    @ApiOperation(value = "批量删除便签阶段", tags = {"便签阶段" },  notes = "批量删除便签阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        note_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.note_stageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Note_stage-Get')")
    @ApiOperation(value = "获取便签阶段", tags = {"便签阶段" },  notes = "获取便签阶段")
	@RequestMapping(method = RequestMethod.GET, value = "/note_stages/{note_stage_id}")
    public ResponseEntity<Note_stageDTO> get(@PathVariable("note_stage_id") Long note_stage_id) {
        Note_stage domain = note_stageService.get(note_stage_id);
        Note_stageDTO dto = note_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取便签阶段草稿", tags = {"便签阶段" },  notes = "获取便签阶段草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/note_stages/getdraft")
    public ResponseEntity<Note_stageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(note_stageMapping.toDto(note_stageService.getDraft(new Note_stage())));
    }

    @ApiOperation(value = "检查便签阶段", tags = {"便签阶段" },  notes = "检查便签阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/note_stages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Note_stageDTO note_stagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(note_stageService.checkKey(note_stageMapping.toDomain(note_stagedto)));
    }

    @PreAuthorize("hasPermission(this.note_stageMapping.toDomain(#note_stagedto),'iBizBusinessCentral-Note_stage-Save')")
    @ApiOperation(value = "保存便签阶段", tags = {"便签阶段" },  notes = "保存便签阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/note_stages/save")
    public ResponseEntity<Boolean> save(@RequestBody Note_stageDTO note_stagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(note_stageService.save(note_stageMapping.toDomain(note_stagedto)));
    }

    @PreAuthorize("hasPermission(this.note_stageMapping.toDomain(#note_stagedtos),'iBizBusinessCentral-Note_stage-Save')")
    @ApiOperation(value = "批量保存便签阶段", tags = {"便签阶段" },  notes = "批量保存便签阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/note_stages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Note_stageDTO> note_stagedtos) {
        note_stageService.saveBatch(note_stageMapping.toDomain(note_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Note_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Note_stage-Get')")
	@ApiOperation(value = "获取数据集", tags = {"便签阶段" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/note_stages/fetchdefault")
	public ResponseEntity<List<Note_stageDTO>> fetchDefault(Note_stageSearchContext context) {
        Page<Note_stage> domains = note_stageService.searchDefault(context) ;
        List<Note_stageDTO> list = note_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Note_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Note_stage-Get')")
	@ApiOperation(value = "查询数据集", tags = {"便签阶段" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/note_stages/searchdefault")
	public ResponseEntity<Page<Note_stageDTO>> searchDefault(@RequestBody Note_stageSearchContext context) {
        Page<Note_stage> domains = note_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(note_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

