package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_degreeService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"申请人学历" })
@RestController("Core-hr_recruitment_degree")
@RequestMapping("")
public class Hr_recruitment_degreeResource {

    @Autowired
    public IHr_recruitment_degreeService hr_recruitment_degreeService;

    @Autowired
    @Lazy
    public Hr_recruitment_degreeMapping hr_recruitment_degreeMapping;

    @PreAuthorize("hasPermission(this.hr_recruitment_degreeMapping.toDomain(#hr_recruitment_degreedto),'iBizBusinessCentral-Hr_recruitment_degree-Create')")
    @ApiOperation(value = "新建申请人学历", tags = {"申请人学历" },  notes = "新建申请人学历")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees")
    public ResponseEntity<Hr_recruitment_degreeDTO> create(@Validated @RequestBody Hr_recruitment_degreeDTO hr_recruitment_degreedto) {
        Hr_recruitment_degree domain = hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedto);
		hr_recruitment_degreeService.create(domain);
        Hr_recruitment_degreeDTO dto = hr_recruitment_degreeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_degreeMapping.toDomain(#hr_recruitment_degreedtos),'iBizBusinessCentral-Hr_recruitment_degree-Create')")
    @ApiOperation(value = "批量新建申请人学历", tags = {"申请人学历" },  notes = "批量新建申请人学历")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_recruitment_degreeDTO> hr_recruitment_degreedtos) {
        hr_recruitment_degreeService.createBatch(hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_recruitment_degree" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_recruitment_degreeService.get(#hr_recruitment_degree_id),'iBizBusinessCentral-Hr_recruitment_degree-Update')")
    @ApiOperation(value = "更新申请人学历", tags = {"申请人学历" },  notes = "更新申请人学历")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_degrees/{hr_recruitment_degree_id}")
    public ResponseEntity<Hr_recruitment_degreeDTO> update(@PathVariable("hr_recruitment_degree_id") Long hr_recruitment_degree_id, @RequestBody Hr_recruitment_degreeDTO hr_recruitment_degreedto) {
		Hr_recruitment_degree domain  = hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedto);
        domain .setId(hr_recruitment_degree_id);
		hr_recruitment_degreeService.update(domain );
		Hr_recruitment_degreeDTO dto = hr_recruitment_degreeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_degreeService.getHrRecruitmentDegreeByEntities(this.hr_recruitment_degreeMapping.toDomain(#hr_recruitment_degreedtos)),'iBizBusinessCentral-Hr_recruitment_degree-Update')")
    @ApiOperation(value = "批量更新申请人学历", tags = {"申请人学历" },  notes = "批量更新申请人学历")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_degrees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_degreeDTO> hr_recruitment_degreedtos) {
        hr_recruitment_degreeService.updateBatch(hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_degreeService.get(#hr_recruitment_degree_id),'iBizBusinessCentral-Hr_recruitment_degree-Remove')")
    @ApiOperation(value = "删除申请人学历", tags = {"申请人学历" },  notes = "删除申请人学历")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_degrees/{hr_recruitment_degree_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_degree_id") Long hr_recruitment_degree_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_degreeService.remove(hr_recruitment_degree_id));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_degreeService.getHrRecruitmentDegreeByIds(#ids),'iBizBusinessCentral-Hr_recruitment_degree-Remove')")
    @ApiOperation(value = "批量删除申请人学历", tags = {"申请人学历" },  notes = "批量删除申请人学历")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_degrees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_recruitment_degreeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_recruitment_degreeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_recruitment_degree-Get')")
    @ApiOperation(value = "获取申请人学历", tags = {"申请人学历" },  notes = "获取申请人学历")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_degrees/{hr_recruitment_degree_id}")
    public ResponseEntity<Hr_recruitment_degreeDTO> get(@PathVariable("hr_recruitment_degree_id") Long hr_recruitment_degree_id) {
        Hr_recruitment_degree domain = hr_recruitment_degreeService.get(hr_recruitment_degree_id);
        Hr_recruitment_degreeDTO dto = hr_recruitment_degreeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取申请人学历草稿", tags = {"申请人学历" },  notes = "获取申请人学历草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_degrees/getdraft")
    public ResponseEntity<Hr_recruitment_degreeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_degreeMapping.toDto(hr_recruitment_degreeService.getDraft(new Hr_recruitment_degree())));
    }

    @ApiOperation(value = "检查申请人学历", tags = {"申请人学历" },  notes = "检查申请人学历")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_recruitment_degreeDTO hr_recruitment_degreedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_degreeService.checkKey(hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedto)));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_degreeMapping.toDomain(#hr_recruitment_degreedto),'iBizBusinessCentral-Hr_recruitment_degree-Save')")
    @ApiOperation(value = "保存申请人学历", tags = {"申请人学历" },  notes = "保存申请人学历")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_recruitment_degreeDTO hr_recruitment_degreedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_degreeService.save(hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedto)));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_degreeMapping.toDomain(#hr_recruitment_degreedtos),'iBizBusinessCentral-Hr_recruitment_degree-Save')")
    @ApiOperation(value = "批量保存申请人学历", tags = {"申请人学历" },  notes = "批量保存申请人学历")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_recruitment_degreeDTO> hr_recruitment_degreedtos) {
        hr_recruitment_degreeService.saveBatch(hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_recruitment_degree-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_recruitment_degree-Get')")
	@ApiOperation(value = "获取数据集", tags = {"申请人学历" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_degrees/fetchdefault")
	public ResponseEntity<List<Hr_recruitment_degreeDTO>> fetchDefault(Hr_recruitment_degreeSearchContext context) {
        Page<Hr_recruitment_degree> domains = hr_recruitment_degreeService.searchDefault(context) ;
        List<Hr_recruitment_degreeDTO> list = hr_recruitment_degreeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_recruitment_degree-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_recruitment_degree-Get')")
	@ApiOperation(value = "查询数据集", tags = {"申请人学历" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_recruitment_degrees/searchdefault")
	public ResponseEntity<Page<Hr_recruitment_degreeDTO>> searchDefault(@RequestBody Hr_recruitment_degreeSearchContext context) {
        Page<Hr_recruitment_degree> domains = hr_recruitment_degreeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_recruitment_degreeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

