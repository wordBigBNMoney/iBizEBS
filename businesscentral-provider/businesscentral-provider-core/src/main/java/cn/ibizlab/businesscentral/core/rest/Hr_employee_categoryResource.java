package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employee_categoryService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employee_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"员工类别" })
@RestController("Core-hr_employee_category")
@RequestMapping("")
public class Hr_employee_categoryResource {

    @Autowired
    public IHr_employee_categoryService hr_employee_categoryService;

    @Autowired
    @Lazy
    public Hr_employee_categoryMapping hr_employee_categoryMapping;

    @PreAuthorize("hasPermission(this.hr_employee_categoryMapping.toDomain(#hr_employee_categorydto),'iBizBusinessCentral-Hr_employee_category-Create')")
    @ApiOperation(value = "新建员工类别", tags = {"员工类别" },  notes = "新建员工类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories")
    public ResponseEntity<Hr_employee_categoryDTO> create(@Validated @RequestBody Hr_employee_categoryDTO hr_employee_categorydto) {
        Hr_employee_category domain = hr_employee_categoryMapping.toDomain(hr_employee_categorydto);
		hr_employee_categoryService.create(domain);
        Hr_employee_categoryDTO dto = hr_employee_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_employee_categoryMapping.toDomain(#hr_employee_categorydtos),'iBizBusinessCentral-Hr_employee_category-Create')")
    @ApiOperation(value = "批量新建员工类别", tags = {"员工类别" },  notes = "批量新建员工类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_employee_categoryDTO> hr_employee_categorydtos) {
        hr_employee_categoryService.createBatch(hr_employee_categoryMapping.toDomain(hr_employee_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_employee_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_employee_categoryService.get(#hr_employee_category_id),'iBizBusinessCentral-Hr_employee_category-Update')")
    @ApiOperation(value = "更新员工类别", tags = {"员工类别" },  notes = "更新员工类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employee_categories/{hr_employee_category_id}")
    public ResponseEntity<Hr_employee_categoryDTO> update(@PathVariable("hr_employee_category_id") Long hr_employee_category_id, @RequestBody Hr_employee_categoryDTO hr_employee_categorydto) {
		Hr_employee_category domain  = hr_employee_categoryMapping.toDomain(hr_employee_categorydto);
        domain .setId(hr_employee_category_id);
		hr_employee_categoryService.update(domain );
		Hr_employee_categoryDTO dto = hr_employee_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_employee_categoryService.getHrEmployeeCategoryByEntities(this.hr_employee_categoryMapping.toDomain(#hr_employee_categorydtos)),'iBizBusinessCentral-Hr_employee_category-Update')")
    @ApiOperation(value = "批量更新员工类别", tags = {"员工类别" },  notes = "批量更新员工类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employee_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_employee_categoryDTO> hr_employee_categorydtos) {
        hr_employee_categoryService.updateBatch(hr_employee_categoryMapping.toDomain(hr_employee_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_employee_categoryService.get(#hr_employee_category_id),'iBizBusinessCentral-Hr_employee_category-Remove')")
    @ApiOperation(value = "删除员工类别", tags = {"员工类别" },  notes = "删除员工类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employee_categories/{hr_employee_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_employee_category_id") Long hr_employee_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_employee_categoryService.remove(hr_employee_category_id));
    }

    @PreAuthorize("hasPermission(this.hr_employee_categoryService.getHrEmployeeCategoryByIds(#ids),'iBizBusinessCentral-Hr_employee_category-Remove')")
    @ApiOperation(value = "批量删除员工类别", tags = {"员工类别" },  notes = "批量删除员工类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employee_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_employee_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_employee_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_employee_category-Get')")
    @ApiOperation(value = "获取员工类别", tags = {"员工类别" },  notes = "获取员工类别")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employee_categories/{hr_employee_category_id}")
    public ResponseEntity<Hr_employee_categoryDTO> get(@PathVariable("hr_employee_category_id") Long hr_employee_category_id) {
        Hr_employee_category domain = hr_employee_categoryService.get(hr_employee_category_id);
        Hr_employee_categoryDTO dto = hr_employee_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取员工类别草稿", tags = {"员工类别" },  notes = "获取员工类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employee_categories/getdraft")
    public ResponseEntity<Hr_employee_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_employee_categoryMapping.toDto(hr_employee_categoryService.getDraft(new Hr_employee_category())));
    }

    @ApiOperation(value = "检查员工类别", tags = {"员工类别" },  notes = "检查员工类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_employee_categoryDTO hr_employee_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_employee_categoryService.checkKey(hr_employee_categoryMapping.toDomain(hr_employee_categorydto)));
    }

    @PreAuthorize("hasPermission(this.hr_employee_categoryMapping.toDomain(#hr_employee_categorydto),'iBizBusinessCentral-Hr_employee_category-Save')")
    @ApiOperation(value = "保存员工类别", tags = {"员工类别" },  notes = "保存员工类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_employee_categoryDTO hr_employee_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_employee_categoryService.save(hr_employee_categoryMapping.toDomain(hr_employee_categorydto)));
    }

    @PreAuthorize("hasPermission(this.hr_employee_categoryMapping.toDomain(#hr_employee_categorydtos),'iBizBusinessCentral-Hr_employee_category-Save')")
    @ApiOperation(value = "批量保存员工类别", tags = {"员工类别" },  notes = "批量保存员工类别")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_employee_categoryDTO> hr_employee_categorydtos) {
        hr_employee_categoryService.saveBatch(hr_employee_categoryMapping.toDomain(hr_employee_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_employee_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"员工类别" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employee_categories/fetchdefault")
	public ResponseEntity<List<Hr_employee_categoryDTO>> fetchDefault(Hr_employee_categorySearchContext context) {
        Page<Hr_employee_category> domains = hr_employee_categoryService.searchDefault(context) ;
        List<Hr_employee_categoryDTO> list = hr_employee_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_employee_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"员工类别" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employee_categories/searchdefault")
	public ResponseEntity<Page<Hr_employee_categoryDTO>> searchDefault(@RequestBody Hr_employee_categorySearchContext context) {
        Page<Hr_employee_category> domains = hr_employee_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_employee_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

