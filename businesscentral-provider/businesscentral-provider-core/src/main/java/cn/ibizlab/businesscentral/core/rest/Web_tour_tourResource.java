package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.businesscentral.core.odoo_web_tour.service.IWeb_tour_tourService;
import cn.ibizlab.businesscentral.core.odoo_web_tour.filter.Web_tour_tourSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"向导" })
@RestController("Core-web_tour_tour")
@RequestMapping("")
public class Web_tour_tourResource {

    @Autowired
    public IWeb_tour_tourService web_tour_tourService;

    @Autowired
    @Lazy
    public Web_tour_tourMapping web_tour_tourMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Create-all')")
    @ApiOperation(value = "新建向导", tags = {"向导" },  notes = "新建向导")
	@RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours")
    public ResponseEntity<Web_tour_tourDTO> create(@Validated @RequestBody Web_tour_tourDTO web_tour_tourdto) {
        Web_tour_tour domain = web_tour_tourMapping.toDomain(web_tour_tourdto);
		web_tour_tourService.create(domain);
        Web_tour_tourDTO dto = web_tour_tourMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Create-all')")
    @ApiOperation(value = "批量新建向导", tags = {"向导" },  notes = "批量新建向导")
	@RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Web_tour_tourDTO> web_tour_tourdtos) {
        web_tour_tourService.createBatch(web_tour_tourMapping.toDomain(web_tour_tourdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Update-all')")
    @ApiOperation(value = "更新向导", tags = {"向导" },  notes = "更新向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_tour_tours/{web_tour_tour_id}")
    public ResponseEntity<Web_tour_tourDTO> update(@PathVariable("web_tour_tour_id") Long web_tour_tour_id, @RequestBody Web_tour_tourDTO web_tour_tourdto) {
		Web_tour_tour domain  = web_tour_tourMapping.toDomain(web_tour_tourdto);
        domain .setId(web_tour_tour_id);
		web_tour_tourService.update(domain );
		Web_tour_tourDTO dto = web_tour_tourMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Update-all')")
    @ApiOperation(value = "批量更新向导", tags = {"向导" },  notes = "批量更新向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_tour_tours/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_tour_tourDTO> web_tour_tourdtos) {
        web_tour_tourService.updateBatch(web_tour_tourMapping.toDomain(web_tour_tourdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Remove-all')")
    @ApiOperation(value = "删除向导", tags = {"向导" },  notes = "删除向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_tour_tours/{web_tour_tour_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("web_tour_tour_id") Long web_tour_tour_id) {
         return ResponseEntity.status(HttpStatus.OK).body(web_tour_tourService.remove(web_tour_tour_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Remove-all')")
    @ApiOperation(value = "批量删除向导", tags = {"向导" },  notes = "批量删除向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_tour_tours/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        web_tour_tourService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Get-all')")
    @ApiOperation(value = "获取向导", tags = {"向导" },  notes = "获取向导")
	@RequestMapping(method = RequestMethod.GET, value = "/web_tour_tours/{web_tour_tour_id}")
    public ResponseEntity<Web_tour_tourDTO> get(@PathVariable("web_tour_tour_id") Long web_tour_tour_id) {
        Web_tour_tour domain = web_tour_tourService.get(web_tour_tour_id);
        Web_tour_tourDTO dto = web_tour_tourMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取向导草稿", tags = {"向导" },  notes = "获取向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/web_tour_tours/getdraft")
    public ResponseEntity<Web_tour_tourDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(web_tour_tourMapping.toDto(web_tour_tourService.getDraft(new Web_tour_tour())));
    }

    @ApiOperation(value = "检查向导", tags = {"向导" },  notes = "检查向导")
	@RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Web_tour_tourDTO web_tour_tourdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(web_tour_tourService.checkKey(web_tour_tourMapping.toDomain(web_tour_tourdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Save-all')")
    @ApiOperation(value = "保存向导", tags = {"向导" },  notes = "保存向导")
	@RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/save")
    public ResponseEntity<Boolean> save(@RequestBody Web_tour_tourDTO web_tour_tourdto) {
        return ResponseEntity.status(HttpStatus.OK).body(web_tour_tourService.save(web_tour_tourMapping.toDomain(web_tour_tourdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-Save-all')")
    @ApiOperation(value = "批量保存向导", tags = {"向导" },  notes = "批量保存向导")
	@RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Web_tour_tourDTO> web_tour_tourdtos) {
        web_tour_tourService.saveBatch(web_tour_tourMapping.toDomain(web_tour_tourdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/web_tour_tours/fetchdefault")
	public ResponseEntity<List<Web_tour_tourDTO>> fetchDefault(Web_tour_tourSearchContext context) {
        Page<Web_tour_tour> domains = web_tour_tourService.searchDefault(context) ;
        List<Web_tour_tourDTO> list = web_tour_tourMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_tour_tour-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/web_tour_tours/searchdefault")
	public ResponseEntity<Page<Web_tour_tourDTO>> searchDefault(@RequestBody Web_tour_tourSearchContext context) {
        Page<Web_tour_tour> domains = web_tour_tourService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(web_tour_tourMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

