package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendarService;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendarSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"资源工作时间" })
@RestController("Core-resource_calendar")
@RequestMapping("")
public class Resource_calendarResource {

    @Autowired
    public IResource_calendarService resource_calendarService;

    @Autowired
    @Lazy
    public Resource_calendarMapping resource_calendarMapping;

    @PreAuthorize("hasPermission(this.resource_calendarMapping.toDomain(#resource_calendardto),'iBizBusinessCentral-Resource_calendar-Create')")
    @ApiOperation(value = "新建资源工作时间", tags = {"资源工作时间" },  notes = "新建资源工作时间")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendars")
    public ResponseEntity<Resource_calendarDTO> create(@Validated @RequestBody Resource_calendarDTO resource_calendardto) {
        Resource_calendar domain = resource_calendarMapping.toDomain(resource_calendardto);
		resource_calendarService.create(domain);
        Resource_calendarDTO dto = resource_calendarMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_calendarMapping.toDomain(#resource_calendardtos),'iBizBusinessCentral-Resource_calendar-Create')")
    @ApiOperation(value = "批量新建资源工作时间", tags = {"资源工作时间" },  notes = "批量新建资源工作时间")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendars/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_calendarDTO> resource_calendardtos) {
        resource_calendarService.createBatch(resource_calendarMapping.toDomain(resource_calendardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "resource_calendar" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.resource_calendarService.get(#resource_calendar_id),'iBizBusinessCentral-Resource_calendar-Update')")
    @ApiOperation(value = "更新资源工作时间", tags = {"资源工作时间" },  notes = "更新资源工作时间")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendars/{resource_calendar_id}")
    public ResponseEntity<Resource_calendarDTO> update(@PathVariable("resource_calendar_id") Long resource_calendar_id, @RequestBody Resource_calendarDTO resource_calendardto) {
		Resource_calendar domain  = resource_calendarMapping.toDomain(resource_calendardto);
        domain .setId(resource_calendar_id);
		resource_calendarService.update(domain );
		Resource_calendarDTO dto = resource_calendarMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_calendarService.getResourceCalendarByEntities(this.resource_calendarMapping.toDomain(#resource_calendardtos)),'iBizBusinessCentral-Resource_calendar-Update')")
    @ApiOperation(value = "批量更新资源工作时间", tags = {"资源工作时间" },  notes = "批量更新资源工作时间")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendars/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendarDTO> resource_calendardtos) {
        resource_calendarService.updateBatch(resource_calendarMapping.toDomain(resource_calendardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.resource_calendarService.get(#resource_calendar_id),'iBizBusinessCentral-Resource_calendar-Remove')")
    @ApiOperation(value = "删除资源工作时间", tags = {"资源工作时间" },  notes = "删除资源工作时间")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendars/{resource_calendar_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_id") Long resource_calendar_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_calendarService.remove(resource_calendar_id));
    }

    @PreAuthorize("hasPermission(this.resource_calendarService.getResourceCalendarByIds(#ids),'iBizBusinessCentral-Resource_calendar-Remove')")
    @ApiOperation(value = "批量删除资源工作时间", tags = {"资源工作时间" },  notes = "批量删除资源工作时间")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendars/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        resource_calendarService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.resource_calendarMapping.toDomain(returnObject.body),'iBizBusinessCentral-Resource_calendar-Get')")
    @ApiOperation(value = "获取资源工作时间", tags = {"资源工作时间" },  notes = "获取资源工作时间")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendars/{resource_calendar_id}")
    public ResponseEntity<Resource_calendarDTO> get(@PathVariable("resource_calendar_id") Long resource_calendar_id) {
        Resource_calendar domain = resource_calendarService.get(resource_calendar_id);
        Resource_calendarDTO dto = resource_calendarMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取资源工作时间草稿", tags = {"资源工作时间" },  notes = "获取资源工作时间草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendars/getdraft")
    public ResponseEntity<Resource_calendarDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(resource_calendarMapping.toDto(resource_calendarService.getDraft(new Resource_calendar())));
    }

    @ApiOperation(value = "检查资源工作时间", tags = {"资源工作时间" },  notes = "检查资源工作时间")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendars/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Resource_calendarDTO resource_calendardto) {
        return  ResponseEntity.status(HttpStatus.OK).body(resource_calendarService.checkKey(resource_calendarMapping.toDomain(resource_calendardto)));
    }

    @PreAuthorize("hasPermission(this.resource_calendarMapping.toDomain(#resource_calendardto),'iBizBusinessCentral-Resource_calendar-Save')")
    @ApiOperation(value = "保存资源工作时间", tags = {"资源工作时间" },  notes = "保存资源工作时间")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendars/save")
    public ResponseEntity<Boolean> save(@RequestBody Resource_calendarDTO resource_calendardto) {
        return ResponseEntity.status(HttpStatus.OK).body(resource_calendarService.save(resource_calendarMapping.toDomain(resource_calendardto)));
    }

    @PreAuthorize("hasPermission(this.resource_calendarMapping.toDomain(#resource_calendardtos),'iBizBusinessCentral-Resource_calendar-Save')")
    @ApiOperation(value = "批量保存资源工作时间", tags = {"资源工作时间" },  notes = "批量保存资源工作时间")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendars/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Resource_calendarDTO> resource_calendardtos) {
        resource_calendarService.saveBatch(resource_calendarMapping.toDomain(resource_calendardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_calendar-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_calendar-Get')")
	@ApiOperation(value = "获取数据集", tags = {"资源工作时间" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendars/fetchdefault")
	public ResponseEntity<List<Resource_calendarDTO>> fetchDefault(Resource_calendarSearchContext context) {
        Page<Resource_calendar> domains = resource_calendarService.searchDefault(context) ;
        List<Resource_calendarDTO> list = resource_calendarMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_calendar-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_calendar-Get')")
	@ApiOperation(value = "查询数据集", tags = {"资源工作时间" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/resource_calendars/searchdefault")
	public ResponseEntity<Page<Resource_calendarDTO>> searchDefault(@RequestBody Resource_calendarSearchContext context) {
        Page<Resource_calendar> domains = resource_calendarService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_calendarMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

