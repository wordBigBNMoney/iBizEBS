package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.businesscentral.core.odoo_digest.service.IDigest_digestService;
import cn.ibizlab.businesscentral.core.odoo_digest.filter.Digest_digestSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"摘要" })
@RestController("Core-digest_digest")
@RequestMapping("")
public class Digest_digestResource {

    @Autowired
    public IDigest_digestService digest_digestService;

    @Autowired
    @Lazy
    public Digest_digestMapping digest_digestMapping;

    @PreAuthorize("hasPermission(this.digest_digestMapping.toDomain(#digest_digestdto),'iBizBusinessCentral-Digest_digest-Create')")
    @ApiOperation(value = "新建摘要", tags = {"摘要" },  notes = "新建摘要")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_digests")
    public ResponseEntity<Digest_digestDTO> create(@Validated @RequestBody Digest_digestDTO digest_digestdto) {
        Digest_digest domain = digest_digestMapping.toDomain(digest_digestdto);
		digest_digestService.create(domain);
        Digest_digestDTO dto = digest_digestMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.digest_digestMapping.toDomain(#digest_digestdtos),'iBizBusinessCentral-Digest_digest-Create')")
    @ApiOperation(value = "批量新建摘要", tags = {"摘要" },  notes = "批量新建摘要")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_digests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Digest_digestDTO> digest_digestdtos) {
        digest_digestService.createBatch(digest_digestMapping.toDomain(digest_digestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "digest_digest" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.digest_digestService.get(#digest_digest_id),'iBizBusinessCentral-Digest_digest-Update')")
    @ApiOperation(value = "更新摘要", tags = {"摘要" },  notes = "更新摘要")
	@RequestMapping(method = RequestMethod.PUT, value = "/digest_digests/{digest_digest_id}")
    public ResponseEntity<Digest_digestDTO> update(@PathVariable("digest_digest_id") Long digest_digest_id, @RequestBody Digest_digestDTO digest_digestdto) {
		Digest_digest domain  = digest_digestMapping.toDomain(digest_digestdto);
        domain .setId(digest_digest_id);
		digest_digestService.update(domain );
		Digest_digestDTO dto = digest_digestMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.digest_digestService.getDigestDigestByEntities(this.digest_digestMapping.toDomain(#digest_digestdtos)),'iBizBusinessCentral-Digest_digest-Update')")
    @ApiOperation(value = "批量更新摘要", tags = {"摘要" },  notes = "批量更新摘要")
	@RequestMapping(method = RequestMethod.PUT, value = "/digest_digests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Digest_digestDTO> digest_digestdtos) {
        digest_digestService.updateBatch(digest_digestMapping.toDomain(digest_digestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.digest_digestService.get(#digest_digest_id),'iBizBusinessCentral-Digest_digest-Remove')")
    @ApiOperation(value = "删除摘要", tags = {"摘要" },  notes = "删除摘要")
	@RequestMapping(method = RequestMethod.DELETE, value = "/digest_digests/{digest_digest_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("digest_digest_id") Long digest_digest_id) {
         return ResponseEntity.status(HttpStatus.OK).body(digest_digestService.remove(digest_digest_id));
    }

    @PreAuthorize("hasPermission(this.digest_digestService.getDigestDigestByIds(#ids),'iBizBusinessCentral-Digest_digest-Remove')")
    @ApiOperation(value = "批量删除摘要", tags = {"摘要" },  notes = "批量删除摘要")
	@RequestMapping(method = RequestMethod.DELETE, value = "/digest_digests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        digest_digestService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.digest_digestMapping.toDomain(returnObject.body),'iBizBusinessCentral-Digest_digest-Get')")
    @ApiOperation(value = "获取摘要", tags = {"摘要" },  notes = "获取摘要")
	@RequestMapping(method = RequestMethod.GET, value = "/digest_digests/{digest_digest_id}")
    public ResponseEntity<Digest_digestDTO> get(@PathVariable("digest_digest_id") Long digest_digest_id) {
        Digest_digest domain = digest_digestService.get(digest_digest_id);
        Digest_digestDTO dto = digest_digestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取摘要草稿", tags = {"摘要" },  notes = "获取摘要草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/digest_digests/getdraft")
    public ResponseEntity<Digest_digestDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(digest_digestMapping.toDto(digest_digestService.getDraft(new Digest_digest())));
    }

    @ApiOperation(value = "检查摘要", tags = {"摘要" },  notes = "检查摘要")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_digests/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Digest_digestDTO digest_digestdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(digest_digestService.checkKey(digest_digestMapping.toDomain(digest_digestdto)));
    }

    @PreAuthorize("hasPermission(this.digest_digestMapping.toDomain(#digest_digestdto),'iBizBusinessCentral-Digest_digest-Save')")
    @ApiOperation(value = "保存摘要", tags = {"摘要" },  notes = "保存摘要")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_digests/save")
    public ResponseEntity<Boolean> save(@RequestBody Digest_digestDTO digest_digestdto) {
        return ResponseEntity.status(HttpStatus.OK).body(digest_digestService.save(digest_digestMapping.toDomain(digest_digestdto)));
    }

    @PreAuthorize("hasPermission(this.digest_digestMapping.toDomain(#digest_digestdtos),'iBizBusinessCentral-Digest_digest-Save')")
    @ApiOperation(value = "批量保存摘要", tags = {"摘要" },  notes = "批量保存摘要")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_digests/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Digest_digestDTO> digest_digestdtos) {
        digest_digestService.saveBatch(digest_digestMapping.toDomain(digest_digestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Digest_digest-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Digest_digest-Get')")
	@ApiOperation(value = "获取数据集", tags = {"摘要" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/digest_digests/fetchdefault")
	public ResponseEntity<List<Digest_digestDTO>> fetchDefault(Digest_digestSearchContext context) {
        Page<Digest_digest> domains = digest_digestService.searchDefault(context) ;
        List<Digest_digestDTO> list = digest_digestMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Digest_digest-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Digest_digest-Get')")
	@ApiOperation(value = "查询数据集", tags = {"摘要" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/digest_digests/searchdefault")
	public ResponseEntity<Page<Digest_digestDTO>> searchDefault(@RequestBody Digest_digestSearchContext context) {
        Page<Digest_digest> domains = digest_digestService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(digest_digestMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

