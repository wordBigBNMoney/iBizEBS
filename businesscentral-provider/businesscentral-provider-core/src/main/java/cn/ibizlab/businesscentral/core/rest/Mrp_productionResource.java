package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_productionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Production Order" })
@RestController("Core-mrp_production")
@RequestMapping("")
public class Mrp_productionResource {

    @Autowired
    public IMrp_productionService mrp_productionService;

    @Autowired
    @Lazy
    public Mrp_productionMapping mrp_productionMapping;

    @PreAuthorize("hasPermission(this.mrp_productionMapping.toDomain(#mrp_productiondto),'iBizBusinessCentral-Mrp_production-Create')")
    @ApiOperation(value = "新建Production Order", tags = {"Production Order" },  notes = "新建Production Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_productions")
    public ResponseEntity<Mrp_productionDTO> create(@Validated @RequestBody Mrp_productionDTO mrp_productiondto) {
        Mrp_production domain = mrp_productionMapping.toDomain(mrp_productiondto);
		mrp_productionService.create(domain);
        Mrp_productionDTO dto = mrp_productionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_productionMapping.toDomain(#mrp_productiondtos),'iBizBusinessCentral-Mrp_production-Create')")
    @ApiOperation(value = "批量新建Production Order", tags = {"Production Order" },  notes = "批量新建Production Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_productionDTO> mrp_productiondtos) {
        mrp_productionService.createBatch(mrp_productionMapping.toDomain(mrp_productiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_production" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_productionService.get(#mrp_production_id),'iBizBusinessCentral-Mrp_production-Update')")
    @ApiOperation(value = "更新Production Order", tags = {"Production Order" },  notes = "更新Production Order")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_productions/{mrp_production_id}")
    public ResponseEntity<Mrp_productionDTO> update(@PathVariable("mrp_production_id") Long mrp_production_id, @RequestBody Mrp_productionDTO mrp_productiondto) {
		Mrp_production domain  = mrp_productionMapping.toDomain(mrp_productiondto);
        domain .setId(mrp_production_id);
		mrp_productionService.update(domain );
		Mrp_productionDTO dto = mrp_productionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_productionService.getMrpProductionByEntities(this.mrp_productionMapping.toDomain(#mrp_productiondtos)),'iBizBusinessCentral-Mrp_production-Update')")
    @ApiOperation(value = "批量更新Production Order", tags = {"Production Order" },  notes = "批量更新Production Order")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_productions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_productionDTO> mrp_productiondtos) {
        mrp_productionService.updateBatch(mrp_productionMapping.toDomain(mrp_productiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_productionService.get(#mrp_production_id),'iBizBusinessCentral-Mrp_production-Remove')")
    @ApiOperation(value = "删除Production Order", tags = {"Production Order" },  notes = "删除Production Order")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_productions/{mrp_production_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_production_id") Long mrp_production_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_productionService.remove(mrp_production_id));
    }

    @PreAuthorize("hasPermission(this.mrp_productionService.getMrpProductionByIds(#ids),'iBizBusinessCentral-Mrp_production-Remove')")
    @ApiOperation(value = "批量删除Production Order", tags = {"Production Order" },  notes = "批量删除Production Order")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_productions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_productionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_productionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_production-Get')")
    @ApiOperation(value = "获取Production Order", tags = {"Production Order" },  notes = "获取Production Order")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_productions/{mrp_production_id}")
    public ResponseEntity<Mrp_productionDTO> get(@PathVariable("mrp_production_id") Long mrp_production_id) {
        Mrp_production domain = mrp_productionService.get(mrp_production_id);
        Mrp_productionDTO dto = mrp_productionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Production Order草稿", tags = {"Production Order" },  notes = "获取Production Order草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_productions/getdraft")
    public ResponseEntity<Mrp_productionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_productionMapping.toDto(mrp_productionService.getDraft(new Mrp_production())));
    }

    @ApiOperation(value = "检查Production Order", tags = {"Production Order" },  notes = "检查Production Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_productionDTO mrp_productiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_productionService.checkKey(mrp_productionMapping.toDomain(mrp_productiondto)));
    }

    @PreAuthorize("hasPermission(this.mrp_productionMapping.toDomain(#mrp_productiondto),'iBizBusinessCentral-Mrp_production-Save')")
    @ApiOperation(value = "保存Production Order", tags = {"Production Order" },  notes = "保存Production Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_productionDTO mrp_productiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_productionService.save(mrp_productionMapping.toDomain(mrp_productiondto)));
    }

    @PreAuthorize("hasPermission(this.mrp_productionMapping.toDomain(#mrp_productiondtos),'iBizBusinessCentral-Mrp_production-Save')")
    @ApiOperation(value = "批量保存Production Order", tags = {"Production Order" },  notes = "批量保存Production Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_productionDTO> mrp_productiondtos) {
        mrp_productionService.saveBatch(mrp_productionMapping.toDomain(mrp_productiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_production-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_production-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Production Order" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_productions/fetchdefault")
	public ResponseEntity<List<Mrp_productionDTO>> fetchDefault(Mrp_productionSearchContext context) {
        Page<Mrp_production> domains = mrp_productionService.searchDefault(context) ;
        List<Mrp_productionDTO> list = mrp_productionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_production-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_production-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Production Order" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_productions/searchdefault")
	public ResponseEntity<Page<Mrp_productionDTO>> searchDefault(@RequestBody Mrp_productionSearchContext context) {
        Page<Mrp_production> domains = mrp_productionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_productionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

