package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goalService;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goalSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"游戏化目标" })
@RestController("Core-gamification_goal")
@RequestMapping("")
public class Gamification_goalResource {

    @Autowired
    public IGamification_goalService gamification_goalService;

    @Autowired
    @Lazy
    public Gamification_goalMapping gamification_goalMapping;

    @PreAuthorize("hasPermission(this.gamification_goalMapping.toDomain(#gamification_goaldto),'iBizBusinessCentral-Gamification_goal-Create')")
    @ApiOperation(value = "新建游戏化目标", tags = {"游戏化目标" },  notes = "新建游戏化目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goals")
    public ResponseEntity<Gamification_goalDTO> create(@Validated @RequestBody Gamification_goalDTO gamification_goaldto) {
        Gamification_goal domain = gamification_goalMapping.toDomain(gamification_goaldto);
		gamification_goalService.create(domain);
        Gamification_goalDTO dto = gamification_goalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_goalMapping.toDomain(#gamification_goaldtos),'iBizBusinessCentral-Gamification_goal-Create')")
    @ApiOperation(value = "批量新建游戏化目标", tags = {"游戏化目标" },  notes = "批量新建游戏化目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_goalDTO> gamification_goaldtos) {
        gamification_goalService.createBatch(gamification_goalMapping.toDomain(gamification_goaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "gamification_goal" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.gamification_goalService.get(#gamification_goal_id),'iBizBusinessCentral-Gamification_goal-Update')")
    @ApiOperation(value = "更新游戏化目标", tags = {"游戏化目标" },  notes = "更新游戏化目标")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goals/{gamification_goal_id}")
    public ResponseEntity<Gamification_goalDTO> update(@PathVariable("gamification_goal_id") Long gamification_goal_id, @RequestBody Gamification_goalDTO gamification_goaldto) {
		Gamification_goal domain  = gamification_goalMapping.toDomain(gamification_goaldto);
        domain .setId(gamification_goal_id);
		gamification_goalService.update(domain );
		Gamification_goalDTO dto = gamification_goalMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_goalService.getGamificationGoalByEntities(this.gamification_goalMapping.toDomain(#gamification_goaldtos)),'iBizBusinessCentral-Gamification_goal-Update')")
    @ApiOperation(value = "批量更新游戏化目标", tags = {"游戏化目标" },  notes = "批量更新游戏化目标")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_goalDTO> gamification_goaldtos) {
        gamification_goalService.updateBatch(gamification_goalMapping.toDomain(gamification_goaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.gamification_goalService.get(#gamification_goal_id),'iBizBusinessCentral-Gamification_goal-Remove')")
    @ApiOperation(value = "删除游戏化目标", tags = {"游戏化目标" },  notes = "删除游戏化目标")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goals/{gamification_goal_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("gamification_goal_id") Long gamification_goal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_goalService.remove(gamification_goal_id));
    }

    @PreAuthorize("hasPermission(this.gamification_goalService.getGamificationGoalByIds(#ids),'iBizBusinessCentral-Gamification_goal-Remove')")
    @ApiOperation(value = "批量删除游戏化目标", tags = {"游戏化目标" },  notes = "批量删除游戏化目标")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        gamification_goalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.gamification_goalMapping.toDomain(returnObject.body),'iBizBusinessCentral-Gamification_goal-Get')")
    @ApiOperation(value = "获取游戏化目标", tags = {"游戏化目标" },  notes = "获取游戏化目标")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_goals/{gamification_goal_id}")
    public ResponseEntity<Gamification_goalDTO> get(@PathVariable("gamification_goal_id") Long gamification_goal_id) {
        Gamification_goal domain = gamification_goalService.get(gamification_goal_id);
        Gamification_goalDTO dto = gamification_goalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取游戏化目标草稿", tags = {"游戏化目标" },  notes = "获取游戏化目标草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_goals/getdraft")
    public ResponseEntity<Gamification_goalDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_goalMapping.toDto(gamification_goalService.getDraft(new Gamification_goal())));
    }

    @ApiOperation(value = "检查游戏化目标", tags = {"游戏化目标" },  notes = "检查游戏化目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goals/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Gamification_goalDTO gamification_goaldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(gamification_goalService.checkKey(gamification_goalMapping.toDomain(gamification_goaldto)));
    }

    @PreAuthorize("hasPermission(this.gamification_goalMapping.toDomain(#gamification_goaldto),'iBizBusinessCentral-Gamification_goal-Save')")
    @ApiOperation(value = "保存游戏化目标", tags = {"游戏化目标" },  notes = "保存游戏化目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goals/save")
    public ResponseEntity<Boolean> save(@RequestBody Gamification_goalDTO gamification_goaldto) {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_goalService.save(gamification_goalMapping.toDomain(gamification_goaldto)));
    }

    @PreAuthorize("hasPermission(this.gamification_goalMapping.toDomain(#gamification_goaldtos),'iBizBusinessCentral-Gamification_goal-Save')")
    @ApiOperation(value = "批量保存游戏化目标", tags = {"游戏化目标" },  notes = "批量保存游戏化目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goals/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Gamification_goalDTO> gamification_goaldtos) {
        gamification_goalService.saveBatch(gamification_goalMapping.toDomain(gamification_goaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_goal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_goal-Get')")
	@ApiOperation(value = "获取数据集", tags = {"游戏化目标" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_goals/fetchdefault")
	public ResponseEntity<List<Gamification_goalDTO>> fetchDefault(Gamification_goalSearchContext context) {
        Page<Gamification_goal> domains = gamification_goalService.searchDefault(context) ;
        List<Gamification_goalDTO> list = gamification_goalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_goal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_goal-Get')")
	@ApiOperation(value = "查询数据集", tags = {"游戏化目标" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/gamification_goals/searchdefault")
	public ResponseEntity<Page<Gamification_goalDTO>> searchDefault(@RequestBody Gamification_goalSearchContext context) {
        Page<Gamification_goal> domains = gamification_goalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_goalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

