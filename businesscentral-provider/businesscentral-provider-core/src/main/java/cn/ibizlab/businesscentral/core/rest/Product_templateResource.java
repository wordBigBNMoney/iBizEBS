package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_templateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品模板" })
@RestController("Core-product_template")
@RequestMapping("")
public class Product_templateResource {

    @Autowired
    public IProduct_templateService product_templateService;

    @Autowired
    @Lazy
    public Product_templateMapping product_templateMapping;

    @PreAuthorize("hasPermission(this.product_templateMapping.toDomain(#product_templatedto),'iBizBusinessCentral-Product_template-Create')")
    @ApiOperation(value = "新建产品模板", tags = {"产品模板" },  notes = "新建产品模板")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates")
    public ResponseEntity<Product_templateDTO> create(@Validated @RequestBody Product_templateDTO product_templatedto) {
        Product_template domain = product_templateMapping.toDomain(product_templatedto);
		product_templateService.create(domain);
        Product_templateDTO dto = product_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_templateMapping.toDomain(#product_templatedtos),'iBizBusinessCentral-Product_template-Create')")
    @ApiOperation(value = "批量新建产品模板", tags = {"产品模板" },  notes = "批量新建产品模板")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_templateDTO> product_templatedtos) {
        product_templateService.createBatch(product_templateMapping.toDomain(product_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_template" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_templateService.get(#product_template_id),'iBizBusinessCentral-Product_template-Update')")
    @ApiOperation(value = "更新产品模板", tags = {"产品模板" },  notes = "更新产品模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}")
    public ResponseEntity<Product_templateDTO> update(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_templateDTO product_templatedto) {
		Product_template domain  = product_templateMapping.toDomain(product_templatedto);
        domain .setId(product_template_id);
		product_templateService.update(domain );
		Product_templateDTO dto = product_templateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_templateService.getProductTemplateByEntities(this.product_templateMapping.toDomain(#product_templatedtos)),'iBizBusinessCentral-Product_template-Update')")
    @ApiOperation(value = "批量更新产品模板", tags = {"产品模板" },  notes = "批量更新产品模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_templateDTO> product_templatedtos) {
        product_templateService.updateBatch(product_templateMapping.toDomain(product_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_templateService.get(#product_template_id),'iBizBusinessCentral-Product_template-Remove')")
    @ApiOperation(value = "删除产品模板", tags = {"产品模板" },  notes = "删除产品模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_template_id") Long product_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_templateService.remove(product_template_id));
    }

    @PreAuthorize("hasPermission(this.product_templateService.getProductTemplateByIds(#ids),'iBizBusinessCentral-Product_template-Remove')")
    @ApiOperation(value = "批量删除产品模板", tags = {"产品模板" },  notes = "批量删除产品模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_templateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_template-Get')")
    @ApiOperation(value = "获取产品模板", tags = {"产品模板" },  notes = "获取产品模板")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}")
    public ResponseEntity<Product_templateDTO> get(@PathVariable("product_template_id") Long product_template_id) {
        Product_template domain = product_templateService.get(product_template_id);
        Product_templateDTO dto = product_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品模板草稿", tags = {"产品模板" },  notes = "获取产品模板草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/getdraft")
    public ResponseEntity<Product_templateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_templateMapping.toDto(product_templateService.getDraft(new Product_template())));
    }

    @ApiOperation(value = "检查产品模板", tags = {"产品模板" },  notes = "检查产品模板")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_templateDTO product_templatedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_templateService.checkKey(product_templateMapping.toDomain(product_templatedto)));
    }

    @PreAuthorize("hasPermission(this.product_templateMapping.toDomain(#product_templatedto),'iBizBusinessCentral-Product_template-Save')")
    @ApiOperation(value = "保存产品模板", tags = {"产品模板" },  notes = "保存产品模板")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_templateDTO product_templatedto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_templateService.save(product_templateMapping.toDomain(product_templatedto)));
    }

    @PreAuthorize("hasPermission(this.product_templateMapping.toDomain(#product_templatedtos),'iBizBusinessCentral-Product_template-Save')")
    @ApiOperation(value = "批量保存产品模板", tags = {"产品模板" },  notes = "批量保存产品模板")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_templateDTO> product_templatedtos) {
        product_templateService.saveBatch(product_templateMapping.toDomain(product_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_template-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品模板" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/fetchdefault")
	public ResponseEntity<List<Product_templateDTO>> fetchDefault(Product_templateSearchContext context) {
        Page<Product_template> domains = product_templateService.searchDefault(context) ;
        List<Product_templateDTO> list = product_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_template-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品模板" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/searchdefault")
	public ResponseEntity<Page<Product_templateDTO>> searchDefault(@RequestBody Product_templateSearchContext context) {
        Page<Product_template> domains = product_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Product_template-Get')")
	@ApiOperation(value = "获取首选表格", tags = {"产品模板" } ,notes = "获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/fetchmaster")
	public ResponseEntity<List<Product_templateDTO>> fetchMaster(Product_templateSearchContext context) {
        Page<Product_template> domains = product_templateService.searchMaster(context) ;
        List<Product_templateDTO> list = product_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Product_template-Get')")
	@ApiOperation(value = "查询首选表格", tags = {"产品模板" } ,notes = "查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/searchmaster")
	public ResponseEntity<Page<Product_templateDTO>> searchMaster(@RequestBody Product_templateSearchContext context) {
        Page<Product_template> domains = product_templateService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

