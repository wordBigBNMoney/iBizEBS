package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Web_editor_converter_testDTO]
 */
@Data
public class Web_editor_converter_testDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TEXT]
     *
     */
    @JSONField(name = "text")
    @JsonProperty("text")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String text;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SELECTION_STR]
     *
     */
    @JSONField(name = "selection_str")
    @JsonProperty("selection_str")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String selectionStr;

    /**
     * 属性 [NUMERIC]
     *
     */
    @JSONField(name = "numeric")
    @JsonProperty("numeric")
    private Double numeric;

    /**
     * 属性 [INTEGER]
     *
     */
    @JSONField(name = "integer")
    @JsonProperty("integer")
    private Integer integer;

    /**
     * 属性 [BINARY]
     *
     */
    @JSONField(name = "binary")
    @JsonProperty("binary")
    private byte[] binary;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [IBIZFLOAT]
     *
     */
    @JSONField(name = "ibizfloat")
    @JsonProperty("ibizfloat")
    private Double ibizfloat;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DATETIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("datetime")
    private Timestamp datetime;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [HTML]
     *
     */
    @JSONField(name = "html")
    @JsonProperty("html")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String html;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [IBIZCHAR]
     *
     */
    @JSONField(name = "ibizchar")
    @JsonProperty("ibizchar")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String ibizchar;

    /**
     * 属性 [SELECTION]
     *
     */
    @JSONField(name = "selection")
    @JsonProperty("selection")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String selection;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [MANY2ONE_TEXT]
     *
     */
    @JSONField(name = "many2one_text")
    @JsonProperty("many2one_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String many2oneText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [MANY2ONE]
     *
     */
    @JSONField(name = "many2one")
    @JsonProperty("many2one")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long many2one;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [TEXT]
     */
    public void setText(String  text){
        this.text = text ;
        this.modify("text",text);
    }

    /**
     * 设置 [SELECTION_STR]
     */
    public void setSelectionStr(String  selectionStr){
        this.selectionStr = selectionStr ;
        this.modify("selection_str",selectionStr);
    }

    /**
     * 设置 [NUMERIC]
     */
    public void setNumeric(Double  numeric){
        this.numeric = numeric ;
        this.modify("numeric",numeric);
    }

    /**
     * 设置 [INTEGER]
     */
    public void setInteger(Integer  integer){
        this.integer = integer ;
        this.modify("integer",integer);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [IBIZFLOAT]
     */
    public void setIbizfloat(Double  ibizfloat){
        this.ibizfloat = ibizfloat ;
        this.modify("ibizfloat",ibizfloat);
    }

    /**
     * 设置 [DATETIME]
     */
    public void setDatetime(Timestamp  datetime){
        this.datetime = datetime ;
        this.modify("datetime",datetime);
    }

    /**
     * 设置 [HTML]
     */
    public void setHtml(String  html){
        this.html = html ;
        this.modify("html",html);
    }

    /**
     * 设置 [IBIZCHAR]
     */
    public void setIbizchar(String  ibizchar){
        this.ibizchar = ibizchar ;
        this.modify("ibizchar",ibizchar);
    }

    /**
     * 设置 [SELECTION]
     */
    public void setSelection(String  selection){
        this.selection = selection ;
        this.modify("selection",selection);
    }

    /**
     * 设置 [MANY2ONE]
     */
    public void setMany2one(Long  many2one){
        this.many2one = many2one ;
        this.modify("many2one",many2one);
    }


}


