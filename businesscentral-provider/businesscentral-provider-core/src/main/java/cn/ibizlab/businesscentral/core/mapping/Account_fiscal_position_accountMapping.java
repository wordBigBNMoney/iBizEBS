package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.businesscentral.core.dto.Account_fiscal_position_accountDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_fiscal_position_accountMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_fiscal_position_accountMapping extends MappingBase<Account_fiscal_position_accountDTO, Account_fiscal_position_account> {


}

