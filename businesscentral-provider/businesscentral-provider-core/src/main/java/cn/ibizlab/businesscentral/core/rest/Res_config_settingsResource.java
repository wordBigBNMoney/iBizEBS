package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_config_settingsService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_config_settingsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"配置设定" })
@RestController("Core-res_config_settings")
@RequestMapping("")
public class Res_config_settingsResource {

    @Autowired
    public IRes_config_settingsService res_config_settingsService;

    @Autowired
    @Lazy
    public Res_config_settingsMapping res_config_settingsMapping;

    @PreAuthorize("hasPermission(this.res_config_settingsMapping.toDomain(#res_config_settingsdto),'iBizBusinessCentral-Res_config_settings-Create')")
    @ApiOperation(value = "新建配置设定", tags = {"配置设定" },  notes = "新建配置设定")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_settings")
    public ResponseEntity<Res_config_settingsDTO> create(@Validated @RequestBody Res_config_settingsDTO res_config_settingsdto) {
        Res_config_settings domain = res_config_settingsMapping.toDomain(res_config_settingsdto);
		res_config_settingsService.create(domain);
        Res_config_settingsDTO dto = res_config_settingsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_config_settingsMapping.toDomain(#res_config_settingsdtos),'iBizBusinessCentral-Res_config_settings-Create')")
    @ApiOperation(value = "批量新建配置设定", tags = {"配置设定" },  notes = "批量新建配置设定")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_config_settingsDTO> res_config_settingsdtos) {
        res_config_settingsService.createBatch(res_config_settingsMapping.toDomain(res_config_settingsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_config_settings" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_config_settingsService.get(#res_config_settings_id),'iBizBusinessCentral-Res_config_settings-Update')")
    @ApiOperation(value = "更新配置设定", tags = {"配置设定" },  notes = "更新配置设定")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_config_settings/{res_config_settings_id}")
    public ResponseEntity<Res_config_settingsDTO> update(@PathVariable("res_config_settings_id") Long res_config_settings_id, @RequestBody Res_config_settingsDTO res_config_settingsdto) {
		Res_config_settings domain  = res_config_settingsMapping.toDomain(res_config_settingsdto);
        domain .setId(res_config_settings_id);
		res_config_settingsService.update(domain );
		Res_config_settingsDTO dto = res_config_settingsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_config_settingsService.getResConfigSettingsByEntities(this.res_config_settingsMapping.toDomain(#res_config_settingsdtos)),'iBizBusinessCentral-Res_config_settings-Update')")
    @ApiOperation(value = "批量更新配置设定", tags = {"配置设定" },  notes = "批量更新配置设定")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_config_settings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_config_settingsDTO> res_config_settingsdtos) {
        res_config_settingsService.updateBatch(res_config_settingsMapping.toDomain(res_config_settingsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_config_settingsService.get(#res_config_settings_id),'iBizBusinessCentral-Res_config_settings-Remove')")
    @ApiOperation(value = "删除配置设定", tags = {"配置设定" },  notes = "删除配置设定")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_config_settings/{res_config_settings_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_config_settings_id") Long res_config_settings_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_config_settingsService.remove(res_config_settings_id));
    }

    @PreAuthorize("hasPermission(this.res_config_settingsService.getResConfigSettingsByIds(#ids),'iBizBusinessCentral-Res_config_settings-Remove')")
    @ApiOperation(value = "批量删除配置设定", tags = {"配置设定" },  notes = "批量删除配置设定")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_config_settings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_config_settingsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_config_settingsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_config_settings-Get')")
    @ApiOperation(value = "获取配置设定", tags = {"配置设定" },  notes = "获取配置设定")
	@RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/{res_config_settings_id}")
    public ResponseEntity<Res_config_settingsDTO> get(@PathVariable("res_config_settings_id") Long res_config_settings_id) {
        Res_config_settings domain = res_config_settingsService.get(res_config_settings_id);
        Res_config_settingsDTO dto = res_config_settingsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取配置设定草稿", tags = {"配置设定" },  notes = "获取配置设定草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/getdraft")
    public ResponseEntity<Res_config_settingsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_config_settingsMapping.toDto(res_config_settingsService.getDraft(new Res_config_settings())));
    }

    @ApiOperation(value = "检查配置设定", tags = {"配置设定" },  notes = "检查配置设定")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_config_settingsDTO res_config_settingsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_config_settingsService.checkKey(res_config_settingsMapping.toDomain(res_config_settingsdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_config_settings-GetLatestSettings-all')")
    @ApiOperation(value = "获取设置", tags = {"配置设定" },  notes = "获取设置")
	@RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/{res_config_settings_id}/getlatestsettings")
    public ResponseEntity<Res_config_settingsDTO> getLatestSettings(@PathVariable("res_config_settings_id") Long res_config_settings_id, @RequestBody Res_config_settingsDTO res_config_settingsdto) {
        Res_config_settings domain = res_config_settingsMapping.toDomain(res_config_settingsdto);
        domain.setId(res_config_settings_id);
        domain = res_config_settingsService.getLatestSettings(domain);
        res_config_settingsdto = res_config_settingsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(res_config_settingsdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_config_settings-Get_default-all')")
    @ApiOperation(value = "获取配置", tags = {"配置设定" },  notes = "获取配置")
	@RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/get_default")
    public ResponseEntity<Res_config_settingsDTO> get_default() {
        Res_config_settings domain =new Res_config_settings();
        domain = res_config_settingsService.get_default(domain);
        Res_config_settingsDTO res_config_settingsdto = res_config_settingsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(res_config_settingsdto);
    }

    @PreAuthorize("hasPermission(this.res_config_settingsMapping.toDomain(#res_config_settingsdto),'iBizBusinessCentral-Res_config_settings-Save')")
    @ApiOperation(value = "保存配置设定", tags = {"配置设定" },  notes = "保存配置设定")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_config_settingsDTO res_config_settingsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_config_settingsService.save(res_config_settingsMapping.toDomain(res_config_settingsdto)));
    }

    @PreAuthorize("hasPermission(this.res_config_settingsMapping.toDomain(#res_config_settingsdtos),'iBizBusinessCentral-Res_config_settings-Save')")
    @ApiOperation(value = "批量保存配置设定", tags = {"配置设定" },  notes = "批量保存配置设定")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_config_settingsDTO> res_config_settingsdtos) {
        res_config_settingsService.saveBatch(res_config_settingsMapping.toDomain(res_config_settingsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_config_settings-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_config_settings-Get')")
	@ApiOperation(value = "获取数据集", tags = {"配置设定" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_config_settings/fetchdefault")
	public ResponseEntity<List<Res_config_settingsDTO>> fetchDefault(Res_config_settingsSearchContext context) {
        Page<Res_config_settings> domains = res_config_settingsService.searchDefault(context) ;
        List<Res_config_settingsDTO> list = res_config_settingsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_config_settings-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_config_settings-Get')")
	@ApiOperation(value = "查询数据集", tags = {"配置设定" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_config_settings/searchdefault")
	public ResponseEntity<Page<Res_config_settingsDTO>> searchDefault(@RequestBody Res_config_settingsSearchContext context) {
        Page<Res_config_settings> domains = res_config_settingsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_config_settingsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

