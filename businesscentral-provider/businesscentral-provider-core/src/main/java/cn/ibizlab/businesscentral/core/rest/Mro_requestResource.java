package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_request;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_requestService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_requestSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"保养请求" })
@RestController("Core-mro_request")
@RequestMapping("")
public class Mro_requestResource {

    @Autowired
    public IMro_requestService mro_requestService;

    @Autowired
    @Lazy
    public Mro_requestMapping mro_requestMapping;

    @PreAuthorize("hasPermission(this.mro_requestMapping.toDomain(#mro_requestdto),'iBizBusinessCentral-Mro_request-Create')")
    @ApiOperation(value = "新建保养请求", tags = {"保养请求" },  notes = "新建保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_requests")
    public ResponseEntity<Mro_requestDTO> create(@Validated @RequestBody Mro_requestDTO mro_requestdto) {
        Mro_request domain = mro_requestMapping.toDomain(mro_requestdto);
		mro_requestService.create(domain);
        Mro_requestDTO dto = mro_requestMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_requestMapping.toDomain(#mro_requestdtos),'iBizBusinessCentral-Mro_request-Create')")
    @ApiOperation(value = "批量新建保养请求", tags = {"保养请求" },  notes = "批量新建保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_requests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_requestDTO> mro_requestdtos) {
        mro_requestService.createBatch(mro_requestMapping.toDomain(mro_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_request" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_requestService.get(#mro_request_id),'iBizBusinessCentral-Mro_request-Update')")
    @ApiOperation(value = "更新保养请求", tags = {"保养请求" },  notes = "更新保养请求")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_requests/{mro_request_id}")
    public ResponseEntity<Mro_requestDTO> update(@PathVariable("mro_request_id") Long mro_request_id, @RequestBody Mro_requestDTO mro_requestdto) {
		Mro_request domain  = mro_requestMapping.toDomain(mro_requestdto);
        domain .setId(mro_request_id);
		mro_requestService.update(domain );
		Mro_requestDTO dto = mro_requestMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_requestService.getMroRequestByEntities(this.mro_requestMapping.toDomain(#mro_requestdtos)),'iBizBusinessCentral-Mro_request-Update')")
    @ApiOperation(value = "批量更新保养请求", tags = {"保养请求" },  notes = "批量更新保养请求")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_requests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_requestDTO> mro_requestdtos) {
        mro_requestService.updateBatch(mro_requestMapping.toDomain(mro_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_requestService.get(#mro_request_id),'iBizBusinessCentral-Mro_request-Remove')")
    @ApiOperation(value = "删除保养请求", tags = {"保养请求" },  notes = "删除保养请求")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_requests/{mro_request_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_request_id") Long mro_request_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_requestService.remove(mro_request_id));
    }

    @PreAuthorize("hasPermission(this.mro_requestService.getMroRequestByIds(#ids),'iBizBusinessCentral-Mro_request-Remove')")
    @ApiOperation(value = "批量删除保养请求", tags = {"保养请求" },  notes = "批量删除保养请求")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_requests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_requestService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_requestMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_request-Get')")
    @ApiOperation(value = "获取保养请求", tags = {"保养请求" },  notes = "获取保养请求")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_requests/{mro_request_id}")
    public ResponseEntity<Mro_requestDTO> get(@PathVariable("mro_request_id") Long mro_request_id) {
        Mro_request domain = mro_requestService.get(mro_request_id);
        Mro_requestDTO dto = mro_requestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取保养请求草稿", tags = {"保养请求" },  notes = "获取保养请求草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_requests/getdraft")
    public ResponseEntity<Mro_requestDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_requestMapping.toDto(mro_requestService.getDraft(new Mro_request())));
    }

    @ApiOperation(value = "检查保养请求", tags = {"保养请求" },  notes = "检查保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_requests/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_requestDTO mro_requestdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_requestService.checkKey(mro_requestMapping.toDomain(mro_requestdto)));
    }

    @PreAuthorize("hasPermission(this.mro_requestMapping.toDomain(#mro_requestdto),'iBizBusinessCentral-Mro_request-Save')")
    @ApiOperation(value = "保存保养请求", tags = {"保养请求" },  notes = "保存保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_requests/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_requestDTO mro_requestdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_requestService.save(mro_requestMapping.toDomain(mro_requestdto)));
    }

    @PreAuthorize("hasPermission(this.mro_requestMapping.toDomain(#mro_requestdtos),'iBizBusinessCentral-Mro_request-Save')")
    @ApiOperation(value = "批量保存保养请求", tags = {"保养请求" },  notes = "批量保存保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_requests/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_requestDTO> mro_requestdtos) {
        mro_requestService.saveBatch(mro_requestMapping.toDomain(mro_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_request-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_request-Get')")
	@ApiOperation(value = "获取数据集", tags = {"保养请求" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_requests/fetchdefault")
	public ResponseEntity<List<Mro_requestDTO>> fetchDefault(Mro_requestSearchContext context) {
        Page<Mro_request> domains = mro_requestService.searchDefault(context) ;
        List<Mro_requestDTO> list = mro_requestMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_request-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_request-Get')")
	@ApiOperation(value = "查询数据集", tags = {"保养请求" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_requests/searchdefault")
	public ResponseEntity<Page<Mro_requestDTO>> searchDefault(@RequestBody Mro_requestSearchContext context) {
        Page<Mro_request> domains = mro_requestService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_requestMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

