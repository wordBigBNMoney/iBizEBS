package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_multi_mixin;
import cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_multi_mixinService;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_multi_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"多网站的Mixin" })
@RestController("Core-website_multi_mixin")
@RequestMapping("")
public class Website_multi_mixinResource {

    @Autowired
    public IWebsite_multi_mixinService website_multi_mixinService;

    @Autowired
    @Lazy
    public Website_multi_mixinMapping website_multi_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Create-all')")
    @ApiOperation(value = "新建多网站的Mixin", tags = {"多网站的Mixin" },  notes = "新建多网站的Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_multi_mixins")
    public ResponseEntity<Website_multi_mixinDTO> create(@Validated @RequestBody Website_multi_mixinDTO website_multi_mixindto) {
        Website_multi_mixin domain = website_multi_mixinMapping.toDomain(website_multi_mixindto);
		website_multi_mixinService.create(domain);
        Website_multi_mixinDTO dto = website_multi_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Create-all')")
    @ApiOperation(value = "批量新建多网站的Mixin", tags = {"多网站的Mixin" },  notes = "批量新建多网站的Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_multi_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_multi_mixinDTO> website_multi_mixindtos) {
        website_multi_mixinService.createBatch(website_multi_mixinMapping.toDomain(website_multi_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Update-all')")
    @ApiOperation(value = "更新多网站的Mixin", tags = {"多网站的Mixin" },  notes = "更新多网站的Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_multi_mixins/{website_multi_mixin_id}")
    public ResponseEntity<Website_multi_mixinDTO> update(@PathVariable("website_multi_mixin_id") Long website_multi_mixin_id, @RequestBody Website_multi_mixinDTO website_multi_mixindto) {
		Website_multi_mixin domain  = website_multi_mixinMapping.toDomain(website_multi_mixindto);
        domain .setId(website_multi_mixin_id);
		website_multi_mixinService.update(domain );
		Website_multi_mixinDTO dto = website_multi_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Update-all')")
    @ApiOperation(value = "批量更新多网站的Mixin", tags = {"多网站的Mixin" },  notes = "批量更新多网站的Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_multi_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_multi_mixinDTO> website_multi_mixindtos) {
        website_multi_mixinService.updateBatch(website_multi_mixinMapping.toDomain(website_multi_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Remove-all')")
    @ApiOperation(value = "删除多网站的Mixin", tags = {"多网站的Mixin" },  notes = "删除多网站的Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_multi_mixins/{website_multi_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("website_multi_mixin_id") Long website_multi_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_multi_mixinService.remove(website_multi_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Remove-all')")
    @ApiOperation(value = "批量删除多网站的Mixin", tags = {"多网站的Mixin" },  notes = "批量删除多网站的Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_multi_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        website_multi_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Get-all')")
    @ApiOperation(value = "获取多网站的Mixin", tags = {"多网站的Mixin" },  notes = "获取多网站的Mixin")
	@RequestMapping(method = RequestMethod.GET, value = "/website_multi_mixins/{website_multi_mixin_id}")
    public ResponseEntity<Website_multi_mixinDTO> get(@PathVariable("website_multi_mixin_id") Long website_multi_mixin_id) {
        Website_multi_mixin domain = website_multi_mixinService.get(website_multi_mixin_id);
        Website_multi_mixinDTO dto = website_multi_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取多网站的Mixin草稿", tags = {"多网站的Mixin" },  notes = "获取多网站的Mixin草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/website_multi_mixins/getdraft")
    public ResponseEntity<Website_multi_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(website_multi_mixinMapping.toDto(website_multi_mixinService.getDraft(new Website_multi_mixin())));
    }

    @ApiOperation(value = "检查多网站的Mixin", tags = {"多网站的Mixin" },  notes = "检查多网站的Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_multi_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Website_multi_mixinDTO website_multi_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(website_multi_mixinService.checkKey(website_multi_mixinMapping.toDomain(website_multi_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Save-all')")
    @ApiOperation(value = "保存多网站的Mixin", tags = {"多网站的Mixin" },  notes = "保存多网站的Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_multi_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Website_multi_mixinDTO website_multi_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(website_multi_mixinService.save(website_multi_mixinMapping.toDomain(website_multi_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-Save-all')")
    @ApiOperation(value = "批量保存多网站的Mixin", tags = {"多网站的Mixin" },  notes = "批量保存多网站的Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_multi_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Website_multi_mixinDTO> website_multi_mixindtos) {
        website_multi_mixinService.saveBatch(website_multi_mixinMapping.toDomain(website_multi_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"多网站的Mixin" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/website_multi_mixins/fetchdefault")
	public ResponseEntity<List<Website_multi_mixinDTO>> fetchDefault(Website_multi_mixinSearchContext context) {
        Page<Website_multi_mixin> domains = website_multi_mixinService.searchDefault(context) ;
        List<Website_multi_mixinDTO> list = website_multi_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_multi_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"多网站的Mixin" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/website_multi_mixins/searchdefault")
	public ResponseEntity<Page<Website_multi_mixinDTO>> searchDefault(@RequestBody Website_multi_mixinSearchContext context) {
        Page<Website_multi_mixin> domains = website_multi_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_multi_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

