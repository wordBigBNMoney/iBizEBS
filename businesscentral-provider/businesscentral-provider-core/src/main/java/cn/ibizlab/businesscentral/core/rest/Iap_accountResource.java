package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.businesscentral.core.odoo_iap.service.IIap_accountService;
import cn.ibizlab.businesscentral.core.odoo_iap.filter.Iap_accountSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"IAP 账户" })
@RestController("Core-iap_account")
@RequestMapping("")
public class Iap_accountResource {

    @Autowired
    public IIap_accountService iap_accountService;

    @Autowired
    @Lazy
    public Iap_accountMapping iap_accountMapping;

    @PreAuthorize("hasPermission(this.iap_accountMapping.toDomain(#iap_accountdto),'iBizBusinessCentral-Iap_account-Create')")
    @ApiOperation(value = "新建IAP 账户", tags = {"IAP 账户" },  notes = "新建IAP 账户")
	@RequestMapping(method = RequestMethod.POST, value = "/iap_accounts")
    public ResponseEntity<Iap_accountDTO> create(@Validated @RequestBody Iap_accountDTO iap_accountdto) {
        Iap_account domain = iap_accountMapping.toDomain(iap_accountdto);
		iap_accountService.create(domain);
        Iap_accountDTO dto = iap_accountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.iap_accountMapping.toDomain(#iap_accountdtos),'iBizBusinessCentral-Iap_account-Create')")
    @ApiOperation(value = "批量新建IAP 账户", tags = {"IAP 账户" },  notes = "批量新建IAP 账户")
	@RequestMapping(method = RequestMethod.POST, value = "/iap_accounts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Iap_accountDTO> iap_accountdtos) {
        iap_accountService.createBatch(iap_accountMapping.toDomain(iap_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "iap_account" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.iap_accountService.get(#iap_account_id),'iBizBusinessCentral-Iap_account-Update')")
    @ApiOperation(value = "更新IAP 账户", tags = {"IAP 账户" },  notes = "更新IAP 账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/iap_accounts/{iap_account_id}")
    public ResponseEntity<Iap_accountDTO> update(@PathVariable("iap_account_id") Long iap_account_id, @RequestBody Iap_accountDTO iap_accountdto) {
		Iap_account domain  = iap_accountMapping.toDomain(iap_accountdto);
        domain .setId(iap_account_id);
		iap_accountService.update(domain );
		Iap_accountDTO dto = iap_accountMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.iap_accountService.getIapAccountByEntities(this.iap_accountMapping.toDomain(#iap_accountdtos)),'iBizBusinessCentral-Iap_account-Update')")
    @ApiOperation(value = "批量更新IAP 账户", tags = {"IAP 账户" },  notes = "批量更新IAP 账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/iap_accounts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Iap_accountDTO> iap_accountdtos) {
        iap_accountService.updateBatch(iap_accountMapping.toDomain(iap_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.iap_accountService.get(#iap_account_id),'iBizBusinessCentral-Iap_account-Remove')")
    @ApiOperation(value = "删除IAP 账户", tags = {"IAP 账户" },  notes = "删除IAP 账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/iap_accounts/{iap_account_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("iap_account_id") Long iap_account_id) {
         return ResponseEntity.status(HttpStatus.OK).body(iap_accountService.remove(iap_account_id));
    }

    @PreAuthorize("hasPermission(this.iap_accountService.getIapAccountByIds(#ids),'iBizBusinessCentral-Iap_account-Remove')")
    @ApiOperation(value = "批量删除IAP 账户", tags = {"IAP 账户" },  notes = "批量删除IAP 账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/iap_accounts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        iap_accountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.iap_accountMapping.toDomain(returnObject.body),'iBizBusinessCentral-Iap_account-Get')")
    @ApiOperation(value = "获取IAP 账户", tags = {"IAP 账户" },  notes = "获取IAP 账户")
	@RequestMapping(method = RequestMethod.GET, value = "/iap_accounts/{iap_account_id}")
    public ResponseEntity<Iap_accountDTO> get(@PathVariable("iap_account_id") Long iap_account_id) {
        Iap_account domain = iap_accountService.get(iap_account_id);
        Iap_accountDTO dto = iap_accountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取IAP 账户草稿", tags = {"IAP 账户" },  notes = "获取IAP 账户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/iap_accounts/getdraft")
    public ResponseEntity<Iap_accountDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(iap_accountMapping.toDto(iap_accountService.getDraft(new Iap_account())));
    }

    @ApiOperation(value = "检查IAP 账户", tags = {"IAP 账户" },  notes = "检查IAP 账户")
	@RequestMapping(method = RequestMethod.POST, value = "/iap_accounts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Iap_accountDTO iap_accountdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(iap_accountService.checkKey(iap_accountMapping.toDomain(iap_accountdto)));
    }

    @PreAuthorize("hasPermission(this.iap_accountMapping.toDomain(#iap_accountdto),'iBizBusinessCentral-Iap_account-Save')")
    @ApiOperation(value = "保存IAP 账户", tags = {"IAP 账户" },  notes = "保存IAP 账户")
	@RequestMapping(method = RequestMethod.POST, value = "/iap_accounts/save")
    public ResponseEntity<Boolean> save(@RequestBody Iap_accountDTO iap_accountdto) {
        return ResponseEntity.status(HttpStatus.OK).body(iap_accountService.save(iap_accountMapping.toDomain(iap_accountdto)));
    }

    @PreAuthorize("hasPermission(this.iap_accountMapping.toDomain(#iap_accountdtos),'iBizBusinessCentral-Iap_account-Save')")
    @ApiOperation(value = "批量保存IAP 账户", tags = {"IAP 账户" },  notes = "批量保存IAP 账户")
	@RequestMapping(method = RequestMethod.POST, value = "/iap_accounts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Iap_accountDTO> iap_accountdtos) {
        iap_accountService.saveBatch(iap_accountMapping.toDomain(iap_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Iap_account-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Iap_account-Get')")
	@ApiOperation(value = "获取数据集", tags = {"IAP 账户" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/iap_accounts/fetchdefault")
	public ResponseEntity<List<Iap_accountDTO>> fetchDefault(Iap_accountSearchContext context) {
        Page<Iap_account> domains = iap_accountService.searchDefault(context) ;
        List<Iap_accountDTO> list = iap_accountMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Iap_account-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Iap_account-Get')")
	@ApiOperation(value = "查询数据集", tags = {"IAP 账户" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/iap_accounts/searchdefault")
	public ResponseEntity<Page<Iap_accountDTO>> searchDefault(@RequestBody Iap_accountSearchContext context) {
        Page<Iap_account> domains = iap_accountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(iap_accountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

