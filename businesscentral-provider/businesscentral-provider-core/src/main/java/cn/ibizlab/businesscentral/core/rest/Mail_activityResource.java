package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activityService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_activitySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动" })
@RestController("Core-mail_activity")
@RequestMapping("")
public class Mail_activityResource {

    @Autowired
    public IMail_activityService mail_activityService;

    @Autowired
    @Lazy
    public Mail_activityMapping mail_activityMapping;

    @PreAuthorize("hasPermission(this.mail_activityMapping.toDomain(#mail_activitydto),'iBizBusinessCentral-Mail_activity-Create')")
    @ApiOperation(value = "新建活动", tags = {"活动" },  notes = "新建活动")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities")
    public ResponseEntity<Mail_activityDTO> create(@Validated @RequestBody Mail_activityDTO mail_activitydto) {
        Mail_activity domain = mail_activityMapping.toDomain(mail_activitydto);
		mail_activityService.create(domain);
        Mail_activityDTO dto = mail_activityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_activityMapping.toDomain(#mail_activitydtos),'iBizBusinessCentral-Mail_activity-Create')")
    @ApiOperation(value = "批量新建活动", tags = {"活动" },  notes = "批量新建活动")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_activityDTO> mail_activitydtos) {
        mail_activityService.createBatch(mail_activityMapping.toDomain(mail_activitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_activity" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_activityService.get(#mail_activity_id),'iBizBusinessCentral-Mail_activity-Update')")
    @ApiOperation(value = "更新活动", tags = {"活动" },  notes = "更新活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activities/{mail_activity_id}")
    public ResponseEntity<Mail_activityDTO> update(@PathVariable("mail_activity_id") Long mail_activity_id, @RequestBody Mail_activityDTO mail_activitydto) {
		Mail_activity domain  = mail_activityMapping.toDomain(mail_activitydto);
        domain .setId(mail_activity_id);
		mail_activityService.update(domain );
		Mail_activityDTO dto = mail_activityMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_activityService.getMailActivityByEntities(this.mail_activityMapping.toDomain(#mail_activitydtos)),'iBizBusinessCentral-Mail_activity-Update')")
    @ApiOperation(value = "批量更新活动", tags = {"活动" },  notes = "批量更新活动")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activities/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_activityDTO> mail_activitydtos) {
        mail_activityService.updateBatch(mail_activityMapping.toDomain(mail_activitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_activityService.get(#mail_activity_id),'iBizBusinessCentral-Mail_activity-Remove')")
    @ApiOperation(value = "删除活动", tags = {"活动" },  notes = "删除活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activities/{mail_activity_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_id") Long mail_activity_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_activityService.remove(mail_activity_id));
    }

    @PreAuthorize("hasPermission(this.mail_activityService.getMailActivityByIds(#ids),'iBizBusinessCentral-Mail_activity-Remove')")
    @ApiOperation(value = "批量删除活动", tags = {"活动" },  notes = "批量删除活动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activities/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_activityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_activityMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_activity-Get')")
    @ApiOperation(value = "获取活动", tags = {"活动" },  notes = "获取活动")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activities/{mail_activity_id}")
    public ResponseEntity<Mail_activityDTO> get(@PathVariable("mail_activity_id") Long mail_activity_id) {
        Mail_activity domain = mail_activityService.get(mail_activity_id);
        Mail_activityDTO dto = mail_activityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动草稿", tags = {"活动" },  notes = "获取活动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activities/getdraft")
    public ResponseEntity<Mail_activityDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activityMapping.toDto(mail_activityService.getDraft(new Mail_activity())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity-Action_done-all')")
    @ApiOperation(value = "完成", tags = {"活动" },  notes = "完成")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/{mail_activity_id}/action_done")
    public ResponseEntity<Mail_activityDTO> action_done(@PathVariable("mail_activity_id") Long mail_activity_id, @RequestBody Mail_activityDTO mail_activitydto) {
        Mail_activity domain = mail_activityMapping.toDomain(mail_activitydto);
        domain.setId(mail_activity_id);
        domain = mail_activityService.action_done(domain);
        mail_activitydto = mail_activityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activitydto);
    }

    @ApiOperation(value = "检查活动", tags = {"活动" },  notes = "检查活动")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activityDTO mail_activitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_activityService.checkKey(mail_activityMapping.toDomain(mail_activitydto)));
    }

    @PreAuthorize("hasPermission(this.mail_activityMapping.toDomain(#mail_activitydto),'iBizBusinessCentral-Mail_activity-Save')")
    @ApiOperation(value = "保存活动", tags = {"活动" },  notes = "保存活动")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_activityDTO mail_activitydto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activityService.save(mail_activityMapping.toDomain(mail_activitydto)));
    }

    @PreAuthorize("hasPermission(this.mail_activityMapping.toDomain(#mail_activitydtos),'iBizBusinessCentral-Mail_activity-Save')")
    @ApiOperation(value = "批量保存活动", tags = {"活动" },  notes = "批量保存活动")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_activityDTO> mail_activitydtos) {
        mail_activityService.saveBatch(mail_activityMapping.toDomain(mail_activitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_activity-Get')")
	@ApiOperation(value = "获取数据集", tags = {"活动" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_activities/fetchdefault")
	public ResponseEntity<List<Mail_activityDTO>> fetchDefault(Mail_activitySearchContext context) {
        Page<Mail_activity> domains = mail_activityService.searchDefault(context) ;
        List<Mail_activityDTO> list = mail_activityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_activity-Get')")
	@ApiOperation(value = "查询数据集", tags = {"活动" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_activities/searchdefault")
	public ResponseEntity<Page<Mail_activityDTO>> searchDefault(@RequestBody Mail_activitySearchContext context) {
        Page<Mail_activity> domains = mail_activityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_activityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

