package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.businesscentral.core.dto.Maintenance_requestDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMaintenance_requestMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Maintenance_requestMapping extends MappingBase<Maintenance_requestDTO, Maintenance_request> {


}

