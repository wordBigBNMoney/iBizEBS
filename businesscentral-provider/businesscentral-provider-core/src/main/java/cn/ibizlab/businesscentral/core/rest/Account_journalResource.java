package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_journalSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"日记账" })
@RestController("Core-account_journal")
@RequestMapping("")
public class Account_journalResource {

    @Autowired
    public IAccount_journalService account_journalService;

    @Autowired
    @Lazy
    public Account_journalMapping account_journalMapping;

    @PreAuthorize("hasPermission(this.account_journalMapping.toDomain(#account_journaldto),'iBizBusinessCentral-Account_journal-Create')")
    @ApiOperation(value = "新建日记账", tags = {"日记账" },  notes = "新建日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals")
    public ResponseEntity<Account_journalDTO> create(@Validated @RequestBody Account_journalDTO account_journaldto) {
        Account_journal domain = account_journalMapping.toDomain(account_journaldto);
		account_journalService.create(domain);
        Account_journalDTO dto = account_journalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_journalMapping.toDomain(#account_journaldtos),'iBizBusinessCentral-Account_journal-Create')")
    @ApiOperation(value = "批量新建日记账", tags = {"日记账" },  notes = "批量新建日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_journalDTO> account_journaldtos) {
        account_journalService.createBatch(account_journalMapping.toDomain(account_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_journal" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_journalService.get(#account_journal_id),'iBizBusinessCentral-Account_journal-Update')")
    @ApiOperation(value = "更新日记账", tags = {"日记账" },  notes = "更新日记账")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_journals/{account_journal_id}")
    public ResponseEntity<Account_journalDTO> update(@PathVariable("account_journal_id") Long account_journal_id, @RequestBody Account_journalDTO account_journaldto) {
		Account_journal domain  = account_journalMapping.toDomain(account_journaldto);
        domain .setId(account_journal_id);
		account_journalService.update(domain );
		Account_journalDTO dto = account_journalMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_journalService.getAccountJournalByEntities(this.account_journalMapping.toDomain(#account_journaldtos)),'iBizBusinessCentral-Account_journal-Update')")
    @ApiOperation(value = "批量更新日记账", tags = {"日记账" },  notes = "批量更新日记账")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_journals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_journalDTO> account_journaldtos) {
        account_journalService.updateBatch(account_journalMapping.toDomain(account_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_journalService.get(#account_journal_id),'iBizBusinessCentral-Account_journal-Remove')")
    @ApiOperation(value = "删除日记账", tags = {"日记账" },  notes = "删除日记账")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_journals/{account_journal_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_journal_id") Long account_journal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_journalService.remove(account_journal_id));
    }

    @PreAuthorize("hasPermission(this.account_journalService.getAccountJournalByIds(#ids),'iBizBusinessCentral-Account_journal-Remove')")
    @ApiOperation(value = "批量删除日记账", tags = {"日记账" },  notes = "批量删除日记账")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_journals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_journalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_journalMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_journal-Get')")
    @ApiOperation(value = "获取日记账", tags = {"日记账" },  notes = "获取日记账")
	@RequestMapping(method = RequestMethod.GET, value = "/account_journals/{account_journal_id}")
    public ResponseEntity<Account_journalDTO> get(@PathVariable("account_journal_id") Long account_journal_id) {
        Account_journal domain = account_journalService.get(account_journal_id);
        Account_journalDTO dto = account_journalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取日记账草稿", tags = {"日记账" },  notes = "获取日记账草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_journals/getdraft")
    public ResponseEntity<Account_journalDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_journalMapping.toDto(account_journalService.getDraft(new Account_journal())));
    }

    @ApiOperation(value = "检查日记账", tags = {"日记账" },  notes = "检查日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_journalDTO account_journaldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_journalService.checkKey(account_journalMapping.toDomain(account_journaldto)));
    }

    @PreAuthorize("hasPermission(this.account_journalMapping.toDomain(#account_journaldto),'iBizBusinessCentral-Account_journal-Save')")
    @ApiOperation(value = "保存日记账", tags = {"日记账" },  notes = "保存日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_journalDTO account_journaldto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_journalService.save(account_journalMapping.toDomain(account_journaldto)));
    }

    @PreAuthorize("hasPermission(this.account_journalMapping.toDomain(#account_journaldtos),'iBizBusinessCentral-Account_journal-Save')")
    @ApiOperation(value = "批量保存日记账", tags = {"日记账" },  notes = "批量保存日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_journalDTO> account_journaldtos) {
        account_journalService.saveBatch(account_journalMapping.toDomain(account_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_journal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_journal-Get')")
	@ApiOperation(value = "获取数据集", tags = {"日记账" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_journals/fetchdefault")
	public ResponseEntity<List<Account_journalDTO>> fetchDefault(Account_journalSearchContext context) {
        Page<Account_journal> domains = account_journalService.searchDefault(context) ;
        List<Account_journalDTO> list = account_journalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_journal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_journal-Get')")
	@ApiOperation(value = "查询数据集", tags = {"日记账" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_journals/searchdefault")
	public ResponseEntity<Page<Account_journalDTO>> searchDefault(@RequestBody Account_journalSearchContext context) {
        Page<Account_journal> domains = account_journalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_journalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

