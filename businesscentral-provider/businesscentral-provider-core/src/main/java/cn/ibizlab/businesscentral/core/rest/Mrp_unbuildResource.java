package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_unbuildSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"拆解单" })
@RestController("Core-mrp_unbuild")
@RequestMapping("")
public class Mrp_unbuildResource {

    @Autowired
    public IMrp_unbuildService mrp_unbuildService;

    @Autowired
    @Lazy
    public Mrp_unbuildMapping mrp_unbuildMapping;

    @PreAuthorize("hasPermission(this.mrp_unbuildMapping.toDomain(#mrp_unbuilddto),'iBizBusinessCentral-Mrp_unbuild-Create')")
    @ApiOperation(value = "新建拆解单", tags = {"拆解单" },  notes = "新建拆解单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds")
    public ResponseEntity<Mrp_unbuildDTO> create(@Validated @RequestBody Mrp_unbuildDTO mrp_unbuilddto) {
        Mrp_unbuild domain = mrp_unbuildMapping.toDomain(mrp_unbuilddto);
		mrp_unbuildService.create(domain);
        Mrp_unbuildDTO dto = mrp_unbuildMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_unbuildMapping.toDomain(#mrp_unbuilddtos),'iBizBusinessCentral-Mrp_unbuild-Create')")
    @ApiOperation(value = "批量新建拆解单", tags = {"拆解单" },  notes = "批量新建拆解单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_unbuildDTO> mrp_unbuilddtos) {
        mrp_unbuildService.createBatch(mrp_unbuildMapping.toDomain(mrp_unbuilddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_unbuild" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_unbuildService.get(#mrp_unbuild_id),'iBizBusinessCentral-Mrp_unbuild-Update')")
    @ApiOperation(value = "更新拆解单", tags = {"拆解单" },  notes = "更新拆解单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_unbuilds/{mrp_unbuild_id}")
    public ResponseEntity<Mrp_unbuildDTO> update(@PathVariable("mrp_unbuild_id") Long mrp_unbuild_id, @RequestBody Mrp_unbuildDTO mrp_unbuilddto) {
		Mrp_unbuild domain  = mrp_unbuildMapping.toDomain(mrp_unbuilddto);
        domain .setId(mrp_unbuild_id);
		mrp_unbuildService.update(domain );
		Mrp_unbuildDTO dto = mrp_unbuildMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_unbuildService.getMrpUnbuildByEntities(this.mrp_unbuildMapping.toDomain(#mrp_unbuilddtos)),'iBizBusinessCentral-Mrp_unbuild-Update')")
    @ApiOperation(value = "批量更新拆解单", tags = {"拆解单" },  notes = "批量更新拆解单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_unbuilds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_unbuildDTO> mrp_unbuilddtos) {
        mrp_unbuildService.updateBatch(mrp_unbuildMapping.toDomain(mrp_unbuilddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_unbuildService.get(#mrp_unbuild_id),'iBizBusinessCentral-Mrp_unbuild-Remove')")
    @ApiOperation(value = "删除拆解单", tags = {"拆解单" },  notes = "删除拆解单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_unbuilds/{mrp_unbuild_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_unbuild_id") Long mrp_unbuild_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_unbuildService.remove(mrp_unbuild_id));
    }

    @PreAuthorize("hasPermission(this.mrp_unbuildService.getMrpUnbuildByIds(#ids),'iBizBusinessCentral-Mrp_unbuild-Remove')")
    @ApiOperation(value = "批量删除拆解单", tags = {"拆解单" },  notes = "批量删除拆解单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_unbuilds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_unbuildService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_unbuildMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_unbuild-Get')")
    @ApiOperation(value = "获取拆解单", tags = {"拆解单" },  notes = "获取拆解单")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/{mrp_unbuild_id}")
    public ResponseEntity<Mrp_unbuildDTO> get(@PathVariable("mrp_unbuild_id") Long mrp_unbuild_id) {
        Mrp_unbuild domain = mrp_unbuildService.get(mrp_unbuild_id);
        Mrp_unbuildDTO dto = mrp_unbuildMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取拆解单草稿", tags = {"拆解单" },  notes = "获取拆解单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/getdraft")
    public ResponseEntity<Mrp_unbuildDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_unbuildMapping.toDto(mrp_unbuildService.getDraft(new Mrp_unbuild())));
    }

    @ApiOperation(value = "检查拆解单", tags = {"拆解单" },  notes = "检查拆解单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_unbuildDTO mrp_unbuilddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_unbuildService.checkKey(mrp_unbuildMapping.toDomain(mrp_unbuilddto)));
    }

    @PreAuthorize("hasPermission(this.mrp_unbuildMapping.toDomain(#mrp_unbuilddto),'iBizBusinessCentral-Mrp_unbuild-Save')")
    @ApiOperation(value = "保存拆解单", tags = {"拆解单" },  notes = "保存拆解单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_unbuildDTO mrp_unbuilddto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_unbuildService.save(mrp_unbuildMapping.toDomain(mrp_unbuilddto)));
    }

    @PreAuthorize("hasPermission(this.mrp_unbuildMapping.toDomain(#mrp_unbuilddtos),'iBizBusinessCentral-Mrp_unbuild-Save')")
    @ApiOperation(value = "批量保存拆解单", tags = {"拆解单" },  notes = "批量保存拆解单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_unbuildDTO> mrp_unbuilddtos) {
        mrp_unbuildService.saveBatch(mrp_unbuildMapping.toDomain(mrp_unbuilddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_unbuild-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_unbuild-Get')")
	@ApiOperation(value = "获取数据集", tags = {"拆解单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_unbuilds/fetchdefault")
	public ResponseEntity<List<Mrp_unbuildDTO>> fetchDefault(Mrp_unbuildSearchContext context) {
        Page<Mrp_unbuild> domains = mrp_unbuildService.searchDefault(context) ;
        List<Mrp_unbuildDTO> list = mrp_unbuildMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_unbuild-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_unbuild-Get')")
	@ApiOperation(value = "查询数据集", tags = {"拆解单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_unbuilds/searchdefault")
	public ResponseEntity<Page<Mrp_unbuildDTO>> searchDefault(@RequestBody Mrp_unbuildSearchContext context) {
        Page<Mrp_unbuild> domains = mrp_unbuildService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_unbuildMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

