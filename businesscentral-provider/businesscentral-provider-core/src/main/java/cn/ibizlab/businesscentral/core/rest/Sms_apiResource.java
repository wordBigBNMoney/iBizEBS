package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.businesscentral.core.odoo_sms.service.ISms_apiService;
import cn.ibizlab.businesscentral.core.odoo_sms.filter.Sms_apiSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"短信API" })
@RestController("Core-sms_api")
@RequestMapping("")
public class Sms_apiResource {

    @Autowired
    public ISms_apiService sms_apiService;

    @Autowired
    @Lazy
    public Sms_apiMapping sms_apiMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Create-all')")
    @ApiOperation(value = "新建短信API", tags = {"短信API" },  notes = "新建短信API")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_apis")
    public ResponseEntity<Sms_apiDTO> create(@Validated @RequestBody Sms_apiDTO sms_apidto) {
        Sms_api domain = sms_apiMapping.toDomain(sms_apidto);
		sms_apiService.create(domain);
        Sms_apiDTO dto = sms_apiMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Create-all')")
    @ApiOperation(value = "批量新建短信API", tags = {"短信API" },  notes = "批量新建短信API")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_apis/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sms_apiDTO> sms_apidtos) {
        sms_apiService.createBatch(sms_apiMapping.toDomain(sms_apidtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Update-all')")
    @ApiOperation(value = "更新短信API", tags = {"短信API" },  notes = "更新短信API")
	@RequestMapping(method = RequestMethod.PUT, value = "/sms_apis/{sms_api_id}")
    public ResponseEntity<Sms_apiDTO> update(@PathVariable("sms_api_id") Long sms_api_id, @RequestBody Sms_apiDTO sms_apidto) {
		Sms_api domain  = sms_apiMapping.toDomain(sms_apidto);
        domain .setId(sms_api_id);
		sms_apiService.update(domain );
		Sms_apiDTO dto = sms_apiMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Update-all')")
    @ApiOperation(value = "批量更新短信API", tags = {"短信API" },  notes = "批量更新短信API")
	@RequestMapping(method = RequestMethod.PUT, value = "/sms_apis/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sms_apiDTO> sms_apidtos) {
        sms_apiService.updateBatch(sms_apiMapping.toDomain(sms_apidtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Remove-all')")
    @ApiOperation(value = "删除短信API", tags = {"短信API" },  notes = "删除短信API")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sms_apis/{sms_api_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sms_api_id") Long sms_api_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sms_apiService.remove(sms_api_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Remove-all')")
    @ApiOperation(value = "批量删除短信API", tags = {"短信API" },  notes = "批量删除短信API")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sms_apis/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sms_apiService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Get-all')")
    @ApiOperation(value = "获取短信API", tags = {"短信API" },  notes = "获取短信API")
	@RequestMapping(method = RequestMethod.GET, value = "/sms_apis/{sms_api_id}")
    public ResponseEntity<Sms_apiDTO> get(@PathVariable("sms_api_id") Long sms_api_id) {
        Sms_api domain = sms_apiService.get(sms_api_id);
        Sms_apiDTO dto = sms_apiMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取短信API草稿", tags = {"短信API" },  notes = "获取短信API草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sms_apis/getdraft")
    public ResponseEntity<Sms_apiDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sms_apiMapping.toDto(sms_apiService.getDraft(new Sms_api())));
    }

    @ApiOperation(value = "检查短信API", tags = {"短信API" },  notes = "检查短信API")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_apis/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sms_apiDTO sms_apidto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sms_apiService.checkKey(sms_apiMapping.toDomain(sms_apidto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Save-all')")
    @ApiOperation(value = "保存短信API", tags = {"短信API" },  notes = "保存短信API")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_apis/save")
    public ResponseEntity<Boolean> save(@RequestBody Sms_apiDTO sms_apidto) {
        return ResponseEntity.status(HttpStatus.OK).body(sms_apiService.save(sms_apiMapping.toDomain(sms_apidto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-Save-all')")
    @ApiOperation(value = "批量保存短信API", tags = {"短信API" },  notes = "批量保存短信API")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_apis/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sms_apiDTO> sms_apidtos) {
        sms_apiService.saveBatch(sms_apiMapping.toDomain(sms_apidtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"短信API" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sms_apis/fetchdefault")
	public ResponseEntity<List<Sms_apiDTO>> fetchDefault(Sms_apiSearchContext context) {
        Page<Sms_api> domains = sms_apiService.searchDefault(context) ;
        List<Sms_apiDTO> list = sms_apiMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_api-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"短信API" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sms_apis/searchdefault")
	public ResponseEntity<Page<Sms_apiDTO>> searchDefault(@RequestBody Sms_apiSearchContext context) {
        Page<Sms_api> domains = sms_apiService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sms_apiMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

