package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_advance_payment_invService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售预付款发票" })
@RestController("Core-sale_advance_payment_inv")
@RequestMapping("")
public class Sale_advance_payment_invResource {

    @Autowired
    public ISale_advance_payment_invService sale_advance_payment_invService;

    @Autowired
    @Lazy
    public Sale_advance_payment_invMapping sale_advance_payment_invMapping;

    @PreAuthorize("hasPermission(this.sale_advance_payment_invMapping.toDomain(#sale_advance_payment_invdto),'iBizBusinessCentral-Sale_advance_payment_inv-Create')")
    @ApiOperation(value = "新建销售预付款发票", tags = {"销售预付款发票" },  notes = "新建销售预付款发票")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_advance_payment_invs")
    public ResponseEntity<Sale_advance_payment_invDTO> create(@Validated @RequestBody Sale_advance_payment_invDTO sale_advance_payment_invdto) {
        Sale_advance_payment_inv domain = sale_advance_payment_invMapping.toDomain(sale_advance_payment_invdto);
		sale_advance_payment_invService.create(domain);
        Sale_advance_payment_invDTO dto = sale_advance_payment_invMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_advance_payment_invMapping.toDomain(#sale_advance_payment_invdtos),'iBizBusinessCentral-Sale_advance_payment_inv-Create')")
    @ApiOperation(value = "批量新建销售预付款发票", tags = {"销售预付款发票" },  notes = "批量新建销售预付款发票")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_advance_payment_invs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_advance_payment_invDTO> sale_advance_payment_invdtos) {
        sale_advance_payment_invService.createBatch(sale_advance_payment_invMapping.toDomain(sale_advance_payment_invdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sale_advance_payment_inv" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sale_advance_payment_invService.get(#sale_advance_payment_inv_id),'iBizBusinessCentral-Sale_advance_payment_inv-Update')")
    @ApiOperation(value = "更新销售预付款发票", tags = {"销售预付款发票" },  notes = "更新销售预付款发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_advance_payment_invs/{sale_advance_payment_inv_id}")
    public ResponseEntity<Sale_advance_payment_invDTO> update(@PathVariable("sale_advance_payment_inv_id") Long sale_advance_payment_inv_id, @RequestBody Sale_advance_payment_invDTO sale_advance_payment_invdto) {
		Sale_advance_payment_inv domain  = sale_advance_payment_invMapping.toDomain(sale_advance_payment_invdto);
        domain .setId(sale_advance_payment_inv_id);
		sale_advance_payment_invService.update(domain );
		Sale_advance_payment_invDTO dto = sale_advance_payment_invMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_advance_payment_invService.getSaleAdvancePaymentInvByEntities(this.sale_advance_payment_invMapping.toDomain(#sale_advance_payment_invdtos)),'iBizBusinessCentral-Sale_advance_payment_inv-Update')")
    @ApiOperation(value = "批量更新销售预付款发票", tags = {"销售预付款发票" },  notes = "批量更新销售预付款发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_advance_payment_invs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_advance_payment_invDTO> sale_advance_payment_invdtos) {
        sale_advance_payment_invService.updateBatch(sale_advance_payment_invMapping.toDomain(sale_advance_payment_invdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sale_advance_payment_invService.get(#sale_advance_payment_inv_id),'iBizBusinessCentral-Sale_advance_payment_inv-Remove')")
    @ApiOperation(value = "删除销售预付款发票", tags = {"销售预付款发票" },  notes = "删除销售预付款发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_advance_payment_invs/{sale_advance_payment_inv_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_advance_payment_inv_id") Long sale_advance_payment_inv_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_advance_payment_invService.remove(sale_advance_payment_inv_id));
    }

    @PreAuthorize("hasPermission(this.sale_advance_payment_invService.getSaleAdvancePaymentInvByIds(#ids),'iBizBusinessCentral-Sale_advance_payment_inv-Remove')")
    @ApiOperation(value = "批量删除销售预付款发票", tags = {"销售预付款发票" },  notes = "批量删除销售预付款发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_advance_payment_invs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_advance_payment_invService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sale_advance_payment_invMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sale_advance_payment_inv-Get')")
    @ApiOperation(value = "获取销售预付款发票", tags = {"销售预付款发票" },  notes = "获取销售预付款发票")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_advance_payment_invs/{sale_advance_payment_inv_id}")
    public ResponseEntity<Sale_advance_payment_invDTO> get(@PathVariable("sale_advance_payment_inv_id") Long sale_advance_payment_inv_id) {
        Sale_advance_payment_inv domain = sale_advance_payment_invService.get(sale_advance_payment_inv_id);
        Sale_advance_payment_invDTO dto = sale_advance_payment_invMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售预付款发票草稿", tags = {"销售预付款发票" },  notes = "获取销售预付款发票草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_advance_payment_invs/getdraft")
    public ResponseEntity<Sale_advance_payment_invDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_advance_payment_invMapping.toDto(sale_advance_payment_invService.getDraft(new Sale_advance_payment_inv())));
    }

    @ApiOperation(value = "检查销售预付款发票", tags = {"销售预付款发票" },  notes = "检查销售预付款发票")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_advance_payment_invs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_advance_payment_invDTO sale_advance_payment_invdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_advance_payment_invService.checkKey(sale_advance_payment_invMapping.toDomain(sale_advance_payment_invdto)));
    }

    @PreAuthorize("hasPermission(this.sale_advance_payment_invMapping.toDomain(#sale_advance_payment_invdto),'iBizBusinessCentral-Sale_advance_payment_inv-Save')")
    @ApiOperation(value = "保存销售预付款发票", tags = {"销售预付款发票" },  notes = "保存销售预付款发票")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_advance_payment_invs/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_advance_payment_invDTO sale_advance_payment_invdto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_advance_payment_invService.save(sale_advance_payment_invMapping.toDomain(sale_advance_payment_invdto)));
    }

    @PreAuthorize("hasPermission(this.sale_advance_payment_invMapping.toDomain(#sale_advance_payment_invdtos),'iBizBusinessCentral-Sale_advance_payment_inv-Save')")
    @ApiOperation(value = "批量保存销售预付款发票", tags = {"销售预付款发票" },  notes = "批量保存销售预付款发票")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_advance_payment_invs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_advance_payment_invDTO> sale_advance_payment_invdtos) {
        sale_advance_payment_invService.saveBatch(sale_advance_payment_invMapping.toDomain(sale_advance_payment_invdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_advance_payment_inv-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_advance_payment_inv-Get')")
	@ApiOperation(value = "获取数据集", tags = {"销售预付款发票" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_advance_payment_invs/fetchdefault")
	public ResponseEntity<List<Sale_advance_payment_invDTO>> fetchDefault(Sale_advance_payment_invSearchContext context) {
        Page<Sale_advance_payment_inv> domains = sale_advance_payment_invService.searchDefault(context) ;
        List<Sale_advance_payment_invDTO> list = sale_advance_payment_invMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_advance_payment_inv-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_advance_payment_inv-Get')")
	@ApiOperation(value = "查询数据集", tags = {"销售预付款发票" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_advance_payment_invs/searchdefault")
	public ResponseEntity<Page<Sale_advance_payment_invDTO>> searchDefault(@RequestBody Sale_advance_payment_invSearchContext context) {
        Page<Sale_advance_payment_inv> domains = sale_advance_payment_invService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_advance_payment_invMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

