package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_cashmoveService;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"午餐费的现金划转" })
@RestController("Core-lunch_cashmove")
@RequestMapping("")
public class Lunch_cashmoveResource {

    @Autowired
    public ILunch_cashmoveService lunch_cashmoveService;

    @Autowired
    @Lazy
    public Lunch_cashmoveMapping lunch_cashmoveMapping;

    @PreAuthorize("hasPermission(this.lunch_cashmoveMapping.toDomain(#lunch_cashmovedto),'iBizBusinessCentral-Lunch_cashmove-Create')")
    @ApiOperation(value = "新建午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "新建午餐费的现金划转")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves")
    public ResponseEntity<Lunch_cashmoveDTO> create(@Validated @RequestBody Lunch_cashmoveDTO lunch_cashmovedto) {
        Lunch_cashmove domain = lunch_cashmoveMapping.toDomain(lunch_cashmovedto);
		lunch_cashmoveService.create(domain);
        Lunch_cashmoveDTO dto = lunch_cashmoveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_cashmoveMapping.toDomain(#lunch_cashmovedtos),'iBizBusinessCentral-Lunch_cashmove-Create')")
    @ApiOperation(value = "批量新建午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "批量新建午餐费的现金划转")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_cashmoveDTO> lunch_cashmovedtos) {
        lunch_cashmoveService.createBatch(lunch_cashmoveMapping.toDomain(lunch_cashmovedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lunch_cashmove" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.lunch_cashmoveService.get(#lunch_cashmove_id),'iBizBusinessCentral-Lunch_cashmove-Update')")
    @ApiOperation(value = "更新午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "更新午餐费的现金划转")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_cashmoves/{lunch_cashmove_id}")
    public ResponseEntity<Lunch_cashmoveDTO> update(@PathVariable("lunch_cashmove_id") Long lunch_cashmove_id, @RequestBody Lunch_cashmoveDTO lunch_cashmovedto) {
		Lunch_cashmove domain  = lunch_cashmoveMapping.toDomain(lunch_cashmovedto);
        domain .setId(lunch_cashmove_id);
		lunch_cashmoveService.update(domain );
		Lunch_cashmoveDTO dto = lunch_cashmoveMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_cashmoveService.getLunchCashmoveByEntities(this.lunch_cashmoveMapping.toDomain(#lunch_cashmovedtos)),'iBizBusinessCentral-Lunch_cashmove-Update')")
    @ApiOperation(value = "批量更新午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "批量更新午餐费的现金划转")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_cashmoves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_cashmoveDTO> lunch_cashmovedtos) {
        lunch_cashmoveService.updateBatch(lunch_cashmoveMapping.toDomain(lunch_cashmovedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.lunch_cashmoveService.get(#lunch_cashmove_id),'iBizBusinessCentral-Lunch_cashmove-Remove')")
    @ApiOperation(value = "删除午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "删除午餐费的现金划转")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_cashmoves/{lunch_cashmove_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("lunch_cashmove_id") Long lunch_cashmove_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_cashmoveService.remove(lunch_cashmove_id));
    }

    @PreAuthorize("hasPermission(this.lunch_cashmoveService.getLunchCashmoveByIds(#ids),'iBizBusinessCentral-Lunch_cashmove-Remove')")
    @ApiOperation(value = "批量删除午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "批量删除午餐费的现金划转")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_cashmoves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        lunch_cashmoveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.lunch_cashmoveMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lunch_cashmove-Get')")
    @ApiOperation(value = "获取午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "获取午餐费的现金划转")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_cashmoves/{lunch_cashmove_id}")
    public ResponseEntity<Lunch_cashmoveDTO> get(@PathVariable("lunch_cashmove_id") Long lunch_cashmove_id) {
        Lunch_cashmove domain = lunch_cashmoveService.get(lunch_cashmove_id);
        Lunch_cashmoveDTO dto = lunch_cashmoveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取午餐费的现金划转草稿", tags = {"午餐费的现金划转" },  notes = "获取午餐费的现金划转草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_cashmoves/getdraft")
    public ResponseEntity<Lunch_cashmoveDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_cashmoveMapping.toDto(lunch_cashmoveService.getDraft(new Lunch_cashmove())));
    }

    @ApiOperation(value = "检查午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "检查午餐费的现金划转")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Lunch_cashmoveDTO lunch_cashmovedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(lunch_cashmoveService.checkKey(lunch_cashmoveMapping.toDomain(lunch_cashmovedto)));
    }

    @PreAuthorize("hasPermission(this.lunch_cashmoveMapping.toDomain(#lunch_cashmovedto),'iBizBusinessCentral-Lunch_cashmove-Save')")
    @ApiOperation(value = "保存午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "保存午餐费的现金划转")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/save")
    public ResponseEntity<Boolean> save(@RequestBody Lunch_cashmoveDTO lunch_cashmovedto) {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_cashmoveService.save(lunch_cashmoveMapping.toDomain(lunch_cashmovedto)));
    }

    @PreAuthorize("hasPermission(this.lunch_cashmoveMapping.toDomain(#lunch_cashmovedtos),'iBizBusinessCentral-Lunch_cashmove-Save')")
    @ApiOperation(value = "批量保存午餐费的现金划转", tags = {"午餐费的现金划转" },  notes = "批量保存午餐费的现金划转")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Lunch_cashmoveDTO> lunch_cashmovedtos) {
        lunch_cashmoveService.saveBatch(lunch_cashmoveMapping.toDomain(lunch_cashmovedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_cashmove-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_cashmove-Get')")
	@ApiOperation(value = "获取数据集", tags = {"午餐费的现金划转" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_cashmoves/fetchdefault")
	public ResponseEntity<List<Lunch_cashmoveDTO>> fetchDefault(Lunch_cashmoveSearchContext context) {
        Page<Lunch_cashmove> domains = lunch_cashmoveService.searchDefault(context) ;
        List<Lunch_cashmoveDTO> list = lunch_cashmoveMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_cashmove-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_cashmove-Get')")
	@ApiOperation(value = "查询数据集", tags = {"午餐费的现金划转" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/lunch_cashmoves/searchdefault")
	public ResponseEntity<Page<Lunch_cashmoveDTO>> searchDefault(@RequestBody Lunch_cashmoveSearchContext context) {
        Page<Lunch_cashmove> domains = lunch_cashmoveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_cashmoveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

