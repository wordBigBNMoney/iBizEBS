package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_followersService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_followersSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"文档关注者" })
@RestController("Core-mail_followers")
@RequestMapping("")
public class Mail_followersResource {

    @Autowired
    public IMail_followersService mail_followersService;

    @Autowired
    @Lazy
    public Mail_followersMapping mail_followersMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Create-all')")
    @ApiOperation(value = "新建文档关注者", tags = {"文档关注者" },  notes = "新建文档关注者")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers")
    public ResponseEntity<Mail_followersDTO> create(@Validated @RequestBody Mail_followersDTO mail_followersdto) {
        Mail_followers domain = mail_followersMapping.toDomain(mail_followersdto);
		mail_followersService.create(domain);
        Mail_followersDTO dto = mail_followersMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Create-all')")
    @ApiOperation(value = "批量新建文档关注者", tags = {"文档关注者" },  notes = "批量新建文档关注者")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_followersDTO> mail_followersdtos) {
        mail_followersService.createBatch(mail_followersMapping.toDomain(mail_followersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Update-all')")
    @ApiOperation(value = "更新文档关注者", tags = {"文档关注者" },  notes = "更新文档关注者")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_followers/{mail_followers_id}")
    public ResponseEntity<Mail_followersDTO> update(@PathVariable("mail_followers_id") Long mail_followers_id, @RequestBody Mail_followersDTO mail_followersdto) {
		Mail_followers domain  = mail_followersMapping.toDomain(mail_followersdto);
        domain .setId(mail_followers_id);
		mail_followersService.update(domain );
		Mail_followersDTO dto = mail_followersMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Update-all')")
    @ApiOperation(value = "批量更新文档关注者", tags = {"文档关注者" },  notes = "批量更新文档关注者")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_followers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_followersDTO> mail_followersdtos) {
        mail_followersService.updateBatch(mail_followersMapping.toDomain(mail_followersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Remove-all')")
    @ApiOperation(value = "删除文档关注者", tags = {"文档关注者" },  notes = "删除文档关注者")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_followers/{mail_followers_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_followers_id") Long mail_followers_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_followersService.remove(mail_followers_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Remove-all')")
    @ApiOperation(value = "批量删除文档关注者", tags = {"文档关注者" },  notes = "批量删除文档关注者")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_followers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_followersService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Get-all')")
    @ApiOperation(value = "获取文档关注者", tags = {"文档关注者" },  notes = "获取文档关注者")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_followers/{mail_followers_id}")
    public ResponseEntity<Mail_followersDTO> get(@PathVariable("mail_followers_id") Long mail_followers_id) {
        Mail_followers domain = mail_followersService.get(mail_followers_id);
        Mail_followersDTO dto = mail_followersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取文档关注者草稿", tags = {"文档关注者" },  notes = "获取文档关注者草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_followers/getdraft")
    public ResponseEntity<Mail_followersDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_followersMapping.toDto(mail_followersService.getDraft(new Mail_followers())));
    }

    @ApiOperation(value = "检查文档关注者", tags = {"文档关注者" },  notes = "检查文档关注者")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_followersDTO mail_followersdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_followersService.checkKey(mail_followersMapping.toDomain(mail_followersdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Save-all')")
    @ApiOperation(value = "保存文档关注者", tags = {"文档关注者" },  notes = "保存文档关注者")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_followersDTO mail_followersdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_followersService.save(mail_followersMapping.toDomain(mail_followersdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-Save-all')")
    @ApiOperation(value = "批量保存文档关注者", tags = {"文档关注者" },  notes = "批量保存文档关注者")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_followersDTO> mail_followersdtos) {
        mail_followersService.saveBatch(mail_followersMapping.toDomain(mail_followersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"文档关注者" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_followers/fetchdefault")
	public ResponseEntity<List<Mail_followersDTO>> fetchDefault(Mail_followersSearchContext context) {
        Page<Mail_followers> domains = mail_followersService.searchDefault(context) ;
        List<Mail_followersDTO> list = mail_followersMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"文档关注者" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_followers/searchdefault")
	public ResponseEntity<Page<Mail_followersDTO>> searchDefault(@RequestBody Mail_followersSearchContext context) {
        Page<Mail_followers> domains = mail_followersService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_followersMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

