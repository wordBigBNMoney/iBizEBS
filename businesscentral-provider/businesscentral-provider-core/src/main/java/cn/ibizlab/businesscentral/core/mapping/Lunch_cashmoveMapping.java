package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.businesscentral.core.dto.Lunch_cashmoveDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreLunch_cashmoveMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Lunch_cashmoveMapping extends MappingBase<Lunch_cashmoveDTO, Lunch_cashmove> {


}

