package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mro_pm_rule_lineDTO]
 */
@Data
public class Mro_pm_rule_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PM_RULE_ID_TEXT]
     *
     */
    @JSONField(name = "pm_rule_id_text")
    @JsonProperty("pm_rule_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pmRuleIdText;

    /**
     * 属性 [METER_INTERVAL_ID_TEXT]
     *
     */
    @JSONField(name = "meter_interval_id_text")
    @JsonProperty("meter_interval_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String meterIntervalIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [TASK_ID_TEXT]
     *
     */
    @JSONField(name = "task_id_text")
    @JsonProperty("task_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String taskIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [TASK_ID]
     *
     */
    @JSONField(name = "task_id")
    @JsonProperty("task_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[Task]不允许为空!")
    private Long taskId;

    /**
     * 属性 [METER_INTERVAL_ID]
     *
     */
    @JSONField(name = "meter_interval_id")
    @JsonProperty("meter_interval_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[Meter Interval]不允许为空!")
    private Long meterIntervalId;

    /**
     * 属性 [PM_RULE_ID]
     *
     */
    @JSONField(name = "pm_rule_id")
    @JsonProperty("pm_rule_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pmRuleId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [TASK_ID]
     */
    public void setTaskId(Long  taskId){
        this.taskId = taskId ;
        this.modify("task_id",taskId);
    }

    /**
     * 设置 [METER_INTERVAL_ID]
     */
    public void setMeterIntervalId(Long  meterIntervalId){
        this.meterIntervalId = meterIntervalId ;
        this.modify("meter_interval_id",meterIntervalId);
    }

    /**
     * 设置 [PM_RULE_ID]
     */
    public void setPmRuleId(Long  pmRuleId){
        this.pmRuleId = pmRuleId ;
        this.modify("pm_rule_id",pmRuleId);
    }


}


