package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_import_wizardService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"从文件中导入你的供应商账单。" })
@RestController("Core-account_invoice_import_wizard")
@RequestMapping("")
public class Account_invoice_import_wizardResource {

    @Autowired
    public IAccount_invoice_import_wizardService account_invoice_import_wizardService;

    @Autowired
    @Lazy
    public Account_invoice_import_wizardMapping account_invoice_import_wizardMapping;

    @PreAuthorize("hasPermission(this.account_invoice_import_wizardMapping.toDomain(#account_invoice_import_wizarddto),'iBizBusinessCentral-Account_invoice_import_wizard-Create')")
    @ApiOperation(value = "新建从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "新建从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards")
    public ResponseEntity<Account_invoice_import_wizardDTO> create(@Validated @RequestBody Account_invoice_import_wizardDTO account_invoice_import_wizarddto) {
        Account_invoice_import_wizard domain = account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddto);
		account_invoice_import_wizardService.create(domain);
        Account_invoice_import_wizardDTO dto = account_invoice_import_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_import_wizardMapping.toDomain(#account_invoice_import_wizarddtos),'iBizBusinessCentral-Account_invoice_import_wizard-Create')")
    @ApiOperation(value = "批量新建从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "批量新建从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_import_wizardDTO> account_invoice_import_wizarddtos) {
        account_invoice_import_wizardService.createBatch(account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_invoice_import_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_invoice_import_wizardService.get(#account_invoice_import_wizard_id),'iBizBusinessCentral-Account_invoice_import_wizard-Update')")
    @ApiOperation(value = "更新从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "更新从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_import_wizards/{account_invoice_import_wizard_id}")
    public ResponseEntity<Account_invoice_import_wizardDTO> update(@PathVariable("account_invoice_import_wizard_id") Long account_invoice_import_wizard_id, @RequestBody Account_invoice_import_wizardDTO account_invoice_import_wizarddto) {
		Account_invoice_import_wizard domain  = account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddto);
        domain .setId(account_invoice_import_wizard_id);
		account_invoice_import_wizardService.update(domain );
		Account_invoice_import_wizardDTO dto = account_invoice_import_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_import_wizardService.getAccountInvoiceImportWizardByEntities(this.account_invoice_import_wizardMapping.toDomain(#account_invoice_import_wizarddtos)),'iBizBusinessCentral-Account_invoice_import_wizard-Update')")
    @ApiOperation(value = "批量更新从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "批量更新从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_import_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_import_wizardDTO> account_invoice_import_wizarddtos) {
        account_invoice_import_wizardService.updateBatch(account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_invoice_import_wizardService.get(#account_invoice_import_wizard_id),'iBizBusinessCentral-Account_invoice_import_wizard-Remove')")
    @ApiOperation(value = "删除从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "删除从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_import_wizards/{account_invoice_import_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_import_wizard_id") Long account_invoice_import_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_import_wizardService.remove(account_invoice_import_wizard_id));
    }

    @PreAuthorize("hasPermission(this.account_invoice_import_wizardService.getAccountInvoiceImportWizardByIds(#ids),'iBizBusinessCentral-Account_invoice_import_wizard-Remove')")
    @ApiOperation(value = "批量删除从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "批量删除从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_import_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_invoice_import_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_invoice_import_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_invoice_import_wizard-Get')")
    @ApiOperation(value = "获取从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "获取从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_import_wizards/{account_invoice_import_wizard_id}")
    public ResponseEntity<Account_invoice_import_wizardDTO> get(@PathVariable("account_invoice_import_wizard_id") Long account_invoice_import_wizard_id) {
        Account_invoice_import_wizard domain = account_invoice_import_wizardService.get(account_invoice_import_wizard_id);
        Account_invoice_import_wizardDTO dto = account_invoice_import_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取从文件中导入你的供应商账单。草稿", tags = {"从文件中导入你的供应商账单。" },  notes = "获取从文件中导入你的供应商账单。草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_import_wizards/getdraft")
    public ResponseEntity<Account_invoice_import_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_import_wizardMapping.toDto(account_invoice_import_wizardService.getDraft(new Account_invoice_import_wizard())));
    }

    @ApiOperation(value = "检查从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "检查从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_import_wizardDTO account_invoice_import_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoice_import_wizardService.checkKey(account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_import_wizardMapping.toDomain(#account_invoice_import_wizarddto),'iBizBusinessCentral-Account_invoice_import_wizard-Save')")
    @ApiOperation(value = "保存从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "保存从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_import_wizardDTO account_invoice_import_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_import_wizardService.save(account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_import_wizardMapping.toDomain(#account_invoice_import_wizarddtos),'iBizBusinessCentral-Account_invoice_import_wizard-Save')")
    @ApiOperation(value = "批量保存从文件中导入你的供应商账单。", tags = {"从文件中导入你的供应商账单。" },  notes = "批量保存从文件中导入你的供应商账单。")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoice_import_wizardDTO> account_invoice_import_wizarddtos) {
        account_invoice_import_wizardService.saveBatch(account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_import_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_import_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"从文件中导入你的供应商账单。" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_import_wizards/fetchdefault")
	public ResponseEntity<List<Account_invoice_import_wizardDTO>> fetchDefault(Account_invoice_import_wizardSearchContext context) {
        Page<Account_invoice_import_wizard> domains = account_invoice_import_wizardService.searchDefault(context) ;
        List<Account_invoice_import_wizardDTO> list = account_invoice_import_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_import_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_import_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"从文件中导入你的供应商账单。" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoice_import_wizards/searchdefault")
	public ResponseEntity<Page<Account_invoice_import_wizardDTO>> searchDefault(@RequestBody Account_invoice_import_wizardSearchContext context) {
        Page<Account_invoice_import_wizard> domains = account_invoice_import_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_import_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

