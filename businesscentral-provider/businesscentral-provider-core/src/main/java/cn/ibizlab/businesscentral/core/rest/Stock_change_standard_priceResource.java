package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_change_standard_priceService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"更改标准价" })
@RestController("Core-stock_change_standard_price")
@RequestMapping("")
public class Stock_change_standard_priceResource {

    @Autowired
    public IStock_change_standard_priceService stock_change_standard_priceService;

    @Autowired
    @Lazy
    public Stock_change_standard_priceMapping stock_change_standard_priceMapping;

    @PreAuthorize("hasPermission(this.stock_change_standard_priceMapping.toDomain(#stock_change_standard_pricedto),'iBizBusinessCentral-Stock_change_standard_price-Create')")
    @ApiOperation(value = "新建更改标准价", tags = {"更改标准价" },  notes = "新建更改标准价")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices")
    public ResponseEntity<Stock_change_standard_priceDTO> create(@Validated @RequestBody Stock_change_standard_priceDTO stock_change_standard_pricedto) {
        Stock_change_standard_price domain = stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedto);
		stock_change_standard_priceService.create(domain);
        Stock_change_standard_priceDTO dto = stock_change_standard_priceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_change_standard_priceMapping.toDomain(#stock_change_standard_pricedtos),'iBizBusinessCentral-Stock_change_standard_price-Create')")
    @ApiOperation(value = "批量新建更改标准价", tags = {"更改标准价" },  notes = "批量新建更改标准价")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_change_standard_priceDTO> stock_change_standard_pricedtos) {
        stock_change_standard_priceService.createBatch(stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_change_standard_price" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_change_standard_priceService.get(#stock_change_standard_price_id),'iBizBusinessCentral-Stock_change_standard_price-Update')")
    @ApiOperation(value = "更新更改标准价", tags = {"更改标准价" },  notes = "更新更改标准价")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_change_standard_prices/{stock_change_standard_price_id}")
    public ResponseEntity<Stock_change_standard_priceDTO> update(@PathVariable("stock_change_standard_price_id") Long stock_change_standard_price_id, @RequestBody Stock_change_standard_priceDTO stock_change_standard_pricedto) {
		Stock_change_standard_price domain  = stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedto);
        domain .setId(stock_change_standard_price_id);
		stock_change_standard_priceService.update(domain );
		Stock_change_standard_priceDTO dto = stock_change_standard_priceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_change_standard_priceService.getStockChangeStandardPriceByEntities(this.stock_change_standard_priceMapping.toDomain(#stock_change_standard_pricedtos)),'iBizBusinessCentral-Stock_change_standard_price-Update')")
    @ApiOperation(value = "批量更新更改标准价", tags = {"更改标准价" },  notes = "批量更新更改标准价")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_change_standard_prices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_change_standard_priceDTO> stock_change_standard_pricedtos) {
        stock_change_standard_priceService.updateBatch(stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_change_standard_priceService.get(#stock_change_standard_price_id),'iBizBusinessCentral-Stock_change_standard_price-Remove')")
    @ApiOperation(value = "删除更改标准价", tags = {"更改标准价" },  notes = "删除更改标准价")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_standard_prices/{stock_change_standard_price_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_change_standard_price_id") Long stock_change_standard_price_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_change_standard_priceService.remove(stock_change_standard_price_id));
    }

    @PreAuthorize("hasPermission(this.stock_change_standard_priceService.getStockChangeStandardPriceByIds(#ids),'iBizBusinessCentral-Stock_change_standard_price-Remove')")
    @ApiOperation(value = "批量删除更改标准价", tags = {"更改标准价" },  notes = "批量删除更改标准价")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_standard_prices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_change_standard_priceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_change_standard_priceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_change_standard_price-Get')")
    @ApiOperation(value = "获取更改标准价", tags = {"更改标准价" },  notes = "获取更改标准价")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_change_standard_prices/{stock_change_standard_price_id}")
    public ResponseEntity<Stock_change_standard_priceDTO> get(@PathVariable("stock_change_standard_price_id") Long stock_change_standard_price_id) {
        Stock_change_standard_price domain = stock_change_standard_priceService.get(stock_change_standard_price_id);
        Stock_change_standard_priceDTO dto = stock_change_standard_priceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取更改标准价草稿", tags = {"更改标准价" },  notes = "获取更改标准价草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_change_standard_prices/getdraft")
    public ResponseEntity<Stock_change_standard_priceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_change_standard_priceMapping.toDto(stock_change_standard_priceService.getDraft(new Stock_change_standard_price())));
    }

    @ApiOperation(value = "检查更改标准价", tags = {"更改标准价" },  notes = "检查更改标准价")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_change_standard_priceDTO stock_change_standard_pricedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_change_standard_priceService.checkKey(stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedto)));
    }

    @PreAuthorize("hasPermission(this.stock_change_standard_priceMapping.toDomain(#stock_change_standard_pricedto),'iBizBusinessCentral-Stock_change_standard_price-Save')")
    @ApiOperation(value = "保存更改标准价", tags = {"更改标准价" },  notes = "保存更改标准价")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_change_standard_priceDTO stock_change_standard_pricedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_change_standard_priceService.save(stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedto)));
    }

    @PreAuthorize("hasPermission(this.stock_change_standard_priceMapping.toDomain(#stock_change_standard_pricedtos),'iBizBusinessCentral-Stock_change_standard_price-Save')")
    @ApiOperation(value = "批量保存更改标准价", tags = {"更改标准价" },  notes = "批量保存更改标准价")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_change_standard_priceDTO> stock_change_standard_pricedtos) {
        stock_change_standard_priceService.saveBatch(stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_change_standard_price-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_change_standard_price-Get')")
	@ApiOperation(value = "获取数据集", tags = {"更改标准价" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_change_standard_prices/fetchdefault")
	public ResponseEntity<List<Stock_change_standard_priceDTO>> fetchDefault(Stock_change_standard_priceSearchContext context) {
        Page<Stock_change_standard_price> domains = stock_change_standard_priceService.searchDefault(context) ;
        List<Stock_change_standard_priceDTO> list = stock_change_standard_priceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_change_standard_price-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_change_standard_price-Get')")
	@ApiOperation(value = "查询数据集", tags = {"更改标准价" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_change_standard_prices/searchdefault")
	public ResponseEntity<Page<Stock_change_standard_priceDTO>> searchDefault(@RequestBody Stock_change_standard_priceSearchContext context) {
        Page<Stock_change_standard_price> domains = stock_change_standard_priceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_change_standard_priceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

