package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_categoryService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品种类" })
@RestController("Core-product_category")
@RequestMapping("")
public class Product_categoryResource {

    @Autowired
    public IProduct_categoryService product_categoryService;

    @Autowired
    @Lazy
    public Product_categoryMapping product_categoryMapping;

    @PreAuthorize("hasPermission(this.product_categoryMapping.toDomain(#product_categorydto),'iBizBusinessCentral-Product_category-Create')")
    @ApiOperation(value = "新建产品种类", tags = {"产品种类" },  notes = "新建产品种类")
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories")
    public ResponseEntity<Product_categoryDTO> create(@Validated @RequestBody Product_categoryDTO product_categorydto) {
        Product_category domain = product_categoryMapping.toDomain(product_categorydto);
		product_categoryService.create(domain);
        Product_categoryDTO dto = product_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_categoryMapping.toDomain(#product_categorydtos),'iBizBusinessCentral-Product_category-Create')")
    @ApiOperation(value = "批量新建产品种类", tags = {"产品种类" },  notes = "批量新建产品种类")
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_categoryDTO> product_categorydtos) {
        product_categoryService.createBatch(product_categoryMapping.toDomain(product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_categoryService.get(#product_category_id),'iBizBusinessCentral-Product_category-Update')")
    @ApiOperation(value = "更新产品种类", tags = {"产品种类" },  notes = "更新产品种类")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_categories/{product_category_id}")
    public ResponseEntity<Product_categoryDTO> update(@PathVariable("product_category_id") Long product_category_id, @RequestBody Product_categoryDTO product_categorydto) {
		Product_category domain  = product_categoryMapping.toDomain(product_categorydto);
        domain .setId(product_category_id);
		product_categoryService.update(domain );
		Product_categoryDTO dto = product_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_categoryService.getProductCategoryByEntities(this.product_categoryMapping.toDomain(#product_categorydtos)),'iBizBusinessCentral-Product_category-Update')")
    @ApiOperation(value = "批量更新产品种类", tags = {"产品种类" },  notes = "批量更新产品种类")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_categoryDTO> product_categorydtos) {
        product_categoryService.updateBatch(product_categoryMapping.toDomain(product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_categoryService.get(#product_category_id),'iBizBusinessCentral-Product_category-Remove')")
    @ApiOperation(value = "删除产品种类", tags = {"产品种类" },  notes = "删除产品种类")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_categories/{product_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_category_id") Long product_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_categoryService.remove(product_category_id));
    }

    @PreAuthorize("hasPermission(this.product_categoryService.getProductCategoryByIds(#ids),'iBizBusinessCentral-Product_category-Remove')")
    @ApiOperation(value = "批量删除产品种类", tags = {"产品种类" },  notes = "批量删除产品种类")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_category-Get')")
    @ApiOperation(value = "获取产品种类", tags = {"产品种类" },  notes = "获取产品种类")
	@RequestMapping(method = RequestMethod.GET, value = "/product_categories/{product_category_id}")
    public ResponseEntity<Product_categoryDTO> get(@PathVariable("product_category_id") Long product_category_id) {
        Product_category domain = product_categoryService.get(product_category_id);
        Product_categoryDTO dto = product_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品种类草稿", tags = {"产品种类" },  notes = "获取产品种类草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_categories/getdraft")
    public ResponseEntity<Product_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_categoryMapping.toDto(product_categoryService.getDraft(new Product_category())));
    }

    @ApiOperation(value = "检查产品种类", tags = {"产品种类" },  notes = "检查产品种类")
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_categoryDTO product_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_categoryService.checkKey(product_categoryMapping.toDomain(product_categorydto)));
    }

    @PreAuthorize("hasPermission(this.product_categoryMapping.toDomain(#product_categorydto),'iBizBusinessCentral-Product_category-Save')")
    @ApiOperation(value = "保存产品种类", tags = {"产品种类" },  notes = "保存产品种类")
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_categoryDTO product_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_categoryService.save(product_categoryMapping.toDomain(product_categorydto)));
    }

    @PreAuthorize("hasPermission(this.product_categoryMapping.toDomain(#product_categorydtos),'iBizBusinessCentral-Product_category-Save')")
    @ApiOperation(value = "批量保存产品种类", tags = {"产品种类" },  notes = "批量保存产品种类")
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_categoryDTO> product_categorydtos) {
        product_categoryService.saveBatch(product_categoryMapping.toDomain(product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品种类" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_categories/fetchdefault")
	public ResponseEntity<List<Product_categoryDTO>> fetchDefault(Product_categorySearchContext context) {
        Page<Product_category> domains = product_categoryService.searchDefault(context) ;
        List<Product_categoryDTO> list = product_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品种类" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_categories/searchdefault")
	public ResponseEntity<Page<Product_categoryDTO>> searchDefault(@RequestBody Product_categorySearchContext context) {
        Page<Product_category> domains = product_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

