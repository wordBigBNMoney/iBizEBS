package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.businesscentral.core.dto.Account_reconciliation_widgetDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_reconciliation_widgetMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_reconciliation_widgetMapping extends MappingBase<Account_reconciliation_widgetDTO, Account_reconciliation_widget> {


}

