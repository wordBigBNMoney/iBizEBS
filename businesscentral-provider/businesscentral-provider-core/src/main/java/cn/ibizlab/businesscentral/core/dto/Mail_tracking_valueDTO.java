package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mail_tracking_valueDTO]
 */
@Data
public class Mail_tracking_valueDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FIELD_DESC]
     *
     */
    @JSONField(name = "field_desc")
    @JsonProperty("field_desc")
    @NotBlank(message = "[字段说明]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String fieldDesc;

    /**
     * 属性 [OLD_VALUE_CHAR]
     *
     */
    @JSONField(name = "old_value_char")
    @JsonProperty("old_value_char")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String oldValueChar;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [NEW_VALUE_DATETIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "new_value_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("new_value_datetime")
    private Timestamp newValueDatetime;

    /**
     * 属性 [OLD_VALUE_MONETARY]
     *
     */
    @JSONField(name = "old_value_monetary")
    @JsonProperty("old_value_monetary")
    private Double oldValueMonetary;

    /**
     * 属性 [TRACKING_SEQUENCE]
     *
     */
    @JSONField(name = "tracking_sequence")
    @JsonProperty("tracking_sequence")
    private Integer trackingSequence;

    /**
     * 属性 [NEW_VALUE_CHAR]
     *
     */
    @JSONField(name = "new_value_char")
    @JsonProperty("new_value_char")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String newValueChar;

    /**
     * 属性 [NEW_VALUE_TEXT]
     *
     */
    @JSONField(name = "new_value_text")
    @JsonProperty("new_value_text")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String newValueText;

    /**
     * 属性 [NEW_VALUE_MONETARY]
     *
     */
    @JSONField(name = "new_value_monetary")
    @JsonProperty("new_value_monetary")
    private Double newValueMonetary;

    /**
     * 属性 [OLD_VALUE_DATETIME]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "old_value_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("old_value_datetime")
    private Timestamp oldValueDatetime;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [OLD_VALUE_INTEGER]
     *
     */
    @JSONField(name = "old_value_integer")
    @JsonProperty("old_value_integer")
    private Integer oldValueInteger;

    /**
     * 属性 [OLD_VALUE_TEXT]
     *
     */
    @JSONField(name = "old_value_text")
    @JsonProperty("old_value_text")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String oldValueText;

    /**
     * 属性 [FIELD_TYPE]
     *
     */
    @JSONField(name = "field_type")
    @JsonProperty("field_type")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String fieldType;

    /**
     * 属性 [NEW_VALUE_INTEGER]
     *
     */
    @JSONField(name = "new_value_integer")
    @JsonProperty("new_value_integer")
    private Integer newValueInteger;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [NEW_VALUE_FLOAT]
     *
     */
    @JSONField(name = "new_value_float")
    @JsonProperty("new_value_float")
    private Double newValueFloat;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [FIELD]
     *
     */
    @JSONField(name = "field")
    @JsonProperty("field")
    @NotBlank(message = "[更改的字段]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String field;

    /**
     * 属性 [OLD_VALUE_FLOAT]
     *
     */
    @JSONField(name = "old_value_float")
    @JsonProperty("old_value_float")
    private Double oldValueFloat;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [MAIL_MESSAGE_ID]
     *
     */
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[邮件消息ID]不允许为空!")
    private Long mailMessageId;


    /**
     * 设置 [FIELD_DESC]
     */
    public void setFieldDesc(String  fieldDesc){
        this.fieldDesc = fieldDesc ;
        this.modify("field_desc",fieldDesc);
    }

    /**
     * 设置 [OLD_VALUE_CHAR]
     */
    public void setOldValueChar(String  oldValueChar){
        this.oldValueChar = oldValueChar ;
        this.modify("old_value_char",oldValueChar);
    }

    /**
     * 设置 [NEW_VALUE_DATETIME]
     */
    public void setNewValueDatetime(Timestamp  newValueDatetime){
        this.newValueDatetime = newValueDatetime ;
        this.modify("new_value_datetime",newValueDatetime);
    }

    /**
     * 设置 [OLD_VALUE_MONETARY]
     */
    public void setOldValueMonetary(Double  oldValueMonetary){
        this.oldValueMonetary = oldValueMonetary ;
        this.modify("old_value_monetary",oldValueMonetary);
    }

    /**
     * 设置 [TRACKING_SEQUENCE]
     */
    public void setTrackingSequence(Integer  trackingSequence){
        this.trackingSequence = trackingSequence ;
        this.modify("tracking_sequence",trackingSequence);
    }

    /**
     * 设置 [NEW_VALUE_CHAR]
     */
    public void setNewValueChar(String  newValueChar){
        this.newValueChar = newValueChar ;
        this.modify("new_value_char",newValueChar);
    }

    /**
     * 设置 [NEW_VALUE_TEXT]
     */
    public void setNewValueText(String  newValueText){
        this.newValueText = newValueText ;
        this.modify("new_value_text",newValueText);
    }

    /**
     * 设置 [NEW_VALUE_MONETARY]
     */
    public void setNewValueMonetary(Double  newValueMonetary){
        this.newValueMonetary = newValueMonetary ;
        this.modify("new_value_monetary",newValueMonetary);
    }

    /**
     * 设置 [OLD_VALUE_DATETIME]
     */
    public void setOldValueDatetime(Timestamp  oldValueDatetime){
        this.oldValueDatetime = oldValueDatetime ;
        this.modify("old_value_datetime",oldValueDatetime);
    }

    /**
     * 设置 [OLD_VALUE_INTEGER]
     */
    public void setOldValueInteger(Integer  oldValueInteger){
        this.oldValueInteger = oldValueInteger ;
        this.modify("old_value_integer",oldValueInteger);
    }

    /**
     * 设置 [OLD_VALUE_TEXT]
     */
    public void setOldValueText(String  oldValueText){
        this.oldValueText = oldValueText ;
        this.modify("old_value_text",oldValueText);
    }

    /**
     * 设置 [FIELD_TYPE]
     */
    public void setFieldType(String  fieldType){
        this.fieldType = fieldType ;
        this.modify("field_type",fieldType);
    }

    /**
     * 设置 [NEW_VALUE_INTEGER]
     */
    public void setNewValueInteger(Integer  newValueInteger){
        this.newValueInteger = newValueInteger ;
        this.modify("new_value_integer",newValueInteger);
    }

    /**
     * 设置 [NEW_VALUE_FLOAT]
     */
    public void setNewValueFloat(Double  newValueFloat){
        this.newValueFloat = newValueFloat ;
        this.modify("new_value_float",newValueFloat);
    }

    /**
     * 设置 [FIELD]
     */
    public void setField(String  field){
        this.field = field ;
        this.modify("field",field);
    }

    /**
     * 设置 [OLD_VALUE_FLOAT]
     */
    public void setOldValueFloat(Double  oldValueFloat){
        this.oldValueFloat = oldValueFloat ;
        this.modify("old_value_float",oldValueFloat);
    }

    /**
     * 设置 [MAIL_MESSAGE_ID]
     */
    public void setMailMessageId(Long  mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }


}


