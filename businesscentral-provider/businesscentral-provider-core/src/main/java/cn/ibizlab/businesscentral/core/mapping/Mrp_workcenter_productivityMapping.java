package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.businesscentral.core.dto.Mrp_workcenter_productivityDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMrp_workcenter_productivityMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mrp_workcenter_productivityMapping extends MappingBase<Mrp_workcenter_productivityDTO, Mrp_workcenter_productivity> {


}

