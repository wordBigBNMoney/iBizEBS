package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_o2m;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_o2mService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_o2mSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型，一对多" })
@RestController("Core-base_import_tests_models_o2m")
@RequestMapping("")
public class Base_import_tests_models_o2mResource {

    @Autowired
    public IBase_import_tests_models_o2mService base_import_tests_models_o2mService;

    @Autowired
    @Lazy
    public Base_import_tests_models_o2mMapping base_import_tests_models_o2mMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2mMapping.toDomain(#base_import_tests_models_o2mdto),'iBizBusinessCentral-Base_import_tests_models_o2m-Create')")
    @ApiOperation(value = "新建测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "新建测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms")
    public ResponseEntity<Base_import_tests_models_o2mDTO> create(@Validated @RequestBody Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto) {
        Base_import_tests_models_o2m domain = base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdto);
		base_import_tests_models_o2mService.create(domain);
        Base_import_tests_models_o2mDTO dto = base_import_tests_models_o2mMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2mMapping.toDomain(#base_import_tests_models_o2mdtos),'iBizBusinessCentral-Base_import_tests_models_o2m-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "批量新建测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_o2mDTO> base_import_tests_models_o2mdtos) {
        base_import_tests_models_o2mService.createBatch(base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_o2m" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_o2mService.get(#base_import_tests_models_o2m_id),'iBizBusinessCentral-Base_import_tests_models_o2m-Update')")
    @ApiOperation(value = "更新测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "更新测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")
    public ResponseEntity<Base_import_tests_models_o2mDTO> update(@PathVariable("base_import_tests_models_o2m_id") Long base_import_tests_models_o2m_id, @RequestBody Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto) {
		Base_import_tests_models_o2m domain  = base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdto);
        domain .setId(base_import_tests_models_o2m_id);
		base_import_tests_models_o2mService.update(domain );
		Base_import_tests_models_o2mDTO dto = base_import_tests_models_o2mMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2mService.getBaseImportTestsModelsO2mByEntities(this.base_import_tests_models_o2mMapping.toDomain(#base_import_tests_models_o2mdtos)),'iBizBusinessCentral-Base_import_tests_models_o2m-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "批量更新测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2ms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_o2mDTO> base_import_tests_models_o2mdtos) {
        base_import_tests_models_o2mService.updateBatch(base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2mService.get(#base_import_tests_models_o2m_id),'iBizBusinessCentral-Base_import_tests_models_o2m-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "删除测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_o2m_id") Long base_import_tests_models_o2m_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2mService.remove(base_import_tests_models_o2m_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2mService.getBaseImportTestsModelsO2mByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_o2m-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "批量删除测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2ms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_o2mService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_o2mMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_o2m-Get')")
    @ApiOperation(value = "获取测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "获取测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")
    public ResponseEntity<Base_import_tests_models_o2mDTO> get(@PathVariable("base_import_tests_models_o2m_id") Long base_import_tests_models_o2m_id) {
        Base_import_tests_models_o2m domain = base_import_tests_models_o2mService.get(base_import_tests_models_o2m_id);
        Base_import_tests_models_o2mDTO dto = base_import_tests_models_o2mMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型，一对多草稿", tags = {"测试:基本导入模型，一对多" },  notes = "获取测试:基本导入模型，一对多草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2ms/getdraft")
    public ResponseEntity<Base_import_tests_models_o2mDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2mMapping.toDto(base_import_tests_models_o2mService.getDraft(new Base_import_tests_models_o2m())));
    }

    @ApiOperation(value = "检查测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "检查测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2mService.checkKey(base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2mMapping.toDomain(#base_import_tests_models_o2mdto),'iBizBusinessCentral-Base_import_tests_models_o2m-Save')")
    @ApiOperation(value = "保存测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "保存测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2mService.save(base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2mMapping.toDomain(#base_import_tests_models_o2mdtos),'iBizBusinessCentral-Base_import_tests_models_o2m-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "批量保存测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_o2mDTO> base_import_tests_models_o2mdtos) {
        base_import_tests_models_o2mService.saveBatch(base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_o2m-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_o2m-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型，一对多" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_o2ms/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_o2mDTO>> fetchDefault(Base_import_tests_models_o2mSearchContext context) {
        Page<Base_import_tests_models_o2m> domains = base_import_tests_models_o2mService.searchDefault(context) ;
        List<Base_import_tests_models_o2mDTO> list = base_import_tests_models_o2mMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_o2m-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_o2m-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型，一对多" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_o2ms/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_o2mDTO>> searchDefault(@RequestBody Base_import_tests_models_o2mSearchContext context) {
        Page<Base_import_tests_models_o2m> domains = base_import_tests_models_o2mService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_o2mMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

