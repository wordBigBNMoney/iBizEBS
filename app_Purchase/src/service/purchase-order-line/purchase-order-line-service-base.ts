import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 采购订单行服务对象基类
 *
 * @export
 * @class Purchase_order_lineServiceBase
 * @extends {EntityServie}
 */
export default class Purchase_order_lineServiceBase extends EntityService {

    /**
     * Creates an instance of  Purchase_order_lineServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_order_lineServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Purchase_order_lineServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='purchase_order_line';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'purchase_order_lines';
        this.APPDETEXT = 'name';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/purchase_orders/${context.purchase_order}/purchase_order_lines`,data,isloading);
            
            return res;
        }
        if(context.product_product && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_order_lines`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/purchase_order_lines`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/purchase_order_lines/${context.purchase_order_line}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/purchase_order_lines/${context.purchase_order_line}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let res:any = await Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let res:any = await Http.getInstance().get(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let res:any = await Http.getInstance().get(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/purchase_order_lines/${context.purchase_order_line}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/getdraft`,isloading);
            res.data.purchase_order_line = data.purchase_order_line;
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/getdraft`,isloading);
            res.data.purchase_order_line = data.purchase_order_line;
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && true){
            let res:any = await Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/getdraft`,isloading);
            res.data.purchase_order_line = data.purchase_order_line;
            
            return res;
        }
        if(context.product_template && context.product_product && true){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/getdraft`,isloading);
            res.data.purchase_order_line = data.purchase_order_line;
            
            return res;
        }
        if(context.purchase_order && true){
            let res:any = await Http.getInstance().get(`/purchase_orders/${context.purchase_order}/purchase_order_lines/getdraft`,isloading);
            res.data.purchase_order_line = data.purchase_order_line;
            
            return res;
        }
        if(context.product_product && true){
            let res:any = await Http.getInstance().get(`/product_products/${context.product_product}/purchase_order_lines/getdraft`,isloading);
            res.data.purchase_order_line = data.purchase_order_line;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/purchase_order_lines/getdraft`,isloading);
        res.data.purchase_order_line = data.purchase_order_line;
        
        return res;
    }

    /**
     * Calc_amount接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async Calc_amount(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/calc_amount`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/calc_amount`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/calc_amount`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/calc_amount`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/calc_amount`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/calc_amount`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_order_lines/${context.purchase_order_line}/calc_amount`,data,isloading);
            return res;
    }

    /**
     * Calc_price接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async Calc_price(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/calc_price`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/calc_price`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/calc_price`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/calc_price`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/calc_price`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/calc_price`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_order_lines/${context.purchase_order_line}/calc_price`,data,isloading);
            return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_order_lines/${context.purchase_order_line}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_order_lines/${context.purchase_order_line}/createbatch`,data,isloading);
            return res;
    }

    /**
     * Product_change接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async Product_change(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/product_change`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/product_change`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/product_change`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/product_change`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/product_change`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/product_change`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_order_lines/${context.purchase_order_line}/product_change`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/removebatch`,isloading);
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/removebatch`,isloading);
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/removebatch`,isloading);
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/removebatch`,isloading);
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/removebatch`,isloading);
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let res:any = Http.getInstance().delete(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/purchase_order_lines/${context.purchase_order_line}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/save`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/save`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/save`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/save`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/save`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/purchase_order_lines/${context.purchase_order_line}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.purchase_order && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/purchase_orders/${context.purchase_order}/purchase_order_lines/${context.purchase_order_line}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_order_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_products/${context.product_product}/purchase_order_lines/${context.purchase_order_line}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/purchase_order_lines/${context.purchase_order_line}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchCalc_order_amount接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async FetchCalc_order_amount(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/fetchcalc_order_amount`,tempData,isloading);
            return res;
        }
        if(context.res_supplier && context.purchase_order && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/fetchcalc_order_amount`,tempData,isloading);
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/fetchcalc_order_amount`,tempData,isloading);
            return res;
        }
        if(context.product_template && context.product_product && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/fetchcalc_order_amount`,tempData,isloading);
            return res;
        }
        if(context.purchase_order && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/purchase_orders/${context.purchase_order}/purchase_order_lines/fetchcalc_order_amount`,tempData,isloading);
            return res;
        }
        if(context.product_product && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_products/${context.product_product}/purchase_order_lines/fetchcalc_order_amount`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/purchase_order_lines/fetchcalc_order_amount`,tempData,isloading);
        return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_order_lineServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.res_supplier && context.purchase_order && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/purchase_order_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.purchase_requisition && context.purchase_order && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/purchase_order_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.product_template && context.product_product && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_order_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.purchase_order && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/purchase_orders/${context.purchase_order}/purchase_order_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.product_product && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_products/${context.product_product}/purchase_order_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/purchase_order_lines/fetchdefault`,tempData,isloading);
        return res;
    }
}