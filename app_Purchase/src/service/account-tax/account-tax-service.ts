import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_taxServiceBase from './account-tax-service-base';


/**
 * 税率服务对象
 *
 * @export
 * @class Account_taxService
 * @extends {Account_taxServiceBase}
 */
export default class Account_taxService extends Account_taxServiceBase {

    /**
     * Creates an instance of  Account_taxService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_taxService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}