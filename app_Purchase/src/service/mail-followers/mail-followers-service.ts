import { Http } from '@/utils';
import { Util } from '@/utils';
import Mail_followersServiceBase from './mail-followers-service-base';


/**
 * 文档关注者服务对象
 *
 * @export
 * @class Mail_followersService
 * @extends {Mail_followersServiceBase}
 */
export default class Mail_followersService extends Mail_followersServiceBase {

    /**
     * Creates an instance of  Mail_followersService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_followersService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}