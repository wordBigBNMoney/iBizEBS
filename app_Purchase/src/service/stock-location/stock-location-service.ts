import { Http } from '@/utils';
import { Util } from '@/utils';
import Stock_locationServiceBase from './stock-location-service-base';


/**
 * 库存位置服务对象
 *
 * @export
 * @class Stock_locationService
 * @extends {Stock_locationServiceBase}
 */
export default class Stock_locationService extends Stock_locationServiceBase {

    /**
     * Creates an instance of  Stock_locationService.
     * 
     * @param {*} [opts={}]
     * @memberof  Stock_locationService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}