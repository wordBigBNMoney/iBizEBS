import { Http } from '@/utils';
import { Util } from '@/utils';
import Mail_messageServiceBase from './mail-message-service-base';


/**
 * 消息服务对象
 *
 * @export
 * @class Mail_messageService
 * @extends {Mail_messageServiceBase}
 */
export default class Mail_messageService extends Mail_messageServiceBase {

    /**
     * Creates an instance of  Mail_messageService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_messageService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}