import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 邮件跟踪值服务对象基类
 *
 * @export
 * @class Mail_tracking_valueServiceBase
 * @extends {EntityServie}
 */
export default class Mail_tracking_valueServiceBase extends EntityService {

    /**
     * Creates an instance of  Mail_tracking_valueServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_tracking_valueServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Mail_tracking_valueServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='mail_tracking_value';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'mail_tracking_values';
        this.APPDETEXT = 'id';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/mail_messages/${context.mail_message}/mail_tracking_values`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/mail_tracking_values`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && context.mail_tracking_value){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/mail_messages/${context.mail_message}/mail_tracking_values/${context.mail_tracking_value}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/mail_tracking_values/${context.mail_tracking_value}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && context.mail_tracking_value){
            let res:any = Http.getInstance().delete(`/mail_messages/${context.mail_message}/mail_tracking_values/${context.mail_tracking_value}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/mail_tracking_values/${context.mail_tracking_value}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && context.mail_tracking_value){
            let res:any = await Http.getInstance().get(`/mail_messages/${context.mail_message}/mail_tracking_values/${context.mail_tracking_value}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/mail_tracking_values/${context.mail_tracking_value}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && true){
            let res:any = await Http.getInstance().get(`/mail_messages/${context.mail_message}/mail_tracking_values/getdraft`,isloading);
            res.data.mail_tracking_value = data.mail_tracking_value;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/mail_tracking_values/getdraft`,isloading);
        res.data.mail_tracking_value = data.mail_tracking_value;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && context.mail_tracking_value){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/mail_messages/${context.mail_message}/mail_tracking_values/${context.mail_tracking_value}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/mail_tracking_values/${context.mail_tracking_value}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && context.mail_tracking_value){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/mail_messages/${context.mail_message}/mail_tracking_values/${context.mail_tracking_value}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/mail_tracking_values/${context.mail_tracking_value}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && context.mail_tracking_value){
            let res:any = Http.getInstance().delete(`/mail_messages/${context.mail_message}/mail_tracking_values/${context.mail_tracking_value}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/mail_tracking_values/${context.mail_tracking_value}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && context.mail_tracking_value){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/mail_messages/${context.mail_message}/mail_tracking_values/${context.mail_tracking_value}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/mail_tracking_values/${context.mail_tracking_value}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && context.mail_tracking_value){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/mail_messages/${context.mail_message}/mail_tracking_values/${context.mail_tracking_value}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/mail_tracking_values/${context.mail_tracking_value}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Mail_tracking_valueServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.mail_message && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/mail_messages/${context.mail_message}/mail_tracking_values/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/mail_tracking_values/fetchdefault`,tempData,isloading);
        return res;
    }
}