import { Http } from '@/utils';
import { Util } from '@/utils';
import Mail_compose_messageServiceBase from './mail-compose-message-service-base';


/**
 * 邮件撰写向导服务对象
 *
 * @export
 * @class Mail_compose_messageService
 * @extends {Mail_compose_messageServiceBase}
 */
export default class Mail_compose_messageService extends Mail_compose_messageServiceBase {

    /**
     * Creates an instance of  Mail_compose_messageService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_compose_messageService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}