import { Http } from '@/utils';
import { Util } from '@/utils';
import Mail_activity_typeServiceBase from './mail-activity-type-service-base';


/**
 * 活动类型服务对象
 *
 * @export
 * @class Mail_activity_typeService
 * @extends {Mail_activity_typeServiceBase}
 */
export default class Mail_activity_typeService extends Mail_activity_typeServiceBase {

    /**
     * Creates an instance of  Mail_activity_typeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_activity_typeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}