import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_partner_titleServiceBase from './res-partner-title-service-base';


/**
 * 业务伙伴称谓服务对象
 *
 * @export
 * @class Res_partner_titleService
 * @extends {Res_partner_titleServiceBase}
 */
export default class Res_partner_titleService extends Res_partner_titleServiceBase {

    /**
     * Creates an instance of  Res_partner_titleService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_titleService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}