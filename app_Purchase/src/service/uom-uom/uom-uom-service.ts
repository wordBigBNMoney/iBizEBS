import { Http } from '@/utils';
import { Util } from '@/utils';
import Uom_uomServiceBase from './uom-uom-service-base';


/**
 * 产品计量单位服务对象
 *
 * @export
 * @class Uom_uomService
 * @extends {Uom_uomServiceBase}
 */
export default class Uom_uomService extends Uom_uomServiceBase {

    /**
     * Creates an instance of  Uom_uomService.
     * 
     * @param {*} [opts={}]
     * @memberof  Uom_uomService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}