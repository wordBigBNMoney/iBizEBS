import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_incotermsServiceBase from './account-incoterms-service-base';


/**
 * 贸易条款服务对象
 *
 * @export
 * @class Account_incotermsService
 * @extends {Account_incotermsServiceBase}
 */
export default class Account_incotermsService extends Account_incotermsServiceBase {

    /**
     * Creates an instance of  Account_incotermsService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_incotermsService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}