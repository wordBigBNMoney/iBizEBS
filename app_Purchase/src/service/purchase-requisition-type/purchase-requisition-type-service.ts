import { Http } from '@/utils';
import { Util } from '@/utils';
import Purchase_requisition_typeServiceBase from './purchase-requisition-type-service-base';


/**
 * 采购申请类型服务对象
 *
 * @export
 * @class Purchase_requisition_typeService
 * @extends {Purchase_requisition_typeServiceBase}
 */
export default class Purchase_requisition_typeService extends Purchase_requisition_typeServiceBase {

    /**
     * Creates an instance of  Purchase_requisition_typeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisition_typeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}