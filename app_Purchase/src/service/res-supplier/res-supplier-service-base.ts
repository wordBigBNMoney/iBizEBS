import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 供应商服务对象基类
 *
 * @export
 * @class Res_supplierServiceBase
 * @extends {EntityServie}
 */
export default class Res_supplierServiceBase extends EntityService {

    /**
     * Creates an instance of  Res_supplierServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_supplierServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Res_supplierServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='res_supplier';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'res_suppliers';
        this.APPDETEXT = 'name';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/res_suppliers`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_product_supplierinfos',JSON.stringify(res.data.product_supplierinfos?res.data.product_supplierinfos:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_orders',JSON.stringify(res.data.purchase_orders?res.data.purchase_orders:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_requisitions',JSON.stringify(res.data.purchase_requisitions?res.data.purchase_requisitions:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_res_partner_banks',JSON.stringify(res.data.res_partner_banks?res.data.res_partner_banks:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_res_partners',JSON.stringify(res.data.res_partners?res.data.res_partners:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/res_suppliers/${context.res_supplier}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let res:any = await  Http.getInstance().get(`/res_suppliers/getdraft`,isloading);
        res.data.res_supplier = data.res_supplier;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            let res:any = Http.getInstance().post(`/res_suppliers/${context.res_supplier}/checkkey`,data,isloading);
            return res;
    }

    /**
     * MasterTabCount接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async MasterTabCount(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            let res:any = Http.getInstance().post(`/res_suppliers/${context.res_supplier}/mastertabcount`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/res_suppliers/${context.res_supplier}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/res_suppliers/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchMaster接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_supplierServiceBase
     */
    public async FetchMaster(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/res_suppliers/fetchmaster`,tempData,isloading);
        return res;
    }
}