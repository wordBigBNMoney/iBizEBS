import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_countryServiceBase from './res-country-service-base';


/**
 * 国家/地区服务对象
 *
 * @export
 * @class Res_countryService
 * @extends {Res_countryServiceBase}
 */
export default class Res_countryService extends Res_countryServiceBase {

    /**
     * Creates an instance of  Res_countryService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_countryService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}