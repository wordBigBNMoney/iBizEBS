import { Http } from '@/utils';
import { Util } from '@/utils';
import Purchase_requisition_lineServiceBase from './purchase-requisition-line-service-base';


/**
 * 采购申请行服务对象
 *
 * @export
 * @class Purchase_requisition_lineService
 * @extends {Purchase_requisition_lineServiceBase}
 */
export default class Purchase_requisition_lineService extends Purchase_requisition_lineServiceBase {

    /**
     * Creates an instance of  Purchase_requisition_lineService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisition_lineService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}