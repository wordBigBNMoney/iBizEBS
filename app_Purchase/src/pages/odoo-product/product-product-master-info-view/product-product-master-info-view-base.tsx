import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Product_productService from '@/service/product-product/product-product-service';
import Product_productAuthService from '@/authservice/product-product/product-product-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Product_productUIService from '@/uiservice/product-product/product-product-ui-service';

/**
 * 主信息概览视图基类
 *
 * @export
 * @class Product_productMasterInfoViewBase
 * @extends {EditViewBase}
 */
export class Product_productMasterInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterInfoViewBase
     */
    protected appDeName: string = 'product_product';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterInfoViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Product_productService}
     * @memberof Product_productMasterInfoViewBase
     */
    protected appEntityService: Product_productService = new Product_productService;

    /**
     * 实体权限服务对象
     *
     * @type Product_productUIService
     * @memberof Product_productMasterInfoViewBase
     */
    public appUIService: Product_productUIService = new Product_productUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_productMasterInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_productMasterInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_product.views.masterinfoview.caption',
        srfTitle: 'entities.product_product.views.masterinfoview.title',
        srfSubTitle: 'entities.product_product.views.masterinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_productMasterInfoViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterInfoViewBase
     */
	protected viewtag: string = 'f70a6b0e958493244ca97f4177ba1c24';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterInfoViewBase
     */ 
    protected viewName:string = "product_productMasterInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_productMasterInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_productMasterInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_productMasterInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'product_product',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_productMasterInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_productMasterInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_productMasterInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}