import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import Product_productService from '@/service/product-product/product-product-service';
import Product_productAuthService from '@/authservice/product-product/product-product-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import Product_productUIService from '@/uiservice/product-product/product-product-ui-service';

/**
 * 概览视图基类
 *
 * @export
 * @class Product_productMasterSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class Product_productMasterSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterSummaryViewBase
     */
    protected appDeName: string = 'product_product';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterSummaryViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterSummaryViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Product_productService}
     * @memberof Product_productMasterSummaryViewBase
     */
    protected appEntityService: Product_productService = new Product_productService;

    /**
     * 实体权限服务对象
     *
     * @type Product_productUIService
     * @memberof Product_productMasterSummaryViewBase
     */
    public appUIService: Product_productUIService = new Product_productUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_productMasterSummaryViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_productMasterSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_product.views.mastersummaryview.caption',
        srfTitle: 'entities.product_product.views.mastersummaryview.title',
        srfSubTitle: 'entities.product_product.views.mastersummaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_productMasterSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterSummaryViewBase
     */
	protected viewtag: string = '69d64b992c942b72ac526dbbb4fa3e6c';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_productMasterSummaryViewBase
     */ 
    protected viewName:string = "product_productMasterSummaryView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_productMasterSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_productMasterSummaryViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_productMasterSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'product_product',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_productMasterSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}