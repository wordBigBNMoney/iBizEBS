import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Product_supplierinfoService from '@/service/product-supplierinfo/product-supplierinfo-service';
import Product_supplierinfoAuthService from '@/authservice/product-supplierinfo/product-supplierinfo-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Product_supplierinfoUIService from '@/uiservice/product-supplierinfo/product-supplierinfo-ui-service';

/**
 * 快速新建视图视图基类
 *
 * @export
 * @class Product_supplierinfoBasicQuickViewBase
 * @extends {OptionViewBase}
 */
export class Product_supplierinfoBasicQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    protected appDeName: string = 'product_supplierinfo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    protected appDeMajor: string = 'name_text';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Product_supplierinfoService}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    protected appEntityService: Product_supplierinfoService = new Product_supplierinfoService;

    /**
     * 实体权限服务对象
     *
     * @type Product_supplierinfoUIService
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    public appUIService: Product_supplierinfoUIService = new Product_supplierinfoUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_supplierinfo.views.basicquickview.caption',
        srfTitle: 'entities.product_supplierinfo.views.basicquickview.title',
        srfSubTitle: 'entities.product_supplierinfo.views.basicquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
	protected viewtag: string = '794b114292ebc7840d037dc2b553456c';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */ 
    protected viewName:string = "product_supplierinfoBasicQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_supplierinfoBasicQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'product_supplierinfo',
            majorPSDEField: 'name_text',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}