import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Product_categoryService from '@/service/product-category/product-category-service';
import Product_categoryAuthService from '@/authservice/product-category/product-category-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Product_categoryUIService from '@/uiservice/product-category/product-category-ui-service';

/**
 * 配置信息编辑视图视图基类
 *
 * @export
 * @class Product_categoryBasicEditViewBase
 * @extends {EditViewBase}
 */
export class Product_categoryBasicEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicEditViewBase
     */
    protected appDeName: string = 'product_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicEditViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicEditViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Product_categoryService}
     * @memberof Product_categoryBasicEditViewBase
     */
    protected appEntityService: Product_categoryService = new Product_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Product_categoryUIService
     * @memberof Product_categoryBasicEditViewBase
     */
    public appUIService: Product_categoryUIService = new Product_categoryUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_categoryBasicEditViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_categoryBasicEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_category.views.basiceditview.caption',
        srfTitle: 'entities.product_category.views.basiceditview.title',
        srfSubTitle: 'entities.product_category.views.basiceditview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_categoryBasicEditViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicEditViewBase
     */
	protected viewtag: string = '913d12caf6e3d513f7e2939e616ff1f3';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicEditViewBase
     */ 
    protected viewName:string = "product_categoryBasicEditView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_categoryBasicEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_categoryBasicEditViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_categoryBasicEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'product_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}