import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Product_supplierinfoService from '@/service/product-supplierinfo/product-supplierinfo-service';
import Product_supplierinfoAuthService from '@/authservice/product-supplierinfo/product-supplierinfo-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Product_supplierinfoUIService from '@/uiservice/product-supplierinfo/product-supplierinfo-ui-service';

/**
 * 配置信息编辑视图视图基类
 *
 * @export
 * @class Product_supplierinfoBasicEditViewBase
 * @extends {EditViewBase}
 */
export class Product_supplierinfoBasicEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    protected appDeName: string = 'product_supplierinfo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    protected appDeMajor: string = 'name_text';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicEditViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Product_supplierinfoService}
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    protected appEntityService: Product_supplierinfoService = new Product_supplierinfoService;

    /**
     * 实体权限服务对象
     *
     * @type Product_supplierinfoUIService
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    public appUIService: Product_supplierinfoUIService = new Product_supplierinfoUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_supplierinfo.views.basiceditview.caption',
        srfTitle: 'entities.product_supplierinfo.views.basiceditview.title',
        srfSubTitle: 'entities.product_supplierinfo.views.basiceditview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicEditViewBase
     */
	protected viewtag: string = 'e10d70201b07a1a9a3f9126cf60265ba';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicEditViewBase
     */ 
    protected viewName:string = "product_supplierinfoBasicEditView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_supplierinfoBasicEditViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'product_supplierinfo',
            majorPSDEField: 'name_text',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}