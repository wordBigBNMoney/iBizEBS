import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Product_templateService from '@/service/product-template/product-template-service';
import Product_templateAuthService from '@/authservice/product-template/product-template-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Product_templateUIService from '@/uiservice/product-template/product-template-ui-service';

/**
 * 主信息概览视图基类
 *
 * @export
 * @class Product_templateMasterInfoViewBase
 * @extends {EditViewBase}
 */
export class Product_templateMasterInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterInfoViewBase
     */
    protected appDeName: string = 'product_template';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterInfoViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Product_templateService}
     * @memberof Product_templateMasterInfoViewBase
     */
    protected appEntityService: Product_templateService = new Product_templateService;

    /**
     * 实体权限服务对象
     *
     * @type Product_templateUIService
     * @memberof Product_templateMasterInfoViewBase
     */
    public appUIService: Product_templateUIService = new Product_templateUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_templateMasterInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_templateMasterInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_template.views.masterinfoview.caption',
        srfTitle: 'entities.product_template.views.masterinfoview.title',
        srfSubTitle: 'entities.product_template.views.masterinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_templateMasterInfoViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterInfoViewBase
     */
	protected viewtag: string = '12583aa40f1e9c0eaa5a5ba2ce0dd7a4';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterInfoViewBase
     */ 
    protected viewName:string = "product_templateMasterInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_templateMasterInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_templateMasterInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_templateMasterInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'product_template',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_templateMasterInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_templateMasterInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_templateMasterInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}