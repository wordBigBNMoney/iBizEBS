import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Product_productService from '@/service/product-product/product-product-service';
import Product_productAuthService from '@/authservice/product-product/product-product-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Product_productUIService from '@/uiservice/product-product/product-product-ui-service';

/**
 * 产品数据选择视图视图基类
 *
 * @export
 * @class Product_productPickupViewBase
 * @extends {PickupViewBase}
 */
export class Product_productPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_productPickupViewBase
     */
    protected appDeName: string = 'product_product';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_productPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_productPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Product_productService}
     * @memberof Product_productPickupViewBase
     */
    protected appEntityService: Product_productService = new Product_productService;

    /**
     * 实体权限服务对象
     *
     * @type Product_productUIService
     * @memberof Product_productPickupViewBase
     */
    public appUIService: Product_productUIService = new Product_productUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_productPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_product.views.pickupview.caption',
        srfTitle: 'entities.product_product.views.pickupview.title',
        srfSubTitle: 'entities.product_product.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_productPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_productPickupViewBase
     */
	protected viewtag: string = '03656c91dd184ab62a81166134954d3b';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_productPickupViewBase
     */ 
    protected viewName:string = "product_productPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_productPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_productPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_productPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'product_product',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_productPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_productPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_productPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}