import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Product_templateService from '@/service/product-template/product-template-service';
import Product_templateAuthService from '@/authservice/product-template/product-template-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Product_templateUIService from '@/uiservice/product-template/product-template-ui-service';

/**
 * 产品模板数据选择视图视图基类
 *
 * @export
 * @class Product_templatePickupViewBase
 * @extends {PickupViewBase}
 */
export class Product_templatePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_templatePickupViewBase
     */
    protected appDeName: string = 'product_template';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_templatePickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_templatePickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Product_templateService}
     * @memberof Product_templatePickupViewBase
     */
    protected appEntityService: Product_templateService = new Product_templateService;

    /**
     * 实体权限服务对象
     *
     * @type Product_templateUIService
     * @memberof Product_templatePickupViewBase
     */
    public appUIService: Product_templateUIService = new Product_templateUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_templatePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_template.views.pickupview.caption',
        srfTitle: 'entities.product_template.views.pickupview.title',
        srfSubTitle: 'entities.product_template.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_templatePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_templatePickupViewBase
     */
	protected viewtag: string = '083974d7385593d391f4c25235061b19';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_templatePickupViewBase
     */ 
    protected viewName:string = "product_templatePickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_templatePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_templatePickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_templatePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'product_template',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_templatePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_templatePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_templatePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}