import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Res_usersService from '@/service/res-users/res-users-service';
import Res_usersAuthService from '@/authservice/res-users/res-users-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Res_usersUIService from '@/uiservice/res-users/res-users-ui-service';

/**
 * 用户数据选择视图视图基类
 *
 * @export
 * @class Res_usersPickupViewBase
 * @extends {PickupViewBase}
 */
export class Res_usersPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_usersPickupViewBase
     */
    protected appDeName: string = 'res_users';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_usersPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_usersPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Res_usersService}
     * @memberof Res_usersPickupViewBase
     */
    protected appEntityService: Res_usersService = new Res_usersService;

    /**
     * 实体权限服务对象
     *
     * @type Res_usersUIService
     * @memberof Res_usersPickupViewBase
     */
    public appUIService: Res_usersUIService = new Res_usersUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_usersPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_users.views.pickupview.caption',
        srfTitle: 'entities.res_users.views.pickupview.title',
        srfSubTitle: 'entities.res_users.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_usersPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_usersPickupViewBase
     */
	protected viewtag: string = '6928c8dc50acd54e98c657410871afa9';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_usersPickupViewBase
     */ 
    protected viewName:string = "res_usersPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_usersPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_usersPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_usersPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'res_users',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_usersPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_usersPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_usersPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}