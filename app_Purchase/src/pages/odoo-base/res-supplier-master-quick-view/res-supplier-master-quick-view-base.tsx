import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Res_supplierService from '@/service/res-supplier/res-supplier-service';
import Res_supplierAuthService from '@/authservice/res-supplier/res-supplier-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Res_supplierUIService from '@/uiservice/res-supplier/res-supplier-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class Res_supplierMasterQuickViewBase
 * @extends {OptionViewBase}
 */
export class Res_supplierMasterQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterQuickViewBase
     */
    protected appDeName: string = 'res_supplier';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Res_supplierService}
     * @memberof Res_supplierMasterQuickViewBase
     */
    protected appEntityService: Res_supplierService = new Res_supplierService;

    /**
     * 实体权限服务对象
     *
     * @type Res_supplierUIService
     * @memberof Res_supplierMasterQuickViewBase
     */
    public appUIService: Res_supplierUIService = new Res_supplierUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Res_supplierMasterQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_supplierMasterQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_supplier.views.masterquickview.caption',
        srfTitle: 'entities.res_supplier.views.masterquickview.title',
        srfSubTitle: 'entities.res_supplier.views.masterquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_supplierMasterQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterQuickViewBase
     */
	protected viewtag: string = '4a9f1473ff1847a6c719009ea4362e88';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterQuickViewBase
     */ 
    protected viewName:string = "res_supplierMasterQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_supplierMasterQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_supplierMasterQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_supplierMasterQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'res_supplier',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierMasterQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierMasterQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierMasterQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}