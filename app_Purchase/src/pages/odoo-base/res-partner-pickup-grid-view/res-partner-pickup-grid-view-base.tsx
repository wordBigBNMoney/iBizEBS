import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Res_partnerService from '@/service/res-partner/res-partner-service';
import Res_partnerAuthService from '@/authservice/res-partner/res-partner-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Res_partnerUIService from '@/uiservice/res-partner/res-partner-ui-service';

/**
 * 联系人选择表格视图视图基类
 *
 * @export
 * @class Res_partnerPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Res_partnerPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupGridViewBase
     */
    protected appDeName: string = 'res_partner';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupGridViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Res_partnerService}
     * @memberof Res_partnerPickupGridViewBase
     */
    protected appEntityService: Res_partnerService = new Res_partnerService;

    /**
     * 实体权限服务对象
     *
     * @type Res_partnerUIService
     * @memberof Res_partnerPickupGridViewBase
     */
    public appUIService: Res_partnerUIService = new Res_partnerUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_partnerPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_partner.views.pickupgridview.caption',
        srfTitle: 'entities.res_partner.views.pickupgridview.title',
        srfSubTitle: 'entities.res_partner.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_partnerPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupGridViewBase
     */
	protected viewtag: string = 'b9c01aac6e18ec1eed1b74d47f275384';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerPickupGridViewBase
     */ 
    protected viewName:string = "res_partnerPickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_partnerPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_partnerPickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_partnerPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'res_partner',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partnerPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Res_partnerPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}