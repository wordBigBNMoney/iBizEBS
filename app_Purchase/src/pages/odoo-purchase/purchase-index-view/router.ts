import Vue from 'vue';
import Router from 'vue-router';
import { AuthGuard } from '@/utils';
import qs from 'qs';
import { globalRoutes, indexRoutes} from '@/router'
import { AppService } from '@/studio-core/service/app-service/AppService';

Vue.use(Router);

const appService = new AppService();

const router = new Router({
    routes: [
                {
            path: '/purchaseindexview/:purchaseindexview?',
            beforeEnter: async (to: any, from: any, next: any) => {
                const routerParamsName = 'purchaseindexview';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    params[routerParamsName] = to.params[routerParamsName];
                }
                const url: string = '/appdata';
                const bol = await AuthGuard.getInstance().authGuard(url, params, router);
                if (bol) {
                    appService.navHistory.indexMeta = {
                        caption: 'app.views.purchaseindexview.caption',
                        info:'',
                        viewType: 'APPINDEX',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                        ],
                        requireAuth: true,
                    };
                    next();
                }
            },
            meta: {  
                caption: 'app.views.purchaseindexview.caption',
                info:'',
                viewType: 'APPINDEX',
                parameters: [
                    { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                ],
                requireAuth: true,
            },
            component: () => import('@pages/odoo-purchase/purchase-index-view/purchase-index-view.vue'),
            children: [
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/product_supplierinfos/:product_supplierinfo?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-quick-view/product-supplierinfo-basic-quick-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_supplierinfos/:product_supplierinfo?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-quick-view/product-supplierinfo-basic-quick-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/product_supplierinfos/:product_supplierinfo?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-quick-view/product-supplierinfo-basic-quick-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/product_supplierinfos/:product_supplierinfo?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-quick-view/product-supplierinfo-basic-quick-view.vue'),
                },
                {
                    path: 'product_supplierinfos/:product_supplierinfo?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-quick-view/product-supplierinfo-basic-quick-view.vue'),
                },
                {
                    path: 'mail_followers/:mail_followers?/byreslistview/:byreslistview?',
                    meta: {
                        caption: 'entities.mail_followers.views.byreslistview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'mail_followers', parameterName: 'mail_followers' },
                            { pathName: 'byreslistview', parameterName: 'byreslistview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-followers-by-res-list-view/mail-followers-by-res-list-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.product_product.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-pickup-view/product-product-pickup-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.product_product.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-pickup-view/product-product-pickup-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.res_supplier.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-supplier-master-quick-view/res-supplier-master-quick-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.product_template.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-template-master-edit-view/product-template-master-edit-view.vue'),
                },
                {
                    path: 'uom_categories/:uom_category?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.uom_category.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'uom_categories', parameterName: 'uom_category' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-uom/uom-category-basic-list-exp-view/uom-category-basic-list-exp-view.vue'),
                },
                {
                    path: 'uom_uoms/:uom_uom?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.uom_uom.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'uom_uoms', parameterName: 'uom_uom' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-uom/uom-uom-basic-edit-view/uom-uom-basic-edit-view.vue'),
                },
                {
                    path: 'uom_uoms/:uom_uom?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.uom_uom.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'uom_uoms', parameterName: 'uom_uom' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-uom/uom-uom-basic-quick-view/uom-uom-basic-quick-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-summary-view/purchase-order-master-summary-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-summary-view/purchase-order-master-summary-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-summary-view/purchase-order-master-summary-view.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-summary-view/purchase-order-master-summary-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.purchase_order.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-info-view/purchase-order-master-info-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.purchase_order.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-info-view/purchase-order-master-info-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.purchase_order.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-info-view/purchase-order-master-info-view.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.purchase_order.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-info-view/purchase-order-master-info-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/res_partner_banks/:res_partner_bank?/editview/:editview?',
                    meta: {
                        caption: 'entities.res_partner_bank.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'res_partner_banks', parameterName: 'res_partner_bank' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-bank-edit-view/res-partner-bank-edit-view.vue'),
                },
                {
                    path: 'res_partner_banks/:res_partner_bank?/editview/:editview?',
                    meta: {
                        caption: 'entities.res_partner_bank.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_partner_banks', parameterName: 'res_partner_bank' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-bank-edit-view/res-partner-bank-edit-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_users.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-pickup-view/res-users-pickup-view.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/sendview/:sendview?',
                    meta: {
                        caption: 'entities.mail_message.views.sendview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'sendview', parameterName: 'sendview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-send-view/mail-message-send-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_supplier.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-supplier-pickup-grid-view/res-supplier-pickup-grid-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.product_product.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-edit-view/product-product-master-edit-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.product_product.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-edit-view/product-product-master-edit-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_users.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-pickup-grid-view/res-users-pickup-grid-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-summary-view/purchase-requisition-master-summary-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-summary-view/purchase-requisition-master-summary-view.vue'),
                },
                {
                    path: 'purchase_requisition_types/:purchase_requisition_type?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.purchase_requisition_type.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisition_types', parameterName: 'purchase_requisition_type' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-type-pickup-grid-view/purchase-requisition-type-pickup-grid-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.purchase_order.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-quick-view/purchase-order-master-quick-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.purchase_order.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-quick-view/purchase-order-master-quick-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.purchase_order.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-quick-view/purchase-order-master-quick-view.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.purchase_order.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-quick-view/purchase-order-master-quick-view.vue'),
                },
                {
                    path: 'account_taxes/:account_tax?/purchasempickupview/:purchasempickupview?',
                    meta: {
                        caption: 'entities.account_tax.views.purchasempickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'account_taxes', parameterName: 'account_tax' },
                            { pathName: 'purchasempickupview', parameterName: 'purchasempickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-tax-purchase-mpickup-view/account-tax-purchase-mpickup-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.product_template.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-template-pickup-view/product-template-pickup-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.product_template.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-template-master-quick-view/product-template-master-quick-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/res_partners/:res_partner?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_partner.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pickup-grid-view/res-partner-pickup-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_partner.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pickup-grid-view/res-partner-pickup-grid-view.vue'),
                },
                {
                    path: 'uom_uoms/:uom_uom?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.uom_uom.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'uom_uoms', parameterName: 'uom_uom' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-uom/uom-uom-basic-list-exp-view/uom-uom-basic-list-exp-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line/purchase-order-line-line.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line/purchase-order-line-line.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line/purchase-order-line-line.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/purchase_order_lines/:purchase_order_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line/purchase-order-line-line.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line/purchase-order-line-line.vue'),
                },
                {
                    path: 'product_products/:product_product?/purchase_order_lines/:purchase_order_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line/purchase-order-line-line.vue'),
                },
                {
                    path: 'purchase_order_lines/:purchase_order_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line/purchase-order-line-line.vue'),
                },
                {
                    path: 'purchase_requisition_types/:purchase_requisition_type?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.purchase_requisition_type.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisition_types', parameterName: 'purchase_requisition_type' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-type-basic-quick-view/purchase-requisition-type-basic-quick-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.product_template.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-template-master-info-view/product-template-master-info-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.product_product.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-tab-info-view/product-product-master-tab-info-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.product_product.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-tab-info-view/product-product-master-tab-info-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_requisition_lines/:purchase_requisition_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line-edit/purchase-requisition-line-line-edit.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/purchase_requisition_lines/:purchase_requisition_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line-edit/purchase-requisition-line-line-edit.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_requisition_lines/:purchase_requisition_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line-edit/purchase-requisition-line-line-edit.vue'),
                },
                {
                    path: 'product_products/:product_product?/purchase_requisition_lines/:purchase_requisition_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line-edit/purchase-requisition-line-line-edit.vue'),
                },
                {
                    path: 'purchase_requisition_lines/:purchase_requisition_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line-edit/purchase-requisition-line-line-edit.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-grid-view/purchase-requisition-master-grid-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-grid-view/purchase-requisition-master-grid-view.vue'),
                },
                {
                    path: 'uom_categories/:uom_category?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.uom_category.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'uom_categories', parameterName: 'uom_category' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-uom/uom-category-basic-edit-view/uom-category-basic-edit-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_requisition_lines/:purchase_requisition_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line/purchase-requisition-line-line.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/purchase_requisition_lines/:purchase_requisition_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line/purchase-requisition-line-line.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_requisition_lines/:purchase_requisition_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line/purchase-requisition-line-line.vue'),
                },
                {
                    path: 'product_products/:product_product?/purchase_requisition_lines/:purchase_requisition_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line/purchase-requisition-line-line.vue'),
                },
                {
                    path: 'purchase_requisition_lines/:purchase_requisition_line?/line/:line?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-line/purchase-requisition-line-line.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.res_supplier.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-supplier-master-info-view/res-supplier-master-info-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.product_product.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-quick-view/product-product-master-quick-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.product_product.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-quick-view/product-product-master-quick-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-tab-info-view/purchase-order-master-tab-info-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-tab-info-view/purchase-order-master-tab-info-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-tab-info-view/purchase-order-master-tab-info-view.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-tab-info-view/purchase-order-master-tab-info-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-edit-view/purchase-requisition-master-edit-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-edit-view/purchase-requisition-master-edit-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-tab-info-view/purchase-requisition-master-tab-info-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-tab-info-view/purchase-requisition-master-tab-info-view.vue'),
                },
                {
                    path: 'uom_categories/:uom_category?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.uom_category.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'uom_categories', parameterName: 'uom_category' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-uom/uom-category-basic-quick-view/uom-category-basic-quick-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-quick-view/purchase-requisition-master-quick-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-quick-view/purchase-requisition-master-quick-view.vue'),
                },
                {
                    path: 'mail_activities/:mail_activity?/editview/:editview?',
                    meta: {
                        caption: 'entities.mail_activity.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'mail_activities', parameterName: 'mail_activity' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-activity-edit-view/mail-activity-edit-view.vue'),
                },
                {
                    path: 'purchase_requisition_types/:purchase_requisition_type?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.purchase_requisition_type.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisition_types', parameterName: 'purchase_requisition_type' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-type-basic-edit-view/purchase-requisition-type-basic-edit-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-edit-view/purchase-order-line-edit-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-edit-view/purchase-order-line-edit-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-edit-view/purchase-order-line-edit-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/purchase_order_lines/:purchase_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-edit-view/purchase-order-line-edit-view.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-edit-view/purchase-order-line-edit-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/purchase_order_lines/:purchase_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-edit-view/purchase-order-line-edit-view.vue'),
                },
                {
                    path: 'purchase_order_lines/:purchase_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-edit-view/purchase-order-line-edit-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.product_product.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-info-view/product-product-master-info-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.product_product.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-info-view/product-product-master-info-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/res_partners/:res_partner?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_partner.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pickup-view/res-partner-pickup-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_partner.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pickup-view/res-partner-pickup-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.res_supplier.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-supplier-master-tab-info-view/res-supplier-master-tab-info-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.res_supplier.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-supplier-master-summary-view/res-supplier-master-summary-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_requisition_lines/:purchase_requisition_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-edit-view/purchase-requisition-line-edit-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/purchase_requisition_lines/:purchase_requisition_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-edit-view/purchase-requisition-line-edit-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_requisition_lines/:purchase_requisition_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-edit-view/purchase-requisition-line-edit-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/purchase_requisition_lines/:purchase_requisition_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-edit-view/purchase-requisition-line-edit-view.vue'),
                },
                {
                    path: 'purchase_requisition_lines/:purchase_requisition_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.purchase_requisition_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisition_lines', parameterName: 'purchase_requisition_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-line-edit-view/purchase-requisition-line-edit-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.product_template.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-template-master-summary-view/product-template-master-summary-view.vue'),
                },
                {
                    path: 'res_config_settings/:res_config_settings?/purchaseeditview/:purchaseeditview?',
                    meta: {
                        caption: 'entities.res_config_settings.views.purchaseeditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_config_settings', parameterName: 'res_config_settings' },
                            { pathName: 'purchaseeditview', parameterName: 'purchaseeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-config-settings-purchase-edit-view/res-config-settings-purchase-edit-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/product_supplierinfos/:product_supplierinfo?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-edit-view/product-supplierinfo-basic-edit-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_supplierinfos/:product_supplierinfo?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-edit-view/product-supplierinfo-basic-edit-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/product_supplierinfos/:product_supplierinfo?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-edit-view/product-supplierinfo-basic-edit-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/product_supplierinfos/:product_supplierinfo?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-edit-view/product-supplierinfo-basic-edit-view.vue'),
                },
                {
                    path: 'product_supplierinfos/:product_supplierinfo?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-edit-view/product-supplierinfo-basic-edit-view.vue'),
                },
                {
                    path: 'product_categories/:product_category?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.product_category.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-basic-quick-view/product-category-basic-quick-view.vue'),
                },
                {
                    path: 'uom_categories/:uom_category?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.uom_category.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'uom_categories', parameterName: 'uom_category' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-uom/uom-category-pickup-view/uom-category-pickup-view.vue'),
                },
                {
                    path: 'uom_categories/:uom_category?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.uom_category.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'uom_categories', parameterName: 'uom_category' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-uom/uom-category-pickup-grid-view/uom-category-pickup-grid-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.res_supplier.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-supplier-master-grid-view/res-supplier-master-grid-view.vue'),
                },
                {
                    path: 'purchase_requisition_types/:purchase_requisition_type?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.purchase_requisition_type.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisition_types', parameterName: 'purchase_requisition_type' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-type-pickup-view/purchase-requisition-type-pickup-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.res_supplier.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-supplier-master-edit-view/res-supplier-master-edit-view.vue'),
                },
                {
                    path: 'res_partner_categories/:res_partner_category?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_partner_category.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_partner_categories', parameterName: 'res_partner_category' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-category-pickup-grid-view/res-partner-category-pickup-grid-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.product_template.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-template-master-tab-info-view/product-template-master-tab-info-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.product_product.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-grid-view/product-product-master-grid-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.product_product.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-grid-view/product-product-master-grid-view.vue'),
                },
                {
                    path: 'mail_activities/:mail_activity?/byreslistview/:byreslistview?',
                    meta: {
                        caption: 'entities.mail_activity.views.byreslistview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'mail_activities', parameterName: 'mail_activity' },
                            { pathName: 'byreslistview', parameterName: 'byreslistview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-activity-by-res-list-view/mail-activity-by-res-list-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/product_supplierinfos/:product_supplierinfo?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-list-exp-view/product-supplierinfo-basic-list-exp-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_supplierinfos/:product_supplierinfo?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-list-exp-view/product-supplierinfo-basic-list-exp-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/product_supplierinfos/:product_supplierinfo?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-list-exp-view/product-supplierinfo-basic-list-exp-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/product_supplierinfos/:product_supplierinfo?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-list-exp-view/product-supplierinfo-basic-list-exp-view.vue'),
                },
                {
                    path: 'product_supplierinfos/:product_supplierinfo?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.product_supplierinfo.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-supplierinfo-basic-list-exp-view/product-supplierinfo-basic-list-exp-view.vue'),
                },
                {
                    path: 'ir_attachments/:ir_attachment?/byresdataview/:byresdataview?',
                    meta: {
                        caption: 'entities.ir_attachment.views.byresdataview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'ir_attachments', parameterName: 'ir_attachment' },
                            { pathName: 'byresdataview', parameterName: 'byresdataview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-ir/ir-attachment-by-res-data-view/ir-attachment-by-res-data-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/res_partner_banks/:res_partner_bank?/line/:line?',
                    meta: {
                        caption: 'entities.res_partner_bank.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'res_partner_banks', parameterName: 'res_partner_bank' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-bank-line/res-partner-bank-line.vue'),
                },
                {
                    path: 'res_partner_banks/:res_partner_bank?/line/:line?',
                    meta: {
                        caption: 'entities.res_partner_bank.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_partner_banks', parameterName: 'res_partner_bank' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-bank-line/res-partner-bank-line.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/res_partner_banks/:res_partner_bank?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.res_partner_bank.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'res_partner_banks', parameterName: 'res_partner_bank' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-bank-line-edit/res-partner-bank-line-edit.vue'),
                },
                {
                    path: 'res_partner_banks/:res_partner_bank?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.res_partner_bank.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_partner_banks', parameterName: 'res_partner_bank' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-bank-line-edit/res-partner-bank-line-edit.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/remarkview/:remarkview?',
                    meta: {
                        caption: 'entities.mail_message.views.remarkview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'remarkview', parameterName: 'remarkview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-remark-view/mail-message-remark-view.vue'),
                },
                {
                    path: 'product_categories/:product_category?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.product_category.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-basic-list-exp-view/product-category-basic-list-exp-view.vue'),
                },
                {
                    path: 'product_categories/:product_category?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.product_category.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-basic-edit-view/product-category-basic-edit-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.product_product.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-pickup-grid-view/product-product-pickup-grid-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.product_product.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-pickup-grid-view/product-product-pickup-grid-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.product_template.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-template-pickup-grid-view/product-template-pickup-grid-view.vue'),
                },
                {
                    path: 'account_taxes/:account_tax?/purchasepickupgridview/:purchasepickupgridview?',
                    meta: {
                        caption: 'entities.account_tax.views.purchasepickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'account_taxes', parameterName: 'account_tax' },
                            { pathName: 'purchasepickupgridview', parameterName: 'purchasepickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-tax-purchase-pickup-grid-view/account-tax-purchase-pickup-grid-view.vue'),
                },
                {
                    path: 'res_partner_categories/:res_partner_category?/mpickupview/:mpickupview?',
                    meta: {
                        caption: 'entities.res_partner_category.views.mpickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_partner_categories', parameterName: 'res_partner_category' },
                            { pathName: 'mpickupview', parameterName: 'mpickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-category-mpickup-view/res-partner-category-mpickup-view.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/byreslistview/:byreslistview?',
                    meta: {
                        caption: 'entities.mail_message.views.byreslistview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'byreslistview', parameterName: 'byreslistview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-by-res-list-view/mail-message-by-res-list-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line-edit/purchase-order-line-line-edit.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line-edit/purchase-order-line-line-edit.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line-edit/purchase-order-line-line-edit.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/purchase_order_lines/:purchase_order_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line-edit/purchase-order-line-line-edit.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/purchase_order_lines/:purchase_order_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line-edit/purchase-order-line-line-edit.vue'),
                },
                {
                    path: 'product_products/:product_product?/purchase_order_lines/:purchase_order_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line-edit/purchase-order-line-line-edit.vue'),
                },
                {
                    path: 'purchase_order_lines/:purchase_order_line?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.purchase_order_line.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-line-line-edit/purchase-order-line-line-edit.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/mainview/:mainview?',
                    meta: {
                        caption: 'entities.mail_message.views.mainview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'mainview', parameterName: 'mainview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-main-view/mail-message-main-view.vue'),
                },
                {
                    path: 'product_templates/:product_template?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.product_template.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-template-master-grid-view/product-template-master-grid-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-info-view/purchase-requisition-master-info-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.purchase_requisition.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-master-info-view/purchase-requisition-master-info-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastergridview_order/:mastergridview_order?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastergridview_order.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastergridview_order', parameterName: 'mastergridview_order' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-grid-view-order/purchase-order-master-grid-view-order.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/mastergridview_order/:mastergridview_order?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastergridview_order.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastergridview_order', parameterName: 'mastergridview_order' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-grid-view-order/purchase-order-master-grid-view-order.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastergridview_order/:mastergridview_order?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastergridview_order.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastergridview_order', parameterName: 'mastergridview_order' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-grid-view-order/purchase-order-master-grid-view-order.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/mastergridview_order/:mastergridview_order?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastergridview_order.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastergridview_order', parameterName: 'mastergridview_order' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-grid-view-order/purchase-order-master-grid-view-order.vue'),
                },
                {
                    path: 'product_templates/:product_template?/product_products/:product_product?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.product_product.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_templates', parameterName: 'product_template' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-summary-view/product-product-master-summary-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.product_product.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-master-summary-view/product-product-master-summary-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastergridview_enquiry/:mastergridview_enquiry?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastergridview_enquiry.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastergridview_enquiry', parameterName: 'mastergridview_enquiry' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-grid-view-enquiry/purchase-order-master-grid-view-enquiry.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/mastergridview_enquiry/:mastergridview_enquiry?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastergridview_enquiry.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastergridview_enquiry', parameterName: 'mastergridview_enquiry' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-grid-view-enquiry/purchase-order-master-grid-view-enquiry.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastergridview_enquiry/:mastergridview_enquiry?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastergridview_enquiry.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastergridview_enquiry', parameterName: 'mastergridview_enquiry' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-grid-view-enquiry/purchase-order-master-grid-view-enquiry.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/mastergridview_enquiry/:mastergridview_enquiry?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastergridview_enquiry.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastergridview_enquiry', parameterName: 'mastergridview_enquiry' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-grid-view-enquiry/purchase-order-master-grid-view-enquiry.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-edit-view/purchase-order-master-edit-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/purchase_orders/:purchase_order?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-edit-view/purchase-order-master-edit-view.vue'),
                },
                {
                    path: 'purchase_requisitions/:purchase_requisition?/purchase_orders/:purchase_order?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-edit-view/purchase-order-master-edit-view.vue'),
                },
                {
                    path: 'purchase_orders/:purchase_order?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.purchase_order.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-order-master-edit-view/purchase-order-master-edit-view.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/mastertabview/:mastertabview?',
                    meta: {
                        caption: 'entities.mail_message.views.mastertabview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'mastertabview', parameterName: 'mastertabview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-master-tab-view/mail-message-master-tab-view.vue'),
                },
                {
                    path: 'purchase_requisition_types/:purchase_requisition_type?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.purchase_requisition_type.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'purchase_requisition_types', parameterName: 'purchase_requisition_type' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-purchase/purchase-requisition-type-basic-list-exp-view/purchase-requisition-type-basic-list-exp-view.vue'),
                },
                {
                    path: 'res_suppliers/:res_supplier?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_supplier.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'purchaseindexview', parameterName: 'purchaseindexview' },
                            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-supplier-pickup-view/res-supplier-pickup-view.vue'),
                },
            ...indexRoutes,
            ],
        },
        ...globalRoutes,
        {
            path: '/login/:login?',
            name: 'login',
            meta: {  
                caption: '登录',
                viewType: 'login',
                requireAuth: false,
                ignoreAddPage: true,
            },
            beforeEnter: (to: any, from: any, next: any) => {
                appService.navHistory.reset();
                next();
            },
            component: () => import('@components/login/login'),
        },
        {
            path: '/403/:p403?',
            name: '403',
            component: () => import('@components/403/403.vue')
        },
        {
            path: '/404/:p404?',
            name: '404',
            component: () => import('@components/404/404.vue')
        },
        {
            path: '/500/:p500',
            name: '500',
            component: () => import('@components/500/500.vue')
        },
        {
            path: '*',
            redirect: 'purchaseindexview'
        }
    ]
});

router.beforeEach((to: any, from: any, next: any) => {
    if (to.meta && !to.meta.ignoreAddPage) {
        appService.navHistory.add(to);
    }
    next();
});

// 解决路由跳转时报 => 路由重复
const originalPush = Router.prototype.push
Router.prototype.push = function push(location: any) {
    let result: any = originalPush.call(this, location);
    return result.catch((err: any) => err);
}

export default router;