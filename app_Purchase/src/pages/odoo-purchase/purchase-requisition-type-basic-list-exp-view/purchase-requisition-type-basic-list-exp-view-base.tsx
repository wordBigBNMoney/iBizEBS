import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { ListExpViewBase } from '@/studio-core';
import Purchase_requisition_typeService from '@/service/purchase-requisition-type/purchase-requisition-type-service';
import Purchase_requisition_typeAuthService from '@/authservice/purchase-requisition-type/purchase-requisition-type-auth-service';
import ListExpViewEngine from '@engine/view/list-exp-view-engine';
import Purchase_requisition_typeUIService from '@/uiservice/purchase-requisition-type/purchase-requisition-type-ui-service';

/**
 * 配置列表导航视图视图基类
 *
 * @export
 * @class Purchase_requisition_typeBasicListExpViewBase
 * @extends {ListExpViewBase}
 */
export class Purchase_requisition_typeBasicListExpViewBase extends ListExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    protected appDeName: string = 'purchase_requisition_type';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisition_typeService}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    protected appEntityService: Purchase_requisition_typeService = new Purchase_requisition_typeService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_requisition_typeUIService
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    public appUIService: Purchase_requisition_typeUIService = new Purchase_requisition_typeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_requisition_type.views.basiclistexpview.caption',
        srfTitle: 'entities.purchase_requisition_type.views.basiclistexpview.title',
        srfSubTitle: 'entities.purchase_requisition_type.views.basiclistexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    protected containerModel: any = {
        view_listexpbar: { name: 'listexpbar', type: 'LISTEXPBAR' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Purchase_requisition_typeBasicListExpView
     */
    public basiclistexpviewlistexpbar_toolbarModels: any = {
        tbitem3: { name: 'tbitem3', caption: '新建', 'isShowCaption': true, 'isShowIcon': true, tooltip: '新建', iconcls: 'fa fa-file-text-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'New', target: '', class: '' } },

        tbitem7: {  name: 'tbitem7', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem8: { name: 'tbitem8', caption: '删除', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'MULTIKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
	protected viewtag: string = '011a0f05ed392aa4fc7b7edfbd30a182';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */ 
    protected viewName:string = "purchase_requisition_typeBasicListExpView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    public engine: ListExpViewEngine = new ListExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            listexpbar: this.$refs.listexpbar,
            keyPSDEField: 'purchase_requisition_type',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * listexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    public listexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'selectionchange', $event);
    }

    /**
     * listexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    public listexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'activated', $event);
    }

    /**
     * listexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typeBasicListExpViewBase
     */
    public listexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Purchase_requisition_typeBasicListExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.purchase_requisition_type;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'purchase_requisition_types', parameterName: 'purchase_requisition_type' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'purchase-requisition-type-basic-quick-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.purchase_requisition_type.views.basicquickview.title'),
            placement: 'DRAWER_RIGHT',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Purchase_requisition_typeBasicListExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }




    /**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicListExpView
     */
    protected viewUID: string = 'odoo-purchase-purchase-requisition-type-basic-list-exp-view';


}