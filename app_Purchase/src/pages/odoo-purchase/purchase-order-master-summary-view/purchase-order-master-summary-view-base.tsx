import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import Purchase_orderService from '@/service/purchase-order/purchase-order-service';
import Purchase_orderAuthService from '@/authservice/purchase-order/purchase-order-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import Purchase_orderUIService from '@/uiservice/purchase-order/purchase-order-ui-service';

/**
 * 概览视图基类
 *
 * @export
 * @class Purchase_orderMasterSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class Purchase_orderMasterSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    protected appDeName: string = 'purchase_order';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Purchase_orderService}
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    protected appEntityService: Purchase_orderService = new Purchase_orderService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_orderUIService
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    public appUIService: Purchase_orderUIService = new Purchase_orderUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_order.views.mastersummaryview.caption',
        srfTitle: 'entities.purchase_order.views.mastersummaryview.title',
        srfSubTitle: 'entities.purchase_order.views.mastersummaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterSummaryViewBase
     */
	protected viewtag: string = 'ba1e054173433d27d55d5ce19d746621';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterSummaryViewBase
     */ 
    protected viewName:string = "purchase_orderMasterSummaryView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_orderMasterSummaryViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'purchase_order',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_orderMasterSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}