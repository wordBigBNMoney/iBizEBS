import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Purchase_requisition_typeService from '@/service/purchase-requisition-type/purchase-requisition-type-service';
import Purchase_requisition_typeAuthService from '@/authservice/purchase-requisition-type/purchase-requisition-type-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Purchase_requisition_typeUIService from '@/uiservice/purchase-requisition-type/purchase-requisition-type-ui-service';

/**
 * 配置信息编辑视图视图基类
 *
 * @export
 * @class Purchase_requisition_typeBasicEditViewBase
 * @extends {EditViewBase}
 */
export class Purchase_requisition_typeBasicEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    protected appDeName: string = 'purchase_requisition_type';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisition_typeService}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    protected appEntityService: Purchase_requisition_typeService = new Purchase_requisition_typeService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_requisition_typeUIService
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    public appUIService: Purchase_requisition_typeUIService = new Purchase_requisition_typeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_requisition_type.views.basiceditview.caption',
        srfTitle: 'entities.purchase_requisition_type.views.basiceditview.title',
        srfSubTitle: 'entities.purchase_requisition_type.views.basiceditview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
	protected viewtag: string = '6917a6b4ac10d888f7e41d0daef2df90';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */ 
    protected viewName:string = "purchase_requisition_typeBasicEditView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'purchase_requisition_type',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typeBasicEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}