import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import Purchase_requisitionService from '@/service/purchase-requisition/purchase-requisition-service';
import Purchase_requisitionAuthService from '@/authservice/purchase-requisition/purchase-requisition-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import Purchase_requisitionUIService from '@/uiservice/purchase-requisition/purchase-requisition-ui-service';

/**
 * 概览视图基类
 *
 * @export
 * @class Purchase_requisitionMasterSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class Purchase_requisitionMasterSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    protected appDeName: string = 'purchase_requisition';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisitionService}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    protected appEntityService: Purchase_requisitionService = new Purchase_requisitionService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_requisitionUIService
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    public appUIService: Purchase_requisitionUIService = new Purchase_requisitionUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_requisition.views.mastersummaryview.caption',
        srfTitle: 'entities.purchase_requisition.views.mastersummaryview.title',
        srfSubTitle: 'entities.purchase_requisition.views.mastersummaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
	protected viewtag: string = '80291fb77442ee85e0bb0ad9249c39e5';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */ 
    protected viewName:string = "purchase_requisitionMasterSummaryView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'purchase_requisition',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisitionMasterSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}