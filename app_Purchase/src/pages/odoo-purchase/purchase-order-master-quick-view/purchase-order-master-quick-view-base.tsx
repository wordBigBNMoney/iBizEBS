import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Purchase_orderService from '@/service/purchase-order/purchase-order-service';
import Purchase_orderAuthService from '@/authservice/purchase-order/purchase-order-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Purchase_orderUIService from '@/uiservice/purchase-order/purchase-order-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class Purchase_orderMasterQuickViewBase
 * @extends {OptionViewBase}
 */
export class Purchase_orderMasterQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterQuickViewBase
     */
    protected appDeName: string = 'purchase_order';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Purchase_orderService}
     * @memberof Purchase_orderMasterQuickViewBase
     */
    protected appEntityService: Purchase_orderService = new Purchase_orderService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_orderUIService
     * @memberof Purchase_orderMasterQuickViewBase
     */
    public appUIService: Purchase_orderUIService = new Purchase_orderUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_orderMasterQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_orderMasterQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_order.views.masterquickview.caption',
        srfTitle: 'entities.purchase_order.views.masterquickview.title',
        srfSubTitle: 'entities.purchase_order.views.masterquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_orderMasterQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterQuickViewBase
     */
	protected viewtag: string = '3fa97a49854e23e3ad5cdb87315bc919';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterQuickViewBase
     */ 
    protected viewName:string = "purchase_orderMasterQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_orderMasterQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_orderMasterQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_orderMasterQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'purchase_order',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_orderMasterQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_orderMasterQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_orderMasterQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}