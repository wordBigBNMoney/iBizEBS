import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Purchase_requisition_typeService from '@/service/purchase-requisition-type/purchase-requisition-type-service';
import Purchase_requisition_typeAuthService from '@/authservice/purchase-requisition-type/purchase-requisition-type-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Purchase_requisition_typeUIService from '@/uiservice/purchase-requisition-type/purchase-requisition-type-ui-service';

/**
 * 采购申请类型数据选择视图视图基类
 *
 * @export
 * @class Purchase_requisition_typePickupViewBase
 * @extends {PickupViewBase}
 */
export class Purchase_requisition_typePickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typePickupViewBase
     */
    protected appDeName: string = 'purchase_requisition_type';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typePickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typePickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisition_typeService}
     * @memberof Purchase_requisition_typePickupViewBase
     */
    protected appEntityService: Purchase_requisition_typeService = new Purchase_requisition_typeService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_requisition_typeUIService
     * @memberof Purchase_requisition_typePickupViewBase
     */
    public appUIService: Purchase_requisition_typeUIService = new Purchase_requisition_typeUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisition_typePickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_requisition_type.views.pickupview.caption',
        srfTitle: 'entities.purchase_requisition_type.views.pickupview.title',
        srfSubTitle: 'entities.purchase_requisition_type.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisition_typePickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typePickupViewBase
     */
	protected viewtag: string = '6a0c617ff14554d4fea25e8572ef67c9';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisition_typePickupViewBase
     */ 
    protected viewName:string = "purchase_requisition_typePickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_requisition_typePickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_requisition_typePickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_requisition_typePickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'purchase_requisition_type',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typePickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typePickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisition_typePickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}