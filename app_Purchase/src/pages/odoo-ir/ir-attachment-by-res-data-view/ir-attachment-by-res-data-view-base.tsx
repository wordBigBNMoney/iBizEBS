

import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { DataViewBase } from '@/studio-core';
import Ir_attachmentService from '@/service/ir-attachment/ir-attachment-service';
import Ir_attachmentAuthService from '@/authservice/ir-attachment/ir-attachment-auth-service';
import DataViewEngine from '@engine/view/data-view-engine';
import Ir_attachmentUIService from '@/uiservice/ir-attachment/ir-attachment-ui-service';
import CodeListService from "@service/app/codelist-service";


/**
 * 附件数据视图视图基类
 *
 * @export
 * @class Ir_attachmentByResDataViewBase
 * @extends {DataViewBase}
 */
export class Ir_attachmentByResDataViewBase extends DataViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Ir_attachmentByResDataViewBase
     */
    protected appDeName: string = 'ir_attachment';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Ir_attachmentByResDataViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Ir_attachmentByResDataViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Ir_attachmentByResDataViewBase
     */ 
    protected dataControl:string = "dataview";

    /**
     * 实体服务对象
     *
     * @type {Ir_attachmentService}
     * @memberof Ir_attachmentByResDataViewBase
     */
    protected appEntityService: Ir_attachmentService = new Ir_attachmentService;

    /**
     * 实体权限服务对象
     *
     * @type Ir_attachmentUIService
     * @memberof Ir_attachmentByResDataViewBase
     */
    public appUIService: Ir_attachmentUIService = new Ir_attachmentUIService(this.$store);

	/**
	 * 自定义视图导航参数集合
	 *
     * @protected
	 * @type {*}
	 * @memberof Ir_attachmentByResDataViewBase
	 */
    protected customViewParams: any = {
        'n_res_model_eq': { isRawValue: false, value: 'n_res_model_eq' },
        'n_res_id_eq': { isRawValue: false, value: 'n_res_id_eq' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Ir_attachmentByResDataViewBase
     */
    protected model: any = {
        srfCaption: 'entities.ir_attachment.views.byresdataview.caption',
        srfTitle: 'entities.ir_attachment.views.byresdataview.title',
        srfSubTitle: 'entities.ir_attachment.views.byresdataview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Ir_attachmentByResDataViewBase
     */
    protected containerModel: any = {
        view_dataview: { name: 'dataview', type: 'DATAVIEW' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Ir_attachmentByResDataViewBase
     */
	protected viewtag: string = 'd5688a3507c45bd42213ffdd5a8376fc';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Ir_attachmentByResDataViewBase
     */ 
    protected viewName:string = "ir_attachmentByResDataView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Ir_attachmentByResDataViewBase
     */
    public engine: DataViewEngine = new DataViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Ir_attachmentByResDataViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Ir_attachmentByResDataViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.opendata(args,fullargs, params, $event, xData);
            },
            newdata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.newdata(args,fullargs, params, $event, xData);
            },
            dataview: this.$refs.dataview,
            keyPSDEField: 'ir_attachment',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * dataview 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Ir_attachmentByResDataViewBase
     */
    public dataview_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dataview', 'selectionchange', $event);
    }

    /**
     * dataview 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Ir_attachmentByResDataViewBase
     */
    public dataview_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dataview', 'beforeload', $event);
    }

    /**
     * dataview 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Ir_attachmentByResDataViewBase
     */
    public dataview_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dataview', 'rowdblclick', $event);
    }

    /**
     * dataview 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Ir_attachmentByResDataViewBase
     */
    public dataview_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dataview', 'remove', $event);
    }

    /**
     * dataview 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Ir_attachmentByResDataViewBase
     */
    public dataview_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dataview', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Ir_attachmentByResDataView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Ir_attachmentByResDataView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Ir_attachmentByResDataView
     */
    protected viewUID: string = 'odoo-ir-ir-attachment-by-res-data-view';
}