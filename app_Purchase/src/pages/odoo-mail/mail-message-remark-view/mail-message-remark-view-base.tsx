import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import Mail_messageAuthService from '@/authservice/mail-message/mail-message-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';

/**
 * 消息编辑视图视图基类
 *
 * @export
 * @class Mail_messageRemarkViewBase
 * @extends {EditViewBase}
 */
export class Mail_messageRemarkViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageRemarkViewBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageRemarkViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageRemarkViewBase
     */
    protected appDeMajor: string = 'id';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageRemarkViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof Mail_messageRemarkViewBase
     */
    protected appEntityService: Mail_messageService = new Mail_messageService;

    /**
     * 实体权限服务对象
     *
     * @type Mail_messageUIService
     * @memberof Mail_messageRemarkViewBase
     */
    public appUIService: Mail_messageUIService = new Mail_messageUIService(this.$store);

	/**
	 * 自定义视图导航参数集合
	 *
     * @protected
	 * @type {*}
	 * @memberof Mail_messageRemarkViewBase
	 */
    protected customViewParams: any = {
        'n_model_eq': { isRawValue: false, value: 'n_res_model_eq' },
        'n_res_id_eq': { isRawValue: false, value: 'n_res_id_eq' }
    };

    /**
     * 是否显示信息栏
     *
     * @memberof Mail_messageRemarkViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Mail_messageRemarkViewBase
     */
    protected model: any = {
        srfCaption: 'entities.mail_message.views.remarkview.caption',
        srfTitle: 'entities.mail_message.views.remarkview.title',
        srfSubTitle: 'entities.mail_message.views.remarkview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Mail_messageRemarkViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageRemarkViewBase
     */
	protected viewtag: string = '7d1c76645cc1b9d4a16e555ed36f03c1';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageRemarkViewBase
     */ 
    protected viewName:string = "mail_messageRemarkView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Mail_messageRemarkViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Mail_messageRemarkViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Mail_messageRemarkViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'mail_message',
            majorPSDEField: 'id',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageRemarkViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageRemarkViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageRemarkViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}