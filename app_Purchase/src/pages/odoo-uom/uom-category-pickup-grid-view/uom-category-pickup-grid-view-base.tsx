import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import Uom_categoryAuthService from '@/authservice/uom-category/uom-category-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Uom_categoryUIService from '@/uiservice/uom-category/uom-category-ui-service';

/**
 * 产品计量单位 类别选择表格视图视图基类
 *
 * @export
 * @class Uom_categoryPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Uom_categoryPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupGridViewBase
     */
    protected appDeName: string = 'uom_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupGridViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Uom_categoryService}
     * @memberof Uom_categoryPickupGridViewBase
     */
    protected appEntityService: Uom_categoryService = new Uom_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Uom_categoryUIService
     * @memberof Uom_categoryPickupGridViewBase
     */
    public appUIService: Uom_categoryUIService = new Uom_categoryUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom_category.views.pickupgridview.caption',
        srfTitle: 'entities.uom_category.views.pickupgridview.title',
        srfSubTitle: 'entities.uom_category.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupGridViewBase
     */
	protected viewtag: string = '825088b4d6d996d3789b9fec1eed8b7a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupGridViewBase
     */ 
    protected viewName:string = "uom_categoryPickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Uom_categoryPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Uom_categoryPickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Uom_categoryPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'uom_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Uom_categoryPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}