import Res_country_stateUIServiceBase from './res-country-state-ui-service-base';

/**
 * 国家/地区州/省UI服务对象
 *
 * @export
 * @class Res_country_stateUIService
 */
export default class Res_country_stateUIService extends Res_country_stateUIServiceBase {

    /**
     * Creates an instance of  Res_country_stateUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_country_stateUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}