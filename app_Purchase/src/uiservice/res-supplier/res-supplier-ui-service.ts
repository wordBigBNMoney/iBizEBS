import Res_supplierUIServiceBase from './res-supplier-ui-service-base';

/**
 * 供应商UI服务对象
 *
 * @export
 * @class Res_supplierUIService
 */
export default class Res_supplierUIService extends Res_supplierUIServiceBase {

    /**
     * Creates an instance of  Res_supplierUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_supplierUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}