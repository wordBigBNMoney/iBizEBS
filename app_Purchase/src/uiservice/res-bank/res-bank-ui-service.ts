import Res_bankUIServiceBase from './res-bank-ui-service-base';

/**
 * 银行UI服务对象
 *
 * @export
 * @class Res_bankUIService
 */
export default class Res_bankUIService extends Res_bankUIServiceBase {

    /**
     * Creates an instance of  Res_bankUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_bankUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}