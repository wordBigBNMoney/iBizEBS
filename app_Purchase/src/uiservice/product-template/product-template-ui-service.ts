import Product_templateUIServiceBase from './product-template-ui-service-base';

/**
 * 产品模板UI服务对象
 *
 * @export
 * @class Product_templateUIService
 */
export default class Product_templateUIService extends Product_templateUIServiceBase {

    /**
     * Creates an instance of  Product_templateUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_templateUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}