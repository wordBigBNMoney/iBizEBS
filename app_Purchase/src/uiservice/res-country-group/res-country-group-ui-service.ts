import Res_country_groupUIServiceBase from './res-country-group-ui-service-base';

/**
 * 国家/地区群组UI服务对象
 *
 * @export
 * @class Res_country_groupUIService
 */
export default class Res_country_groupUIService extends Res_country_groupUIServiceBase {

    /**
     * Creates an instance of  Res_country_groupUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_country_groupUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}