import Stock_picking_typeUIServiceBase from './stock-picking-type-ui-service-base';

/**
 * 拣货类型UI服务对象
 *
 * @export
 * @class Stock_picking_typeUIService
 */
export default class Stock_picking_typeUIService extends Stock_picking_typeUIServiceBase {

    /**
     * Creates an instance of  Stock_picking_typeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Stock_picking_typeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}