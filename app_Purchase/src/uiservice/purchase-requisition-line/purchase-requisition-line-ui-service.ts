import Purchase_requisition_lineUIServiceBase from './purchase-requisition-line-ui-service-base';

/**
 * 采购申请行UI服务对象
 *
 * @export
 * @class Purchase_requisition_lineUIService
 */
export default class Purchase_requisition_lineUIService extends Purchase_requisition_lineUIServiceBase {

    /**
     * Creates an instance of  Purchase_requisition_lineUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisition_lineUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}