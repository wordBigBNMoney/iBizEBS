import Account_taxUIServiceBase from './account-tax-ui-service-base';

/**
 * 税率UI服务对象
 *
 * @export
 * @class Account_taxUIService
 */
export default class Account_taxUIService extends Account_taxUIServiceBase {

    /**
     * Creates an instance of  Account_taxUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_taxUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}