import Delivery_carrierUIServiceBase from './delivery-carrier-ui-service-base';

/**
 * 送货方式UI服务对象
 *
 * @export
 * @class Delivery_carrierUIService
 */
export default class Delivery_carrierUIService extends Delivery_carrierUIServiceBase {

    /**
     * Creates an instance of  Delivery_carrierUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Delivery_carrierUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}