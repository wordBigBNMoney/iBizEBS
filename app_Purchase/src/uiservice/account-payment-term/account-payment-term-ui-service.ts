import Account_payment_termUIServiceBase from './account-payment-term-ui-service-base';

/**
 * 付款条款UI服务对象
 *
 * @export
 * @class Account_payment_termUIService
 */
export default class Account_payment_termUIService extends Account_payment_termUIServiceBase {

    /**
     * Creates an instance of  Account_payment_termUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_payment_termUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}