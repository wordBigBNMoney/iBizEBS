import Product_template_attribute_lineUIServiceBase from './product-template-attribute-line-ui-service-base';

/**
 * 产品模板属性明细行UI服务对象
 *
 * @export
 * @class Product_template_attribute_lineUIService
 */
export default class Product_template_attribute_lineUIService extends Product_template_attribute_lineUIServiceBase {

    /**
     * Creates an instance of  Product_template_attribute_lineUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_template_attribute_lineUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}