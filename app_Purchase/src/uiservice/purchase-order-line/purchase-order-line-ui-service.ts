import Purchase_order_lineUIServiceBase from './purchase-order-line-ui-service-base';

/**
 * 采购订单行UI服务对象
 *
 * @export
 * @class Purchase_order_lineUIService
 */
export default class Purchase_order_lineUIService extends Purchase_order_lineUIServiceBase {

    /**
     * Creates an instance of  Purchase_order_lineUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_order_lineUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}