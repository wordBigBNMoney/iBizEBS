import Uom_categoryUIServiceBase from './uom-category-ui-service-base';

/**
 * 产品计量单位 类别UI服务对象
 *
 * @export
 * @class Uom_categoryUIService
 */
export default class Uom_categoryUIService extends Uom_categoryUIServiceBase {

    /**
     * Creates an instance of  Uom_categoryUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Uom_categoryUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}