/**
 * 所有应用视图
 */
export const viewstate: any = {
    appviews: [
        {
            viewtag: '011a0f05ed392aa4fc7b7edfbd30a182',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisition_typeBasicListExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '6cf91abfbf2a39c529df314651e7b8e9',
                '6917a6b4ac10d888f7e41d0daef2df90',
            ],
        },
        {
            viewtag: '03656c91dd184ab62a81166134954d3b',
            viewmodule: 'odoo_product',
            viewname: 'product_productPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '36b18dd7ec23975b726c9ce6149e1f4e',
            ],
        },
        {
            viewtag: '0678faedaa5916c618d1d73a44b13024',
            viewmodule: 'odoo_account',
            viewname: 'account_taxPurchasePickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '083974d7385593d391f4c25235061b19',
            viewmodule: 'odoo_product',
            viewname: 'product_templatePickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '985f726271ed67e18824f88e3b68bb17',
            ],
        },
        {
            viewtag: '0a68f3cf7fc39261aec28734fcaec309',
            viewmodule: 'odoo_base',
            viewname: 'res_supplierMasterTabInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'b42a148563e92720dcbdc83b5d46117a',
                '72e741b20d81402c6a306e779d4d24b7',
                '23d299cd56de1e4e8cabd5d5dc155438',
                '881f113aaefe3dc53ffd9c967417bf3e',
                'fa79c811f8008cd1b1d9e2f4502e85d8',
            ],
        },
        {
            viewtag: '0bb629da79eaf7ac10841ccb2737e094',
            viewmodule: 'odoo_base',
            viewname: 'res_partner_categoryPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '0df51febb18b55dd835055f5fcb60e46',
            viewmodule: 'odoo_base',
            viewname: 'res_partner_categoryMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0bb629da79eaf7ac10841ccb2737e094',
            ],
        },
        {
            viewtag: '0ed9b8a29b1aaaf542dbcc7b9ea9ec1d',
            viewmodule: 'odoo_mail',
            viewname: 'mail_activityEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '12583aa40f1e9c0eaa5a5ba2ce0dd7a4',
            viewmodule: 'odoo_product',
            viewname: 'product_templateMasterInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '1551b807bd3deca8061c8cda38fadbc2',
            viewmodule: 'odoo_uom',
            viewname: 'uom_uomBasicListExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '7d4c11699bc484f1477d6c640fbd63cf',
                'e3a30586132f7bb68b424b8057900c39',
            ],
        },
        {
            viewtag: '175b7500bc95e4c2564abecb6e5bfb57',
            viewmodule: 'odoo_product',
            viewname: 'product_templateMasterTabInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c49562ec1ffc30686416dd5faee2326d',
                '44a7d256aebf043cc67d2e1b956d61eb',
                '927f2214efea768cf3cd3c4cffea0f08',
            ],
        },
        {
            viewtag: '23d299cd56de1e4e8cabd5d5dc155438',
            viewmodule: 'odoo_product',
            viewname: 'product_supplierinfoBasicListExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '794b114292ebc7840d037dc2b553456c',
                'e10d70201b07a1a9a3f9126cf60265ba',
            ],
        },
        {
            viewtag: '244f52820f45a1fd04532bf700eb3af6',
            viewmodule: 'odoo_uom',
            viewname: 'uom_categoryBasicQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '273bd8803959ff9357b9c2479037352c',
            viewmodule: 'odoo_base',
            viewname: 'res_supplierMasterInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '73a5fcd7f7450ec1c38adecb5fd8fc38',
            ],
        },
        {
            viewtag: '2c2058b72216560ba352abf8f1fb59de',
            viewmodule: 'odoo_product',
            viewname: 'product_productMasterQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '083974d7385593d391f4c25235061b19',
            ],
        },
        {
            viewtag: '2f2403c07f98ac47a93c2ccbf1485a9c',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_orderMasterInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'f3659046722b66fc4a405e6967b1cc86',
            ],
        },
        {
            viewtag: '360a6bd6871b47937d21942ed041e923',
            viewmodule: 'odoo_product',
            viewname: 'product_productMasterEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '083974d7385593d391f4c25235061b19',
            ],
        },
        {
            viewtag: '3650b5d36d60a645cbc3dbdb6aebd03b',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisition_lineEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '36b18dd7ec23975b726c9ce6149e1f4e',
            viewmodule: 'odoo_product',
            viewname: 'product_productPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '37f650c54b5668f3be6a4511c4b81f66',
            viewmodule: 'odoo_base',
            viewname: 'res_supplierMasterGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '4a9f1473ff1847a6c719009ea4362e88',
                '0a68f3cf7fc39261aec28734fcaec309',
            ],
        },
        {
            viewtag: '3fa97a49854e23e3ad5cdb87315bc919',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_orderMasterQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'f65c8430fe2c75de5d2abdb244453ef7',
            ],
        },
        {
            viewtag: '44a7d256aebf043cc67d2e1b956d61eb',
            viewmodule: 'odoo_product',
            viewname: 'product_templateMasterSummaryView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '12583aa40f1e9c0eaa5a5ba2ce0dd7a4',
            ],
        },
        {
            viewtag: '474a86d5a3c0c3907c16dc3fdfcabfb8',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_orderMasterTabInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'd0e6c6f2ae964187efd612a848818619',
                'ba1e054173433d27d55d5ce19d746621',
            ],
        },
        {
            viewtag: '47b5eabc445ed79c0fd04d8da748c776',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageMainView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'fc5eec6ef08d3fe889ac304fa8d6b45d',
                'e79e0aa4e64b7c70a08332f6cc3aaa93',
                'b08e5b415c5f01a339a70045e7d3bbd2',
                'c1160abb36ff4fc18ddcaea34cc2ab51',
            ],
        },
        {
            viewtag: '4a9f1473ff1847a6c719009ea4362e88',
            viewmodule: 'odoo_base',
            viewname: 'res_supplierMasterQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '6abc5f67274f993c9ecb116f94900e85',
            ],
        },
        {
            viewtag: '6917a6b4ac10d888f7e41d0daef2df90',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisition_typeBasicEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '6928c8dc50acd54e98c657410871afa9',
            viewmodule: 'odoo_base',
            viewname: 'res_usersPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '9d9be8fc580e553c1afb70e2906de3b1',
            ],
        },
        {
            viewtag: '69d64b992c942b72ac526dbbb4fa3e6c',
            viewmodule: 'odoo_product',
            viewname: 'product_productMasterSummaryView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'f70a6b0e958493244ca97f4177ba1c24',
            ],
        },
        {
            viewtag: '6a0c617ff14554d4fea25e8572ef67c9',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisition_typePickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '77a2a70b11298000acab49c9de786dea',
            ],
        },
        {
            viewtag: '6abc5f67274f993c9ecb116f94900e85',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'b9c01aac6e18ec1eed1b74d47f275384',
            ],
        },
        {
            viewtag: '6c2ed7dbdc3e94cfd8dadb9915ac60d1',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisition_lineLineEdit',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '3650b5d36d60a645cbc3dbdb6aebd03b',
            ],
        },
        {
            viewtag: '6cf91abfbf2a39c529df314651e7b8e9',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisition_typeBasicQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '6ff1a605b7c38a825e0b678bb9b0b340',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisition_lineLine',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '3650b5d36d60a645cbc3dbdb6aebd03b',
            ],
        },
        {
            viewtag: '7045de775c0fdff5d3e3bd0cef75f348',
            viewmodule: 'odoo_base',
            viewname: 'res_config_settingsPurchaseEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '72e741b20d81402c6a306e779d4d24b7',
            viewmodule: 'odoo_base',
            viewname: 'res_supplierMasterEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0df51febb18b55dd835055f5fcb60e46',
                'a7f3041df1b70e8537a0e99589e43185',
            ],
        },
        {
            viewtag: '73a5fcd7f7450ec1c38adecb5fd8fc38',
            viewmodule: 'odoo_base',
            viewname: 'res_partner_bankLine',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'b37c46823c4ae40f305089f3a62ff069',
            ],
        },
        {
            viewtag: '73bbfb31298bf72c9dbd20a9ff078c3a',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_order_lineEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '73ec5ee653ea252842738499c351e806',
            viewmodule: 'odoo_uom',
            viewname: 'uom_categoryBasicEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '77a2a70b11298000acab49c9de786dea',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisition_typePickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '78c65f1c30d343c4e926b75e04a0270c',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_orderMasterGridView_Order',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '474a86d5a3c0c3907c16dc3fdfcabfb8',
                '3fa97a49854e23e3ad5cdb87315bc919',
            ],
        },
        {
            viewtag: '794b114292ebc7840d037dc2b553456c',
            viewmodule: 'odoo_product',
            viewname: 'product_supplierinfoBasicQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '083974d7385593d391f4c25235061b19',
                'f65c8430fe2c75de5d2abdb244453ef7',
            ],
        },
        {
            viewtag: '7d1c76645cc1b9d4a16e555ed36f03c1',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageRemarkView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '7d4c11699bc484f1477d6c640fbd63cf',
            viewmodule: 'odoo_uom',
            viewname: 'uom_uomBasicQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c98d519438489bdaaf82b58443149cc1',
            ],
        },
        {
            viewtag: '80291fb77442ee85e0bb0ad9249c39e5',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisitionMasterSummaryView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '47b5eabc445ed79c0fd04d8da748c776',
                'ed8d6abde7ba9daaccdfa5049373df37',
            ],
        },
        {
            viewtag: '825088b4d6d996d3789b9fec1eed8b7a',
            viewmodule: 'odoo_uom',
            viewname: 'uom_categoryPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '85a1249f4157e1334350ca14e8960e4c',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_order_lineLineEdit',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '9cfc393e5fbc8f48d6de3e36d5bdb397',
                '73bbfb31298bf72c9dbd20a9ff078c3a',
            ],
        },
        {
            viewtag: '881f113aaefe3dc53ffd9c967417bf3e',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_orderMasterGridView_Enquiry',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '474a86d5a3c0c3907c16dc3fdfcabfb8',
                '3fa97a49854e23e3ad5cdb87315bc919',
            ],
        },
        {
            viewtag: '8edd23ee9ade06059e61231de1194c86',
            viewmodule: 'odoo_product',
            viewname: 'product_categoryBasicListExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'd59fb68967527633b916a81f3fa6ae0b',
                '913d12caf6e3d513f7e2939e616ff1f3',
            ],
        },
        {
            viewtag: '913d12caf6e3d513f7e2939e616ff1f3',
            viewmodule: 'odoo_product',
            viewname: 'product_categoryBasicEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '927f2214efea768cf3cd3c4cffea0f08',
            viewmodule: 'odoo_product',
            viewname: 'product_templateMasterEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '96541c93d48032e914cc7645b075a17c',
            viewmodule: 'odoo_base',
            viewname: 'res_supplierPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '985f726271ed67e18824f88e3b68bb17',
            viewmodule: 'odoo_product',
            viewname: 'product_templatePickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '999794b38995fb8355f30590b92500b1',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisitionMasterTabInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'cbed9661c4340482cd2c392a349d8edc',
                '881f113aaefe3dc53ffd9c967417bf3e',
                '80291fb77442ee85e0bb0ad9249c39e5',
                '3fa97a49854e23e3ad5cdb87315bc919',
            ],
        },
        {
            viewtag: '9cfc393e5fbc8f48d6de3e36d5bdb397',
            viewmodule: 'odoo_account',
            viewname: 'account_taxPurchaseMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0678faedaa5916c618d1d73a44b13024',
            ],
        },
        {
            viewtag: '9d9be8fc580e553c1afb70e2906de3b1',
            viewmodule: 'odoo_base',
            viewname: 'res_usersPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'a7f3041df1b70e8537a0e99589e43185',
            viewmodule: 'odoo_base',
            viewname: 'res_partner_bankLineEdit',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'b37c46823c4ae40f305089f3a62ff069',
            ],
        },
        {
            viewtag: 'b08e5b415c5f01a339a70045e7d3bbd2',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageByResListView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'b37c46823c4ae40f305089f3a62ff069',
            viewmodule: 'odoo_base',
            viewname: 'res_partner_bankEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'b42a148563e92720dcbdc83b5d46117a',
            viewmodule: 'odoo_base',
            viewname: 'res_supplierMasterSummaryView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '273bd8803959ff9357b9c2479037352c',
            ],
        },
        {
            viewtag: 'b9c01aac6e18ec1eed1b74d47f275384',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'ba1e054173433d27d55d5ce19d746621',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_orderMasterSummaryView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '2f2403c07f98ac47a93c2ccbf1485a9c',
            ],
        },
        {
            viewtag: 'be4ed5c86df8369fa55d46dd5c404cd0',
            viewmodule: 'odoo_product',
            viewname: 'product_templateMasterGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'e85d9e51a4c1b134bb3745f0817f34bf',
                '175b7500bc95e4c2564abecb6e5bfb57',
            ],
        },
        {
            viewtag: 'c1160abb36ff4fc18ddcaea34cc2ab51',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageMasterTabView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'dbd94e64a1afa7763551ee81ad319556',
                'fc5eec6ef08d3fe889ac304fa8d6b45d',
                'd5688a3507c45bd42213ffdd5a8376fc',
                '7d1c76645cc1b9d4a16e555ed36f03c1',
            ],
        },
        {
            viewtag: 'c3c9c9431ba6282dfa40dabd8dc01145',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisitionMasterQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'c49562ec1ffc30686416dd5faee2326d',
            viewmodule: 'odoo_product',
            viewname: 'product_productMasterGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '2c2058b72216560ba352abf8f1fb59de',
                'fdabc40fcb32358d1035072e381e847c',
            ],
        },
        {
            viewtag: 'c98d519438489bdaaf82b58443149cc1',
            viewmodule: 'odoo_uom',
            viewname: 'uom_categoryPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '825088b4d6d996d3789b9fec1eed8b7a',
            ],
        },
        {
            viewtag: 'cbed9661c4340482cd2c392a349d8edc',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisitionMasterEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '6a0c617ff14554d4fea25e8572ef67c9',
                '6c2ed7dbdc3e94cfd8dadb9915ac60d1',
                '6928c8dc50acd54e98c657410871afa9',
            ],
        },
        {
            viewtag: 'd0e6c6f2ae964187efd612a848818619',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_orderMasterEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '85a1249f4157e1334350ca14e8960e4c',
            ],
        },
        {
            viewtag: 'd5688a3507c45bd42213ffdd5a8376fc',
            viewmodule: 'odoo_ir',
            viewname: 'ir_attachmentByResDataView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'd59fb68967527633b916a81f3fa6ae0b',
            viewmodule: 'odoo_product',
            viewname: 'product_categoryBasicQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'dbd94e64a1afa7763551ee81ad319556',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageSendView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'e10d70201b07a1a9a3f9126cf60265ba',
            viewmodule: 'odoo_product',
            viewname: 'product_supplierinfoBasicEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '083974d7385593d391f4c25235061b19',
                '03656c91dd184ab62a81166134954d3b',
                'f65c8430fe2c75de5d2abdb244453ef7',
            ],
        },
        {
            viewtag: 'e3a30586132f7bb68b424b8057900c39',
            viewmodule: 'odoo_uom',
            viewname: 'uom_uomBasicEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c98d519438489bdaaf82b58443149cc1',
            ],
        },
        {
            viewtag: 'e79e0aa4e64b7c70a08332f6cc3aaa93',
            viewmodule: 'odoo_mail',
            viewname: 'mail_activityByResListView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0ed9b8a29b1aaaf542dbcc7b9ea9ec1d',
            ],
        },
        {
            viewtag: 'e85d9e51a4c1b134bb3745f0817f34bf',
            viewmodule: 'odoo_product',
            viewname: 'product_templateMasterQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'ed8d6abde7ba9daaccdfa5049373df37',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisitionMasterInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '6ff1a605b7c38a825e0b678bb9b0b340',
            ],
        },
        {
            viewtag: 'f3659046722b66fc4a405e6967b1cc86',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_order_lineLine',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '73bbfb31298bf72c9dbd20a9ff078c3a',
            ],
        },
        {
            viewtag: 'f65c8430fe2c75de5d2abdb244453ef7',
            viewmodule: 'odoo_base',
            viewname: 'res_supplierPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '96541c93d48032e914cc7645b075a17c',
            ],
        },
        {
            viewtag: 'f70a6b0e958493244ca97f4177ba1c24',
            viewmodule: 'odoo_product',
            viewname: 'product_productMasterInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'f76810e24d59572d6248acabba384f15',
            viewmodule: 'odoo_uom',
            viewname: 'uom_categoryBasicListExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '244f52820f45a1fd04532bf700eb3af6',
                '73ec5ee653ea252842738499c351e806',
            ],
        },
        {
            viewtag: 'fa79c811f8008cd1b1d9e2f4502e85d8',
            viewmodule: 'odoo_purchase',
            viewname: 'purchase_requisitionMasterGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '999794b38995fb8355f30590b92500b1',
                'c3c9c9431ba6282dfa40dabd8dc01145',
            ],
        },
        {
            viewtag: 'fb00976b7de01eefb698c5529e7bc9a0',
            viewmodule: 'odoo_purchase',
            viewname: 'PurchaseIndexView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '1551b807bd3deca8061c8cda38fadbc2',
                'c49562ec1ffc30686416dd5faee2326d',
                '23d299cd56de1e4e8cabd5d5dc155438',
                '881f113aaefe3dc53ffd9c967417bf3e',
                '7045de775c0fdff5d3e3bd0cef75f348',
                'be4ed5c86df8369fa55d46dd5c404cd0',
                'fa79c811f8008cd1b1d9e2f4502e85d8',
                'f76810e24d59572d6248acabba384f15',
                '011a0f05ed392aa4fc7b7edfbd30a182',
                '78c65f1c30d343c4e926b75e04a0270c',
                '8edd23ee9ade06059e61231de1194c86',
                '37f650c54b5668f3be6a4511c4b81f66',
            ],
        },
        {
            viewtag: 'fc5eec6ef08d3fe889ac304fa8d6b45d',
            viewmodule: 'odoo_mail',
            viewname: 'mail_followersByResListView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'fdabc40fcb32358d1035072e381e847c',
            viewmodule: 'odoo_product',
            viewname: 'product_productMasterTabInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '69d64b992c942b72ac526dbbb4fa3e6c',
                '360a6bd6871b47937d21942ed041e923',
            ],
        },
    ],
    createdviews: [],
}