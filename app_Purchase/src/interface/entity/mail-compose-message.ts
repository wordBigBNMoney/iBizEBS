/**
 * 邮件撰写向导
 *
 * @export
 * @interface Mail_compose_message
 */
export interface Mail_compose_message {

    /**
     * 有效域名
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    active_domain?: any;

    /**
     * 内容
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    body?: any;

    /**
     * 收藏夹
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    starred_partner_ids?: any;

    /**
     * 待处理的业务伙伴
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    needaction_partner_ids?: any;

    /**
     * 管理状态
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    moderation_status?: any;

    /**
     * 删除邮件
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    auto_delete?: any;

    /**
     * 使用有效域名
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    use_active_domain?: any;

    /**
     * 群发邮件标题
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mass_mailing_name?: any;

    /**
     * 相关评级
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    rating_ids?: any;

    /**
     * 通知
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    notification_ids?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    description?: any;

    /**
     * 添加联系人
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    partner_ids?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    display_name?: any;

    /**
     * 无响应
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    no_auto_thread?: any;

    /**
     * 追踪值
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    tracking_value_ids?: any;

    /**
     * 回复 至
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    reply_to?: any;

    /**
     * 有误差
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    has_error?: any;

    /**
     * 消息ID
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    message_id?: any;

    /**
     * 写作模式
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    composition_mode?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    __last_update?: any;

    /**
     * 待处理
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    needaction?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    attachment_ids?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    subject?: any;

    /**
     * 添加签名
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    add_sign?: any;

    /**
     * 邮件发送服务器
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mail_server_id?: any;

    /**
     * 渠道
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    channel_ids?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    date?: any;

    /**
     * 已发布
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    website_published?: any;

    /**
     * 下级消息
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    child_ids?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    message_type?: any;

    /**
     * 相关文档编号
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    res_id?: any;

    /**
     * 删除消息副本
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    auto_delete_message?: any;

    /**
     * 需审核
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    need_moderation?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    create_date?: any;

    /**
     * 布局
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    layout?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    write_date?: any;

    /**
     * 评级值
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    rating_value?: any;

    /**
     * 相关的文档模型
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    model?: any;

    /**
     * 通知关注者
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    notify?: any;

    /**
     * 从
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    email_from?: any;

    /**
     * 邮件列表
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mailing_list_ids?: any;

    /**
     * 记录内部备注
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    is_log?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    id?: any;

    /**
     * 加星的邮件
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    starred?: any;

    /**
     * 消息记录名称
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    record_name?: any;

    /**
     * 使用模版
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    template_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    create_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    write_uid_text?: any;

    /**
     * 作者
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    author_id_text?: any;

    /**
     * 子类型
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    subtype_id_text?: any;

    /**
     * 管理员
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    moderator_id_text?: any;

    /**
     * 群发邮件营销
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mass_mailing_campaign_id_text?: any;

    /**
     * 邮件活动类型
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mail_activity_type_id_text?: any;

    /**
     * 作者头像
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    author_avatar?: any;

    /**
     * 群发邮件
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mass_mailing_id_text?: any;

    /**
     * 上级消息
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    parent_id?: any;

    /**
     * 子类型
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    subtype_id?: any;

    /**
     * 使用模版
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    template_id?: any;

    /**
     * 邮件活动类型
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mail_activity_type_id?: any;

    /**
     * 作者
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    author_id?: any;

    /**
     * 群发邮件营销
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mass_mailing_campaign_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    create_uid?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    write_uid?: any;

    /**
     * 管理员
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    moderator_id?: any;

    /**
     * 群发邮件
     *
     * @returns {*}
     * @memberof Mail_compose_message
     */
    mass_mailing_id?: any;
}