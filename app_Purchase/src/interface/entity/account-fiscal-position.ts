/**
 * 税科目调整
 *
 * @export
 * @interface Account_fiscal_position
 */
export interface Account_fiscal_position {

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    __last_update?: any;

    /**
     * 税科目调整
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    name?: any;

    /**
     * VAT必须
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    vat_required?: any;

    /**
     * 科目映射
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    account_ids?: any;

    /**
     * 状态数
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    states_count?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    sequence?: any;

    /**
     * 联邦政府
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    state_ids?: any;

    /**
     * 自动检测
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    auto_apply?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    write_date?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    display_name?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    id?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    note?: any;

    /**
     * 邮编范围到
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    zip_to?: any;

    /**
     * 税映射
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    tax_ids?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    active?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    create_date?: any;

    /**
     * 邮编范围从
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    zip_from?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    create_uid_text?: any;

    /**
     * 国家群组
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    country_group_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    company_id_text?: any;

    /**
     * 国家
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    country_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    company_id?: any;

    /**
     * 国家
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    country_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    write_uid?: any;

    /**
     * 国家群组
     *
     * @returns {*}
     * @memberof Account_fiscal_position
     */
    country_group_id?: any;
}