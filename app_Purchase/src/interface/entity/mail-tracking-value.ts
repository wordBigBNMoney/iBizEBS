/**
 * 邮件跟踪值
 *
 * @export
 * @interface Mail_tracking_value
 */
export interface Mail_tracking_value {

    /**
     * 字段说明
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    field_desc?: any;

    /**
     * 旧字符值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    old_value_char?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    write_date?: any;

    /**
     * 新日期时间值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    new_value_datetime?: any;

    /**
     * 旧货币值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    old_value_monetary?: any;

    /**
     * tracking_sequence
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    tracking_sequence?: any;

    /**
     * 新字符值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    new_value_char?: any;

    /**
     * 新文本值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    new_value_text?: any;

    /**
     * 新货币值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    new_value_monetary?: any;

    /**
     * 旧日期时间值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    old_value_datetime?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    __last_update?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    create_date?: any;

    /**
     * 旧整数值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    old_value_integer?: any;

    /**
     * 旧文本值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    old_value_text?: any;

    /**
     * 字段类型
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    field_type?: any;

    /**
     * 新整数值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    new_value_integer?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    display_name?: any;

    /**
     * 新浮点值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    new_value_float?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    id?: any;

    /**
     * 更改的字段
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    field?: any;

    /**
     * 旧浮点值
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    old_value_float?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    create_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    create_uid?: any;

    /**
     * 邮件消息ID
     *
     * @returns {*}
     * @memberof Mail_tracking_value
     */
    mail_message_id?: any;
}