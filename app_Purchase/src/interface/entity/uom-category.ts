/**
 * 产品计量单位 类别
 *
 * @export
 * @interface Uom_category
 */
export interface Uom_category {

    /**
     * 计量单位类别
     *
     * @returns {*}
     * @memberof Uom_category
     */
    name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Uom_category
     */
    create_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Uom_category
     */
    __last_update?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Uom_category
     */
    write_date?: any;

    /**
     * 分组销售点中的产品
     *
     * @returns {*}
     * @memberof Uom_category
     */
    is_pos_groupable?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Uom_category
     */
    display_name?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Uom_category
     */
    id?: any;

    /**
     * 计量类型
     *
     * @returns {*}
     * @memberof Uom_category
     */
    measure_type?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Uom_category
     */
    create_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Uom_category
     */
    write_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Uom_category
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Uom_category
     */
    create_uid?: any;
}