/**
 * 国家/地区州/省
 *
 * @export
 * @interface Res_country_state
 */
export interface Res_country_state {

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    __last_update?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    id?: any;

    /**
     * 州/省名称
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    name?: any;

    /**
     * 州/省代码
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    code?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    create_date?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    write_date?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    display_name?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    country_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    create_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    write_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    create_uid?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_country_state
     */
    country_id?: any;
}