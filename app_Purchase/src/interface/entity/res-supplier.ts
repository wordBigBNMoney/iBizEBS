/**
 * 供应商
 *
 * @export
 * @interface Res_supplier
 */
export interface Res_supplier {

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    id?: any;

    /**
     * 条码
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    barcode?: any;

    /**
     * 电话
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    phone?: any;

    /**
     * 公司类别
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    company_type?: any;

    /**
     * 内部参考
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    ref?: any;

    /**
     * EMail
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    email?: any;

    /**
     * 地址类型
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    type?: any;

    /**
     * 工作岗位
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    function?: any;

    /**
     * 标签
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    category_id?: any;

    /**
     * 手机
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    mobile?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    name?: any;

    /**
     * 税号
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    vat?: any;

    /**
     * 供应商货币
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_purchase_currency_name?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    company_id_text?: any;

    /**
     * 分包商位置
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_stock_subcontractor_name?: any;

    /**
     * 客户位置
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_stock_customer_name?: any;

    /**
     * 供应商位置
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_stock_supplier_name?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    parent_name?: any;

    /**
     * 付款条款
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_payment_term_name?: any;

    /**
     * 交货方法
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_delivery_carrier_name?: any;

    /**
     * 价格表
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_product_pricelist_name?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    country_id_text?: any;

    /**
     * 税科目调整
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_account_position_name?: any;

    /**
     * 州/省
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    state_id_text?: any;

    /**
     * 称谓
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    title_text?: any;

    /**
     * 采购付款条例
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_supplier_payment_term_name?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    user_name?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    state_id?: any;

    /**
     * 供应商货币
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_purchase_currency_id?: any;

    /**
     * 采购付款条例
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_supplier_payment_term_id?: any;

    /**
     * 销售付款条款
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_payment_term_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    country_id?: any;

    /**
     * 供应商位置
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_stock_supplier?: any;

    /**
     * 交货方法
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_delivery_carrier_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    title?: any;

    /**
     * 税科目调整
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_account_position_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    parent_id?: any;

    /**
     * 客户位置
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_stock_customer?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    user_id?: any;

    /**
     * 分包商位置
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_stock_subcontractor?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    company_id?: any;

    /**
     * 价格表
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    property_product_pricelist?: any;

    /**
     * 街道
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    street?: any;

    /**
     * 网站网址
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    website_url?: any;

    /**
     * 城市
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    city?: any;

    /**
     * 街道 2
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    street2?: any;

    /**
     * 邮政编码
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    zip?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_supplier
     */
    is_company?: any;
}