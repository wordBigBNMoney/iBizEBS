/**
 * 业务伙伴称谓
 *
 * @export
 * @interface Res_partner_title
 */
export interface Res_partner_title {

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    display_name?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    write_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    __last_update?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    create_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    id?: any;

    /**
     * 简称
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    shortcut?: any;

    /**
     * 称谓
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    name?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    create_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    create_uid?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_partner_title
     */
    write_uid?: any;
}