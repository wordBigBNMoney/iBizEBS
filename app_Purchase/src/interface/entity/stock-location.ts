/**
 * 库存位置
 *
 * @export
 * @interface Stock_location
 */
export interface Stock_location {

    /**
     * 通道(X)
     *
     * @returns {*}
     * @memberof Stock_location
     */
    posx?: any;

    /**
     * 货架(Y)
     *
     * @returns {*}
     * @memberof Stock_location
     */
    posy?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Stock_location
     */
    active?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Stock_location
     */
    create_date?: any;

    /**
     * 条码
     *
     * @returns {*}
     * @memberof Stock_location
     */
    barcode?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Stock_location
     */
    write_date?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Stock_location
     */
    display_name?: any;

    /**
     * 高度(Z)
     *
     * @returns {*}
     * @memberof Stock_location
     */
    posz?: any;

    /**
     * 完整的位置名称
     *
     * @returns {*}
     * @memberof Stock_location
     */
    complete_name?: any;

    /**
     * 是一个报废位置？
     *
     * @returns {*}
     * @memberof Stock_location
     */
    scrap_location?: any;

    /**
     * 是一个退回位置？
     *
     * @returns {*}
     * @memberof Stock_location
     */
    return_location?: any;

    /**
     * 位置名称
     *
     * @returns {*}
     * @memberof Stock_location
     */
    name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Stock_location
     */
    __last_update?: any;

    /**
     * 父级路径
     *
     * @returns {*}
     * @memberof Stock_location
     */
    parent_path?: any;

    /**
     * 即时库存
     *
     * @returns {*}
     * @memberof Stock_location
     */
    quant_ids?: any;

    /**
     * 位置类型
     *
     * @returns {*}
     * @memberof Stock_location
     */
    usage?: any;

    /**
     * 额外的信息
     *
     * @returns {*}
     * @memberof Stock_location
     */
    comment?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Stock_location
     */
    id?: any;

    /**
     * 包含
     *
     * @returns {*}
     * @memberof Stock_location
     */
    child_ids?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Stock_location
     */
    company_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Stock_location
     */
    create_uid_text?: any;

    /**
     * 库存计价科目（出向）
     *
     * @returns {*}
     * @memberof Stock_location
     */
    valuation_out_account_id_text?: any;

    /**
     * 下架策略
     *
     * @returns {*}
     * @memberof Stock_location
     */
    removal_strategy_id_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Stock_location
     */
    write_uid_text?: any;

    /**
     * 库存计价科目（入向）
     *
     * @returns {*}
     * @memberof Stock_location
     */
    valuation_in_account_id_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Stock_location
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Stock_location
     */
    create_uid?: any;

    /**
     * 库存计价科目（入向）
     *
     * @returns {*}
     * @memberof Stock_location
     */
    valuation_in_account_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Stock_location
     */
    company_id?: any;

    /**
     * 库存计价科目（出向）
     *
     * @returns {*}
     * @memberof Stock_location
     */
    valuation_out_account_id?: any;

    /**
     * 下架策略
     *
     * @returns {*}
     * @memberof Stock_location
     */
    removal_strategy_id?: any;
}