/**
 * 文档关注者
 *
 * @export
 * @interface Mail_followers
 */
export interface Mail_followers {

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    display_name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    __last_update?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    id?: any;

    /**
     * 相关文档编号
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    res_id?: any;

    /**
     * 子类型
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    subtype_ids?: any;

    /**
     * 相关的文档模型名称
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    res_model?: any;

    /**
     * 相关的业务伙伴
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    partner_id_text?: any;

    /**
     * 监听器
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    channel_id_text?: any;

    /**
     * 监听器
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    channel_id?: any;

    /**
     * 相关的业务伙伴
     *
     * @returns {*}
     * @memberof Mail_followers
     */
    partner_id?: any;
}