/**
 * 活动类型
 *
 * @export
 * @interface Mail_activity_type
 */
export interface Mail_activity_type {

    /**
     * 模型已更改
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    res_model_change?: any;

    /**
     * 摘要
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    summary?: any;

    /**
     * 自动安排下一个活动
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    force_next?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    sequence?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    id?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    category?: any;

    /**
     * 图标
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    icon?: any;

    /**
     * 之后
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    delay_count?: any;

    /**
     * 预先活动
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    previous_type_ids?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    display_name?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    active?: any;

    /**
     * 延迟类型
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    delay_from?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    write_date?: any;

    /**
     * 邮件模板
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    mail_template_ids?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    create_date?: any;

    /**
     * 模型
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    res_model_id?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    __last_update?: any;

    /**
     * 排版类型
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    decoration_type?: any;

    /**
     * 推荐的下一活动
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    next_type_ids?: any;

    /**
     * 延迟单位
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    delay_unit?: any;

    /**
     * 初始模型
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    initial_res_model_id?: any;

    /**
     * 设置默认下一个活动
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    default_next_type_id_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    create_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    create_uid?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    write_uid?: any;

    /**
     * 设置默认下一个活动
     *
     * @returns {*}
     * @memberof Mail_activity_type
     */
    default_next_type_id?: any;
}