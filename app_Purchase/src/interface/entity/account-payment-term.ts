/**
 * 付款条款
 *
 * @export
 * @interface Account_payment_term
 */
export interface Account_payment_term {

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    __last_update?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    display_name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    create_date?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    sequence?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    active?: any;

    /**
     * 付款条款
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    name?: any;

    /**
     * 发票描述
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    note?: any;

    /**
     * 条款
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    line_ids?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    id?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    write_date?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    create_uid_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    company_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    company_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_payment_term
     */
    create_uid?: any;
}