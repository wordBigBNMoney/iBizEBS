/**
 * 贸易条款
 *
 * @export
 * @interface Account_incoterms
 */
export interface Account_incoterms {

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    id?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    __last_update?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    active?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    create_date?: any;

    /**
     * 代码
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    code?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    display_name?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    create_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_incoterms
     */
    write_uid?: any;
}