import Res_countryAuthServiceBase from './res-country-auth-service-base';


/**
 * 国家/地区权限服务对象
 *
 * @export
 * @class Res_countryAuthService
 * @extends {Res_countryAuthServiceBase}
 */
export default class Res_countryAuthService extends Res_countryAuthServiceBase {

    /**
     * Creates an instance of  Res_countryAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_countryAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}