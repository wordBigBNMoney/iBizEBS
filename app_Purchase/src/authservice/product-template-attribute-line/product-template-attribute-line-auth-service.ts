import Product_template_attribute_lineAuthServiceBase from './product-template-attribute-line-auth-service-base';


/**
 * 产品模板属性明细行权限服务对象
 *
 * @export
 * @class Product_template_attribute_lineAuthService
 * @extends {Product_template_attribute_lineAuthServiceBase}
 */
export default class Product_template_attribute_lineAuthService extends Product_template_attribute_lineAuthServiceBase {

    /**
     * Creates an instance of  Product_template_attribute_lineAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_template_attribute_lineAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}