import Account_incotermsAuthServiceBase from './account-incoterms-auth-service-base';


/**
 * 贸易条款权限服务对象
 *
 * @export
 * @class Account_incotermsAuthService
 * @extends {Account_incotermsAuthServiceBase}
 */
export default class Account_incotermsAuthService extends Account_incotermsAuthServiceBase {

    /**
     * Creates an instance of  Account_incotermsAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_incotermsAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}