import Stock_picking_typeAuthServiceBase from './stock-picking-type-auth-service-base';


/**
 * 拣货类型权限服务对象
 *
 * @export
 * @class Stock_picking_typeAuthService
 * @extends {Stock_picking_typeAuthServiceBase}
 */
export default class Stock_picking_typeAuthService extends Stock_picking_typeAuthServiceBase {

    /**
     * Creates an instance of  Stock_picking_typeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Stock_picking_typeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}