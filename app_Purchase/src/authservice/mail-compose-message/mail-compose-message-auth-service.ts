import Mail_compose_messageAuthServiceBase from './mail-compose-message-auth-service-base';


/**
 * 邮件撰写向导权限服务对象
 *
 * @export
 * @class Mail_compose_messageAuthService
 * @extends {Mail_compose_messageAuthServiceBase}
 */
export default class Mail_compose_messageAuthService extends Mail_compose_messageAuthServiceBase {

    /**
     * Creates an instance of  Mail_compose_messageAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_compose_messageAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}