import Res_partnerAuthServiceBase from './res-partner-auth-service-base';


/**
 * 联系人权限服务对象
 *
 * @export
 * @class Res_partnerAuthService
 * @extends {Res_partnerAuthServiceBase}
 */
export default class Res_partnerAuthService extends Res_partnerAuthServiceBase {

    /**
     * Creates an instance of  Res_partnerAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partnerAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}