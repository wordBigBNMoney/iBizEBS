import Account_fiscal_positionAuthServiceBase from './account-fiscal-position-auth-service-base';


/**
 * 税科目调整权限服务对象
 *
 * @export
 * @class Account_fiscal_positionAuthService
 * @extends {Account_fiscal_positionAuthServiceBase}
 */
export default class Account_fiscal_positionAuthService extends Account_fiscal_positionAuthServiceBase {

    /**
     * Creates an instance of  Account_fiscal_positionAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_fiscal_positionAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}