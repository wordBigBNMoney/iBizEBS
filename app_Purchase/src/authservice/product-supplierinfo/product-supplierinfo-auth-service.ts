import Product_supplierinfoAuthServiceBase from './product-supplierinfo-auth-service-base';


/**
 * 供应商价格表权限服务对象
 *
 * @export
 * @class Product_supplierinfoAuthService
 * @extends {Product_supplierinfoAuthServiceBase}
 */
export default class Product_supplierinfoAuthService extends Product_supplierinfoAuthServiceBase {

    /**
     * Creates an instance of  Product_supplierinfoAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_supplierinfoAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}