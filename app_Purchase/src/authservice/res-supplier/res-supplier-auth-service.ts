import Res_supplierAuthServiceBase from './res-supplier-auth-service-base';


/**
 * 供应商权限服务对象
 *
 * @export
 * @class Res_supplierAuthService
 * @extends {Res_supplierAuthServiceBase}
 */
export default class Res_supplierAuthService extends Res_supplierAuthServiceBase {

    /**
     * Creates an instance of  Res_supplierAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_supplierAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}