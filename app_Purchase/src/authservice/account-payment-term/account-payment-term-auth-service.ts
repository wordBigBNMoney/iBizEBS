import Account_payment_termAuthServiceBase from './account-payment-term-auth-service-base';


/**
 * 付款条款权限服务对象
 *
 * @export
 * @class Account_payment_termAuthService
 * @extends {Account_payment_termAuthServiceBase}
 */
export default class Account_payment_termAuthService extends Account_payment_termAuthServiceBase {

    /**
     * Creates an instance of  Account_payment_termAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_payment_termAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}