/**
 * Line 部件模型
 *
 * @export
 * @class LineModel
 */
export default class LineModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof LineGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof LineGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'product_name',
          prop: 'product_name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'product_qty',
          prop: 'product_qty',
          dataType: 'FLOAT',
          isEditable:true
        },
        {
          name: 'qty_ordered',
          prop: 'qty_ordered',
          dataType: 'INT',
          isEditable:true
        },
        {
          name: 'product_uom_name',
          prop: 'product_uom_name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'schedule_date',
          prop: 'schedule_date',
          dataType: 'DATE',
          isEditable:true
        },
        {
          name: 'price_unit',
          prop: 'price_unit',
          dataType: 'FLOAT',
          isEditable:true
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'product_uom_id',
          prop: 'product_uom_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_id',
          prop: 'product_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'account_analytic_id',
          prop: 'account_analytic_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'move_dest_id',
          prop: 'move_dest_id',
          dataType: 'PICKUP',
        },
        {
          name: 'requisition_id',
          prop: 'requisition_id',
          dataType: 'PICKUP',
        },
        {
          name: 'purchase_requisition_line',
          prop: 'id',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}