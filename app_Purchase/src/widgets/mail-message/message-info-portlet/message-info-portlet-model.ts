/**
 * MessageInfo 部件模型
 *
 * @export
 * @class MessageInfoModel
 */
export default class MessageInfoModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MessageInfoModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'needaction',
      },
      {
        name: 'model',
      },
      {
        name: 'no_auto_thread',
      },
      {
        name: 'partner_ids',
      },
      {
        name: 'create_date',
      },
      {
        name: 'record_name',
      },
      {
        name: 'message_id',
      },
      {
        name: 'tracking_value_ids',
      },
      {
        name: 'moderation_status',
      },
      {
        name: 'notification_ids',
      },
      {
        name: 'email_from',
      },
      {
        name: 'channel_ids',
      },
      {
        name: 'mail_message',
        prop: 'id',
      },
      {
        name: 'description',
      },
      {
        name: 'starred',
      },
      {
        name: 'rating_ids',
      },
      {
        name: 'needaction_partner_ids',
      },
      {
        name: 'reply_to',
      },
      {
        name: 'mail_server_id',
      },
      {
        name: 'res_id',
      },
      {
        name: 'need_moderation',
      },
      {
        name: 'subject',
      },
      {
        name: 'body',
      },
      {
        name: 'display_name',
      },
      {
        name: 'website_published',
      },
      {
        name: 'attachment_ids',
      },
      {
        name: 'child_ids',
      },
      {
        name: 'rating_value',
      },
      {
        name: 'add_sign',
      },
      {
        name: 'starred_partner_ids',
      },
      {
        name: '__last_update',
      },
      {
        name: 'write_date',
      },
      {
        name: 'date',
      },
      {
        name: 'has_error',
      },
      {
        name: 'message_type',
      },
      {
        name: 'author_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'mail_activity_type_id_text',
      },
      {
        name: 'moderator_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'subtype_id_text',
      },
      {
        name: 'author_avatar',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'mail_activity_type_id',
      },
      {
        name: 'moderator_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'author_id',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'subtype_id',
      },
    ]
  }


}
