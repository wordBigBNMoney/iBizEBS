import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import SendService from './send-form-service';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {SendEditFormBase}
 */
export class SendEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SendEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {SendService}
     * @memberof SendEditFormBase
     */
    public service: SendService = new SendService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof SendEditFormBase
     */
    public appEntityService: Mail_messageService = new Mail_messageService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SendEditFormBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SendEditFormBase
     */
    protected appDeLogicName: string = '消息';

    /**
     * 界面UI服务对象
     *
     * @type {Mail_messageUIService}
     * @memberof SendBase
     */  
    public appUIService:Mail_messageUIService = new Mail_messageUIService(this.$store);

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public form_button1_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Mail_messageUIService  = new Mail_messageUIService();
        curUIService.Mail_message_Send(datas,contextJO, paramJO,  $event, xData,this,"Mail_message");
    }

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof SendEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        create_uid_text: null,
        body: null,
        attachment_ids: null,
        subtype_id: null,
        model: null,
        res_id: null,
        id: null,
        mail_message:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof SendEditFormBase
     */
    public majorMessageField: string = "";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SendEditFormBase
     */
    public rules():any{
        return {
        body: [
            { required: this.detailsModel.body.required, type: 'string', message: '内容 值不能为空', trigger: 'change' },
            { required: this.detailsModel.body.required, type: 'string', message: '内容 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof SendBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof SendEditFormBase
     */
    public detailsModel: any = {
        button1: new FormButtonModel({ caption: '发送', detailType: 'BUTTON', name: 'button1', visible: true, isShowCaption: true, form: this, showMoreMode: 0,disabled: false, uiaction: { type: 'DEUIACTION', 
 tag: 'Send',actiontarget: 'SINGLEKEY',noprivdisplaymode:2,visabled: true,disabled: false} }),

        group1: new FormGroupPanelModel({ caption: '消息基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.mail_message.send_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        create_uid_text: new FormItemModel({ caption: '创建人', detailType: 'FORMITEM', name: 'create_uid_text', visible: true, isShowCaption: false, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        body: new FormItemModel({ caption: '内容', detailType: 'FORMITEM', name: 'body', visible: true, isShowCaption: false, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        attachment_ids: new FormItemModel({ caption: '附件', detailType: 'FORMITEM', name: 'attachment_ids', visible: true, isShowCaption: false, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        subtype_id: new FormItemModel({ caption: '子类型', detailType: 'FORMITEM', name: 'subtype_id', visible: true, isShowCaption: false, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        model: new FormItemModel({ caption: '相关的文档模型', detailType: 'FORMITEM', name: 'model', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        res_id: new FormItemModel({ caption: '相关文档编号', detailType: 'FORMITEM', name: 'res_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

    };

	/**
	 * 表单 发送 事件
	 *
	 * @memberof @memberof SendEditFormBase
	 */
    public button1_click($event: any): void {
        this.form_button1_click(null, null, $event);

    }

    /**
     * 新建默认值
     * @memberof SendEditFormBase
     */
    public createDefault(){                    
        if (this.data.hasOwnProperty('create_uid_text')) {
            this.data['create_uid_text'] = '至：关注者“”';
        }
        if (this.data.hasOwnProperty('subtype_id')) {
            this.data['subtype_id'] = 1;
        }
        if (this.data.hasOwnProperty('model')) {
            this.data['model'] = this.viewparams['n_model_eq'];
        }
        if (this.data.hasOwnProperty('res_id')) {
            this.data['res_id'] = this.viewparams['n_res_id_eq'];
        }
    }
}