/**
 * IF_Master 部件模型
 *
 * @export
 * @class IF_MasterModel
 */
export default class IF_MasterModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof IF_MasterModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'partner_id_text',
        prop: 'partner_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'partner_ref',
        prop: 'partner_ref',
        dataType: 'TEXT',
      },
      {
        name: 'requisition_id_text',
        prop: 'requisition_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'currency_id_text',
        prop: 'currency_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'date_order',
        prop: 'date_order',
        dataType: 'DATETIME',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'date_planned',
        prop: 'date_planned',
        dataType: 'DATETIME',
      },
      {
        name: 'picking_type_id_text',
        prop: 'picking_type_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'incoterm_id_text',
        prop: 'incoterm_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'user_id_text',
        prop: 'user_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'payment_term_id_text',
        prop: 'payment_term_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'fiscal_position_id_text',
        prop: 'fiscal_position_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'purchase_order',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}