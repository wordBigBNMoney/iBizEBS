import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import Purchase_orderService from '@/service/purchase-order/purchase-order-service';
import Master_OrderService from './master-order-grid-service';
import Purchase_orderUIService from '@/uiservice/purchase-order/purchase-order-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {Master_OrderGridBase}
 */
export class Master_OrderGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Master_OrderGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {Master_OrderService}
     * @memberof Master_OrderGridBase
     */
    public service: Master_OrderService = new Master_OrderService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_orderService}
     * @memberof Master_OrderGridBase
     */
    public appEntityService: Purchase_orderService = new Purchase_orderService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Master_OrderGridBase
     */
    protected appDeName: string = 'purchase_order';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Master_OrderGridBase
     */
    protected appDeLogicName: string = '采购订单';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_orderUIService}
     * @memberof Master_OrderBase
     */  
    public appUIService:Purchase_orderUIService = new Purchase_orderUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof Master_OrderBase
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof Master_OrderBase
     */  
    public majorInfoColName:string = "name";


    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof Master_OrderBase
     */
    protected localStorageTag: string = 'purchase_order_master_order_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof Master_OrderGridBase
     */
    public allColumns: any[] = [
        {
            name: 'name',
            label: '订单关联',
            langtag: 'entities.purchase_order.master_order_grid.columns.name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'date_approve',
            label: '审批日期',
            langtag: 'entities.purchase_order.master_order_grid.columns.date_approve',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'partner_id_text',
            label: '供应商',
            langtag: 'entities.purchase_order.master_order_grid.columns.partner_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'company_id_text',
            label: '公司',
            langtag: 'entities.purchase_order.master_order_grid.columns.company_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'date_planned',
            label: '计划日期',
            langtag: 'entities.purchase_order.master_order_grid.columns.date_planned',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'user_id_text',
            label: '采购员',
            langtag: 'entities.purchase_order.master_order_grid.columns.user_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'origin',
            label: '源文档',
            langtag: 'entities.purchase_order.master_order_grid.columns.origin',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'amount_total',
            label: '总计',
            langtag: 'entities.purchase_order.master_order_grid.columns.amount_total',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'invoice_status',
            label: '账单状态',
            langtag: 'entities.purchase_order.master_order_grid.columns.invoice_status',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof Master_OrderGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Master_OrderGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Master_OrderBase
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof Master_OrderBase
     */
    public hasRowEdit: any = {
        'name':false,
        'date_approve':false,
        'partner_id_text':false,
        'company_id_text':false,
        'date_planned':false,
        'user_id_text':false,
        'origin':false,
        'amount_total':false,
        'invoice_status':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof Master_OrderBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof Master_OrderGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'invoice_status',
                srfkey: 'PURCHASE_ORDER__INVOICE_STATUS',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof Master_OrderBase
     */
    public updateDefault(row: any){                    
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof Master_OrderBase
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.id === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}