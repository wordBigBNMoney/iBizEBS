/**
 * MasterSummary 部件模型
 *
 * @export
 * @class MasterSummaryModel
 */
export default class MasterSummaryModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterSummaryModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'is_shipped',
      },
      {
        name: 'write_date',
      },
      {
        name: 'access_token',
      },
      {
        name: 'activity_user_id',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'order_line',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'notes',
      },
      {
        name: 'amount_total',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'amount_tax',
      },
      {
        name: 'access_url',
      },
      {
        name: 'invoice_count',
      },
      {
        name: 'amount_untaxed',
      },
      {
        name: 'picking_ids',
      },
      {
        name: 'create_date',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'access_warning',
      },
      {
        name: '__last_update',
      },
      {
        name: 'state',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'picking_count',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'invoice_ids',
      },
      {
        name: 'purchase_order',
        prop: 'id',
      },
      {
        name: 'product_id',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'date_order',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'default_location_dest_id_usage',
      },
      {
        name: 'name',
      },
      {
        name: 'group_id',
      },
      {
        name: 'display_name',
      },
      {
        name: 'invoice_status',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'date_approve',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'partner_ref',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'date_planned',
      },
      {
        name: 'origin',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'fiscal_position_id_text',
      },
      {
        name: 'currency_id_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'dest_address_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'requisition_id_text',
      },
      {
        name: 'payment_term_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'partner_id_text',
      },
      {
        name: 'picking_type_id_text',
      },
      {
        name: 'incoterm_id_text',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'partner_id',
      },
      {
        name: 'dest_address_id',
      },
      {
        name: 'company_id',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'requisition_id',
      },
      {
        name: 'payment_term_id',
      },
      {
        name: 'user_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'incoterm_id',
      },
      {
        name: 'fiscal_position_id',
      },
      {
        name: 'picking_type_id',
      },
    ]
  }


}