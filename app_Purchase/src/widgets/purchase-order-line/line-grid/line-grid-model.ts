/**
 * Line 部件模型
 *
 * @export
 * @class LineModel
 */
export default class LineModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof LineGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof LineGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'product_id_text',
          prop: 'product_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'name',
          prop: 'name',
          dataType: 'LONGTEXT_1000',
          isEditable:true
        },
        {
          name: 'product_qty',
          prop: 'product_qty',
          dataType: 'FLOAT',
          isEditable:true
        },
        {
          name: 'product_uom_text',
          prop: 'product_uom_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'price_unit',
          prop: 'price_unit',
          dataType: 'FLOAT',
          isEditable:true
        },
        {
          name: 'taxes_id',
          prop: 'taxes_id',
          dataType: 'ONE2MANYDATA',
          isEditable:true
        },
        {
          name: 'price_subtotal',
          prop: 'price_subtotal',
          dataType: 'DECIMAL',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'sale_line_id',
          prop: 'sale_line_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_id',
          prop: 'product_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'product_uom',
          prop: 'product_uom',
          dataType: 'PICKUP',
        },
        {
          name: 'order_id',
          prop: 'order_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'id',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'price_tax',
          prop: 'price_tax',
          dataType: 'FLOAT',
        },
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'account_analytic_id',
          prop: 'account_analytic_id',
          dataType: 'PICKUP',
        },
        {
          name: 'sale_order_id',
          prop: 'sale_order_id',
          dataType: 'PICKUP',
        },
        {
          name: 'orderpoint_id',
          prop: 'orderpoint_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_uom_category_id',
          prop: 'product_uom_category_id',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'price_total',
          prop: 'price_total',
          dataType: 'DECIMAL',
        },
        {
          name: 'purchase_order_line',
          prop: 'id',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}