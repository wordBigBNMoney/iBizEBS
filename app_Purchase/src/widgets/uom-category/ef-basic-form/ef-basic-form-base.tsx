import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import EF_BasicService from './ef-basic-form-service';
import Uom_categoryUIService from '@/uiservice/uom-category/uom-category-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_BasicEditFormBase}
 */
export class EF_BasicEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_BasicService}
     * @memberof EF_BasicEditFormBase
     */
    public service: EF_BasicService = new EF_BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Uom_categoryService}
     * @memberof EF_BasicEditFormBase
     */
    public appEntityService: Uom_categoryService = new Uom_categoryService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected appDeName: string = 'uom_category';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected appDeLogicName: string = '产品计量单位 类别';

    /**
     * 界面UI服务对象
     *
     * @type {Uom_categoryUIService}
     * @memberof EF_BasicBase
     */  
    public appUIService:Uom_categoryUIService = new Uom_categoryUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        measure_type: null,
        is_pos_groupable: null,
        id: null,
        uom_category:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public rules():any{
        return {
        name: [
            { required: this.detailsModel.name.required, type: 'string', message: '计量单位类别 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name.required, type: 'string', message: '计量单位类别 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public detailsModel: any = {
        grouppanel1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.uom_category.ef_basic_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '计量单位类别', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '计量单位类别', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        measure_type: new FormItemModel({ caption: '计量类型', detailType: 'FORMITEM', name: 'measure_type', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        is_pos_groupable: new FormItemModel({ caption: '分组销售点中的产品', detailType: 'FORMITEM', name: 'is_pos_groupable', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

    };
}