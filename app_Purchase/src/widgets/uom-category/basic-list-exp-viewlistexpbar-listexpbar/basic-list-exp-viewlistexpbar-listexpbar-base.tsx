
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, ListViewExpBarControlBase } from '@/studio-core';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import BasicListExpViewlistexpbarService from './basic-list-exp-viewlistexpbar-listexpbar-service';
import Uom_categoryUIService from '@/uiservice/uom-category/uom-category-ui-service';
import CodeListService from "@service/app/codelist-service";


/**
 * listexpbar部件基类
 *
 * @export
 * @class ListViewExpBarControlBase
 * @extends {BasicListExpViewlistexpbarListexpbarBase}
 */
export class BasicListExpViewlistexpbarListexpbarBase extends ListViewExpBarControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof BasicListExpViewlistexpbarListexpbarBase
     */
    protected controlType: string = 'LISTEXPBAR';

    /**
     * 建构部件服务对象
     *
     * @type {BasicListExpViewlistexpbarService}
     * @memberof BasicListExpViewlistexpbarListexpbarBase
     */
    public service: BasicListExpViewlistexpbarService = new BasicListExpViewlistexpbarService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Uom_categoryService}
     * @memberof BasicListExpViewlistexpbarListexpbarBase
     */
    public appEntityService: Uom_categoryService = new Uom_categoryService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListExpViewlistexpbarListexpbarBase
     */
    protected appDeName: string = 'uom_category';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListExpViewlistexpbarListexpbarBase
     */
    protected appDeLogicName: string = '产品计量单位 类别';

    /**
     * 界面UI服务对象
     *
     * @type {Uom_categoryUIService}
     * @memberof BasicListExpViewlistexpbarBase
     */  
    public appUIService:Uom_categoryUIService = new Uom_categoryUIService(this.$store);

    /**
     * listexpbar_list 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof BasicListExpViewlistexpbarListexpbarBase
     */
    public listexpbar_list_selectionchange($event: any, $event2?: any) {
        this.listexpbar_selectionchange($event, 'listexpbar_list', $event2);
    }

    /**
     * listexpbar_list 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof BasicListExpViewlistexpbarListexpbarBase
     */
    public listexpbar_list_load($event: any, $event2?: any) {
        this.listexpbar_load($event, 'listexpbar_list', $event2);
    }

    /**
     * listexpbar_toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof BasicListExpViewlistexpbarListexpbarBase
     */
    public listexpbar_toolbar_click($event: any, $event2?: any) {
        if (Object.is($event.tag, 'tbitem3')) {
            this.listexpbar_toolbar_tbitem3_click(null, 'listexpbar_toolbar', $event2);
        }
        if (Object.is($event.tag, 'tbitem8')) {
            this.listexpbar_toolbar_tbitem8_click(null, 'listexpbar_toolbar', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public listexpbar_toolbar_tbitem3_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.listexpbar_list;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.New(datas, contextJO,paramJO,  $event, xData,this,"Uom_category");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public listexpbar_toolbar_tbitem8_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.listexpbar_list;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Remove(datas, contextJO,paramJO,  $event, xData,this,"Uom_category");
    }

    /**
     * 新建
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Uom_categoryBasicListExpViewBase
     */
    public New(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
         const _this: any = this;
        if (_this.newdata && _this.newdata instanceof Function) {
            const data: any = {};
            _this.newdata([{ ...data }],[{ ...data }], params, $event, xData);
        } else {
            _this.$Notice.error({ title: '错误', desc: 'newdata 视图处理逻辑不存在，请添加!' });
        }
    }

    /**
     * 删除
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Uom_categoryBasicListExpViewBase
     */
    public Remove(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        const _this: any = this;
        if (!xData || !(xData.remove instanceof Function)) {
            return ;
        }
        xData.remove(args);
    }



        /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Uom_categoryBasicListExpView
     */
    public basiclistexpviewlistexpbar_toolbarModels: any = {
        tbitem3: { name: 'tbitem3', caption: '新建', 'isShowCaption': true, 'isShowIcon': true, tooltip: '新建', iconcls: 'fa fa-file-text-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'New', target: '', class: '' } },

        tbitem7: {  name: 'tbitem7', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem8: { name: 'tbitem8', caption: '删除', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'MULTIKEY', class: '' } },

    };


    /**
     * 导航视图名称
     *
     * @type {string}
     * @memberof BasicListExpViewlistexpbarBase
     */
    public navViewName: string = 'uom-category-basic-edit-view';

    /**
     * 计算导航工具栏按钮状态
     *
     * @param {boolean} state
     * @param {*} models
     * @memberof BasicListExpViewlistexpbarBase
     */
    public calcToolbarItemState(state: boolean, models?: any) {
        const curUIService:any  = new Uom_categoryUIService();
        super.calcToolbarItemState(state, this.basiclistexpviewlistexpbar_toolbarModels,curUIService);
    }

    /**
    * 刷新
    *
    * @memberof BasicListExpViewlistexpbarBase
    */
    public refresh(args?: any): void {
        const refs: any = this.$refs;
        if (refs && refs.listexpbar_list) {
            refs.listexpbar_list.refresh();
        }
    }


    /**
     * 搜索字段名称
     * 
     * @type {(string)}
     * @memberof BasicListExpViewlistexpbarBase
     */
    public placeholder="计量单位类别";

    /**
     * 呈现模式，可选值：horizontal或者vertical
     * 
     * @public
     * @type {(string)}
     * @memberof BasicListExpViewlistexpbarBase
     */
    public showMode:string ="horizontal";

    /**
     * 控件高度
     *
     * @type {number}
     * @memberof BasicListExpViewlistexpbarBase
     */
    public ctrlHeight: number = 0;
    
    /**
     * listexpbar的选中数据事件
     * 
     * @memberof BasicListExpViewlistexpbarBase
     */
    public listexpbar_selectionchange(args: any [], tag?: string, $event2?: any): void {
        let tempContext:any = {};
        let tempViewParam:any = {};
        if (args.length === 0) {
            this.selection = {};
            this.calcToolbarItemState(true);
            return ;
        }
        const arg:any = args[0];
        if(this.context){
            Object.assign(tempContext,JSON.parse(JSON.stringify(this.context)));
        }
        Object.assign(tempContext,{'uom_category':arg['uom_category']});
        Object.assign(tempContext,{srfparentdename:'Uom_category',srfparentkey:arg['uom_category']});
        if(this.navFilter && !Object.is(this.navFilter,"")){
            Object.assign(tempViewParam,{[this.navFilter]:arg['uom_category']});
        }
        if(this.navPSDer && !Object.is(this.navPSDer,"")){
            Object.assign(tempViewParam,{[this.navPSDer]:arg['uom_category']});
        }
        if(this.navigateContext && Object.keys(this.navigateContext).length >0){
            let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navigateContext);
            Object.assign(tempContext,_context);
        }
        if(this.navigateParams && Object.keys(this.navigateParams).length >0){
            let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navigateParams);
            Object.assign(tempViewParam,_params);
        }
        this.selection = {};
        Object.assign(this.selection, { view: { viewname: this.navViewName },context:tempContext,viewparam:tempViewParam});
        this.calcToolbarItemState(false);
        this.$forceUpdate();
    }

    /**
     * listexpbar的load完成事件
     * 
     * @memberof BasicListExpViewlistexpbarBase
     */
    public listexpbar_load(args:any, tag?: string, $event2?: any){
        this.$emit('load',args);
    }

    /**
    * 执行搜索
    *
    * @memberof BasicListExpViewlistexpbarBase
    */
    public onSearch($event:any) {
        this.viewState.next({ tag: 'listexpbar_list', action: "load", data: {query : this.searchText}});
    }

}