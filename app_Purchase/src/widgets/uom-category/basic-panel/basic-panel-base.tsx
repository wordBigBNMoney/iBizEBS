import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import BasicService from './basic-panel-service';
import Uom_categoryUIService from '@/uiservice/uom-category/uom-category-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import BasicModel from './basic-panel-model';
import CodeListService from "@service/app/codelist-service";


/**
 * listexpbar_list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {BasicPanelBase}
 */
export class BasicPanelBase extends PanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof BasicPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {BasicService}
     * @memberof BasicPanelBase
     */
    public service: BasicService = new BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Uom_categoryService}
     * @memberof BasicPanelBase
     */
    public appEntityService: Uom_categoryService = new Uom_categoryService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof BasicPanelBase
     */
    protected appDeName: string = 'uom_category';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof BasicPanelBase
     */
    protected appDeLogicName: string = '产品计量单位 类别';

    /**
     * 界面UI服务对象
     *
     * @type {Uom_categoryUIService}
     * @memberof BasicBase
     */  
    public appUIService:Uom_categoryUIService = new Uom_categoryUIService(this.$store);


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Basic
     */
    public detailsModel: any = {
        name: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'name', panel: this })
,
        measure_type: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'measure_type', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof Basic
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                


    }

    /**
     * 数据模型对象
     *
     * @type {BasicModel}
     * @memberof Basic
     */
    public dataModel: BasicModel = new BasicModel();

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof Basic
     */
    public uiAction(row: any, tag: any, $event: any) {
    }
}