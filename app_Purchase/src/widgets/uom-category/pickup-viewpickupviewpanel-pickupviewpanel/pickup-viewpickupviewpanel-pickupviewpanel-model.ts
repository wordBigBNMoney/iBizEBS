/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'name',
      },
      {
        name: 'create_date',
      },
      {
        name: '__last_update',
      },
      {
        name: 'write_date',
      },
      {
        name: 'is_pos_groupable',
      },
      {
        name: 'display_name',
      },
      {
        name: 'uom_category',
        prop: 'id',
      },
      {
        name: 'measure_type',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'create_uid',
      },
    ]
  }


}