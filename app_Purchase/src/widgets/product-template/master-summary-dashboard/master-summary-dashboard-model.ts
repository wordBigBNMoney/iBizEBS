/**
 * MasterSummary 部件模型
 *
 * @export
 * @class MasterSummaryModel
 */
export default class MasterSummaryModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterSummaryModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'activity_ids',
      },
      {
        name: 'website_style_ids',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'website_url',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: '__last_update',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'purchase_ok',
      },
      {
        name: 'nbr_reordering_rules',
      },
      {
        name: 'warehouse_id',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'packaging_ids',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'rating_last_feedback',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'valid_product_attribute_wnva_ids',
      },
      {
        name: 'available_threshold',
      },
      {
        name: 'purchase_line_warn',
      },
      {
        name: 'seller_ids',
      },
      {
        name: 'mrp_product_qty',
      },
      {
        name: 'product_variant_id',
      },
      {
        name: 'name',
      },
      {
        name: 'rating_count',
      },
      {
        name: 'property_cost_method',
      },
      {
        name: 'service_to_purchase',
      },
      {
        name: 'property_stock_account_input',
      },
      {
        name: 'purchase_line_warn_msg',
      },
      {
        name: 'website_price_difference',
      },
      {
        name: 'website_meta_title',
      },
      {
        name: 'description_pickingin',
      },
      {
        name: 'display_name',
      },
      {
        name: 'pos_categ_id',
      },
      {
        name: 'optional_product_ids',
      },
      {
        name: 'weight_uom_id',
      },
      {
        name: 'hide_expense_policy',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'image',
      },
      {
        name: 'invoice_policy',
      },
      {
        name: 'website_price',
      },
      {
        name: 'type',
      },
      {
        name: 'valid_product_attribute_value_ids',
      },
      {
        name: 'valid_archived_variant_ids',
      },
      {
        name: 'expense_policy',
      },
      {
        name: 'is_seo_optimized',
      },
      {
        name: 'rating_last_value',
      },
      {
        name: 'product_template',
        prop: 'id',
      },
      {
        name: 'property_account_income_id',
      },
      {
        name: 'alternative_product_ids',
      },
      {
        name: 'valid_existing_variant_ids',
      },
      {
        name: 'product_variant_count',
      },
      {
        name: 'purchased_product_qty',
      },
      {
        name: 'valid_product_template_attribute_line_ids',
      },
      {
        name: 'description_picking',
      },
      {
        name: 'event_ok',
      },
      {
        name: 'standard_price',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'bom_line_ids',
      },
      {
        name: 'create_date',
      },
      {
        name: 'rating_last_image',
      },
      {
        name: 'qty_available',
      },
      {
        name: 'cost_method',
      },
      {
        name: 'website_meta_og_img',
      },
      {
        name: 'website_id',
      },
      {
        name: 'website_meta_keywords',
      },
      {
        name: 'image_small',
      },
      {
        name: 'pricelist_id',
      },
      {
        name: 'website_size_x',
      },
      {
        name: 'price',
      },
      {
        name: 'rental',
      },
      {
        name: 'outgoing_qty',
      },
      {
        name: 'sequence',
      },
      {
        name: 'property_stock_account_output',
      },
      {
        name: 'route_ids',
      },
      {
        name: 'property_account_expense_id',
      },
      {
        name: 'sales_count',
      },
      {
        name: 'reordering_min_qty',
      },
      {
        name: 'to_weight',
      },
      {
        name: 'valid_product_attribute_value_wnva_ids',
      },
      {
        name: 'item_ids',
      },
      {
        name: 'supplier_taxes_id',
      },
      {
        name: 'volume',
      },
      {
        name: 'description',
      },
      {
        name: 'property_stock_production',
      },
      {
        name: 'activity_user_id',
      },
      {
        name: 'property_stock_inventory',
      },
      {
        name: 'active',
      },
      {
        name: 'sale_line_warn',
      },
      {
        name: 'list_price',
      },
      {
        name: 'public_categ_ids',
      },
      {
        name: 'valuation',
      },
      {
        name: 'description_pickingout',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'is_published',
      },
      {
        name: 'color',
      },
      {
        name: 'accessory_product_ids',
      },
      {
        name: 'route_from_categ_ids',
      },
      {
        name: 'write_date',
      },
      {
        name: 'website_sequence',
      },
      {
        name: 'is_product_variant',
      },
      {
        name: 'location_id',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'incoming_qty',
      },
      {
        name: 'rating_ids',
      },
      {
        name: 'website_meta_description',
      },
      {
        name: 'bom_ids',
      },
      {
        name: 'sale_line_warn_msg',
      },
      {
        name: 'purchase_method',
      },
      {
        name: 'produce_delay',
      },
      {
        name: 'bom_count',
      },
      {
        name: 'taxes_id',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'can_be_expensed',
      },
      {
        name: 'sale_ok',
      },
      {
        name: 'service_type',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'tracking',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'valid_product_template_attribute_line_wnva_ids',
      },
      {
        name: 'property_account_creditor_price_difference',
      },
      {
        name: 'inventory_availability',
      },
      {
        name: 'website_size_y',
      },
      {
        name: 'image_medium',
      },
      {
        name: 'valid_product_attribute_ids',
      },
      {
        name: 'lst_price',
      },
      {
        name: 'custom_message',
      },
      {
        name: 'available_in_pos',
      },
      {
        name: 'weight_uom_name',
      },
      {
        name: 'cost_currency_id',
      },
      {
        name: 'attribute_line_ids',
      },
      {
        name: 'weight',
      },
      {
        name: 'virtual_available',
      },
      {
        name: 'product_image_ids',
      },
      {
        name: 'used_in_bom_count',
      },
      {
        name: 'default_code',
      },
      {
        name: 'barcode',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'property_valuation',
      },
      {
        name: 'website_description',
      },
      {
        name: 'product_variant_ids',
      },
      {
        name: 'website_published',
      },
      {
        name: 'website_public_price',
      },
      {
        name: 'reordering_max_qty',
      },
      {
        name: 'sale_delay',
      },
      {
        name: 'variant_seller_ids',
      },
      {
        name: 'description_purchase',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'description_sale',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'uom_po_id_text',
      },
      {
        name: 'uom_name',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'categ_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'company_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'categ_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'uom_id',
      },
      {
        name: 'uom_po_id',
      },
    ]
  }


}