import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Purchase_requisition_typeService from '@/service/purchase-requisition-type/purchase-requisition-type-service';
import BasicListExpViewlistexpbarModel from './basic-list-exp-viewlistexpbar-listexpbar-model';


/**
 * BasicListExpViewlistexpbar 部件服务对象
 *
 * @export
 * @class BasicListExpViewlistexpbarService
 */
export default class BasicListExpViewlistexpbarService extends ControlService {

    /**
     * 采购申请类型服务对象
     *
     * @type {Purchase_requisition_typeService}
     * @memberof BasicListExpViewlistexpbarService
     */
    public appEntityService: Purchase_requisition_typeService = new Purchase_requisition_typeService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof BasicListExpViewlistexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of BasicListExpViewlistexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof BasicListExpViewlistexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new BasicListExpViewlistexpbarModel();
    }

}