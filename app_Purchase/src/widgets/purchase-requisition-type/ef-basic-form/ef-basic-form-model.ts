/**
 * EF_Basic 部件模型
 *
 * @export
 * @class EF_BasicModel
 */
export default class EF_BasicModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_BasicModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'exclusive',
        prop: 'exclusive',
        dataType: 'SSCODELIST',
      },
      {
        name: 'line_copy',
        prop: 'line_copy',
        dataType: 'SSCODELIST',
      },
      {
        name: 'quantity_copy',
        prop: 'quantity_copy',
        dataType: 'SSCODELIST',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'purchase_requisition_type',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}