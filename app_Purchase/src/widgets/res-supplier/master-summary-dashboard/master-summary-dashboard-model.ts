/**
 * MasterSummary 部件模型
 *
 * @export
 * @class MasterSummaryModel
 */
export default class MasterSummaryModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterSummaryModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'res_supplier',
        prop: 'id',
      },
      {
        name: 'barcode',
      },
      {
        name: 'phone',
      },
      {
        name: 'company_type',
      },
      {
        name: 'ref',
      },
      {
        name: 'email',
      },
      {
        name: 'type',
      },
      {
        name: 'function',
      },
      {
        name: 'category_id',
      },
      {
        name: 'mobile',
      },
      {
        name: 'name',
      },
      {
        name: 'vat',
      },
      {
        name: 'property_purchase_currency_name',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'property_stock_subcontractor_name',
      },
      {
        name: 'property_stock_customer_name',
      },
      {
        name: 'property_stock_supplier_name',
      },
      {
        name: 'parent_name',
      },
      {
        name: 'property_payment_term_name',
      },
      {
        name: 'property_delivery_carrier_name',
      },
      {
        name: 'property_product_pricelist_name',
      },
      {
        name: 'country_id_text',
      },
      {
        name: 'property_account_position_name',
      },
      {
        name: 'state_id_text',
      },
      {
        name: 'title_text',
      },
      {
        name: 'property_supplier_payment_term_name',
      },
      {
        name: 'user_name',
      },
      {
        name: 'state_id',
      },
      {
        name: 'property_purchase_currency_id',
      },
      {
        name: 'property_supplier_payment_term_id',
      },
      {
        name: 'property_payment_term_id',
      },
      {
        name: 'country_id',
      },
      {
        name: 'property_stock_supplier',
      },
      {
        name: 'property_delivery_carrier_id',
      },
      {
        name: 'title',
      },
      {
        name: 'property_account_position_id',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'property_stock_customer',
      },
      {
        name: 'user_id',
      },
      {
        name: 'property_stock_subcontractor',
      },
      {
        name: 'company_id',
      },
      {
        name: 'property_product_pricelist',
      },
      {
        name: 'street',
      },
      {
        name: 'website_url',
      },
      {
        name: 'city',
      },
      {
        name: 'street2',
      },
      {
        name: 'zip',
      },
      {
        name: 'is_company',
      },
    ]
  }


}