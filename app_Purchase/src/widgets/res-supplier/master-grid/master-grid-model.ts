/**
 * Master 部件模型
 *
 * @export
 * @class MasterModel
 */
export default class MasterModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MasterGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MasterGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'phone',
          prop: 'phone',
          dataType: 'TEXT',
        },
        {
          name: 'mobile',
          prop: 'mobile',
          dataType: 'TEXT',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'property_account_position_id',
          prop: 'property_account_position_id',
          dataType: 'PICKUP',
        },
        {
          name: 'country_id',
          prop: 'country_id',
          dataType: 'PICKUP',
        },
        {
          name: 'property_purchase_currency_id',
          prop: 'property_purchase_currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'property_stock_supplier',
          prop: 'property_stock_supplier',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'property_stock_subcontractor',
          prop: 'property_stock_subcontractor',
          dataType: 'PICKUP',
        },
        {
          name: 'state_id',
          prop: 'state_id',
          dataType: 'PICKUP',
        },
        {
          name: 'title',
          prop: 'title',
          dataType: 'PICKUP',
        },
        {
          name: 'property_stock_customer',
          prop: 'property_stock_customer',
          dataType: 'PICKUP',
        },
        {
          name: 'property_supplier_payment_term_id',
          prop: 'property_supplier_payment_term_id',
          dataType: 'PICKUP',
        },
        {
          name: 'property_delivery_carrier_id',
          prop: 'property_delivery_carrier_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'property_product_pricelist',
          prop: 'property_product_pricelist',
          dataType: 'PICKUP',
        },
        {
          name: 'property_payment_term_id',
          prop: 'property_payment_term_id',
          dataType: 'PICKUP',
        },
        {
          name: 'parent_id',
          prop: 'parent_id',
          dataType: 'PICKUP',
        },
        {
          name: 'res_supplier',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}