/**
 * EF_Master 部件模型
 *
 * @export
 * @class EF_MasterModel
 */
export default class EF_MasterModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'type',
        prop: 'type',
        dataType: 'SSCODELIST',
      },
      {
        name: 'vat',
        prop: 'vat',
        dataType: 'TEXT',
      },
      {
        name: 'is_company',
        prop: 'is_company',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'parent_name',
        prop: 'parent_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'ibizfunction',
        prop: 'function',
        dataType: 'TEXT',
      },
      {
        name: 'title_text',
        prop: 'title_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'phone',
        prop: 'phone',
        dataType: 'TEXT',
      },
      {
        name: 'mobile',
        prop: 'mobile',
        dataType: 'TEXT',
      },
      {
        name: 'email',
        prop: 'email',
        dataType: 'TEXT',
      },
      {
        name: 'website_url',
        prop: 'website_url',
        dataType: 'TEXT',
      },
      {
        name: 'category_id',
        prop: 'category_id',
        dataType: 'ONE2MANYDATA',
      },
      {
        name: 'country_id_text',
        prop: 'country_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'state_id_text',
        prop: 'state_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'city',
        prop: 'city',
        dataType: 'TEXT',
      },
      {
        name: 'street',
        prop: 'street',
        dataType: 'TEXT',
      },
      {
        name: 'street2',
        prop: 'street2',
        dataType: 'TEXT',
      },
      {
        name: 'zip',
        prop: 'zip',
        dataType: 'TEXT',
      },
      {
        name: 'user_name',
        prop: 'user_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_delivery_carrier_name',
        prop: 'property_delivery_carrier_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_payment_term_name',
        prop: 'property_payment_term_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_product_pricelist_name',
        prop: 'property_product_pricelist_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'barcode',
        prop: 'barcode',
        dataType: 'TEXT',
      },
      {
        name: 'property_supplier_payment_term_name',
        prop: 'property_supplier_payment_term_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_purchase_currency_name',
        prop: 'property_purchase_currency_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_stock_customer_name',
        prop: 'property_stock_customer_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_stock_supplier_name',
        prop: 'property_stock_supplier_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_stock_subcontractor_name',
        prop: 'property_stock_subcontractor_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'ref',
        prop: 'ref',
        dataType: 'TEXT',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_account_position_name',
        prop: 'property_account_position_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'property_account_position_id',
        prop: 'property_account_position_id',
        dataType: 'PICKUP',
      },
      {
        name: 'country_id',
        prop: 'country_id',
        dataType: 'PICKUP',
      },
      {
        name: 'property_purchase_currency_id',
        prop: 'property_purchase_currency_id',
        dataType: 'PICKUP',
      },
      {
        name: 'property_stock_supplier',
        prop: 'property_stock_supplier',
        dataType: 'PICKUP',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'property_stock_subcontractor',
        prop: 'property_stock_subcontractor',
        dataType: 'PICKUP',
      },
      {
        name: 'state_id',
        prop: 'state_id',
        dataType: 'PICKUP',
      },
      {
        name: 'title',
        prop: 'title',
        dataType: 'PICKUP',
      },
      {
        name: 'property_stock_customer',
        prop: 'property_stock_customer',
        dataType: 'PICKUP',
      },
      {
        name: 'property_delivery_carrier_id',
        prop: 'property_delivery_carrier_id',
        dataType: 'PICKUP',
      },
      {
        name: 'property_supplier_payment_term_id',
        prop: 'property_supplier_payment_term_id',
        dataType: 'PICKUP',
      },
      {
        name: 'user_id',
        prop: 'user_id',
        dataType: 'PICKUP',
      },
      {
        name: 'property_payment_term_id',
        prop: 'property_payment_term_id',
        dataType: 'PICKUP',
      },
      {
        name: 'property_product_pricelist',
        prop: 'property_product_pricelist',
        dataType: 'PICKUP',
      },
      {
        name: 'parent_id',
        prop: 'parent_id',
        dataType: 'PICKUP',
      },
      {
        name: 'res_supplier',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}