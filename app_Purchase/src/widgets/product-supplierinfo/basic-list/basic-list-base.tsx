import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import Product_supplierinfoService from '@/service/product-supplierinfo/product-supplierinfo-service';
import BasicService from './basic-list-service';
import Product_supplierinfoUIService from '@/uiservice/product-supplierinfo/product-supplierinfo-ui-service';


/**
 * listexpbar_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {BasicListBase}
 */
export class BasicListBase extends ListControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {BasicService}
     * @memberof BasicListBase
     */
    public service: BasicService = new BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Product_supplierinfoService}
     * @memberof BasicListBase
     */
    public appEntityService: Product_supplierinfoService = new Product_supplierinfoService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeName: string = 'product_supplierinfo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeLogicName: string = '供应商价格表';

    /**
     * 界面UI服务对象
     *
     * @type {Product_supplierinfoUIService}
     * @memberof BasicBase
     */  
    public appUIService:Product_supplierinfoUIService = new Product_supplierinfoUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof BasicListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof BasicListBase
     */
    public minorSortDir: string = '';


}