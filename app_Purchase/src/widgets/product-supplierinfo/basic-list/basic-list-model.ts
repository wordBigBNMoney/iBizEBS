/**
 * Basic 部件模型
 *
 * @export
 * @class BasicModel
 */
export default class BasicModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof BasicListexpbar_listMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'price',
			},
			{
				name: 'product_tmpl_id_text',
			},
			{
				name: 'name_text',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name_text',
				dataType: 'PICKUPTEXT',
			},
			{
				name: 'product_tmpl_id',
				prop: 'product_tmpl_id',
				dataType: 'PICKUP',
			},
			{
				name: 'company_id',
				prop: 'company_id',
				dataType: 'PICKUP',
			},
			{
				name: 'product_id',
				prop: 'product_id',
				dataType: 'PICKUP',
			},
			{
				name: 'purchase_requisition_line_id',
				prop: 'purchase_requisition_line_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'currency_id',
				prop: 'currency_id',
				dataType: 'PICKUP',
			},
			{
				name: 'name',
				prop: 'name',
				dataType: 'PICKUP',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'product_supplierinfo',
				prop: 'id',
				dataType: 'FONTKEY',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}