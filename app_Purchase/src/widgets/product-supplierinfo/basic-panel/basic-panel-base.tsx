import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import Product_supplierinfoService from '@/service/product-supplierinfo/product-supplierinfo-service';
import BasicService from './basic-panel-service';
import Product_supplierinfoUIService from '@/uiservice/product-supplierinfo/product-supplierinfo-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import BasicModel from './basic-panel-model';
import CodeListService from "@service/app/codelist-service";


/**
 * listexpbar_list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {BasicPanelBase}
 */
export class BasicPanelBase extends PanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof BasicPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {BasicService}
     * @memberof BasicPanelBase
     */
    public service: BasicService = new BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Product_supplierinfoService}
     * @memberof BasicPanelBase
     */
    public appEntityService: Product_supplierinfoService = new Product_supplierinfoService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof BasicPanelBase
     */
    protected appDeName: string = 'product_supplierinfo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof BasicPanelBase
     */
    protected appDeLogicName: string = '供应商价格表';

    /**
     * 界面UI服务对象
     *
     * @type {Product_supplierinfoUIService}
     * @memberof BasicBase
     */  
    public appUIService:Product_supplierinfoUIService = new Product_supplierinfoUIService(this.$store);


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Basic
     */
    public detailsModel: any = {
        product_tmpl_id_text: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'product_tmpl_id_text', panel: this })
,
        name_text: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'name_text', panel: this })
,
        price: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'price', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof Basic
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                



    }

    /**
     * 数据模型对象
     *
     * @type {BasicModel}
     * @memberof Basic
     */
    public dataModel: BasicModel = new BasicModel();

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof Basic
     */
    public uiAction(row: any, tag: any, $event: any) {
    }
}