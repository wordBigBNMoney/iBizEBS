/**
 * EF_Basic 部件模型
 *
 * @export
 * @class EF_BasicModel
 */
export default class EF_BasicModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_BasicModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name_text',
        prop: 'name_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'product_code',
        prop: 'product_code',
        dataType: 'TEXT',
      },
      {
        name: 'product_name',
        prop: 'product_name',
        dataType: 'TEXT',
      },
      {
        name: 'product_id_text',
        prop: 'product_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'delay',
        prop: 'delay',
        dataType: 'INT',
      },
      {
        name: 'product_tmpl_id_text',
        prop: 'product_tmpl_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'min_qty',
        prop: 'min_qty',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'date_start',
        prop: 'date_start',
        dataType: 'DATE',
      },
      {
        name: 'date_end',
        prop: 'date_end',
        dataType: 'DATE',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'product_tmpl_id',
        prop: 'product_tmpl_id',
        dataType: 'PICKUP',
      },
      {
        name: 'product_id',
        prop: 'product_id',
        dataType: 'PICKUP',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'PICKUP',
      },
      {
        name: 'product_supplierinfo',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}