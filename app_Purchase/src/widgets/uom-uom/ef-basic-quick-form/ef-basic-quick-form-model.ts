/**
 * EF_BasicQuick 部件模型
 *
 * @export
 * @class EF_BasicQuickModel
 */
export default class EF_BasicQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_BasicQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'category_id_text',
        prop: 'category_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rounding',
        prop: 'rounding',
        dataType: 'FLOAT',
      },
      {
        name: 'active',
        prop: 'active',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'category_id',
        prop: 'category_id',
        dataType: 'PICKUP',
      },
      {
        name: 'uom_uom',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}