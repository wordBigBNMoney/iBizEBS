import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Uom_uomService from '@/service/uom-uom/uom-uom-service';
import EF_BasicService from './ef-basic-form-service';
import Uom_uomUIService from '@/uiservice/uom-uom/uom-uom-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_BasicEditFormBase}
 */
export class EF_BasicEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_BasicService}
     * @memberof EF_BasicEditFormBase
     */
    public service: EF_BasicService = new EF_BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Uom_uomService}
     * @memberof EF_BasicEditFormBase
     */
    public appEntityService: Uom_uomService = new Uom_uomService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected appDeName: string = 'uom_uom';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected appDeLogicName: string = '产品计量单位';

    /**
     * 界面UI服务对象
     *
     * @type {Uom_uomUIService}
     * @memberof EF_BasicBase
     */  
    public appUIService:Uom_uomUIService = new Uom_uomUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        category_id_text: null,
        active: null,
        uom_type: null,
        rounding: null,
        factor_inv: null,
        id: null,
        category_id: null,
        uom_uom:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public rules():any{
        return {
        name: [
            { required: this.detailsModel.name.required, type: 'string', message: '单位 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name.required, type: 'string', message: '单位 值不能为空', trigger: 'blur' },
        ],
        category_id_text: [
            { required: this.detailsModel.category_id_text.required, type: 'string', message: '类别 值不能为空', trigger: 'change' },
            { required: this.detailsModel.category_id_text.required, type: 'string', message: '类别 值不能为空', trigger: 'blur' },
        ],
        active: [
            { required: this.detailsModel.active.required, type: 'string', message: '有效 值不能为空', trigger: 'change' },
            { required: this.detailsModel.active.required, type: 'string', message: '有效 值不能为空', trigger: 'blur' },
            { validator: (rule:any, value:any, callback:any) => { 
                if(rule.required && value != 1){
                   return false;
                }else{
                   return true;
                }
              }, message: '有效 必须填写', trigger: 'change' ,required:true}
        ],
        uom_type: [
            { required: this.detailsModel.uom_type.required, type: 'string', message: '类型 值不能为空', trigger: 'change' },
            { required: this.detailsModel.uom_type.required, type: 'string', message: '类型 值不能为空', trigger: 'blur' },
        ],
        rounding: [
            { required: this.detailsModel.rounding.required, type: 'number', message: '舍入精度 值不能为空', trigger: 'change' },
            { required: this.detailsModel.rounding.required, type: 'number', message: '舍入精度 值不能为空', trigger: 'blur' },
        ],
        factor_inv: [
            { required: this.detailsModel.factor_inv.required, type: 'number', message: '更大比率 值不能为空', trigger: 'change' },
            { required: this.detailsModel.factor_inv.required, type: 'number', message: '更大比率 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public detailsModel: any = {
        rawitem1: new FormRowItemModel({ caption: '', detailType: 'RAWITEM', name: 'rawitem1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel2: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: false, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.uom_uom.ef_basic_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.uom_uom.ef_basic_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '单位', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '单位', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        category_id_text: new FormItemModel({ caption: '类别', detailType: 'FORMITEM', name: 'category_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        active: new FormItemModel({ caption: '有效', detailType: 'FORMITEM', name: 'active', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        uom_type: new FormItemModel({ caption: '类型', detailType: 'FORMITEM', name: 'uom_type', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        rounding: new FormItemModel({ caption: '舍入精度', detailType: 'FORMITEM', name: 'rounding', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        factor_inv: new FormItemModel({ caption: '更大比率', detailType: 'FORMITEM', name: 'factor_inv', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 0 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        category_id: new FormItemModel({ caption: '类别', detailType: 'FORMITEM', name: 'category_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };

    /**
     * 表单项逻辑
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @returns {Promise<void>}
     * @memberof EF_BasicEditFormBase
     */
    public async formLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): Promise<void> {
                

        if (Object.is(name, '') || Object.is(name, 'uom_type')) {
            let ret = false;
            const _uom_type = this.data.uom_type;
            if (this.$verify.testCond(_uom_type, 'EQ', 'bigger') || this.$verify.testCond(_uom_type, 'EQ', 'smaller')) {
                ret = true;
            }
            this.detailsModel.grouppanel2.setVisible(ret);
        }



















    }
}