import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * PurchaseMPickupViewpickupviewpanel 部件服务对象
 *
 * @export
 * @class PurchaseMPickupViewpickupviewpanelService
 */
export default class PurchaseMPickupViewpickupviewpanelService extends ControlService {
}