/**
 * EF_Master 部件模型
 *
 * @export
 * @class EF_MasterModel
 */
export default class EF_MasterModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'user_name',
        prop: 'user_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'type_id_text',
        prop: 'type_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'vendor_id_text',
        prop: 'vendor_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'currency_name',
        prop: 'currency_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'date_end',
        prop: 'date_end',
        dataType: 'DATETIME',
      },
      {
        name: 'ordering_date',
        prop: 'ordering_date',
        dataType: 'DATE',
      },
      {
        name: 'schedule_date',
        prop: 'schedule_date',
        dataType: 'DATE',
      },
      {
        name: 'origin',
        prop: 'origin',
        dataType: 'TEXT',
      },
      {
        name: 'company_name',
        prop: 'company_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'currency_id',
        prop: 'currency_id',
        dataType: 'PICKUP',
      },
      {
        name: 'user_id',
        prop: 'user_id',
        dataType: 'PICKUP',
      },
      {
        name: 'vendor_id',
        prop: 'vendor_id',
        dataType: 'PICKUP',
      },
      {
        name: 'type_id',
        prop: 'type_id',
        dataType: 'PICKUP',
      },
      {
        name: 'purchase_requisition',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}