/**
 * MasterSummary 部件模型
 *
 * @export
 * @class MasterSummaryModel
 */
export default class MasterSummaryModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MasterSummaryModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'name',
      },
      {
        name: 'origin',
      },
      {
        name: 'date_end',
      },
      {
        name: 'schedule_date',
      },
      {
        name: 'create_date',
      },
      {
        name: 'purchase_requisition',
        prop: 'id',
      },
      {
        name: 'ordering_date',
      },
      {
        name: '__last_update',
      },
      {
        name: 'state',
      },
      {
        name: 'write_date',
      },
      {
        name: 'order_count',
      },
      {
        name: 'currency_name',
      },
      {
        name: 'create_uname',
      },
      {
        name: 'quantity_copy',
      },
      {
        name: 'company_name',
      },
      {
        name: 'user_name',
      },
      {
        name: 'type_id_text',
      },
      {
        name: 'vendor_id_text',
      },
      {
        name: 'write_uname',
      },
      {
        name: 'picking_type_id',
      },
      {
        name: 'vendor_id',
      },
      {
        name: 'type_id',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'warehouse_id',
      },
      {
        name: 'user_id',
      },
      {
        name: 'company_id',
      },
    ]
  }


}
