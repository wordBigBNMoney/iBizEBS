import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import Purchase_requisitionService from '@/service/purchase-requisition/purchase-requisition-service';
import MasterTabInfoViewtabexppanelService from './master-tab-info-viewtabexppanel-tabexppanel-service';
import Purchase_requisitionUIService from '@/uiservice/purchase-requisition/purchase-requisition-ui-service';
import Purchase_requisitionAuthService from '@/authservice/purchase-requisition/purchase-requisition-auth-service';
import { Environment } from '@/environments/environment';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {MasterTabInfoViewtabexppanelTabexppanelBase}
 */
export class MasterTabInfoViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {MasterTabInfoViewtabexppanelService}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    public service: MasterTabInfoViewtabexppanelService = new MasterTabInfoViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisitionService}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    public appEntityService: Purchase_requisitionService = new Purchase_requisitionService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'purchase_requisition';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '采购申请';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_requisitionUIService}
     * @memberof MasterTabInfoViewtabexppanelBase
     */  
    public appUIService:Purchase_requisitionUIService = new Purchase_requisitionUIService(this.$store);

    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected isInit: any = {
        tabviewpanel21:  true ,
        tabviewpanel22:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected activatedTabViewPanel: string = 'tabviewpanel21';

    /**
     * 实体权限服务对象
     *
     * @protected
     * @type Purchase_requisitionAuthServiceBase
     * @memberof TabExpViewtabexppanelBase
     */
    protected appAuthService: Purchase_requisitionAuthService = new Purchase_requisitionAuthService();

    /**
     * 分页面板权限标识存储对象
     *
     * @protected
     * @type {*}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected authResourceObject:any = {'tabviewpanel21':{resourcetag:null,visabled: true,disabled: false},'tabviewpanel22':{resourcetag:null,visabled: true,disabled: false}};

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.purchase_requisition) {
            Object.assign(this.context, { srfparentdename: 'Purchase_requisition', srfparentkey: this.context.purchase_requisition });
        }
        super.ctrlCreated();
    }
}