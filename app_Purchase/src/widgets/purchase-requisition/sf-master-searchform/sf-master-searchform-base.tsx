import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import Purchase_requisitionService from '@/service/purchase-requisition/purchase-requisition-service';
import SF_MasterService from './sf-master-searchform-service';
import Purchase_requisitionUIService from '@/uiservice/purchase-requisition/purchase-requisition-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {SF_MasterSearchFormBase}
 */
export class SF_MasterSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SF_MasterSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {SF_MasterService}
     * @memberof SF_MasterSearchFormBase
     */
    public service: SF_MasterService = new SF_MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisitionService}
     * @memberof SF_MasterSearchFormBase
     */
    public appEntityService: Purchase_requisitionService = new Purchase_requisitionService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SF_MasterSearchFormBase
     */
    protected appDeName: string = 'purchase_requisition';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SF_MasterSearchFormBase
     */
    protected appDeLogicName: string = '采购申请';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_requisitionUIService}
     * @memberof SF_MasterBase
     */  
    public appUIService:Purchase_requisitionUIService = new Purchase_requisitionUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof SF_MasterSearchFormBase
     */
    public data: any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof SF_MasterSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
    };

    /**
     * 新建默认值
     * @memberof SF_MasterBase
     */
    public createDefault(){                    
    }
}