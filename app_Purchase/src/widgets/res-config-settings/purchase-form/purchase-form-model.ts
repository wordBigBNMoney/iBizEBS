/**
 * Purchase 部件模型
 *
 * @export
 * @class PurchaseModel
 */
export default class PurchaseModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof PurchaseModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'po_order_approval',
        prop: 'po_order_approval',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'po_double_validation',
        prop: 'po_double_validation',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'po_double_validation_amount',
        prop: 'po_double_validation_amount',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'lock_confirmed_po',
        prop: 'lock_confirmed_po',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'group_warning_purchase',
        prop: 'group_warning_purchase',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'module_purchase_requisition',
        prop: 'module_purchase_requisition',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'po_lock',
        prop: 'po_lock',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'po_lead',
        prop: 'po_lead',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'purchase_tax_id',
        prop: 'purchase_tax_id',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'default_purchase_method',
        prop: 'default_purchase_method',
        dataType: 'SSCODELIST',
      },
      {
        name: 'module_account_3way_match',
        prop: 'module_account_3way_match',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'module_stock_dropshipping',
        prop: 'module_stock_dropshipping',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'res_config_settings',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}