/**
 * List 部件模型
 *
 * @export
 * @class ListModel
 */
export default class ListModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ListListMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'res_name',
			},
			{
				name: 'create_date',
			},
			{
				name: 'date_deadline',
			},
			{
				name: 'icon',
			},
			{
				name: 'activity_type_id_text',
			},
			{
				name: 'user_id_text',
			},
			{
				name: 'note',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'recommended_activity_type_id',
				prop: 'recommended_activity_type_id',
				dataType: 'PICKUP',
			},
			{
				name: 'activity_type_id',
				prop: 'activity_type_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'user_id',
				prop: 'user_id',
				dataType: 'PICKUP',
			},
			{
				name: 'note_id',
				prop: 'note_id',
				dataType: 'PICKUP',
			},
			{
				name: 'previous_activity_type_id',
				prop: 'previous_activity_type_id',
				dataType: 'PICKUP',
			},
			{
				name: 'calendar_event_id',
				prop: 'calendar_event_id',
				dataType: 'PICKUP',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'mail_activity',
				prop: 'id',
				dataType: 'FONTKEY',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}