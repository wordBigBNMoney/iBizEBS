export default {
  fields: {
    product_qty: "数量",
    __last_update: "最后修改日",
    write_date: "最后更新时间",
    id: "ID",
    create_date: "创建时间",
    schedule_date: "安排的日期",
    qty_ordered: "已订购数量",
    price_unit: "价格",
    company_name: "公司名称",
    requisition_name: "采购申请",
    product_uom_name: "单位",
    move_dest_name: "说明",
    account_analytic_name: "分析账户",
    write_uname: "最后更新人",
    product_name: "产品",
    create_uname: "创建人",
    write_uid: "最后更新人",
    product_id: "产品",
    product_uom_id: "单位",
    create_uid: "创建人",
    move_dest_id: "说明",
    account_analytic_id: "分析账户",
    requisition_id: "采购申请",
    company_id: "公司",
  },
	views: {
		editview: {
			caption: "采购申请行",
      		title: "订单申请行编辑视图",
		},
		lineedit: {
			caption: "采购申请行",
      		title: "行编辑表格视图",
		},
		line: {
			caption: "采购申请行",
      		title: "行编辑表格视图",
		},
	},
	main_form: {
		details: {
			group1: "订单申请行基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "ID", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	line_grid: {
		nodata: "",
		columns: {
			product_name: "产品",
			product_qty: "数量",
			qty_ordered: "已订购数量",
			product_uom_name: "单位",
			schedule_date: "安排的日期",
			price_unit: "价格",
		},
		uiactions: {
		},
	},
	lineedit_grid: {
		nodata: "",
		columns: {
			product_name: "产品",
			product_qty: "数量",
			qty_ordered: "已订购数量",
			product_uom_name: "单位",
			schedule_date: "安排的日期",
			price_unit: "价格",
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	lineedittoolbar_toolbar: {
		tbitem24: {
			caption: "行编辑",
			tip: "行编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem25: {
			caption: "新建行",
			tip: "新建行",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "保存行",
			tip: "保存行",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem11: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
	},
};