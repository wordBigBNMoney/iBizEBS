export default {
  fields: {
    is_published: "是否公开",
    fixed_price: "固定价格",
    delivery_type: "供应商",
    write_date: "最后更新时间",
    return_label_on_delivery: "交易退货标签",
    name: "名称",
    invoice_policy: "发票原则",
    create_date: "创建时间",
    sequence: "序号",
    margin: "利润率",
    id: "ID",
    amount: "金额",
    debug_logging: "调试记录",
    zip_to: "邮编到",
    zip_from: "邮编从",
    free_over: "如果订货量大则免费",
    integration_level: "集成级别",
    prod_environment: "生产环境",
    active: "有效",
    get_return_label_from_portal: "从门户获取返回标签",
    product_name: "名称",
    company_name: "公司名称",
    write_uname: "名称",
    create_uname: "名称",
    write_uid: "ID",
    company_id: "ID",
    product_id: "ID",
    create_uid: "ID",
  },
	views: {
		pickupview: {
			caption: "送货方式",
      		title: "送货方式数据选择视图",
		},
		pickupgridview: {
			caption: "送货方式",
      		title: "送货方式选择表格视图",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};