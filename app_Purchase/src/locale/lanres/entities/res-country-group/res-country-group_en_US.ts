export default {
  fields: {
    write_date: "最后更新时间",
    __last_update: "最后修改日",
    id: "ID",
    pricelist_ids: "价格表",
    name: "名称",
    display_name: "显示名称",
    create_date: "创建时间",
    country_ids: "国家/地区",
    create_uid_text: "创建人",
    write_uid_text: "最后更新者",
    write_uid: "最后更新者",
    create_uid: "创建人",
  },
};