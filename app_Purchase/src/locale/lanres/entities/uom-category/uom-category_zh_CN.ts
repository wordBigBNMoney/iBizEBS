export default {
  fields: {
    name: "计量单位类别",
    create_date: "创建时间",
    __last_update: "最后修改日",
    write_date: "最后更新时间",
    is_pos_groupable: "分组销售点中的产品",
    display_name: "显示名称",
    id: "ID",
    measure_type: "计量类型",
    create_uid_text: "创建人",
    write_uid_text: "最后更新者",
    write_uid: "最后更新者",
    create_uid: "创建人",
  },
	views: {
		basicquickview: {
			caption: "快速新建",
      		title: "快速新建视图",
		},
		basiceditview: {
			caption: "供应商价格表",
      		title: "配置信息编辑视图",
		},
		pickupgridview: {
			caption: "产品计量单位 类别",
      		title: "产品计量单位 类别选择表格视图",
		},
		pickupview: {
			caption: "产品计量单位 类别",
      		title: "产品计量单位 类别数据选择视图",
		},
		basiclistexpview: {
			caption: "产品计量单位 类别",
      		title: "配置列表导航视图",
		},
	},
	ef_basicquick_form: {
		details: {
			grouppanel1: "分组面板", 
			formpage1: "基本信息", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "计量单位类别", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "计量单位类别", 
			measure_type: "计量类型", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	ef_basic_form: {
		details: {
			grouppanel1: "基本信息", 
			formpage1: "基本信息", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "计量单位类别", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "计量单位类别", 
			measure_type: "计量类型", 
			is_pos_groupable: "分组销售点中的产品", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "计量单位类别",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	basiclistexpviewlistexpbar_toolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
	},
	basic_list: {
		nodata: "",
		uiactions: {
		},
	},
};