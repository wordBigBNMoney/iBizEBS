export default {
  fields: {
    sanitized_acc_number: "核对银行账号",
    display_name: "显示名称",
    sequence: "序号",
    journal_id: "会计日记账",
    id: "ID",
    create_date: "创建时间",
    __last_update: "最后修改日",
    acc_number: "账户号码",
    acc_type: "类型",
    qr_code_valid: "‎有所有必需的参数‎",
    acc_holder_name: "账户持有人名称",
    write_date: "最后更新时间",
    bank_name: "名称",
    create_uid_text: "创建人",
    partner_id_text: "账户持有人",
    currency_id_text: "币种",
    bank_bic: "银行识别代码",
    write_uid_text: "最后更新者",
    company_id_text: "公司",
    write_uid: "最后更新者",
    bank_id: "银行",
    company_id: "公司",
    partner_id: "账户持有人",
    create_uid: "创建人",
    currency_id: "币种",
  },
	views: {
		line: {
			caption: "银行账户",
      		title: "行编辑表格视图",
		},
		lineedit: {
			caption: "银行账户",
      		title: "行编辑表格视图",
		},
		editview: {
			caption: "银行账户",
      		title: "银行账户编辑视图",
		},
	},
	main_form: {
		details: {
			group1: "银行账户基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "ID", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	line_grid: {
		nodata: "",
		columns: {
			bank_name: "银行",
			acc_number: "账户号码",
		},
		uiactions: {
		},
	},
	lineedit_grid: {
		nodata: "",
		columns: {
			bank_name: "银行",
			acc_number: "账户号码",
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "帮助",
			tip: "帮助",
		},
	},
	lineedittoolbar_toolbar: {
		tbitem24: {
			caption: "行编辑",
			tip: "行编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem25: {
			caption: "新建行",
			tip: "新建行",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "保存行",
			tip: "保存行",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem11: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
	},
};