export default {
  fields: {
    write_date: "最后更新时间",
    id: "ID",
    name: "名称",
    __last_update: "最后修改日",
    active: "有效",
    create_date: "创建时间",
    code: "代码",
    display_name: "显示名称",
    write_uid_text: "最后更新人",
    create_uid_text: "创建人",
    create_uid: "创建人",
    write_uid: "最后更新人",
  },
};